package org.mysys.listeners;

import javax.persistence.PrePersist;

import org.mysys.model.AcctYearAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.env.Environment;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Configurable
public class MysysEntityListener{
	
	@Autowired
	private Environment env;
	
	@PrePersist 
	void onPrePersist(AcctYearAware<String> o) {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		o.setAcctyear(env.getProperty("application.acctYear"));
	}
}
