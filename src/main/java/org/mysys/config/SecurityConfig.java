package org.mysys.config;

import org.mysys.constant.APPConstant;
import org.mysys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.header.HeaderWriterFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;

	@Autowired
	private AuthenticationSuccessHandlerImpl successHandler;

	@Override
	
    protected void configure(HttpSecurity http) throws Exception {
    	http.addFilterAfter(new CharacterEncodingFilter(APPConstant.CONTENT_ENCODING, true),HeaderWriterFilter.class);
    	http.sessionManagement().maximumSessions(1).expiredUrl("/logout");
    	http
    	.authorizeRequests()
    	.antMatchers("/assets/**","/pages/**","/css/**","/images/**", "/bootstrap/**","/bootstrap/css/**" ,"/bootstrap/js/**", "/js/**", "/templates/pages/**","/forgetPassword")	//.hasIpAddress("127.0.0.1")	//TODO
    	.permitAll()
    	.anyRequest().authenticated()
    	.and()    
    	.formLogin()
    	.loginPage("/login")
    	.failureUrl("/login?error=true")
    	.successHandler(successHandler)
    	.permitAll()
    	.and()
    	.logout()
    	.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
    	.invalidateHttpSession(true)
    	.permitAll();
    	/*.and()
    	.sessionManagement()
    	.maximumSessions(1);*/
    }
	
//	public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**").allowedMethods("*");
//    }

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
		auth.setUserDetailsService(userService);
		auth.setPasswordEncoder(passwordEncoder());
		return auth;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/job/getAllJobOpeningList").antMatchers("/job/getJobMaster").antMatchers("/job/saveApplicantMasterWeb");
		
	}
	
	

}