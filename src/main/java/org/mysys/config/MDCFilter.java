package org.mysys.config;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.jboss.logging.MDC;
import org.mysys.model.security.CustomUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

public class MDCFilter implements Filter{
	
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		//  Authentication authentication =
		Authentication authentication =
		        SecurityContextHolder.getContext().getAuthentication();
		String g = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

		MDC.put("clientip", req.getRemoteAddr());
		MDC.put("requestid", requestId());
		
        if (authentication != null && !g.equalsIgnoreCase("anonymousUser")) {
        	Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        	if(user != null){
        		CustomUser mySysUser = (CustomUser) user;
                MDC.put("username", mySysUser.getUsername());
        	}
          }

		try {
			chain.doFilter(req, res);
		} catch (Exception e) {
			MDC.remove("username");
			MDC.remove("clientip");
			MDC.remove("requestid");
		}
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}
	
	
	public String requestId(){
		return UUID.randomUUID().toString();
	}
	
}
