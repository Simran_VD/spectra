package org.mysys.config;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mysys.constant.APPConstant;
import org.mysys.model.User;
import org.mysys.model.UserSessionLog;
import org.mysys.repository.UserSessionLogRepo;
import org.mysys.service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component("authenticationSuccessHandler")

public class AuthenticationSuccessHandlerImpl extends AbstractService implements AuthenticationSuccessHandler {

	private static final Logger logger = LogManager.getLogger();

	@Autowired
	private UserSessionLogRepo sessionLogRepo;

	
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
		User user = userRepo.findByLoginid(authentication.getName());
		handle(request, response, authentication);
		HttpSession session = request.getSession(false);
		if (session != null) {
			UserSessionLog sessionLog = new UserSessionLog(APPConstant.LOGGED_IN_ACTION, session.getId(),user.getSiteid(), user.getUserid());
			sessionLogRepo.save(sessionLog);
			session.setAttribute("user", user);
		}
		clearAuthenticationAttributes(request);
	}

	protected void handle(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) throws IOException {
		final String targetUrl = determineTargetUrl(authentication);

		if (response.isCommitted()) {
			logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
			return;
		}
		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	protected String determineTargetUrl(final Authentication authentication) {
		return "/";
	}


	protected void clearAuthenticationAttributes(final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		if (session == null) {
			return;
		}
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}
}