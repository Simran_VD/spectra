package org.mysys.config;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import org.mysys.constant.APPConstant;
import org.mysys.model.User;
import org.mysys.model.UserSessionLog;
import org.mysys.repository.UserSessionLogRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.web.session.HttpSessionDestroyedEvent;
import org.springframework.stereotype.Component;


@Component 
public class SessionListener implements ApplicationListener<HttpSessionDestroyedEvent> { 
	
	@Autowired
	private UserSessionLogRepo sessionLogRepo;
	
	
	public void onApplicationEvent(HttpSessionDestroyedEvent evt) {
		HttpSession session = evt.getSession();
		User user = (User)session.getAttribute("user");
		if(user != null){
			UserSessionLog sessionLog = new UserSessionLog(APPConstant.LOGGED_OUT_ACTION, session.getId(), user.getSiteid(), user.getUserid());
			sessionLogRepo.save(sessionLog);
		}
		
	}
	
}
