package org.mysys.config;

import java.io.File;

import org.springframework.boot.system.ApplicationHome;

public class ApplicationDynamicPath {
	
	
	/***********************GET PATH OF PARENT FOLDER WHERE JAR IS RUNNING*********************/
	public static File getPath() {
		
		try {
			String jarDir = new File(".").getCanonicalPath();
//			
//			ApplicationHome home= new ApplicationHome(GrpCabServiceApplication.class);			
//			jarDir=home.getDir();	
			System.out.println(jarDir);
			File f = new File(jarDir);
			return f;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static void main(String[] args) {
		getPath();
	}
	

}
