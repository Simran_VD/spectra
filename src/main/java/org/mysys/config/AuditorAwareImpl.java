package org.mysys.config;

import java.util.Optional;

import org.mysys.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

public class AuditorAwareImpl implements AuditorAware<String> {
	
	@Autowired
	protected UserRepository userRepo;

	@Override
	public Optional<String> getCurrentAuditor() {
		return Optional.of(((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
	}

}
