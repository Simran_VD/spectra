package org.mysys.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class Logout extends SimpleUrlLogoutSuccessHandler{
	
	 @Override
	    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
	            Authentication authentication) throws IOException, ServletException {

	        if (authentication != null) {
	            // do something 
	        }

	        setDefaultTargetUrl("/login");
	        super.onLogoutSuccess(request, response, authentication);       
	    }

}
