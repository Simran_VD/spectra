package org.mysys.config;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mysys.model.notification.Note;
//import org.mysys.model.notification.NotifyListVw;
import org.mysys.service.AbstractService;
import org.mysys.service.FirebaseMessagingService;
import org.mysys.service.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.google.firebase.messaging.FirebaseMessagingException;

@EnableScheduling
@Configuration
public class SchedulerConfig extends AbstractService {
	
	@Autowired SimpMessagingTemplate template;
	
//	@Scheduled(fixedDelay = 1500) 
//	public void sendRecruiterNoti() { 
//		template.convertAndSend("/user/topic/user", notifyRepo.findAll());
//	}
	
	/*
	 * @Autowired SimpMessagingTemplate template;
	 * 
	 * @Autowired protected FirebaseMessagingService fcm;
	 * 
	 * @Autowired protected MasterService mstService;
	 * 
	 * @Scheduled(fixedDelay = 5000) public void sendAdhocMessages() throws
	 * FirebaseMessagingException {
	 * 
	 * try { Map<String,String> data= new HashMap<>(); Date d = null;
	 * List<NotifyListVw> teds=notifyRepo.findByMobileUpdDt(d); List<String> f= new
	 * ArrayList<>(); String ids=""; if(teds != null && teds.size() >0) {
	 * for(NotifyListVw lvw:teds) { if(lvw.getToken() !=null) {
	 * f.add(lvw.getToken()); ids+=lvw.getNtId()+",";
	 * data.put("Servlead",lvw.getMobileMessage());
	 * data.put("slId",String.valueOf(lvw.getSlId())); data.put("address","");
	 * data.put("custName",""); data.put("serviceReqTime",
	 * String.valueOf(lvw.getServRequiredAtTime())); data.put("Product Name",
	 * lvw.getProductName()); data.put("Service Name", lvw.getServiceName());
	 * data.put("mobile", ""); data.put("city",""); data.put("area",
	 * lvw.getAreaName()); data.put("ntType", lvw.getNtType()); } } if (f.size() >
	 * 0) { fcm.sendMultiNotification(new
	 * Note("Notification from Service","Service lead",data,"",""), f);
	 * mstService.notifyAtMobile(ids); } } } catch(Exception e) {
	 * System.out.println("Exception has been cought"); }
	 * template.convertAndSend("/user/topic/user", notifyRepo.findAll()); }
	 */
}