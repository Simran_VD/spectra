package org.mysys.constant;

public interface APPConstant {
	//TODO pick Timezone, date format, and date time formate from application properties.
	public static final String TIMEZONE = "Asia/Kolkata";
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
	public static final String DATE_TIME_FORMAT_HM = "dd/MM/yyyy HH:mm";
	
	public static final int LOGGED_IN_ACTION = 1;
	public static final int LOGGED_OUT_ACTION = 2;
	public static final int TIMED_OUT_ACTION = 3;
	
	public static final String Approval = "Y";
	
	public static final String CONTENT_ENCODING = "UTF-8";
	
	public enum EBILL{
		VERSION("1.0.0501"),
		INVOICE("Invoice"),
		JOBWORK("Jobwork");
		
		private final String value;
		private EBILL(String val){
			this.value = val;
		}
		@Override
        public String toString() {
            return this.value;
        }
	}
	
}
