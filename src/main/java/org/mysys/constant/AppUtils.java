package org.mysys.constant;

public final class AppUtils {

    public static String nullToEmptyString(Object obj) {
	if (obj == null)
	    return "";
	return obj.toString();
    }

    public static long nullToZero(Object obj) {
    	try{
    		return Long.valueOf(obj.toString());
    	} catch (Exception e) {
    		return 0l;

    	}
    }

    public static Float nullToFloatZero(Object obj) {
    	try{
    		return Float.valueOf(obj.toString());
    	} catch (Exception e) {
    		return 0f;
    	}
    }    
}
