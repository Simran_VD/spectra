package org.mysys.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the contactemaildetail database table.
 * 
 */
@Entity
@NamedQuery(name="Contactemaildetail.findAll", query="SELECT c FROM Contactemaildetail c")
public class Contactemaildetail extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="contactemaildtl_id_seq", name="contactemaildtlIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="contactemaildtlIdSeq")
	@Column(name="sno", updatable = false, nullable = false)
	private long sno;

	private String emailbody;

	@Temporal(TemporalType.DATE)
	private Date emaildate;

	private String subject;

	//bi-directional many-to-one association to Contact
	@ManyToOne
	@JoinColumn(name="contactid")
	private Contact contact;

	private long siteid;

	public Contactemaildetail() {
	}

	public long getSno() {
		return this.sno;
	}

	public void setSno(long sno) {
		this.sno = sno;
	}

	public String getEmailbody() {
		return this.emailbody;
	}

	public void setEmailbody(String emailbody) {
		this.emailbody = emailbody;
	}

	public Date getEmaildate() {
		return this.emaildate;
	}

	public void setEmaildate(Date emaildate) {
		this.emaildate = emaildate;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Contact getContact() {
		return this.contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}