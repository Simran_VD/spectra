package org.mysys.model.spectra;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "role_drop_down_vw")
@NamedQuery(name = "RoleDropDownVw.findAll", query = " select i from RoleDropDownVw i")
public class RoleDropDownVw extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	public long id;
	
	public String rolename;
	
	public Date startDate;
	
	public Date endDate;
	
	public Character status;
	
	public long siteid;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}
