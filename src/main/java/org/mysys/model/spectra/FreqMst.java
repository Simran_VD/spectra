package org.mysys.model.spectra;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

/**
 * The persistent class for the FreqMst database table.
 */
@Entity
@Table(name = "freq_mst")
@NamedQuery(name="FreqMst.findAll", query="SELECT c FROM FreqMst c")
public class FreqMst extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="freq_mst_seq", name="freqmstseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="freqmstseq")
	@Column(name="fm_id", updatable = false, nullable = false)
	private long fmId;
	
	@Column(name = "freq_name")
	private String freqName;
	
	private String isactive;
	
	private String remarks;
	
	private long siteid;
	
	@Column(name = "createdBy_id",insertable=true,updatable=false)
	private long createdById;
	
	@Column(name = "modifiedBy_id")
	private long modifiedById;

	public long getFmId() {
		return fmId;
	}

	public void setFmId(long fmId) {
		this.fmId = fmId;
	}

	
	public String getFreqName() {
		return freqName;
	}

	public void setFreqName(String freqName) {
		this.freqName = freqName;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public long getCreatedById() {
		return createdById;
	}

	public void setCreatedById(long createdById) {
		this.createdById = createdById;
	}

	public long getModifiedById() {
		return modifiedById;
	}

	public void setModifiedById(long modifiedById) {
		this.modifiedById = modifiedById;
	}

}
