package org.mysys.model.spectra;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="org_type_drop_down_vw")
public class OrgTypeVw extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ot_id", updatable = false, nullable = false)
	private long otid;

	@Column(name="org_type", updatable = false, nullable = false)
	private String orgType;
	
	private String isactive;

	public long getOtid() {
		return otid;
	}

	public void setOtid(long otid) {
		this.otid = otid;
	}

	
	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}
	
	
}
