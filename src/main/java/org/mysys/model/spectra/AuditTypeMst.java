package org.mysys.model.spectra;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

/**
 * The persistent class for the orgType database table.
 */
@Entity
@Table(name = "audit_type_mst")
@NamedQuery(name="AuditTypeMst.findAll", query="SELECT c FROM AuditTypeMst c")
public class AuditTypeMst extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="audit_type_mst_seq", name="audittypemstseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="audittypemstseq")
	@Column(name="atm_id", updatable = false, nullable = false)
	private long atmId;
	
	@Column(name = "audit_name")
	private String auditName;
	
	@Column(name = "audit_level")
	private String auditLevel;
	
	private String isactive;
	
	private String remarks;
	
	private long siteid;
	
	@Column(name = "createdBy_id",insertable=true,updatable=false)
	private long createdById;
	
	@Column(name = "modifiedBy_id")
	private long modifiedById;

	public long getAtmId() {
		return atmId;
	}

	public void setAtmId(long atmId) {
		this.atmId = atmId;
	}

	public String getAuditName() {
		return auditName;
	}

	public void setAuditName(String auditName) {
		this.auditName = auditName;
	}

	public String getAuditLevel() {
		return auditLevel;
	}

	public void setAuditLevel(String auditLevel) {
		this.auditLevel = auditLevel;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public long getCreatedById() {
		return createdById;
	}

	public void setCreatedById(long createdById) {
		this.createdById = createdById;
	}

	public long getModifiedById() {
		return modifiedById;
	}

	public void setModifiedById(long modifiedById) {
		this.modifiedById = modifiedById;
	}
	
	
}
