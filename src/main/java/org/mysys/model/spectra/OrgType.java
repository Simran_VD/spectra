package org.mysys.model.spectra;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

/**
 * The persistent class for the orgType database table.
 */
@Entity
@Table(name = "org_type")
@NamedQuery(name="OrgType.findAll", query="SELECT c FROM OrgType c")
public class OrgType extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="org_type_seq", name="orgtypeseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="orgtypeseq")
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ot_id", updatable = false, nullable = false)
	private long otId;
	
	@Column(name = "org_type")
	private String orgType;
	
	private String isactive;
	
	private String remarks;
	
	private long siteid;
	
	@Column(name = "createdBy_id",insertable=true,updatable=false)
	private long createdById;
	
	@Column(name = "modifiedBy_id")
	private long modifiedById;

	public long getOtId() {
		return otId;
	}

	public void setOtId(long otId) {
		this.otId = otId;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public long getCreatedById() {
		return createdById;
	}

	public void setCreatedById(long createdById) {
		this.createdById = createdById;
	}

	public long getModifiedById() {
		return modifiedById;
	}

	public void setModifiedById(long modifiedById) {
		this.modifiedById = modifiedById;
	}

	
}
