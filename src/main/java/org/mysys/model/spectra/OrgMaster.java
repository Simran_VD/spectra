package org.mysys.model.spectra;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="org_mst")
@NamedQuery(name="orgMaster.findAll", query="SELECT o FROM User o")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class OrgMaster extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="org_mst_seq", name="org_mst_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="org_mst_seq")
	@Column(name="org_id", updatable = false, nullable = false)
	private long orgid;
	
	@Column(name="org_name", updatable = false, nullable = false)
	private String orgName;
	
	@Column(name="display_name", updatable = false, nullable = false)
	private String displayName;
	
	@Column(name="ot_id", updatable = false, nullable = false)
	private String OtId;
	
	private Integer cityid;
	
	private Integer stateid;
	
	private String phone;
	
	private String email;
	
	private String mobile;
	
	private String landmark;
		
	private String address;
	
	private String pincode;
	
	private String gstin;
	
	private String pan;
	
	private String tan;
	
	private String cin;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT)
	@Temporal(TemporalType.DATE)
	private Date estb_date;
	
	@Column(name = "nature_of_business")
	private String natureOfBusiness;
	
	@Column(name = "ceo_name")
	private String ceoName;
	
	@Column(name = "ceo_mobile")
	private String ceoMobile;
	
	@Column(name = "ceo_email")
	private String ceoEmail;
	
	@Column(name = "nature_of_org")
	private String natureOfOrg;
	
	@Column(name = "corp_org_id")
	private Integer corpOrgId;
	
	@Column(name = "parent_org_id")
	private String parentOrgId;
	
	@Column(name = "contact_person")
	private String contactPerson;
	
	@Column(name = "contact_per_mobile")
	private String contactPersonMobile;
	
	@Column(name = "contact_per_email")
	private String contactPersonEmail;
	
	private String isActive;
	
	private String remarks;
	
	@Column(name = "createdBy_id")
	private Integer createdById;
	
	@Column(name = "modifiedBy_id")
	private Integer modifiedById;
	
	private Integer siteid;

	public long getOrgid() {
		return orgid;
	}

	public void setOrgid(long orgid) {
		this.orgid = orgid;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getOtId() {
		return OtId;
	}

	public void setOtId(String otId) {
		OtId = otId;
	}

	public Integer getCityid() {
		return cityid;
	}

	public void setCityid(Integer cityid) {
		this.cityid = cityid;
	}

	public Integer getStateid() {
		return stateid;
	}

	public void setStateid(Integer stateid) {
		this.stateid = stateid;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getGstin() {
		return gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getTan() {
		return tan;
	}

	public void setTan(String tan) {
		this.tan = tan;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public Date getEstb_date() {
		return estb_date;
	}

	public void setEstb_date(Date estb_date) {
		this.estb_date = estb_date;
	}

	public String getNatureOfBusiness() {
		return natureOfBusiness;
	}

	public void setNatureOfBusiness(String natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}

	public String getCeoName() {
		return ceoName;
	}

	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}

	public String getCeoMobile() {
		return ceoMobile;
	}

	public void setCeoMobile(String ceoMobile) {
		this.ceoMobile = ceoMobile;
	}

	public String getCeoEmail() {
		return ceoEmail;
	}

	public void setCeoEmail(String ceoEmail) {
		this.ceoEmail = ceoEmail;
	}

	public String getNatureOfOrg() {
		return natureOfOrg;
	}

	public void setNatureOfOrg(String natureOfOrg) {
		this.natureOfOrg = natureOfOrg;
	}

	public Integer getCorpOrgId() {
		return corpOrgId;
	}

	public void setCorpOrgId(Integer corpOrgId) {
		this.corpOrgId = corpOrgId;
	}

	public String getParentOrgId() {
		return parentOrgId;
	}

	public void setParentOrgId(String parentOrgId) {
		this.parentOrgId = parentOrgId;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactPersonMobile() {
		return contactPersonMobile;
	}

	public void setContactPersonMobile(String contactPersonMobile) {
		this.contactPersonMobile = contactPersonMobile;
	}

	public String getContactPersonEmail() {
		return contactPersonEmail;
	}

	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getCreatedById() {
		return createdById;
	}

	public void setCreatedById(Integer createdById) {
		this.createdById = createdById;
	}

	public Integer getModifiedById() {
		return modifiedById;
	}

	public void setModifiedById(Integer modifiedById) {
		this.modifiedById = modifiedById;
	}

	public Integer getSiteid() {
		return siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}
	
	
}
