package org.mysys.model.spectra;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="org_parent_drop_down_vw")
public class OrgParentDropDownVw  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="org_id", updatable = false, nullable = false)
	private long orgid;
	
	@Column(name="org_name")
	private String orgName;
	
	//bi-directional many-to-one association to Site
		@JsonIgnore
		@ManyToOne
		@NotFound(action = NotFoundAction.IGNORE)
		@JoinColumn(name="org_id", insertable=false, updatable=false)	
		private CorpDropDownVw corpdropdownvw;
		
		public long getOrgid() {
			return orgid;
		}

		public void setOrgid(long orgid) {
			this.orgid = orgid;
		}

		public String getOrgName() {
			return orgName;
		}

		public void setOrgName(String orgName) {
			this.orgName = orgName;
		}

		public CorpDropDownVw getCorpdropdownvw() {
			return corpdropdownvw;
		}

		public void setCorpdropdownvw(CorpDropDownVw corpdropdownvw) {
			this.corpdropdownvw = corpdropdownvw;
		}

}
