package org.mysys.model.spectra;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;




import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="corp_drop_down_vw")
public class CorpDropDownVw implements Serializable{
private static final long serialVersionUID = 1L;
	
	@Id
	//@Column(name="org_id")
	private Long org_id;
	
	@Column(name="org_name")
	private String orgName;
	
	//bi-directional many-to-one association to Citymaster
		@JsonIgnore
		@OneToMany(mappedBy="corpdropdownvw", fetch=FetchType.EAGER)
		private List<OrgParentDropDownVw> orgparentdropdownVw;

	

		public Long getOrg_id() {
			return org_id;
		}

		public void setOrg_id(Long org_id) {
			this.org_id = org_id;
		}

		public String getOrgName() {
			return orgName;
		}

		public void setOrgName(String orgName) {
			this.orgName = orgName;
		}

		public List<OrgParentDropDownVw> getOrgparentdropdownVw() {
			return orgparentdropdownVw;
		}

		public void setOrgparentdropdownVw(List<OrgParentDropDownVw> orgparentdropdownVw) {
			this.orgparentdropdownVw = orgparentdropdownVw;
		}
	
}
