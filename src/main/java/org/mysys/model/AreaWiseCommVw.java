package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="dashboard_kpi_area_wise_comm_vw")
@NamedQuery(name="AreaWiseCommVw.findAll",query="Select c from AreaWiseCommVw c")
public class AreaWiseCommVw implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private long id;
	
	 private String pinCode;
	 
	 private String area;
	
	 private Long today;
	 
	 private Long amId;
		
	 public Long getAmId() {
		return amId;
	}

	public void setAmId(Long amId) {
		this.amId = amId;
	}

	private Long lastWeek;
	 
	 private Long lastMonth;
		 
	 private Long lastYear;
	 
	 public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Long getToday() {
		return today;
	}

	public void setToday(Long today) {
		this.today = today;
	}

	public Long getLastWeek() {
		return lastWeek;
	}

	public void setLastWeek(Long lastWeek) {
		this.lastWeek = lastWeek;
	}

	public Long getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(Long lastMonth) {
		this.lastMonth = lastMonth;
	}

	public Long getLastYear() {
		return lastYear;
	}

	public void setLastYear(Long lastYear) {
		this.lastYear = lastYear;
	}	
	 public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}
		
}
