//package org.mysys.model;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="partner_to_notify_vw")
//@NamedQuery(name="PartnerToNotifyVw.findAll",query="Select c from PartnerToNotifyVw c")
//public class PartnerToNotifyVw implements Serializable {
//	private static final long serialVersionUID = 1L;
//	
//	@Id
//	private long id;
//	
//    @Column(name="sl_id")
//	private Long slId;
//	
//    @Column(name="pm_id")
//	private Long pmId;
//    
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public Long getSlId() {
//		return slId;
//	}
//
//	public void setSlId(Long slId) {
//		this.slId = slId;
//	}
//
//	public Long getPmId() {
//		return pmId;
//	}
//
//	public void setPmId(Long pmId) {
//		this.pmId = pmId;
//	}
//
//}
