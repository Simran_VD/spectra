package org.mysys.model;

import java.util.Comparator;

import org.springframework.stereotype.Component;

// kprajap
@Component
public class DropDownComparator implements Comparator<DropdownVO> {

	@Override
	public int compare(DropdownVO o1, DropdownVO o2) {
		if (o1 == null && o2 == null) {
			return 0;
		} else if (o1 == null || o2 == null) {
			return 1;
		} else {
			return o1.getDisplayValue().compareTo(o2.getDisplayValue());
		}
	}

}