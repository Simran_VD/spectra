package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="pin_board_list_vw")
@NamedQuery(name="PinBoardListView.findAll",query="select c from PinBoardListView c")

public class PinBoardListView implements Serializable{
	private static final long serialVersionUID = 1L;

	
	@Id
	private int il_id;
	private String type;
	private String title;
	private String description;
	private String log_date;
	private String status;
	private String st_colour;
	private long siteid;
	
	@Column(name="assigned_to")
	private Long assignedto;
	
	@Column(name="pin_board_status")
	private String pinboardstatus;
	
	@Column(name="task_status")
	private String taskstatus;
	
	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setAssignedto(Long assignedto) {
		this.assignedto = assignedto;
	}
	public int getIl_id() {
		return il_id;
	}
	public void setIl_id(int il_id) {
		this.il_id = il_id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLog_date() {
		return log_date;
	}
	public void setLog_date(String log_date) {
		this.log_date = log_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSt_colour() {
		return st_colour;
	}
	public void setSt_colour(String st_colour) {
		this.st_colour = st_colour;
	}
	public long getSiteid() {
		return siteid;
	}
	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
	public long getAssignedto() {
		return assignedto;
	}
	public void setAssignedto(long assignedto) {
		this.assignedto = assignedto;
	}
	
	public String getPinboardstatus() {
		return pinboardstatus;
	}
	public void setPinboardstatus(String pinboardstatus) {
		this.pinboardstatus = pinboardstatus;
	}
		
	public String getTaskstatus() {
		return taskstatus;
	}
	public void setTaskstatus(String taskstatus) {
		this.taskstatus = taskstatus;
	}
}

	