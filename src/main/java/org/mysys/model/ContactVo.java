package org.mysys.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the contact database table.
 * 
 */
@Entity
@Table(name="contact")
@NamedQuery(name="ContactVo.findAll", query="SELECT c FROM ContactVo c")
public class ContactVo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long contactid;

	private String addr1;

	private String addr2;

	private String addr3;

	private BigDecimal categoryid;

	private BigDecimal cityid;

	private long contactroleid;

	private BigDecimal contacttypeid;

	private String country;

	private String createdby;

	@Temporal(TemporalType.DATE)
	private Date createddt;

	private String displayname;
 
	private Float discount;
	
	private String email;

	@Temporal(TemporalType.DATE)
	private Date enddate;

	private String gstno;

	private String mobile;

	private String modifiedby;

	@Temporal(TemporalType.DATE)
	private Date modifieddt;

	private String name;

	private String phone;

	private String pincode;

	private long siteid;

	@Temporal(TemporalType.DATE)
	private Date startdate;

	private BigDecimal stateid;

	private String status;

	private String transporter;

	private String vendorcode;
	
	@Column(name="payment_term")
	private String paymentTerm;
	
    @Column(name="credit_limit_days")
    private BigDecimal creditLimitDays;
    
    @Column(name="is_inter_state")
    private BigDecimal isInterState;
    
    private BigDecimal distance;
    
	public ContactVo() {
	}

	public long getContactid() {
		return this.contactid;
	}

	public void setContactid(long contactid) {
		this.contactid = contactid;
	}

	public String getAddr1() {
		return this.addr1;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public String getAddr2() {
		return this.addr2;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public String getAddr3() {
		return this.addr3;
	}

	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}

	public BigDecimal getCategoryid() {
		return this.categoryid;
	}

	public void setCategoryid(BigDecimal categoryid) {
		this.categoryid = categoryid;
	}

	public BigDecimal getCityid() {
		return this.cityid;
	}

	public void setCityid(BigDecimal cityid) {
		this.cityid = cityid;
	}

	public long getContactroleid() {
		return this.contactroleid;
	}

	public void setContactroleid(long contactroleid) {
		this.contactroleid = contactroleid;
	}

	public BigDecimal getContacttypeid() {
		return this.contacttypeid;
	}

	public void setContacttypeid(BigDecimal contacttypeid) {
		this.contacttypeid = contacttypeid;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getCreateddt() {
		return this.createddt;
	}

	public void setCreateddt(Date createddt) {
		this.createddt = createddt;
	}

	public String getDisplayname() {
		return this.displayname;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getEnddate() {
		return this.enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public String getGstno() {
		return this.gstno;
	}

	public void setGstno(String gstno) {
		this.gstno = gstno;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getModifiedby() {
		return this.modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifieddt() {
		return this.modifieddt;
	}

	public void setModifieddt(Date modifieddt) {
		this.modifieddt = modifieddt;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public Date getStartdate() {
		return this.startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public BigDecimal getStateid() {
		return this.stateid;
	}

	public void setStateid(BigDecimal stateid) {
		this.stateid = stateid;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransporter() {
		return this.transporter;
	}

	public void setTransporter(String transporter) {
		this.transporter = transporter;
	}

	public String getVendorcode() {
		return this.vendorcode;
	}

	public void setVendorcode(String vendorcode) {
		this.vendorcode = vendorcode;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public BigDecimal getCreditLimitDays() {
		return creditLimitDays;
	}

	public void setCreditLimitDays(BigDecimal creditLimitDays) {
		this.creditLimitDays = creditLimitDays;
	}

	public BigDecimal getIsInterState() {
		return isInterState;
	}

	public void setIsInterState(BigDecimal isInterState) {
		this.isInterState = isInterState;
	}

	public BigDecimal getDistance() {
		return distance;
	}

	public void setDistance(BigDecimal distance) {
		this.distance = distance;
	}

}