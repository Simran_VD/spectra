package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;


/**
 * The persistent class for the emailtemplate database table.
 * 
 */
@Entity
@NamedQuery(name="Emailtemplate.findAll", query="SELECT e FROM Emailtemplate e")
public class Emailtemplate extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="emailtemplate_id_seq", name="emailtemplateIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="emailtemplateIdSeq")
	@Column(name="templateid", updatable = false, nullable = false)
	private long templateid;

	private String status;

	private String templatebody;

	private String templateclass;

	private String templatename;

	//bi-directional many-to-one association to Site
	@ManyToOne
	@JoinColumn(name="siteid")
	private Site site;

	public Emailtemplate() {
	}

	public long getTemplateid() {
		return this.templateid;
	}

	public void setTemplateid(long templateid) {
		this.templateid = templateid;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTemplatebody() {
		return this.templatebody;
	}

	public void setTemplatebody(String templatebody) {
		this.templatebody = templatebody;
	}

	public String getTemplateclass() {
		return this.templateclass;
	}

	public void setTemplateclass(String templateclass) {
		this.templateclass = templateclass;
	}

	public String getTemplatename() {
		return this.templatename;
	}

	public void setTemplatename(String templatename) {
		this.templatename = templatename;
	}

	public Site getSite() {
		return this.site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

}