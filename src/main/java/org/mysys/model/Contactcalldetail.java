package org.mysys.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the contactcalldetail database table.
 * 
 */
@Entity
@NamedQuery(name="Contactcalldetail.findAll", query="SELECT c FROM Contactcalldetail c")
public class Contactcalldetail extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="contact_call_dtl_id_seq", name="contactCallDtlIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="contactCallDtlIdSeq")
	@Column(name="callid", updatable = false, nullable = false)
	private long callid;

	@Temporal(TemporalType.DATE)
	private Date calldate;

	private String calldesc;

	@Temporal(TemporalType.DATE)
	private Date nextcalldate;

	private String purpose;

	//bi-directional many-to-one association to Contact
	@ManyToOne
	@JoinColumn(name="contactid")
	private Contact contact;

	private long siteid;

	public Contactcalldetail() {
	}

	public long getCallid() {
		return this.callid;
	}

	public void setCallid(long callid) {
		this.callid = callid;
	}

	public Date getCalldate() {
		return this.calldate;
	}

	public void setCalldate(Date calldate) {
		this.calldate = calldate;
	}

	public String getCalldesc() {
		return this.calldesc;
	}

	public void setCalldesc(String calldesc) {
		this.calldesc = calldesc;
	}


	public Date getNextcalldate() {
		return this.nextcalldate;
	}

	public void setNextcalldate(Date nextcalldate) {
		this.nextcalldate = nextcalldate;
	}

	public String getPurpose() {
		return this.purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public Contact getContact() {
		return this.contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

}