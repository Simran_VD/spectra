package org.mysys.model;

import java.util.Comparator;

import org.springframework.stereotype.Component;
@Component
public class ScreenComparator implements Comparator<Screen>{

	@Override
	public int compare(Screen o1, Screen o2) {
		if(o1.getSeq() ==  null && o2.getSeq()== null){
			return 0;
		}else if(o1.getSeq() ==  null || o2.getSeq()== null){
			return 1;
		}
		return o1.getSeq().compareTo(o2.getSeq());
	}

	
	
}