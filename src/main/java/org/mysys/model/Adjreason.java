package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the adjreason database table.
 * 
 */
@Entity
@NamedQuery(name="Adjreason.findAll", query="SELECT a FROM Adjreason a")
public class Adjreason extends Auditable<String> implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String adjreason;

	private long siteid;

	public Adjreason() {
	}

	public String getAdjreason() {
		return this.adjreason;
	}

	public void setAdjreason(String adjreason) {
		this.adjreason = adjreason;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

}