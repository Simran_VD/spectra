package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="companyinfo")
@NamedQuery(name="CompanySetup.findAll", query="SELECT m FROM CompanySetup m")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CompanySetup  extends  Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long siteid;

	@Column(name="name")
	private String cmpName;

	@Column(name="display_name")
	private String displayName;

	@Column(name="deals_in")	
	private String dealsIn;

	@Column(name="addr1")
	private String address1;

	@Column(name="addr2")
	private String address2;

	@Column(name="addr3")
	private String address3;

	private Long cityid;

	@Column(name="city_name")
	private String cityName;

	private Long stateid;

	@Column(name="state_name")
	private String stateName;

	private String fullAddress;

	@Column(name="zip_code")
	private String zipCode;

	@Column(name="factory_addr1")
	private String factoryAddress1;

	@Column(name="factory_addr2")
	private String factoryAddress2;

	@Column(name="factory_addr3")
	private String factoryAddress3;

	@Column(name="factory_cityid")
	private Long factoryCityid;

	@Column(name="factory_cityname")
	private String factoryCityname;

	@Column(name="factory_stateid")
	private Long factoryStateid;

	@Column(name="factory_statename")
	private String factoryStatename;

	@Column(name="full_factory_address")
	private String fullFactoryAddress;

	@Column(name="primary_phone")
	private String primaryPhone;

	@Column(name="secondary_phone")
	private String secondoryPhone;

	private String mobile;

	private String email;

	@Column(name="website_url")
	private String websiteUrl;

	private String gstin;

	private String cin;

	/*	@Lob
	@Column(name = "company_logo", length = Integer.MAX_VALUE, nullable = true)	
	private byte[] companyLogo;*/

	private String active;
	
	@Column(name="account_name")
	private String accountName;
	
	@Column(name="bank_name")
	private String  bankName;
	
    private String branch;
    
    @Column(name="account_type")
    private String accountType;

    @Column(name="account_no")
    private String accountNo;
	
    @Column(name="ifsc_code")
    private String ifscCode;

	/* getter and setter */
	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public String getCmpName() {
		return cmpName;
	}

	public void setCmpName(String cmpName) {
		this.cmpName = cmpName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDealsIn() {
		return dealsIn;
	}

	public void setDealsIn(String dealsIn) {
		this.dealsIn = dealsIn;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public Long getCityid() {
		return cityid;
	}

	public void setCityid(Long cityid) {
		this.cityid = cityid;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Long getStateid() {
		return stateid;
	}

	public void setStateid(Long stateid) {
		this.stateid = stateid;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getFactoryAddress1() {
		return factoryAddress1;
	}

	public void setFactoryAddress1(String factoryAddress1) {
		this.factoryAddress1 = factoryAddress1;
	}

	public String getFactoryAddress2() {
		return factoryAddress2;
	}

	public void setFactoryAddress2(String factoryAddress2) {
		this.factoryAddress2 = factoryAddress2;
	}

	public String getFactoryAddress3() {
		return factoryAddress3;
	}

	public void setFactoryAddress3(String factoryAddress3) {
		this.factoryAddress3 = factoryAddress3;
	}

	public Long getFactoryCityid() {
		return factoryCityid;
	}

	public void setFactoryCityid(Long factoryCityid) {
		this.factoryCityid = factoryCityid;
	}

	public String getFactoryCityname() {
		return factoryCityname;
	}

	public void setFactoryCityname(String factoryCityname) {
		this.factoryCityname = factoryCityname;
	}

	public Long getFactoryStateid() {
		return factoryStateid;
	}

	public void setFactoryStateid(Long factoryStateid) {
		this.factoryStateid = factoryStateid;
	}

	public String getFactoryStatename() {
		return factoryStatename;
	}

	public void setFactoryStatename(String factoryStatename) {
		this.factoryStatename = factoryStatename;
	}

	public String getFullFactoryAddress() {
		return fullFactoryAddress;
	}

	public void setFullFactoryAddress(String fullFactoryAddress) {
		this.fullFactoryAddress = fullFactoryAddress;
	}

	public String getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getSecondoryPhone() {
		return secondoryPhone;
	}

	public void setSecondoryPhone(String secondoryPhone) {
		this.secondoryPhone = secondoryPhone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getGstin() {
		return gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	
}
