//package org.mysys.model.partner;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//import org.hibernate.validator.constraints.Length;
//
//@Entity
//@Table(name="pincode_mst_vw")
//@NamedQuery(name="PincodeMstVw.findAll",query="Select c from PincodeMstVw c")
//public class PincodeMstVw  implements Serializable {
//	private static final long serialVersionUID =1l;
//	
//	@Id
//    @Column(name="pc_id")
//	private long pcId;
//	
//	private String pincode ;
//	
//	@Length(max=1)
//	private String isActive;
//	
//	private String remarks;
//	
//	private long siteid;
//	
//	private long pin;
//	
//	private long cityId;
//
//	public long getPcId() {
//		return pcId;
//	}
//
//	public void setPcId(long pcId) {
//		this.pcId = pcId;
//	}
//
//	public String getPincode() {
//		return pincode;
//	}
//
//	public void setPincode(String pincode) {
//		this.pincode = pincode;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//	public long getPin() {
//		return pin;
//	}
//
//	public void setPin(long pin) {
//		this.pin = pin;
//	}
//
//	public long getCityId() {
//		return cityId;
//	}
//
//	public void setCityId(long cityId) {
//		this.cityId = cityId;
//	}
//	
//	
//}
