//package org.mysys.model.partner;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.mysys.model.Auditable;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//@Entity
//@Table(name="partner_service_map")
//@NamedQuery(name="PartnerServiceMap.findAll",query="Select c from PartnerMst c")
//public class PartnerServiceMap extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID =1l;
//
//	@Id
//	@SequenceGenerator(allocationSize=1, sequenceName="partner_service_map_seq", name="partnerMapSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="partnerMapSeq")
//	@Column(name="psm_id",nullable = false,updatable = false)
//	private long pamId;
//
//	@Column(name="sm_id")
//	private Long smId;
//
//	@Column(name="pm_id")
//	private Long pmId;
//
//	private String isActive;
//
//	private String remarks;
//
//	@JsonIgnore
//	@ManyToOne
//	@JoinColumn(name="pm_id",insertable=false,updatable=false)
//	private PartnerMst partnerMst;
//	private long siteid;
//
//	public long getPamId() {
//		return pamId;
//	}
//
//	public void setPamId(long pamId) {
//		this.pamId = pamId;
//	}
//
//	public Long getSmId() {
//		return smId;
//	}
//
//	public void setSmId(Long smId) {
//		this.smId = smId;
//	}
//
//	public Long getPmId() {
//		return pmId;
//	}
//
//	public void setPmId(Long pmId) {
//		this.pmId = pmId;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//	public PartnerMst getPartnerMst() {
//		return partnerMst;
//	}
//
//	public void setPartnerMst(PartnerMst partnerMst) {
//		this.partnerMst = partnerMst;
//	}
//	
//}
