//package org.mysys.model.partner;
//
//import java.io.Serializable;
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//
//import org.mysys.constant.APPConstant;
//import org.mysys.model.Auditable;
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//
//@Entity
//@Table(name="partner_ledger_vw")
//@NamedQuery(name="PartnerLedgerVw.findAll",query="Select c from PartnerLedgerVw c")
//public class PartnerLedgerVw implements Serializable {
//	private static final long serialVersionUID =1l;
//	
//	@Id
//	@Column(name="pl_id")
//	private long plId;
//	
//	@Column(name="ttm_id")
//	private long ttmId;
//	
//	@Column(name="pmm_id")
//	private long pmmId;
//	
//	@Column(name="sl_id")
//	private Long slId;
//	
//	@Column(name="pm_id")
//	private long pmId;
//	
//	@Column(name="tran_desc")
//	private String transDesc;
//	
//	@Column(name="tran_type")
//	private String transType;
//	
//	@Column(name="payment_mode")
//	private String paymentMode;
//	
//	@Column(name="display_name")
//	private String displayName;
//	
//	@Column(name="tran_no")
//	private String tranNo;
//	
//	@Column(name="card_no")
//	private String cardNo;
//	
//	@Column(name="tran_amount")
//	private long tranAmount;
//	
//	@Column(name="reference_no")
//	private String referenceNo;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="pl_date")
//	private Date plDate;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="tran_date")
//	private Date tranDate;
//	
//	private String remarks;
//
//	private long siteId;
//	
//	@Column(name="approved_by")
//	private String approvedBy;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="approved_time")
//	private Date approvedTime;
//	
//	public long getPlId() {
//		return plId;
//	}
//	public void setPlId(long plId) {
//		this.plId = plId;
//	}
//	public long getTtmId() {
//		return ttmId;
//	}
//	public void setTtmId(long ttmId) {
//		this.ttmId = ttmId;
//	}
//	public long getPmmId() {
//		return pmmId;
//	}
//	public void setPmmId(long pmmId) {
//		this.pmmId = pmmId;
//	}
//	public Long getSlId() {
//		return slId;
//	}
//	public void setSlId(Long slId) {
//		this.slId = slId;
//	}
//	public long getPmId() {
//		return pmId;
//	}
//	public void setPmId(long pmId) {
//		this.pmId = pmId;
//	}
//	public String getTranNo() {
//		return tranNo;
//	}
//	public void setTranNo(String tranNo) {
//		this.tranNo = tranNo;
//	}
//	
//	public long getTranAmount() {
//		return tranAmount;
//	}
//	public void setTranAmount(long tranAmount) {
//		this.tranAmount = tranAmount;
//	}
//
//	public Date getPlDate() {
//		return plDate;
//	}
//	public void setPlDate(Date plDate) {
//		this.plDate = plDate;
//	}
//	public Date getTranDate() {
//		return tranDate;
//	}
//	public void setTranDate(Date tranDate) {
//		this.tranDate = tranDate;
//	}
//	public String getRemarks() {
//		return remarks;
//	}
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//	public long getSiteId() {
//		return siteId;
//	}
//	public void setSiteId(long siteId) {
//		this.siteId = siteId;
//	}
//	public String getApprovedBy() {
//		return approvedBy;
//	}
//	public void setApprovedBy(String approvedBy) {
//		this.approvedBy = approvedBy;
//	}
//	public Date getApprovedTime() {
//		return approvedTime;
//	}
//	public void setApprovedTime(Date approvedTime) {
//		this.approvedTime = approvedTime;
//	}
//	public String getCardNo() {
//		return cardNo;
//	}
//	public void setCardNo(String cardNo) {
//		this.cardNo = cardNo;
//	}
//	public String getReferenceNo() {
//		return referenceNo;
//	}
//	public void setReferenceNo(String referenceNo) {
//		this.referenceNo = referenceNo;
//	}
//	public String getTransDesc() {
//		return transDesc;
//	}
//	public void setTransDesc(String transDesc) {
//		this.transDesc = transDesc;
//	}
//	public String getTransType() {
//		return transType;
//	}
//	public void setTransType(String transType) {
//		this.transType = transType;
//	}
//	public String getPaymentMode() {
//		return paymentMode;
//	}
//	public void setPaymentMode(String paymentMode) {
//		this.paymentMode = paymentMode;
//	}
//	public String getDisplayName() {
//		return displayName;
//	}
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//	
//	
//}
