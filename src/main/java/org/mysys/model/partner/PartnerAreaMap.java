//package org.mysys.model.partner;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.mysys.model.Auditable;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//@Entity
//@Table(name="partner_area_map")
//@NamedQuery(name="PartnerAreaMap.findAll",query="Select c from PartnerAreaMap c")
//public class PartnerAreaMap extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID =1l;
//
//	@Id
//	@SequenceGenerator(allocationSize=1, sequenceName="partner_area_map_seq", name="partnerAreaMapSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="partnerAreaMapSeq")
//	@Column(name="pam_id")
//	private long pamId;
//
//	@Column(name="am_id")
//	private Long amId;
//
//	@Column(name="pm_id")
//	private Long pmId;
//
//	private String isActive;
//
//	private String remarks;
//	
//	@Column(name="pc_id")
//	private Long pcId;
//
//	private long siteid;
//	
//	@JsonIgnore
//	@ManyToOne
//	@JoinColumn(name="pm_id",insertable=false,updatable=false)
//	private PartnerMst partnerMst;
//
//	public long getPamId() {
//		return pamId;
//	}
//
//	public void setPamId(long pamId) {
//		this.pamId = pamId;
//	}
//
//	public Long getAmId() {
//		return amId;
//	}
//
//	public void setAmId(Long amId) {
//		this.amId = amId;
//	}
//
//	public Long getPmId() {
//		return pmId;
//	}
//
//	public void setPmId(Long pmId) {
//		this.pmId = pmId;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//	public PartnerMst getPartnerMst() {
//		return partnerMst;
//	}
//
//	public void setPartnerMst(PartnerMst partnerMst) {
//		this.partnerMst = partnerMst;
//	}
//
//	public Long getPcId() {
//		return pcId;
//	}
//
//	public void setPcId(Long pcId) {
//		this.pcId = pcId;
//	}
//	
//}
