//package org.mysys.model.partner;
//
//import java.io.Serializable;
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//
//import org.mysys.constant.APPConstant;
//import org.mysys.model.Auditable;
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//
//
//@Entity
//@Table(name="serv_lead_notify")
//@NamedQuery(name="ServLeadNotify.findAll",query="Select c from ServLeadNotify c")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//public class ServLeadNotify extends Auditable<String> implements Serializable{
//    private static final long serialVersionUID = 1L;
//    
//    @Id
//    @SequenceGenerator(allocationSize=1, sequenceName="serv_lead_notify_seq", name="servLeadSeq")
//  	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="servLeadSeq")
//    @Column(name="sln_id",nullable = false,updatable = false)
//    private long slnId;
//    
//    @Column(name="sl_id")
//    private long slid;
//    
//    @Column(name="pm_id")
//    private long pmId;
//        
//    @Column(name="am_id")
//    private long amId;
//    
//    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="notify_time")
//	private Date notifyTime;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="lead_accepted_time")
//	private Date leadAccTime;
//	
//	private String remarks;
// 	private long siteid;
//	public long getSlnId() {
//		return slnId;
//	}
//	public void setSlnId(long slnId) {
//		this.slnId = slnId;
//	}
//	public long getSlid() {
//		return slid;
//	}
//	public void setSlid(long slid) {
//		this.slid = slid;
//	}
//	public long getPmId() {
//		return pmId;
//	}
//	public void setPmId(long pmId) {
//		this.pmId = pmId;
//	}
//	public long getAmId() {
//		return amId;
//	}
//	public void setAmId(long amId) {
//		this.amId = amId;
//	}
//	public Date getNotifyTime() {
//		return notifyTime;
//	}
//	public void setNotifyTime(Date notifyTime) {
//		this.notifyTime = notifyTime;
//	}
//	public Date getLeadAccTime() {
//		return leadAccTime;
//	}
//	public void setLeadAccTime(Date leadAccTime) {
//		this.leadAccTime = leadAccTime;
//	}
//	public String getRemarks() {
//		return remarks;
//	}
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//	public long getSiteid() {
//		return siteid;
//	}
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//}
