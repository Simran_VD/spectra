//package org.mysys.model.partner;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//import org.hibernate.validator.constraints.Length;
//
//@Entity
//@Table(name="area_mst_vw")
//@NamedQuery(name="AreaVw.findAll",query="Select c from AreaVw c")
//public class AreaVw implements Serializable {
//	private static final long serialVersionUID =1l;
//	
//	@Id
//	@Column(name="am_id")
//	private long amId;
//	
//	private Long cityid;
//	
//	@Column(name="area_name")
//	private String areaName;
//	
//	private String cityName;
//	
//	private Long stateid;
//	
//	private String pincode;
//	
//	@Length(max=1)
//	private String isActive;
//	
//	private String remarks;
//	
//	private long siteid;
//		
//	public long getAmId() {
//		return amId;
//	}
//
//	public void setAmId(long amId) {
//		this.amId = amId;
//	}
//
//	public Long getCityid() {
//		return cityid;
//	}
//
//	public void setCityid(Long cityid) {
//		this.cityid = cityid;
//	}
//
//	public String getAreaName() {
//		return areaName;
//	}
//
//	public void setAreaName(String areaName) {
//		this.areaName = areaName;
//	}
//
//	public String getCityName() {
//		return cityName;
//	}
//
//	public void setCityName(String cityName) {
//		this.cityName = cityName;
//	}
//
//	public Long getStateid() {
//		return stateid;
//	}
//
//	public void setStateid(Long stateid) {
//		this.stateid = stateid;
//	}
//
//	public String getPincode() {
//		return pincode;
//	}
//
//	public void setPincode(String pincode) {
//		this.pincode = pincode;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//}
