//package org.mysys.model.partner;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//import org.hibernate.validator.constraints.Length;
//
//@Entity
//@Table(name="lead_status_mst_vw")
//@NamedQuery(name="LeadSourceMstVw.findAll",query="Select c from LeadSourceMstVw c")
//public class LeadSourceMstVw implements Serializable {
//	private static final long serialVersionUID =1l;
//	
//	@Id
//	@Column(name="lsm_id")
//	private long lsmId;
//	
//	private String status;
//	
//	private String isActive;
//	
//	private String remarks;
//	
//	private long siteid;
//	
//	private long userid;
//
//	public long getLsmId() {
//		return lsmId;
//	}
//
//	public void setLsmId(long lsmId) {
//		this.lsmId = lsmId;
//	}
//
//	public String getStatus() {
//		return status;
//	}
//
//	public void setStatus(String status) {
//		this.status = status;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//	public long getUserid() {
//		return userid;
//	}
//
//	public void setUserid(long userid) {
//		this.userid = userid;
//	}
//	
//
//}
