//package org.mysys.model.partner;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.hibernate.validator.constraints.Length;
//import org.mysys.model.Auditable;
//
//@Entity
//@Table(name="pincode_mst")
//@NamedQuery(name="PincodeMst.findAll",query="Select c from PincodeMst c")
//public class PincodeMst extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID =1l;
//	
//	@Id
//	@SequenceGenerator(allocationSize=1, sequenceName="pincode_mst_seq", name="pincodeSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="pincodeSeq")
//	@Column(name="pc_id ")
//	private long pcId;
//	
//	private String pincode ;
//	
//	@Length(max=1)
//	private String isActive;
//	
//	private String remarks;
//	
//	private long siteid;
//
//	public long getPcId() {
//		return pcId;
//	}
//
//	public void setPcId(long pcId) {
//		this.pcId = pcId;
//	}
//
//	public String getPincode() {
//		return pincode;
//	}
//
//	public void setPincode(String pincode) {
//		this.pincode = pincode;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//}
