//package org.mysys.model.partner;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//
//import org.mysys.constant.APPConstant;
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//
//@Entity
//@Table(name="partner_lead_map")
//@NamedQuery(name="PartnerLeadMap.findAll",query="Select c from PartnerLeadMap c")
//public class PartnerLeadMap implements Serializable {
//	private static final long serialVersionUID =1l;
//
//	@Id
//	@SequenceGenerator(allocationSize=1, sequenceName="partner_lead_map_seq", name="partnerLeadMapSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="partnerLeadMapSeq")
//	@Column(name="plm_id")
//	private long plmId;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="map_time")
//	private Date mapTime;
//	
//	@Column(name="sl_id")
//	private long slId;
//	
//	@Column(name="pm_id")
//	private long pmId;
//	
//	@Column(name="pt_id")
//	private Long ptId;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="team_map_time")
//	private Date teamMapTime;
//	
//	@Column(name="comm_ded")
//	private BigDecimal commDed;
//
//	@Column(name="tran_id")
//	private String tranId;
//	
//	@Column(name="tran_ref_no")
//	private String tranRefId;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="tran_dt")
//	private Date tranDt;
//	
//	private String reversal;
//	
//	private String remarks;
//	
//	private long siteid;
//
//	public long getPlmId() {
//		return plmId;
//	}
//
//	public void setPlmId(long plmId) {
//		this.plmId = plmId;
//	}
//
//	public Date getMapTime() {
//		return mapTime;
//	}
//
//	public void setMapTime(Date mapTime) {
//		this.mapTime = mapTime;
//	}
//
//	public long getSlId() {
//		return slId;
//	}
//
//	public void setSlId(long slId) {
//		this.slId = slId;
//	}
//
//	public long getPmId() {
//		return pmId;
//	}
//
//	public void setPmId(long pmId) {
//		this.pmId = pmId;
//	}
//
//	public Long getPtId() {
//		return ptId;
//	}
//
//	public void setPtId(Long ptId) {
//		this.ptId = ptId;
//	}
//
//	public Date getTeamMapTime() {
//		return teamMapTime;
//	}
//
//	public void setTeamMapTime(Date teamMapTime) {
//		this.teamMapTime = teamMapTime;
//	}
//
//	public BigDecimal getCommDed() {
//		return commDed;
//	}
//
//	public void setCommDed(BigDecimal commDed) {
//		this.commDed = commDed;
//	}
//
//	public String getTranId() {
//		return tranId;
//	}
//
//	public void setTranId(String tranId) {
//		this.tranId = tranId;
//	}
//
//	public String getTranRefId() {
//		return tranRefId;
//	}
//
//	public void setTranRefId(String tranRefId) {
//		this.tranRefId = tranRefId;
//	}
//
//	public Date getTranDt() {
//		return tranDt;
//	}
//
//	public void setTranDt(Date tranDt) {
//		this.tranDt = tranDt;
//	}
//
//	public String getReversal() {
//		return reversal;
//	}
//
//	public void setReversal(String reversal) {
//		this.reversal = reversal;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}	
//}
