//package org.mysys.model.partner;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.mysys.model.Auditable;
//
//@Entity
//@Table(name="service_mst")
//@NamedQuery(name="ServiceMst.findAll",query="Select c from ServiceMst c")
//public class ServiceMst extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID =1l;
//	
//	@Id
//	@SequenceGenerator(allocationSize=1, sequenceName="service_mst_seq", name="servicemstSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="servicemstSeq")
//	@Column(name="sm_id")
//	private long smId;
//	
//	@Column(name="service_name")
//	private String serviceName;
//	
//    private String isActive;
//	
//	private String remarks;
//	
//	private long siteid;
//
//	public long getSmId() {
//		return smId;
//	}
//
//	public void setSmId(long smId) {
//		this.smId = smId;
//	}
//
//	public String getServiceName() {
//		return serviceName;
//	}
//
//	public void setServiceName(String serviceName) {
//		this.serviceName = serviceName;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//}
