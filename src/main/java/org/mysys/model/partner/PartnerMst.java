//package org.mysys.model.partner;
//
//import java.io.Serializable;
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Lob;
//import javax.persistence.NamedQuery;
//import javax.persistence.OneToMany;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.Fetch;
//import org.hibernate.annotations.FetchMode;
//import org.hibernate.annotations.Type;
//import org.mysys.model.Auditable;
//
//@Entity
//@Table(name="partner_mst")
//@NamedQuery(name="PartnerMst.findAll",query="Select c from PartnerMst c")
////@JsonIgnoreProperties({""})
//public class PartnerMst extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID =1l;
//
//    @Id
//    @SequenceGenerator(allocationSize=1, sequenceName="partner_mst_seq", name="partnerSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="partnerSeq")
//    @Column(name="pm_id",nullable = false,updatable = false)
//    private long pmId;
//    private String name;
//    
//    @Column(name="display_name")
//    private String displayName;
//    
//    private String company;
//    
//    private String address;
//    
//    private String landmark;
//    
//    private String city;
//    private String state;
//    
//    private String pinCode;
//
//    private String phone;
//
//    private String mobile;
//    
//    private String email;
//    
//    @Column(name="bank_acct_no")
//    private String bankAccountNo;
//    
//    @Column(name="ifsc_code")
//    private String ifscCode;
//    
//    @Column(name="bank_name")
//    private String bankName;
//    
//    private String branch;
//    
//    private String gstin;
//    
//    private String pan;
//    
//    @Column(name="aadhaar_no")
//    private String adharNo;
//    
//    @Lob
//    @Column(name="pan_image",nullable =true)
//	@Type(type="org.hibernate.type.BinaryType")
//    private byte[] panImage;
//    
//    @Lob
//    @Column(name="aadhaar_image",nullable =true)
//	@Type(type="org.hibernate.type.BinaryType")
//    private byte[] adharImage;
//    
//    @Lob
//    @Column(name="gstin_image",nullable =true)
//	@Type(type="org.hibernate.type.BinaryType")
//    private byte[] gstinImage;
//
//    @Column(name="contact_person")
//    private String contactPerson;
//    
//    @Column(name="contact_person_mobile")
//    private String contactPersonMob;
//    
//    @Column(name="contact_person_email")
//    private String contactPersonEmail;
//    
//    private String isActive;
//	
//   	private String remarks;
//   	
//   	private long siteid;
//   	
//   	@OneToMany(mappedBy = "partnerMst",fetch = FetchType.EAGER)
//   	private List<PartnerAreaMap> partnerAreaMap;
//   	
//	@OneToMany(mappedBy = "partnerMst",cascade = CascadeType.ALL,fetch=FetchType.EAGER,orphanRemoval=true)
//	@Fetch(value = FetchMode.SELECT)
//   	private List<PartnerServiceMap> partnerServiceMap;
//
//	public long getPmId() {
//		return pmId;
//	}
//
//	public void setPmId(long pmId) {
//		this.pmId = pmId;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	public String getCompany() {
//		return company;
//	}
//
//	public void setCompany(String company) {
//		this.company = company;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public String getLandmark() {
//		return landmark;
//	}
//
//	public void setLandmark(String landmark) {
//		this.landmark = landmark;
//	}
//
//	public String getCity() {
//		return city;
//	}
//
//	public void setCity(String city) {
//		this.city = city;
//	}
//
//	public String getState() {
//		return state;
//	}
//
//	public void setState(String state) {
//		this.state = state;
//	}
//
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	public String getMobile() {
//		return mobile;
//	}
//
//	public void setMobile(String mobile) {
//		this.mobile = mobile;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getBankAccountNo() {
//		return bankAccountNo;
//	}
//
//	public void setBankAccountNo(String bankAccountNo) {
//		this.bankAccountNo = bankAccountNo;
//	}
//
//	public String getIfscCode() {
//		return ifscCode;
//	}
//
//	public void setIfscCode(String ifscCode) {
//		this.ifscCode = ifscCode;
//	}
//
//	public String getBankName() {
//		return bankName;
//	}
//
//	public void setBankName(String bankName) {
//		this.bankName = bankName;
//	}
//
//	public String getBranch() {
//		return branch;
//	}
//
//	public void setBranch(String branch) {
//		this.branch = branch;
//	}
//
//	public String getGstin() {
//		return gstin;
//	}
//
//	public void setGstin(String gstin) {
//		this.gstin = gstin;
//	}
//
//	public String getPan() {
//		return pan;
//	}
//
//	public void setPan(String pan) {
//		this.pan = pan;
//	}
//
//	public String getAdharNo() {
//		return adharNo;
//	}
//
//	public void setAdharNo(String adharNo) {
//		this.adharNo = adharNo;
//	}
//
//	public byte[] getPanImage() {
//		return panImage;
//	}
//
//	public void setPanImage(byte[] panImage) {
//		this.panImage = panImage;
//	}
//
//	public byte[] getAdharImage() {
//		return adharImage;
//	}
//
//	public void setAdharImage(byte[] adharImage) {
//		this.adharImage = adharImage;
//	}
//
//	public byte[] getGstinImage() {
//		return gstinImage;
//	}
//
//	public void setGstinImage(byte[] gstinImage) {
//		this.gstinImage = gstinImage;
//	}
//
//	public String getContactPerson() {
//		return contactPerson;
//	}
//
//	public void setContactPerson(String contactPerson) {
//		this.contactPerson = contactPerson;
//	}
//
//	public String getContactPersonMob() {
//		return contactPersonMob;
//	}
//
//	public void setContactPersonMob(String contactPersonMob) {
//		this.contactPersonMob = contactPersonMob;
//	}
//
//	public String getContactPersonEmail() {
//		return contactPersonEmail;
//	}
//
//	public void setContactPersonEmail(String contactPersonEmail) {
//		this.contactPersonEmail = contactPersonEmail;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//	public List<PartnerAreaMap> getPartnerAreaMap() {
//		return partnerAreaMap;
//	}
//
//	public void setPartnerAreaMap(List<PartnerAreaMap> partnerAreaMap) {
//		this.partnerAreaMap = partnerAreaMap;
//	}
//
//	public List<PartnerServiceMap> getPartnerServiceMap() {
//		return partnerServiceMap;
//	}
//
//	public void setPartnerServiceMap(List<PartnerServiceMap> partnerServiceMap) {
//		this.partnerServiceMap = partnerServiceMap;
//	}
//
//	public String getPinCode() {
//		return pinCode;
//	}
//
//	public void setPinCode(String pinCode) {
//		this.pinCode = pinCode;
//	}
//	
//}
