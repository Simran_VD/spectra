//package org.mysys.model.partner;
//
//import java.io.Serializable;
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Lob;
//import javax.persistence.NamedQuery;
//import javax.persistence.OneToMany;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.Fetch;
//import org.hibernate.annotations.FetchMode;
//import org.hibernate.annotations.Type;
//import org.mysys.model.Auditable;
//
//@Entity
//@Table(name="partner_team")
//@NamedQuery(name="PartnerTeam.findAll",query="Select c from PartnerTeam c")
////@JsonIgnoreProperties({""})
//public class PartnerTeam extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID =1l;
//
//    @Id
//    @SequenceGenerator(allocationSize=1, sequenceName="partner_team_seq", name="partnerTeamSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="partnerTeamSeq")
//    @Column(name="pt_id",nullable = false, updatable = false)
//    private long ptId;
//    
//    @Column(name="pm_id")
//    private long pmId;
//    private String name;
//    
//    @Column(name="display_name")
//    private String displayName;
//    
//    private String company;
//    
//    private String address;
//    
//    private String landmark;
//    
//    private String city;
//    private String state;
//    
//    private String phone;
//
//    private String mobile;
//    
//    private String email;
//  
//    
//    @Column(name="aadhaar_no")
//    private String adharNo;
//    
//    @Lob
//    @Column(name="partner_team_photo",nullable =true)
//	@Type(type="org.hibernate.type.BinaryType")
//    private byte[] partnerTeamPhoto;
//    
//    @Lob
//    @Column(name="aadhaar_image",nullable =true)
//	@Type(type="org.hibernate.type.BinaryType")
//    private byte[] adharImage;
//    
//    private String isActive;
//	
//   	private String remarks;
//   	
//   	private long siteid;
//
//	public long getPtId() {
//		return ptId;
//	}
//
//	public void setPtId(long ptId) {
//		this.ptId = ptId;
//	}
//
//	public long getPmId() {
//		return pmId;
//	}
//
//	public void setPmId(long pmId) {
//		this.pmId = pmId;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	public String getCompany() {
//		return company;
//	}
//
//	public void setCompany(String company) {
//		this.company = company;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public String getLandmark() {
//		return landmark;
//	}
//
//	public void setLandmark(String landmark) {
//		this.landmark = landmark;
//	}
//
//	public String getCity() {
//		return city;
//	}
//
//	public void setCity(String city) {
//		this.city = city;
//	}
//
//	public String getState() {
//		return state;
//	}
//
//	public void setState(String state) {
//		this.state = state;
//	}
//
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	public String getMobile() {
//		return mobile;
//	}
//
//	public void setMobile(String mobile) {
//		this.mobile = mobile;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getAdharNo() {
//		return adharNo;
//	}
//
//	public void setAdharNo(String adharNo) {
//		this.adharNo = adharNo;
//	}
//
//	public byte[] getPartnerTeamPhoto() {
//		return partnerTeamPhoto;
//	}
//
//	public void setPartnerTeamPhoto(byte[] partnerTeamPhoto) {
//		this.partnerTeamPhoto = partnerTeamPhoto;
//	}
//
//	public byte[] getAdharImage() {
//		return adharImage;
//	}
//
//	public void setAdharImage(byte[] adharImage) {
//		this.adharImage = adharImage;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//}
