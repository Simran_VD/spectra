//package org.mysys.model.partner;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="partner_ins_ledger")
//public class PartnerAcceptanceLadger {
//	
//	@Id
//	@Column(name="pv_success")
//	private int success;
//	
//	@Column(name="pv_clos_bal")
//	private Long pvCloseBal;
//	
//	@Column(name="pv_reject_reason")
//	private String rejectReason;
//	
//	@Column(name="pv_threshold")
//	private Long pvThreshold;
//	
//	@Column(name="pv_commission")
//	private Long pvCommission;
//
//	public int getSuccess() {
//		return success;
//	}
//
//	public void setSuccess(int success) {
//		this.success = success;
//	}
//
//	public Long getPvCloseBal() {
//		return pvCloseBal;
//	}
//
//	public void setPvCloseBal(Long pvCloseBal) {
//		this.pvCloseBal = pvCloseBal;
//	}
//
//	public String getRejectReason() {
//		return rejectReason;
//	}
//
//	public void setRejectReason(String rejectReason) {
//		this.rejectReason = rejectReason;
//	}
//
//	public Long getPvThreshold() {
//		return pvThreshold;
//	}
//
//	public void setPvThreshold(Long pvThreshold) {
//		this.pvThreshold = pvThreshold;
//	}
//
//	public Long getPvCommission() {
//		return pvCommission;
//	}
//
//	public void setPvCommission(Long pvCommission) {
//		this.pvCommission = pvCommission;
//	}
//
//
//}
