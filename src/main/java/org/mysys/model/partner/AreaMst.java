//package org.mysys.model.partner;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.hibernate.validator.constraints.Length;
//import org.mysys.model.Auditable;
//
//@Entity
//@Table(name="area_mst")
//@NamedQuery(name="AreaMst.findAll",query="Select c from AreaMst c")
//public class AreaMst extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID =1l;
//	
//	@Id
//	@SequenceGenerator(allocationSize=1, sequenceName="area_mst_seq", name="areSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="areSeq")
//	@Column(name="am_id")
//	private long amId;
//	
//	private Long cityid;
//	
//	@Column(name="area_name")
//	private String areaName;
//		
//	private String pincode;
//	
//	@Length(max=1)
//	private String isActive;
//	
//	private String remarks;
//	
//	private long siteid;
//	
//	@Column(name="pc_id")
//	private Long pcId;
//		
//	public long getAmId() {
//		return amId;
//	}
//
//	public void setAmId(long amId) {
//		this.amId = amId;
//	}
//
//	public Long getCityid() {
//		return cityid;
//	}
//
//	public void setCityid(Long cityid) {
//		this.cityid = cityid;
//	}
//
//	public String getAreaName() {
//		return areaName;
//	}
//
//	public void setAreaName(String areaName) {
//		this.areaName = areaName;
//	}
//
//	public String getPincode() {
//		return pincode;
//	}
//
//	public void setPincode(String pincode) {
//		this.pincode = pincode;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//	public Long getPcId() {
//		return pcId;
//	}
//
//	public void setPcId(Long pcId) {
//		this.pcId = pcId;
//	}
//
//}
