package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "citymaster_vw")
public class CityMasterView extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cityid")
	private int cityId;
	
	@Column(name = "cityname")
	private String cityName;
	
	@Column(name = "stateid")
	private int stateId;
	
	@Column(name = "siteid")
	private int siteId;
	
	@Column(name="site")
	private String site;
	
	@Column(name = "stateMaster")
	private String statemaster;
	
	@Column(name = "isActive")
	private String isactive;
	
	
	public CityMasterView()
	{
		
	}

	public int getCityId() {
		return cityId;
	}


	public void setCityId(int cityId) {
		this.cityId = cityId;
	}


	public String getCityName() {
		return cityName;
	}


	public void setCityName(String cityName) {
		this.cityName = cityName;
	}


	public int getStateId() {
		return stateId;
	}


	public void setStateId(int stateId) {
		this.stateId = stateId;
	}


	public int getSiteId() {
		return siteId;
	}


	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}


	public String getSite() {
		return site;
	}


	public void setSite(String site) {
		this.site = site;
	}


	public String getStatemaster() {
		return statemaster;
	}


	public void setStatemaster(String statemaster) {
		this.statemaster = statemaster;
	}


	public String getIsactive() {
		return isactive;
	}


	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}
	
	

}
