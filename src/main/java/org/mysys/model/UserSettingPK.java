package org.mysys.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the user_settings database table.
 */
@Embeddable
public class UserSettingPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String loginid;

	private String key;

	//@Column(insertable=false, updatable=false)
	private long siteid;

	public UserSettingPK() {
	}
	
	public UserSettingPK(String loginid, String key, long siteid) {
		super();
		this.loginid = loginid;
		this.key = key;
		this.siteid = siteid;
	}

	public String getLoginid() {
		return this.loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getKey() {
		return this.key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public long getSiteid() {
		return this.siteid;
	}
	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserSettingPK)) {
			return false;
		}
		UserSettingPK castOther = (UserSettingPK)other;
		return 
			this.loginid.equals(castOther.loginid)
			&& this.key.equals(castOther.key)
			&& (this.siteid == castOther.siteid);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.loginid.hashCode();
		hash = hash * prime + this.key.hashCode();
		hash = hash * prime + ((int) (this.siteid ^ (this.siteid >>> 32)));
		
		return hash;
	}
}