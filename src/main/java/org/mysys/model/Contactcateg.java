package org.mysys.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the contactcateg database table.
 * 
 */
@Entity
@NamedQuery(name="Contactcateg.findAll", query="SELECT c FROM Contactcateg c")
public class Contactcateg extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="contactcateg_id_seq", name="contactcategIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="contactcategIdSeq")
	@Column(name="categoryid", updatable = false, nullable = false)
	private long categoryid;

	private String categoryname;

	//bi-directional many-to-one association to Contact
	@JsonIgnore
	@OneToMany(mappedBy="contactcateg")
	private List<Contact> contacts;

	private long siteid;

	public Contactcateg() {
	}

	public long getCategoryid() {
		return this.categoryid;
	}

	public void setCategoryid(long categoryid) {
		this.categoryid = categoryid;
	}

	public String getCategoryname() {
		return this.categoryname;
	}

	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}

	public List<Contact> getContacts() {
		return this.contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public Contact addContact(Contact contact) {
		getContacts().add(contact);
		contact.setContactcateg(this);

		return contact;
	}

	public Contact removeContact(Contact contact) {
		getContacts().remove(contact);
		contact.setContactcateg(null);

		return contact;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}