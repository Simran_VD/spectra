//package org.mysys.model;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="partner_team_vw")
//@NamedQuery(name="PartnerTeamVw.findAll",query="Select c from PartnerTeamVw c")
//public class PartnerTeamVw implements Serializable{
//	private static final long serialVersionUID = 1L;
//	
//	
//    @Column(name="pm_id")
//	private Long pmId;
//    
//    @Id
//	@Column(name="pt_id")
//	private Long prdId;
//	
//	private String name;
//	
//	@Column(name="display_name")
//	private String displayName;
//	
//	private String company;
//	
//	private String address;
//	
//	private String landmark;
//	
//	private String city;
//	
//	private String state;
//	
//	private String phone;
//	
//	private String mobile;
//	
//	private String email;
//	
//	@Column(name="aadhaar_no")
//	private String aadhaarNo;
//	
//	private String isActive;
//	
//	private String remarks;
//	
//	private Long siteId;
//
//	public Long getPmId() {
//		return pmId;
//	}
//
//	public void setPmId(Long pmId) {
//		this.pmId = pmId;
//	}
//
//	public Long getPrdId() {
//		return prdId;
//	}
//
//	public void setPrdId(Long prdId) {
//		this.prdId = prdId;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	public String getCompany() {
//		return company;
//	}
//
//	public void setCompany(String company) {
//		this.company = company;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public String getLandmark() {
//		return landmark;
//	}
//
//	public void setLandmark(String landmark) {
//		this.landmark = landmark;
//	}
//
//	public String getCity() {
//		return city;
//	}
//
//	public void setCity(String city) {
//		this.city = city;
//	}
//
//	public String getState() {
//		return state;
//	}
//
//	public void setState(String state) {
//		this.state = state;
//	}
//
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	public String getMobile() {
//		return mobile;
//	}
//
//	public void setMobile(String mobile) {
//		this.mobile = mobile;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getAadhaarNo() {
//		return aadhaarNo;
//	}
//
//	public void setAadhaarNo(String aadhaarNo) {
//		this.aadhaarNo = aadhaarNo;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public Long getSiteId() {
//		return siteId;
//	}
//
//	public void setSiteId(Long siteId) {
//		this.siteId = siteId;
//	}
//
//
//}
