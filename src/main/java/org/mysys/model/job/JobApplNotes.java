package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "job_appl_notes")
@NamedQuery(name = "JobApplNotes.findAll", query = "select i from JobApplNotes i")
public class JobApplNotes extends AuditableCustom<String> implements Serializable{
	public static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize = 1, sequenceName = "job_appl_notes_seq", name = "jobapplnotesseq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jobapplnotesseq")
	@Column(name = "jan_id")
	private long janId;
	
	@Column(name = "ja_id")
	private long jaId;
	
	@Column(name = "ntm_id")
	private long ntmId;
	
	private String remarks;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "follow_up_dt")
	private Date followUpDt;
	
	private String isActive;
	
	private long siteid;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "ntm_id", insertable = false, updatable = false)
	private NoteTypeMst noteTypeMst;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "ja_id", insertable = false, updatable = false)
	private JobApplicant jobApplicant;
	
	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;

	public long getJanId() {
		return janId;
	}

	public void setJanId(long janId) {
		this.janId = janId;
	}

	public long getJaId() {
		return jaId;
	}

	public void setJaId(long jaId) {
		this.jaId = jaId;
	}

	public long getNtmId() {
		return ntmId;
	}

	public void setNtmId(long ntmId) {
		this.ntmId = ntmId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getFollowUpDt() {
		return followUpDt;
	}

	public void setFollowUpDt(Date followUpDt) {
		this.followUpDt = followUpDt;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public NoteTypeMst getNoteTypeMst() {
		return noteTypeMst;
	}

	public void setNoteTypeMst(NoteTypeMst noteTypeMst) {
		this.noteTypeMst = noteTypeMst;
	}

	public JobApplicant getJobApplicant() {
		return jobApplicant;
	}

	public void setJobApplicant(JobApplicant jobApplicant) {
		this.jobApplicant = jobApplicant;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	
	
	
	
}
