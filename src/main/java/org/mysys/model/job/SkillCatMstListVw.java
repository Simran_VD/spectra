package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "skill_cat_mst_list_vw")
@NamedQuery(name="SkillCatMstListVw.findAll",query="Select c from SkillCatMstListVw c")
public class SkillCatMstListVw implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "scm_id")
	private long scmId;
	
	@Column(name = "skill_cat_name")
	private String skillCatName;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "siteid")
	private Long siteId;
	
	@Column(name = "userid")
	private Long userId;
	
	public long getScmId() {
		return scmId;
	}

	public void setScmId(long scmId) {
		this.scmId = scmId;
	}

	public String getSkillCatName() {
		return skillCatName;
	}

	public void setSkillCatName(String skillCatName) {
		this.skillCatName = skillCatName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
