package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "job_recu_map")
@NamedQuery(name="JobRecuMap.findAll",query="Select c from JobRecuMap c")
public class JobRecuMap extends AuditableCustom<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="job_recu_map_seq", name="jobrecumapseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="jobrecumapseq")
	@Column(name = "jrm_id")
	private Long jrmId;
	
	@Column(name = "jm_id")
	private long jmId;
	
	@Column(name = "rg_id")
	private Long rgId;
	
	@Column(name = "rec_id")
	private Long recId;
	
	@Column(name = "rgm_id")
	private Long rgmId;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteid;
	
	@Column(name = "spare_v1")
	private String sparev1;
	
	


	@JsonIgnore
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="jm_id",insertable=false,updatable=false)
	private JobMaster jobMaster;
	
	
	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;
	
	public JobRecuMap() {
	}


	public Long getJrmId() {
		return jrmId;
	}


	public void setJrmId(Long jrmId) {
		this.jrmId = jrmId;
	}


	public long getJmId() {
		return jmId;
	}


	public void setJmId(long jmId) {
		this.jmId = jmId;
	}


	public Long getRgId() {
		return rgId;
	}


	public void setRgId(Long rgId) {
		this.rgId = rgId;
	}


	public Long getRecId() {
		return recId;
	}


	public void setRecId(Long recId) {
		this.recId = recId;
	}


	public Long getRgmId() {
		return rgmId;
	}


	public void setRgmId(Long rgmId) {
		this.rgmId = rgmId;
	}


	public String getIsActive() {
		return isActive;
	}


	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public Long getSiteid() {
		return siteid;
	}


	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}


	public JobMaster getJobMaster() {
		return jobMaster;
	}


	public void setJobMaster(JobMaster jobMaster) {
		this.jobMaster = jobMaster;
	}

	public String getSparev1() {
		return sparev1;
	}


	public void setSparev1(String sparev1) {
		this.sparev1 = sparev1;
	}


	public String getCreatedby() {
		return createdby;
	}


	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}


	public String getModifiedby() {
		return modifiedby;
	}


	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	
	
}
