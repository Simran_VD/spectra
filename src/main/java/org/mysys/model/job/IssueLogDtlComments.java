package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="issue_log_dtl_comments")
@NamedQuery(name="IssueLogDtlComments.findAll()",query="Select c from IssueLogDtlComments c")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IssueLogDtlComments extends Auditable<String> implements Serializable  {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1,name="issueLogDtlCommSeq",sequenceName="issue_log_dtl_comm_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="issueLogDtlCommSeq")
	@Column(name="ildc_id")
	private Long ildcId;
	
	@Column(name="il_id")
	private Long ilId;
	
	@Column(name="comm_text")
	private String commText;
	
	@Column(name="st_id")
	private long stId;
	
	@Column(name="comm_user_id")
	private Long commUserId;

	@Column(name="remarks")
	private String remarks;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="siteid")
	private long siteId;

	public Long getIldcId() {
		return ildcId;
	}

	public void setIldcId(Long ildcId) {
		this.ildcId = ildcId;
	}

	public String getCommText() {
		return commText;
	}

	public void setCommText(String commText) {
		this.commText = commText;
	}

	public long getStId() {
		return stId;
	}

	public void setStId(long stId) {
		this.stId = stId;
	}

	public Long getCommUserId() {
		return commUserId;
	}

	public void setCommUserId(Long commUserId) {
		this.commUserId = commUserId;
	}


	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public long getSiteId() {
		return siteId;
	}

	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}
	
	public Long getIlId() {
		return ilId;
	}

	public void setIlId(Long ilId) {
		this.ilId = ilId;
	}

}
