package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "domain_mst_drop_down_vw")
@NamedQuery(name="DomainMstDropDownVw.findAll",query="Select c from DomainMstDropDownVw c")
public class DomainMstDropDownVw extends Auditable<String> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="cl_id")
	private long clId;
	
	@Column(name="company_name")
	private String companyName;
	
	@Column(name="location")
	private String location;
	
	@Column(name="country")
	private String country;
	
	@Column(name="domain_name")
	private String domainName;
	
	@Column(name="company_type")
	private String companyType;
	
	@Column(name="inv_waiting_period")
	private Long invWaitingPeriod;
	
	@Column(name="replacement_period")
	private Long replacementPeriod;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;
	
	public DomainMstDropDownVw() {
		
	}

	public long getClId() {
		return clId;
	}

	public void setClId(long clId) {
		this.clId = clId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public Long getInvWaitingPeriod() {
		return invWaitingPeriod;
	}

	public void setInvWaitingPeriod(Long invWaitingPeriod) {
		this.invWaitingPeriod = invWaitingPeriod;
	}

	public Long getReplacementPeriod() {
		return replacementPeriod;
	}

	public void setReplacementPeriod(Long replacementPeriod) {
		this.replacementPeriod = replacementPeriod;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	
}
