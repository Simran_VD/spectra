package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "applicant_mst")
@NamedQuery(name="ApplicantMasterWeb.findAll",query="Select c from ApplicantMasterWeb c")
public class ApplicantMasterWeb implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="applicant_mst_seq", name="applicantmstseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="applicantmstseq")
	@Column(name = "am_id")
	private Long amId;
	
	@Column(name = "jm_id")
	private Long jmId;
	
	@Column(name = "lm_id")
	private Long lmId;

	@Column(name = "email")
	private String email;
	
	@Column(name = "mobile")
	private String mobile;
	
	@Column(name = "mobile_1")
	private String mobile1;
	
	@Column(name = "mobile_2")
	private String mobile2;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "father_name")
	private String fatherName;
	
	@Column(name = "mother_name")
	private String motherName;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dob")
	private Date dob;
	
	@Column(name = "addr_1")
	private String addr1;
	
	@Column(name = "addr_2")
	private String addr2;
	
	@Column(name = "addr_3")
	private String addr3;
	
	@Column(name = "cityid")
	private Long cityId;
	
	@Column(name = "stateid")
	private Long stateId;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "zip_code")
	private String zipCode;
	
	@Column(name = "current_city_id")
	private Long currentCityId;
	
	@Column(name = "linkedin_url")
	private String linkedinUrl;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "pan_no")
	private String panNo;
	
	@Column(name = "aadhaar")
	private String aadhaar;
	
	@Column(name = "applicant_code")
	private String applicantCode;
	
	@Column(name = "vn_id")
	private Long vnId;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteid;
	
	@Column(name = "jac_id")
	private Long jacId;
	
	@Column(name = "som_id")
	private Long somId;
	
	@Column(name = "refferedby")
	private String sourceName;
	
	@Column(name = "dm_id")
	private Long dmId;
	
	@Column(name = "exp_ctc")
	private Long expCtc;
	
	@Column(name = "current_ctc")
	private Long currentCtc;
	
	@Column(name ="applicant_image")
	private String applicantImage;

    @Column(name ="applicant_resume")
	private String applicantResume;
    
    @Column(updatable=false)
    private String createdby;
    
    private String modifiedby;
	
	public String getApplicantResume() {
		return applicantResume;
	}

	public void setApplicantResume(String applicantResume) {
		this.applicantResume = applicantResume;
	}

	
	public String getApplicantImage() {
		return applicantImage;
	}

	public void setApplicantImage(String applicantImage) {
		this.applicantImage = applicantImage;
	}


	@Column(name = "current_state_id")
	private Long currentstateid;
	

	@Column(name = "notice_period")
	private Long noticePeriod;

	
	@Column(name = "total_exp")
	private Long totalexp;
	
	
	
	@OneToMany(mappedBy = "applicantMaster", fetch = FetchType.EAGER)
	private List<ApplicantPrefLocation> applicantPrefLocation;
	
	@OneToMany(mappedBy = "applicantMaster", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<ApplicantSkill> applicantSkill;
	
	@OneToMany(mappedBy = "applicantMaster", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<ApplicantEducation> applicanteducation;
	
	@OneToMany(mappedBy = "applicantMaster", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<ApplicantExperience> applicantexperience;
	
	
	@OneToMany(mappedBy="applicantMaster",fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@Fetch(value=FetchMode.SELECT)
	private List<ApplicantCertificate> applicantCertificate;
	

	public List<ApplicantExperience> getApplicantexperience() {
		return applicantexperience;
	}


	public void setApplicantexperience(List<ApplicantExperience> applicantexperience) {
		this.applicantexperience = applicantexperience;
	}


	public List<ApplicantEducation> getApplicanteducation() {
		return applicanteducation;
	}


	public void setApplicanteducation(List<ApplicantEducation> applicanteducation) {
		this.applicanteducation = applicanteducation;
	}


	public ApplicantMasterWeb() {
		
	}


	public Long getAmId() {
		return amId;
	}


	public void setAmId(Long amId) {
		this.amId = amId;
	}


	public Long getJmId() {
		return jmId;
	}


	public void setJmId(Long jmId) {
		this.jmId = jmId;
	}

	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getMobile1() {
		return mobile1;
	}


	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}


	public String getMobile2() {
		return mobile2;
	}


	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}




	public String getFatherName() {
		return fatherName;
	}


	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}


	public String getMotherName() {
		return motherName;
	}


	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}


	public Date getDob() {
		return dob;
	}


	public void setDob(Date dob) {
		this.dob = dob;
	}


	public String getAddr1() {
		return addr1;
	}


	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}


	public String getAddr2() {
		return addr2;
	}


	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}


	public String getAddr3() {
		return addr3;
	}


	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}


	public Long getCityId() {
		return cityId;
	}


	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}


	public Long getStateId() {
		return stateId;
	}


	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getZipCode() {
		return zipCode;
	}


	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}


	public Long getCurrentCityId() {
		return currentCityId;
	}


	public void setCurrentCityId(Long currentCityId) {
		this.currentCityId = currentCityId;
	}


	public String getLinkedinUrl() {
		return linkedinUrl;
	}


	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getPanNo() {
		return panNo;
	}


	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}


	public String getAadhaar() {
		return aadhaar;
	}


	public void setAadhaar(String aadhaar) {
		this.aadhaar = aadhaar;
	}


	public String getApplicantCode() {
		return applicantCode;
	}


	public void setApplicantCode(String applicantCode) {
		this.applicantCode = applicantCode;
	}


	public Long getVnId() {
		return vnId;
	}


	public void setVnId(Long vnId) {
		this.vnId = vnId;
	}


	public String getIsActive() {
		return isActive;
	}


	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public Long getSiteid() {
		return siteid;
	}


	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}


	public Long getJacId() {
		return jacId;
	}


	public void setJacId(Long jacId) {
		this.jacId = jacId;
	}


	public Long getSomId() {
		return somId;
	}


	public void setSomId(Long somId) {
		this.somId = somId;
	}


	public String getSourceName() {
		return sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public Long getDmId() {
		return dmId;
	}


	public void setDmId(Long dmId) {
		this.dmId = dmId;
	}


	public List<ApplicantPrefLocation> getApplicantPrefLocation() {
		return applicantPrefLocation;
	}


	public void setApplicantPrefLocation(List<ApplicantPrefLocation> applicantPrefLocation) {
		this.applicantPrefLocation = applicantPrefLocation;
	}


	public List<ApplicantSkill> getApplicantSkill() {
		return applicantSkill;
	}


	public void setApplicantSkill(List<ApplicantSkill> applicantSkill) {
		this.applicantSkill = applicantSkill;
	}


	public Long getExpCtc() {
		return expCtc;
	}


	public void setExpCtc(Long expCtc) {
		this.expCtc = expCtc;
	}


	public Long getNoticePeriod() {
		return noticePeriod;
	}


	public void setNoticePeriod(Long noticePeriod) {
		this.noticePeriod = noticePeriod;
	}
    
	
	public Long getLmId() {
		return lmId;
	}


	public void setLmId(Long lmId) {
		this.lmId = lmId;
	}


	public List<ApplicantCertificate> getApplicantCertificate() {
		return applicantCertificate;
	}


	public void setApplicantCertificate(List<ApplicantCertificate> applicantCertificate) {
		this.applicantCertificate = applicantCertificate;
	}


	/*
	 * public List<ApplicantSkill> getPrimarySkills() { return primarySkills; }
	 * 
	 * 
	 * public void setPrimarySkills(List<ApplicantSkill> primarySkills) {
	 * this.primarySkills = primarySkills; }
	 * 
	 * 
	 * public List<ApplicantSkill> getSecondarySkills() { return secondarySkills; }
	 * 
	 * 
	 * public void setSecondarySkills(List<ApplicantSkill> secondarySkills) {
	 * this.secondarySkills = secondarySkills; }
	 */


	public Long getCurrentCtc() {
		return currentCtc;
	}


	public void setCurrentCtc(Long currentCtc) {
		this.currentCtc = currentCtc;
	}

	
	public Long getCurrentstateid() {
		return currentstateid;
	}


	public void setCurrentstateid(Long currentstateid) {
		this.currentstateid = currentstateid;
	}




	public Long getTotalexp() {
		return totalexp;
	}


	public void setTotalexp(Long totalexp) {
		this.totalexp = totalexp;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	
	

}
