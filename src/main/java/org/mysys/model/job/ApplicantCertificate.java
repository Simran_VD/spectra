package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "applicant_cert")
@NamedQuery(name="ApplicantCertificate.findAll",query="Select c from ApplicantCertificate c")
public class ApplicantCertificate extends AuditableCustom<String> implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="applicant_cert_seq", name="applicantcertseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="applicantcertseq")
	private Long ac_id;
	
	private Long am_id;
	
	@Column(name="certifi_name")
	private String certificate_name;
	
	private String certified_by;
	

	@Column(name = "year_of_compl")
	private String year_of_compl;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="am_id",insertable=false,updatable=false)
	private ApplicantMaster applicantMaster;
	
	private String isactive;
	
	private String remarks;
	
	private Long siteid;
	
	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;

	public Long getAc_id() {
		return ac_id;
	}

	public void setAc_id(Long ac_id) {
		this.ac_id = ac_id;
	}

	public Long getAm_id() {
		return am_id;
	}

	public void setAm_id(Long am_id) {
		this.am_id = am_id;
	}

	public String getCertificate_name() {
		return certificate_name;
	}

	public void setCertificate_name(String certificate_name) {
		this.certificate_name = certificate_name;
	}

	public String getCertified_by() {
		return certified_by;
	}

	public void setCertified_by(String certified_by) {
		this.certified_by = certified_by;
	}

	public String getYear_of_compl() {
		return year_of_compl;
	}

	public void setYear_of_compl(String year_of_compl) {
		this.year_of_compl = year_of_compl;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public ApplicantMaster getApplicantMaster() {
		return applicantMaster;
	}

	public void setApplicantMaster(ApplicantMaster applicantMaster) {
		this.applicantMaster = applicantMaster;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	
	
	

}
