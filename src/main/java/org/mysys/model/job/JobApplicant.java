package org.mysys.model.job;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;
import org.mysys.model.Screen;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "job_applicant")
@NamedQuery(name="JobApplicant.findAll",query="Select j from JobApplicant j")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class JobApplicant extends AuditableCustom<String> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="job_applicant_seq", name="jobapplicantseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="jobapplicantseq")
	@Column(name = "ja_id")
	private Long jaId;
	
	@Column(name = "jm_id")
	private Long jmId;
	
	@Column(name = "am_id")
	private Long amId;
	
	@Column(name = "jsm_id")
	private Long jsmId;
	
	@Column(name = "rec_id")
	private Long recId;
	
	@Column(name = "csm_id")
	private Long csmId;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_applied")
	private Date dateapplied;
	
	private String source;
	
	@Column(name = "curr_ctc")
	private BigDecimal currctc;
	
	@Column(name = "exp_ctc")
	private BigDecimal expctc;
	
	@Column(name = "notice_period")
	private Long noticeperiod;
	
	@Column(name = "arm_id")
	private Long armId;
	
	@Column(name = "abom_id")
	private Long abomId;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "submission_dt")
	private Date submissiondt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "exp_join_dt")
	private Date expjoindt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "actual_join_dt")
	private Date actualjoindt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "inv_dt")
	private Date invdt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "payment_recd_dt")
	private Date paymentrecddt;

	@OneToMany(mappedBy="jobApplicant", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SELECT)
	private List<JobApplNotes> jobApplNotes;
	
	@OneToMany(mappedBy="jobApplicant", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SELECT)
	private List<JobApplicantInterview> jobInterview;
	
	private Long siteid;
	
	private String negotiable;
	
	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;
	
	/*
	 * @Transient private List<JobApplNotes> applNotes;
	 * 
	 * public List<JobApplNotes> getApplNotes() { return applNotes; }
	 * 
	 * public void setApplNotes(List<JobApplNotes> applNotes) { this.applNotes =
	 * applNotes; }
	 */
	
	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public Long getJaId() {
		return jaId;
	}

	public void setJaId(Long jaId) {
		this.jaId = jaId;
	}

	public Long getJmId() {
		return jmId;
	}

	public void setJmId(Long jmId) {
		this.jmId = jmId;
	}

	public Long getAmId() {
		return amId;
	}

	public void setAmId(Long amId) {
		this.amId = amId;
	}

	public Long getJsmId() {
		return jsmId;
	}

	public void setJsmId(Long jsmId) {
		this.jsmId = jsmId;
	}

	public Date getDateapplied() {
		return dateapplied;
	}

	public void setDateapplied(Date dateapplied) {
		this.dateapplied = dateapplied;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public BigDecimal getCurrctc() {
		return currctc;
	}

	public void setCurrctc(BigDecimal currctc) {
		this.currctc = currctc;
	}

	public BigDecimal getExpctc() {
		return expctc;
	}

	public void setExpctc(BigDecimal expctc) {
		this.expctc = expctc;
	}

	public Long getNoticeperiod() {
		return noticeperiod;
	}

	public void setNoticeperiod(Long noticeperiod) {
		this.noticeperiod = noticeperiod;
	}

	public Date getSubmissiondt() {
		return submissiondt;
	}

	public void setSubmissiondt(Date submissiondt) {
		this.submissiondt = submissiondt;
	}

	public Date getExpjoindt() {
		return expjoindt;
	}

	public void setExpjoindt(Date expjoindt) {
		this.expjoindt = expjoindt;
	}

	public Date getActualjoindt() {
		return actualjoindt;
	}

	public void setActualjoindt(Date actualjoindt) {
		this.actualjoindt = actualjoindt;
	}

	public Date getInvdt() {
		return invdt;
	}

	public void setInvdt(Date invdt) {
		this.invdt = invdt;
	}

	public Date getPaymentrecddt() {
		return paymentrecddt;
	}

	public void setPaymentrecddt(Date paymentrecddt) {
		this.paymentrecddt = paymentrecddt;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getCsmId() {
		return csmId;
	}

	public void setCsmId(Long csmId) {
		this.csmId = csmId;
	}

	public List<JobApplNotes> getJobApplNotes() {
		return jobApplNotes;
	}

	public void setJobApplNotes(List<JobApplNotes> jobApplNotes) {
		this.jobApplNotes = jobApplNotes;
	}

	public String getNegotiable() {
		return negotiable;
	}

	public void setNegotiable(String negotiable) {
		this.negotiable = negotiable;
	}

	public List<JobApplicantInterview> getJobInterview() {
		return jobInterview;
	}

	public void setJobInterview(List<JobApplicantInterview> jobInterview) {
		this.jobInterview = jobInterview;
	}

	public Long getArmId() {
		return armId;
	}

	public void setArmId(Long armId) {
		this.armId = armId;
	}

	public Long getAbomId() {
		return abomId;
	}

	public void setAbomId(Long abomId) {
		this.abomId = abomId;
	}

	public Long getRecId() {
		return recId;
	}

	public void setRecId(Long recId) {
		this.recId = recId;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}	
	
	
	
	
}
