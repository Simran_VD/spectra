package org.mysys.model.job;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.mysys.model.Auditable;


@Entity
@Table(name = "note_typ_mst")
@NamedQuery(name = "NoteTypeMst", query = "select n from NoteTypeMst n")
public class NoteTypeMst extends Auditable<String> implements Serializable{
	public static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize = 1,sequenceName = "note_typ_mst_seq", name = "notetypmstseq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notetypmstseq")
	@Column(name = "ntm_id")
	private long ntmId;
	
	private String noteType;
	
	private String description;

	private String isActive;
	
	private String remarks;
	
	private long siteid;
	
	@OneToMany(mappedBy="noteTypeMst", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SELECT)
	private List<JobApplNotes> jobApplNotes;

	public long getNtmId() {
		return ntmId;
	}

	public void setNtmId(long ntmId) {
		this.ntmId = ntmId;
	}

	public String getNoteType() {
		return noteType;
	}

	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public List<JobApplNotes> getJobApplNotes() {
		return jobApplNotes;
	}

	public void setJobApplNotes(List<JobApplNotes> jobApplNotes) {
		this.jobApplNotes = jobApplNotes;
	}
	
	
}