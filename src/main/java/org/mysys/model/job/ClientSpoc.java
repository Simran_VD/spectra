package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name = "client_spoc_vw")
@NamedQuery(name="ClientSpoc.findAll",query="Select c from ClientSpoc c")
public class ClientSpoc implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "cl_id")
	private long clId;
	
	@Column(name = "company_spoc")
	private String companySpoc;
	
	@Column(name = "company_spoc_email")
	private String companySpocEmail;
	
	@Column(name = "company_spoc_phone")
	private String companySpocPhone;
	
	
	public ClientSpoc() {
		
	}

	public long getClId() {
		return clId;
	}


	public void setClId(long clId) {
		this.clId = clId;
	}

	
	public String getCompanySpoc() {
		return companySpoc;
	}


	public void setCompanySpoc(String companySpoc) {
		this.companySpoc = companySpoc;
	}



	public String getCompanySpocEmail() {
		return companySpocEmail;
	}


	public void setCompanySpocEmail(String companySpocEmail) {
		this.companySpocEmail = companySpocEmail;
	}


	public String getCompanySpocPhone() {
		return companySpocPhone;
	}


	public void setCompanySpocPhone(String companySpocPhone) {
		this.companySpocPhone = companySpocPhone;
	}

}
