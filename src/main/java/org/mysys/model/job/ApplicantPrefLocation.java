package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "applicant_pref_loc")
@NamedQuery(name="ApplicantPrefLocation.findAll",query="Select c from ApplicantPrefLocation c")
public class ApplicantPrefLocation extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="applicant_pref_loc_seq", name="applicantpreflocseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="applicantpreflocseq")
	@Column(name = "apl_id")
	private Long aplId;
	
	@Column(name = "am_id")
	private Long amId;
	
	@Column(name = "lm_id")
	private Long lmId;
	
	@Column(name = "isactive")
    private String isActive;
	
	@Column(name = "remarks")
    private String remarks;
	
	@Column(name = "siteid")
    private Long siteid;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="am_id",insertable=false,updatable=false)
	private ApplicantMaster applicantMaster;
	
	
	public ApplicantPrefLocation() {
		
	}

	public Long getAplId() {
		return aplId;
	}

	public void setAplId(Long aplId) {
		this.aplId = aplId;
	}

	public Long getAmId() {
		return amId;
	}

	public void setAmId(Long amId) {
		this.amId = amId;
	}

	public Long getLmId() {
		return lmId;
	}

	public void setLmId(Long lmId) {
		this.lmId = lmId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public ApplicantMaster getApplicantMaster() {
		return applicantMaster;
	}

	public void setApplicantMaster(ApplicantMaster applicantMaster) {
		this.applicantMaster = applicantMaster;
	}

	
	
}
