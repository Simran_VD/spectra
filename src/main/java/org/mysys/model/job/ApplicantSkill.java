package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "applicant_skill")
@NamedQuery(name="ApplicantSkill.findAll",query="Select c from ApplicantSkill c")
public class ApplicantSkill extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="applicant_skill_seq", name="applicantskillseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="applicantskillseq")
	@Column(name = "as_id")
    private Long asId;
	
	@Column(name = "sm_id")
    private Long smId;
	
	@Column(name = "sdm_id")
    private Long sdmId;
	
	public Long getSdmId() {
		return sdmId;
	}


	public void setSdmId(Long sdmId) {
		this.sdmId = sdmId;
	}


	@Column(name = "am_id")
    private Long amId;
	
	@Column(name = "expertise_level")
    private String expertiseLevel;
	
	@Column(name = "version")
    private String version;
	
	@Column(name = "isactive")
    private String isActive;
	
	@Column(name = "remarks")
    private String remarks;
	
	@Column(name = "siteid")
    private Long siteid;
	
	@Column(name = "skill_pref")
	private String skillPref;

	@JsonIgnore
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="am_id",insertable=false,updatable=false)
	private ApplicantMaster applicantMaster;
	
	
	public ApplicantSkill() {
		
	}

	public String getSkillPref() {
		return skillPref;
	}

	public void setSkillPref(String skillPref) {
		this.skillPref = skillPref;
	}

	public Long getAsId() {
		return asId;
	}


	public void setAsId(Long asId) {
		this.asId = asId;
	}


	public Long getSmId() {
		return smId;
	}


	public void setSmId(Long smId) {
		this.smId = smId;
	}


	public Long getAmId() {
		return amId;
	}


	public void setAmId(Long amId) {
		this.amId = amId;
	}


	public String getExpertiseLevel() {
		return expertiseLevel;
	}


	public void setExpertiseLevel(String expertiseLevel) {
		this.expertiseLevel = expertiseLevel;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public String getIsActive() {
		return isActive;
	}


	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public Long getSiteid() {
		return siteid;
	}


	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}


	public ApplicantMaster getApplicantMaster() {
		return applicantMaster;
	}


	public void setApplicantMaster(ApplicantMaster applicantMaster) {
		this.applicantMaster = applicantMaster;
	}
	
	
	
	
}
