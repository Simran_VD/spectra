package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="fnc_dashboard1_job_appl_list")
@NamedQuery(name="DashboardApplicantCases.findAll",query="Select r from DashboardApplicantCases r")
public class DashboardApplicantCases implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Long ja_id;
	
	private String applicant_name;
	
	private String mobile;
	
	private String job_title;
	
	private String company_name;
	
	private String status;
	
	private Long jm_id;
	
	private Long rec_id;

	public Long getJa_id() {
		return ja_id;
	}

	public void setJa_id(Long ja_id) {
		this.ja_id = ja_id;
	}

	public String getApplicant_name() {
		return applicant_name;
	}

	public void setApplicant_name(String applicant_name) {
		this.applicant_name = applicant_name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getJob_title() {
		return job_title;
	}

	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getJm_id() {
		return jm_id;
	}

	public void setJm_id(Long jm_id) {
		this.jm_id = jm_id;
	}

	public Long getRec_id() {
		return rec_id;
	}

	public void setRec_id(Long rec_id) {
		this.rec_id = rec_id;
	}
	
	

}
