package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "skill_cat_mst")
@NamedQuery(name="SkillCatMst.findAll",query="Select c from SkillCatMst c")
public class SkillCatMst extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="skill_cat_mst_seq", name="skillCatMstseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="skillCatMstseq")
	@Column(name = "scm_id")
	private long scmId;
	
	@Column(name = "skill_cat_name")
	private String skillCatName;
	
	@Column(name = "isactive")
	private String isActive;
	
	public long getScmId() {
		return scmId;
	}

	public void setScmId(long scmId) {
		this.scmId = scmId;
	}

	public String getSkillCatName() {
		return skillCatName;
	}

	public void setSkillCatName(String skillCatName) {
		this.skillCatName = skillCatName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteid;

}
