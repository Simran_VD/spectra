package org.mysys.model.job;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="job_skill")
@NamedQuery(name = "JobSkill.findAll", query="Select c from JobSkill c")
public class JobSkill extends AuditableCustom<String> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="job_skill_seq", name="jobskillSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="jobskillSeq")
	@Column(name="js_id")
	private long jsId;
	
	@Column(name="jm_id")
	private Long jmId;
	
	@Column(name="sm_id")
	private Long smId;
	
	@Column(name="skill_level")
	private String skillLevel;
	
	@Column(name="min_exp")
	private Double minExp;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;
	
	@Column(name="sdm_id")
	private Long sdmId;
	
	@Column(name="ismandatory")
	private String isMandatory;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="jm_id",insertable=false,updatable=false)
	private JobMaster jobMaster;

	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;

	public JobSkill() {
		
	}

	public long getJsId() {
		return jsId;
	}

	public void setJsId(long jsId) {
		this.jsId = jsId;
	}

	public Long getJmId() {
		return jmId;
	}

	public void setJmId(Long jmId) {
		this.jmId = jmId;
	}

	public Long getSmId() {
		return smId;
	}

	public void setSmId(Long smId) {
		this.smId = smId;
	}

	public String getSkillLevel() {
		return skillLevel;
	}

	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public JobMaster getJobMaster() {
		return jobMaster;
	}

	public Long getSdmId() {
		return sdmId;
	}

	public void setSdmId(Long sdmId) {
		this.sdmId = sdmId;
	}

	public Double getMinExp() {
		return minExp;
	}

	public void setMinExp(Double minExp) {
		this.minExp = minExp;
	}
	
	public void setJobMaster(JobMaster jobMaster) {
		this.jobMaster = jobMaster;
	}
	
	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}

	public String getIsMandatory() {
		return isMandatory;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	
	

}
