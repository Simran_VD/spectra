package org.mysys.model.job;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "experience_drop_down_vw")
@NamedQuery(name="ExperienceDropDownVw.findAll",query="Select c from ExperienceDropDownVw c")
public class ExperienceDropDownVw implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "et_id")
	private long etId;
	
	@Column(name = "displayname")
	private String displayName;
	
	@Column(name = "values")
	private BigDecimal values;

	public long getEtId() {
		return etId;
	}

	public void setEtId(long etId) {
		this.etId = etId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public BigDecimal getValues() {
		return values;
	}

	public void setValues(BigDecimal values) {
		this.values = values;
	}

	


}
