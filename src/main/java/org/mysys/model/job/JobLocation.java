package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="job_loc")
@NamedQuery(name = "JobLocation.findAll", query="Select c from JobLocation c")
public class JobLocation extends AuditableCustom<String> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="job_loc_seq", name="joblocSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="joblocSeq")
	@Column(name="jl_id")
	private long jlId;
	
	@Column(name="jm_id", updatable = false)
	private Long jmId;
	
	@Column(name="lm_id")
	private Long lmId;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="jm_id",insertable=false,updatable=false)
	private JobMaster jobMaster;
	
	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;
	
	
	public JobLocation() {
		
	}

	public long getJlId() {
		return jlId;
	}

	public void setJlId(long jlId) {
		this.jlId = jlId;
	}

	public Long getJmId() {
		return jmId;
	}

	public void setJmId(Long jmId) {
		this.jmId = jmId;
	}

	public Long getLmId() {
		return lmId;
	}

	public void setLmId(Long lmId) {
		this.lmId = lmId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public JobMaster getJobMaster() {
		return jobMaster;
	}

	public void setJobMaster(JobMaster jobMaster) {
		this.jobMaster = jobMaster;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	
	

	
}
