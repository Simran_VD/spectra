package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "spoc_mst")
@NamedQuery(name="SpocMst.findAll",query="Select c from SpocMst c")
public class SpocMst extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="spoc_mst_seq", name="spocmstseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="spocmstseq")
	@Column(name = "spoc_id")
	private long spocId;
	
	@Column(name = "spoc_name")
	private String spocName;
	
	@Column(name = "spoc_email")
	private String spocEmail;
	
	@Column(name = "spoc_phone")
	private String spocPhone;
	
	@Column(name = "cl_id")
	private Long clId;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteid;
	
	@Column(name = "fm_id")
	private Long fmId;
	
	
	
	public long getSpocId() {
		return spocId;
	}

	public void setSpocId(long spocId) {
		this.spocId = spocId;
	}

	public String getSpocName() {
		return spocName;
	}

	public void setSpocName(String spocName) {
		this.spocName = spocName;
	}

	public String getSpocEmail() {
		return spocEmail;
	}

	public void setSpocEmail(String spocEmail) {
		this.spocEmail = spocEmail;
	}

	public String getSpocPhone() {
		return spocPhone;
	}

	public void setSpocPhone(String spocPhone) {
		this.spocPhone = spocPhone;
	}

	public Long getClId() {
		return clId;
	}

	public void setClId(Long clId) {
		this.clId = clId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	
	public Long getFmId() {
		return fmId;
	}

	public void setFmId(Long fmId) {
		this.fmId = fmId;
	}

}
