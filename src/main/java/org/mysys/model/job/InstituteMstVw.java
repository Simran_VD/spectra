package org.mysys.model.job;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "institute_mst_vw")
@NamedQuery(name="InstituteMstVw.findAll",query="Select c from InstituteMstVw c")
public class InstituteMstVw {

	
	public Long getImId() {
		return imId;
	}

	public void setImId(Long imId) {
		this.imId = imId;
	}

	public String getInsName() {
		return insName;
	}

	public void setInsName(String insName) {
		this.insName = insName;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	@Id
	@Column(name = "im_id")
	private Long imId;
	
	@Column(name = "ins_name")
	private String insName;
	
	private Long siteid;
	
	private Long userid;
}
