package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "recu_grp_mst_list_vw")
@NamedQuery(name ="RecuGrpMstListView.findAll", query = "select r from RecuGrpMstListView r")
public class RecuGrpMstListView extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "rgm_id")
	private Long rgmId;
	
	@Column(name = "recu_grp_name")
	private String recuGrpName;
	
	@Column(name = "grp_type")
	private String grpType;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteId;
	
	@Column(name = "userid")
	private Long userId;
	
	
	public RecuGrpMstListView() {
		
	}


	public Long getRgmId() {
		return rgmId;
	}


	public void setRgmId(Long rgmId) {
		this.rgmId = rgmId;
	}


	public String getRecuGrpName() {
		return recuGrpName;
	}


	public void setRecuGrpName(String recuGrpName) {
		this.recuGrpName = recuGrpName;
	}


	public String getGrpType() {
		return grpType;
	}


	public void setGrpType(String grpType) {
		this.grpType = grpType;
	}


	public String getIsActive() {
		return isActive;
	}


	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public Long getSiteId() {
		return siteId;
	}


	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}


	public Long getUserId() {
		return userId;
	}


	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	

}
