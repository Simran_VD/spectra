package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="job_edu")
@NamedQuery(name = "JobEdu.findAll", query="Select c from JobEdu c")
public class JobEdu extends AuditableCustom<String> implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="job_edu_seq", name="jobeduSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="jobeduSeq")
	@Column(name="je_id")
	private long jeId;
	
	@Column(name="jm_id")
	private Long jmId;
	
	@Column(name="em_id")
	private Long emId;
	
	@Column(name="co_id")
	private Long coId;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;
	
	@Column(name="ismandatory")
	private String isMandatory;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="jm_id",insertable=false,updatable=false)
	private JobMaster jobMaster;
	
	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;

	public long getJeId() {
		return jeId;
	}

	public void setJeId(long jeId) {
		this.jeId = jeId;
	}

	public Long getJmId() {
		return jmId;
	}

	public void setJmId(Long jmId) {
		this.jmId = jmId;
	}

	public Long getEmId() {
		return emId;
	}

	public void setEmId(Long emId) {
		this.emId = emId;
	}

	public Long getCoId() {
		return coId;
	}

	public void setCoId(Long coId) {
		this.coId = coId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public String getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}

	public JobMaster getJobMaster() {
		return jobMaster;
	}

	public void setJobMaster(JobMaster jobMaster) {
		this.jobMaster = jobMaster;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	
	
}
