package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "job_appl_notes_comment_vw")
@NamedQuery(name = "JobApplNotesCommentsVw.findAll" ,query = "select j from JobApplNotesCommentsVw j")
public class JobApplNotesCommentsVw implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "jan_id")
	private Long janId;
	
	@Column(name = "comm_text")
	private String commText;
	
	@Column(name = "ja_id")
	private Long jaId;

	public Long getJaId() {
		return jaId;
	}

	public void setJaId(Long jaId) {
		this.jaId = jaId;
	}

	public Long getJanId() {
		return janId;
	}

	public void setJanId(Long janId) {
		this.janId = janId;
	}

	public String getCommText() {
		return commText;
	}

	public void setCommText(String commText) {
		this.commText = commText;
	}
}
