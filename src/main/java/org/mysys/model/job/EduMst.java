package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "edu_mst")
@NamedQuery(name="EduMst.findAll",query="Select c from EduMst c")
public class EduMst extends Auditable<String> implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="edu_mst_seq", name="edumstseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="edumstseq")
	@Column(name = "em_id")
	private long emId;
	
	@Column(name = "edu_level")
	private String eduLevel;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteid;
	
	public long getEmId() {
		return emId;
	}

	public void setEmId(long emId) {
		this.emId = emId;
	}

	public String getEduLevel() {
		return eduLevel;
	}

	public void setEduLevel(String eduLevel) {
		this.eduLevel = eduLevel;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	

}
