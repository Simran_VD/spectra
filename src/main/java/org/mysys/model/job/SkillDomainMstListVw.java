package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "skill_domain_mst_list_vw")
@NamedQuery(name="SkillDomainMstListVw.findAll",query="Select c from SkillDomainMstListVw c")
public class SkillDomainMstListVw implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "sdm_id")
	private long sdmId;
	
	@Column(name = "skill_domain_name")
	private String skillDomainName;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "siteid")
	private Long siteId;
	
	@Column(name = "userid")
	private Long userId;
	
	public long getSdmId() {
		return sdmId;
	}

	public void setSdmId(long sdmId) {
		this.sdmId = sdmId;
	}

	public String getSkillDomainName() {
		return skillDomainName;
	}

	public void setSkillDomainName(String skillDomainName) {
		this.skillDomainName = skillDomainName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
