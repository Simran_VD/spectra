package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="job_cert")
@NamedQuery(name = "JobCert.findAll", query="Select c from JobCert c")
public class JobCert extends AuditableCustom<String> implements Serializable  {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="job_cert_seq", name="jobcertSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="jobcertSeq")
	@Column(name="jc_id")
	private long jcId;
	
	@Column(name="jm_id")
	private Long jmId;
	
	@Column(name="certifi_name")
	private String certifiName;
	
	@Column(name="certifed_by")
	private String certifedBy;
	
	@Column(name="year_of_compl")
	private String yearOfCompl;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;
	
	@Column(name="ismandatory")
	private String isMandatory;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="jm_id",insertable=false,updatable=false)
	private JobMaster jobMaster;
	
	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;

	public long getJcId() {
		return jcId;
	}

	public void setJcId(long jcId) {
		this.jcId = jcId;
	}

	public Long getJmId() {
		return jmId;
	}

	public void setJmId(Long jmId) {
		this.jmId = jmId;
	}

	public String getCertifiName() {
		return certifiName;
	}

	public void setCertifiName(String certifiName) {
		this.certifiName = certifiName;
	}

	public String getCertifedBy() {
		return certifedBy;
	}

	public void setCertifedBy(String certifedBy) {
		this.certifedBy = certifedBy;
	}

	public String getYearOfCompl() {
		return yearOfCompl;
	}

	public void setYearOfCompl(String yearOfCompl) {
		this.yearOfCompl = yearOfCompl;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public String getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}

	public JobMaster getJobMaster() {
		return jobMaster;
	}

	public void setJobMaster(JobMaster jobMaster) {
		this.jobMaster = jobMaster;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	
	
}
