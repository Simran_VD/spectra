package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "source_mst_drop_down_vw")
@NamedQuery(name="SourceMstDropDown.findAll",query="Select c from SourceMstDropDown c")
public class SourceMstDropDown  extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "som_id")
	private Long somId;
	
	@Column(name = "source_name")
	private String sourceName;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteid;
	
	public SourceMstDropDown() {
		
	}

	public Long getSomId() {
		return somId;
	}

	public void setSomId(Long somId) {
		this.somId = somId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	
}
