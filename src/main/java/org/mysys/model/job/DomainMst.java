package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "domain_mst")
@NamedQuery(name="DomainMst.findAll",query="Select c from DomainMst c")
public class DomainMst extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="domain_mst_seq", name="domainmstseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="domainmstseq")
	@Column(name = "dm_id")
	private long dmId;
	
	@Column(name = "domain_name")
	private String domainName;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteid;
	
	public long getDmId() {
		return dmId;
	}

	public void setDmId(long dmId) {
		this.dmId = dmId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	
	
}
