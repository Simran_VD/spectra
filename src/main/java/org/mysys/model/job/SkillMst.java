package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;


@Entity
@Table(name = "skill_mst")
@NamedQuery(name="SkillMst.findAll",query="Select c from SkillMst c")
public class SkillMst extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="skill_mst_seq", name="skillmstSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="skillmstSeq")
	@Column(name="sm_id")
	private long smId;
	
	@Column(name="skill_name")
	private String skillName;
	
	@Column(name="skill_desc")
	private String skillDesc;
	
	@Column(name="sgm_id")
	private Long sgmId;
	
	@Column(name="scm_id")
	private Long scmId;
	
	@Column(name="sdm_id")
	private Long sdmId;
	
	@Column(name="isactive")
	private String isActive;
	
	public long getSmId() {
		return smId;
	}

	public void setSmId(long smId) {
		this.smId = smId;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public String getSkillDesc() {
		return skillDesc;
	}

	public void setSkillDesc(String skillDesc) {
		this.skillDesc = skillDesc;
	}

	public Long getSgmId() {
		return sgmId;
	}

	public void setSgmId(Long sgmId) {
		this.sgmId = sgmId;
	}

	public Long getScmId() {
		return scmId;
	}

	public void setScmId(Long scmId) {
		this.scmId = scmId;
	}

	public Long getSdmId() {
		return sdmId;
	}

	public void setSdmId(Long sdmId) {
		this.sdmId = sdmId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;


}
