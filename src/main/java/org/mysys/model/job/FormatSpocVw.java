package org.mysys.model.job;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "format_spoc_drop_down_vw")
public class FormatSpocVw {

	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name="fm_id")
	private Long fmId;
	
	//private Long spoc_id;
	
	private Long cl_id;
	
	private String name;
	
	
	//private String spoc_name;
	
	private String company_name;
	
	private Long siteid;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*
	 * public Long getSpoc_id() { return spoc_id; }
	 * 
	 * public void setSpoc_id(Long spoc_id) { this.spoc_id = spoc_id; }
	 */

	public Long getCl_id() {
		return cl_id;
	}

	public void setCl_id(Long cl_id) {
		this.cl_id = cl_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/*
	 * public String getSpoc_name() { return spoc_name; }
	 * 
	 * public void setSpoc_name(String spoc_name) { this.spoc_name = spoc_name; }
	 */

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public Long getFmId() {
		return fmId;
	}

	public void setFmId(Long fmId) {
		this.fmId = fmId;
	}
	
	
	
}
