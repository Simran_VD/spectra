package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "skill_domain_mst_drop_down_vw")
@NamedQuery(name="SkillDomainMstDropDownVw.findAll",query="Select c from SkillDomainMstDropDownVw c")
public class SkillDomainMstDropDownVw extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
//	@SequenceGenerator(allocationSize=1, sequenceName="skill_domain_mst_seq", name="skilldomainmstseq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="skilldomainmstseq")
	@Id
	@Column(name = "sdm_id")
	private long sdmId;
	
	@Column(name = "skill_domain_name")
	private String skillDomainName;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteid;
	
//	@JsonIgnore
//	@ManyToOne
//	@JoinColumn(name="jm_id",insertable=false,updatable=false)
//	private JobMaster jobMaster;
	
	public SkillDomainMstDropDownVw() {
		
	}

	public long getSdmId() {
		return sdmId;
	}

	public void setSdmId(long sdmId) {
		this.sdmId = sdmId;
	}

	public String getSkillDomainName() {
		return skillDomainName;
	}

	public void setSkillDomainName(String skillDomainName) {
		this.skillDomainName = skillDomainName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

//	public JobMaster getJobMaster() {
//		return jobMaster;
//	}
//
//	public void setJobMaster(JobMaster jobMaster) {
//		this.jobMaster = jobMaster;
//	}
//	
    

}
