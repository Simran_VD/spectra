package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "job_status_mst_drop_down_vw")
@NamedQuery(name="JobStatusMaster.findAll",query="Select c from JobStatusMaster c")
public class JobStatusMaster extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="job_status_mst_seq", name="jobstatusmstseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="jobstatusmstseq")
	@Column(name="jsm_id")
	private long jsmId;
	
	@Column(name="status")
	private String status;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;
	
	
	public JobStatusMaster() {
		
	}

	public long getJsmId() {
		return jsmId;
	}


	public void setJsmId(long jsmId) {
		this.jsmId = jsmId;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getIsActive() {
		return isActive;
	}


	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public Long getSiteid() {
		return siteid;
	}


	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	
}
