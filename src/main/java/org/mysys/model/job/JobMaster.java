package org.mysys.model.job;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "job_mst")
@NamedQuery(name="JobMaster.findAll",query="Select c from JobMaster c")
public class JobMaster extends AuditableCustom<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="job_mst_seq", name="jobmasterSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="jobmasterSeq")
	
	@Column(name = "jm_id")
	private Long jmId;

	@Column(name = "cl_id")
	private Long clId;

	@Column(name = "rm_id")
	private Long rmId;

	@Column(name = "company_spoc")
	private String companySpoc;

	@Column(name = "company_spoc_email")
	private String companySpocEmail;

	@Column(name = "company_spoc_phone")
	private String companySpocPhone;

	@Column(name = "job_title")
	private String jobTitle;

	@Column(name = "min_total_exp")
	private BigDecimal minTotalExp;

	@Column(name = "max_total_exp")
	private BigDecimal maxTotalExp;

	@Column(name = "min_rel_exp")
	private BigDecimal minRelExp;
	
	@Column(name = "ctc")
	private Long ctc;

	@Column(name = "joining_period")
	private Long joiningPeriod;

	@Column(name = "job_type")
	private String jobType;

	@Column(name = "job_level")
	private String jobLevel;

	@Column(name = "job_desc")
	private String jobDesc;

	@Column(name = "job_code")
	private String jobCode;

	@Column(name = "no_of_position")
	private Long noOfPosition;

	@Column(name = "employment_type")
	private String employmentType;

	@Column(name = "budget")
	private String budget;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="job_recd_dt")
	private Date jobRecdDt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="job_start_dt")
	private Date jobStartDt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="job_endt_dt")
	private Date jobEndtDt;

	@Column(name = "isactive")
	private String isActive;

	@Column(name = "remarks")
	private String remarks;

	@Column(name = "siteid")
	private Long siteId;

	@Column(name = "jsm_id")
	private Long jsmId;
	
	@Column(name = "spare_n1")
	private Long domainId;
	
	@Column(name = "skill_notes")
	private String skillNote;
	
    @Column(updatable=false)
    private String createdby;
    
    private String modifiedby;


	@OneToMany(mappedBy = "jobMaster", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<JobLocation> jobLocation;
	
	@OneToMany(mappedBy = "jobMaster", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<JobRecuMap> jobRecuMap;
	
	@OneToMany(mappedBy = "jobMaster", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<JobSkill> jobSkills;
	
	@OneToMany(mappedBy = "jobMaster", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<JobEdu> jobEdu;
	
	@OneToMany(mappedBy = "jobMaster", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<JobCert> jobCert;
	
	@OneToMany(mappedBy = "jobMaster", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SELECT)
	private List<JobSpoc> jobSpoc;
	
	public List<JobSpoc> getJobSpoc() {
		return jobSpoc;
	}

	public void setJobSpoc(List<JobSpoc> jobSpoc) {
		this.jobSpoc = jobSpoc;
	}

	public List<JobCert> getJobCert() {
		return jobCert;
	}

	public void setJobCert(List<JobCert> jobCert) {
		this.jobCert = jobCert;
	}

	public List<JobEdu> getJobEdu() {
		return jobEdu;
	}

	public void setJobEdu(List<JobEdu> jobEdu) {
		this.jobEdu = jobEdu;
	}

	public Long getJmId() {
		return jmId;
	}

	public void setJmId(Long jmId) {
		this.jmId = jmId;
	}

	public Long getClId() {
		return clId;
	}

	public void setClId(Long clId) {
		this.clId = clId;
	}

	public Long getRmId() {
		return rmId;
	}

	public void setRmId(Long rmId) {
		this.rmId = rmId;
	}

	public String getCompanySpoc() {
		return companySpoc;
	}

	public void setCompanySpoc(String companySpoc) {
		this.companySpoc = companySpoc;
	}

	public String getCompanySpocEmail() {
		return companySpocEmail;
	}

	public void setCompanySpocEmail(String companySpocEmail) {
		this.companySpocEmail = companySpocEmail;
	}

	public String getCompanySpocPhone() {
		return companySpocPhone;
	}

	public void setCompanySpocPhone(String companySpocPhone) {
		this.companySpocPhone = companySpocPhone;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public BigDecimal getMinTotalExp() {
		return minTotalExp;
	}

	public void setMinTotalExp(BigDecimal minTotalExp) {
		this.minTotalExp = minTotalExp;
	}

	public BigDecimal getMaxTotalExp() {
		return maxTotalExp;
	}

	public void setMaxTotalExp(BigDecimal maxTotalExp) {
		this.maxTotalExp = maxTotalExp;
	}

	public BigDecimal getMinRelExp() {
		return minRelExp;
	}

	public void setMinRelExp(BigDecimal minRelExp) {
		this.minRelExp = minRelExp;
	}

	public Long getCtc() {
		return ctc;
	}

	public void setCtc(Long ctc) {
		this.ctc = ctc;
	}

	public Long getJoiningPeriod() {
		return joiningPeriod;
	}

	public void setJoiningPeriod(Long joiningPeriod) {
		this.joiningPeriod = joiningPeriod;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getJobLevel() {
		return jobLevel;
	}

	public void setJobLevel(String jobLevel) {
		this.jobLevel = jobLevel;
	}

	public String getJobDesc() {
		return jobDesc;
	}

	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public Long getNoOfPosition() {
		return noOfPosition;
	}

	public void setNoOfPosition(Long noOfPosition) {
		this.noOfPosition = noOfPosition;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public Date getJobStartDt() {
		return jobStartDt;
	}

	public void setJobStartDt(Date jobStartDt) {
		this.jobStartDt = jobStartDt;
	}

	public Date getJobEndtDt() {
		return jobEndtDt;
	}

	public void setJobEndtDt(Date jobEndtDt) {
		this.jobEndtDt = jobEndtDt;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public Long getJsmId() {
		return jsmId;
	}

	public void setJsmId(Long jsmId) {
		this.jsmId = jsmId;
	}

	public List<JobLocation> getJobLocation() {
		return jobLocation;
	}

	public void setJobLocation(List<JobLocation> jobLocation) {
		this.jobLocation = jobLocation;
	}

	public List<JobSkill> getJobSkills() {
		return jobSkills;
	}

	public void setJobSkills(List<JobSkill> jobSkills) {
		this.jobSkills = jobSkills;
	}

	public Long getDomainId() {
		return domainId;
	}

	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}

	public String getSkillNote() {
		return skillNote;
	}

	public void setSkillNote(String skillNote) {
		this.skillNote = skillNote;
	}

	public Date getJobRecdDt() {
		return jobRecdDt;
	}

	public void setJobRecdDt(Date jobRecdDt) {
		this.jobRecdDt = jobRecdDt;
	}

	public List<JobRecuMap> getJobRecuMap() {
		return jobRecuMap;
	}

	public void setJobRecuMap(List<JobRecuMap> jobRecuMap) {
		this.jobRecuMap = jobRecuMap;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	
	

	
}
