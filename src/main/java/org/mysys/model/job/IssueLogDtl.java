package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;
import org.mysys.model.Department;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="issue_log_dtl")
@NamedQuery(name="IssueLogDtl.findAll()",query="Select c from IssueLogDtl c")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IssueLogDtl extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1,name="issueLogDtlSeq",sequenceName="issue_log_dtl_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="issueLogDtlSeq")
	@Column(name="ild_id")
	private Long ildId;
	
	@Column(name="il_id")
	private long ilId;
	
	@Column(name="code")
	private String code;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="log_date")
	private Date logDate;
	
	@Column(name="issue_type")
	private String issueType;
	
	@Column(name="description")
	private String description;
	
	@Column(name="status_id")
	private long statusId;
	
	@Column(name="priority")
	private String priority;
	
	@Column(name="assigned_to")
	private Long assignedTo;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="exp_dlv_dt")
	private Date expDlvDt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="prop_dlv_dt")
	private Date propDlvDt;
	
	@Column(name="prev_ild_id")
	private Long prevIldId;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="actual_compl_dt")
	private Date actualComplDt;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="modify_user_id")
	private Long modifyUserId;
	
	@Column(name="siteid")
	private Long siteId;
	
	@Column(name="title")
	private String title;
	
	@Column(name="pin_board_status")
	private String pinboardstatus;
	
	
	

	@ManyToOne
	@JoinColumn(name="il_id", insertable=false, updatable=false)
	private IssueLog issueLog;

	public Long getIldId() {
		return ildId;
	}

	public void setIldId(Long ildId) {
		this.ildId = ildId;
	}

	public long getIlId() {
		return ilId;
	}

	public void setIlId(long ilId) {
		this.ilId = ilId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public String getIssueType() {
		return issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getStatusId() {
		return statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Long getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}

	public Date getExpDlvDt() {
		return expDlvDt;
	}

	public void setExpDlvDt(Date expDlvDt) {
		this.expDlvDt = expDlvDt;
	}

	public Date getPropDlvDt() {
		return propDlvDt;
	}

	public void setPropDlvDt(Date propDlvDt) {
		this.propDlvDt = propDlvDt;
	}

	public Long getPrevIldId() {
		return prevIldId;
	}

	public void setPrevIldId(Long prevIldId) {
		this.prevIldId = prevIldId;
	}

	public Date getActualComplDt() {
		return actualComplDt;
	}

	public void setActualComplDt(Date actualComplDt) {
		this.actualComplDt = actualComplDt;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Long getModifyUserId() {
		return modifyUserId;
	}
	

	public void setModifyUserId(Long modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getPinboardstatus() {
		return pinboardstatus;
	}

	public void setPinboardstatus(String pinboardstatus) {
		this.pinboardstatus = pinboardstatus;
	}

}
