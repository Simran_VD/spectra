package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "applicant_resume")
@NamedQuery(name="ApplicantResume.findAll",query="Select c from ApplicantResume c")
public class ApplicantResume extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="applicant_resume_seq", name="applicantresumeseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="applicantresumeseq")
	@Column(name = "ar_id")
	private Long arId;
	
	@Column(name = "am_id")
	private Long amId;
	
	@Column(name = "resume_name")
	private String resumeName;
	
	@Column(name = "dir_location")
	private String dirLocation;
	
	@Column(name = "isactive")
    private String isActive;
	
	@Column(name = "remarks")
    private String remarks;
	
	@Column(name = "siteid")
    private Long siteid;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="am_id",insertable=false,updatable=false)
	private ApplicantMaster applicantMaster;

	public Long getArId() {
		return arId;
	}

	public void setArId(Long arId) {
		this.arId = arId;
	}

	public Long getAmId() {
		return amId;
	}

	public void setAmId(Long amId) {
		this.amId = amId;
	}

	public String getResumeName() {
		return resumeName;
	}

	public void setResumeName(String resumeName) {
		this.resumeName = resumeName;
	}

	public String getDirLocation() {
		return dirLocation;
	}

	public void setDirLocation(String dirLocation) {
		this.dirLocation = dirLocation;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public ApplicantMaster getApplicantMaster() {
		return applicantMaster;
	}

	public void setApplicantMaster(ApplicantMaster applicantMaster) {
		this.applicantMaster = applicantMaster;
	}
	
	
}
