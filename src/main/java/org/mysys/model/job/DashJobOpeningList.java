package org.mysys.model.job;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table(name="fnc_dash_job_opening_list")
public class DashJobOpeningList {

	@Id
	private Long jm_id;
	
	private Long cl_id;
	
	private String company_type;
	
	private String company_spoc;
	
	private String company_spoc_email;
	
	private String company_spoc_phone;
	
	private String job_type;
	
	private String job_title;
	
	private String job_desc;
	
	private String job_level;
	
	private Double min_total_exp;
	
	private Double min_rel_exp;
	
	private Long ctc;
	
	private String joining_period;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	private Date job_recd_dt;
	
	private Long inv_waiting_period;
	
	private Long replacement_period;
	
	private Long siteid;
	
	private Integer no_of_position;
	
	private BigDecimal budget;
	
	private Integer userid;
	
	private String job_code;
	
	private String company_name;
	
	@Column(name="preferred_location", columnDefinition="text")
	private String preferred_location;
	
	@Column(name="skill_name", columnDefinition="text")
	private String skill_name;
	
	private Double max_total_exp;

	public Long getJm_id() {
		return jm_id;
	}

	public void setJm_id(Long jm_id) {
		this.jm_id = jm_id;
	}

	public Long getCl_id() {
		return cl_id;
	}

	public void setCl_id(Long cl_id) {
		this.cl_id = cl_id;
	}

	public String getCompany_type() {
		return company_type;
	}

	public void setCompany_type(String company_type) {
		this.company_type = company_type;
	}

	public String getCompany_spoc() {
		return company_spoc;
	}

	public void setCompany_spoc(String company_spoc) {
		this.company_spoc = company_spoc;
	}

	public String getCompany_spoc_email() {
		return company_spoc_email;
	}

	public void setCompany_spoc_email(String company_spoc_email) {
		this.company_spoc_email = company_spoc_email;
	}

	public String getCompany_spoc_phone() {
		return company_spoc_phone;
	}

	public void setCompany_spoc_phone(String company_spoc_phone) {
		this.company_spoc_phone = company_spoc_phone;
	}

	public String getJob_type() {
		return job_type;
	}

	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}

	public String getJob_title() {
		return job_title;
	}

	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}

	public String getJob_desc() {
		return job_desc;
	}

	public void setJob_desc(String job_desc) {
		this.job_desc = job_desc;
	}

	public String getJob_level() {
		return job_level;
	}

	public void setJob_level(String job_level) {
		this.job_level = job_level;
	}

	public Double getMin_total_exp() {
		return min_total_exp;
	}

	public void setMin_total_exp(Double min_total_exp) {
		this.min_total_exp = min_total_exp;
	}

	public Double getMin_rel_exp() {
		return min_rel_exp;
	}

	public void setMin_rel_exp(Double min_rel_exp) {
		this.min_rel_exp = min_rel_exp;
	}

	public Long getCtc() {
		return ctc;
	}

	public void setCtc(Long ctc) {
		this.ctc = ctc;
	}

	public String getJoining_period() {
		return joining_period;
	}

	public void setJoining_period(String joining_period) {
		this.joining_period = joining_period;
	}

	public Date getJob_recd_dt() {
		return job_recd_dt;
	}

	public void setJob_recd_dt(Date job_recd_dt) {
		this.job_recd_dt = job_recd_dt;
	}

	public Long getInv_waiting_period() {
		return inv_waiting_period;
	}

	public void setInv_waiting_period(Long inv_waiting_period) {
		this.inv_waiting_period = inv_waiting_period;
	}

	public Long getReplacement_period() {
		return replacement_period;
	}

	public void setReplacement_period(Long replacement_period) {
		this.replacement_period = replacement_period;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public Integer getNo_of_position() {
		return no_of_position;
	}

	public void setNo_of_position(Integer no_of_position) {
		this.no_of_position = no_of_position;
	}

	public BigDecimal getBudget() {
		return budget;
	}

	public void setBudget(BigDecimal budget) {
		this.budget = budget;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getJob_code() {
		return job_code;
	}

	public void setJob_code(String job_code) {
		this.job_code = job_code;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getPreferred_location() {
		return preferred_location;
	}

	public void setPreferred_location(String preferred_location) {
		this.preferred_location = preferred_location;
	}

	public String getSkill_name() {
		return skill_name;
	}

	public void setSkill_name(String skill_name) {
		this.skill_name = skill_name;
	}

	public Double getMax_total_exp() {
		return max_total_exp;
	}

	public void setMax_total_exp(Double max_total_exp) {
		this.max_total_exp = max_total_exp;
	}
	
	
	
	
}
