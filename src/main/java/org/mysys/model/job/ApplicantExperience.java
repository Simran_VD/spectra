package org.mysys.model.job;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "applicant_exp")
@NamedQuery(name="ApplicantExperience.findAll",query="Select c from ApplicantExperience c")
public class ApplicantExperience extends AuditableCustom<String> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="applicant_exp_seq", name="applicantexpseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="applicantexpseq")
	private Long aex_id;
	
	@Column(name = "cm_id")
	private Long cmId;
	
	private Long am_id;
	
	@Column(name = "pm_id")
	private Long pmId;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_dt")
	private Date start_date;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_dt")
	private Date end_date;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="am_id",insertable=false,updatable=false)
	private ApplicantMaster applicantMaster;
	
	
	public ApplicantMaster getApplicantMaster() {
		return applicantMaster;
	}

	public void setApplicantMaster(ApplicantMaster applicantMaster) {
		this.applicantMaster = applicantMaster;
	}

	private String position_held;
	
	private Long last_ctc;
	
	private String isactive;
	
	private String remarks;
	
	private Long siteid;

	private String spare_v2;
	
	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;
	
	public Long getCmId() {
		return cmId;
	}

	public void setCmId(Long cmId) {
		this.cmId = cmId;
	}

	public Long getPmId() {
		return pmId;
	}

	public void setPmId(Long pmId) {
		this.pmId = pmId;
	}

	public String getSpare_v2() {
		return spare_v2;
	}

	public void setSpare_v2(String spare_v2) {
		this.spare_v2 = spare_v2;
	}

	public Long getAex_id() {
		return aex_id;
	}

	public void setAex_id(Long aex_id) {
		this.aex_id = aex_id;
	}

	public Long getAm_id() {
		return am_id;
	}

	public void setAm_id(Long am_id) {
		this.am_id = am_id;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getPosition_held() {
		return position_held;
	}

	public void setPosition_held(String position_held) {
		this.position_held = position_held;
	}

	public Long getLast_ctc() {
		return last_ctc;
	}

	public void setLast_ctc(Long last_ctc) {
		this.last_ctc = last_ctc;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	
	
	
	

}
