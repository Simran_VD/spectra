package org.mysys.model.job;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "cases_status_mst")
@NamedQuery(name = "CasesStatusMst", query = "select c from CasesStatusMst c")
public class CasesStatusMst extends Auditable<String> implements Serializable{
	public static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize = 1, sequenceName = "cases_status_mst_seq", name = "casesstatusmstseq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "casesstatusmstseq")
	@Column(name = "csm_id")
	private long csmId;
	
	private String status;
	
	private String isActive;
	
	private String remarks;
	
	private long siteid;
	
	/*
	 * @OneToMany(mappedBy="casesStatusMst", fetch = FetchType.EAGER) private
	 * List<JobApplicantInterview> jobApplicantInterview;
	 */
	
	/*
	 * public List<JobApplicantInterview> getJobApplicantInterview() { return
	 * jobApplicantInterview; }
	 * 
	 * public void setJobApplicantInterview(List<JobApplicantInterview>
	 * jobApplicantInterview) { this.jobApplicantInterview = jobApplicantInterview;
	 * }
	 */


	public long getCsmId() {
		return csmId;
	}

	public void setCsmId(long csmId) {
		this.csmId = csmId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}
