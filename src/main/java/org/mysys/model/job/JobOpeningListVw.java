package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table(name = "job_opening_list_vw")
@NamedQuery(name="JobOpeningListVw.findAll",query="Select c from JobOpeningListVw c")
public class JobOpeningListVw implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "jm_id")
	private Long jmId;
	
	@Column(name = "job_title")
	private String jobTitle;

	@Column(name = "preferred_location")
	private String preferredLocation;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="job_recd_dt")
	private Date jobRecdDt;
	

	@Column(name = "skill_name")
	private String skillName;
	
	public JobOpeningListVw() {

	}

	public Long getJmId() {
		return jmId;
	}
	
	public void setJmId(Long jmId) {
		this.jmId = jmId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getPreferredLocation() {
		return preferredLocation;
	}

	public void setPreferredLocation(String preferredLocation) {
		this.preferredLocation = preferredLocation;
	}

	
	public Date getJobRecdDt() {
		return jobRecdDt;
	}

	public void setJobRecdDt(Date jobRecdDt) {
		this.jobRecdDt = jobRecdDt;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}


}
