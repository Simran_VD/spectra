package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "company_mst_vw")
@NamedQuery(name="CompanyMstVw.findAll",query="Select c from CompanyMstVw c")
public class CompanyMstVw implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "cm_id")
	private long cmId;
	
	@Column(name = "company_name")
	private String companyName;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "country")
	private String country;

	@Column(name = "domain_name")
	private String domainName;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "siteid")
	private Long siteid;

	public long getCmId() {
		return cmId;
	}

	public void setCmId(long cmId) {
		this.cmId = cmId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

}
