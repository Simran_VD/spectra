package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "statemaster_drop_down_vw")
@NamedQuery(name="StateMasterView.findAll",query="Select c from StateMasterView c")
public class StateMasterView extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "stateid")
	private Long stateId;
	
	@Column(name = "statename")
	private String stateName;
	
	@Column(name = "gstcode")
	private String gstCode;
	
	@Column(name = "tin")
	private String tin;
	
	@Column(name = "siteid")
	private Long siteid;
	
//	@Column(name = "createdBy_id")
//	private Integer createdById;
//	
//	@Column(name = "modifiedBy_id")
//	private Integer modifiedById;
	
	public String getIsActive() {
		return isActive;
	}


	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}


	@Column(name = "isactive")
	private String isActive;
	
	public StateMasterView() {
		
	}


	public Long getStateId() {
		return stateId;
	}


	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}


	public String getStateName() {
		return stateName;
	}


	public void setStateName(String stateName) {
		this.stateName = stateName;
	}


	public String getGstCode() {
		return gstCode;
	}


	public void setGstCode(String gstCode) {
		this.gstCode = gstCode;
	}


	public String getTin() {
		return tin;
	}


	public void setTin(String tin) {
		this.tin = tin;
	}


	public Long getSiteid() {
		return siteid;
	}


	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	
	
	

}
