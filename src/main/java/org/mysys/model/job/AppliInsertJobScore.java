package org.mysys.model.job;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fnc_calc_appl_insert_job_score_P")
public class AppliInsertJobScore {
	
	@Id
	@Column(name = "fnc_calc_appl_insert_job_score_p")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
