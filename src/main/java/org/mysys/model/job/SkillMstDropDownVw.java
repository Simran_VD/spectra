package org.mysys.model.job;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;


@Entity
@Table(name = "skill_mst_drop_down_vw")
@NamedQuery(name="SkillMstDropDownVw.findAll",query="Select c from SkillMstDropDownVw c")
public class SkillMstDropDownVw extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="sm_id")
	private long smId;
	
	@Column(name="skill_name")
	private String skillName;
	
	@Column(name="skill_desc")
	private String skillDesc;
	
	@Column(name="sgm_id")
	private Long sgmId;
	
	@Column(name="scm_id")
	private Long scmId;
	
	@Column(name="sdm_id")
	private Long sdmId;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;

	public SkillMstDropDownVw() {
		
	}

	public long getSmId() {
		return smId;
	}

	public void setSmId(long smId) {
		this.smId = smId;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public String getSkillDesc() {
		return skillDesc;
	}

	public void setSkillDesc(String skillDesc) {
		this.skillDesc = skillDesc;
	}

	public Long getSgmId() {
		return sgmId;
	}

	public void setSgmId(Long sgmId) {
		this.sgmId = sgmId;
	}

	public Long getScmId() {
		return scmId;
	}

	public void setScmId(Long scmId) {
		this.scmId = scmId;
	}

	public Long getSdmId() {
		return sdmId;
	}

	public void setSdmId(Long sdmId) {
		this.sdmId = sdmId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
}
