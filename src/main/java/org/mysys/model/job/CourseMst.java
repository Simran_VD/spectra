package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "course_mst")
@NamedQuery(name="CourseMst.findAll",query="Select c from CourseMst c")
public class CourseMst extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="course_mst_seq", name="coursemstseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="coursemstseq")
	@Column(name = "co_id")
	private long coId;
	
	@Column(name = "course_name")
	private String courseName;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteid;

	public long getCoId() {
		return coId;
	}

	public void setCoId(long coId) {
		this.coId = coId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	

}
