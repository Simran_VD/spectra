package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "fnc_dashboard_applicant_list")
@NamedQuery(name="DashboardApplicantList.findAll",query="Select d from DashboardApplicantList d")
public class DashboardApplicantList  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	public Long getJmId() {
		return jmId;
	}

	public void setJmId(Long jmId) {
		this.jmId = jmId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public Long getCurrentctc() {
		return currentctc;
	}

	public void setCurrentctc(Long currentctc) {
		this.currentctc = currentctc;
	}

	public Long getExpctc() {
		return expctc;
	}

	public void setExpctc(Long expctc) {
		this.expctc = expctc;
	}

	public Long getNoticeperiod() {
		return noticeperiod;
	}

	public void setNoticeperiod(Long noticeperiod) {
		this.noticeperiod = noticeperiod;
	}

	public String getCurrentlocation() {
		return currentlocation;
	}

	public void setCurrentlocation(String currentlocation) {
		this.currentlocation = currentlocation;
	}

	public String getPreferredlocation() {
		return preferredlocation;
	}

	public void setPreferredlocation(String preferredlocation) {
		this.preferredlocation = preferredlocation;
	}

	public Long getRecid() {
		return recid;
	}

	public void setRecid(Long recid) {
		this.recid = recid;
	}

	public Long getAmid() {
		return amid;
	}

	public void setAmid(Long amid) {
		this.amid = amid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
 
	@Id
	@Column(name = "jm_id")
	private Long jmId;

	
	private String name;

	private String mobile;

	
	@Column(name = "skill_name")
	private String  skillName;

	@Column(name = "current_ctc")
	private Long currentctc;

	@Column(name = "exp_ctc")
	private Long expctc;

	@Column(name = "notice_period")
	private Long noticeperiod;

	@Column(name = "current_location")
	private String currentlocation;

	@Column(name = "preferred_location")
	private String preferredlocation;


	@Column(name = "rec_id")
	private Long recid;
	
	@Column(name = "am_id")
	private Long amid;
}
