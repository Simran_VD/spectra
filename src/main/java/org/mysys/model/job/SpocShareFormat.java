package org.mysys.model.job;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="fnc_spoc_share_format")
@NamedQuery(name="SpocShareFormat.findAll",query="Select r from SpocShareFormat r")
public class SpocShareFormat implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	private BigInteger id;
	
	@Column(name = "col_name")
	private String colName;
	
	
	@Column(name = "col_disp_name")
	private String colDispName;
	

	

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}
	
	public String getColDispName() {
		return colDispName;
	}

	public void setColDispName(String colDispName) {
		this.colDispName = colDispName;
	}

}
