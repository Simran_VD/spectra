package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "domain_dropdown_vw")
@NamedQuery(name="DomainDropdownVw.findAll",query="Select c from DomainDropdownVw c")
public class DomainDropdownVw implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "dm_id")
	private long dmId;
	
	@Column(name = "domain_name")
	private String domainName;
	
	public long getDmId() {
		return dmId;
	}

	public void setDmId(long dmId) {
		this.dmId = dmId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

}
