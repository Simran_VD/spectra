package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "applicant_edu")
@NamedQuery(name="ApplicantEducation.findAll",query="Select c from ApplicantEducation c")
public class ApplicantEducation extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="applicant_edu_seq", name="applicanedutseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="applicanedutseq")
	
	@Column(name = "ae_id")
	private Long aeId;
	
	@Column(name = "em_id")
	private Long emId;
	
	@Column(name = "im_id")
	private Long imId;
	
	@Column(name = "co_id")
	private Long coId;
	
	@Column(name = "am_id")
	private Long amId;
	
	@Column(name = "start_year")
	private String startyear;
	
	@Column(name = "complete_year")
	private String completeyear;
	
	private String grade;
	
	private String perc;
	
	private String cgp;
	
	private String isactive;
	
	private String remarks;
	
	private Long siteid;

	@JsonIgnore
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="am_id",insertable=false,updatable=false)
	private ApplicantMaster applicantMaster;
	
	public ApplicantMaster getApplicantMaster() {
		return applicantMaster;
	}

	public void setApplicantMaster(ApplicantMaster applicantMaster) {
		this.applicantMaster = applicantMaster;
	}

	public Long getAeId() {
		return aeId;
	}

	public void setAeId(Long aeId) {
		this.aeId = aeId;
	}

	public Long getEmId() {
		return emId;
	}

	public void setEmId(Long emId) {
		this.emId = emId;
	}

	public Long getImId() {
		return imId;
	}

	public void setImId(Long imId) {
		this.imId = imId;
	}

	public Long getCoId() {
		return coId;
	}

	public void setCoId(Long coId) {
		this.coId = coId;
	}

	public Long getAmId() {
		return amId;
	}

	public void setAmId(Long amId) {
		this.amId = amId;
	}

	public String getStartyear() {
		return startyear;
	}

	public void setStartyear(String startyear) {
		this.startyear = startyear;
	}

	public String getCompleteyear() {
		return completeyear;
	}

	public void setCompleteyear(String completeyear) {
		this.completeyear = completeyear;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getPerc() {
		return perc;
	}

	public void setPerc(String perc) {
		this.perc = perc;
	}

	public String getCgp() {
		return cgp;
	}

	public void setCgp(String cgp) {
		this.cgp = cgp;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
	
}
