package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "notice_period_dropdown_list_vw")
@NamedQuery(name="NoticePeriodDropdownListVw.findAll",query="Select c from NoticePeriodDropdownListVw c")
public class NoticePeriodDropdownListVw implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "np_id")
	private long npId;
	
	@Column(name = "time_period")
	private String timePeriod;
	
	@Column(name = "values")
	private Long values;

	public long getNpId() {
		return npId;
	}

	public void setNpId(long npId) {
		this.npId = npId;
	}

	public String getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(String timePeriod) {
		this.timePeriod = timePeriod;
	}

	public Long getValues() {
		return values;
	}

	public void setValues(Long values) {
		this.values = values;
	}

}
