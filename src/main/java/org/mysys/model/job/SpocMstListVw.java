package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "spoc_mst_list_vw")
@NamedQuery(name="SpocMstListVw.findAll",query="Select c from SpocMstListVw c")
public class SpocMstListVw extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "spoc_id")
	private long spocId;
	
	@Column(name = "spoc_name")
	private String spocName;
	
	@Column(name = "spoc_email")
	private String spocEmail;
	
	@Column(name = "spoc_phone")
	private String spocPhone;
	
	@Column(name = "cl_id")
	private Long clId;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteid;
	
	public long getSpocId() {
		return spocId;
	}

	public void setSpocId(long spocId) {
		this.spocId = spocId;
	}

	public String getSpocName() {
		return spocName;
	}

	public void setSpocName(String spocName) {
		this.spocName = spocName;
	}

	public String getSpocEmail() {
		return spocEmail;
	}

	public void setSpocEmail(String spocEmail) {
		this.spocEmail = spocEmail;
	}

	public String getSpocPhone() {
		return spocPhone;
	}

	public void setSpocPhone(String spocPhone) {
		this.spocPhone = spocPhone;
	}

	public Long getClId() {
		return clId;
	}

	public void setClId(Long clId) {
		this.clId = clId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	
}
