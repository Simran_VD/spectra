package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="fnc_dashboard_tasks_n_reminders")
@NamedQuery(name="TaskAndReminder.findAll",query="Select r from TaskAndReminder r")
public class TaskAndReminder implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;
	
	@Column(name = "ja_id")
	private Long ja_id;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "follow_up_dt")
	private Date followupDate;
	
	private String remarks;
	
	private String message;
	
	private String createdby;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getJa_id() {
		return ja_id;
	}

	public void setJa_id(Long ja_id) {
		this.ja_id = ja_id;
	}


	public Date getFollowupDate() {
		return followupDate;
	}


	public void setFollowupDate(Date followupDate) {
		this.followupDate = followupDate;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getCreatedby() {
		return createdby;
	}


	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	 
}