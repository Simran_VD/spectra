package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "client_mst")
@NamedQuery(name="ClientMst.findAll",query="Select c from ClientMst c")
public class ClientMst  extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="client_mst_seq", name="clientmstseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="clientmstseq")
	@Column(name = "cl_id")
	private long clId;
	
	@Column(name = "company_name")
	private String companyName;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "country")
	private String country;

	@Column(name = "domain_name")
	private String domainName;
	
	@Column(name = "company_type")
	private String companyType;
	
	@Column(name = "inv_waiting_period")
	private Long invWaitingPeriod;
	
	@Column(name = "replacement_period")
	private Long replacementPeriod;
	
	@Column(name = "loc_id")
	private Long locId;

	@Column(name = "dm_id")
	private Long dmId;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	public long getClId() {
		return clId;
	}

	public void setClId(long clId) {
		this.clId = clId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public Long getInvWaitingPeriod() {
		return invWaitingPeriod;
	}

	public void setInvWaitingPeriod(Long invWaitingPeriod) {
		this.invWaitingPeriod = invWaitingPeriod;
	}

	public Long getReplacementPeriod() {
		return replacementPeriod;
	}

	public void setReplacementPeriod(Long replacementPeriod) {
		this.replacementPeriod = replacementPeriod;
	}

	public Long getLocId() {
		return locId;
	}

	public void setLocId(Long locId) {
		this.locId = locId;
	}

	public Long getDmId() {
		return dmId;
	}

	public void setDmId(Long dmId) {
		this.dmId = dmId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	@Column(name = "siteid")
	private Long siteid;
	

}
