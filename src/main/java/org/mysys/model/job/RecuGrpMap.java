package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;
import org.mysys.model.User;
import org.mysys.model.UserSkill;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "recu_grp_map")
@NamedQuery(name="RecuGrpMap.findAll",query="Select c from RecuGrpMap c")
public class RecuGrpMap extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
		
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="recu_grp_map_seq", name="recugrpmapseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="recugrpmapseq")
	@Column(name = "rg_id")
	private long rgId;

	@Column(name = "rec_id")
	private long rec_id;
	
	@Column(name = "rgm_id")
	private long rgmId;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "siteid")
	private Long siteId;
	
	//@JsonIgnore
//	@ManyToOne
//	@JoinColumn(name="rec_id",insertable=false,updatable=false)
//	private User user;
	
	public long getRgId() {
		return rgId;
	}

	public void setRgId(long rgId) {
		this.rgId = rgId;
	}


	public long getRgmId() {
		return rgmId;
	}

	public void setRgmId(long rgmId) {
		this.rgmId = rgmId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}

	public long getRec_id() {
		return rec_id;
	}

	public void setRec_id(long rec_id) {
		this.rec_id = rec_id;
	}
	
	
	
}
