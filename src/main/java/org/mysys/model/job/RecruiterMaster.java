package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "recu_mst")
@NamedQuery(name="RecruiterMaster.findAll",query="Select c from RecruiterMaster c")
public class RecruiterMaster extends Auditable<String> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="recu_mst_seq", name="recumstSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="recumstSeq")
	@Column(name="rm_id")
	private long rmId;
	
	@Column(name="recu_name")
	private String recuName;
	
	@Column(name="recu_start_dt")
	private Date recuStartDt;
	
	@Column(name="recu_end_dt")
	private Date recuEndDt;
	
	@Column(name="email")
	private String email;
	
	@Column(name="mobile")
	private String mobile;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;
	
	public RecruiterMaster(){
		
	}

	public long getRmId() {
		return rmId;
	}

	public void setRmId(long rmId) {
		this.rmId = rmId;
	}

	public String getRecuName() {
		return recuName;
	}

	public void setRecuName(String recuName) {
		this.recuName = recuName;
	}

	public Date getRecuStartDt() {
		return recuStartDt;
	}

	public void setRecuStartDt(Date recuStartDt) {
		this.recuStartDt = recuStartDt;
	}

	public Date getRecuEndDt() {
		return recuEndDt;
	}

	public void setRecuEndDt(Date recuEndDt) {
		this.recuEndDt = recuEndDt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

}
