package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="issue_log")
@NamedQuery(name="IssueLog.findAll()",query="Select c from IssueLog c")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IssueLog extends Auditable<String> implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1,name="issueLogSeq",sequenceName="issue_log_seq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="issueLogSeq")
	@Column(name="il_id")
	private Long ilId;

	@Column(name="module_id")
	private long moduleId;
	
	@Column(name="screen_id")
	private long screenId;
	
	private String type;
	
    private String remarks;
	
	@OneToMany(mappedBy="issueLog", fetch = FetchType.EAGER)
	private List<IssueLogDtl> issueLogDtl;
	
	private long siteId;
	
	@Column(name="task_status")
	private String taskStatus;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Long getIlId() {
		return ilId;
	}

	public void setIlId(Long ilId) {
		this.ilId = ilId;
	}

	public long getModuleId() {
		return moduleId;
	}

	public void setModuleId(long moduleId) {
		this.moduleId = moduleId;
	}

	public long getScreenId() {
		return screenId;
	}

	public void setScreenId(long screenId) {
		this.screenId = screenId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<IssueLogDtl> getIssueLogDtl() {
		return issueLogDtl;
	}

	public void setIssueLogDtl(List<IssueLogDtl> issueLogDtl) {
		this.issueLogDtl = issueLogDtl;
	}

	public long getSiteId() {
		return siteId;
	}

	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
}
