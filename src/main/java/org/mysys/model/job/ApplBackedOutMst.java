package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "appl_backed_out_mst")
@NamedQuery(name = "ApplBackedOutMst.findAll", query = "select b from ApplBackedOutMst b")
public class ApplBackedOutMst implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="appl_backed_out_mst_seq", name="applbackedoutseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="applbackedoutseq")
	@Column(name = "abom_id")
	private long abomId;
	
	private String reasons;
	
	private String isActive;
	
	private long siteid;

	public long getAbomId() {
		return abomId;
	}

	public void setAbomId(long abomId) {
		this.abomId = abomId;
	}

	public String getReasons() {
		return reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
	
	
}
