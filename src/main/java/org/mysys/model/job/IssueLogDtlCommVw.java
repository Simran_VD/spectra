package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="issue_log_dtl_comm_vw")
@NamedQuery(name="IssueLogDtlCommVw.findAll", query="SELECT i FROM IssueLogDtlCommVw i")
public class IssueLogDtlCommVw implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ildc_id")
	private Long ildcId;
	
	@Column(name="il_id")
	private Long ilId;
	
	@Column(name="comm_text")
	private String commText;
		
	@Column(name="displayname")
	private String displayName;
	
	@Column(name="status")
	private String status;
	
	@Column(name="button_type")
	private String buttonType;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="createddt")
	private Date createddt;

	public Date getCreateddt() {
		return createddt;
	}

	public void setCreateddt(Date createddt) {
		this.createddt = createddt;
	}

	public Long getIldcId() {
		return ildcId;
	}

	public void setIldcId(Long ildcId) {
		this.ildcId = ildcId;
	}

	public Long getIlId() {
		return ilId;
	}

	public void setIlId(Long ilId) {
		this.ilId = ilId;
	}

	public String getCommText() {
		return commText;
	}

	public void setCommText(String commText) {
		this.commText = commText;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getButtonType() {
		return buttonType;
	}

	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}
	
	
}
