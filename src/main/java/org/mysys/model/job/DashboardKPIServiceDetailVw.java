package org.mysys.model.job;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="fnc_kpi_job_opening_detail")
public class DashboardKPIServiceDetailVw{
	
	@Id
	private Long id;
	
	private String company_name;
	
	private String job_title;
	
	private String min_rel_exp;
	
	private String job_desc;
	
	private String location;
	
	private String first_name;
	
	private String curr_ctc;
	
	private String exp_ctc;
	
	private String notice_period;

	private String source;
	
	private String skill;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getJob_title() {
		return job_title;
	}

	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}

	public String getMin_rel_exp() {
		return min_rel_exp;
	}

	public void setMin_rel_exp(String min_rel_exp) {
		this.min_rel_exp = min_rel_exp;
	}

	public String getJob_desc() {
		return job_desc;
	}

	public void setJob_desc(String job_desc) {
		this.job_desc = job_desc;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getCurr_ctc() {
		return curr_ctc;
	}

	public void setCurr_ctc(String curr_ctc) {
		this.curr_ctc = curr_ctc;
	}

	public String getExp_ctc() {
		return exp_ctc;
	}

	public void setExp_ctc(String exp_ctc) {
		this.exp_ctc = exp_ctc;
	}

	public String getNotice_period() {
		return notice_period;
	}

	public void setNotice_period(String notice_period) {
		this.notice_period = notice_period;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}
	
	
	
}
