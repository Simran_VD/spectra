package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "recu_grp_map_vw")
@NamedQuery(name="RecuGrpMapVw.findAll",query="Select r from RecuGrpMapVw r")
public class RecuGrpMapVw implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "rg_id")
	private Long rgId;
	
	@Column(name = "rgm_id")
	private Long rgmId;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "siteid")
	private Long siteId;
	
	private String userid;
	
	private String displayname;
	
	@Column(name = "recu_grp_name")
	private String recugrpmap;
	
	@Column(name = "grp_type")
	private String grptype;

	public Long getRgId() {
		return rgId;
	}

	public void setRgId(Long rgId) {
		this.rgId = rgId;
	}

	public Long getRgmId() {
		return rgmId;
	}

	public void setRgmId(Long rgmId) {
		this.rgmId = rgmId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getRecugrpmap() {
		return recugrpmap;
	}

	public void setRecugrpmap(String recugrpmap) {
		this.recugrpmap = recugrpmap;
	}

	public String getGrptype() {
		return grptype;
	}

	public void setGrptype(String grptype) {
		this.grptype = grptype;
	}
}
