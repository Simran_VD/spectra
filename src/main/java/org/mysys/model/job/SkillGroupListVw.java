package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "skill_group_list_vw")
@NamedQuery(name="SkillGroupListVw.findAll",query="Select c from SkillGroupListVw c")
public class SkillGroupListVw implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "sgm_id")
	private long sgmId;
	
	@Column(name = "skill_group_name")
	private String skillGroupName;
	
	@Column(name = "isactive")
	private String isActive;
	
	@Column(name = "siteid")
	private Long siteId;
	
	@Column(name = "userid")
	private Long userId;
	
	public long getSgmId() {
		return sgmId;
	}

	public void setSgmId(long sgmId) {
		this.sgmId = sgmId;
	}

	public String getSkillGroupName() {
		return skillGroupName;
	}

	public void setSkillGroupName(String skillGroupName) {
		this.skillGroupName = skillGroupName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	

}
