package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name = "cases_status_mst_vw")
@NamedQuery(name = "CasesStatusMstVw", query = "select c from CasesStatusMstVw c")
public class CasesStatusMstVw extends Auditable<String> implements Serializable
{
	public static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "csm_id")
	private long csmId;
	
	private String status;
	
	private String isActive;
	
	private String remarks;
	
	private long siteid;

	public long getCsmId() {
		return csmId;
	}

	public void setCsmId(long csmId) {
		this.csmId = csmId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}	
	
	
}
