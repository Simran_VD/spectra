package org.mysys.model.job;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;
import org.mysys.model.InterviewLevelMst;
import org.mysys.model.InterviewStatusMst;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "job_applicant_interview")
@NamedQuery(name = "JobApplicantInterview", query = "select j from JobApplicantInterview j")
public class JobApplicantInterview extends AuditableCustom<String> implements Serializable{
	public static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize = 1, sequenceName = "job_applicant_interview_seq", name = "jobapplicantinterviewseq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jobapplicantinterviewseq")
	@Column(name = "jai_id")
	private long jaiId;

	@Column(name = "ja_id")
	private long jaId;

	@Column(name ="ilm_id")
	private Long ilmId;
	//private long csmId;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "sch_date")
	private Date schDate;

	@Column(name = "panel_name")
	private String panelName;

	@Column(name = "panel_feedback")
	private String panelFeedback;

	private String remarks;

	private String isActive;

	private long siteid;

	@Column(name ="ism_id")
	private Long ismId;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "ja_id", insertable = false, updatable = false)
	private JobApplicant jobApplicant;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "ism_id", insertable = false, updatable = false)
	private InterviewStatusMst interviewStatusMst;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "ilm_id", insertable = false, updatable = false)
	private InterviewLevelMst interviewLevelMst;

	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;
	
	
	//	@ManyToOne
	//	@JoinColumn(name = "csm_id", insertable = false, updatable = false)
	//	private CasesStatusMst casesStatusMst;

	public long getJaiId() {
		return jaiId;
	}

	public void setJaiId(long jaiId) {
		this.jaiId = jaiId;
	}

	public long getJaId() {
		return jaId;
	}

	public void setJaId(long jaId) {
		this.jaId = jaId;
	}

	/*
	 * public long getCsmId() { return csmId; }
	 * 
	 * public void setCsmId(long csmId) { this.csmId = csmId; }
	 */

	public Date getSchDate() {
		return schDate;
	}

	public void setSchDate(Date schDate) {
		this.schDate = schDate;
	}

	public String getPanelName() {
		return panelName;
	}

	public void setPanelName(String panelName) {
		this.panelName = panelName;
	}

	public String getPanelFeedback() {
		return panelFeedback;
	}

	public void setPanelFeedback(String panelFeedback) {
		this.panelFeedback = panelFeedback;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public JobApplicant getJobApplicant() {
		return jobApplicant;
	}

	public void setJobApplicant(JobApplicant jobApplicant) {
		this.jobApplicant = jobApplicant;
	}

	public Long getIsmId() {
		return ismId;
	}

	public void setIsmId(Long ismId) {
		this.ismId = ismId;
	}

	public InterviewStatusMst getInterviewStatusMst() {
		return interviewStatusMst;
	}

	public void setInterviewStatusMst(InterviewStatusMst interviewStatusMst) {
		this.interviewStatusMst = interviewStatusMst;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public InterviewLevelMst getInterviewLevelMst() {
		return interviewLevelMst;
	}

	public void setInterviewLevelMst(InterviewLevelMst interviewLevelMst) {
		this.interviewLevelMst = interviewLevelMst;
	}

	public Long getIlmId() {
		return ilmId;
	}

	public void setIlmId(Long ilmId) {
		this.ilmId = ilmId;
	}

	
	

	/*
	 * public CasesStatusMst getCasesStatusMst() { return casesStatusMst; }
	 * 
	 * public void setCasesStatusMst(CasesStatusMst casesStatusMst) {
	 * this.casesStatusMst = casesStatusMst; }
	 */

}
