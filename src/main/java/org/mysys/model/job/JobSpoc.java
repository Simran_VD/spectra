package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;
import org.mysys.model.AuditableCustom;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="job_spoc")
@NamedQuery(name = "JobSpoc.findAll", query="Select c from JobSpoc c")
public class JobSpoc extends AuditableCustom<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="job_spoc_seq", name="jobspocSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="jobspocSeq")
	@Column(name="js_id")
	private long jsId;
	
	@Column(name="jm_id")
	private Long jmId;
	
	@Column(name="category")
	private String category;
	
	@Column(name="spoc_id")
	private Long spocId;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;
	
	@Column(name="ismandatory")
	private String isMandatory;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="jm_id",insertable=false,updatable=false)
	private JobMaster jobMaster;
	
	@Column(updatable=false)
    private String createdby;
    
    private String modifiedby;

	public long getJsId() {
		return jsId;
	}

	public void setJsId(long jsId) {
		this.jsId = jsId;
	}

	public Long getJmId() {
		return jmId;
	}

	public void setJmId(Long jmId) {
		this.jmId = jmId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getSpocId() {
		return spocId;
	}

	public void setSpocId(Long spocId) {
		this.spocId = spocId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public String getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}

	public JobMaster getJobMaster() {
		return jobMaster;
	}

	public void setJobMaster(JobMaster jobMaster) {
		this.jobMaster = jobMaster;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}


	
	
	
}
