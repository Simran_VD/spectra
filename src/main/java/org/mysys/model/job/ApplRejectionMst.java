package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "appl_rejection_mst")
@NamedQuery(name = "ApplRejectionMst.findAll", query = "select a from ApplRejectionMst a")
public class ApplRejectionMst implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="appl_rejection_mst_seq", name="applrejectionseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="applrejectionseq")
	@Column(name = "arm_id")
	private long armId;
	
	@Column(name = "reasons")
	private String reasons;
	
	private String isActive;
	
	private long siteid;

	public long getArmId() {
		return armId;
	}

	public void setArmId(long armId) {
		this.armId = armId;
	}

	public String getReasons() {
		return reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}	
}
