package org.mysys.model.job;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;


@Entity
@Table(name = "loc_mst_drop_down_vw")
@NamedQuery(name="LocationMasterView.findAll",query="Select c from LocationMasterView c")
public class LocationMasterView extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="loc_mst_seq", name="locmstSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="locmstSeq")
	@Column(name="lm_id")
	private Long lmId;
	
	@Column(name="city_name")
	private String cityName;
	
	@Column(name="country")
	private String country;
	
	@Column(name="stateid")
	private Long stateId;
	
	@Column(name="isactive")
	private String isActive;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="siteid")
	private Long siteid;

	public LocationMasterView() {
		
	}

	public Long getLmId() {
		return lmId;
	}

	public void setLmId(Long lmId) {
		this.lmId = lmId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	
}
