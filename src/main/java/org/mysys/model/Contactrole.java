package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

/**
 * The persistent class for the contactrole database table.
 */
@Entity
@NamedQuery(name="Contactrole.findAll", query="SELECT c FROM Contactrole c")
public class Contactrole extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="contactrole_id_seq", name="contactroleIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="contactroleIdSeq")
	@Column(name="contactroleid", updatable = false, nullable = false)
	private long contactroleid;

	private String contactrolename;

	private long siteid;
	
	public Contactrole() {
	}

	public long getContactroleid() {
		return this.contactroleid;
	}

	public void setContactroleid(long contactroleid) {
		this.contactroleid = contactroleid;
	}

	public String getContactrolename() {
		return this.contactrolename;
	}

	public void setContactrolename(String contactrolename) {
		this.contactrolename = contactrolename;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}