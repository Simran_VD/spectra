package org.mysys.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the contact database table.
 * 
 */
@Entity
@NamedQuery(name="Contact.findAll", query="SELECT c FROM Contact c")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler","site","contactcalldetails","contactemaildetails","customerproductmasters","itemissues","itemissuedforjobs","itemrecds","itemrecdafterjobs","itemreturns1","itemreturns2","supplieritemmasters","vendoritemadjs","vendoritemmasters","contactcateg","contacttype"})
public class Contact extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="contact_id_seq", name="contactIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="contactIdSeq")
	@Column(name="contactid", updatable = false, nullable = false)
	private long contactid;
	
	private Float discount;
	
	private String addr1;

	private String addr2;

	private String addr3;

	private BigDecimal cityid;

	private long contactroleid;

	private String country;

	private String displayname;

	private String email;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = APPConstant.DATE_FORMAT,timezone = APPConstant.TIMEZONE)
    @Temporal(TemporalType.DATE)
	private Date startdate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = APPConstant.DATE_FORMAT,timezone = APPConstant.TIMEZONE)
    @Temporal(TemporalType.DATE)
	private Date enddate;

	private String gstno;

	private String mobile;

	private String name;

	private String phone;

	private String pincode;
	
	private BigDecimal stateid;

	private String status;

	private String transporter;

	private String vendorcode;
	
	//bi-directional many-to-one association to Contactcateg
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="categoryid",insertable=false,updatable=false)
	private Contactcateg contactcateg;
	
	private Long categoryid;

	//bi-directional many-to-one association to Contacttype
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="contacttypeid",insertable=false,updatable=false)
	private Contacttype contacttype;
	
	private Long contacttypeid;

	private long siteid;

	//bi-directional many-to-one association to Contactcalldetail
	@OneToMany(mappedBy="contact")
	private List<Contactcalldetail> contactcalldetails;

	//bi-directional many-to-one association to Contactemaildetail
	@OneToMany(mappedBy="contact")
	private List<Contactemaildetail> contactemaildetails;

	//bi-directional many-to-one association to Contactperson
	@OneToMany(mappedBy="contact",cascade = CascadeType.ALL,fetch=FetchType.EAGER,orphanRemoval=true)
	@Fetch(value = FetchMode.SELECT)
	private List<Contactperson> contactpersons;

	//bi-directional many-to-one association to Customerproductmaster
	
	@Column(name="payment_term")
	private String paymentTerm;
	
    @Column(name="credit_limit_days")
    private BigDecimal creditLimitDays;
    
    @Column(name="is_inter_state")
    private BigDecimal isInterState;
    
    private BigDecimal distance;
    
	public Contact() {
	}

	public long getContactid() {
		return this.contactid;
	}

	public void setContactid(long contactid) {
		this.contactid = contactid;
	}

	public String getAddr1() {
		return this.addr1;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public String getAddr2() {
		return this.addr2;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public String getAddr3() {
		return this.addr3;
	}

	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}

	public BigDecimal getCityid() {
		return this.cityid;
	}

	public void setCityid(BigDecimal cityid) {
		this.cityid = cityid;
	}

	public long getContactroleid() {
		return this.contactroleid;
	}

	public void setContactroleid(long contactroleid) {
		this.contactroleid = contactroleid;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDisplayname() {
		return this.displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public Date getEnddate() {
		return this.enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public String getGstno() {
		return this.gstno;
	}

	public void setGstno(String gstno) {
		this.gstno = gstno;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Date getStartdate() {
		return this.startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public BigDecimal getStateid() {
		return this.stateid;
	}

	public void setStateid(BigDecimal stateid) {
		this.stateid = stateid;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransporter() {
		return this.transporter;
	}

	public void setTransporter(String transporter) {
		this.transporter = transporter;
	}

	public String getVendorcode() {
		return this.vendorcode;
	}

	public void setVendorcode(String vendorcode) {
		this.vendorcode = vendorcode;
	}

	public Contactcateg getContactcateg() {
		return this.contactcateg;
	}

	public void setContactcateg(Contactcateg contactcateg) {
		this.contactcateg = contactcateg;
	}

	public Contacttype getContacttype() {
		return this.contacttype;
	}

	public void setContacttype(Contacttype contacttype) {
		this.contacttype = contacttype;
	}

	public List<Contactcalldetail> getContactcalldetails() {
		return this.contactcalldetails;
	}

	public void setContactcalldetails(List<Contactcalldetail> contactcalldetails) {
		this.contactcalldetails = contactcalldetails;
	}

	public Contactcalldetail addContactcalldetail(Contactcalldetail contactcalldetail) {
		getContactcalldetails().add(contactcalldetail);
		contactcalldetail.setContact(this);

		return contactcalldetail;
	}

	public Contactcalldetail removeContactcalldetail(Contactcalldetail contactcalldetail) {
		getContactcalldetails().remove(contactcalldetail);
		contactcalldetail.setContact(null);

		return contactcalldetail;
	}

	public List<Contactemaildetail> getContactemaildetails() {
		return this.contactemaildetails;
	}

	public void setContactemaildetails(List<Contactemaildetail> contactemaildetails) {
		this.contactemaildetails = contactemaildetails;
	}

	public Contactemaildetail addContactemaildetail(Contactemaildetail contactemaildetail) {
		getContactemaildetails().add(contactemaildetail);
		contactemaildetail.setContact(this);

		return contactemaildetail;
	}

	public Contactemaildetail removeContactemaildetail(Contactemaildetail contactemaildetail) {
		getContactemaildetails().remove(contactemaildetail);
		contactemaildetail.setContact(null);

		return contactemaildetail;
	}

	public List<Contactperson> getContactpersons() {
		return this.contactpersons;
	}

	public void setContactpersons(List<Contactperson> contactpersons) {
		this.contactpersons = contactpersons;
	}

	public Contactperson addContactperson(Contactperson contactperson) {
		getContactpersons().add(contactperson);
		contactperson.setContact(this);

		return contactperson;
	}

	public Contactperson removeContactperson(Contactperson contactperson) {
		getContactpersons().remove(contactperson);
		contactperson.setContact(null);

		return contactperson;
	}

	
	public Long getCategoryid() {
		return categoryid;
	}

	public void setCategoryid(Long categoryid) {
		this.categoryid = categoryid;
	}

	public Long getContacttypeid() {
		return contacttypeid;
	}

	public void setContacttypeid(Long contacttypeid) {
		this.contacttypeid = contacttypeid;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public BigDecimal getCreditLimitDays() {
		return creditLimitDays;
	}

	public void setCreditLimitDays(BigDecimal creditLimitDays) {
		this.creditLimitDays = creditLimitDays;
	}

	public BigDecimal getDistance() {
		return distance;
	}

	public void setDistance(BigDecimal distance) {
		this.distance = distance;
	}

	public BigDecimal getIsInterState() {
		return isInterState;
	}

	public void setIsInterState(BigDecimal isInterState) {
		this.isInterState = isInterState;
	}
}