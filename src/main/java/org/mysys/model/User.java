package org.mysys.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.mysys.constant.APPConstant;
import org.mysys.model.job.RecuGrpMap;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="users_seq", name="userSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="userSeq")
	@Column(name="user_id", updatable = false, nullable = false)
	private long userid;

	private String address;
	
	private String pincode;

	private String displayname;

	private String email;
	
	@Column(name = "profile_img")
	private String profileImage;

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT)
	@Temporal(TemporalType.DATE)
	private Date exitdate;

	private String name;

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT)
	@Temporal(TemporalType.DATE)
	private Date joindate;

	private String loginid;

	private String mobile;

	private String password;

	private String phone;

	private String status;

	private String title;

	@Column(name="spare_v1")
	private String spareV1;
	
	@Column(name="spare_v2")
	private String sparev2;

	@Column(name="spare_v3")
	private String spareV3;
	
	@Column(name="spare_v4")
	private String sparev4;
	
	
	@Column(name="spare_n1")
	private Long spareN1;
	
	@Column(name="spare_n2")
	private Long spareN2;
	
	@Column(name="spare_n3")
	private Long spareN3;
	
	@Column(name="spare_n4")
	private Long spareN4;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="spare_d1")
	private Date spareD1;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="spare_d2")
	private Date spareD2;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="spare_d3")
	private Date spareD3;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="spare_d4")
	private Date spareD4;

	//bi-directional many-to-one association to Userrole
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	private Set<Userrole> userroles;

	private long siteid;
	
	private String token;

	@Column(name="password_reset_date")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_TIME_FORMAT)
	@Temporal(TemporalType.TIMESTAMP)
	private Date passwordResetDate;
	
	@Column(name="state_id")
	private Integer stateId;
	
	@Column(name="city_id")
	private Integer cityId;
	
	public Date getPasswordResetDate() {
		return passwordResetDate;
	}

	public void setPasswordResetDate(Date passwordResetDate) {
		this.passwordResetDate = passwordResetDate;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public User() {
	}

	public long getUserid() {
		return this.userid;
	}

	public void setUserid(long userid) {
		this.userid = userid;
	}


	public String getDisplayname() {
		return this.displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getExitdate() {
		return this.exitdate;
	}

	public void setExitdate(Date exitdate) {
		this.exitdate = exitdate;
	}

	public Date getJoindate() {
		return this.joindate;
	}

	public void setJoindate(Date joindate) {
		this.joindate = joindate;
	}

	public String getLoginid() {
		return this.loginid;
	}

	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<Userrole> getUserroles() {
		return userroles;
	}

	public void setUserroles(Set<Userrole> userroles) {
		this.userroles = userroles;
	}

	public Userrole addUserrole(Userrole userrole) {
		getUserroles().add(userrole);
		userrole.setUser(this);
		return userrole;
	}

	public Userrole removeUserrole(Userrole userrole) {
		getUserroles().remove(userrole);
		userrole.setUser(null);
		return userrole;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getSpareV1() {
		return spareV1;
	}

	public void setSpareV1(String spareV1) {
		this.spareV1 = spareV1;
	}

	public String getSparev2() {
		return sparev2;
	}

	public void setSparev2(String sparev2) {
		this.sparev2 = sparev2;
	}

	public String getSpareV3() {
		return spareV3;
	}

	public void setSpareV3(String spareV3) {
		this.spareV3 = spareV3;
	}

	public String getSparev4() {
		return sparev4;
	}

	public void setSparev4(String sparev4) {
		this.sparev4 = sparev4;
	}

	public Long getSpareN1() {
		return spareN1;
	}

	public void setSpareN1(Long spareN1) {
		this.spareN1 = spareN1;
	}

	public Long getSpareN2() {
		return spareN2;
	}

	public void setSpareN2(Long spareN2) {
		this.spareN2 = spareN2;
	}

	public Long getSpareN3() {
		return spareN3;
	}

	public void setSpareN3(Long spareN3) {
		this.spareN3 = spareN3;
	}

	public Long getSpareN4() {
		return spareN4;
	}

	public void setSpareN4(Long spareN4) {
		this.spareN4 = spareN4;
	}

	public Date getSpareD1() {
		return spareD1;
	}

	public void setSpareD1(Date spareD1) {
		this.spareD1 = spareD1;
	}

	public Date getSpareD2() {
		return spareD2;
	}

	public void setSpareD2(Date spareD2) {
		this.spareD2 = spareD2;
	}

	public Date getSpareD3() {
		return spareD3;
	}

	public void setSpareD3(Date spareD3) {
		this.spareD3 = spareD3;
	}

	public Date getSpareD4() {
		return spareD4;
	}

	public void setSpareD4(Date spareD4) {
		this.spareD4 = spareD4;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	
	
	
}