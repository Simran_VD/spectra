package org.mysys.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.mysys.model.job.JobApplicantInterview;

@Entity
@Table(name = "interview_level_mst")
@NamedQuery(name = "InterviewLevelMst.findAll", query = "Select i from InterviewLevelMst i")
public class InterviewLevelMst extends Auditable<String> implements Serializable{
	public static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize = 1, sequenceName = "interview_level_mst_seq", name = "interviewlevelmstseq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "interviewlevelmstseq")
	@Column(name = "ilm_id")
	private long ilmId;

	private String level;

	private String isactive;
	
	private long siteid;

	@OneToMany(mappedBy="interviewLevelMst", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SELECT)
	private List<JobApplicantInterview> jobApplicantInterview;

	public long getIlmId() {
		return ilmId;
	}

	public void setIlmId(long ilmId) {
		this.ilmId = ilmId;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public List<JobApplicantInterview> getJobApplicantInterview() {
		return jobApplicantInterview;
	}

	public void setJobApplicantInterview(List<JobApplicantInterview> jobApplicantInterview) {
		this.jobApplicantInterview = jobApplicantInterview;
	}
	
	

}
