package org.mysys.model;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="misc_cfg")
@NamedQuery(name="MiscCfg.findAll", query="SELECT m FROM MiscCfg m")
public class MiscCfg  extends  Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="misc_cfg_id_seq ", name="miscCgfIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="miscCgfIdSeq")
	@Column(name="cfg_ID", updatable = false, nullable = false)
	private long cfgId;

	@Column(name="CFG_KEY ", updatable = false, nullable = false)	
	private String cfgKey;

	@Column(name="CFG_VAl ", nullable = false)
	private String cfgValue;

	private String remarks;

	private long siteid;

	public long getCfgId() {
		return cfgId;
	}

	public void setCfgId(long cfgId) {
		this.cfgId = cfgId;
	}

	public String getCfgKey() {
		return cfgKey;
	}

	public void setCfgKey(String cfgKey) {
		this.cfgKey = cfgKey;
	}

	public String getCfgValue() {
		return cfgValue;
	}

	public void setCfgValue(String cfgValue) {
		this.cfgValue = cfgValue;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}
