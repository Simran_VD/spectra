//package org.mysys.model.customer;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="cust_mst")
//@NamedQuery(name="Customer.findAll",query="Select c from Customer c")
//public class Customer implements Serializable {
//	private static final long serialVersionUID =1l;
//
//
//    @Id
//    @SequenceGenerator(allocationSize=1, sequenceName="cust_mst_seq", name="custSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="custSeq")
//    @Column(name="cs_id",nullable = false,updatable = false)
//    private long csId;
//    private String name;
//    
//    @Column(name="display_name")
//    private String displayName;
//        
//    @Column(name="am_id")
//    private Long am_id;
//    
//    private String address;
//    
//    private String landmark;
//    
//    private String city;
//    private String state;
//    
//    private String pinCode;
//
//    private String phone;
//
//    private String mobile;
//    
//    private String email;
//    
//    private String isActive;
//	
//   	private String remarks;
//   	
//   	private long siteid;
//
//	public long getCsId() {
//		return csId;
//	}
//
//	public void setCsId(long csId) {
//		this.csId = csId;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	public Long getAm_id() {
//		return am_id;
//	}
//
//	public void setAm_id(Long am_id) {
//		this.am_id = am_id;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public String getLandmark() {
//		return landmark;
//	}
//
//	public void setLandmark(String landmark) {
//		this.landmark = landmark;
//	}
//
//	public String getCity() {
//		return city;
//	}
//
//	public void setCity(String city) {
//		this.city = city;
//	}
//
//	public String getState() {
//		return state;
//	}
//
//	public void setState(String state) {
//		this.state = state;
//	}
//
//	public String getPinCode() {
//		return pinCode;
//	}
//
//	public void setPinCode(String pinCode) {
//		this.pinCode = pinCode;
//	}
//
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	public String getMobile() {
//		return mobile;
//	}
//
//	public void setMobile(String mobile) {
//		this.mobile = mobile;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//	
//}
