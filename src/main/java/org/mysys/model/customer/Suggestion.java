//package org.mysys.model.customer;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
////import org.mysys.model.Auditable;
//
//@Entity
//@Table(name="suggestion ")
//@NamedQuery(name="Suggestion.findAll",query="Select c from Suggestion c")
//public class Suggestion implements Serializable {
//	private static final long serialVersionUID =1l;
//
//    @Id
//    @SequenceGenerator(allocationSize=1, sequenceName="suggestion_seq", name="suggestion")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="suggestion")
//    @Column(name="s_id ",nullable = false,updatable = false)
//    private long sId;
//    private String name;
//    
//    private String phone;
//
//    private String message;
//        
//    private String email;
//    
//    private String isActive;
//	
//   	private String remarks;
//   	
// 	private long siteid;
//
//	public long getsId() {
//		return sId;
//	}
//
//	public void setsId(long sId) {
//		this.sId = sId;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	public String getMessage() {
//		return message;
//	}
//
//	public void setMessage(String message) {
//		this.message = message;
//	}
//
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
// 	
// 	
//}
