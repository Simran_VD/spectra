//package org.mysys.model;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="prod_brand_mst_vw")
//@NamedQuery(name="ProductBrandMstVw.findAll",query="Select c from ProductBrandMstVw c")
//public class ProductBrandMstVw implements Serializable {
//	private static final long serialVersionUID = 1L;
//	
//	@Id
//    @Column(name="pbm_id")
//	private long pbmId;
//	
//	@Column(name="prd_id")
//	private long prdId;
//	 
//	@Column(name="bm_id")
//	private long bmId;
//	
//	@Column(name="prod_name")
//	private String prodName;
//	
//	@Column(name="brand_name")
//	private String brandName;
//	
//	private String isActive;
//	
//	private long siteId;
//	
//	public long getPbmId() {
//		return pbmId;
//	}
//
//	public void setPbmId(long pbmId) {
//		this.pbmId = pbmId;
//	}
//
//	public long getPrdId() {
//		return prdId;
//	}
//
//	public void setPrdId(long prdId) {
//		this.prdId = prdId;
//	}
//
//	public long getBmId() {
//		return bmId;
//	}
//
//	public void setBmId(long bmId) {
//		this.bmId = bmId;
//	}
//
//	public String getProdName() {
//		return prodName;
//	}
//
//	public void setProdName(String prodName) {
//		this.prodName = prodName;
//	}
//
//	public String getBrandName() {
//		return brandName;
//	}
//
//	public void setBrandName(String brandName) {
//		this.brandName = brandName;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public long getSiteId() {
//		return siteId;
//	}
//
//	public void setSiteId(long siteId) {
//		this.siteId = siteId;
//	}
//
//	
//	
//}
