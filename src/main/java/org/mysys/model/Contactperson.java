package org.mysys.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the contactperson database table.
 * 
 */
@Entity
@NamedQuery(name="Contactperson.findAll", query="SELECT c FROM Contactperson c")
public class Contactperson extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="contactperson_id_seq", name="contactpersonIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="contactpersonIdSeq")
	@Column(name="personid", updatable = false, nullable = false)
	private long personid;

	private String contactpersonname;

	private String department;

	private String designation;

	private String email;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT)
	@Temporal(TemporalType.DATE)
	private Date enddate;

	private String mobile;

	private String phone;

	private String primaryperson;
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT)
	@Temporal(TemporalType.DATE)
	private Date startdate;

	//bi-directional many-to-one association to Contact
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="contactid")
	private Contact contact;

	private long siteid;
	
	public Contactperson() {
	}

	public long getPersonid() {
		return this.personid;
	}

	public void setPersonid(long personid) {
		this.personid = personid;
	}

	public String getContactpersonname() {
		return this.contactpersonname;
	}

	public void setContactpersonname(String contactpersonname) {
		this.contactpersonname = contactpersonname;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getEnddate() {
		return this.enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPrimaryperson() {
		return this.primaryperson;
	}

	public void setPrimaryperson(String primaryperson) {
		this.primaryperson = primaryperson;
	}

	public Date getStartdate() {
		return this.startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Contact getContact() {
		return this.contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}