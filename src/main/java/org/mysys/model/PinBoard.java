package org.mysys.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PinBoard 
{
	@Id
	private int pb_id;
	private String task_title;
	private String task_desc;
	private int sm_id ;
	private String exp_compl_dt;
	private String completion_date ;
	private int creator_id ;
	private int assigned_to_group_id;
	private String comm_type;
	private int assigner_id;
	private int assigned_to;
	private double disp_hrs;
	private String remarks;
	private String isactive;
	private double spare_n1;
	private double spare_n2;
	private String spare_v1;
	private String spare_v2;
	private String spare_d1;
	private String spare_d2;
	private int siteid;
	private String createdby;
	private String createddt;
	private String modifiedby;
	private String modifieddt;
	
	public PinBoard() 
	{
		
	}

	public PinBoard(int pb_id, String task_title, String task_desc, int sm_id, String exp_compl_dt,
			String completion_date, int creator_id, int assigned_to_group_id, String comm_type, int assigner_id,
			int assigned_to, double disp_hrs, String remarks, String isactive, double spare_n1, double spare_n2,
			String spare_v1, String spare_v2, String spare_d1, String spare_d2, int siteid, String createdby,
			String createddt, String modifiedby, String modifieddt) {
		super();
		this.pb_id = pb_id;
		this.task_title = task_title;
		this.task_desc = task_desc;
		this.sm_id = sm_id;
		this.exp_compl_dt = exp_compl_dt;
		this.completion_date = completion_date;
		this.creator_id = creator_id;
		this.assigned_to_group_id = assigned_to_group_id;
		this.comm_type = comm_type;
		this.assigner_id = assigner_id;
		this.assigned_to = assigned_to;
		this.disp_hrs = disp_hrs;
		this.remarks = remarks;
		this.isactive = isactive;
		this.spare_n1 = spare_n1;
		this.spare_n2 = spare_n2;
		this.spare_v1 = spare_v1;
		this.spare_v2 = spare_v2;
		this.spare_d1 = spare_d1;
		this.spare_d2 = spare_d2;
		this.siteid = siteid;
		this.createdby = createdby;
		this.createddt = createddt;
		this.modifiedby = modifiedby;
		this.modifieddt = modifieddt;
	}

	public int getPb_id() {
		return pb_id;
	}

	public void setPb_id(int pb_id) {
		this.pb_id = pb_id;
	}

	public String getTask_title() {
		return task_title;
	}

	public void setTask_title(String task_title) {
		this.task_title = task_title;
	}

	public String getTask_desc() {
		return task_desc;
	}

	public void setTask_desc(String task_desc) {
		this.task_desc = task_desc;
	}

	public int getSm_id() {
		return sm_id;
	}

	public void setSm_id(int sm_id) {
		this.sm_id = sm_id;
	}

	public String getExp_compl_dt() {
		return exp_compl_dt;
	}

	public void setExp_compl_dt(String exp_compl_dt) {
		this.exp_compl_dt = exp_compl_dt;
	}

	public String getCompletion_date() {
		return completion_date;
	}

	public void setCompletion_date(String completion_date) {
		this.completion_date = completion_date;
	}

	public int getCreator_id() {
		return creator_id;
	}

	public void setCreator_id(int creator_id) {
		this.creator_id = creator_id;
	}

	public int getAssigned_to_group_id() {
		return assigned_to_group_id;
	}

	public void setAssigned_to_group_id(int assigned_to_group_id) {
		this.assigned_to_group_id = assigned_to_group_id;
	}

	public String getComm_type() {
		return comm_type;
	}

	public void setComm_type(String comm_type) {
		this.comm_type = comm_type;
	}

	public int getAssigner_id() {
		return assigner_id;
	}

	public void setAssigner_id(int assigner_id) {
		this.assigner_id = assigner_id;
	}

	public int getAssigned_to() {
		return assigned_to;
	}

	public void setAssigned_to(int assigned_to) {
		this.assigned_to = assigned_to;
	}

	public double getDisp_hrs() {
		return disp_hrs;
	}

	public void setDisp_hrs(double disp_hrs) {
		this.disp_hrs = disp_hrs;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public double getSpare_n1() {
		return spare_n1;
	}

	public void setSpare_n1(double spare_n1) {
		this.spare_n1 = spare_n1;
	}

	public double getSpare_n2() {
		return spare_n2;
	}

	public void setSpare_n2(double spare_n2) {
		this.spare_n2 = spare_n2;
	}

	public String getSpare_v1() {
		return spare_v1;
	}

	public void setSpare_v1(String spare_v1) {
		this.spare_v1 = spare_v1;
	}

	public String getSpare_v2() {
		return spare_v2;
	}

	public void setSpare_v2(String spare_v2) {
		this.spare_v2 = spare_v2;
	}

	public String getSpare_d1() {
		return spare_d1;
	}

	public void setSpare_d1(String spare_d1) {
		this.spare_d1 = spare_d1;
	}

	public String getSpare_d2() {
		return spare_d2;
	}

	public void setSpare_d2(String spare_d2) {
		this.spare_d2 = spare_d2;
	}

	public int getSiteid() {
		return siteid;
	}

	public void setSiteid(int siteid) {
		this.siteid = siteid;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCreateddt() {
		return createddt;
	}

	public void setCreateddt(String createddt) {
		this.createddt = createddt;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifieddt() {
		return modifieddt;
	}

	public void setModifieddt(String modifieddt) {
		this.modifieddt = modifieddt;
	}
	
	
	
	
	
	
	

}
