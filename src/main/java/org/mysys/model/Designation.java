package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

/**
 * The persistent class for the contacttype database table.
 */
@Entity
@NamedQuery(name="Designation.findAll", query="SELECT c FROM Designation c")
public class Designation extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="designation_id_seq", name="designationIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="designationIdSeq")
	@Column(name="designationid", updatable = false, nullable = false)
	private long designationid;
	
	@Column(name="desig_code")
	private String desigCode;

	private String designationName;

	private long siteid;

	public Designation() {
	}

	public long getDesignationid() {
		return designationid;
	}

	public void setDesignationid(long designationid) {
		this.designationid = designationid;
	}

	public String getDesigCode() {
		return desigCode;
	}

	public void setDesigCode(String desigCode) {
		this.desigCode = desigCode;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}