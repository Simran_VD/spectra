package org.mysys.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the rolescreenaccess database table.
 * 
 */
@Entity
@NamedQuery(name = "Rolescreenaccess.findAll", query = "SELECT r FROM Rolescreenaccess r")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Rolescreenaccess extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RolescreenaccessPK id;

	private long accesslevel;

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name="roleid",insertable=false,updatable=false)
	@JsonIgnore
	private Role role;

	//bi-directional many-to-one association to Screen
	@ManyToOne
	@JoinColumn(name="screenid",insertable=false,updatable=false)
	@JsonIgnore
	private Screen screen;

	private long siteid;
	
	public Rolescreenaccess() {
	}

	public RolescreenaccessPK getId() {
		return this.id;
	}

	public void setId(RolescreenaccessPK id) {
		this.id = id;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Screen getScreen() {
		return this.screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	public long getAccesslevel() {
		return this.accesslevel;
	}
	
	public void setAccesslevel(long accesslevel) {
		this.accesslevel = accesslevel;
	}
	
	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

}