package org.mysys.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the MstOperationDepartment database table.
 * 
 */
@Embeddable
public class UserSkillPk implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long userid;

	@Column(name="skill_id")
	private long skillId;

	public UserSkillPk() {
	}

	public long getUserid() {
		return userid;
	}

	public void setUserid(long userid) {
		this.userid = userid;
	}

	public long getSkillId() {
		return skillId;
	}

	public void setSkillId(long skillId) {
		this.skillId = skillId;
	}
	
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserSkillPk)) {
			return false;
		}
		UserSkillPk castOther = (UserSkillPk)other;
		return 
			(this.userid == castOther.userid)
			&& (this.skillId == castOther.skillId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.userid ^ (this.userid >>> 32)));
		hash = hash * prime + ((int) (this.skillId ^ (this.skillId >>> 32)));
		
		return hash;
	}
}