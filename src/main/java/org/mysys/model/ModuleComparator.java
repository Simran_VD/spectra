package org.mysys.model;

import java.util.Comparator;

import org.springframework.stereotype.Component;
@Component
public class ModuleComparator implements Comparator<Module>{

	@Override
	public int compare(Module o1, Module o2) {
		if(o1 == null && o2 == null){
			return 0;
		}else if(o1 == null || o2 == null){
			return 1;
		}else{
			//return o1.getModuledisplayname().compareTo(o2.getModuledisplayname());
			return o1.getSeq() - o2.getSeq();
		}
	}
}