
package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the contacttype database table.
 */
@Entity
@Table(name="mst_skill")
@NamedQuery(name="Skill.findAll", query="SELECT s FROM Skill s")
public class Skill extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="mst_skill_seq ", name="skillIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="skillIdSeq")
	@Column(name="id", updatable = false, nullable = false)
	private long id;

	private String name;

	private String description ;

	private long siteid;

	public Skill() {
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}
