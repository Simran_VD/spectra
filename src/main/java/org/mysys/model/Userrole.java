package org.mysys.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the userrole database table.
 * 
 */
@Entity
@NamedQuery(name="Userrole.findAll", query="SELECT u FROM Userrole u")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Userrole extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="userrole_seq", name="userIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="userIdSeq")
	@Column(name="ur_id", updatable = false, nullable = false)
	private long urid;
	
	//bi-directional many-to-one association to Role
	@JsonIgnore
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="roleid",insertable=false,updatable=false)
	private Role role;

	private long siteid;
	
	private long userid;

	private long roleid;

	
	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	//bi-directional many-to-one association to User
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="userid",insertable=false,updatable=false)
	private User user;

	public Userrole() {
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getUserid() {
		return userid;
	}

	public void setUserid(long userid) {
		this.userid = userid;
	}

	public long getRoleid() {
		return roleid;
	}

	public void setRoleid(long roleid) {
		this.roleid = roleid;
	}

	public long getUrid() {
		return urid;
	}

	public void setUrid(long urid) {
		this.urid = urid;
	}

}