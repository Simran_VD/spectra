package org.mysys.model;

import java.io.Serializable;
import javax.persistence.*;

import org.mysys.widget.model.Widget;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the rolescreenaccess database table.
 * 
 */
@Entity
@NamedQuery(name = "Rolewidgetaccess.findAll", query = "SELECT r FROM Rolewidgetaccess r")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Rolewidgetaccess extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RolewidgetaccessPK id;

	private long accesslevel;

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name="roleid",insertable=false,updatable=false)
	@JsonIgnore
	private Role role;

	//bi-directional many-to-one association to Screen
	@ManyToOne
	@JoinColumn(name="widgetId",insertable=false,updatable=false)
	@JsonIgnore
	private Widget widget;

	private long siteid;
	
	public Rolewidgetaccess() {
	}

	public RolewidgetaccessPK getId() {
		return this.id;
	}

	public void setId(RolewidgetaccessPK id) {
		this.id = id;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	

	public Widget getWidget() {
		return widget;
	}

	public void setWidget(Widget widget) {
		this.widget = widget;
	}

	public long getAccesslevel() {
		return this.accesslevel;
	}
	
	public void setAccesslevel(long accesslevel) {
		this.accesslevel = accesslevel;
	}
	
	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

}