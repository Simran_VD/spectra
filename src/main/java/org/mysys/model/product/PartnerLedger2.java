//package org.mysys.model.product;
//
//import java.io.Serializable;
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//import javax.persistence.Transient;
//
//import org.mysys.constant.APPConstant;
//import org.mysys.model.Auditable;
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//
//@Entity
//@Table(name="partner_ledger")
//@NamedQuery(name="PartnerLedger2.findAll",query="Select c from PartnerLedger2 c")
//public class PartnerLedger2 implements Serializable {
//	private static final long serialVersionUID =1l;
//	
//	@Id
//	@SequenceGenerator(allocationSize=1, sequenceName="partner_ledger_seq", name="partnerledSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="partnerledSeq")
//	@Column(name="pl_id")
//	private long plId;
//	
//	@Column(name="ttm_id")
//	private long ttmId;
//	
//	@Column(name="pmm_id")
//	private long pmmId;
//	
//	@Column(name="sl_id")
//	private Long slId;
//	
//	@Column(name="pm_id")
//	private long pmId;
//	
//	@Column(name="tran_no")
//	private String tranNo;
//	
//	@Column(name="card_no")
//	private String cardNo;
//	
//	@Column(name="tran_amount")
//	private long tranAmount;
//	
//	@Column(name="reference_no")
//	private String referenceNo;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="pl_date")
//	private Date plDate;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="tran_date")
//	private Date tranDate;
//	
//	private String remarks;
//
//	private long siteId;
//	
//	@Column(name="approved_by")
//	private String approvedBy;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	@Column(name="approved_time")
//	private Date approvedTime;
//	
//    protected String createdby;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date createddt;
//
//	protected String modifiedby;
//	
//	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//	protected Date modifieddt;
//	
//	
//	public long getPlId() {
//		return plId;
//	}
//	public void setPlId(long plId) {
//		this.plId = plId;
//	}
//	public long getTtmId() {
//		return ttmId;
//	}
//	public void setTtmId(long ttmId) {
//		this.ttmId = ttmId;
//	}
//	public long getPmmId() {
//		return pmmId;
//	}
//	public void setPmmId(long pmmId) {
//		this.pmmId = pmmId;
//	}
//	public Long getSlId() {
//		return slId;
//	}
//	public void setSlId(Long slId) {
//		this.slId = slId;
//	}
//	public long getPmId() {
//		return pmId;
//	}
//	public void setPmId(long pmId) {
//		this.pmId = pmId;
//	}
//	public String getTranNo() {
//		return tranNo;
//	}
//	public void setTranNo(String tranNo) {
//		this.tranNo = tranNo;
//	}
//	
//	public long getTranAmount() {
//		return tranAmount;
//	}
//	public void setTranAmount(long tranAmount) {
//		this.tranAmount = tranAmount;
//	}
//
//	public Date getPlDate() {
//		return plDate;
//	}
//	public void setPlDate(Date plDate) {
//		this.plDate = plDate;
//	}
//	public Date getTranDate() {
//		return tranDate;
//	}
//	public void setTranDate(Date tranDate) {
//		this.tranDate = tranDate;
//	}
//	public String getRemarks() {
//		return remarks;
//	}
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//	public long getSiteId() {
//		return siteId;
//	}
//	public void setSiteId(long siteId) {
//		this.siteId = siteId;
//	}
//	public String getApprovedBy() {
//		return approvedBy;
//	}
//	public void setApprovedBy(String approvedBy) {
//		this.approvedBy = approvedBy;
//	}
//	public Date getApprovedTime() {
//		return approvedTime;
//	}
//	public void setApprovedTime(Date approvedTime) {
//		this.approvedTime = approvedTime;
//	}
//	public String getCardNo() {
//		return cardNo;
//	}
//	public void setCardNo(String cardNo) {
//		this.cardNo = cardNo;
//	}
//	public String getReferenceNo() {
//		return referenceNo;
//	}
//	public void setReferenceNo(String referenceNo) {
//		this.referenceNo = referenceNo;
//	}
//	public String getCreatedby() {
//		return createdby;
//	}
//	public void setCreatedby(String createdby) {
//		this.createdby = createdby;
//	}
//	public Date getCreateddt() {
//		return createddt;
//	}
//	public void setCreateddt(Date createddt) {
//		this.createddt = createddt;
//	}
//	public String getModifiedby() {
//		return modifiedby;
//	}
//	public void setModifiedby(String modifiedby) {
//		this.modifiedby = modifiedby;
//	}
//	public Date getModifieddt() {
//		return modifieddt;
//	}
//	public void setModifieddt(Date modifieddt) {
//		this.modifieddt = modifieddt;
//	}
//	
//	
//}
