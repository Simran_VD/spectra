//package org.mysys.model.product;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="prod_mst_vw")
//@NamedQuery(name="ProductMstVw.findAll",query="Select c from ProductMstVw c")
//public class ProductMstVw implements Serializable {
//	private static final long serialVersionUID =1l;
//
//    @Id
//    @Column(name="prd_id")
//    private long prdId;
//    
//    @Column(name="prod_name")
//    private String prodName;
//    
//   private String isActive;
//	
//   	private String remarks;
//   	
//   	private long siteid;
//   	
//	public long getPrdId() {
//		return prdId;
//	}
//
//	public void setPrdId(long prdId) {
//		this.prdId = prdId;
//	}
//
//	public String getProdName() {
//		return prodName;
//	}
//
//	public void setProdName(String prodName) {
//		this.prodName = prodName;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//   	
//}
