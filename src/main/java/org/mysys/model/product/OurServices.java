//package org.mysys.model.product;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.mysys.model.Auditable;
//
//@Entity
//@Table(name="our_services_vw")
//@NamedQuery(name="OurServices.findAll",query="Select c from OurServices c")
//public class OurServices extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID =1l;
//
//    @Id
//    @SequenceGenerator(allocationSize=1, sequenceName="our_services_seq", name="orSeq")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="orSeq")
//    @Column(name="serv_id  ",nullable = false,updatable = false)
//    private long servId;
//    
//    private String image;
//    
//    private String header;
//    
//    @Column(name="sub_header")
//    private String subHeader;
//    
//    private String text;
//    
//    private String isActive;
//	
//   	private String remarks;
//   	
// 	private long siteid;
//
//	public long getServId() {
//		return servId;
//	}
//
//	public void setServId(long servId) {
//		this.servId = servId;
//	}
//
//	public String getImage() {
//		return image;
//	}
//
//	public void setImage(String image) {
//		this.image = image;
//	}
//
//	public String getHeader() {
//		return header;
//	}
//
//	public void setHeader(String header) {
//		this.header = header;
//	}
//
//	public String getSubHeader() {
//		return subHeader;
//	}
//
//	public void setSubHeader(String subHeader) {
//		this.subHeader = subHeader;
//	}
//
//	public String getText() {
//		return text;
//	}
//
//	public void setText(String text) {
//		this.text = text;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//    
//    
//}
