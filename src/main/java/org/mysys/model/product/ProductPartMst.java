//package org.mysys.model.product;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.mysys.model.Auditable;
//
//@Entity
//@Table(name="prod_part_mst")
//@NamedQuery(name="ProductPartMst.findAll",query="SELECT c FROM ProductPartMst c")
//public class ProductPartMst extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID = 1L;
//	
//	 @Id
//	  @SequenceGenerator(allocationSize=1, sequenceName="prod_part_mst_seq", name="prodPartMstSeq")
//	  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="prodPartMstSeq")	 
//	  @Column(name="pp_id")
//	  private Long ppId;
//	 
//	    @Column(name="parts_name")
//	    private String partsName;
//	    
//	    private String kent;
//	    
//	    private String aquaguard;
//	    
//	    @Column(name="zero_b")
//	    private String zeroB;
//	    
//	    private String normal;
//	    
//
//		private String remarks;
//	  	
//	  	private String isActive;
//	  	
//		private Long siteId;
//	  	
//	  	
//	  	
//	  	
//	  	
//	  	public String getPartsName() {
//			return partsName;
//		}
//
//		public void setPartsName(String partsName) {
//			this.partsName = partsName;
//		}
//
//		public String getKent() {
//			return kent;
//		}
//
//		public void setKent(String kent) {
//			this.kent = kent;
//		}
//
//		public String getAquaguard() {
//			return aquaguard;
//		}
//
//		public void setAquaguard(String aquaguard) {
//			this.aquaguard = aquaguard;
//		}
//
//		public String getZeroB() {
//			return zeroB;
//		}
//
//		public void setZeroB(String zeroB) {
//			this.zeroB = zeroB;
//		}
//
//		public String getNormal() {
//			return normal;
//		}
//
//		public void setNormal(String normal) {
//			this.normal = normal;
//		}
//	  	
//	  	public Long getPpId() {
//			return ppId;
//		}
//
//		public void setPpId(Long ppId) {
//			this.ppId = ppId;
//		}
//
//		public String getRemarks() {
//			return remarks;
//		}
//
//		public void setRemarks(String remarks) {
//			this.remarks = remarks;
//		}
//
//		public String getIsActive() {
//			return isActive;
//		}
//
//		public void setIsActive(String isActive) {
//			this.isActive = isActive;
//		}
//
//		public Long getSiteId() {
//			return siteId;
//		}
//
//		public void setSiteId(Long siteId) {
//			this.siteId = siteId;
//		}
//
//		
//	    
//
//}
