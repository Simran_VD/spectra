//package org.mysys.model.product;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="prod_service_map_vw")
//@NamedQuery(name="ProductServiceVw.findAll",query="Select c from ProductServiceVw c")
//public class ProductServiceVw implements Serializable {
//	private static final long serialVersionUID =1l;
//
//	@Id
//	@Column(name="psm_id")
//	private long psmId;
//
//	@Column(name="prd_id")
//	private long prdId;
//
//	@Column(name="prod_name")
//	private String prodName;
//
//	@Column(name="service_name")
//	private String serviceName;
//	
//	@Column(name="service_time")
//	private String serviceTime;
//	
//	@Column(name="sm_id")
//	private Long smId;
//
//	private BigDecimal rate;
//	
//	
//	@Column(name="comm_ded")
//	private BigDecimal comm;
//
//	private String isActive;
//
//
//	private long siteid;
//	
//	public long getPsmId() {
//		return psmId;
//	}
//
//	public void setPsmId(long psmId) {
//		this.psmId = psmId;
//	}
//
//	public long getPrdId() {
//		return prdId;
//	}
//
//	public void setPrdId(long prdId) {
//		this.prdId = prdId;
//	}
//
//	public String getProdName() {
//		return prodName;
//	}
//
//	public void setProdName(String prodName) {
//		this.prodName = prodName;
//	}
//
//	public String getServiceName() {
//		return serviceName;
//	}
//
//	public void setServiceName(String serviceName) {
//		this.serviceName = serviceName;
//	}
//
//	public BigDecimal getRate() {
//		return rate;
//	}
//
//	public void setRate(BigDecimal rate) {
//		this.rate = rate;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//	public Long getSmId() {
//		return smId;
//	}
//
//	public void setSmId(Long smId) {
//		this.smId = smId;
//	}
//
//	public String getServiceTime() {
//		return serviceTime;
//	}
//
//	public void setServiceTime(String serviceTime) {
//		this.serviceTime = serviceTime;
//	}
//
//	public BigDecimal getComm() {
//		return comm;
//	}
//
//	public void setComm(BigDecimal comm) {
//		this.comm = comm;
//	}
//	
//	
//
//}
