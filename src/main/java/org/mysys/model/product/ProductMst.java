//package org.mysys.model.product;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.mysys.model.Auditable;
//
//@Entity
//@Table(name="prod_mst")
//@NamedQuery(name="ProductMst.findAll",query="Select c from ProductMst c")
//public class ProductMst extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID =1l;
//	
//	   @Id
//		@SequenceGenerator(allocationSize=1, sequenceName="prod_mst_seq", name="partnerSeq")
//		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="partnerSeq")
//	    @Column(name="prd_id")
//	    private long prdId;
//	    
//	    @Column(name="prod_name")
//	    private String prodName;
//	    
//	    @Column(name="isactive")
//	   private String isActive;
//		
//	   	private String remarks;
//	   	
//	   	@Column(name="siteid")
//	   	private long siteId;
//	   	
//	   	public long getPrdId() {
//			return prdId;
//		}
//
//		public void setPrdId(long prdId) {
//			this.prdId = prdId;
//		}
//
//		public String getProdName() {
//			return prodName;
//		}
//
//		public void setProdName(String prodName) {
//			this.prodName = prodName;
//		}
//
//		public String getIsActive() {
//			return isActive;
//		}
//
//		public void setIsActive(String isActive) {
//			this.isActive = isActive;
//		}
//
//		public String getRemarks() {
//			return remarks;
//		}
//
//		public void setRemarks(String remarks) {
//			this.remarks = remarks;
//		}
//
//		public long getSiteId() {
//			return siteId;
//		}
//
//		public void setSiteId(long siteId) {
//			this.siteId = siteId;
//		}
//
//
//	   	
//	   	
//}
