//package org.mysys.model.product;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="payment_mode_mst")
//@NamedQuery(name="PaymentModeMst.findAll",query="Select c from PaymentModeMst c")
//
//public class PaymentModeMst implements Serializable{
//	
//	@Id
//    @Column(name="pmm_id")
//	private long pmmId;
//	
//	private String payment_mode;
//	
//	private String remarks;
//	
//	private long siteId;
//
//	public long getPmmId() {
//		return pmmId;
//	}
//
//	public void setPmmId(long pmmId) {
//		this.pmmId = pmmId;
//	}
//
//	public String getPaymentMode() {
//		return payment_mode;
//	}
//
//	public void setPaymentMode(String paymentMode) {
//		this.payment_mode = paymentMode;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteId() {
//		return siteId;
//	}
//
//	public void setSiteId(long siteId) {
//		this.siteId = siteId;
//	}
//    
//
//}
