//package org.mysys.model.product;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="lead_source_mst_vw")
//@NamedQuery(name="LeadSourceVw.findAll",query="Select c from LeadSourceVw c")
//public class LeadSourceVw implements Serializable {
//	private static final long serialVersionUID =1l;
//
//    @Id
//    @Column(name="lsm_id")
//    private long prdId;
//    
//    private String source;
//    
//   private String isActive;
//	
//   	private String remarks;
//   	
//   	private long siteid;
//   	
//	private long userid;
//
//	public long getPrdId() {
//		return prdId;
//	}
//
//	public void setPrdId(long prdId) {
//		this.prdId = prdId;
//	}
//
//	public String getSource() {
//		return source;
//	}
//
//	public void setSource(String source) {
//		this.source = source;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public long getSiteid() {
//		return siteid;
//	}
//
//	public void setSiteid(long siteid) {
//		this.siteid = siteid;
//	}
//
//	public long getUserid() {
//		return userid;
//	}
//
//	public void setUserid(long userid) {
//		this.userid = userid;
//	}
//   	
//   	
//
//}
