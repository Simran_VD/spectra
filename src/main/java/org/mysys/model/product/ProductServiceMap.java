//package org.mysys.model.product;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//import org.mysys.model.Auditable;
//
//@Entity
//@Table(name="prod_service_map")
//@NamedQuery(name="ProductServiceMap.findAll",query="SELECT c FROM ProductServiceMap c")
////@JsonIgnoreProperties({""})
//
//public class ProductServiceMap extends Auditable<String> implements Serializable {
//	private static final long serialVersionUID =1l;
//	
//  @Id
//  @SequenceGenerator(allocationSize=1, sequenceName="prod_service_mst_seq", name="prodServiceSeq")
//  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="prodServiceSeq")
//
//  	@Column(name="psm_id")
//  	private Long psmId;
//  
//  	@Column(name="prd_id")
//  	private Long prdId;
//  
//  	@Column(name="sm_id")
//  	private Long smId;
//  
//  	private Long rate;
//
//  	@Column(name="service_time")
//  	private Long serviceTime;
//  
//  	private String isActive;
//
//  	private String remarks;
//
//  	private Long siteId;
//
//  	@Column(name="comm_ded")
//  	private BigDecimal commDed;
//
//	public Long getPsmId() {
//		return psmId;
//	}
//
//	public void setPsmId(Long psmId) {
//		this.psmId = psmId;
//	}
//
//	public Long getPrdId() {
//		return prdId;
//	}
//
//	public void setPrdId(Long prdId) {
//		this.prdId = prdId;
//	}
//
//	public Long getSmId() {
//		return smId;
//	}
//
//	public void setSmId(Long smId) {
//		this.smId = smId;
//	}
//
//	public Long getRate() {
//		return rate;
//	}
//
//	public void setRate(Long rate) {
//		this.rate = rate;
//	}
//
//	public Long getServiceTime() {
//		return serviceTime;
//	}
//
//	public void setServiceTime(Long serviceTime) {
//		this.serviceTime = serviceTime;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getRemarks() {
//		return remarks;
//	}
//
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//
//	public Long getSiteId() {
//		return siteId;
//	}
//
//	public void setSiteId(Long siteId) {
//		this.siteId = siteId;
//	}
//
//	public BigDecimal getCommDed() {
//		return commDed;
//	}
//
//	public void setCommDed(BigDecimal commDed) {
//		this.commDed = commDed;
//	}
//  	
//  	
//  
//}
