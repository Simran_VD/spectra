//package org.mysys.model.product;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="tran_type_mst")
//@NamedQuery(name="TranTypeMst.findAll",query="Select c from TranTypeMst c")
//public class TranTypeMst implements Serializable{
//     private static final long serialVersionUID = 1L;
//	
//     @Id
//    @Column(name="ttm_id")
//	private long ttmId;
//	private String tran_desc;
//    private String tran_type;
////    private String type;
//    private String remarks;
//    private long siteId;
//    
//    public long getTtmId() {
//		return ttmId;
//	}
//	public void setTtmId(long ttmId) {
//		this.ttmId = ttmId;
//	}
//	public String getTranDesc() {
//		return tran_desc;
//	}
//	public void setTranDesc(String tranDesc) {
//		this.tran_desc = tranDesc;
//	}
//	public String getTranType() {
//		return tran_type;
//	}
//	public void setTranType(String tranType) {
//		this.tran_type = tranType;
//	}
////	public String getType() {
////		return type;
////	}
////	public void setType(String type) {
////		this.type = type;
////	}
//	public String getRemarks() {
//		return remarks;
//	}
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//	public long getSiteId() {
//		return siteId;
//	}
//	public void setSiteId(long siteId) {
//		this.siteId = siteId;
//	}
//
//}
