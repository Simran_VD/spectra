package org.mysys.model;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;

import org.mysys.constant.APPConstant;
import org.mysys.listeners.MysysEntityListener;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;

@MappedSuperclass
@EntityListeners({MysysEntityListener.class,AuditingEntityListener.class})
public abstract class AcctYearAware<U>{
	
	@Column(updatable=false)
	protected String acctyear;
	
	@CreatedBy
    @Column(updatable=false)
    protected U createdby;

    @CreatedDate
    @Temporal(TIMESTAMP)
    @Column(updatable=false)
    protected Date createddt;

    @LastModifiedBy
    protected U modifiedby;

    @LastModifiedDate
    @Temporal(TIMESTAMP)
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT)
    protected Date modifieddt;

	public U getCreatedby() {
		return createdby;
	}

	public void setCreatedby(U createdby) {
		this.createdby = createdby;
	}

	public Date getCreateddt() {
		return createddt;
	}

	public void setCreateddt(Date createddt) {
		this.createddt = createddt;
	}

	public U getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(U modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifieddt() {
		return modifieddt;
	}

	public void setModifieddt(Date modifieddt) {
		this.modifieddt = modifieddt;
	}

	public String getAcctyear() {
		return acctyear;
	}

	public void setAcctyear(String acctyear) {
		this.acctyear = acctyear;
	}
}
