package org.mysys.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "roleList_vw")
@NamedQuery(name = "RoleListVw.findAll", query = "select r from RoleListVw r")
public class RoleListVw implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	private long id;
	
	private String rolename;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	private String status;
	
	private long siteid;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
	
	
}
