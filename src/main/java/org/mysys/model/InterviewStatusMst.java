package org.mysys.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.mysys.model.job.JobApplicant;
import org.mysys.model.job.JobApplicantInterview;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "interview_status_mst")
@NamedQuery(name = "InterviewStatusMst.findAll", query = "select i from InterviewStatusMst i")
public class InterviewStatusMst implements Serializable{
	public static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize = 1, sequenceName = "interview_status_mst_seq", name = "interviewstatusmstseq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "interviewstatusmstseq")
	@Column(name = "ism_id")
	private long ismid;

	private String status;

	private String isActive;

	@OneToMany(mappedBy="interviewStatusMst", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SELECT)
	private List<JobApplicantInterview> jobApplicantInterview;

	public long getIsmid() {
		return ismid;
	}

	public void setIsmid(long ismid) {
		this.ismid = ismid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public List<JobApplicantInterview> getJobApplicantInterview() {
		return jobApplicantInterview;
	}

	public void setJobApplicantInterview(List<JobApplicantInterview> jobApplicantInterview) {
		this.jobApplicantInterview = jobApplicantInterview;
	}


}
