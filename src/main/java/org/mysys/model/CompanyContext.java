package org.mysys.model;

import java.util.Map;

public class CompanyContext {

	private Map<String,String> companyContext;

	public Map<String, String> getCompanyContext() {
		return companyContext;
	}

	public void setCompanyContext(Map<String, String> companyContext) {
		this.companyContext = companyContext;
	}
	
	
	
}
