//package org.mysys.model;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="dashboard_kpi_partner_wise_comm_vw")
//@NamedQuery(name="PartnerWiseCommVw.findAll",query="Select c from PartnerWiseCommVw c")
//public class PartnerWiseCommVw implements Serializable {
//	private static final long serialVersionUID = 1L;
//	
//	@Id
//	private long id;
//	
//	 public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//	
//	 private String partner;
//	
//	 private Long today;
//	 	
//	 private Long lastWeek;
//		 
//	 private Long lastMonth;
//				 
//	 private Long lastYear;
//	 
//	 private Long pmId;
//	 
//	 public Long getPmId() {
//		return pmId;
//	}
//
//	public void setPmId(Long pmId) {
//		this.pmId = pmId;
//	}
//
//	public String getPartner() {
//		return partner;
//	}
//
//	public void setPartner(String partner) {
//		this.partner = partner;
//	}
//
//	public Long getToday() {
//		return today;
//	}
//
//	public void setToday(Long today) {
//		this.today = today;
//	}
//
//	public Long getLastWeek() {
//		return lastWeek;
//	}
//
//	public void setLastWeek(Long lastWeek) {
//		this.lastWeek = lastWeek;
//	}
//
//	public Long getLastMonth() {
//		return lastMonth;
//	}
//
//	public void setLastMonth(Long lastMonth) {
//		this.lastMonth = lastMonth;
//	}
//
//	public Long getLastYear() {
//		return lastYear;
//	}
//
//	public void setLastYear(Long lastYear) {
//		this.lastYear = lastYear;
//	}
//	
//
//}
