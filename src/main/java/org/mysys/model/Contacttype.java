package org.mysys.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the contacttype database table.
 */
@Entity
@NamedQuery(name="Contacttype.findAll", query="SELECT c FROM Contacttype c")
public class Contacttype extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="contacttype_id_seq", name="contacttypeIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="contacttypeIdSeq")
	@Column(name="contacttypeid", updatable = false, nullable = false)
	private Long contacttypeid;

	private String contacttypename;

	//bi-directional many-to-one association to Contact
	@JsonIgnore
	@OneToMany(mappedBy="contacttype")
	private List<Contact> contacts;

	private long siteid;

	public Contacttype() {
	}

	public Long getContacttypeid() {
		return this.contacttypeid;
	}

	public void setContacttypeid(Long contacttypeid) {
		this.contacttypeid = contacttypeid;
	}

	public String getContacttypename() {
		return this.contacttypename;
	}

	public void setContacttypename(String contacttypename) {
		this.contacttypename = contacttypename;
	}

	public List<Contact> getContacts() {
		return this.contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public Contact addContact(Contact contact) {
		getContacts().add(contact);
		contact.setContacttype(this);

		return contact;
	}

	public Contact removeContact(Contact contact) {
		getContacts().remove(contact);
		contact.setContacttype(null);

		return contact;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}