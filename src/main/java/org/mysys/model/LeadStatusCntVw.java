package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="dashboard_kpi_lead_status_cnt_vw")
@NamedQuery(name="LeadStatusCntVw.findAll",query="Select c from LeadStatusCntVw c")
public class LeadStatusCntVw implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private long id;
	
	@Column(name="status_id")
	private String statusId;
	

	private String leadStatus;
	
	 private Long today;
	 	
	 private Long lastWeek;
	 
	 private Long lastMonth;
			 
	 private Long lastYear;
	 
	 public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public Long getToday() {
		return today;
	}

	public void setToday(Long today) {
		this.today = today;
	}

	public Long getLastWeek() {
		return lastWeek;
	}

	public void setLastWeek(Long lastWeek) {
		this.lastWeek = lastWeek;
	}

	public Long getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(Long lastMonth) {
		this.lastMonth = lastMonth;
	}

	public Long getLastYear() {
		return lastYear;
	}

	public void setLastYear(Long lastYear) {
		this.lastYear = lastYear;
	}

	 public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	
}
