package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="dashboard_setting")
@NamedQuery(name="DashboardSetting.findAll", query="SELECT d FROM DashboardSetting d")
public class DashboardSetting extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long d_id;
	
	private String value;
	
	private String isactive;
	
	private String remarks;
	
	private Long siteid;

	public Long getD_id() {
		return d_id;
	}

	public void setD_id(Long d_id) {
		this.d_id = d_id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
