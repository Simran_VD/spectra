package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the rolescreenaccess database table.
 * 
 */
@Embeddable
public class RolewidgetaccessPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long roleid;

	@Column(insertable=false, updatable=false)
	private long widgetid;

	private String operation;
	
	public RolewidgetaccessPK() {
	}
	public long getRoleid() {
		return this.roleid;
	}
	public void setRoleid(long roleid) {
		this.roleid = roleid;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((operation == null) ? 0 : operation.hashCode());
		result = prime * result + (int) (roleid ^ (roleid >>> 32));
		result = prime * result + (int) (widgetid ^ (widgetid >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RolewidgetaccessPK other = (RolewidgetaccessPK) obj;
		if (operation == null) {
			if (other.operation != null)
				return false;
		} else if (!operation.equals(other.operation))
			return false;
		if (roleid != other.roleid)
			return false;
		if (widgetid != other.widgetid)
			return false;
		return true;
	}
	public long getWidgetid() {
		return widgetid;
	}
	public void setWidgetid(long widgetid) {
		this.widgetid = widgetid;
	}
	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

}