package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the db_setting database table.
 * 
 */
@Entity
@Table(name="db_setting")
@NamedQuery(name="DbSetting.findAll", query="SELECT d FROM DbSetting d")
public class DbSetting extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	private String comments;

	private String datatype;
	
	@Id
	private String key;

	private java.math.BigDecimal siteid;

	private String value;

	public DbSetting() {
	}

		public boolean isPresent() {
		// TODO Auto-generated method stub
		return false;
	}

	public DbSetting get() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public java.math.BigDecimal getSiteid() {
		return siteid;
	}

	public void setSiteid(java.math.BigDecimal siteid) {
		this.siteid = siteid;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	
}