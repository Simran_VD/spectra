package org.mysys.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the department database table.
 */
@Entity
@NamedQuery(name="Department.findAll", query="SELECT d FROM Department d")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Department extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="department_id_seq", name="departmentIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="departmentIdSeq")
	@Column(name="deptid", updatable = false, nullable = false)
	private long deptid;

	private Boolean consumable;

	private String deptname;
	
	private String deptdesc;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT)
	@Temporal(TemporalType.DATE)
	private Date enddate;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT)
	@Temporal(TemporalType.DATE)
	private Date startdate;

	private long siteid;

	//bi-directional many-to-one association to Itemissue
	public Department() {
	}

	public long getDeptid() {
		return this.deptid;
	}

	public void setDeptid(long deptid) {
		this.deptid = deptid;
	}

	

	public Boolean isConsumable() {
		return consumable;
	}

	public void setConsumable(Boolean consumable) {
		this.consumable = consumable;
	}

	public String getDeptname() {
		return this.deptname;
	}

	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}

	public Date getEnddate() {
		return this.enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public Date getStartdate() {
		return this.startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}


	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public String getDeptdesc() {
		return deptdesc;
	}

	public void setDeptdesc(String deptdesc) {
		this.deptdesc = deptdesc;
	}
}