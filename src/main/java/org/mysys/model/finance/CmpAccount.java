package org.mysys.model.finance;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the cmp_accounts database table.
 */
@Entity
@Table(name="cmp_accounts")
@NamedQuery(name="CmpAccount.findAll", query="SELECT c FROM CmpAccount c")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CmpAccount extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="cmp_account_id_seq", name="cmpAcctIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cmpAcctIdSeq")
	@Column(name="ca_id", updatable = false, nullable = false)
	private long caId;

	private String name;

	@Column(name="account_no")
	private String accountNo;

	private String branch;

	@Column(name="ifsc_code")
	private String ifscCode;

	private Long siteid;

	public CmpAccount() {
	}

	public long getCaId() {
		return caId;
	}

	public void setCaId(long caId) {
		this.caId = caId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
}