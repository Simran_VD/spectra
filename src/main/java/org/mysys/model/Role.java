package org.mysys.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the role database table.
 */
@Entity
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "rolewidgetaccesses"})
public class Role extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="role_id_seq", name="roleIdSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="roleIdSeq")
	@Column(name="id", updatable = false, nullable = false)
	private long id;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT)
	@Temporal(TemporalType.DATE)
	private Date enddate;

	private String rolename;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT)
	@Temporal(TemporalType.DATE)
	private Date startdate;

	private String status;

	private long siteid;

	//bi-directional many-to-one association to Rolescreenaccess
	@OneToMany(mappedBy="role",fetch=FetchType.EAGER)
	private List<Rolescreenaccess> rolescreenaccesses;
	
	//bi-directional many-to-one association to Rolescreenaccess
	@OneToMany(mappedBy="role",fetch=FetchType.LAZY)
	private List<Rolewidgetaccess> rolewidgetaccesses;

	//bi-directional many-to-one association to Userrole
	@OneToMany(mappedBy="role", fetch=FetchType.LAZY)
	@JsonIgnore
	@Column(insertable=false,updatable = false)
	private List<Userrole> userroles;

	public Role() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getEnddate() {
		return this.enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public String getRolename() {
		return this.rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public Date getStartdate() {
		return this.startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Rolescreenaccess> getRolescreenaccesses() {
		return this.rolescreenaccesses;
	}
	
	public List<Rolewidgetaccess> getRolewidgetaccesses() {
		return rolewidgetaccesses;
	}

	public void setRolewidgetaccesses(List<Rolewidgetaccess> rolewidgetaccesses) {
		this.rolewidgetaccesses = rolewidgetaccesses;
	}



	public void setRolescreenaccesses(List<Rolescreenaccess> rolescreenaccesses) {
		this.rolescreenaccesses = rolescreenaccesses;
	}

	public Rolescreenaccess addRolescreenaccess(Rolescreenaccess rolescreenaccess) {
		getRolescreenaccesses().add(rolescreenaccess);
		rolescreenaccess.setRole(this);

		return rolescreenaccess;
	}

	public Rolescreenaccess removeRolescreenaccess(Rolescreenaccess rolescreenaccess) {
		getRolescreenaccesses().remove(rolescreenaccess);
		rolescreenaccess.setRole(null);

		return rolescreenaccess;
	}

	public List<Userrole> getUserroles() {
		return this.userroles;
	}

	public void setUserroles(List<Userrole> userroles) {
		this.userroles = userroles;
	}

	public Userrole addUserrole(Userrole userrole) {
		getUserroles().add(userrole);
		userrole.setRole(this);

		return userrole;
	}

	public Userrole removeUserrole(Userrole userrole) {
		getUserroles().remove(userrole);
		userrole.setRole(null);

		return userrole;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

}