package org.mysys.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the user_session_log database table.
 * 
 */

@Entity
@NamedStoredProcedureQuery(
		name = "func", 
		procedureName = "get_active_session", 
		parameters = { 
				@StoredProcedureParameter(mode = ParameterMode.INOUT, type = Integer.class, name = "pv_userid"),
				@StoredProcedureParameter(mode = ParameterMode.INOUT, type = Integer.class, name="pv_siteid"), 
		}
		)
@Table(name="user_session_log")
@NamedQuery(name="UserSessionLog.findAll", query="SELECT u FROM UserSessionLog u")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserSessionLog extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="usl_id")
	private long id;
	
	@Column(name="action_desc")
	private String actionDesc;

	@Column(name="action_id")
	private int actionId;

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_TIME_FORMAT)
	@Temporal(TemporalType.TIMESTAMP)
	private Date sdate;

	@Column(name="session_id")
	private String sessionId;

	private long siteid;

	private long userid;

	public UserSessionLog() {
	}
	
	public UserSessionLog(int actionId, String sessionId, long siteid, long userid) {
		this.actionId = actionId;
		this.sessionId = sessionId;
		this.siteid = siteid;
		this.userid = userid;
		this.sdate = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getActionDesc() {
		return this.actionDesc;
	}

	public void setActionDesc(String actionDesc) {
		this.actionDesc = actionDesc;
	}

	public int getActionId() {
		return this.actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public Date getSdate() {
		return this.sdate;
	}

	public void setSdate(Date sdate) {
		this.sdate = sdate;
	}

	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public long getSiteid() {
		return this.siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public long getUserid() {
		return this.userid;
	}

	public void setUserid(long userid) {
		this.userid = userid;
	}

}