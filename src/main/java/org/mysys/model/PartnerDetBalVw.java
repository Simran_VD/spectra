//package org.mysys.model;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="partner_det_bal_vw")
//@NamedQuery(name="PartnerDetBalVw.findAll",query="Select c from PartnerDetBalVw c")
//public class PartnerDetBalVw implements Serializable {
//	private static final long serialVersionUID = 1L;
//	
//	@Id
//	@Column(name="pm_id")
//	private long pmId;
//
//	private String name;
//	
//	@Column(name="display_name")
//	private String displayName;
//	
//	private String company;
//	
//	@Column(name="clos_bal")
//	private long closBal;
//	
//	private String address;
//	
//	private String pincode;
//	
//	private String cityName;
//	
//	private String stateName;
//	
//	private String phone;
//	
//	private String mobile;
//	
//	private String email;
//	
//	public long getPmId() {
//		return pmId;
//	}
//
//	public void setPmId(long pmId) {
//		this.pmId = pmId;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	public String getCompany() {
//		return company;
//	}
//
//	public void setCompany(String company) {
//		this.company = company;
//	}
//
//	public long getClosBal() {
//		return closBal;
//	}
//
//	public void setClosBal(long closBal) {
//		this.closBal = closBal;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public String getPincode() {
//		return pincode;
//	}
//
//	public void setPincode(String pincode) {
//		this.pincode = pincode;
//	}
//
//	public String getCityName() {
//		return cityName;
//	}
//
//	public void setCityName(String cityName) {
//		this.cityName = cityName;
//	}
//
//	public String getStateName() {
//		return stateName;
//	}
//
//	public void setStateName(String stateName) {
//		this.stateName = stateName;
//	}
//
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	public String getMobile() {
//		return mobile;
//	}
//
//	public void setMobile(String mobile) {
//		this.mobile = mobile;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//}
