package org.mysys.model.Service;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.mysys.model.Auditable;

@Entity
@Table(name="service_charges")
@NamedQuery(name="ServiceCharges.findAll",query="SELECT c FROM ServiceCharges c")
public class ServiceCharges extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	 @Id
	  @SequenceGenerator(allocationSize=1, sequenceName="service_charges_seq", name="serviceChargesSeq")
	  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="serviceChargesSeq")	 
	  @Column(name="sc_id")
	  private Long scId;
	 
	    @Column(name="prod_name")
	    private String prodName;
	    
	    @Column(name="service_type")
	    private String serviceType;
	    
	    @Column(name="service_cost")
	    private Long serviceCost;
	    
	    @Column(name="lead_cost")
	    private Long leadCost;
	    
		private String remarks;
	  	
	  	private String isActive;	

		private Long siteId;
	  	
	  	
	  	
	  	private Long userId;
	  	
		public Long getScId() {
			return scId;
		}

		public void setScId(Long scId) {
			this.scId = scId;
		}

		public String getProdName() {
			return prodName;
		}

		public void setProdName(String prodName) {
			this.prodName = prodName;
		}

		public String getServiceType() {
			return serviceType;
		}

		public void setServiceType(String serviceType) {
			this.serviceType = serviceType;
		}

		public Long getServiceCost() {
			return serviceCost;
		}

		public void setServiceCost(Long serviceCost) {
			this.serviceCost = serviceCost;
		}

		public Long getLeadCost() {
			return leadCost;
		}

		public void setLeadCost(Long leadCost) {
			this.leadCost = leadCost;
		}

		public String getRemarks() {
			return remarks;
		}

		public void setRemarks(String remarks) {
			this.remarks = remarks;
		}

		public String getIsActive() {
			return isActive;
		}

		public void setIsActive(String isActive) {
			this.isActive = isActive;
		}

		public Long getSiteId() {
			return siteId;
		}

		public void setSiteId(Long siteId) {
			this.siteId = siteId;
		}

		

		public Long getUserId() {
			return userId;
		}

		public void setUserId(Long userId) {
			this.userId = userId;
		}

}
