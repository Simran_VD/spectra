package org.mysys.model.Service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="serv_lead_vw")
@NamedQuery(name="ServiceLeadVw.findAll",query="Select c from ServiceLeadVw c")
public class ServiceLeadVw implements Serializable {
	private static final long serialVersionUID =1l;

	@Id
	@Column(name="sl_id")
	private long slId;

	@Column(name="sm_id")
	private Long smId;
	
	@Column(name="am_id")
	private Long amId;
	
	@Column(name="prd_id")
	private Long prdId;
	
	@Column(name="psm_id")
	private Long psmId;

	@Column(name="cs_id")
	private Long csId;
	
	@Column(name="lsm_id")
	private Long lsmId;

	@Column(name="status_id")
	private Long statusId;

	private String name;
	
	@Column(name="display_name")
	private String displayName;
	
	private String address;
	private String landmark;
	private String city;
	private String state;
	
	@Column(name="area_name")
	private String areaName;
	private String pincode;
	private String phone;
	private String mobile;
	private String email;
	
	@Column(name="brand_name")
	private String BrandName;
	
	
	@Column(name="prod_name")
	private String prodName;
	
	@Column(name="service_name")
	private String serviceName;
	
	@Column(name="service_time")

	private Long serviceTime;
	private String source;
	private String status;
	private Long rate;
	private String remarks;
	
	@Column(name="cust_rating")
	private String custRating;
	
	@Column(name="partner_rating")
	private String partnerRating;
	
	
	@Column(name="total_charges")
	private Long totalCharges;
	
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="lead_gen_time")
	private Date leadGenTime;
	
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="lead_notify_time")
	private Date leadNotifyTime;
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_reqd_at_time")
	private Date servReqdAtTime;
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_start_time")
	private Date servStartTime;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_complete_time")
	private Date servCompleteTime;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_closure_time")
	private Date servCloserTime;
	
	@Column(name="parent_sl_id")
	private Long parentSlId;
	
	@Column(name="partner_userid")
	private Long parentUserId;
	
	@Column(name="partner_team_userid")
	private Long parentTeamUserId;
	
	private long siteId;
	private Long userId;
	
	private Long pmId;
	
    @Column(name="pt_id")
    private Long ptId;
	
	public long getSlId() {
		return slId;
	}
	public void setSlId(long slId) {
		this.slId = slId;
	}
	public Long getSmId() {
		return smId;
	}
	public void setSmId(Long smId) {
		this.smId = smId;
	}
	public Long getAmId() {
		return amId;
	}
	public void setAmId(Long amId) {
		this.amId = amId;
	}
	public Long getPrdId() {
		return prdId;
	}
	public void setPrdId(Long prdId) {
		this.prdId = prdId;
	}
	public Long getPsmId() {
		return psmId;
	}
	public void setPsmId(Long psmId) {
		this.psmId = psmId;
	}
	public Long getCsId() {
		return csId;
	}
	public void setCsId(Long csId) {
		this.csId = csId;
	}
	public Long getLsmId() {
		return lsmId;
	}
	public void setLsmId(Long lsmId) {
		this.lsmId = lsmId;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public Long getServiceTime() {
		return serviceTime;
	}
	public void setServiceTime(Long serviceTime) {
		this.serviceTime = serviceTime;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getRate() {
		return rate;
	}
	public void setRate(Long rate) {
		this.rate = rate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getCustRating() {
		return custRating;
	}
	public void setCustRating(String custRating) {
		this.custRating = custRating;
	}
	public String getPartnerRating() {
		return partnerRating;
	}
	public void setPartnerRating(String partnerRating) {
		this.partnerRating = partnerRating;
	}
	public Long getTotalCharges() {
		return totalCharges;
	}
	public void setTotalCharges(Long totalCharges) {
		this.totalCharges = totalCharges;
	}
	public Date getLeadGenTime() {
		return leadGenTime;
	}
	public void setLeadGenTime(Date leadGenTime) {
		this.leadGenTime = leadGenTime;
	}
	public Date getLeadNotifyTime() {
		return leadNotifyTime;
	}
	public void setLeadNotifyTime(Date leadNotifyTime) {
		this.leadNotifyTime = leadNotifyTime;
	}
	public Date getServReqdAtTime() {
		return servReqdAtTime;
	}
	public void setServReqdAtTime(Date servReqdAtTime) {
		this.servReqdAtTime = servReqdAtTime;
	}
	public Date getServStartTime() {
		return servStartTime;
	}
	public void setServStartTime(Date servStartTime) {
		this.servStartTime = servStartTime;
	}
	public Date getServCompleteTime() {
		return servCompleteTime;
	}
	public void setServCompleteTime(Date servCompleteTime) {
		this.servCompleteTime = servCompleteTime;
	}
	public Date getServCloserTime() {
		return servCloserTime;
	}
	public void setServCloserTime(Date servCloserTime) {
		this.servCloserTime = servCloserTime;
	}
	public Long getParentSlId() {
		return parentSlId;
	}
	public void setParentSlId(Long parentSlId) {
		this.parentSlId = parentSlId;
	}
	public long getSiteId() {
		return siteId;
	}
	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getParentUserId() {
		return parentUserId;
	}
	public void setParentUserId(Long parentUserId) {
		this.parentUserId = parentUserId;
	}
	public Long getParentTeamUserId() {
		return parentTeamUserId;
	}
	public void setParentTeamUserId(Long parentTeamUserId) {
		this.parentTeamUserId = parentTeamUserId;
	}
	public Long getPmId() {
		return pmId;
	}
	public void setPmId(Long pmId) {
		this.pmId = pmId;
	}
	public Long getPtId() {
		return ptId;
	}
	public void setPtId(Long ptId) {
		this.ptId = ptId;
	}
		public String getBrandName() {
			return BrandName;
		}
		public void setBrandName(String brandName) {
			BrandName = brandName;
		}
}
