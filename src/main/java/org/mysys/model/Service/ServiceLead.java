package org.mysys.model.Service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="serv_lead")
@NamedQuery(name="ServiceLead.findAll",query="Select c from ServiceLead c")
public class ServiceLead extends Auditable<String> implements Serializable {
	private static final long serialVersionUID =1l;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="serv_lead_seq", name="partnerSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="partnerSeq")
	@Column(name="sl_id",nullable = false,updatable = false)
	private long slId;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="lead_gen_time")
	private Date leadGenTime;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="lead_notify_time")
	private Date leadNotifyTime;

	@Column(name="lsm_id")
	private long lsmId;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_reqd_at_time")
	private Date servReqdAtTime;

	@Column(name="cs_id")
	private Long csId;
	
	private String name;

	@Column(name="display_name")
	private String displayName;

	private String address;

	private String landmark;

	private String city;
	
	private Long cityId;
	private String state;
	
    private Long stateId;
	private String pinCode;

	private String phone;

	private String mobile;

	private String email;

	private String status;

	private String remarks;

	@Column(name="am_id")
	private Long amId;

	@Column(name="psm_id")
	private Long psmId;

	@Column(name="prd_id")
	private Long prdId;

	@Column(name="sm_id")
	private Long smId;
	
	@Column(name="pm_id")
	private Long pmId;
	
	@Column(name="pbm_id")
	private Long pbmId;

	private BigDecimal rate;

	@Column(name="cust_rating")
    private String 	custRating;
	
	@Column(name="partner_rating")
    private String 	partnerRating;
	
	@Column(name="total_charges")
	private BigDecimal totalChargs;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_start_time")
	private Date servStartTime;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_complete_time")
	private Date servCompleteTime;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_closure_time")
	private Date servCloserTime;
	
	@Column(name="parent_sl_id")
	private Long parentSlId;
	
	private long siteid;
	
	private long service_time;
	
	private String  customerName;
	
	@Column(name="ls_id")
	private long lsId;
	
	@Column(name="pt_id")
	private Long ptId;

	public long getSlId() {
		return slId;
	}

	public void setSlId(long slId) {
		this.slId = slId;
	}

	public Date getLeadGenTime() {
		return leadGenTime;
	}

	public void setLeadGenTime(Date leadGenTime) {
		this.leadGenTime = leadGenTime;
	}

	public Date getLeadNotifyTime() {
		return leadNotifyTime;
	}

	public void setLeadNotifyTime(Date leadNotifyTime) {
		this.leadNotifyTime = leadNotifyTime;
	}

	public long getLsmId() {
		return lsmId;
	}

	public void setLsmId(long lsmId) {
		this.lsmId = lsmId;
	}

	public Date getServReqdAtTime() {
		return servReqdAtTime;
	}

	public void setServReqdAtTime(Date servReqdAtTime) {
		this.servReqdAtTime = servReqdAtTime;
	}

	public Long getCsId() {
		return csId;
	}

	public void setCsId(Long csId) {
		this.csId = csId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getAmId() {
		return amId;
	}

	public void setAmId(Long amId) {
		this.amId = amId;
	}

	public Long getPsmId() {
		return psmId;
	}

	public void setPsmId(Long psmId) {
		this.psmId = psmId;
	}

	public Long getPrdId() {
		return prdId;
	}

	public void setPrdId(Long prdId) {
		this.prdId = prdId;
	}

	public Long getSmId() {
		return smId;
	}

	public void setSmId(Long smId) {
		this.smId = smId;
	}

	public Long getPmId() {
		return pmId;
	}

	public void setPmId(Long pmId) {
		this.pmId = pmId;
	}

	public Long getPbmId() {
		return pbmId;
	}

	public void setPbmId(Long pbmId) {
		this.pbmId = pbmId;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getCustRating() {
		return custRating;
	}

	public void setCustRating(String custRating) {
		this.custRating = custRating;
	}

	public String getPartnerRating() {
		return partnerRating;
	}

	public void setPartnerRating(String partnerRating) {
		this.partnerRating = partnerRating;
	}

	public BigDecimal getTotalChargs() {
		return totalChargs;
	}

	public void setTotalChargs(BigDecimal totalChargs) {
		this.totalChargs = totalChargs;
	}

	public Date getServStartTime() {
		return servStartTime;
	}

	public void setServStartTime(Date servStartTime) {
		this.servStartTime = servStartTime;
	}

	public Date getServCompleteTime() {
		return servCompleteTime;
	}

	public void setServCompleteTime(Date servCompleteTime) {
		this.servCompleteTime = servCompleteTime;
	}

	public Date getServCloserTime() {
		return servCloserTime;
	}

	public void setServCloserTime(Date servCloserTime) {
		this.servCloserTime = servCloserTime;
	}

	public Long getParentSlId() {
		return parentSlId;
	}

	public void setParentSlId(Long parentSlId) {
		this.parentSlId = parentSlId;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public long getService_time() {
		return service_time;
	}

	public void setService_time(long service_time) {
		this.service_time = service_time;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getLsId() {
		return lsId;
	}

	public void setLsId(long lsId) {
		this.lsId = lsId;
	}

	public Long getPtId() {
		return ptId;
	}

	public void setPtId(Long ptId) {
		this.ptId = ptId;
	}
	
	
}
