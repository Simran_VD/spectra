package org.mysys.model.Service;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="partner_lead_map_vw")
@NamedQuery(name="ServliceLeadMapVw.findAll",query="Select c from ServliceLeadMapVw c")
public class ServliceLeadMapVw implements Serializable {
	private static final long serialVersionUID =1l;

	@Id
	@Column(name="plm_id")
	private long plmId;
	
	@Column(name="sl_id")
	private long slId;
	
	@Column(name="cs_id")
	private long csId;
	
	private String customerName;
	
	@Column(name="pm_id")
	private long pmId;
	
	private String name;
	
	@Column(name="display_name")
	private String displayName;
	
	private String company;
	
	private String address;
	
	private String landmark;
	
	private String phone;
	
	private String mobile;
	
	private String email;
	
	private String city;
	
	private String state;
	
	@Column(name="total_charges")
	private Long totalCharge;
	
	@Column(name="comm_ded")
	private Long commded;
	
	private String status;
	
	private long siteid;

	public long getPlmId() {
		return plmId;
	}

	public void setPlmId(long plmId) {
		this.plmId = plmId;
	}

	public long getSlId() {
		return slId;
	}

	public void setSlId(long slId) {
		this.slId = slId;
	}

	public long getCsId() {
		return csId;
	}

	public void setCsId(long csId) {
		this.csId = csId;
	}

	public long getPmId() {
		return pmId;
	}

	public void setPmId(long pmId) {
		this.pmId = pmId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(Long totalCharge) {
		this.totalCharge = totalCharge;
	}

	public Long getCommded() {
		return commded;
	}

	public void setCommded(Long commded) {
		this.commded = commded;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
}
