package org.mysys.model;

public class DropdownVO {
	private String key;
	private String displayValue;
	
	
	public DropdownVO(String key, String displayValue) {
		super();
		this.key = key;
		this.displayValue = displayValue;
	}
	
	/*
	 * public DropdownVO(Itemmaster item) { super(); this.key =
	 * Long.toString(item.getItemid()); this.displayValue = item.getItemcode(); }
	 */
	
	public DropdownVO(Contact contact) {
		super();
		this.key = Long.toString(contact.getContactid());
		this.displayValue = contact.getDisplayname();
	}
	
	public DropdownVO(Designation designation) {
		super();
		this.key = Long.toString(designation.getDesignationid());
		this.displayValue = designation.getDesignationName();
	}
	
	public DropdownVO(Skill skill) {
		super();
		this.key = Long.toString(skill.getId());
		this.displayValue = skill.getName();
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getDisplayValue() {
		return displayValue;
	}
	public DropdownVO() {
		super();
	}
	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}
	
}
