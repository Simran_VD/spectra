package org.mysys.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="fnc_kpi_job_opening_dtl")
public class DashboardKPIServiceVw{

	@Id
	private Long id;
	
	private Long jm_id;
	
	
	private String message;

	
	private Long total_matching;
	
	private Long matching_applicants;
	
	

	public Long getTotal_matching() {
		return total_matching;
	}

	public void setTotal_matching(Long total_matching) {
		this.total_matching = total_matching;
	}

	public Long getMatching_applicants() {
		return matching_applicants;
	}

	public void setMatching_applicants(Long matching_applicants) {
		this.matching_applicants = matching_applicants;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	


	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	public Long getJm_id() {
		return jm_id;
	}

	public void setJm_id(Long jm_id) {
		this.jm_id = jm_id;
	}

	

	
}
