package org.mysys.model.notification;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="notification_vw")
@NamedQuery(name="NotifyListVw.findAll",query="Select c from NotifyListVw c")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class NotifyListVw implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="nt_id")
    private long ntId;
    
    @Column(name="ns_id")
    private long nsId;
    
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="notify_dt")
	private Date notifyDate;
	
	@Column(name="sender_user_id")
	private Long senduserId;
	
	private String message;
	
//	@Column(name="status_name")
//	private String statusName;

	private String priority;
	
	@Column(name="receiver_user_id")
	private Long recUserId;
	
	@Column(name="receiver_group_id")
	private Long recGrpId;
	
	@Column(name="is_universal")
	private String isUniversal;
	
	@Column(name="page_link")
	private String pageLink;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="notified_dt")
	private Date notifiedDt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="bell_dt")
	private Date bellDt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="seen_dt")
	private Date seenDt;

	private long siteid;
	
//	private Long slId;

	@Column(name="notification_type")
	private String ntType;
	
//	@Column(name="lead_type")
//	private String leadType;
	
//    private String areaName;
    
//    private String token;
    
//    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
//	@Temporal(TemporalType.TIMESTAMP)
//    @Column(name="mobile_upd_dt")
//    private Date mobileUpdDt;
    
//    private String mobileMessage;
    
//    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
// 	@Temporal(TemporalType.TIMESTAMP)
// 	@Column(name="mobile_seen_dt")
// 	private Date mobieSeenDt;
    
//    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
// 	@Temporal(TemporalType.TIMESTAMP)
// 	@Column(name="serv_reqd_at_time")
// 	private Date servRequiredAtTime;
    
// 	@Column(name="prod_name")
//    private String productName;
    
// 	@Column(name="service_name")
//    private String serviceName;
    

	public long getNtId() {
		return ntId;
	}

	public void setNtId(long ntId) {
		this.ntId = ntId;
	}

	public long getNsId() {
		return nsId;
	}

	public void setNsId(long nsId) {
		this.nsId = nsId;
	}

	public Date getNotifyDate() {
		return notifyDate;
	}

	public void setNotifyDate(Date notifyDate) {
		this.notifyDate = notifyDate;
	}

	public Long getSenduserId() {
		return senduserId;
	}

	public void setSenduserId(Long senduserId) {
		this.senduserId = senduserId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

//	public String getStatusName() {
//		return statusName;
//	}
//
//	public void setStatusName(String statusName) {
//		this.statusName = statusName;
//	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Long getRecUserId() {
		return recUserId;
	}

	public void setRecUserId(Long recUserId) {
		this.recUserId = recUserId;
	}

	public Long getRecGrpId() {
		return recGrpId;
	}

	public void setRecGrpId(Long recGrpId) {
		this.recGrpId = recGrpId;
	}

	public String getIsUniversal() {
		return isUniversal;
	}

	public void setIsUniversal(String isUniversal) {
		this.isUniversal = isUniversal;
	}

	public String getPageLink() {
		return pageLink;
	}

	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	public Date getNotifiedDt() {
		return notifiedDt;
	}

	public void setNotifiedDt(Date notifiedDt) {
		this.notifiedDt = notifiedDt;
	}

	public Date getBellDt() {
		return bellDt;
	}

	public void setBellDt(Date bellDt) {
		this.bellDt = bellDt;
	}

	public Date getSeenDt() {
		return seenDt;
	}

	public void setSeenDt(Date seenDt) {
		this.seenDt = seenDt;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public String getNtType() {
		return ntType;
	}

	public void setNtType(String ntType) {
		this.ntType = ntType;
	}

//	public String getLeadType() {
//		return leadType;
//	}
//
//	public void setLeadType(String leadType) {
//		this.leadType = leadType;
//	}
//	
//	public Long getSlId() {
//		return slId;
//	}
//
//	public void setSlId(Long slId) {
//		this.slId = slId;
//	}

//	public String getAreaName() {
//		return areaName;
//	}
//
//	public void setAreaName(String areaName) {
//		this.areaName = areaName;
//	}

//	public String getToken() {
//		return token;
//	}
//
//	public void setToken(String token) {
//		this.token = token;
//	}
//
//	public Date getMobileUpdDt() {
//		return mobileUpdDt;
//	}
//
//	public void setMobileUpdDt(Date mobileUpdDt) {
//		this.mobileUpdDt = mobileUpdDt;
//	}
//
//	public String getMobileMessage() {
//		return mobileMessage;
//	}
//
//	public void setMobileMessage(String mobileMessage) {
//		this.mobileMessage = mobileMessage;
//	}
//
//	public Date getMobieSeenDt() {
//		return mobieSeenDt;
//	}
//
//	public void setMobieSeenDt(Date mobieSeenDt) {
//		this.mobieSeenDt = mobieSeenDt;
//	}

//	public Date getServRequiredAtTime() {
//		return servRequiredAtTime;
//	}
//
//	public void setServRequiredAtTime(Date servRequiredAtTime) {
//		this.servRequiredAtTime = servRequiredAtTime;
//	}

//	public String getProductName() {
//		return productName;
//	}
//
//	public void setProductName(String productName) {
//		this.productName = productName;
//	}
//
//	public String getServiceName() {
//		return serviceName;
//	}
//
//	public void setServiceName(String serviceName) {
//		this.serviceName = serviceName;
//	}
	
	
	
	
}
