package org.mysys.model.notification;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="notification")
@NamedQuery(name="Notification1.findAll",query="Select c from Notification1 c")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Notification1 implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
	@SequenceGenerator(allocationSize=1, sequenceName="notification_seq", name="notificationseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="notificationseq")
    @Column(name="nt_id")
    private long ntId;
    
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="notify_dt")
	private Date notifyDate;
	
	@Column(name="receiver_user_id")
	private Long recUserId;
	
	@Column(name="sender_user_id")
	private Long senderUserId;
	
	@Column(name="receiver_group_id")
	private Long senderGroupId;
	
	@Column(name="message")
	private String message;
	
	@Column(name="ns_id")
	private Long nsId;
	
	@Column(name="page_link")
	private String pageLink;
	
	@Column(name="is_universal")
	private String isUniversal;
	
	@Column(name="priority")
	private Long priority;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="notified_dt")
	private Date notifiedDt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="bell_dt")
	private Date bellDt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="seen_dt")
	private Date seenDt;
	
	@Column(name="notification_type")
	private String notyType;
	
	@Column(name="lead_type")
	private String leadType;

	private long siteid;
	
	private Long slId;
	
	protected String createdby;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createddt;

	protected String modifiedby;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	protected Date modifieddt;
	
	private Long amId;

	public long getNtId() {
		return ntId;
	}

	public void setNtId(long ntId) {
		this.ntId = ntId;
	}

	public Date getNotifyDate() {
		return notifyDate;
	}

	public void setNotifyDate(Date notifyDate) {
		this.notifyDate = notifyDate;
	}

	public Long getRecUserId() {
		return recUserId;
	}

	public void setRecUserId(Long recUserId) {
		this.recUserId = recUserId;
	}

	public Long getSenderUserId() {
		return senderUserId;
	}

	public void setSenderUserId(Long senderUserId) {
		this.senderUserId = senderUserId;
	}

	public Long getSenderGroupId() {
		return senderGroupId;
	}

	public void setSenderGroupId(Long senderGroupId) {
		this.senderGroupId = senderGroupId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getNsId() {
		return nsId;
	}

	public void setNsId(Long nsId) {
		this.nsId = nsId;
	}

	public String getPageLink() {
		return pageLink;
	}

	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	public String getIsUniversal() {
		return isUniversal;
	}

	public void setIsUniversal(String isUniversal) {
		this.isUniversal = isUniversal;
	}

	public Long getPriority() {
		return priority;
	}

	public void setPriority(Long priority) {
		this.priority = priority;
	}

	public Date getNotifiedDt() {
		return notifiedDt;
	}

	public void setNotifiedDt(Date notifiedDt) {
		this.notifiedDt = notifiedDt;
	}

	public Date getBellDt() {
		return bellDt;
	}

	public void setBellDt(Date bellDt) {
		this.bellDt = bellDt;
	}

	public Date getSeenDt() {
		return seenDt;
	}

	public void setSeenDt(Date seenDt) {
		this.seenDt = seenDt;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public String getNotyType() {
		return notyType;
	}

	public void setNotyType(String notyType) {
		this.notyType = notyType;
	}

	public String getLeadType() {
		return leadType;
	}

	public void setLeadType(String leadType) {
		this.leadType = leadType;
	}

	public Long getSlId() {
		return slId;
	}

	public void setSlId(Long slId) {
		this.slId = slId;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getCreateddt() {
		return createddt;
	}

	public void setCreateddt(Date createddt) {
		this.createddt = createddt;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifieddt() {
		return modifieddt;
	}

	public void setModifieddt(Date modifieddt) {
		this.modifieddt = modifieddt;
	}

	public Long getAmId() {
		return amId;
	}

	public void setAmId(Long amId) {
		this.amId = amId;
	}
	
}
