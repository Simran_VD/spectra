package org.mysys.model.notification;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="SERV_LEAD_NOTIFY_VW")
@NamedQuery(name="ServiceLeadNotifyVw.findAll",query="Select c from ServiceLeadNotifyVw c")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ServiceLeadNotifyVw implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="sln_id")
    private long slnId;
    
    @Column(name="sl_id")
    private long slid;
    
    @Column(name="pm_id")
    private long pmId;
    
    private String name; 
    
    @Column(name="display_name")
    private String displayName;
    
    private String company; 
        
    @Column(name="am_id")
    private long amId;
    
    private String address;
    
    private String landmark;
    
    private String city;
    private String state;
    
    private String phone;

    private String mobile;
    
    private String email;
    
    private String status;
  
   	@Column(name="contact_person")
    private String contactPerson;
   	
   	@Column(name="contact_person_mobile")
    private String contactPersonMobile;
   	
   	@Column(name="contact_person_email")
    private String contactPersonEmail;
   	
	@Column(name="area_name")
    private String areaName;
   	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="notify_time")
	private Date notifyTime;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="lead_accepted_time")
	private Date leadAccTime;
   	
   	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="lead_generation_time")
	private Date leadGenTime;

	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="lead_notify_time")
	private Date leadNotifyTime;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_reqd_at_time")
	private Date servReqAtTime;
	
	private BigDecimal rate;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_start_time")
	private Date servStartTime;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_complete_time")
	private Date servCompTime;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_TIME_FORMAT_HM,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serv_closure_time")
	private Date servClosureTime;
	
	@Column(name="partner_rating")
    private String partRating;

	@Column(name="total_charges")
	private BigDecimal totalCharges;
	
 	private long siteid;
 	
 	private long userId;

	public long getSlnId() {
		return slnId;
	}

	public void setSlnId(long slnId) {
		this.slnId = slnId;
	}

	public long getSlid() {
		return slid;
	}

	public void setSlid(long slid) {
		this.slid = slid;
	}

	public long getPmId() {
		return pmId;
	}

	public void setPmId(long pmId) {
		this.pmId = pmId;
	}

	public long getAmId() {
		return amId;
	}

	public void setAmId(long amId) {
		this.amId = amId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactPersonMobile() {
		return contactPersonMobile;
	}

	public void setContactPersonMobile(String contactPersonMobile) {
		this.contactPersonMobile = contactPersonMobile;
	}

	public String getContactPersonEmail() {
		return contactPersonEmail;
	}

	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Date getNotifyTime() {
		return notifyTime;
	}

	public void setNotifyTime(Date notifyTime) {
		this.notifyTime = notifyTime;
	}

	public Date getLeadAccTime() {
		return leadAccTime;
	}

	public void setLeadAccTime(Date leadAccTime) {
		this.leadAccTime = leadAccTime;
	}

	public Date getLeadGenTime() {
		return leadGenTime;
	}

	public void setLeadGenTime(Date leadGenTime) {
		this.leadGenTime = leadGenTime;
	}

	public Date getLeadNotifyTime() {
		return leadNotifyTime;
	}

	public void setLeadNotifyTime(Date leadNotifyTime) {
		this.leadNotifyTime = leadNotifyTime;
	}

	public Date getServReqAtTime() {
		return servReqAtTime;
	}

	public void setServReqAtTime(Date servReqAtTime) {
		this.servReqAtTime = servReqAtTime;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Date getServStartTime() {
		return servStartTime;
	}

	public void setServStartTime(Date servStartTime) {
		this.servStartTime = servStartTime;
	}

	public Date getServCompTime() {
		return servCompTime;
	}

	public void setServCompTime(Date servCompTime) {
		this.servCompTime = servCompTime;
	}

	public Date getServClosureTime() {
		return servClosureTime;
	}

	public void setServClosureTime(Date servClosureTime) {
		this.servClosureTime = servClosureTime;
	}

	public String getPartRating() {
		return partRating;
	}

	public void setPartRating(String partRating) {
		this.partRating = partRating;
	}

	public BigDecimal getTotalCharges() {
		return totalCharges;
	}

	public void setTotalCharges(BigDecimal totalCharges) {
		this.totalCharges = totalCharges;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
   	
	
}
