package org.mysys.model.notification;

import java.util.Map;

import org.osgi.service.component.annotations.Component;

@Component
public class Note {

	  private String subject;
	    private String content;
	    private Map<String, String> data;
	    private String image;
	    private String token;
		public String getSubject() {
			return subject;
		}
		public void setSubject(String subject) {
			this.subject = subject;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		public Map<String, String> getData() {
			return data;
		}
		public void setData(Map<String, String> data) {
			this.data = data;
		}
		public String getImage() {
			return image;
		}
		public void setImage(String image) {
			this.image = image;
		}
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		public Note(String subject, String content, Map<String, String> data, String image, String token) {
			super();
			this.subject = subject;
			this.content = content;
			this.data = data;
			this.image = image;
			this.token = token;
		}
}
