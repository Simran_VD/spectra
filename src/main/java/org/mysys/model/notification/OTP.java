package org.mysys.model.notification;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="otp_mobile")
@NamedQuery(name="OTP.findAll",query="Select c from OTP c")
public class OTP implements Serializable{
	private static final long serialVersionUID =1l;
	
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="otp_mobile_seq", name="otpmobileseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="otpmobileseq")
	@Column(name="otp_id",nullable = false,updatable = false)
	private long otpId;
	
	private String phone;
	
	
	private String otp;

	public long getOtpId() {
		return otpId;
	}

	public void setOtpId(long otpId) {
		this.otpId = otpId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

}
