package org.mysys.model.notification;

import org.osgi.service.component.annotations.Component;

@Component
public class Emp {
	
	private long id;
	
	private Long count;
	
	private String status;
	
	private Long param;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getParam() {
		return param;
	}

	public void setParam(Long param) {
		this.param = param;
	}


}
