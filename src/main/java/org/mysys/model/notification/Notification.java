package org.mysys.model.notification;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.mysys.constant.APPConstant;
import org.mysys.model.Auditable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="notification")
@NamedQuery(name="Notification.findAll",query="Select c from Notification c")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Notification extends Auditable<String> implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
	@SequenceGenerator(allocationSize=1, sequenceName="notification_seq", name="notificationseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="notificationseq")
    @Column(name="nt_id")
    private long ntId;
    
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="notify_dt")
	private Date notifyDate;
	
	@Column(name="receiver_user_id")
	private Long recUserId;
	
	@Column(name="sender_user_id")
	private Long senderUserId;
	
	@Column(name="receiver_group_id")
	private Long senderGroupId;
	
	@Column(name="message")
	private String message;
	
	@Column(name="ns_id")
	private Long nsId;
	
	@Column(name="page_link")
	private String pageLink;
	
	@Column(name="is_universal")
	private String isUniversal;
	
	@Column(name="priority")
	private Long priority;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="notified_dt")
	private Date notifiedDt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="bell_dt")
	private Date bellDt;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="seen_dt")
	private Date seenDt;
	
	@Column(name="notification_type")
	private String notyType;
	
	@Column(name="lead_type")
	private String leadType;

	private long siteid;
	
	private Long slId;
	
	private Long amId;
	
    private String token;
    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="mobile_upd_dt")
    private Date mobileUpdDt;
    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern=APPConstant.DATE_FORMAT,timezone=APPConstant.TIMEZONE)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="mobile_seen_dt")
	private Date mobieSeenDt;

	public long getNtId() {
		return ntId;
	}

	public void setNtId(long ntId) {
		this.ntId = ntId;
	}

	public Date getNotifyDate() {
		return notifyDate;
	}

	public void setNotifyDate(Date notifyDate) {
		this.notifyDate = notifyDate;
	}

	public Long getRecUserId() {
		return recUserId;
	}

	public void setRecUserId(Long recUserId) {
		this.recUserId = recUserId;
	}

	public Long getSenderUserId() {
		return senderUserId;
	}

	public void setSenderUserId(Long senderUserId) {
		this.senderUserId = senderUserId;
	}

	public Long getSenderGroupId() {
		return senderGroupId;
	}

	public void setSenderGroupId(Long senderGroupId) {
		this.senderGroupId = senderGroupId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getNsId() {
		return nsId;
	}

	public void setNsId(Long nsId) {
		this.nsId = nsId;
	}

	public String getPageLink() {
		return pageLink;
	}

	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	public String getIsUniversal() {
		return isUniversal;
	}

	public void setIsUniversal(String isUniversal) {
		this.isUniversal = isUniversal;
	}

	public Long getPriority() {
		return priority;
	}

	public void setPriority(Long priority) {
		this.priority = priority;
	}

	public Date getNotifiedDt() {
		return notifiedDt;
	}

	public void setNotifiedDt(Date notifiedDt) {
		this.notifiedDt = notifiedDt;
	}

	public Date getBellDt() {
		return bellDt;
	}

	public void setBellDt(Date bellDt) {
		this.bellDt = bellDt;
	}

	public Date getSeenDt() {
		return seenDt;
	}

	public void setSeenDt(Date seenDt) {
		this.seenDt = seenDt;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public String getNotyType() {
		return notyType;
	}

	public void setNotyType(String notyType) {
		this.notyType = notyType;
	}

	public String getLeadType() {
		return leadType;
	}

	public void setLeadType(String leadType) {
		this.leadType = leadType;
	}

	public Long getSlId() {
		return slId;
	}

	public void setSlId(Long slId) {
		this.slId = slId;
	}

	public Long getAmId() {
		return amId;
	}

	public void setAmId(Long amId) {
		this.amId = amId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getMobileUpdDt() {
		return mobileUpdDt;
	}

	public void setMobileUpdDt(Date mobileUpdDt) {
		this.mobileUpdDt = mobileUpdDt;
	}

	public Date getMobieSeenDt() {
		return mobieSeenDt;
	}

	public void setMobieSeenDt(Date mobieSeenDt) {
		this.mobieSeenDt = mobieSeenDt;
	}
	
	
}
