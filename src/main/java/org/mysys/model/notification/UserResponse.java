package org.mysys.model.notification;

public class UserResponse {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserResponse(String name) {
		this.name = name;
	}
	
	

}
