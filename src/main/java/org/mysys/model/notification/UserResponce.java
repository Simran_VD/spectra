package org.mysys.model.notification;

public class UserResponce {
	
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public UserResponce(String content) {
		super();
		this.content = content;
	}

	public UserResponce() {
		
	}
	
	

}
