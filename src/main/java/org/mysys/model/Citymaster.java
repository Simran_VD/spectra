package org.mysys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the citymaster database table.
 */
@Entity
@Table(name = "citymaster")
@NamedQuery(name="Citymaster.findAll", query="SELECT c FROM Citymaster c")
public class Citymaster extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="citymaster_seq", name="citymasterSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="citymasterSeq")
	@Column(name="cityid", updatable = false, nullable = false)
	private long cityId;

	@Column(name="cityname")
	private String cityName;
	
	private long siteid;
	
	//bi-directional many-to-one association to Site
	@JsonIgnore
	@ManyToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="stateid", insertable=false, updatable=false)	
	private Statemaster statemaster;

	private Long stateid;
	
//	@Column(name="isactive")
	private String isactive;
	
	@Column(name = "createdBy_id",insertable=true,updatable=false)
	private long createdById;
	
	@Column(name = "modifiedBy_id")
	private long modifiedById;

	
	public Citymaster() {
	}

	

	public long getCityId() {
		return cityId;
	}



	public void setCityId(long cityId) {
		this.cityId = cityId;
	}



	public String getCityName() {
		return cityName;
	}



	public void setCityName(String cityName) {
		this.cityName = cityName;
	}



	public Statemaster getStatemaster() {
		return this.statemaster;
	}

	public void setStatemaster(Statemaster statemaster) {
		this.statemaster = statemaster;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	public Long getStateid() {
		return stateid;
	}

	public void setStateid(Long stateid) {
		this.stateid = stateid;
	}


	public String getIsactive() {
		return isactive;
	}


	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}



	public long getCreatedById() {
		return createdById;
	}



	public void setCreatedById(long createdById) {
		this.createdById = createdById;
	}



	public long getModifiedById() {
		return modifiedById;
	}



	public void setModifiedById(long modifiedById) {
		this.modifiedById = modifiedById;
	}
	
	
}