package org.mysys.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the user_settings database table.
 */
@Entity
@Table(name="user_settings")
@NamedQuery(name="UserSetting.findAll", query="SELECT u FROM UserSetting u")
public class UserSetting extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserSettingPK id;

	private String value;

	public UserSetting() {
	}

	public UserSetting(UserSettingPK id, String value) {
		this.id = id;
		this.value = value;
	}
	
	public UserSettingPK getId() {
		return this.id;
	}

	public void setId(UserSettingPK id) {
		this.id = id;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}