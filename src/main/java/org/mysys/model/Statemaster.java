package org.mysys.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the statemaster database table.
 */
@Entity
@Table(name = "statemaster")
@NamedQuery(name="Statemaster.findAll", query="SELECT s FROM Statemaster s")
public class Statemaster extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="statemaster_seq", name="statemasterseq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="statemasterseq")
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="stateid", updatable = false, nullable = false)
	private long stateid;

	private String statename;
	
	private String gstcode;

	private String tin;  
	
//	@Column(name="isactive")
	private String isactive;

	//bi-directional many-to-one association to Citymaster
	@JsonIgnore
	@OneToMany(mappedBy="statemaster", fetch=FetchType.EAGER)
	private List<Citymaster> locMsts;

	private long siteid;
	
	@Column(name = "createdBy_id",insertable=true,updatable=false)
	private long createdById;
	
	@Column(name = "modifiedBy_id")
	private long modifiedById;

	public Statemaster() {
	}

	public long getStateid() {
		return this.stateid;
	}

	public void setStateid(long stateid) {
		this.stateid = stateid;
	}

	public String getGstcode() {
		return this.gstcode;
	}

	public void setGstcode(String gstcode) {
		this.gstcode = gstcode;
	}

	public String getStatename() {
		return this.statename;
	}

	public void setStatename(String statename) {
		this.statename = statename;
	}

	public String getTin() {
		return this.tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public List<Citymaster> getLocMst() {
		return this.locMsts;
	}

	public void setLocMst(List<Citymaster> locMsts) {
		this.locMsts = locMsts;
	}

	public Citymaster addLocMst(Citymaster locMst) {
		getLocMst().add(locMst);
		locMst.setStatemaster(this);

		return locMst;
	}

	public Citymaster removeLocMst(Citymaster locMst) {
		getLocMst().remove(locMst);
		locMst.setStatemaster(null);

		return locMst;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	
	public String getIsactive() {
		return isactive;
	}

	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	public long getCreatedById() {
		return createdById;
	}

	public void setCreatedById(long createdById) {
		this.createdById = createdById;
	}

	public long getModifiedById() {
		return modifiedById;
	}

	public void setModifiedById(long modifiedById) {
		this.modifiedById = modifiedById;
	}
	
	
	
}