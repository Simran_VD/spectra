package org.mysys.model;

public class SMS {
	
	private String message;
	
	private String mobile;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public SMS(String message, String mobile) {
		super();
		this.message = message;
		this.mobile = mobile;
	}

	
}
