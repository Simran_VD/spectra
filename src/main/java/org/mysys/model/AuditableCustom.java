package org.mysys.model;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;

import org.mysys.constant.APPConstant;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AuditableCustom<U> {
	
	

	    @CreatedDate
	    @Temporal(TIMESTAMP)
	    @Column(updatable=false)
	    protected Date createddt;

	    @LastModifiedDate
	    @Temporal(TIMESTAMP)
	    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern=APPConstant.DATE_FORMAT)
	    protected Date modifieddt;

		public Date getCreateddt() {
			return createddt;
		}

		public void setCreateddt(Date createddt) {
			this.createddt = createddt;
		}

		public Date getModifieddt() {
			return modifieddt;
		}

		public void setModifieddt(Date modifieddt) {
			this.modifieddt = modifieddt;
		}

	    
	    
}
