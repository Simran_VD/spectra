package org.mysys.model;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the emailtemplate database table.
 */
@Entity
@Table(name="template_list")
@NamedQuery(name="TemplateList.findAll", query="SELECT i FROM TemplateList i")
public class TemplateList extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;
 
	@Id
	@SequenceGenerator(allocationSize=1, sequenceName="template_list_seq", name="templatelistSeq")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="templatelistSeq")
	@Column(name="tl_id", updatable = false, nullable = false)
	private long tlId;
	
	@Column(name="tl_name")
	private String tempateName;
	
	@Column(name="tl_category")
	private Long templateCategory;
  
	@Column(name="tl_content_type")
	private String templateContentType;
	
	
	@Column(name="tl_subject")
	private String tempateSubject;
	
	@Column(name="tl_group")
	private String templateGroup;
	
	@Column(name="tl_data")
	private String templateData;
	
	private long siteid;

	public long getTlId() {
		return tlId;
	}

	public void setTlId(long tlId) {
		this.tlId = tlId;
	}

	public String getTempateName() {
		return tempateName;
	}

	public void setTempateName(String tempateName) {
		this.tempateName = tempateName;
	}

	public Long getTemplateCategory() {
		return templateCategory;
	}

	public void setTemplateCategory(Long templateCategory) {
		this.templateCategory = templateCategory;
	}

	public String getTemplateContentType() {
		return templateContentType;
	}

	public void setTemplateContentType(String templateContentType) {
		this.templateContentType = templateContentType;
	}

	public String getTempateSubject() {
		return tempateSubject;
	}

	public void setTempateSubject(String tempateSubject) {
		this.tempateSubject = tempateSubject;
	}

	public String getTemplateGroup() {
		return templateGroup;
	}

	public void setTemplateGroup(String templateGroup) {
		this.templateGroup = templateGroup;
	}

	public String getTemplateData() {
		return templateData;
	}

	public void setTemplateData(String templateData) {
		this.templateData = templateData;
	}

	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}
}