package org.mysys.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the userrole database table.
 * 
 */
@Entity
@Table(name="user_skill")
@NamedQuery(name="UserSkill.findAll", query="SELECT u FROM UserSkill u")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserSkill extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserSkillPk id;
	
	private long siteid;
	
	@Column(name="skill_level")
	private long skillLevel;
	
	public long getSiteid() {
		return siteid;
	}

	public void setSiteid(long siteid) {
		this.siteid = siteid;
	}

	//bi-directional many-to-one association to User
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="userid",insertable=false,updatable=false)
	private User user;

	public UserSkill() {
	}

	public UserSkillPk getId() {
		return id;
	}

	public void setId(UserSkillPk id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getSkillLevel() {
		return skillLevel;
	}

	public void setSkillLevel(long skillLevel) {
		this.skillLevel = skillLevel;
	}

}