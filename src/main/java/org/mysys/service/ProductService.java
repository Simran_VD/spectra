//package org.mysys.service;
//
//import java.math.BigDecimal;
//import java.util.List;
//
//import javax.transaction.Transactional;
//
//import org.mysys.model.PartnerDetBalVw;
//import org.mysys.model.Service.ServiceLeadVw;
//import org.mysys.model.Service.ServliceLeadMapVw;
//import org.mysys.model.partner.PartnerLedgerVw;
//import org.mysys.model.product.LeadSourceVw;
//import org.mysys.model.product.OurServices;
//import org.mysys.model.product.PartnerLedger;
//import org.mysys.model.product.PaymentModeMst;
//import org.mysys.model.product.ProductMst;
//import org.mysys.model.product.ProductMstVw;
//import org.mysys.model.product.ProductServiceMap;
//import org.mysys.model.product.ProductServiceVw;
//import org.mysys.model.product.TranTypeMst;
//import org.mysys.repository.DbSettingRepo;
//import org.mysys.repository.Service.ServliceLeadMapVwRepo;
//import org.mysys.repository.partner.PartnerMstRepo;
//import org.mysys.repository.product.LeadSourceVwRepo;
//import org.mysys.repository.product.OurServicesRepo;
//import org.mysys.repository.product.PartnerLedgerRepo;
//import org.mysys.repository.product.PaymentModeMstRepo;
//import org.mysys.repository.product.ProductMstRepo;
//import org.mysys.repository.product.ProductServiceMapRepo;
//import org.mysys.repository.product.ProductServiceVwRepo;
//import org.mysys.repository.product.TranTypeMstRepo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.DataIntegrityViolationException;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.stereotype.Service;
//
//@Service
//public class ProductService extends AbstractService {
//
//	String status="Y";
//
//	@Autowired
//	protected OurServicesRepo ourServicesRepo;
//
//	@Autowired
//	protected ProductServiceVwRepo prodServiceVwRepo;
//
//	@Autowired
//	protected LeadSourceVwRepo leadSourceRepo;
//
//	@Autowired
//	ProductServiceMapRepo prodServiceMapRepo;
//
//	@Autowired
//	ProductMstRepo productMstRepo;
//
//	@Transactional
//	public List<ProductMstVw> getProductList() {
//		return prodMstVwRepo.findByIsActive(status);
//	}
//
//
//	public List<ProductServiceVw> getProductServiceList(long prdId) {
//		String status="Y";
//		return prodServiceVwRepo.findByPrdIdAndIsActive(prdId,status);
//	}
//
//	public List<ProductServiceVw> getProductServiceList() {
//		String status="Y";
//		return prodServiceVwRepo.findByIsActive(status);
//	}
//
//	@Transactional
//	public List<LeadSourceVw> getLeadSource() {
//		return leadSourceRepo.findByUserid(getLoggedInUserUserId());
//	}
//
//	@PreAuthorize("hasRole('PRODUCT_SERVICE_MAP_ADD') or hasRole('PRODUCT_SERVICE_MAP_EDIT')")
//	@Transactional
//	public ProductServiceMap saveProdServiceMap(ProductServiceMap prodServiceMap) {
//		if(prodServiceMap.getPsmId() <= 0) {
//			prodServiceMap.setSiteId(getLoggedInUserSiteId());
//		} 
//		prodServiceMap=	prodServiceMapRepo.save(prodServiceMap);
//		return prodServiceMap;
//	}
//
//
//	@PreAuthorize("hasRole('PRODUCT_SERVICE_MAP_VIEW') or hasRole('PRODUCT_SERVICE_MAP_EDIT')")
//	@Transactional
//	public ProductServiceMap getProdServiceMap(Long psmId) {
//		return prodServiceMapRepo.findByPsmId(psmId);
//	}
//
//	@PreAuthorize("hasRole('PRODUCT_SERVICE_MAP_DELETE')")
//	@Transactional
//	public String deleteProdServiceMap(long psmId) {
//		String result="Success";
//		try {
//			prodServiceMapRepo.deleteByPsmId(psmId);
//		} catch(DataIntegrityViolationException e) {
//			result="DATA_INTEGRITY";
//		} catch (Exception e) {
//			result="DATA_INTEGRITY";
//		}
//		return result;
//	}
//
//
//	@Transactional
//	public List<OurServices> getAllServices(){
//		return ourServicesRepo.findByIsActive(status); 
//
//	}
//
//	@Transactional
//	public List<ProductServiceMap> getProductServiceMapByProductAndService(Long prdId, Long smId) {
//		return prodServiceMapRepo.findByPrdIdAndSmId(prdId,smId);
//	}
//
//
//	@PreAuthorize("hasRole('PRODUCT_MASTER_ADD') or hasRole('PRODUCT_MASTER_EDIT')")
//	@Transactional
//	public ProductMst saveProdMst(ProductMst productMst) {
//		if(productMst.getPrdId() <= 0) {
//			productMst.setSiteId(getLoggedInUserSiteId());
//		} 
//		productMst=	productMstRepo.save(productMst);
//		return productMst;
//	}
//
//	public List<ProductMst> getProductMstByProdName(String prodName) {
//		return productMstRepo.findByProdName(prodName);
//	}
//
//	@PreAuthorize("hasRole('PRODUCT_MASTER_VIEW') or hasRole('PRODUCT_MASTER_EDIT')")
//	@Transactional
//	public ProductMst getProductMst(long prdId) {	
//		return productMstRepo.findByPrdIdAndSiteId(prdId,getLoggedInUserSiteId());
//	}
//
//	@PreAuthorize("hasRole('PRODUCT_MASTER_DELETE')")
//	@Transactional
//	public String deleteProductMst(long prdId) {
//		String result="Success";
//		try {
//			productMstRepo.deleteByPrdId(prdId);
//		} catch(DataIntegrityViolationException e) {
//			result="DATA_INTEGRITY";
//		} catch (Exception e) {
//			result="DATA_INTEGRITY";
//		}
//		return result;
//	}
//
//	@Autowired
//	TranTypeMstRepo tranTypeMstRepo;
//
//	@Autowired
//	PaymentModeMstRepo paymentModeMstRepo;
//
//	@Autowired
//	PartnerLedgerRepo partnerLedgerRepo;
//
//	public List<TranTypeMst> getTranType() {
//		return tranTypeMstRepo.findAll();
//	}
//
//	public List<PaymentModeMst> getPaymentMode() {
//		return paymentModeMstRepo.findAll();
//	}
//
//
//	public PartnerLedger getPartnerLedger(long plId) {
//		return partnerLedgerRepo.findByPlId	(plId);
//	}
//
//
//	@Transactional
//	public PartnerLedger savePartnerLedger(PartnerLedger partnerLedger) {
//		if(partnerLedger.getPlId() <= 0) {
//			partnerLedger.setSiteId(getLoggedInUserSiteId());
//		} 
//
//		if(partnerLedger.getTtmId() == 1) {
//			long rate =partnerLedger.getTranAmount();
//			Long recharge=null;
//			String pmIdStateId =partnerMstRepo.findByPmId(partnerLedger.getPmId()).getState();
//			String stateid= dbSettingRepo.findById("gst_statecode").get().getValue();
//			Long gstRate= Long.valueOf(dbSettingRepo.findById("gst").get().getValue());
//
//			if(!(Long.valueOf(pmIdStateId).equals(Long.valueOf(stateid)))) {
//				Long  iGst= (rate*gstRate)/(100+gstRate);
//				recharge =rate-iGst;
//				partnerLedger.setIgst(BigDecimal.valueOf(iGst));
//				partnerLedger.setSgst(BigDecimal.valueOf(0));
//				partnerLedger.setCgst(BigDecimal.valueOf(0));
//
//				partnerLedger.setIgstPerc(Long.valueOf(gstRate));
//				partnerLedger.setSgstPerc(0l);
//				partnerLedger.setCgstPerc(0l);
//			} else {
//				long gs=gstRate/2;
//				Long gst= (rate*gstRate)/(100+gstRate);
//				recharge =rate-gst;
//				partnerLedger.setIgst(BigDecimal.valueOf(0));
//				partnerLedger.setSgst(BigDecimal.valueOf(gst/2));
//				partnerLedger.setCgst(BigDecimal.valueOf(gst/2));
//
//				partnerLedger.setIgstPerc(Long.valueOf(0));
//				partnerLedger.setSgstPerc(gs);
//				partnerLedger.setCgstPerc(gs);
//			}
//			partnerLedger.setRechargeAmt(BigDecimal.valueOf(rate));
//			partnerLedger.setTranAmount(recharge);
//		}
//
//		partnerLedger =	partnerLedgerRepo.save(partnerLedger);
//		return partnerLedger;
//	}
//
//	@Transactional
//	public String deletePartnerLedger(long plId) {
//		String result="Success";
//		try {
//			partnerLedgerRepo.deleteById(plId);
//		} catch(DataIntegrityViolationException e) {
//			result="DATA_INTEGRITY";
//		} catch (Exception e) {
//			result="DATA_INTEGRITY";
//		}
//		return result;
//	}
//
//	@Autowired
//	ServliceLeadMapVwRepo servLeadRepo;
//
//	public List<ServliceLeadMapVw> getPartnerDetails(long pmId) {
//		return servLeadRepo.findByPmId(pmId);
//	}
//
//
//	public List<ServiceLeadVw> getServiceLeadVw() {
//		return serviceLeadVwRepo.findAll();
//	}
//
//	public PartnerDetBalVw getPartnerBal(long pmId) {
//		return partDetBalRepo.findByPmId(pmId);
//	}
//
//	public List<PartnerLedgerVw> getPartnerLedgerPm(long pmId) {
//		return partLedgerRepo.findByPmId(pmId);
//	}
//}
