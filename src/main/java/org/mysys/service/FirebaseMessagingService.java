package org.mysys.service;

import java.util.ArrayList;
import java.util.List;

import org.mysys.model.notification.Note;
import org.springframework.stereotype.Service;

import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.SendResponse;

@Service
public class FirebaseMessagingService {

	private final FirebaseMessaging firebaseMessaging;

	public FirebaseMessagingService(FirebaseMessaging firebaseMessaging) {
		this.firebaseMessaging = firebaseMessaging;
	}

	public String sendNotification(Note note) throws FirebaseMessagingException {
		 Notification notification = Notification
	                .builder()
	                .setTitle(note.getSubject())
	                .setBody(note.getContent())
	                .build();

	        Message message = Message
	                .builder()
	                .setToken(note.getToken())
	                .setNotification(notification)
	                .putAllData(note.getData())
	                .build();
	        System.out.println(message.toString());
	        return firebaseMessaging.send(message);
	}

	public String sendMultiNotification(Note note, List<String> tokes) throws FirebaseMessagingException {
      String result = "";
		Notification notification = Notification
				.builder()
				.setTitle(note.getSubject())
				.setBody(note.getContent())
				.build();

		MulticastMessage messages = MulticastMessage
				.builder()
				.setNotification(notification)
				.putAllData(note.getData())
				.addAllTokens(tokes)
				.build();

		BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(messages);

		if (response.getFailureCount() > 0) {
			List<SendResponse> responses = response.getResponses();
			List<String> failedTokens = new ArrayList<>();
			for (int i = 0; i < responses.size(); i++) {
				if (!responses.get(i).isSuccessful()) {
					failedTokens.add(tokes.get(i));
				}
			}
			System.out.println(response.getSuccessCount() + " messages were sent successfully");
		} else {
			result = "Suceefully Notificatiin Sent";
		}
		return result;
	}
}
