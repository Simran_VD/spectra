package org.mysys.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.mysys.model.Module;
import org.mysys.model.ModuleComparator;
import org.mysys.model.Screen;
import org.mysys.model.ScreenComparator;
import org.mysys.model.User;
import org.mysys.repository.IGenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MenuService extends AbstractService {
	
	@Autowired
	private ScreenComparator comparator;
	
	IGenericDao<Module, Long> moduleRepo;
	
	@Autowired
	public void setModuleRepo( IGenericDao<Module,Long > daoToSet ){
		moduleRepo = daoToSet;
		moduleRepo.setClazz( Module.class );
	}
	
	public List<Module> getMenu(String loginId){
		User us = userRepo.findByLoginid(loginId);
		List<Module> modules = new ArrayList<>();
		Set<String> userScreens = new HashSet<>();
		us.getUserroles().stream().forEach((r)->{r.getRole().getRolescreenaccesses().stream().forEach((rsa)->{
			userScreens.add(rsa.getScreen().getScreenname());
		});
		});

		//moduleRepo.findAll().stream().forEach((module)->{
		moduleRepo.findAllByFieldValue("1", "active").stream().forEach((module)->{

			List<Screen> mScreen = new ArrayList<>();
			module.getModulescreens().stream().forEach((ms)->{
				if(ms.getScreen().getActive().equalsIgnoreCase("1")) {
					Screen sc =  null;
					try {
						sc = ms.getScreen().clone();
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(userScreens.contains(sc.getScreenname())){
						sc.setSeq(ms.getSeq());
						mScreen.add(sc);
					}
				}
			});
			if(mScreen.size() > 0){
				mScreen.sort(comparator);
				module.setSubModules(mScreen);
				modules.add(module);
			}
		});
		modules.sort(new ModuleComparator());
		return modules;
	}
}