package org.mysys.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.mysys.model.SMS;
import org.mysys.model.User;
import org.mysys.model.Service.ServiceLead;
import org.mysys.model.Service.ServiceLead1;
import org.mysys.model.Service.ServiceLeadVw;
//import org.mysys.model.customer.CustMst;
//import org.mysys.model.customer.CustMstVw;
//import org.mysys.model.customer.Customer;
/*import org.mysys.model.notification.OTP;
*///import org.mysys.model.partner.PartnerAcceptanceLadger;
//import org.mysys.model.partner.PartnerLeadMap;
//import org.mysys.model.partner.PartnerMst;
//import org.mysys.model.product.PartnerLedger;
//import org.mysys.model.product.PartnerLedger2;
//import org.mysys.model.product.ProductServiceVw;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.google.firebase.messaging.FirebaseMessagingException;

@Service
public class CustomerService extends AbstractService{
	public static String status = "Y";

//	@PreAuthorize("hasRole('CUSTOMER_MASTER_ADD') or hasRole('CUSTOMER_MASTER_EDIT')")
//	@Transactional
//	public CustMst saveCustMst(CustMst custMst) {
//		Long csId = custMst.getCsId();
//		if(csId <= 0) {
//			if(custMst.getSiteid()!=0) 
//				custMst.setSiteid(getLoggedInUserSiteId());
//		}
//		custMst =custMstRepo.save(custMst);
//		if (csId <= 0) 
//			adminService.saveUsers(custMst);
//
//		return custMst;
//	}

//	@Transactional
//	public Customer saveCustomer(Customer custMst) {
//		Long csId = custMst.getCsId();
//		if(csId <= 0) {
//			if(custMst.getSiteid()!=0) 
//				custMst.setSiteid(getLoggedInUserSiteId());
//		}
//		return customerRepo.save(custMst);
//	}

//	@Transactional
//	public ServiceLead1 saveCustomer(ServiceLead1 serviceLead) throws FirebaseMessagingException {
//		Customer cust=customerRepo.findByMobile(serviceLead.getMobile());
//		if (cust == null ) {
//			cust=this.saveCust1(serviceLead);
//			serviceLead.setCsId(cust.getCsId());
//		} else {
//			serviceLead.setCsId(cust.getCsId());
//			this.checkerCust(serviceLead);
//		} 
//		serviceLead.setDisplayName(serviceLead.getName());
//		serviceLead.setLeadGenTime(new Date());
//		serviceLead.setSiteid(0l);
//		serviceLead.setCreatedby("portal");
//		serviceLead.setCreateddt(new Date());
//		serviceLead.setModifiedby("portal");
//		serviceLead.setModifieddt(new Date());
//		serviceLead = serLead1Repo.save(serviceLead);
//		return serviceLead;
//	}

//	@PreAuthorize("hasRole('CUSTOMER_MASTER_VIEW') or hasRole('CUSTOMER_MASTER_EDIT')")
//	@Transactional
//	public CustMst getCustomer(Long csId) {
//		return custMstRepo.findByCsId(csId);
//	}
//
//	@PreAuthorize("hasRole('CUSTOMER_MASTER_DELETE')")
//	public String deleteCustomer(long csId) {
//		String result="Success";
//		try {
//			custMstRepo.deleteById(csId);
//		} catch(DataIntegrityViolationException e) {
//			result="DATA_INTEGRITY";
//		} catch (Exception e) {
//			result="DATA_INTEGRITY";
//		}
//		return result;
//	}

//	@Transactional
//	public List<CustMstVw> getCustmors() {
//		return custMstVwRepo.findByIsActive(status);
//	}

	/******************************* Service Lead 
	 * @throws FirebaseMessagingException *********************************/
//	@PreAuthorize("hasRole('SERVICE_LEAD_ADD') or hasRole('SERVICE_LEAD_EDIT')")
//	@Transactional
//	public ServiceLead saveServiceLead(ServiceLead servLead) throws FirebaseMessagingException {
//		long slid=servLead.getSlId();
//		if(servLead.getSlId() <= 0) {
//			servLead.setSiteid(getLoggedInUserSiteId());
//		} 
//		if(servLead.getCsId() == null)  {
//			CustMst csMst= saveCust(servLead);
//			servLead.setCsId(csMst.getCsId());
//		}
//		if(servLead.getCsId() > 0) {
//			checker(servLead);
//		}
//		servLead=servLeadRepo.save(servLead);
//
//		return servLead;
//	}

//	public CustMst saveCust(ServiceLead servLead) {
//		CustMst csMst= custMstRepo.findByMobile(servLead.getMobile());
//		if(csMst == null)  {
//			csMst= new CustMst();
//			csMst.setName(servLead.getName());
//			csMst.setDisplayName(servLead.getDisplayName());
//			csMst.setAm_id(servLead.getAmId());
//			csMst.setAddress(servLead.getAddress());
//			csMst.setLandmark(servLead.getLandmark());
//			csMst.setCity(servLead.getCityId().toString());
//			csMst.setPhone(servLead.getPhone());
//			csMst.setState(servLead.getStateId().toString());
//			csMst.setPinCode(servLead.getPinCode());
//			csMst.setMobile(servLead.getMobile());
//			csMst.setEmail(servLead.getEmail());
//			csMst.setIsActive(servLead.getStatus());
//			csMst.setRemarks(servLead.getRemarks());
//			csMst.setSiteid(servLead.getSiteid());
//			csMst=this.saveCustMst(csMst);
//		} 
//		return csMst;
//	}


//	public Customer saveCust1(ServiceLead1 servLead) {
//		Customer csMst= new Customer();
//		if(servLead.getCsId() == null)  {
//			try {
//				csMst.setName(servLead.getName());
//				csMst.setDisplayName(servLead.getName());
//				csMst.setAm_id(servLead.getAmId());
//				csMst.setAddress(servLead.getAddress());
//				csMst.setLandmark(servLead.getLandmark());
//				csMst.setCity(servLead.getCityId().toString());
//				csMst.setPhone(servLead.getPhone());
//				csMst.setState(servLead.getStateId().toString());
//				csMst.setPinCode(servLead.getPinCode());
//				csMst.setMobile(servLead.getMobile());
//				csMst.setEmail(servLead.getEmail());
//				csMst.setIsActive("Y");
//				csMst.setRemarks(servLead.getRemarks());
//				csMst.setSiteid(servLead.getSiteid());
//			} catch(Exception e) {
//			}
//		}
//		csMst=this.saveCustomer(csMst);
//		return csMst;
//	}

//	@Transactional
//	public void checker(ServiceLead servLead) {
//		try {
//			CustMst cstMst= custMstRepo.findByCsId(servLead.getCsId());
//			if(servLead.getCsId() > 0 &&
//					(!cstMst.getName().equals(servLead.getName()) 
//
//							|| !cstMst.getDisplayName().equals(servLead.getDisplayName())
//							|| !(cstMst.getAm_id() == servLead.getAmId())
//							|| !cstMst.getAddress().equals(servLead.getAddress())
//							|| !cstMst.getCity().equals(servLead.getCity())
//							|| !cstMst.getLandmark().equals(servLead.getLandmark())
//							|| !cstMst.getState().equals(servLead.getState())
//							|| !cstMst.getMobile().equals(servLead.getMobile())
//							|| !cstMst.getEmail().equals(servLead.getEmail())
//							|| !cstMst.getIsActive().equals(servLead.getStatus())
//							|| !cstMst.getRemarks().equals(servLead.getRemarks())
//
//							))  {
//				cstMst.setName(servLead.getName());
//				cstMst.setDisplayName(servLead.getDisplayName()); 
//				cstMst.setAm_id(servLead.getAmId());
//				cstMst.setAddress(servLead.getAddress());
//				cstMst.setLandmark(servLead.getLandmark());
//				cstMst.setCity(servLead.getCity());
//				cstMst.setState(servLead.getState());
//				cstMst.setPinCode(servLead.getPinCode());
//				cstMst.setMobile(servLead.getMobile());
//				cstMst.setEmail(servLead.getEmail());
//				cstMst.setIsActive(servLead.getStatus());
//				cstMst.setRemarks(servLead.getRemarks());
//				cstMst.setSiteid(servLead.getSiteid());
//				cstMst=this.saveCustMst(cstMst);
//			}
//		}catch(Exception e) {
//			System.out.println("Exception accured"+e.getMessage());
//		}
//	}

//	@Transactional
//	public void checkerCust(ServiceLead1 servLead) {
//		try {
//			Customer cstMst= customerRepo.findByCsId(servLead.getCsId());
//			if(servLead.getCsId() > 0 &&
//					(!cstMst.getName().equals(servLead.getName()) 
//
//							|| !cstMst.getDisplayName().equals(servLead.getDisplayName())
//							|| !(cstMst.getAm_id() == servLead.getAmId())
//							|| !cstMst.getAddress().equals(servLead.getAddress())
//							|| !cstMst.getCity().equals(servLead.getCity())
//							|| !cstMst.getLandmark().equals(servLead.getLandmark())
//							|| !cstMst.getState().equals(servLead.getState())
//							|| !cstMst.getMobile().equals(servLead.getMobile())
//							|| !cstMst.getEmail().equals(servLead.getEmail())
//							|| !cstMst.getIsActive().equals(servLead.getStatus())
//							|| !cstMst.getRemarks().equals(servLead.getRemarks())
//
//							))  {
//				cstMst.setName(servLead.getName());
//				cstMst.setDisplayName(servLead.getDisplayName()); 
//				cstMst.setAm_id(servLead.getAmId());
//				cstMst.setAddress(servLead.getAddress());
//				cstMst.setLandmark(servLead.getLandmark());
//				cstMst.setCity(servLead.getCity());
//				cstMst.setState(servLead.getState());
//				cstMst.setPinCode(servLead.getPinCode());
//				cstMst.setMobile(servLead.getMobile());
//				cstMst.setEmail(servLead.getEmail());
//				cstMst.setIsActive(servLead.getStatus());
//				cstMst.setRemarks(servLead.getRemarks());
//				cstMst.setSiteid(servLead.getSiteid());
//				cstMst=this.saveCustomer(cstMst);
//			}
//		}catch(Exception e) {
//			System.out.println("Exception accured"+e.getMessage());
//		}
//	}

	@Transactional
	public void updateLsId(long lsId,long slId) {
		servLeadRepo.updateLsId(lsId,slId);
	}

	@PreAuthorize("hasRole('SERVICE_LEAD_VIEW') or hasRole('SERVICE_LEAD_EDIT')")
	@Transactional
	public ServiceLead getServLead(Long slId) {
		return servLeadRepo.findBySlId(slId);
	}

	@PreAuthorize("hasRole('SERVICE_LEAD_DELETE')")
	public String deleteServLead(long slId) {
		String result="Success";
		try {
			servLeadRepo.deleteBySlId(slId);
		} catch(DataIntegrityViolationException e) {
			result="DATA_INTEGRITY";
		} catch (Exception e) {
			result="DATA_INTEGRITY";
		}
		return result;
	}	


//	@Transactional
//	public synchronized PartnerAcceptanceLadger updatePmId(long slId, String loginId) {
//		boolean flag=true;
//		if(loginId.equals("") && loginId.length() <=0) {
//			loginId=getLoggedInUserLoinId();
//			flag=false;
//		}
//		servLeadRepo.updatedForLock(slId);
//		Long allocated = 3l;String result = "fail";
//		ServiceLead sl =servLeadRepo.findBySlId(slId);
//		User u = userRepo.findByLoginid(loginId);
//		PartnerLeadMap plm = new PartnerLeadMap();
//		PartnerAcceptanceLadger partLedger =null;
//		PartnerMst pmst=partnerMstRepo.findByPmId(u.getUiid());;
//		if(sl.getPmId()== null) {
//			int i =0;
//			try {
//				partLedger = commonRepo.executeNativeQuery("select * from fnc_ins_partner_ledger_new("+slId+","+u.getUiid()+")",
//						PartnerAcceptanceLadger.class).get(0);
//				if(partLedger.getSuccess() > 0) {
//					plm.setMapTime(new Date());
//					plm.setSlId(slId);
//					plm.setSiteid(0);
//					plm.setPmId(u.getUiid());
//					plm.setPtId(null);
//					plm.setCommDed(sl.getRate());
//					plm.setRemarks("Sl has been assigned");
//					plm.setReversal("N");
//					result="Success";
//					i=servLeadRepo.updatePmIdNot(slId,u.getUiid(),allocated);
//					partnerLeadMapRepo.save(plm);
//					this.saveTransactionDtl(slId,u.getUiid(),partLedger.getPvCommission(),flag,loginId);
//				}
//			}
//
//			catch(Exception e) {
//			}
//			ProductServiceVw psm= prodServiceVwRepo.findByPsmId(sl.getPsmId());
//			if(psm != null) {
//				String partnerMessage="Congratulations! \r\n"
//						+ "You have been allocated a Service Lead.\r\n"
//						+ "\r\n"
//						+ "Customer: "+sl.getDisplayName()+" ("+sl.getMobile()+")\r\n"
//						+ "Address: "+sl.getAddress()+", "+sl.getCity()+", "+sl.getState()+"\r\n"
//						+ "\r\n"
//						+ "Product: "+psm.getProdName()+"\r\n"
//						+ "Service Required: "+psm.getServiceName()+"\r\n"
//						+ "Service Required Time:"+sl.getServReqdAtTime();
//				sms.sendSms(new SMS(partnerMessage,pmst.getMobile()+",9654306585"));
//
//
//				String s =sl.getMobile()+",9654306585";
//				String mesage="Dear Customer, Your Ticket No. is "+sl.getSlId()+". Our representative will visit shortly.\r\n"
//						+ " For assistance please call 9654306585.";
//				sms.sendSms(new SMS(mesage,s));
//			}
//		} else {
//			partLedger= new PartnerAcceptanceLadger();
//			partLedger.setPvCloseBal(0l);
//			partLedger.setRejectReason("Already Allocated");
//			partLedger.setSuccess(2);
//		}
//		return partLedger;
//	}


	@Transactional
	public void updateLeadDate(Date date, long slId) {
		servLeadRepo.updateDate(date,slId);
	}


//	@Transactional
//	public void saveTransactionDtl(Long slId,Long pmId,Long amount,boolean mobile,String loginid) {
//		if(!mobile) {
//			PartnerLedger pl =new PartnerLedger();
//			pl.setTtmId(2l);
//			pl.setPmmId(4l);
//			pl.setSlId(slId);
//			pl.setPmId(pmId);
//			pl.setTranAmount(amount);
//			Date date = new Date();
//			Long timeMilli = date.getTime();
//			pl.setTranNo(timeMilli.toString());
//			pl.setPlDate(new Date());
//			pl.setTranDate(new Date());
//			pl.setRemarks("Commission Deduction");
//			pl.setSiteId(0l);
//			synchronized(pl) {
//				partnerledgerRepo.save(pl);
//			}
//		} else {
//			PartnerLedger2 pl =new PartnerLedger2();
//			pl.setTtmId(2l);
//			pl.setPmmId(4l);
//			pl.setSlId(slId);
//			pl.setPmId(pmId);
//			pl.setTranAmount(amount);
//			Date date = new Date();
//			Long timeMilli = date.getTime();
//			pl.setTranNo(timeMilli.toString());
//			pl.setPlDate(new Date());
//			pl.setTranDate(new Date());
//			pl.setRemarks("Commission Deduction");
//			pl.setSiteId(0l);
//			pl.setCreatedby(loginid);
//			pl.setCreateddt(date);
//			pl.setModifiedby(loginid);
//			pl.setModifieddt(date);
//			synchronized(pl) {
//				partnerledgerRepo2.save(pl);
//			}
//		}
//	}
//
//


	@Transactional
	public String updateWIP(long slId) {
		long lsId=4l;
		servLeadRepo.updateLsId(lsId,slId);
		return "success";
	}

	@Transactional
	public String markComplete(long slId,Date start,Date end,String rating,BigDecimal totalCharge) {
		long lsId=5l;
		int i=servLeadRepo.markAsComplete(slId,start,end,rating,totalCharge,lsId);
		return (i>0?"success":"fail");
	}

	@Transactional
	public String markUpdatePtIdByPmId(long slId,long pmId,long ptId) {
		int i=servLeadRepo.updatePtIdBySlId(slId,pmId,ptId);
		return (i >0?"success":"fail");
	}

	/*
	 * @Transactional public Long saveOTP(String mobile, String otp) {
	 * otpRepo.deleteByPhone(mobile); OTP o= new OTP(); o.setOtp(otp);
	 * o.setPhone(mobile); o= otpRepo.save(o);
	 * 
	 * 
	 * return o.getOtpId(); }
	 * 
	 * public boolean validateOTP(String mobile,String machId) { OTP otp=
	 * otpRepo.findByPhone(mobile); if(otp.getOtp().equals(machId)) {
	 * otpRepo.delete(otp); return true; }; return false; }
	 */

	public ServiceLeadVw getServleadVw(long slId) {
		return serviceLeadVwRepo.findBySlId(slId);
	}

}