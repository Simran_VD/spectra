package org.mysys.service;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mysys.model.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author kkaur
 * 
 */

@Service("emailService")
public class EmailService {
	private static final Log logger = LogFactory.getLog(EmailService.class);
	@Autowired
	JavaMailSender mailSender;

	@Autowired
	ServletContext servletContext;

	@Autowired
	private Environment env;


    public String sendEmail(Mail mail) {
    	String response = "success"; 
    	MimeMessage mimeMessage = mailSender.createMimeMessage();
    	 
        try {
 
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
 
            mimeMessageHelper.setSubject(mail.getMailSubject());
            mimeMessageHelper.setFrom(env.getProperty("email.service.mailFrom"));
            mimeMessageHelper.setTo(mail.getMailTo());
            if (mail.getMailBcc() != null && !mail.getMailBcc().isEmpty()) {
            	mimeMessageHelper.setBcc(mail.getMailBcc());
            }
            if (mail.getMailCc() != null && !mail.getMailCc().isEmpty()) {
            	mimeMessageHelper.setCc(mail.getMailCc());
            }
            mimeMessageHelper.setText(mail.getMailContent());
            List<File> destinationFiles = null;
            if (!mail.getAttachments().isEmpty()) {
            	try {
            		destinationFiles = new ArrayList<File>();
            		for (MultipartFile file: mail.getAttachments()) {
            			String path = servletContext.getRealPath(env.getProperty("email.service.temp.location")) + File.separator +
    	                        file.getOriginalFilename();
            			File destinationFile = new File(path);
            			file.transferTo(destinationFile);
            			destinationFiles.add(destinationFile);
            			mimeMessageHelper.addAttachment(destinationFile.getName(), destinationFile);
            		}
            	} /*catch (Exception e) {
            		response = "error";
            		logger.error("Error while adding attachment to the mail", e);
            	}*/
            	  catch(NullPointerException e) 
                { 
                    System.out.print("NullPointerException Caught"); 
                    response = "error";
            		logger.error("Error while adding attachment to the mail", e);
                } 
            }
            
            if (response.equalsIgnoreCase("success")) {
            	mailSender.send(mimeMessageHelper.getMimeMessage());
                if (destinationFiles != null) {
                	for (File file : destinationFiles) {
    					if (file.exists()) {
    						file.delete();
    					}
    				}
                }
            }
        } catch (MessagingException e) {
        	response = "error";
    		logger.error("Error while sending mail", e);
        } catch (Exception e) {
        	response = "error";
    		logger.error("Error while sending mail", e);
        }
        return response;
    }
    
 
}