package org.mysys.service;


import java.awt.image.BufferedImage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.transaction.Transactional;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.hibernate.id.UUIDGenerator;
import org.mysys.model.job.ApplicantResume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author Sudipsingh
 * 
 */
@Controller
public class FileUploadService  extends AbstractService{
	
	@Autowired
	private Environment env;


	@PostMapping("/FileUpload/FileUpload")
	@ResponseBody
	public String sendFile(@RequestParam("resumeName") MultipartFile[] files, @RequestParam Long enId,@RequestParam String drName) throws IOException {
		String fileName = "", originalFileName = "" , docPath = "";
		String name=drName;    		
		String folderName = env.getProperty("application.temp.applicantFolder")+"/ApplicantResume/"+ enId+"/";
		
//    		Path directory=Files.createDirectories(Paths.get(folderName));
		System.out.println("Folder is created");

//    		directory=directory.toAbsolutePath();
		for(MultipartFile uploadedFile:files) {
			originalFileName = uploadedFile.getOriginalFilename();
			String fileExtension = FilenameUtils.getExtension(originalFileName);
			
			docPath = folderName + originalFileName; 
			File file = new File(docPath);
			
			uploadedFile.transferTo(file);
			System.out.println("file is created");
			
			if(fileExtension.equalsIgnoreCase("doc") || fileExtension.equalsIgnoreCase("docx")) {
				// convert doc file into pdf, then upload
				originalFileName = originalFileName.replace(fileExtension, "");
				try (InputStream is = new FileInputStream(new File(docPath));
	    				OutputStream out = new FileOutputStream(new File(folderName + originalFileName +"pdf"));) {
	    			long start = System.currentTimeMillis();
	    			// 1) Load DOCX into XWPFDocument
	    			XWPFDocument document = new XWPFDocument(is);
	    			// 2) Prepare Pdf options
	    			PdfOptions options = PdfOptions.create();
	    			// 3) Convert XWPFDocument to Pdf
	    			PdfConverter.getInstance().convert(document, out, options);
	    			System.out.println("rdtschools-Docx2PdfConversion-word-sample.docx was converted to a PDF file in :: "
	    					+ (System.currentTimeMillis() - start) + " milli seconds");
	    		} catch (Throwable e) {
	    			e.printStackTrace();
	    		}
			}
			
		}

		jobService.updateResumeRecord(enId, originalFileName +"pdf");
		return "Success";
	}

	
	@PostMapping("/FileUpload/getApplicantResume")
	@ResponseBody
	public String getFiles(long enId) throws NullPointerException {
		String Fname = applicantResumeRepo.getByAmId(enId);
		return Fname;  
	}

	@PostMapping("/FileUpload/getUploadedFiles")
	@ResponseBody
	public List<String> getFile(long enId,String drName) throws NullPointerException {
		Long fileName=enId;
		String name=drName;
		String folderName = env.getProperty("application.temp.applicantFolder")+"/ApplicantResume/"+ fileName;
		File root = new File(folderName);

		List<String> results = new ArrayList<String>();
		if(root.listFiles() != null) {
			for (File file : root.listFiles()) {
    			if (file.isFile()) {
    				String dd=file.getName();
    				results.add(dd);		    	
    			}
    		}
		}
		
		return results;
	}

	@PostMapping("/fileUpload/deleteFiles")
	@ResponseBody
	public boolean deleteFile(@RequestParam String fileName,@RequestParam String folderName,String drName) throws IOException {
		String name=drName;
		fileName = env.getProperty("application.temp.applicantFolder") + "/ApplicantResume/" +folderName.trim()+"/"+fileName.trim();
		File root = new File(fileName);
		boolean result = Files.deleteIfExists(root.toPath());
		return result;
	}
	
	@PostMapping("/fileUpload/deleteResume")
	@ResponseBody
	@Transactional
	public boolean deleteResume(Long amid) {
		applicantResumeRepo.deleteByamId(amid);
		return true;
	}
	
	/******  Image Upload Application Master  **********/
	
	@PostMapping("/FileUpload/uploadImgOfAppliction")
	@ResponseBody
	public String ImageUpload(@RequestParam("imgFile") MultipartFile  imgFile,@RequestParam Long amId) throws IOException{
		if (!imgFile.isEmpty()) {
			String pathOfImgWrite = env.getProperty("application.temp.applicantFolder") + "/ApplicantProf/";
			BufferedImage src = ImageIO.read(new ByteArrayInputStream(imgFile.getBytes()));
//    			String name = UUID.randomUUID().toString();
			File destination = new File(pathOfImgWrite +amId + ".png"); // something like C:/Users/tom/Documents/nameBasedOnSomeId.png
			//File destination = new File(pathOfImgWrite +itemid + ".png"); // something like C:/Users/tom/Documents/nameBasedOnSomeId.png
			boolean result = Files.deleteIfExists(destination.toPath());
			ImageIO.write(src, "png", destination);
			String path=destination.getAbsolutePath();
			
			return "Succcess";
		} else {
			return "fail";
		}

	}
	
	
	@PostMapping("/FileUpload/FileUploadforQuotation")
	@ResponseBody
	public void carryForward(@RequestParam Long enId,@RequestParam Long quoId,@RequestParam String custname,@RequestParam String[] fileName,@RequestParam String drName) throws IOException {
		String folderName=enId+"_"+custname;
		String qouFolderName=quoId+"_"+custname;
		File getFileName = new File(env.getProperty("application.temp.EnqFolder")+"/"+"EnquiryFiles"+"/"+folderName.trim());
		File[] newFile=getFileName.listFiles();
		String name=drName;
		String path = env.getProperty("application.temp.EnqFolder")+"/" +name +"/"+ qouFolderName.trim();
		Path directory=Files.createDirectories(Paths.get(path));
		String files,original ;
		Path originalFi = null,destinationFile = null;
		int i,j;
		for(i=0;i<newFile.length;i++)
		{
			if(Arrays.asList(fileName).contains(newFile[i].getName())){
				original=getFileName+"/"+newFile[i].getName();
				originalFi=Paths.get(original);
				files = path +"/"+ newFile[i].getName();
				destinationFile=Paths.get(files);
				Files.copy(originalFi, destinationFile, StandardCopyOption.REPLACE_EXISTING);

			}
		}

	}
	
	
	public static void main(String[] args) {
		try (InputStream is = new FileInputStream(new File("C:\\Users\\Lenovo\\Documents\\simran\\JPA-Mappings.docx"));
				OutputStream out = new FileOutputStream(new File("C:\\Users\\Lenovo\\Documents\\simran\\rdtschools-Docx2PdfConverted_PDF_File.pdf"));) {
			long start = System.currentTimeMillis();
			// 1) Load DOCX into XWPFDocument
			XWPFDocument document = new XWPFDocument(is);
			// 2) Prepare Pdf options
			PdfOptions options = PdfOptions.create();
			// 3) Convert XWPFDocument to Pdf
			PdfConverter.getInstance().convert(document, out, options);
			System.out.println("rdtschools-Docx2PdfConversion-word-sample.docx was converted to a PDF file in :: "
					+ (System.currentTimeMillis() - start) + " milli seconds");
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}