package org.mysys.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mysys.model.spectra.OrgMaster;
import org.springframework.stereotype.Service;

@Service
public class SpectraService extends AbstractService{
	
	private static final Logger LOGGER = LogManager.getLogger(SpectraService.class);
	
	// Table : org_mst
	// Action : save/edit data of org_mst
	public OrgMaster saveOrgMaster(OrgMaster master) {
		LOGGER.info("inside saveOrgMaster() method. Saving data for Org Master");
		return orgMasterRepo.save(master);
	}
	

}
