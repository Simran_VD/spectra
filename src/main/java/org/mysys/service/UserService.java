package org.mysys.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.mysys.model.Role;
import org.mysys.model.Rolescreenaccess;
import org.mysys.model.User;
import org.mysys.model.UserSetting;
import org.mysys.model.UserSettingPK;
import org.mysys.model.Userrole;
import org.mysys.model.security.CustomUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService extends AbstractService implements UserDetailsService {

	
    @Override
    public UserDetails loadUserByUsername(String loginId) throws UsernameNotFoundException {
        User user = userRepo.findByLoginid(loginId);
        if (user == null || StringUtils.isEmpty(user.getStatus()) || user.getStatus().equalsIgnoreCase("0")){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        CustomUser mySysUser = new CustomUser(user.getLoginid(),
                user.getPassword(),
                mapRolesToAuthorities(user.getUserroles()));
        mySysUser.setSiteid(user.getSiteid());
        return mySysUser;
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Userrole> roles){
         List<Role> rol = roles.stream().filter(role-> role.getRole()!=null)
                .map(role -> role.getRole())
                .collect(Collectors.toList());
         List<Rolescreenaccess> roleAccess = new ArrayList<>();
         for(Role rl : rol){
        	 if(rl.getRolescreenaccesses()!= null && rl.getRolescreenaccesses().size() > 0){
        		 roleAccess.addAll(rl.getRolescreenaccesses());
        	 }
         }
         return roleAccess.stream().filter(ra-> ra.getScreen() != null && ra.getId().getOperation() != null)
         .map(ra -> new SimpleGrantedAuthority(String.join("_", "ROLE",ra.getScreen().getScreenname(),ra.getId().getOperation()).toUpperCase())).collect(Collectors.toList());
    }
    
    public List<UserSetting> getUserSetting() {
    	return userSettingRepo.findById_LoginidAndId_Siteid(getLoggedInUserLoinId(), getLoggedInUserSiteId());
    }
    
    public UserSetting saveUserSetting(String key, String value) {
    	UserSettingPK id = new UserSettingPK(getLoggedInUserLoinId(), key, getLoggedInUserSiteId());
    	UserSetting userSetting = new UserSetting(id, value);
    	return userSettingRepo.save(userSetting);
    }
}