package org.mysys.service;
import java.util.Collections;

import java.util.List;

import org.mysys.model.DropDownComparator;
import org.mysys.model.DropdownVO;
import org.mysys.model.InterviewLevelMst;
import org.mysys.model.User;
import org.mysys.model.security.CustomUser;
import org.mysys.model.spectra.RoleDropDownVw;
import org.mysys.repository.CasesStatusMstRepo;
import org.mysys.repository.CityMasterViewRepo;
import org.mysys.repository.ClientSpocRepo;
import org.mysys.repository.CommonRepo;
import org.mysys.repository.ContactRepo;
import org.mysys.repository.ContactVoRepo;
import org.mysys.repository.DashboardKPIServiceDetailVwRepo;
import org.mysys.repository.DashboardKPIServiceVwRepo;
import org.mysys.repository.DashboardSettingRepo;
import org.mysys.repository.DbSettingRepo;
import org.mysys.repository.DepartmentRepo;
import org.mysys.repository.DesignationRepo;
import org.mysys.repository.DomainMstDropDownVwRepo;
import org.mysys.repository.FormatSpocVwRepository;
import org.mysys.repository.InstituteMstRepo;
import org.mysys.repository.JobLocationRepo;
import org.mysys.repository.JobMasterRepo;
import org.mysys.repository.JobSkillRepo;
import org.mysys.repository.JobStatusMasterRepo;
import org.mysys.repository.CitymasterRepo;
import org.mysys.repository.LocationMasterViewRepo;
import org.mysys.repository.MatchingApplListVwRepo;
import org.mysys.repository.PinBoardListViewRepo;
import org.mysys.repository.RecruiterMasterRepo;
//import org.mysys.repository.PartnerToNotifyVwRepo;
import org.mysys.repository.RoleRepo;
import org.mysys.repository.RolescreenaccessRepo;
import org.mysys.repository.ScreenRepo;
import org.mysys.repository.SiteRepo;
import org.mysys.repository.SkillDomainMstDropDownVwRepo;
import org.mysys.repository.SkillDomainMstRepo;
import org.mysys.repository.SkillGroupMstRepo;
import org.mysys.repository.SkillMstDropDownVwRepo;
import org.mysys.repository.SkillMstRepo;
import org.mysys.repository.SkillRepo;
import org.mysys.repository.StateRepo;
import org.mysys.repository.TemplateListRepo;
import org.mysys.repository.UserRepository;
import org.mysys.repository.UserRoleRepository;
import org.mysys.repository.UserSettingRepo;
import org.mysys.repository.UserTypeMstListVwRepo;
import org.mysys.repository.Service.ServiceLead1Repo;
import org.mysys.repository.Service.ServiceLeadRepo;
import org.mysys.repository.crm.CompanySetupRepo;
import org.mysys.repository.crm.MiscCfgRepo;
import org.mysys.repository.job.ApplBackedOutMstRepo;
import org.mysys.repository.job.ApplRejectionMstRepo;
import org.mysys.repository.job.AppliInsertJobScoreRepo;
import org.mysys.repository.job.ApplicantCertificateRepository;
import org.mysys.repository.job.ApplicantEducationRepo;
import org.mysys.repository.job.ApplicantExperienceRepository;
import org.mysys.repository.job.ApplicantMasterRepo;
import org.mysys.repository.job.ApplicantMasterWebRepo;
import org.mysys.repository.job.ApplicantPrefLocationRepo;
import org.mysys.repository.job.ApplicantResumeRepo;
import org.mysys.repository.job.ApplicantSkillRepo;
import org.mysys.repository.job.CasesStatusMstVwRepo;
import org.mysys.repository.job.ClientMstRepo;
import org.mysys.repository.job.CompanyMstRepo;
import org.mysys.repository.job.CompanyMstVwRepo;
import org.mysys.repository.job.CourseMstListVwRepo;
import org.mysys.repository.job.CourseMstRepo;
import org.mysys.repository.job.DashJobOpeningListRepository;
import org.mysys.repository.job.DashboardApplicantCasesRepository;
import org.mysys.repository.job.DashboardApplicantListRepo;
import org.mysys.repository.job.DashboardJobApplicantMatchRepo;
import org.mysys.repository.job.DashboardJobCountRepo;
import org.mysys.repository.job.DashboardJobStatusCountRepo;
import org.mysys.repository.job.DomainDropdownVwRepo;
import org.mysys.repository.job.DomainMstRepo;
import org.mysys.repository.job.EduMstListVwRepo;
import org.mysys.repository.job.EduMstRepo;
import org.mysys.repository.job.ExperienceDropDownVwRepo;
import org.mysys.repository.job.InstituteMstVwRepo;
import org.mysys.repository.job.InterviewLevelMstRepo;
import org.mysys.repository.job.InterviewStatusMstRepo;
import org.mysys.repository.job.JobApplNotesCommentsVwRepo;
import org.mysys.repository.job.JobApplNotesRepo;
import org.mysys.repository.job.JobApplicantInterviewRepo;
import org.mysys.repository.job.JobApplicantRepo;
import org.mysys.repository.job.JobCertRepo;
import org.mysys.repository.job.JobEduRepo;
import org.mysys.repository.job.JobMatchWeightScoreRepository;
import org.mysys.repository.job.JobOpeningListVwRepo;
import org.mysys.repository.job.JobRecuMapRepo;
import org.mysys.repository.job.JobSpocRepo;
import org.mysys.repository.job.MatchWeightCfgRepo;
import org.mysys.repository.job.NoticePeriodDropdownListVwRepo;
import org.mysys.repository.job.PositionMstRepo;
import org.mysys.repository.job.PositionMstVwRepo;
import org.mysys.repository.job.RecuGrpMapRepo;
import org.mysys.repository.job.RecuGrpMapVwRepo;
import org.mysys.repository.job.RecuGrpMstListViewRepo;
import org.mysys.repository.job.RecuGrpMstRepo;
import org.mysys.repository.job.RoleListVwRepo;
import org.mysys.repository.job.SkillCatMstListVwRepo;
import org.mysys.repository.job.SkillCatMstRepo;
import org.mysys.repository.job.SkillDomainMstListVwRepo;
import org.mysys.repository.job.SkillGroupListVwRepo;
import org.mysys.repository.job.SourceMstDropDownRepo;
import org.mysys.repository.job.SourceMstRepo;
import org.mysys.repository.job.SpocMstListVwRepo;
import org.mysys.repository.job.SpocMstRepo;
import org.mysys.repository.job.SpocShareFormatRepo;
import org.mysys.repository.job.StateMasterViewRepo;
import org.mysys.repository.job.TaskAndReminderRepo;
import org.mysys.repository.job.UserListVwRepo;
import org.mysys.repository.job.UserTypeMstRepo;
import org.mysys.repository.notification.Notification1Repo;
import org.mysys.repository.notification.NotificationRepo;
import org.mysys.repository.notification.NotifyListVwRepo;
import org.mysys.repository.notification.OTPRepo;
import org.mysys.repository.notification.ServiceLeadNotifyVwRepo;
import org.mysys.repository.job.IssueLogDtlCommRepo;
import org.mysys.repository.job.IssueLogDtlRepo;
import org.mysys.repository.job.IssueLogRepo;
import org.mysys.repository.product.ServiceLeadVwRepo;
import org.mysys.repository.spectra.AuditTypeMstRepo;
import org.mysys.repository.spectra.CorpDropDownVwRepository;
import org.mysys.repository.spectra.FreqMstRepo;
import org.mysys.repository.spectra.OrgMasterRepository;
import org.mysys.repository.spectra.OrgParentDropDownVwRepository;
import org.mysys.repository.spectra.RoleDropDownVwRepo;
import org.mysys.repository.spectra.OrgTypeRepo;
import org.mysys.repository.spectra.OrgTypeVwRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class AbstractService 
{
	
    @Autowired
    protected UserService userService;
    
	@Autowired
	protected UserSettingRepo userSettingRepo;
	
	@Autowired
	protected UserRepository userRepo;
	
	@Autowired
	protected NotifyListVwRepo notifyRepo;
	
	@Autowired
	protected RoleRepo roleRep;
	
	@Autowired
	protected UserRoleRepository userRoleRepo;
	
	@Autowired
	protected DesignationRepo designRepo;
	
	@Autowired
	protected ScreenRepo screenRepo;
	
	@Autowired
	protected RolescreenaccessRepo rolescreenaccessRepo;
	
	@Autowired
	protected DropDownComparator dropDownComparator;
	
	@Autowired
	protected CommonRepo commonRepo;
	
	@Autowired
	protected DepartmentRepo deptRepo;
	
	/***************** City Master Repo   ***********/
	@Autowired
	protected CitymasterRepo citymasterRepo;

	@Autowired
	protected RoleRepo roleRepo;

	@Autowired
	protected SiteRepo siteRepo;

	@Autowired
	protected StateRepo stateRepo;

	@Autowired
	protected JobApplicantInterviewRepo jobAppliInterRepo;
	
	@Autowired
	protected JobApplNotesCommentsVwRepo jobApplNotesCommentsVwRepo;
	
	@Autowired
	protected ContactRepo contactRepo;
	
	@Autowired
	protected ContactVoRepo contactVoRepo;
	
	@Autowired
	protected Environment env;
	
	@Autowired
	protected DesignationRepo designationRepo;
	
	@Autowired
	protected SkillRepo skillRepo;
	
	@Autowired
	protected EduMstRepo eduMstRepo;
	
	@Autowired
	protected SourceMstRepo sourceMstRepo;
	
	@Autowired
	protected UserTypeMstRepo userTypeMstRepo;
	
	@Autowired
	protected UserTypeMstListVwRepo userTypeMstListVwRepo;
	
	@Autowired
	protected RecuGrpMstRepo recuGrpMstRepo;
	
	@Autowired
	protected CompanyMstRepo companyMstRepo;
	
	@Autowired
	protected ClientMstRepo clientMstRepo;
	
	@Autowired
	protected DomainDropdownVwRepo domainDropdownVwRepo;
	
	@Autowired
	protected MatchWeightCfgRepo matchWeightCfgRepo;
	
	@Autowired
	protected DomainMstRepo domainMstRepo;
	
	@Autowired
	protected CourseMstRepo courseMstRepo;
	
	@Autowired
	protected InstituteMstRepo instituteMstRepo;

	//Start: CRM Repository instances
	
	@Autowired
	protected TemplateListRepo tempateListRepo;
	
	@Autowired
	protected MiscCfgRepo  miscCfgRepo;
	
	@Autowired
	protected CompanySetupRepo  cmpRepo;
	
	@Autowired
	protected PayrollService payrollService;
	
	@Autowired
	DashboardApplicantCasesRepository dashboardApplCasesRepo;
	
	@Autowired
	JobService jobService;
	
	@Autowired
	CasesStatusMstRepo caseStatusMstRepo;
//	
//	@Autowired
//	protected AreaVwRepo areaVwRepo;
//	
//	@Autowired
//	protected ServiceMstVwRepo productServiceRepo;
	
//	
//	@Autowired
//	protected PartnerMstRepo partnerMstRepo;
//	
//	@Autowired
//	protected PartnerAreaMapRepo partnerAreaMapRepo;
//	
//	@Autowired
//	protected PartnerServiceMapRepo partnerServiceRepo;
//	
//
//	@Autowired
//	protected CustMstRepo custMstRepo;
//	
//	@Autowired
//	protected CustMstVwRepo custMstVwRepo;
//	
//	@Autowired
//	protected ProductMstVwRepo prodMstVwRepo;
	
	@Autowired
	protected ServiceLeadRepo servLeadRepo;
	
	@Autowired
	protected NotificationRepo notificationRepo;
	
	@Autowired
	protected ServiceLeadNotifyVwRepo servLeadNotifyRepo;
	
	@Autowired
	protected DashboardKPIServiceVwRepo dashBoardServiceRepo;
	
	@Autowired
	protected DashboardKPIServiceDetailVwRepo dashboardDetailRepo;
	
	@Autowired
	protected ServiceLeadVwRepo serviceLeadVwRepo;
	
	@Autowired
	protected FirebaseMessagingService fcm;
	
	@Autowired
	protected AdminService adminService;

	@Autowired
	protected SMSService sms;

	@Autowired
	protected ServiceLead1Repo serLead1Repo;
	
	@Autowired
	protected Notification1Repo ntRepo;

	@Autowired
	protected OTPRepo otpRepo;
	
	@Autowired
	protected DashboardServices dasboardServices;

	@Autowired
	protected AdminService service;

	@Autowired
	protected AppService appService;

	@Autowired
	ContactService contactService;

	@Autowired
	protected MasterService masterService;

	@Autowired
	protected PartnerService partnerService;

	@Autowired
	protected CustomerService customerService;

//	@Autowired
//	protected ProductService prodService;
	
	@Autowired
	protected DbSettingRepo dbSettingRepo;
	
	@Autowired
	protected CityMasterViewRepo cityMasterViewRepo;
	
	@Autowired
	protected RoleListVwRepo roleListVwRepo;
	
	@Autowired
	protected JobMasterRepo jobMasterRepo;
	
	@Autowired
	protected DomainMstDropDownVwRepo clientMasterRepo;
	
	@Autowired
	protected LocationMasterViewRepo locationMasterViewRepo;
	
	@Autowired
	protected SkillMstDropDownVwRepo skillMstDropDownVwRepo;
	
	@Autowired
	protected JobLocationRepo jobLocationRepo;
	
	@Autowired
	protected JobStatusMasterRepo jobStatusMasterRepo;
	
	@Autowired
	protected RecruiterMasterRepo recruiterMasterRepo;
	
	@Autowired
	protected JobSkillRepo jobSkillRepo;
	
	@Autowired
	protected JobCertRepo jobCertRepo;
	
	@Autowired
	protected JobSpocRepo jobSpocRepo;
	
	@Autowired
	protected ClientSpocRepo clientSpocRepo;
	
	@Autowired
	protected SkillDomainMstDropDownVwRepo skillDomainMstDropDownVwRepo;
	
	@Autowired
	protected SkillCatMstRepo skillCatMstRepo;
	
	@Autowired
	protected SkillDomainMstRepo skillDomainMstRepo;
	
	@Autowired
	protected SkillGroupMstRepo skillGroupMstRepo;
	
	@Autowired
	protected NoticePeriodDropdownListVwRepo noticePerDropListVwRepo;
	
	@Autowired
	protected ExperienceDropDownVwRepo experienceDropVwRepo;
	
	@Autowired
	protected EduMstListVwRepo eduMstListVwRepo;
	
	@Autowired
	protected CourseMstListVwRepo courseMstListVwRepo;
	
	@Autowired
	protected SkillMstRepo skillMstRepo;
	
	@Autowired
	protected SkillCatMstListVwRepo skillCatMstListVwRepo;
	
	@Autowired
	protected SkillDomainMstListVwRepo skillDomainMstListVwRepo;
	
	@Autowired
	protected SkillGroupListVwRepo skillGroupListVwRepo;
	
	@Autowired
	protected ApplicantMasterRepo applicantMasterRepo;
	
	@Autowired
	protected ApplicantMasterWebRepo applicantMasterWebRepo;

	@Autowired
	protected SourceMstDropDownRepo sourceMstDropDownRepo;
	
	@Autowired
	protected StateMasterViewRepo stateMasterViewRepo;
	
	@Autowired
	protected ApplicantPrefLocationRepo applicantPrefLocationRepo;
	
	@Autowired
	protected ApplicantSkillRepo applicantSkillRepo;
	
	@Autowired
	protected RecuGrpMstListViewRepo recuGrpMstListViewRepo;
	
	@Autowired
	protected JobRecuMapRepo jobRecuMapRepo;
	
	@Autowired
	protected JobEduRepo jobEduRepo;
	
	@Autowired
	protected SpocMstRepo spocMstRepo;
	
	@Autowired
	protected InterviewStatusMstRepo interviewStatusMstRepo;
	
	@Autowired
	protected ApplicantResumeRepo applicantResumeRepo;
	
	@Autowired 
	protected InterviewLevelMstRepo interviewLevelMstRepo;
	
	@Autowired
	protected SpocMstListVwRepo spocMstListVwRepo;
	
	@Autowired
	protected MatchingApplListVwRepo matchingappllistvwRepo;
	
	@Autowired
	protected PositionMstRepo positionMstRepo;
	
	@Autowired
	protected DashboardApplicantListRepo dashboardapplicantlistRepo;
	
	@Autowired
	protected DashboardJobApplicantMatchRepo dashboardjobapplicantmatchRepo;
	
	@Autowired
	protected JobApplicantRepo jobapplicantRepo;
	
	@Autowired
	protected ApplRejectionMstRepo applRejectionMstRepo;
	
	@Autowired
	protected ApplBackedOutMstRepo applBackedOutMstRepo;
	
	@Autowired
	protected JobApplNotesRepo jobApplNotesRepo;
	
	@Autowired
	protected DashboardJobCountRepo dashboardJobCountRepo;
	
	@Autowired
	protected DashboardJobStatusCountRepo dashboardJobStatusCountRepo;
	
	@Autowired
	protected DashboardSettingRepo dashboardsettingRepo;
	
	@Autowired
	protected JobOpeningListVwRepo jobOpeningListVwRepo;
	
	@Autowired
	protected RecuGrpMapVwRepo recugrpmapVwRepo;
	
	@Autowired
	protected ApplicantEducationRepo applicanteducationRepo;
	
	@Autowired
	protected CourseMstListVwRepo coursemstlistvwRepo;
	
	@Autowired
	protected InstituteMstVwRepo institutemstvwRepo;
	
	@Autowired
	protected ApplicantExperienceRepository applicantexpRepo;
	
	@Autowired
	protected DashJobOpeningListRepository jobOpeningListRepo;
	
	@Autowired
	ApplicantCertificateRepository applicantCertRepo;
	
	@Autowired
	protected CasesStatusMstVwRepo casesStatusMstVwRepo;
	
	@Autowired
	protected CompanyMstVwRepo companyMstVwRepo;
	
	@Autowired
	protected PositionMstVwRepo positionMstVwRepo;
	
	@Autowired
	protected DashboardApplicantCasesRepository dashboardapplicantcasesRepo;
	
	@Autowired
	protected SpocShareFormatRepo spocShareFormatRepo;
	
	@Autowired
	protected TaskAndReminderRepo taskandreminerRepo;
	
	@Autowired
	protected UserListVwRepo userlistvwRepo;
	
	@Autowired
	protected JobMatchWeightScoreRepository jobMatchWeightRepo;
	
	@Autowired
	protected AppliInsertJobScoreRepo appliInsertJobScoreRepo;
	
	@Autowired
	protected RecuGrpMapRepo recuGroupMapRepo;
	
	@Autowired
	protected FormatSpocVwRepository formatSpocViewRepo;
	
	@Autowired
	protected PinBoardListViewRepo pinBoardListViewRepo;
	
	@Autowired
	protected IssueLogRepo issueLogRepo;

	@Autowired
	protected IssueLogDtlRepo issueLogDtlRepo;
	
	@Autowired
	protected OrgMasterRepository orgMasterRepo;
	
	@Autowired
	protected OrgTypeRepo  orgTypeRepo;
	
	@Autowired
	protected AuditTypeMstRepo auditTypeMstRepo;

	@Autowired
	protected RoleDropDownVwRepo roleDropDownVwRepo;
	
	@Autowired
	protected OrgTypeVwRepository orgTypeVwRepo;
	
	@Autowired
	protected FreqMstRepo freqMstRepo;
	
	@Autowired
	protected CorpDropDownVwRepository corpdropdownVwRepository;
	
	@Autowired
	protected OrgParentDropDownVwRepository orgparentdropdownVw;
	
	
	public static final String APPLICATION_ACCTYEAR = "application.acctYear";
	
	
	public String getApplicationAcctYear() {
	    return env.getProperty(APPLICATION_ACCTYEAR);
	}

	public Long getNext(Long value) {
	    return (value != null) ? (value + 1) : 1;
	}
	
	public List<DropdownVO> sortIt(List<DropdownVO> dropdownVOs) {
		Collections.sort(dropdownVOs, dropDownComparator);
		return dropdownVOs;
	}
	
	public long getLoggedInUserSiteId(){
		CustomUser mySysUser = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return mySysUser.getSiteid();
	}
	
	public boolean validateLogin(String token) {
		Long userId = userRepo.findUserId(token);
		return (userId !=null?true:false);
	}
	
	public String getCorporateToken() {
		String token = userRepo.findToken("deepak");
		return token;
	}
	
	public long getLoggedInUserUserId(){
		User u=userRepo.findByLoginid(getLoggedInUserLoinId());
		return u.getUserid();
	}
	

	public String getLoggedInUserLoinId(){
		CustomUser mySysUser = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return mySysUser.getUsername();
	}
	
	
	
}
