package org.mysys.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;


import org.mysys.model.Contact;
import org.mysys.model.Contactcateg;
import org.mysys.model.Contactperson;
import org.mysys.model.Contacttype;
import org.mysys.model.DropdownVO;
import org.mysys.repository.ContactCategRepo;
import org.mysys.repository.ContactPersonRepo;
import org.mysys.repository.ContactRepo;
import org.mysys.repository.ContactRoleRepo;
import org.mysys.repository.ContactTypeRepo;
import org.mysys.repository.StateRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class ContactService extends AbstractService {

//    @Autowired
//    private LocMstRepo locMstRepo;
    
    @Autowired
    private ContactRoleRepo roleRepo;
    
    @Autowired
    private ContactCategRepo categRepo;
    
    @Autowired
    private ContactTypeRepo typeRepo;
    
    @Autowired
    private StateRepo stateRepo;
    
    @Autowired
    private ContactRepo contactRepo;
   
    @Autowired
    private ContactPersonRepo contactPersonRepo;
    
    
//    public Page<LocMst> findPages(int pageNo,int pageSize){
//      return locMstRepo.findAll(PageRequest.of(pageNo, pageSize));
//    }
//    
//    public List<LocMst> findCityByState(long stateId){
//    	return locMstRepo.findByStatemaster_stateidOrderByCitynameAsc(stateId);
//    }
//    
    @Transactional
    public Map<String,List<DropdownVO>> getDropDownData(){
    	Map<String,List<DropdownVO>> dropdownData = new HashMap<>();
    	dropdownData.put("role", sortIt(roleRepo.findBySiteid(getLoggedInUserSiteId()).stream().map((a)->{return new DropdownVO(String.valueOf(a.getContactroleid()),a.getContactrolename()); }).collect(Collectors.toList())));
    	dropdownData.put("state", stateRepo.findAll(new Sort(Sort.Direction.ASC, "statename")).stream().map((a)->{return new DropdownVO(String.valueOf(a.getStateid()),a.getStatename()); }).collect(Collectors.toList()));
    	dropdownData.put("categ", sortIt(categRepo.findBySiteid(getLoggedInUserSiteId()).stream().map((a)->{return new DropdownVO(String.valueOf(a.getCategoryid()),a.getCategoryname()); }).collect(Collectors.toList())));
    	dropdownData.put("type", sortIt(typeRepo.findBySiteid(getLoggedInUserSiteId()).stream().map((a)->{return new DropdownVO(String.valueOf(a.getContacttypeid()),a.getContacttypename()); }).collect(Collectors.toList())));
    	return dropdownData;
    }
    
    @Transactional
    public Contact saveContact(Contact contact) {
    	contact.setSiteid(getLoggedInUserSiteId());
    	List<Contactperson> contactPerson = new ArrayList<>(contact.getContactpersons());
    	contact.getContactpersons().clear();
    	if (CheckContactPerson(contactPerson)) {
    		if(contact.getContactid()!=0){
    			contactPersonRepo.deleteByContact(contact);
    		}
    		for(Contactperson cp : contactPerson){
    			cp.setSiteid(getLoggedInUserSiteId());
    			cp.setSiteid(getLoggedInUserSiteId());
    			contact.addContactperson(cp);
    		}
    	}
    	Contact temp = contactRepo.save(contact);
    	return temp;
    }
    
    public boolean CheckContactPerson(List<Contactperson> contactPersons) {
    	if (contactPersons != null && !contactPersons.isEmpty() && contactPersons.size() > 0) {
    		for (Contactperson contactperson : contactPersons) {
				if (contactperson.getStartdate() == null) {
					return false;
				}
			}
    	} else {
    		return false;
    	}
    	return true;
    }
    
    public Contactcateg saveContactCateg(final String contactCateg){
    	Contactcateg categ = new Contactcateg();
    	categ.setCategoryname(contactCateg);
    	categ.setSiteid(getLoggedInUserSiteId());
    	return categRepo.save(categ);
    }

	public Contacttype saveContactType(String contactTypeName) {
		Contacttype categ = new Contacttype();
    	categ.setContacttypename(contactTypeName);
    	categ.setSiteid(getLoggedInUserSiteId());
    	return typeRepo.save(categ);
	}

	@Transactional
	public Contact getContact(Long contactId) {
		 Contact cnt = contactRepo.getOne(contactId);
		 cnt.getAddr1();
		 return cnt;
	}
	
	@PreAuthorize("hasRole('CONTACT_DELETE')")
	public String deleteContact(Long contactId){
		String result = "SUCCESS";
		try{
			contactRepo.deleteById(contactId);
		}catch(DataIntegrityViolationException ex){
			result = "DATA_INTEGRITY";
		}catch(Exception ex){
			result = "DATA_INTEGRITY";
		}
		return result;
	}
	
	@Transactional
	public List<Contactcateg> isContactCategExist(String categoryName){
		 List<Contactcateg> cnCateg=categRepo.findByCategorynameAndSiteid(categoryName,getLoggedInUserSiteId());
		 return cnCateg;
	}
	
	/*
	 * @Transactional public List<Contacttype> isContactTypeExist(String
	 * contacttypename){ List<Contacttype>
	 * cnCateg=typeRepo.findByContacttypenameAndSiteid(contacttypename,
	 * getLoggedInUserSiteId()); return cnCateg; }
	 */
	
}
