package org.mysys.service;

import javax.transaction.Transactional;

import org.mysys.model.User;
import org.springframework.stereotype.Service;

@Service
public class AppService extends AbstractService {

	@Transactional
	public boolean isLoginExists(String userid ,String pass,String token) {
		boolean result = true;
		User u=this.userRepo.findByActiveLoginidAndPassword(userid,pass);
		if(u==null) {
			result= false;
		} else {
			userRepo.updateFCMToken(u.getUserid(),token);
		}
		return result;
	} 
	

	@Transactional
	public boolean loggOut(String loginId,String token) {
		String token1=null;
		int i=	userRepo.loggOut(loginId,token1);
		return (i>0?true:false);
	} 
}
