package org.mysys.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.mysys.constant.APPConstant;
import org.mysys.exception.IdenticalException;
import org.mysys.model.DashboardKPIServiceVw;
import org.mysys.model.InterviewLevelMst;
import org.mysys.model.InterviewStatusMst;
import org.mysys.model.User;
import org.mysys.model.job.ApplBackedOutMst;
import org.mysys.model.job.ApplRejectionMst;
import org.mysys.model.job.ApplicantCertificate;
import org.mysys.model.job.ApplicantEducation;
import org.mysys.model.job.ApplicantExperience;
import org.mysys.model.job.ApplicantMaster;
import org.mysys.model.job.ApplicantMasterWeb;
import org.mysys.model.job.ApplicantPrefLocation;
import org.mysys.model.job.ApplicantResume;
import org.mysys.model.job.ApplicantSkill;
import org.mysys.model.job.CasesStatusMstVw;
import org.mysys.model.job.ClientMst;
import org.mysys.model.job.ClientSpoc;
import org.mysys.model.job.CourseMstListVw;
import org.mysys.model.job.DashJobOpeningList;
import org.mysys.model.job.DashboardApplicantCases;
import org.mysys.model.job.DashboardApplicantList;
import org.mysys.model.job.DashboardJobApplicantMatch;
import org.mysys.model.job.DashboardJobCount;
import org.mysys.model.job.DashboardJobStatusCount;
import org.mysys.model.job.DashboardKPIServiceDetailVw;
import org.mysys.model.job.DomainMstDropDownVw;
import org.mysys.model.job.EduMst;
import org.mysys.model.job.EduMstListVw;
import org.mysys.model.job.ExperienceDropDownVw;
import org.mysys.model.job.FormatSpocVw;
import org.mysys.model.job.InstituteMstVw;
import org.mysys.model.job.JobApplNotes;
import org.mysys.model.job.JobApplNotesCommentsVw;
import org.mysys.model.job.JobApplicant;
import org.mysys.model.job.JobApplicantInterview;
import org.mysys.model.job.JobCert;
import org.mysys.model.job.JobEdu;
import org.mysys.model.job.JobLocation;
import org.mysys.model.job.JobMaster;
import org.mysys.model.job.JobOpeningListVw;
import org.mysys.model.job.JobRecuMap;
import org.mysys.model.job.JobSkill;
import org.mysys.model.job.JobSpoc;
import org.mysys.model.job.JobStatusMaster;
import org.mysys.model.job.LocationMasterView;
import org.mysys.model.job.NoticePeriodDropdownListVw;
import org.mysys.model.job.RecruiterMaster;
import org.mysys.model.job.RecuGrpMapVw;
import org.mysys.model.job.RecuGrpMstListView;
import org.mysys.model.job.SkillCatMst;
import org.mysys.model.job.SkillCatMstListVw;
import org.mysys.model.job.SkillDomainMst;
import org.mysys.model.job.SkillDomainMstDropDownVw;
import org.mysys.model.job.SkillDomainMstListVw;
import org.mysys.model.job.SkillGroupListVw;
import org.mysys.model.job.SkillGroupMst;
import org.mysys.model.job.SkillMst;
import org.mysys.model.job.SkillMstDropDownVw;
import org.mysys.model.job.SourceMst;
import org.mysys.model.job.SourceMstDropDown;
import org.mysys.model.job.SpocMst;
import org.mysys.model.job.SpocShareFormat;
import org.mysys.model.job.StateMasterView;
import org.mysys.model.job.TaskAndReminder;
import org.mysys.model.job.UserListVw;
import org.mysys.repository.UserRepository;
import org.mysys.utility.DashboardCountStatusVO;
import org.mysys.utility.Email;
import org.mysys.utility.EmailThreadUtility;
import org.mysys.utility.SMSThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class JobService extends AbstractService{
	
	/**** Start Job Master ****/
	

	String isActive = "Y";
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired 
	RestTemplate restTemplate;
	
   /**********    Find duplicate JobCode of Job master     ***********/
	public Integer getJobCode(String jobCode){
		return jobMasterRepo.findduplicateByJobCode(jobCode);
	}
	
	/**********    Find duplicate JobCode of Job master on edit    ***********/
	public Integer checkDuplicateJobMaster(String jobCode, Long jmId) {
		Integer dupl = jobMasterRepo.findduplicateByJobCodeonEdit(jobCode,jmId);
		return dupl;
	}

	@Transactional
	public JobMaster saveJobMaster(JobMaster jobMaster){
		List<JobLocation> jobLocation = null;
		List<JobSkill> jobSkill = null;
		List<JobRecuMap> jobRecuMap=null;
		List<JobEdu> jobEdu = null;
		List<JobCert> jobCert = null;
		List<JobSpoc> jobSpoc = null;
		JobMaster old_job_master = null;
		int update_score = 0;
		
		if(jobMaster.getJobLocation() != null && jobMaster.getJobLocation().size() > 0) {
			jobLocation= new ArrayList<JobLocation>(jobMaster.getJobLocation());
			jobMaster.getJobLocation().clear();
		}

		if(jobMaster.getJobSkills() != null && jobMaster.getJobSkills().size() > 0) {
			jobSkill= new ArrayList<JobSkill>(jobMaster.getJobSkills());
			jobMaster.getJobSkills().clear();
		}
		
		if(jobMaster.getJobRecuMap() != null && jobMaster.getJobRecuMap().size() > 0) {
			jobRecuMap= new ArrayList<JobRecuMap>(jobMaster.getJobRecuMap());
			jobMaster.getJobRecuMap().clear();
		}
		
		if(jobMaster.getJobEdu() != null && jobMaster.getJobEdu().size() > 0) {
			jobEdu= new ArrayList<JobEdu>(jobMaster.getJobEdu());
			jobMaster.getJobEdu().clear();
		}
		
		if(jobMaster.getJobCert() != null && jobMaster.getJobCert().size() > 0) {
			jobCert= new ArrayList<JobCert>(jobMaster.getJobCert());
			jobMaster.getJobCert().clear();
		}
		
		if(jobMaster.getJobSpoc() != null && jobMaster.getJobSpoc().size() > 0) {
			jobSpoc= new ArrayList<JobSpoc>(jobMaster.getJobSpoc());
			jobMaster.getJobSpoc().clear();
		}
		
		if(!StringUtils.isEmpty(jobMaster.getJobDesc())) {
			jobMaster.getJobDesc().trim();
		}
	
		// call db function if job is updated to update matching score
		if(jobMaster.getJmId() != null && jobMaster.getJmId() > 0){
			
			//fetch old JM
			old_job_master = jobMasterRepo.findByjmId(jobMaster.getJmId());
			if(old_job_master != null){
				// OLD JOBMASTER FOUND 
				if(old_job_master.getCtc() != null && old_job_master.getCtc() > 0){
					if(jobMaster.getCtc() != null){
						if(jobMaster.getCtc() != old_job_master.getCtc()){
							//jobMatchWeightRepo.updateScore( (int) getLoggedInUserUserId(), old_job_master.getJmId().intValue(), "U",0);
							update_score++;
						}
					}
				}
				
				if(old_job_master.getJoiningPeriod()!= null && old_job_master.getJoiningPeriod() > 0){
					if(jobMaster.getJoiningPeriod() != null){
						if(jobMaster.getJoiningPeriod() != old_job_master.getJoiningPeriod()){
							//jobMatchWeightRepo.updateScore( (int) getLoggedInUserUserId(), old_job_master.getJmId().intValue(), "U",0);
							update_score++;
						}
					}
				}
				
				if(old_job_master.getMinTotalExp() != null){
					if(jobMaster.getMinTotalExp() != null){
						if(jobMaster.getMinTotalExp() != old_job_master.getMinTotalExp()){
							update_score++;
						}
					}
				}
				if(old_job_master.getMaxTotalExp() != null){
					if(jobMaster.getMaxTotalExp() != null){
						if(jobMaster.getMaxTotalExp() != old_job_master.getMaxTotalExp()){
							update_score++;
						}
					}
				}
				if(old_job_master.getMinRelExp() != null){
					if(jobMaster.getMinRelExp() != null){
						if(jobMaster.getMinRelExp() != old_job_master.getMinRelExp()){
							update_score++;
						}
					}
				}
				
			}
			
			if(update_score > 0)
				jobMatchWeightRepo.updateScore( (int) getLoggedInUserUserId(), old_job_master.getJmId().intValue(), "U",0);
			
		}else{
			//NEW JOB MASTER
			// UPDATE JOB MATCH WEIGHT
			
			jobMaster.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			jobMaster = this.jobMasterRepo.save(jobMaster);
			
			jobMatchWeightRepo.updateScore( (int) getLoggedInUserUserId() , (int) jobMaster.getJmId().intValue(), "I",0);
			System.err.println("IT WORKED , INSERT");
		}

		if(old_job_master != null)			
			jobMaster.setModifiedby(String.valueOf(getLoggedInUserUserId()));
		else
			jobMaster.setCreatedby(String.valueOf(getLoggedInUserUserId()));
		
		jobMaster = this.jobMasterRepo.save(jobMaster);
		
		jobLocationRepo.deleteByJobMasterId(jobMaster.getJmId());
		if(jobLocation !=null && jobLocation.size() > 0 )
		{
			for(JobLocation lc: jobLocation) {
				lc.setSiteid(getLoggedInUserSiteId());
				lc.setJmId(jobMaster.getJmId());
				lc.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			/* if(jobMaster.getJmId() >= 0) { jobLocationRepo.updateByJmId(jmId); } */
			jobLocationRepo.saveAll(jobLocation);
		}
		
		
		jobSkillRepo.deleteByJobMasterId(jobMaster.getJmId());
		if(jobSkill !=null && jobSkill.size() > 0 ) 
		{
			for(JobSkill lc: jobSkill) {
				lc.setSiteid(getLoggedInUserSiteId());
				lc.setJmId(jobMaster.getJmId());
				lc.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			/* if(jobMaster.getJmId() >= 0) { jobSkillRepo.updateByJmId(jmId); } */
			jobSkillRepo.saveAll(jobSkill);
		}
		
        jobRecuMapRepo.deleteByJobMasterId(jobMaster.getJmId());
		if(jobRecuMap !=null && jobRecuMap.size() > 0 )
		{
			for(JobRecuMap lc: jobRecuMap) {
				lc.setSiteid(getLoggedInUserSiteId());
				lc.setJmId(jobMaster.getJmId());
				lc.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			/* if(jobMaster.getJmId() >= 0) { jobRecuMapRepo.updateByJmId(jmId); } */
			jobRecuMapRepo.saveAll(jobRecuMap);
		}
		
		jobEduRepo.deleteByJobMasterId(jobMaster.getJmId());
		if(jobEdu !=null && jobEdu.size() > 0 )
		{
			for(JobEdu lc: jobEdu) {
				lc.setSiteid(getLoggedInUserSiteId());
				lc.setJmId(jobMaster.getJmId());
				lc.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			/* if(jobMaster.getJmId() >= 0) { jobEduRepo.updateByJmId(jmId); } */
			jobEduRepo.saveAll(jobEdu);
		}
		
		jobCertRepo.deleteByJobMasterId(jobMaster.getJmId());
		if(jobCert !=null && jobCert.size() > 0 ) 
		{
			for(JobCert lc: jobCert) {
				lc.setSiteid(getLoggedInUserSiteId());
				lc.setJmId(jobMaster.getJmId());
				lc.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			/* if(jobMaster.getJmId() >= 0) { jobCertRepo.updateByJmId(jmId); }*/
			jobCertRepo.saveAll(jobCert);
		}
		
		jobSpocRepo.deleteByJobMasterId(jobMaster.getJmId());
		if(jobSpoc !=null && jobSpoc.size() > 0 ) 
		{
			for(JobSpoc lc: jobSpoc) {
				lc.setSiteid(getLoggedInUserSiteId());
				lc.setJmId(jobMaster.getJmId());
				lc.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			/* if(jobMaster.getJmId() >= 0) { jobSpocRepo.updateByJmId(jmId); } */
			jobSpocRepo.saveAll(jobSpoc);
		}
		
		jobMaster.setJobRecuMap(jobRecuMap);
		return jobMaster;
	}
	


//	@Transactional
	public JobMaster getJobMaster(Long jmId)
	{
		 JobMaster jobmaster = jobMasterRepo.findByjmId(jmId);
		 List<JobLocation> locs = jobmaster.getJobLocation().stream().filter(p-> p.getIsActive().equals("Y")).collect(Collectors.toList());
		 jobmaster.getJobLocation().clear();
		 jobmaster.setJobLocation(locs);
		 
		 List<JobSkill> jobsk = jobmaster.getJobSkills().stream().filter(p-> p.getIsActive().equals("Y")).collect(Collectors.toList());

		 jobmaster.getJobSkills().clear();
		 
		 jobmaster.setJobSkills(jobsk);
		 
		 
		 List<JobRecuMap> jobrecu = jobmaster.getJobRecuMap().stream().filter(p-> p.getIsActive().equals("Y")).collect(Collectors.toList());

		 jobmaster.getJobRecuMap().clear();
		 jobmaster.setJobRecuMap(jobrecu);

		 List<JobEdu> jobEd = jobmaster.getJobEdu().stream().filter(p-> p.getIsActive().equals("Y")).collect(Collectors.toList());

		 jobmaster.getJobEdu().clear();
		 jobmaster.setJobEdu(jobEd);
		 
		 List<JobCert> jobcert = jobmaster.getJobCert().stream().filter(p-> p.getIsActive().equals("Y")).collect(Collectors.toList());

		 jobmaster.getJobCert().clear();
		 jobmaster.setJobCert(jobcert);
		 
		 List<JobSpoc> jobSpoc = jobmaster.getJobSpoc().stream().filter(p-> p.getIsActive().equals("Y")).collect(Collectors.toList());

		 jobmaster.getJobSpoc().clear();
		 jobmaster.setJobSpoc(jobSpoc);
		 
		 
		 return jobmaster;
	}


	/**** End Job Master ****/

	@Transactional
	public List<LocationMasterView> getAllCityData()
	{
		List<LocationMasterView> allCityName = this.locationMasterViewRepo.findByIsActive(isActive);

		return allCityName;
	}

	@Transactional
	public List<SkillMstDropDownVw> getAllSkillData()
	{
		List<SkillMstDropDownVw> allSkillName = this.skillMstDropDownVwRepo.findByIsActive(isActive);
		return allSkillName;
	}
	
	@Transactional
	public List<JobStatusMaster> getAllJobStatusData()
	{
		List<JobStatusMaster> allStatus = this.jobStatusMasterRepo.getAllStatus();
		return allStatus;
	}
	
	@Transactional
	public List<DomainMstDropDownVw> getAllCompanyNameData()
	{
		List<DomainMstDropDownVw> getallCompanyName = this.clientMasterRepo.findByIsActive(isActive);
		return getallCompanyName;
	}
	
	@Transactional
	public List<RecruiterMaster> getAllRecruiterData()
	{
		 List<RecruiterMaster> allRecruiterData = this.recruiterMasterRepo.getallrecruiter();
		return allRecruiterData;
	}
	
	@Transactional
	public List<ClientSpoc> getAllClientSpocData(Long clId)
	{
		 List<ClientSpoc> getallSpocData = this.clientSpocRepo.getallSpocData(clId);
		 return getallSpocData;
		
	}
	@Transactional
	public SkillDomainMst saveSkillDomainMst(SkillDomainMst skillDomainMst) {
		skillDomainMst.setSiteid(getLoggedInUserSiteId());
		SkillDomainMst tmpSkillDomainMst = this.skillDomainMstRepo.save(skillDomainMst);
		return tmpSkillDomainMst;
	}
	
	public Integer getSkillDomainMst(String skillDomainName) {
		return skillDomainMstRepo.findduplicateBySkillDomainName(skillDomainName);
	}

	public Integer checkDuplicateSkillDomainMst(String skillDomainName, long sdmId) {
		Integer dupl = skillDomainMstRepo.findduplicateBySkillDomainNameonEdit(skillDomainName,sdmId);
		return dupl;
	}
	
	@Transactional
	public SkillDomainMst getSkillDomainMst(Long sdmId) {
		SkillDomainMst tmpSkillDomainMst = this.skillDomainMstRepo.findBySdmId(sdmId);
		return tmpSkillDomainMst;
	}
	
	@Transactional
	public List<SkillDomainMstDropDownVw> getAllSkillDomainData()
	{
		return this.skillDomainMstDropDownVwRepo.findByIsActive(isActive);
	}

//	@PreAuthorize("hasRole('SKILLCATMST_ADD') or hasRole('SKILLCATMST_EDIT') ")
	@Transactional
	public SkillCatMst saveSkillCatMst(SkillCatMst skillCatMst) {
		skillCatMst.setSiteid(getLoggedInUserSiteId());
		SkillCatMst tmpSkillCatMst = this.skillCatMstRepo.save(skillCatMst);
		return tmpSkillCatMst;
	}
	
	public Integer getSkillCatMst(String skillCatName) {
		return skillCatMstRepo.findduplicateBySkillCatName(skillCatName);
	}
	
	public Integer checkDuplicateSkillCatMst(String skillCatName, long scmId) {
		Integer dupl = skillCatMstRepo.findduplicateBySkillCatNameonEdit(skillCatName,scmId);
		return dupl;
	}

//	@PreAuthorize("hasRole('SKILLCATMST_VIEW') or hasRole('SKILLCATMST_EDIT') ")
	@Transactional
	public SkillCatMst getSkillCatMst(Long scmId) {
		SkillCatMst tmpSkillCatMst = this.skillCatMstRepo.findByScmId(scmId);
		return tmpSkillCatMst;
	}

	@Transactional
	public SkillGroupMst saveSkillGroupMst(SkillGroupMst skillGroupMst) {
		skillGroupMst.setSiteid(getLoggedInUserSiteId());
		SkillGroupMst tmpSkillGroupMst = this.skillGroupMstRepo.save(skillGroupMst);
		return tmpSkillGroupMst;
	}
	
	public Integer getSkillGroupMst(String skillGroupName) {
		return skillGroupMstRepo.findduplicateBySkillGroupName(skillGroupName);
	}
	
	public Integer checkDuplicateSkillGroupMst(String skillGroupName, long sgmId) {
		Integer dupl = skillGroupMstRepo.findduplicateBySkillGroupNameonEdit(skillGroupName,sgmId);
		return dupl;
	}

	@Transactional
	public SkillGroupMst getSkillGroupMst(Long sgmId) {
		SkillGroupMst tmpSkillGroupMst = this.skillGroupMstRepo.findBySgmId(sgmId);
		return tmpSkillGroupMst;
	}
	
	
	
	@Transactional
	public List<DashboardKPIServiceVw> getAllDashboardJobs()
	{
		// fetch all jobs by rm_id and displid
		return this.dashBoardServiceRepo.getAllKPI(1,(int) getLoggedInUserUserId());
	}
	
	
	@Transactional
	public List<DashboardKPIServiceDetailVw> getDashboardJobDetail(Long jmId)
	{
		// fetch job detail by jm_id
		return this.dashboardDetailRepo.getDetailKPI(jmId);
	}

	@Transactional
	public SkillMst saveSkillMst(SkillMst skillMst) {
		skillMst.setSiteid(getLoggedInUserSiteId());
		SkillMst tmpSkillMst = this.skillMstRepo.save(skillMst);
		return tmpSkillMst;
	}
	
	public Integer getSkillMst(String skillName) {
		return skillMstRepo.findduplicateBySkillName(skillName);
	}
	
	
	public Integer checkDuplicateSkillMst(String skillName, long smId) {
		Integer dupl = skillMstRepo.findduplicateBySkillNameonEdit(skillName,smId);
		return dupl;
	}

	@Transactional
	public SkillMst getSkillMst(Long smId) {
		SkillMst tempSkillMst = this.skillMstRepo.findBySmId(smId);
		return tempSkillMst;
	}
	
	
	@Transactional
	public List<SkillCatMstListVw> getSkillCatMstListVw() {
		List<SkillCatMstListVw> skillCatMstListVw = skillCatMstListVwRepo.findByIsActive(isActive);
		return skillCatMstListVw;
	}
	
	@Transactional
	public List<SkillDomainMstListVw> getSkillDomainMstListVw() {
		List<SkillDomainMstListVw> skillDomainMstListVw = skillDomainMstListVwRepo.findByIsActive(isActive);
		return skillDomainMstListVw;
	}
	
	@Transactional
	public List<SkillGroupListVw> getSkillGroupListVw() {
		List<SkillGroupListVw> skillGroupListVw = skillGroupListVwRepo.findByIsActive(isActive);
		return skillGroupListVw;
	}
	@Transactional
	public List<NoticePeriodDropdownListVw> getNoticePeriodDropdownListVw() {
		List<NoticePeriodDropdownListVw> noticePeriod = noticePerDropListVwRepo.findAll();
		return noticePeriod;
	}
	
	@Transactional
	public List<ExperienceDropDownVw> getExperienceDropDownVw() {
		List<ExperienceDropDownVw> experience = experienceDropVwRepo.findAll();
		return experience;
	}
	
	@Transactional
	public List<EduMstListVw> getEduMstListVw() {
		List<EduMstListVw> education = eduMstListVwRepo.findByIsActive(isActive);
		return education;
	}
	
	@Transactional
	public ApplicantMaster saveApplicantMaster(ApplicantMaster applicantMaster)
	{
		List<ApplicantPrefLocation> applicantPrefLocation = null;
		List<ApplicantSkill> applicantSkill = null;
		List<ApplicantEducation> applicantEducation = null;
		List<ApplicantExperience> applicantExperience = null;
		List<ApplicantCertificate> applicantCertificates = null;
		List<ApplicantResume> applicantResume = null;
		ApplicantMaster old_appli_master = null;
		if(applicantMaster.getAmId() <= 0) 
			applicantMaster.setCreatedby(String.valueOf(getLoggedInUserUserId()));
		else
			applicantMaster.setModifiedby(String.valueOf(getLoggedInUserUserId()));
		
		String abc = "Add Job Applicant";
		String jAppicant = applicantMaster.getTabName();
		Long jmId = applicantMaster.getJmId();
		
		if(applicantMaster.getApplicantPrefLocation() != null && applicantMaster.getApplicantPrefLocation().size() > 0) {
			applicantPrefLocation= new ArrayList<ApplicantPrefLocation>(applicantMaster.getApplicantPrefLocation());
			applicantMaster.getApplicantPrefLocation().clear();
		}

		if(applicantMaster.getApplicantSkill() != null && applicantMaster.getApplicantSkill().size() > 0) {
			applicantSkill= new ArrayList<ApplicantSkill>(applicantMaster.getApplicantSkill());
			applicantMaster.getApplicantSkill().clear();
		}
		
		if(applicantMaster.getApplicanteducation() != null && applicantMaster.getApplicanteducation().size() > 0) {
			applicantEducation= new ArrayList<ApplicantEducation>(applicantMaster.getApplicanteducation());
			applicantMaster.getApplicanteducation().clear();
		}

		if(applicantMaster.getApplicantexperience() != null && applicantMaster.getApplicantexperience().size() > 0) {
			applicantExperience = new ArrayList<ApplicantExperience>(applicantMaster.getApplicantexperience());
			applicantMaster.getApplicantexperience().clear();
		}

		if(applicantMaster.getApplicantCertificate() != null && applicantMaster.getApplicantCertificate().size() > 0){
			applicantCertificates = new ArrayList<>(applicantMaster.getApplicantCertificate());
			applicantMaster.getApplicantCertificate().clear();
		}
		
		if(applicantMaster.getApplicantImage() !=null) {
		applicantMaster.setApplicantImage(applicantMaster.getApplicantImage());
		}
		
		if(applicantMaster.getAppliResume() != null && applicantMaster.getAppliResume().size() > 0) {
			applicantResume= new ArrayList<ApplicantResume>(applicantMaster.getAppliResume());
			applicantMaster.getAppliResume().clear();
		}
		
		int update_score = 0;
		// call db function if Applicant is updated to update matching score
			if(applicantMaster.getAmId() != null && applicantMaster.getAmId() > 0){
				
				//fetch old AM
				old_appli_master = applicantMasterRepo.findByAmId(applicantMaster.getAmId());
				if(old_appli_master != null){
					// OLD JOBAPPLICANT FOUND 
					
					if(old_appli_master.getCurrentCtc() != null){
						if(applicantMaster.getCurrentCtc() != null){
							if(applicantMaster.getCurrentCtc() != old_appli_master.getCurrentCtc()){
								update_score++;
							}
						}
					}
					
					if(old_appli_master.getNoticePeriod() != null && old_appli_master.getNoticePeriod() > 0){
						if(applicantMaster.getNoticePeriod() != null){
							if(applicantMaster.getNoticePeriod() != old_appli_master.getNoticePeriod()){
								update_score++;
							}
						}
					}
					
					if(old_appli_master.getTotalexp() != null){
						if(applicantMaster.getTotalexp() != null){
							if(applicantMaster.getTotalexp() != old_appli_master.getTotalexp()){
								update_score++;
							}
						}
					}
					
					if(old_appli_master.getApplicantPrefLocation() != null){
						if(applicantMaster.getApplicantPrefLocation() != null){
							if(applicantMaster.getApplicantPrefLocation() != old_appli_master.getApplicantPrefLocation()){
								update_score++;
							}
						}
					}
					
					if(old_appli_master.getRelExp() != null){
						if(applicantMaster.getRelExp() != null){
							if(applicantMaster.getRelExp() != old_appli_master.getRelExp()){
								update_score++;
							}
						}
					}
					
					if(old_appli_master.getApplicantSkill() != null){
						if(applicantMaster.getApplicantSkill() != null){
							if(applicantMaster.getApplicantSkill() != old_appli_master.getApplicantSkill()){
								update_score++;
							}
						}
					}
					
				}
				
				if(update_score > 0)
					appliInsertJobScoreRepo.updateApplicantScore( (int) getLoggedInUserUserId(), 0 , (int) applicantMaster.getAmId().intValue(), "U");
					applicantMaster = this.applicantMasterRepo.save(applicantMaster);
				
			}else{
				//NEW Applicant MASTER
				// UPDATE APPLICANT MATCH WEIGHT
				applicantMaster = this.applicantMasterRepo.save(applicantMaster);
				
				appliInsertJobScoreRepo.updateApplicantScore( (int) getLoggedInUserUserId(), 0 , (int) applicantMaster.getAmId().intValue(), "I");
				System.err.println("IT WORKED , INSERT");
			}
			
		
		
		if(abc.equals(jAppicant)) {
			long recid = getLoggedInUserUserId() ; //jobRecuMapRepo.getByJmId(jmId);
			long csm = 1;
			JobApplicant ja = new JobApplicant();
			ja.setAmId(applicantMaster.getAmId());
			ja.setJmId(jmId);
			ja.setCurrctc(applicantMaster.getCurrentCtc());
			ja.setExpctc(applicantMaster.getExpCtc());
			ja.setNoticeperiod(applicantMaster.getNoticePeriod());
			ja.setSiteid(getLoggedInUserSiteId());
			ja.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			ja.setRecId(recid);
			ja.setCsmId(csm);
			jobapplicantRepo.save(ja);
		}
		
        
		if(applicantPrefLocation !=null && applicantPrefLocation.size() > 0 )
		{
			Long amId = applicantMaster.getAmId();
			for(ApplicantPrefLocation lc: applicantPrefLocation) {
				lc.setSiteid(getLoggedInUserSiteId());
				lc.setAmId(applicantMaster.getAmId());
				lc.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			if(applicantMaster.getAmId() >= 0) {
				applicantPrefLocationRepo.updateByAmId(amId);
			}
			applicantPrefLocationRepo.saveAll(applicantPrefLocation);
		}
		
		//applicantSkillRepo.deleteByAmId(getLoggedInUserSiteId());
		if(applicantSkill !=null && applicantSkill.size() > 0 ) 
		{
			Long amId = applicantMaster.getAmId();
			for(ApplicantSkill lc: applicantSkill) {
				lc.setSiteid(getLoggedInUserSiteId());
				lc.setAmId(applicantMaster.getAmId());
				lc.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			
			if(applicantMaster.getAmId() >= 0) {
				applicantSkillRepo.updateByAmId(amId);
			}
			applicantSkillRepo.saveAll(applicantSkill);
		}
		
		if(applicantEducation != null && applicantEducation.size() > 0)
		{
			Long amId = applicantMaster.getAmId();
			for(ApplicantEducation ae: applicantEducation) {
				ae.setSiteid(getLoggedInUserSiteId());
				ae.setAmId(applicantMaster.getAmId());
				ae.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			if(applicantMaster.getAmId() >= 0) {
				applicanteducationRepo.updateByAmId(amId);
			}
			applicanteducationRepo.saveAll(applicantEducation);
		}
		
		
		
		if(applicantExperience != null && applicantExperience.size() > 0)
		{
			Long amId = applicantMaster.getAmId();
			for(ApplicantExperience ae: applicantExperience) {
				ae.setAm_id(applicantMaster.getAmId());
				ae.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			if(applicantMaster.getAmId() >= 0) {
				applicantexpRepo.updateByAmId(amId);
			}
		
			applicantexpRepo.saveAll(applicantExperience);
		}
		
		
		if(applicantCertificates != null && applicantCertificates.size() > 0)
		{
			Long amId = applicantMaster.getAmId();
			for(ApplicantCertificate ae: applicantCertificates) {
				ae.setAm_id(applicantMaster.getAmId());
				ae.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			if(applicantMaster.getAmId() >= 0) {
				applicantCertRepo.updateByAmId(amId);
			}
		
			applicantCertRepo.saveAll(applicantCertificates);
		}
		
		if(applicantResume != null && applicantResume.size() > 0)
		{
			Long amId = applicantMaster.getAmId();
			for(ApplicantResume ar: applicantResume) {
				ar.setSiteid(getLoggedInUserSiteId());
				ar.setAmId(applicantMaster.getAmId());
				ar.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			if(applicantMaster.getAmId() >= 0) {
				applicantResumeRepo.updateByAmId(amId);
			}
			applicantResumeRepo.saveAll(applicantResume);
		}
		
		
		return applicantMaster;
		
	}
	
	public ApplicantMasterWeb saveApplicantMasterWeb(ApplicantMasterWeb applicantMasterWeb)
	{
		List<ApplicantPrefLocation> applicantPrefLocation = null;
		List<ApplicantSkill> applicantSkill = null;
		List<ApplicantEducation> applicantEducation = null;
		List<ApplicantExperience> applicantExperience = null;
		List<ApplicantCertificate> applicantCertificates = null;
		Long site_id = new Long(0);
        
		//applicantMasterWeb.setIsActive("1");
		if(applicantMasterWeb.getApplicantPrefLocation() != null && applicantMasterWeb.getApplicantPrefLocation().size() > 0) {
			applicantPrefLocation= new ArrayList<ApplicantPrefLocation>(applicantMasterWeb.getApplicantPrefLocation());
			applicantMasterWeb.getApplicantPrefLocation().clear();
		}

		if(applicantMasterWeb.getApplicantSkill() != null && applicantMasterWeb.getApplicantSkill().size() > 0) {
			applicantSkill= new ArrayList<ApplicantSkill>(applicantMasterWeb.getApplicantSkill());
			applicantMasterWeb.getApplicantSkill().clear();
		}
		
		if(applicantMasterWeb.getApplicanteducation() != null && applicantMasterWeb.getApplicanteducation().size() > 0) {
			applicantEducation= new ArrayList<ApplicantEducation>(applicantMasterWeb.getApplicanteducation());
			applicantMasterWeb.getApplicanteducation().clear();
		}

		if(applicantMasterWeb.getApplicantexperience() != null && applicantMasterWeb.getApplicantexperience().size() > 0) {
			applicantExperience = new ArrayList<ApplicantExperience>(applicantMasterWeb.getApplicantexperience());
			applicantMasterWeb.getApplicantexperience().clear();
		}

		if(applicantMasterWeb.getApplicantCertificate() != null && applicantMasterWeb.getApplicantCertificate().size() > 0){
			applicantCertificates = new ArrayList<>(applicantMasterWeb.getApplicantCertificate());
			applicantMasterWeb.getApplicantCertificate().clear();
		}
		
		if(applicantMasterWeb.getApplicantImage() !=null) {
			applicantMasterWeb.setApplicantImage(applicantMasterWeb.getApplicantImage());
		}
		if(applicantMasterWeb.getApplicantResume() !=null) {
			applicantMasterWeb.setApplicantResume(applicantMasterWeb.getApplicantResume());
		}
		
		applicantMasterWeb.setCreatedby(String.valueOf(getLoggedInUserUserId()));
		applicantMasterWeb = this.applicantMasterWebRepo.save(applicantMasterWeb);
        
		if(applicantPrefLocation !=null && applicantPrefLocation.size() > 0 )
		{
			Long amId = applicantMasterWeb.getAmId();
			for(ApplicantPrefLocation lc: applicantPrefLocation) {
				lc.setSiteid(site_id);
				lc.setAmId(applicantMasterWeb.getAmId());
				lc.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			if(applicantMasterWeb.getAmId() >= 0) {
				applicantPrefLocationRepo.updateByAmId(amId);
			}
			applicantPrefLocationRepo.saveAll(applicantPrefLocation);
		}
		
		//applicantSkillRepo.deleteByAmId(getLoggedInUserSiteId());
		if(applicantSkill !=null && applicantSkill.size() > 0 ) 
		{
			Long amId = applicantMasterWeb.getAmId();
			for(ApplicantSkill lc: applicantSkill) {
				lc.setSiteid(site_id);
				lc.setAmId(applicantMasterWeb.getAmId());
				lc.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			
			if(applicantMasterWeb.getAmId() >= 0) {
				applicantSkillRepo.updateByAmId(amId);
			}
			applicantSkillRepo.saveAll(applicantSkill);
		}
		
		if(applicantEducation != null && applicantEducation.size() > 0)
		{
			Long amId = applicantMasterWeb.getAmId();
			for(ApplicantEducation ae: applicantEducation) {
				ae.setSiteid(site_id);
				ae.setAmId(applicantMasterWeb.getAmId());
				ae.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			if(applicantMasterWeb.getAmId() >= 0) {
				applicanteducationRepo.updateByAmId(amId);
			}
			applicanteducationRepo.saveAll(applicantEducation);
		}
		
		
		
		if(applicantExperience != null && applicantExperience.size() > 0)
		{
			Long amId = applicantMasterWeb.getAmId();
			for(ApplicantExperience ae: applicantExperience) {
				ae.setAm_id(applicantMasterWeb.getAmId());
				ae.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			if(applicantMasterWeb.getAmId() >= 0) {
				applicantexpRepo.updateByAmId(amId);
			}
			applicantexpRepo.saveAll(applicantExperience);
		}
		
		
		if(applicantCertificates != null && applicantCertificates.size() > 0)
		{
			Long amId = applicantMasterWeb.getAmId();
			for(ApplicantCertificate ae: applicantCertificates) {
				ae.setAm_id(applicantMasterWeb.getAmId());
				ae.setCreatedby(String.valueOf(getLoggedInUserUserId()));
			}
			if(applicantMasterWeb.getAmId() >= 0) {
				applicantCertRepo.updateByAmId(amId);
			}
			applicantCertRepo.saveAll(applicantCertificates);
		}
		
		
		return applicantMasterWeb;
		
	}
	
	public ApplicantMaster getApplicantMaster(Long amId)
	{
		ApplicantMaster applicantMaster = applicantMasterRepo.findByamId(amId);
		
			if(applicantMaster.getApplicantImage() !=null) {
			applicantMaster.setApplicantImage(applicantMaster.getApplicantImage());
			}
			
		 List<ApplicantResume> applResume = applicantMaster.getAppliResume().stream().filter(p-> p.getIsActive().equals("Y")).collect(Collectors.toList());
		 applicantMaster.getAppliResume().clear();
		 applicantMaster.setAppliResume(applResume);
		
		 List<ApplicantPrefLocation> appperloc = applicantMaster.getApplicantPrefLocation().stream().filter(p-> p.getIsActive().equals("Y")).collect(Collectors.toList());
		 applicantMaster.getApplicantPrefLocation().clear();
		 applicantMaster.setApplicantPrefLocation(appperloc);
		 
		 List<ApplicantSkill> appskill = applicantMaster.getApplicantSkill().stream().filter(p-> p.getIsActive().equals("Y")).collect(Collectors.toList());

		 applicantMaster.getApplicantSkill().clear();
		 applicantMaster.setApplicantSkill(appskill);
		 
		 List<ApplicantEducation> appedu = applicantMaster.getApplicanteducation().stream().filter(p-> p.getIsactive().equals("Y")).collect(Collectors.toList());

		 applicantMaster.getApplicanteducation().clear();
		 applicantMaster.setApplicanteducation(appedu);
		 
		 List<ApplicantCertificate> appcert = applicantMaster.getApplicantCertificate().stream().filter(p-> p.getIsactive().equals("Y")).collect(Collectors.toList());

		 applicantMaster.getApplicantCertificate().clear();
		 applicantMaster.setApplicantCertificate(appcert);
		 
		 List<ApplicantExperience> appexper = applicantMaster.getApplicantexperience().stream().filter(p-> p.getIsactive().equals("Y")).collect(Collectors.toList());

		 applicantMaster.getApplicantexperience().clear();
		 applicantMaster.setApplicantexperience(appexper);
		 
		 return applicantMaster;
	
	}
	
	@Transactional
	public List<SourceMstDropDown> getAllSourceType()
	{
		return this.sourceMstDropDownRepo.findByIsActive(isActive);
	}
	
	@Transactional
	public List<StateMasterView> getAllState()
	{
		String isActive = "Y";
		return this.stateMasterViewRepo.findByIsActive(isActive);
	}

	@Transactional
	public EduMst saveEduMst(EduMst eduMst) {
		eduMst.setSiteid(getLoggedInUserSiteId());
		EduMst eduM = this.eduMstRepo.save(eduMst);
		return eduM;
	}

	public Integer getEduMst(String eduLevel) {
		return eduMstRepo.findduplicateByEduLevel(eduLevel);
	}
	
	public Integer checkDuplicateEduMst(String eduLevel, long emId) {
		Integer dupl = eduMstRepo.findduplicateByEduLevelonEdit(eduLevel,emId);
		return dupl;
	}

	@Transactional
	public EduMst getEduMst(Long emId) {
		EduMst tempEduMst =  this.eduMstRepo.findByEmId(emId);
		return tempEduMst;
	}

	@Transactional
	public SourceMst saveSourceMst(SourceMst sourceMst) {
		sourceMst.setSiteid(getLoggedInUserSiteId()); 
		SourceMst tempSourceMst = this.sourceMstRepo.save(sourceMst);
		return tempSourceMst;
	}

	public Integer getSourceMst(String sourceName) {
		return sourceMstRepo.findduplicateBySourceName(sourceName);
	}
	
	public Integer checkDuplicateSourceMst(String sourceName, long somId) {
		Integer dupl = sourceMstRepo.findduplicateBySourceNameonEdit(sourceName,somId);
		return dupl;
	}

	@Transactional
	public SourceMst getSourceMst(Long somId) {
		SourceMst sourceMst = this.sourceMstRepo.findBySomId(somId);
		return sourceMst;
	}
	
	@Transactional
	public List<RecuGrpMstListView> getGrpType(String grpType) {
		List<RecuGrpMstListView> findGrptype = this.recuGrpMstListViewRepo.findGrptypeAndIsActive(grpType,isActive);
		return findGrptype;

	}

	
	@Transactional
	public List<DashboardApplicantList> getDashboardApplicant(Long jmId) {
		List<DashboardApplicantList> temDashboardApplicantList = this.dashboardapplicantlistRepo.getDashboardApplicantList((int)getLoggedInUserUserId(), (int)getLoggedInUserSiteId(),jmId);
		return temDashboardApplicantList ;

	}

	@Transactional
	public List<RecuGrpMstListView> getAllRecuGrp() {
		List<RecuGrpMstListView> recugrp= this.recuGrpMstListViewRepo.findAll();
		return recugrp;

	}
	
	@Transactional
	public List<DashboardJobApplicantMatch> getDashboardApplicantMatch(Long jmId) {
		List<DashboardJobApplicantMatch> DashboardApplicantMatches = this.dashboardjobapplicantmatchRepo.getDashboardApplicantMatch((int)getLoggedInUserUserId(), (int)getLoggedInUserSiteId());
		return DashboardApplicantMatches;

	}
	
	
	@Transactional
	public List<JobApplicant> saveJobApplicant(List<JobApplicant> jobapplicant) {
		
		List<JobApplicant> res_jobs = new ArrayList<JobApplicant>();
		long recid = getLoggedInUserUserId(); //jobRecuMapRepo.getByJmId(jobapplicant.get(0).getJmId());
		long csm = 1;
		if(jobapplicant != null && jobapplicant.size() > 0) {
			for(JobApplicant ja : jobapplicant) {
				ja.setSiteid(getLoggedInUserSiteId());
				ja.setCreatedby(String.valueOf(getLoggedInUserUserId()));
				ja.setRecId(recid);
				ja.setCsmId(csm);
				JobApplicant tempJobApplicant = this.jobapplicantRepo.save(ja);
				res_jobs.add(tempJobApplicant);
				
				
			}
		}
		return res_jobs;
	}
	@Transactional
	public List<DashboardJobCount> getAllDashboardJobCount(Integer jmid) {
		return this.dashboardJobCountRepo.getAllDashboardJobCount((int) getLoggedInUserUserId(),(int) 1,(int) jmid );
	}
	
	/* Dashboard Count by Status & Month/Year API
	 * Use this to display Jobs & Applicant Count on Dashboard screen
	 *  */
	@Transactional
	public DashboardCountStatusVO getAllDashboardJobStatusCount(String status)
	{
		DashboardCountStatusVO res_vo = new DashboardCountStatusVO();
		List<DashboardJobStatusCount> res = this.dashboardJobStatusCountRepo.getAllDashboardJobStatusCount((int) getLoggedInUserUserId(),status);
		if(res != null && res.size() > 0){
			//get jobs count
			res_vo.setJobsCount(res.stream().filter(r -> r.getType().equals("J")).collect(Collectors.toList()));
			
			//get applicants count
			res_vo.setApplicantCount(res.stream().filter(r -> r.getType().equals("A")).collect(Collectors.toList()));
		}
		return res_vo;
	}
	
	public List<JobOpeningListVw> getAllJobOpeningList()
	{
		return this.jobOpeningListVwRepo.getAllJobOpeningList();
	}
	
	
	public List<RecuGrpMapVw> getAllrecugrp(Long rgm_id)
	{
		return this.recugrpmapVwRepo.getAllrecugrp(rgm_id);
	}

//	public List<EduMst> getAllEduMst() {
//		return eduMstRepo.findAll();
//	}
//	
	public List<CourseMstListVw> getAllCourseMstVw() {
		return coursemstlistvwRepo.findAll();
	}
	
	public List<InstituteMstVw> getAllinstMstVw() {
		return institutemstvwRepo.findAll();
	}
	
	
	public List<DashJobOpeningList> getJobOpeningList(int position) {
		return jobOpeningListRepo.getJobOpeningList((int) getLoggedInUserUserId(), position);
	}

	public List<CasesStatusMstVw> getCasesStatus() {
		return casesStatusMstVwRepo.findAll().stream().filter(c->c.getIsActive().equals("Y")).collect(Collectors.toList());
	}

	@Transactional
	public JobApplicant saveJobApplicantNotes(JobApplicant jobApplicant) {
		
		try {
			if(jobApplicant.getCsmId() != null && jobApplicant.getCsmId() > 0) {
				jobapplicantRepo.updateByCsmIdAndJaId(jobApplicant.getCsmId(),jobApplicant.getJaId(), String.valueOf(getLoggedInUserUserId()));
				
				try {
					JobApplicant ja = jobapplicantRepo.findByJaId(jobApplicant.getJaId());
					//get primary recruiter's mobile from user table
					if( ja != null){
						Set<String> mobiles = new HashSet<>();
						
						// get recruiters id
						List<JobRecuMap> rec_ids = jobRecuMapRepo.getRecMapByJmId(ja.getJmId());
						
						if(rec_ids != null){
							for(int i = 0; i<rec_ids.size(); i++){
								String mobile = userRepo.getMobile(rec_ids.get(i).getRecId());

								mobiles.add(mobile);	
							}
						}
						
						
						String updatedBy = "";
						if(!StringUtils.isEmpty(ja.getModifiedby())){
							updatedBy = userRepo.getName(Long.parseLong(ja.getModifiedby()));
						}
						
						String appl_name = applicantMasterRepo.getName(ja.getAmId());
						String msg = "", status = "";
						if(ja.getCsmId() != null){
							status = caseStatusMstRepo.getCaseStatusById( ja.getCsmId());
						}
						
						msg = "Status of " + appl_name + " has been changed to " + status + " by " +updatedBy;
						//send Noti/SMS
						Runnable runn = new SMSThread(mobiles,msg);
						ExecutorService exec = Executors.newFixedThreadPool(5);
						exec.execute(runn);
					}
				
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
			if(jobApplicant.getNegotiable() != null && jobApplicant.getNegotiable().length() > 0) {
				jobapplicantRepo.updateByNegotiableAndJaId(jobApplicant.getNegotiable(),jobApplicant.getJaId());
			}
			
			if(jobApplicant.getArmId() == null || jobApplicant.getArmId() == 0) {
				jobapplicantRepo.updateByArmIdAndJaId(0,jobApplicant.getJaId());
			}else
				jobapplicantRepo.updateByArmIdAndJaId(jobApplicant.getArmId(),jobApplicant.getJaId());
			
			if(jobApplicant.getAbomId() == null || jobApplicant.getAbomId() == 0) {
				jobapplicantRepo.updateByAbomIdAndJaId(0,jobApplicant.getJaId());
			}
			else
				jobapplicantRepo.updateByAbomIdAndJaId(jobApplicant.getAbomId(), jobApplicant.getJaId());
			
			if(jobApplicant.getJobApplNotes() != null && jobApplicant.getJobApplNotes().size() > 0 ) {
				for(JobApplNotes jan : jobApplicant.getJobApplNotes()) {
					jan.setJaId(jobApplicant.getJaId());
					if(!StringUtils.isEmpty(jan.getRemarks())) {
						jan.setCreatedby(String.valueOf(getLoggedInUserUserId()));
						jobApplNotesRepo.save(jan);
					}
				}
			}	
			
			jobApplicant.setModifiedby(String.valueOf(getLoggedInUserUserId()));
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
				
		return jobApplicant;
		
	}

	@Transactional
	public JobApplicant saveAppliInterview(JobApplicant jobApplicant) {
		try {
			if(jobApplicant.getJobInterview().get(0).getJaiId() > 0) {
				jobAppliInterRepo.updateByJaiIdAndJaId(jobApplicant.getJobInterview().get(0).getJaiId(),
						jobApplicant.getJobInterview().get(0).getSchDate(),
						jobApplicant.getJobInterview().get(0).getPanelName(),
						jobApplicant.getJobInterview().get(0).getPanelFeedback(),
						jobApplicant.getJobInterview().get(0).getIlmId(),
						jobApplicant.getJobInterview().get(0).getIsmId());
			}
			if(jobApplicant.getJobInterview() != null && jobApplicant.getJobInterview().size() > 0) {
				for(JobApplicantInterview ji : jobApplicant.getJobInterview()) {
					ji.setSiteid(getLoggedInUserSiteId());
					ji.setCreatedby(String.valueOf(getLoggedInUserUserId()));
					jobAppliInterRepo.save(ji);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return jobApplicant;
	}

	public List<JobApplicantInterview> getStatus(String status) {
		return jobAppliInterRepo.findByCsmId(status);
	}

	public List<JobApplNotesCommentsVw> getJobComm(long jaId) {
		return jobApplNotesCommentsVwRepo.findByJaId(jaId);
	}

	public Integer checkDuplicateApplicantMaster(String email, Long amId) {
		Integer dupl = applicantMasterRepo.findduplicateByInsEmailonEdit(email,amId);
		return dupl;
	}

	public Integer getApplicantMaster(String email) {
		return applicantMasterRepo.findduplicateByEmail(email);
	}

	public JobApplicant getJobAppli(long jaId) {
		
		JobApplicant ap = jobapplicantRepo.findByJaId(jaId);
		return ap;
	}

	//Reason of Rejection dropdown
	public List<ApplRejectionMst> getApplicantRejection() {
		return applRejectionMstRepo.findAll();
	}

	//Reason of Backing Out dropdown
	public List<ApplBackedOutMst> getApplicantBackOut() {
		return applBackedOutMstRepo.findAll();
	}

	//Get Interview Detail of Job Applicant
	@Transactional
	public JobApplicantInterview getInterviewDetail(Long jaiId) {
		return jobAppliInterRepo.findByJaiId(jaiId);
	}

	public List<SpocMst> getSpocName() {
		return spocMstRepo.findAll();
	}

	public List<InterviewStatusMst> getStatusInterview() {
		return interviewStatusMstRepo.findAll();
	}
	
	/* this method is used for cases listing data on dashboard */
	@Transactional
	public List<DashboardApplicantCases> getDashboardApplicantCases() {
		List<DashboardApplicantCases> getDashboardApplicantCases = this.dashboardapplicantcasesRepo.getDashboardApplicantCases((int)getLoggedInUserUserId());
		return getDashboardApplicantCases ;

	}
	
	/* this method is used for task and reminder data on dashboard */
	@Transactional
	public List<TaskAndReminder> getTaskAndReminder() {
		List<TaskAndReminder> getTaskAndReminder = this.taskandreminerRepo.getTaskAndReminder((int)getLoggedInUserUserId());
		return getTaskAndReminder ;

	}
	
	/* this method is used for getting data in usere manager in user form */
	@Transactional
	public List<UserListVw> getUserListVwRepo() {
		List<UserListVw> getUserListVwRepo = this.userlistvwRepo.getUserListVwRepo();
		return getUserListVwRepo ;

	}
	
	public ApplicantResume getApplicantResume(long amid) {
		return applicantResumeRepo.findByAmId(amid);
	}
	
	
//	public Email sendEmail(Email emailObj){
//		
//		String email = emailObj.getMailTo();
//		if(email != null) {
//			try {
//			ExecutorService executor  = Executors.newFixedThreadPool(1);
//				
//			Runnable emailThread = new EmailThreadUtility(emailObj.getMailTo(), emailObj.getMailSubject(), emailObj.getMailContent());
//			executor.execute(emailThread);
//			executor.shutdown();
//			
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		
//		}
//		return emailObj;
//		
//	}

	@Transactional
	public List<SpocShareFormat> getSpocShareFormat(Long spocid,Long fmid) {
		
		if(fmid == null)
			fmid = 0L;
			
		return spocShareFormatRepo.getSpocFormat(spocid.intValue(),(int) getLoggedInUserUserId() ,(int) getLoggedInUserSiteId(),fmid.intValue());
	}

	/* This method is used to check duplicate email when user type */
	@Transactional
	public Integer getAllApplicantEmail(String email, String phoneno){
		Integer applicantMaster = 0 ;
		if(!StringUtils.isEmpty(email)) {
			applicantMaster = this.applicantMasterRepo.findduplicateByEmail(email);
		}
		
		if(!StringUtils.isEmpty(phoneno)) {
			int apliMst = this.applicantMasterRepo.findduplicateByMobile(phoneno);
			if(apliMst > 0) {
				applicantMaster = 1;
			}
		}
		return applicantMaster ;
	}

	
	// update resume record in db
	public void updateResumeRecord(Long amId,String path) {
		
		try {
			ApplicantResume fdm = applicantResumeRepo.findByAmId(amId);
			if(fdm != null){
				fdm.setResumeName(path);
				applicantResumeRepo.save(fdm);
			}else{
				ApplicantResume ar = new ApplicantResume();
				ar.setAmId(amId);
				ar.setResumeName(path);
				ar.setSiteid(getLoggedInUserSiteId());
				ar.setIsActive("Y");
				applicantResumeRepo.save(ar);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Transactional
	public List<FormatSpocVw> getSpocFormatView(Long clId) {
		
		return formatSpocViewRepo.getAllFormats((int) getLoggedInUserSiteId(),clId);
	}

	
//	public int sendNewJobPUSH(List<JobRecuMap> jobRecumaps, JobMaster jobMaster){
//		
//	try {
//		Set<String> mobiles = new HashSet<>();
//	for (JobRecuMap jobRecuMap1 : jobRecumaps) {
//		User user = userRepository.findByUseridAndSiteid(jobRecuMap1.getRecId(), jobMaster.getSiteId());
//		mobiles.add(user.getMobile());
//		 String email = user.getEmail();
//		if(email != null) {
//		String message = "Dear " + user.getFirstname()+ ",<br>"
//				+ "<pre>             You have been assigned a new job to explore candidates."
//				+ "                  Job Title     :   "+ jobMaster.getJobTitle()+ "  " + jobMaster.getNoOfPosition() +"Position/s </pre>"
//			    + "                  Company       :   "+jobMaster.getCompanySpoc()+""
//			    + "                  Notice Period :   "+jobMaster.getJoiningPeriod()+""
//			    + "                  Budget        :"+jobMaster.getBudget()+""
//			    + "                  Spoc          :"+jobMaster.getCompanySpoc()+","+jobMaster.getCompanySpocEmail()+","+jobMaster.getCompanySpocPhone()+""
//			    + jobMaster.getCreatedby();
//		
//			try {
//			ExecutorService executor  = Executors.newFixedThreadPool(20);
//			
//			Runnable emailThread = new EmailThreadUtility(email, "New job assigned", message);
//			executor.execute(emailThread);
//			
//			 executor.shutdown();
//			// Wait until all threads are finish
//			while (!executor.isTerminated()) {
//
//			}
//				
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
//	
//	if(mobiles != null){
//	
//		String createdBy = "";
//		if(!StringUtils.isEmpty(jobMaster.getCreatedby())){
//			createdBy = userRepo.getName(Long.parseLong(jobMaster.getCreatedby()));
//		}
//		
//		String comapny = "";
//		if(jobMaster.getClId() != null){
//			ClientMst client  = clientMstRepo.findByClId(jobMaster.getClId());
//			if(client != null){
//				comapny = client.getCompanyName();
//			}
//		}
//		
//	String msg = "You have been assigned a new job opening "+jobMaster.getJobTitle()+" for "+comapny+" by "+createdBy+"";	
//		
//	//send Noti/SMS
//	Runnable runn = new SMSThread(mobiles,msg);
//	ExecutorService exec = Executors.newFixedThreadPool(10);
//	exec.execute(runn);
//	}
//	
//	return 1;
//	} catch (Exception e) {
//		e.printStackTrace();
//		return 0;
//	}
//		
//	}
	
	
	
	public int sendEmail(Email emailObj){
		
		try {
			
			String email = emailObj.getMailTo();
			if(email != null) {
				String message = emailObj.getMailSubject();
				try {
				Email map = new Email();
				map.setMailSubject(message);
				map.setMailTo(email);
				map.setMailContent(emailObj.getMailContent());
				map.setMailFrom(emailObj.getMailFrom());
				URI uri = new URI("http://164.52.211.113:8080/sendMail");

//				URI uri = new URI("http://localhost:8080/sendMail");
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				
				HttpEntity<Email> entity = new HttpEntity<Email>(map,headers);
				
			
				ResponseEntity<String> result =	restTemplate.postForEntity(uri,entity, String.class);
				if(result != null){
					System.out.println("RESULT " +result.getStatusCodeValue());
					System.out.println(result.getBody());
					if(!StringUtils.isEmpty(result.getBody()) && result.getBody().equalsIgnoreCase("success") ){
						return 1;
					}
				}
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public List<InterviewLevelMst> getInterviewLevel() {
		return interviewLevelMstRepo.findAll();
	}
	
}
