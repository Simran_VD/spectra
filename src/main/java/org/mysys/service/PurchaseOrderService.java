package org.mysys.service;

import javax.transaction.Transactional;


import org.mysys.repository.CitymasterRepo;
import org.mysys.repository.ContactRepo;
import org.mysys.repository.StateRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class PurchaseOrderService extends AbstractService {

	@Autowired
	private ContactRepo contactRepo;
	
	@Autowired
	private CitymasterRepo citymasterRepo;
	
	@Autowired
	private StateRepo stateRepo;
	
	
	@Autowired
	private Environment env;
	
	public String getContactAddress(long id){
		return contactRepo.getContactAddressByID(id);
	}
	
	public String getContactStateID(long id){
		return contactRepo.getContactStateID(id);
	}
	
	@Transactional
	public String getCityByID(Long id) {
		return citymasterRepo.getOne(id).getCityName();
	}
	
	@Transactional
	public String getStateByID(Long id) {
		return stateRepo.getOne(id).getStatename();
	}
	
	
}
