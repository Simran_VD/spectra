package org.mysys.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mysys.model.ContactVo;
import org.mysys.model.Department;
import org.mysys.model.DropdownVO;
import org.mysys.model.Citymaster;
import org.mysys.model.User;
import org.mysys.model.Service.ServiceLeadVw;
import org.mysys.model.job.ClientMst;
import org.mysys.model.job.CompanyMst;
import org.mysys.model.job.CompanyMstVw;
import org.mysys.model.job.CourseMst;
import org.mysys.model.job.DomainDropdownVw;
import org.mysys.model.job.DomainMst;
import org.mysys.model.job.InstituteMst;
import org.mysys.model.job.MatchWeightCfg;
import org.mysys.model.job.PositionMst;
import org.mysys.model.job.PositionMstVw;
import org.mysys.model.job.RecuGrpMst;
import org.mysys.model.job.SpocMst;
import org.mysys.model.job.SpocMstListVw;
import org.mysys.model.job.UserTypeMst;
import org.mysys.model.job.UserTypeMstListVw;
import org.mysys.model.notification.NotifyListVw;
//import org.mysys.model.customer.Suggestion;
//import org.mysys.model.notification.NotifyListVw;
import org.mysys.model.notification.ServiceLeadNotifyVw;
//import org.mysys.model.partner.LeadSourceMstVw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class MasterService extends AbstractService 
{
	@Autowired
	AdminService adminService;

	private static final Logger LOGGER = LogManager.getLogger();

	public Page<Citymaster> findPages(int pageNo,int pageSize){
		return citymasterRepo.findAll(PageRequest.of(pageNo, pageSize));
	}

	public List<Citymaster> getAllCity(){
		String status="Y";
		List<Citymaster> ls= citymasterRepo.findByIsactive(status);
        Collections.sort(ls, (s1,s2)->s1.getCityName().compareToIgnoreCase(s2.getCityName()));
		return ls;
	}
//	public List<LocMst> findCityByState(long stateId){
//		return locMstRepo.findByStatemaster_stateidOrderByCitynameAsc(stateId);
//	}
	
	public List<Citymaster> findCity(){
		return citymasterRepo.findAll();
	}

	public Map<String,List<DropdownVO>> getDropDownData(){
		Map<String,List<DropdownVO>> dropdownData = new HashMap<>();
		dropdownData.put("role", sortIt(roleRepo.findAll().stream().map((a)->{return new DropdownVO(String.valueOf(a.getId()),a.getRolename()); }).collect(Collectors.toList())));
		dropdownData.put("state", sortIt(stateRepo.findAll().stream().map((a)->{return new DropdownVO(String.valueOf(a.getStateid()),a.getStatename()); }).collect(Collectors.toList())));
		dropdownData.put("site", sortIt(siteRepo.findAll().stream().map((a)->{return new DropdownVO(String.valueOf(a.getSiteid()),a.getSitename());}).collect(Collectors.toList())));
		return dropdownData;
	}







	/** Customer Product Master **/
	public Map<String,List<DropdownVO>> getItemAndParty(){
		Map<String,List<DropdownVO>> dropdownData = new HashMap<>();
		dropdownData.put("parties", contactRepo.findAll().stream().map(DropdownVO::new).sorted(dropDownComparator).collect(Collectors.toList()));
		return dropdownData;
	}

	public Map<String,List<DropdownVO>> getParties(){
		Map<String,List<DropdownVO>> dropdownData = new HashMap<>();
		dropdownData.put("parties", contactRepo.findAll().stream().map(DropdownVO::new).sorted(dropDownComparator).collect(Collectors.toList()));
		return dropdownData;
	}

	@SuppressWarnings("deprecation")
	public List<ContactVo> getContacts(Long contactroleid){
		Sort sort = new Sort(new Sort.Order(Direction.ASC, "displayname"));
		if(contactroleid == null) {
			return contactVoRepo.findAll(sort);
		} else {
			return contactVoRepo.findByContactroleidAndSiteid(contactroleid, sort,getLoggedInUserSiteId());
		}
	}



	@Transactional
	public List<DropdownVO> getAllDistinctVendorList() {
		return sortIt(commonRepo.findAllDistinctVendorList(getLoggedInUserSiteId()).stream().map((a) -> {
			return new DropdownVO(String.valueOf(a[0]), String.valueOf(a[1]));
		}).collect(Collectors.toList()));
	}

	@Transactional
	public List<DropdownVO> getAllItemCodeList(Integer contactId) {
		LOGGER.debug("AllItemCodeList w.r.t. contactId={}", contactId);
		return sortIt(commonRepo.findAllItemCodeList(contactId, getLoggedInUserSiteId()).stream().map((a) -> {
			return new DropdownVO(String.valueOf(a[2]), String.valueOf(a[0]));
		}).collect(Collectors.toList()));
	}

	@PreAuthorize("hasRole('MASTERDATA_ADD') or hasRole('MASTERDATA_EDIT') ")
	public long saveDepartment(String departmentName) {
		Department savedDept = deptRepo.findByDeptnameAndSiteid(departmentName, getLoggedInUserSiteId());
		if(savedDept != null){
			savedDept.setDeptid(-100L);
		}else{
			savedDept = new Department();
			savedDept.setSiteid(getLoggedInUserSiteId());
			savedDept.setDeptname(departmentName);
			savedDept.setStartdate(new Date());
			deptRepo.save(savedDept);
		}
		return savedDept.getDeptid();
	}

	@PreAuthorize("hasRole('DEPARTMENT_ADD') or hasRole('DEPARTMENT_EDIT') ")
	@Transactional
	public Department saveDetailDept(Department department) {
		Department tmpDepartment = deptRepo.save(department);
		return tmpDepartment;
	}


	@PreAuthorize("hasRole('DEPARTMENT_VIEW') or hasRole('DEPARTMENT_EDIT') ")
	@Transactional
	public Department getDept(Long deptId) {
		Department tmpDepartment = deptRepo.findById(deptId).get();
		return getDepartmentOnly(tmpDepartment);
	}


	private Department getDepartmentOnly(Department dept) {
		dept.setSiteid(getLoggedInUserSiteId());
		return dept;
	}


	@Transactional
	public List<DropdownVO> getItemSpecifications(long itemId) {
		return commonRepo.getItemSpecifications(itemId).stream().map((a) -> {
			return new DropdownVO(String.valueOf(a[0]), String.valueOf(a[1]));
		}).collect(Collectors.toList());
	}

	/**************** Start :transporter**********************/
	@Transactional
	public List<DropdownVO> getDepartments() {
		return sortIt(deptRepo.findAll().stream().map((a) -> {
			return new DropdownVO(String.valueOf(a.getDeptid()), a.getDeptname());
		}).collect(Collectors.toList()));
	}


	
//	@Transactional
//	public List<LeadSourceMstVw> getLeadSource() {
//		return leadSourceRepo.findByUserid(getLoggedInUserUserId());
//	}
//
//	@Transactional
//	public Suggestion saveSuggestion(Suggestion sugges) {
//		return suuggRepo.save(sugges);
//	}

	/*
	 * @Transactional public List<NotifyListVw> getNotification() { //return
	 * notifyRepo.findByRecUserId(getLoggedInUserUserId()); return new
	 * NotifyListVw(); }
	 */
	
	
	@Transactional
	public List<ServiceLeadNotifyVw> getServiceLeadNotify() {
		return servLeadNotifyRepo.findByUserId(getLoggedInUserUserId());
	}
	
	@Transactional
	public String notifyAt(String itemIds) {
		String result = "fail";
		List<Long> itemids=Arrays.asList(itemIds.split(",")).stream().map((str)->Long.valueOf(str)).collect(Collectors.toList());
		Integer i= notificationRepo.updateNotifyAt(new Date(),itemids,getLoggedInUserSiteId());
		if(i > 0 ) {
			result="Success";
		}
		return result;	
	}	

	@Transactional
	public String notifyAtMobile(String itemIds) {
		String result = "fail";
		List<Long> itemids=Arrays.asList(itemIds.split(",")).stream().map((str)->Long.valueOf(str)).collect(Collectors.toList());
		Integer i= notificationRepo.updateNotifyAtMobile(new Date(),itemids,0L);
		if(i > 0 ) {
			result="Success";
		}
		return result;	
	}	
	
	@Transactional
	public String serviceSeenDate(String itemIds) {
		String result = "fail";
		List<Long> itemids=Arrays.asList(itemIds.split(",")).stream().map((str)->Long.valueOf(str)).collect(Collectors.toList());
		Integer i= notificationRepo.updateSeenAtMobile(new Date(),itemids,0L);
		if(i > 0 ) {
			result="Success";
		}
		return result;	
	}	
	
	
	@Transactional
	public String bellAt(String itemIds) {
		String result = "fail";
		List<Long> itemids=Arrays.asList(itemIds.split(",")).stream().map((str)->Long.valueOf(str)).collect(Collectors.toList());
		Integer i= notificationRepo.updateBellAt(new Date(),itemids,getLoggedInUserSiteId());
		if(i > 0 ) {
			result="Success";
		}
		return result;	
	}	
	
	@Transactional
	public int seenAt(Long id) {
		return notificationRepo.updateSeenAt(new Date(),id,getLoggedInUserSiteId());
	}	
	
	@Transactional
	public List<NotifyListVw> getAllNotification() {
		return notifyRepo.findByRecUserId(getLoggedInUserUserId());
	}
	
//	public List<NotifyListVw> getAllNotification(String token) {
//		return notifyRepo.findByRecUserId(userRepo.findUserId(token));
//	}
	
	
//	@Transactional
//	public List<DashboardKPIServiceVw> getDashBoardKPI() {
//		return dashBoardServiceRepo.getAllKPI();
//	}
	
	
	@Transactional
	public List<ServiceLeadVw> getDrilDown(Long amId, Long tmId) {
	    User u= userRepo.findByLoginid(getLoggedInUserLoinId());
		return serviceLeadVwRepo.findByUserIdAndAmIdAndTmId((int)u.getUserid(),amId.intValue() , tmId.intValue());
	}
	
	
	@Transactional
	public List<ServiceLeadVw> getDrilDown1(Long pmId, Long tmId) {
	    User u= userRepo.findByLoginid(getLoggedInUserLoinId());
		return serviceLeadVwRepo.findByUserIdAndPmIdAndTmId((int)u.getUserid(),pmId.intValue() , tmId.intValue());
	}
	
	@Transactional
	public List<ServiceLeadVw> getDrilDown2(Long statusId, Long tmId) {
	    User u= userRepo.findByLoginid(getLoggedInUserLoinId());
		return serviceLeadVwRepo.findLeadWiseAndPmIdAndTmId((int)u.getUserid(),statusId.intValue() , tmId.intValue());
	}

	/***********************  Recruiter Form Starts Here   *******************************/
					/************User Type Master****************/
					  /*******User Type Master Saveing**********/
	
	String isActive = "Y";
	
	@Transactional
	public UserTypeMst saveUserTypeMst(UserTypeMst userTypeMst) {
		userTypeMst.setSiteid(getLoggedInUserSiteId());
		UserTypeMst tempUserTypeMst = this.userTypeMstRepo.save(userTypeMst);
		return tempUserTypeMst;
	}

	/*******User Type Master Check Duplicate**********/
	@Transactional
	public Integer getUserTypeMst(String userType) {
		return userTypeMstRepo.findduplicateByUserType(userType);
	}

	/*******User Type Master Get IN Edit View**********/
	@Transactional
	public UserTypeMst getUserTypeMst(Long typeId) {
		UserTypeMst userTypeMst = this.userTypeMstRepo.findByTypeId(typeId);
		return userTypeMst;
	}

	/*******User Type Master List View**********/
	@Transactional
	public List<UserTypeMstListVw> getUserTypeMstListVw() {
		List<UserTypeMstListVw> userTypeMstListVw = userTypeMstListVwRepo.findAll();
		return userTypeMstListVw;
	}

	/*******Recruiter Group Master Save**********/
	@Transactional
	public RecuGrpMst saveRecuGrpMst(RecuGrpMst recuGrpMst) {
		recuGrpMst.setSiteId(getLoggedInUserSiteId());
		RecuGrpMst tempRecuGrpMst = this.recuGrpMstRepo.save(recuGrpMst);
		return tempRecuGrpMst;
	}

	/*******Recruiter Group Master Check Duplicate**********/
	@Transactional
	public Integer getRecuGrpMst(String recuGrpName) {
		return recuGrpMstRepo.findduplicateByRecuGrpName(recuGrpName);
	}
	
	/*******Recruiter Group Master Get IN Edit View**********/ 
	@Transactional
	public RecuGrpMst getRecuGrpMst(Long rgmId) {
		RecuGrpMst tmpRecuGrpMst = this.recuGrpMstRepo.findByRgmId(rgmId);
		return tmpRecuGrpMst;
	}

	/*******Company Master Save**********/
	@Transactional
	public CompanyMst saveCompanyMst(CompanyMst companyMst) {
		companyMst.setSiteid(getLoggedInUserSiteId());
		CompanyMst tempCompanyMst = this.companyMstRepo.save(companyMst);
		return tempCompanyMst;
	}

	/*******Company Master Check Duplicate**********/
	public Integer getCompanyMst(String companyName) {
		return companyMstRepo.findduplicateByCompanyName(companyName);
	}
	
	public Integer checkDuplicateCompanyMst(String companyName, long cmId) {
		Integer dupl =	companyMstRepo.findduplicateByCompanyNameonEdit(companyName,cmId);
		return dupl;
	}
	
	/*******Company Master Get IN Edit View**********/ 	
	public CompanyMst getCompanyMst(Long cmId) {
		CompanyMst tmpCompanyMst = this.companyMstRepo.findByCmId(cmId);
		return tmpCompanyMst;
	}

	/*******Client Master Save**********/
	@Transactional
	public ClientMst saveClientMst(ClientMst clientMst) {
		clientMst.setSiteid(getLoggedInUserSiteId());
		ClientMst tempClientMst = this.clientMstRepo.save(clientMst);
		return tempClientMst;
	}

	/*******Client Master Check Duplicate**********/
	@Transactional
	public Integer getClientMst(String companyName) {
		return clientMstRepo.findduplicateByCompanyName(companyName);
	}
	
	/*******Client Master Get IN Edit View**********/ 
	@Transactional
	public ClientMst getClientMst(Long clId) {
		ClientMst tmpClientMst = this.clientMstRepo.findByClId(clId);
		return tmpClientMst;
	}

	@Transactional
	public List<DomainDropdownVw> getDomainDropdownVw() {
		List<DomainDropdownVw>  domainDropdownVw = domainDropdownVwRepo.findAll();
		return domainDropdownVw;
	}

	/*******Match Weight Cfg Save**********/
	@Transactional
	public MatchWeightCfg saveMatchWeightCfg(MatchWeightCfg matchWeightCfg) {
		matchWeightCfg.setSiteid(getLoggedInUserSiteId());
		MatchWeightCfg tempMatchWeightCfg = matchWeightCfgRepo.save(matchWeightCfg);
		return tempMatchWeightCfg;
	}
	
	/*******Match Weight Cfg Check Duplicate**********/
	@Transactional
	public Integer getMatchWeightCfg(String paramName) {
		return matchWeightCfgRepo.findduplicateByParamName(paramName);
	}
	
	/*******Match Weight Cfg  Get IN Edit View**********/
	@Transactional
	public MatchWeightCfg getMatchWeightCfg(Long paramId) {
		MatchWeightCfg tmpMatchWeightCfg = this.matchWeightCfgRepo.findByParamId(paramId);
		return tmpMatchWeightCfg;
	}
	
	/*******Domain Mst save**********/
	@Transactional
	public DomainMst saveDomainMst(DomainMst domainMst) {
		domainMst.setSiteid(getLoggedInUserSiteId());
		DomainMst tempDomainMst = domainMstRepo.save(domainMst);
		return tempDomainMst;
	}

	/*******Domain Mst check duplicate**********/
	@Transactional
	public Integer getDomainMst(String domainName) {
		return domainMstRepo.findduplicateByDomainName(domainName);
	}

	/*******Domain Mst check edit view**********/
	@Transactional
	public DomainMst getDomainMst(Long dmId) {
		DomainMst tmpDomainMst = domainMstRepo.findByDmId(dmId);
		return tmpDomainMst;
	}

	/*******Course Mst save**********/
	@Transactional
	public CourseMst saveCourseMst(CourseMst courseMst) {
		courseMst.setSiteid(getLoggedInUserSiteId());
		CourseMst tempCourseMst = courseMstRepo.save(courseMst);
		return tempCourseMst;
	}

	/*******Course Mst check duplicate**********/
	@Transactional
	public Integer getCourseMst(String courseName) {
		return courseMstRepo.findduplicateByCourseName(courseName);
	}

	/*******Course Mst check edit view**********/
	@Transactional
	public CourseMst getCourseMst(Long coId) {
		CourseMst tmpCourseMst = courseMstRepo.findByCoId(coId);
		return tmpCourseMst;
	}

	/*******Spoc Mst save**********/
	@Transactional
	public SpocMst saveSpocMst(SpocMst spocMst) {
		spocMst.setSiteid(getLoggedInUserSiteId());
		SpocMst tempSpocMst = spocMstRepo.save(spocMst);
		return tempSpocMst;
	}

	/*******Domain Mst check duplicate**********/
	@Transactional
	public Integer getSpocMst(String spocName) {
		return spocMstRepo.findduplicateBySpocName(spocName);
	}
	
	/*******Spoc Mst check edit view**********/
	@Transactional
	public SpocMst getSpocMst(Long spocId) {
		SpocMst tmpSpocMst = spocMstRepo.findBySpocId(spocId);
		return tmpSpocMst;
	}
	
	@Transactional
	public List<SpocMstListVw> getSpocMstListVw(Long clId) {
		List<SpocMstListVw> tmpSpocMstListVw = spocMstListVwRepo.findByClId(clId);
		return tmpSpocMstListVw;
	}
	
	/*******check Duplicate on edit Time**********/
	public Integer checkDuplicateMatchWeightCfg(String paramName,Long paramid) {
		Integer dupl = matchWeightCfgRepo.findduplicateByParamNameonEdit(paramName, paramid);
		return dupl;
	}

	public Integer checkDuplicateDomainMst(String domainName, Long dmId) {
		Integer duplDm = domainMstRepo.findduplicateByDomainNameonEdit(domainName,dmId);
		return duplDm;
	}

	public Integer checkDuplicateCourseMst(String courseName, Long coId) {
		Integer dupl = courseMstRepo.findduplicateByCourseNameonEdit(courseName,coId);
		return dupl;
	}

	public Integer checkDuplicateSpocMst(String spocName, Long spocId) {
		Integer dupl = spocMstRepo.findduplicateBySpocNameonEdit(spocName,spocId);
		return dupl;
	}

	public Integer checkDuplicateClientMst(String companyName, Long clId) {
		Integer dupl = clientMstRepo.findduplicateByCompanyNameonEdit(companyName,clId);
		return dupl;
	}

	public Integer checkDuplicateRecuGrpMst(String recuGrpName, long rgmId) {
		Integer dupl = recuGrpMstRepo.findduplicateByRecuGrpNameonEdit(recuGrpName,rgmId);
		return dupl;
	}

	public Integer checkDuplicateUserTypeMst(String userType, long typeId) {
		Integer dupl =	userTypeMstRepo.findduplicateByUserTypeonEdit(userType,typeId);
		return dupl;
	}

	public Integer checkDuplicateInstituteMst(String insName, Long imId) {
		Integer dupl = instituteMstRepo.findduplicateByInsNameonEdit(insName,imId);
		return dupl;
	}

	public InstituteMst saveInstituteMst(InstituteMst instituteMst) {
		instituteMst.setSiteid(getLoggedInUserSiteId());
		InstituteMst tempInstituteMst = instituteMstRepo.save(instituteMst);
		return tempInstituteMst;
	}

	public Integer getInstituteMst(String insName) {
		return instituteMstRepo.findduplicateByInsName(insName);
	}

	public InstituteMst getInstituteMst(Long imId) {
		InstituteMst tmpInstituteMst = instituteMstRepo.findByImId(imId);
		return tmpInstituteMst;
	}

	public Integer checkDuplicatePositionMst(String positionName, Long pmId) {
		Integer dupl = positionMstRepo.findduplicateByPositionNameonEdit(positionName,pmId);
		return dupl;
	}

	public PositionMst savePositionMst(PositionMst positionMst) {
		positionMst.setSiteid(getLoggedInUserSiteId());
		PositionMst tempPositionMst = positionMstRepo.save(positionMst);
		return tempPositionMst;
	}

	public Integer getPositionMst(String positionName) {
		return positionMstRepo.findduplicateByPositionName(positionName);
	}

	public PositionMst getPositionMst(Long pmId) {
		PositionMst tmpPositionMst = positionMstRepo.findByPmId(pmId);
		return tmpPositionMst;
	}

	public List<CompanyMstVw> getCompanyMstVw() {
		List<CompanyMstVw> tempCompanyMstVw = companyMstVwRepo.findByIsActive(isActive);
		return tempCompanyMstVw;
	}

	public List<PositionMstVw> getPositionMstVw() {
		List<PositionMstVw> tempPositionMstVw = positionMstVwRepo.findByIsActive(isActive);
		return tempPositionMstVw;
	}

	@Transactional
	public List<SpocMstListVw> getSpocMasterListVw() {
		List<SpocMstListVw> tmpSpocMstListVw = spocMstListVwRepo.findAll();
		return tmpSpocMstListVw;
	}


	
}
