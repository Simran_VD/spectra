package org.mysys.service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.mysys.model.PartnerTeamVw;
//import org.mysys.model.PartnerToNotifyVw;
import org.mysys.model.Service.ServiceCharges;
//import org.mysys.model.partner.AreaMst;
//import org.mysys.model.partner.AreaVw;
//import org.mysys.model.partner.PartnerAreaMap;
//import org.mysys.model.partner.PartnerMst;
//import org.mysys.model.partner.PartnerMstVw;
//import org.mysys.model.partner.PartnerServiceMap;
//import org.mysys.model.partner.PartnerTeam;
//import org.mysys.model.partner.PincodeMstVw;
//import org.mysys.model.partner.ServiceMst;
//import org.mysys.model.partner.ServiceMstVw;
//import org.mysys.model.product.ProductPartMst;
//import org.mysys.repository.PartnerToNotifyVwRepo;
import org.mysys.repository.Service.ServiceChargesRepo;
//import org.mysys.repository.partner.AreaMstRepo;
//import org.mysys.repository.partner.PartnerTeamVwRepo;
//import org.mysys.repository.partner.PincodeMstVwRepo;
//import org.mysys.repository.partner.ServiceMstRepo;
//import org.mysys.repository.product.ProductPartMstRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class PartnerService extends AbstractService{

	@Autowired
	protected AdminService adminService;
	
//	@Autowired
//	protected PartnerTeamVwRepo partnerTeamVwRepo;
	
	private static final Logger LOGGER = LogManager.getLogger();

//	@Transactional
//	public List<AreaVw> getArea(Long cityid){
//		String isActive="Y";
//		return areaVwRepo.findByIsActiveAndCityid(isActive,cityid);
//	}
//	
//	@Transactional
//	public List<PincodeMstVw> getPincode(Long cityid){
//		return pincodeMstVwRepo.findByCityId(cityid);
//	}


	
//	@Transactional
//	public List<PartnerMstVw> getPartnerList(){
//		return partMstVwRepo.findByUserid(getLoggedInUserUserId());
//	}

//	@Transactional
//	public List<ServiceMstVw> getpProdServices(){
//		String isActive="Y";
//		return productServiceRepo.findByIsActive(isActive);
//	}

//	@PreAuthorize("hasRole('PARTNERMST_ADD') or hasRole('PARTNERMST_EDIT')")
//	@Transactional
//	public PartnerMst savePartner(PartnerMst partnerMst) {
//		long pmId=partnerMst.getPmId();
//		List<PartnerAreaMap> partnerAreaMap =null;
//		List<PartnerServiceMap> partnerServiceMap =null;
//
//		if(partnerMst.getPartnerAreaMap().size() >0) {
//			partnerAreaMap=new LinkedList<PartnerAreaMap>(partnerMst.getPartnerAreaMap());
//			partnerMst.getPartnerAreaMap().clear();
//		}
//
//		if(partnerMst.getPartnerServiceMap().size() >0) {
//			partnerServiceMap=new LinkedList<PartnerServiceMap>(partnerMst.getPartnerServiceMap());
//			partnerMst.getPartnerServiceMap().clear();
//		} 
//
//		if(partnerMst.getPmId() <=0) {
//			partnerMst.setSiteid(getLoggedInUserSiteId());
//			partnerMst = partnerMstRepo.save(partnerMst);
//		} else {
//			PartnerMst pm=partnerMstRepo.findByPmId(partnerMst.getPmId());
//			partnerMst.setAdharImage(pm.getAdharImage());
//			partnerMst.setPanImage(pm.getPanImage());
//			partnerMst.setGstinImage(pm.getGstinImage());
//			partnerMst = partnerMstRepo.save(partnerMst);
//		}
//
//		partnerAreaMapRepo.deleteByPmId(partnerMst.getPmId());
//		if(partnerAreaMap !=null && partnerAreaMap.size()>0) {
//			for(PartnerAreaMap partAreamap:partnerAreaMap) {
//				partAreamap.setSiteid(getLoggedInUserSiteId());
//				partAreamap.setPmId(partnerMst.getPmId());
//				partAreamap.setIsActive("Y");
//			}
//			partnerAreaMapRepo.saveAll(partnerAreaMap);
//		}
//
//		partnerServiceRepo.deleteByPmId(partnerMst.getPmId());
//		if(partnerServiceMap !=null && partnerServiceMap.size()>0) {
//			for(PartnerServiceMap partServicemap:partnerServiceMap) {
//				partServicemap.setSiteid(getLoggedInUserSiteId());
//				partServicemap.setPmId(partnerMst.getPmId());
//				partServicemap.setIsActive("Y");
//			}
//			partnerServiceRepo.saveAll(partnerServiceMap);
//		}
//		
//		if(pmId <= 0) {
//			adminService.saveUsers(partnerMst);
//		}
//		
//		return partnerMst;
//	}
//
//	@PreAuthorize("hasRole('PARTNERMST_VIEW') or hasRole('PARTNERMST_EDIT')")
//	@Transactional
//	public PartnerMst gePartnerMst(long id) {
//		return partnerMstRepo.findByPmId(id);
//	}
//
//	@PreAuthorize("hasRole('PARTNERMST_DELETE')")
//	@Transactional
//	public String deletePartnerMst(long id) {
//		String result = "SUCCESS";
//		try{partnerAreaMapRepo.deleteByPmId(id);
//		partnerServiceRepo.deleteByPmId(id);
//		partnerMstRepo.deleteById(id);
//		}catch(DataIntegrityViolationException ex){
//			result = "DATA_INTEGRITY";
//		}catch(Exception ex){
//			result = "DATA_INTEGRITY";
//		}
//		return result;
//	}
//

//	@Transactional
//	public void savePartnerDoc(Long pmId, MultipartFile addhar, MultipartFile gstin, MultipartFile pan) throws IOException {
//		if(addhar !=null) {
//			byte[] adharByte=addhar.getBytes();
//			int i=partnerMstRepo.updateAdharPartnerDocs(adharByte,pmId);
//		} 
//		if(gstin !=null) {
//			byte[] gstinByte=gstin.getBytes();
//			int i=partnerMstRepo.updateGstinPartnerDocs(gstinByte,pmId);
//		}
//		if(pan !=null) {
//			byte[] panByte=pan.getBytes();
//			int i=partnerMstRepo.updatePanPartnerDocs(panByte,pmId);
//		}
//	}
	
	
	/****************************** Parner Team ****************************************/
	
//	@PreAuthorize("hasRole('PARTNER_TEAM_ADD') or hasRole('PARTNER_TEAM_EDIT')")
//	@Transactional
//	public PartnerTeam savePartnerTeam(PartnerTeam partnerTeam) {
//		long ptId=partnerTeam.getPtId();
//		if(partnerTeam.getPtId() <= 0) {
//			partnerTeam.setSiteid(getLoggedInUserSiteId());
//		} else {
//			PartnerTeam pt=	partnerTeamRepo.findByPtId(partnerTeam.getPtId());
//			partnerTeam.setAdharImage(pt.getAdharImage());
//		}
//		partnerTeamRepo.save(partnerTeam);
//		
//		if(ptId <= 0) {
//		  adminService.saveUsers(partnerTeam);
//		}
//
//		return partnerTeam;
//	}
//	
//	
//	@PreAuthorize("hasRole('PARTNER_TEAM_VIEW') or hasRole('PARTNER_TEAM_EDIT')")
//	@Transactional
//	public PartnerTeam getPartnerTeam(long ptId) {
//		return partnerTeamRepo.findByPtId(ptId);
//	}
//
//	@PreAuthorize("hasRole('PARTNER_TEAM_DELETE')")
//	@Transactional
//	public String deletePartnerTeam(long ptId) {
//		String result = "SUCCESS";
//		try{
//			partnerTeamRepo.deleteByPtId(ptId);
//		}catch(DataIntegrityViolationException ex){
//			result = "DATA_INTEGRITY";
//		}catch(Exception ex){
//			result = "DATA_INTEGRITY";
//		}
//		return result;
//	}
//
//	
//	@Transactional
//	public void savePartnerTeamDoc(Long ptId, MultipartFile addhar) throws IOException {
//		if(addhar !=null) {
//			byte[] adharByte=addhar.getBytes();
//			int i=partnerTeamRepo.updateAdharPartnerTeamDocs(adharByte,ptId);
//		} 
//	}
//	
//
//	@Transactional
//	public void deletePartnerDoc(Long id,String type ) {
//		if(type.equals("PanImg")) {
//		partnerMstRepo.deletePanPartnerDocs(id);
//		    }
//		if(type.equals("AdharImg")) {
//			partnerMstRepo.deleteAdharPartnerDocs(id);
//			}
//		if(type.equals("GstImg")) {
//			partnerMstRepo.deleteGstinPartnerDocs(id);
//			}
//	}
//	
//	@Transactional
//	public void deletePartnerteamDoc(Long id) {
//		partnerTeamRepo.deleteAdharPartnerDocs(id);
//	}
	
//	@Autowired
//	private ServiceMstRepo serviceRepo;
//	
//	@PreAuthorize("hasRole('SERVICE_MASTER_ADD') or hasRole('SERVICE_MASTER_EDIT')")
//	@Transactional
//	public ServiceMst saveServiceMst(ServiceMst servMst) {
//		if(servMst.getSmId() < 0) {
//			servMst.setSiteid(getLoggedInUserSiteId());
//		}
//		  return serviceRepo.save(servMst);
//	}
//	
//	
//	@PreAuthorize("hasRole('SERVICE_MASTER_VIEW') or hasRole('SERVICE_MASTER_EDIT')")
//	@Transactional
//	public ServiceMst getServiceMst(long smId) {
//		return serviceRepo.findBySmId(smId);
//	}
//
//	@PreAuthorize("hasRole('SERVICE_MASTER_DELETE')")
//	@Transactional
//	public String deleteServiceMst(long smId) {
//		String result = "SUCCESS";
//		try{
//			serviceRepo.deleteBySmId(smId);
//		}catch(DataIntegrityViolationException ex){
//			result = "DATA_INTEGRITY";
//		}catch(Exception ex){
//			result = "DATA_INTEGRITY";
//		}
//		return result;
//	}
//	
//	public List<ServiceMst> getListServiceMst(String serviceName) {
//		return serviceRepo.findByServiceNameAndSiteid(serviceName,getLoggedInUserSiteId());
//	}
//	
//	
//	@Autowired
//	private AreaMstRepo areaRepo;
//	
//	@PreAuthorize("hasRole('AREA_MASTER_ADD') or hasRole('AREA_MASTER_EDIT')")
//	@Transactional
//	public AreaMst saveAreaMst(AreaMst servMst) {
//		if(servMst.getAmId() < 0) {
//			servMst.setSiteid(getLoggedInUserSiteId());
//		}
//		  return areaRepo.save(servMst);
//	}
//	
//	
//	@PreAuthorize("hasRole('AREA_MASTER_VIEW') or hasRole('AREA_MASTER_EDIT')")
//	@Transactional
//	public AreaMst getAreaMst(long amId) {
//		return areaRepo.findByAmId(amId);
//	}
//
//	@PreAuthorize("hasRole('AREA_MASTER_DELETE')")
//	@Transactional
//	public String deleteAreaMst(long amId) {
//		String result = "SUCCESS";
//		try{
//			areaRepo.deleteByAmId(amId);
//		}catch(DataIntegrityViolationException ex){
//			result = "DATA_INTEGRITY";
//		}catch(Exception ex){
//			result = "DATA_INTEGRITY";
//		}
//		return result;
//	}
//	
//	public List<AreaMst> getListAreaMst(String areaName,String pincode) {
//		return areaRepo.findByAreaNameAndPincodeAndSiteid(areaName,pincode,getLoggedInUserSiteId());
//	}
//	
//	@Autowired
//	PincodeMstVwRepo pincodeVwRepo;
//	
//	public List<PincodeMstVw> getPincode() {
//		return pincodeVwRepo.findAll();
//	}
	
//	@Autowired
//    private ProductPartMstRepo productPartMstRepo ;
//	
//	@PreAuthorize("hasRole('PRODUCT_PART_MASTER_ADD') or hasRole('PRODUCT_PART_MASTER_EDIT')")
//	@Transactional
//	public ProductPartMst saveProdPartMst(ProductPartMst prodPartMst) {
//		if(prodPartMst.getPpId() < 0) {
//			prodPartMst.setSiteId(getLoggedInUserSiteId());
//		}
//		  return productPartMstRepo.save(prodPartMst);
//	}
//   
//	@PreAuthorize("hasRole('PRODUCT_PART_MASTER_VIEW') or hasRole('PRODUCT_PART_MASTER_EDIT')")
//	@Transactional
//	public ProductPartMst getProductPartMst(long ppId) {
//		return productPartMstRepo.findByPpId(ppId);
//	}
	
//	@PreAuthorize("hasRole('PRODUCT_PART_MASTER_DELETE')")
//	@Transactional
//	public String deleteProdPartMst(long ppId) {
//		String result = "SUCCESS";
//		try{
//			productPartMstRepo.deleteByPpId(ppId);
//		}catch(DataIntegrityViolationException ex){
//			result = "DATA_INTEGRITY";
//		}catch(Exception ex){
//			result = "DATA_INTEGRITY";
//		}
//		return result;
//	}
//
//	public List<ProductPartMst> getListProdPartMst(Long ppId) {
//		return productPartMstRepo.findByPpIdAndSiteId(ppId,getLoggedInUserSiteId());
//	}
	
	/****************************** service charges ****************************************/
	
	@Autowired
    private ServiceChargesRepo serviceChargesRepo ;
	
	@PreAuthorize("hasRole('SERVICE_CHARGES_ADD') or hasRole('SERVICE_CHARGES_EDIT')")
	@Transactional
	public ServiceCharges saveServiceCharges(ServiceCharges servCharges) {
		if(servCharges.getScId() < 0) {
			servCharges.setSiteId(getLoggedInUserSiteId());
		}
		  return serviceChargesRepo.save(servCharges);
	}
   
	@PreAuthorize("hasRole('SERVICE_CHARGES_VIEW') or hasRole('SERVICE_CHARGES_EDIT')")
	@Transactional
	public ServiceCharges getServiceCharges(long ScId) {
		return serviceChargesRepo.findByScId(ScId);
	}
	
	@PreAuthorize("hasRole('SERVICE_CHARGES_DELETE')")
	@Transactional
	public String deleteServiceCharges(long ScId) {
		String result = "SUCCESS";
		try{
			serviceChargesRepo.deleteByScId(ScId);
		}catch(DataIntegrityViolationException ex){
			result = "DATA_INTEGRITY";
		}catch(Exception ex){
			result = "DATA_INTEGRITY";
		}
		return result;
	}

	public List<ServiceCharges> getListServiceCharges(Long ScId) {
		return serviceChargesRepo.findByScIdAndSiteId(ScId,getLoggedInUserSiteId());
	}
    
	
//	
//	public List<PartnerTeamVw> getListPartnerTeamVw(Long pmId) {
//		return partnerTeamVwRepo.findByPmId(pmId);
//	}
//
//	@Autowired
//	PartnerToNotifyVwRepo partnerToNotifyVwRepo;
//	
//	public List<PartnerToNotifyVw> getListPartnerToNotifyVw(Long slId) {
//		return partnerToNotifyVwRepo.findBySlId(slId);
//	}


	
	
}
