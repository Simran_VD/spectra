package org.mysys.service;

import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.mysys.model.SMS;
import org.springframework.stereotype.Service;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

@Service("smsService")
public class SMSService {
	private static String authorization="5WRVlhydZQIe2zOoamBbpcuf0UNgHt8PSTnAq6Cjk4MEvrsiJwI6Np45m09gXwLuSeFZRtJfbCq3EQAy";
	
	private static String senderId="FSTSMS";
	private static String languge="english";//english // unicode

	public int sendSms(SMS s) {
		HttpResponse response = Unirest.post("https://www.fast2sms.com/dev/bulk")
				.header("authorization", authorization)
				.header("cache-control", "no-cache")
				.header("content-type", "application/x-www-form-urlencoded")
				.body("sender_id="+senderId+"&language="+languge+"&route=p&numbers="+s.getMobile()+"&message="+s.getMessage())
				.asString();
		
		return response.getStatus();
	}
//	
//	public int sendOTP(String mobile,String otp) {
//	  kong.unirest.HttpResponse<String> response1 = Unirest.post("https://www.fast2sms.com/dev/bulk")
//			  .header("authorization",authorization )
//			  .header("cache-control", "no-cache")
//			  .header("content-type", "application/x-www-form-urlencoded")
//			  .body("sender_id=FSTSMS&language=english&route=qt&numbers="+mobile+"&message=42906&variables={#BB#}&variables_values="+otp)
//			  .asString();
//	  return response1.getStatus();
//	}
	
	
	//send custom sms
		public static int sendCustomMsg(String mobile,String message) {
			
			if(!StringUtils.isEmpty(message)){
				message = URLEncoder.encode(message);
				  kong.unirest.HttpResponse<String> response1 = Unirest.post("https://www.fast2sms.com/dev/bulkV2")
						  .header("authorization",authorization )
						  .header("cache-control", "no-cache")
						  .header("content-type", "application/x-www-form-urlencoded")
						  .body("sender_id=TXTIND&language=english&route=v3&numbers="+mobile+"&message= "+message)
						  .asString();	
				  return response1.getStatus();
			}
			
			  return 0;
		}
		
		
		//send custom sms
		public static int sendCustomOTP(String mobile,String message) {
			
			if(!StringUtils.isEmpty(message)){
				message = URLEncoder.encode(message);
				  kong.unirest.HttpResponse<String> response1 = Unirest.post("https://www.fast2sms.com/dev/bulkV2")
						  .header("authorization",authorization )
						  .header("cache-control", "no-cache")
						  .header("content-type", "application/x-www-form-urlencoded")
						  .body("sender_id=TXTIND&language=english&route=v3&numbers="+mobile+"&message= "+message)
						  .asString();	
				  return response1.getStatus();
			}
			
			  return 0;
		}
		
	
}
