
package org.mysys.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.mysys.model.CityMasterView;
import org.mysys.model.CompanySetup;
import org.mysys.model.DashboardSetting;
import org.mysys.model.Designation;
import org.mysys.model.DropdownVO;
import org.mysys.model.job.IssueLogDtlCommVw;
import org.mysys.model.Mail;
import org.mysys.model.MiscCfg;
import org.mysys.model.PinBoardListView;
import org.mysys.model.Role;
import org.mysys.model.RoleListVw;
import org.mysys.model.Rolescreenaccess;
import org.mysys.model.SMS;
import org.mysys.model.Screen;
import org.mysys.model.User;
import org.mysys.model.UserSkill;
import org.mysys.model.UserSkillPk;
import org.mysys.model.Userrole;
import org.mysys.model.job.RecuGrpMap;
import org.mysys.model.job.UserListVw;
import org.mysys.model.job.IssueLogDtlComments;
import org.mysys.model.job.IssueLog;
import org.mysys.model.job.IssueLogDtl;
//import org.mysys.model.customer.CustMst;
//import org.mysys.model.partner.PartnerMst;
//import org.mysys.model.partner.PartnerTeam;
import org.mysys.repository.DesignationRepo;
import org.mysys.repository.IGenericDao;
import org.mysys.repository.RoleRepo;
import org.mysys.repository.RolescreenaccessRepo;
import org.mysys.repository.ScreenRepo;
import org.mysys.repository.UserRoleRepository;
import org.mysys.repository.job.IssueLogDtlCommRepo;
import org.mysys.repository.job.IssueLogDtlCommVwRepo;
import org.mysys.repository.job.IssueLogDtlRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AdminService extends AbstractService {
	
		private EmailService emailService;
		
		 @Autowired
		 public void setEmailService(@Lazy EmailService email) {
		        this.emailService = email;
		    }
	
	@Autowired
	protected SMSService smsService;

	@Autowired
	private RoleRepo roleRep;

	@Autowired
	private UserRoleRepository userRoleRepo;
	
	@Autowired
	private DesignationRepo designRepo;
	
	@Autowired
	private ScreenRepo screenRepo;
	
	@Autowired
	private RolescreenaccessRepo rolescreenaccessRepo;
	
	
	@Autowired
	protected IssueLogDtlRepo issueLogDtlRepo;

	@Autowired
	protected IssueLogDtlCommRepo issueLogDtlCommRepo;
	
	@Autowired
	protected IssueLogDtlCommVwRepo issueLogDtlCommVwRepo;
	
	private IGenericDao<UserSkill, UserSkillPk> userSkillRepo;

	@Autowired
	public void setUserskillRepo( IGenericDao<UserSkill,UserSkillPk > daoToSet ){
		userSkillRepo = daoToSet;
		userSkillRepo.setClazz( UserSkill.class );
	}
	
	@Transactional
	//@PreAuthorize("hasRole('USER_ADD') or hasRole('USER_EDIT')")
	public User saveUser(User user) {
		user.setSiteid(getLoggedInUserSiteId());

		ArrayList<Userrole> userroles = null;
		if (user.getUserroles() != null) { 
			userroles = new ArrayList<>(user.getUserroles()); 
			user.getUserroles().clear(); 
		}

		User userCreated = userRepo.save(user);
		
		userRoleRepo.deleteByUserid(userCreated.getUserid());
		if (userroles != null && !userroles.isEmpty()) { 
			for (Userrole userrole : userroles) { 
				userrole.setUserid(userCreated.getUserid());
				userrole.setSiteid(userCreated.getSiteid());
			}	
		userRoleRepo.saveAll(userroles); 
		}		
		return userCreated;
	}
	
	public Integer checkDuplicateUser(String loginid, long userid) {
		Integer dupl = userRepo.findduplicateByLoginidonEdit(loginid,userid);
		return dupl;
	}

	public Integer getUser(String loginid) {
		return userRepo.findduplicateByLoginid(loginid);
	}
	
	@Transactional
	public String changePwd(String curPwd, String newPwd) {
		Object secPrincipal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String loginId = null;
		if(secPrincipal instanceof UserDetails) {
			loginId = ((UserDetails)secPrincipal).getUsername();
			
			User user = userRepo.findByLoginid(loginId);
			
			if(user != null && user.getPassword() != null && user.getPassword().equals(curPwd)) {
				int count = userRepo.changePwd(loginId, newPwd);
				return (count > 0) ? "SUCCESS" : "FAIL";
			} else { 
				return "INVALID_PWD";
			}
		} else {
			return "INVALID_USR";
		}
	}
	
/*	@PreAuthorize("hasRole('USER_ADD') or hasRole('USER_EDIT')")
	public boolean changePwd(long userid, String password) {
		int count = repo.changePwd(userid, password);
		return (count > 0);
	}*/
	
	@PreAuthorize("hasRole('ROLE_ROLE_ADD') or hasRole('ROLE_ROLE_EDIT')")
	@Transactional
	public Role saveRole(Role role) {
		role.setSiteid(getLoggedInUserSiteId());
		List<Rolescreenaccess> rolescreenaccess = null;
		if (role.getRolescreenaccesses() != null) {
			rolescreenaccess = new ArrayList<>(role.getRolescreenaccesses());
			role.getRolescreenaccesses().clear();
		}
		roleRep.save(role);
		if (rolescreenaccess != null && !rolescreenaccess.isEmpty()) {
			for (Rolescreenaccess rolescreen : rolescreenaccess) {
				rolescreen.getId().setRoleid(role.getId());
				rolescreen.setSiteid(role.getSiteid());
			}
			rolescreenaccessRepo.deleteById_Roleid(role.getId());
			rolescreenaccessRepo.saveAll(rolescreenaccess);
		}
		return role;
	}

	@Transactional
	public User getUser(long id){
		List<RecuGrpMap> rgm = recuGroupMapRepo.findByRecId(id);
		User user = userRepo.getOne(id);
		return user;
	}
	
	/*@PreAuthorize("hasRole('USER_ADD') or hasRole('USER_EDIT')")
	@Transactional
	public User getUserName(long userid){
		return userRepo.findByUserid(userid);
	}*/
	/*
	 * public User getUserName(String name){ User user = new User();
	 * user.setSiteid(getLoggedInUserSiteId()); user.setFirstname(name);
	 * user.setLastname(name); return userRepo.save(user); }
	 */

	@Transactional
	public List<DropdownVO> getUsers(){
		return userRepo.findAll().stream().map((a)->{return new DropdownVO(String.valueOf(a.getUserid()), (a.getName())); }).collect(Collectors.toList());
	}
	
	@Transactional
	public List<User> getUsersManager(String loginid){
		List<User> users= userRepo.getUsersManager(loginid);
		return users;	
	}

	@Transactional
	public String getUsersEmail(long userid,long siteid){
		return   userRepo.getUserEmail(userid,siteid);
	}
	
	@Transactional
	public long getCurrentUserId(String loginid){
		return  userRepo.getCurrentUserId(loginid);
	}


	@PreAuthorize("hasRole('ROLE_USER_DELETE')")
	@Transactional
	public String deleteUser(long id){
		String result = "SUCCESS";
		try{
			userRoleRepo.deleteByUserid(id);
			userRepo.deleteById(id);
		}catch(DataIntegrityViolationException ex){
			result = "DATA_INTEGRITY";
		}catch(Exception ex){
			result = "DATA_INTEGRITY";
		}
		return result;
	}
	
	public Map<String,List<DropdownVO>> getDropDownData(){
		Map<String,List<DropdownVO>> dropdownData = new HashMap<>();
		dropdownData.put("role", roleRep.findAll().stream().map((a)->{return new DropdownVO(String.valueOf(a.getRolename()),a.getRolename()); }).collect(Collectors.toList()));
		return dropdownData;
	}
	
	public Map<String,List<DropdownVO>> GetDesignDropdownData(){
		Map<String,List<DropdownVO>> dropdownData = new HashMap<>();
		dropdownData.put("designation", designRepo.findAll().stream().map((a)->{return new DropdownVO(String.valueOf(a.getDesignationName()),a.getDesignationName()); }).collect(Collectors.toList()));
		return dropdownData;
	}
	
	@PreAuthorize("hasRole('ROLE_ROLE_DELETE')")
	public String deleteRole(Long id){
		String result = "SUCCESS";
		try{
			roleRep.deleteById(id);
		}catch(DataIntegrityViolationException ex){
			result = "DATA_INTEGRITY";
		}catch(Exception ex){
			result = "DATA_INTEGRITY";
		}
		return result;
	}
	
	@Transactional
	public Role getRole(Long id) {
		Role role = roleRep.getOne(id);
		role.getStartdate();
		role.getEnddate();
		return role;
	}
	
	public String LoginIdExists(String loginId) {
		User user = userRepo.findByLoginid(loginId);
		if (user != null) {
			return "Found";
		}
		return "Not Found";
	}
	
	
	public Designation saveDesignation(String designationName, String desigCode) {
		Designation design = new Designation();
    	design.setDesignationName(designationName);
    	design.setDesigCode(desigCode);
    	design.setSiteid(getLoggedInUserSiteId());
    	return designRepo.save(design);
	}
	
	@Transactional
	public List<Screen> getScreens() {
		//return screenRepo.findAll();
		//return screenRepo.findByActiveAndSiteid("1", getLoggedInUserSiteId());
		return screenRepo.findByActiveAndSiteidOrderByScreenname("1", getLoggedInUserSiteId());
	}	
	
	/********************** Start: setup ************************/
	@Transactional
	@PreAuthorize("hasRole('SETUP_ADD') or hasRole('SETUP_EDIT')")
	public MiscCfg saveMiscCfg(MiscCfg miscCfg){
		miscCfg.setSiteid(getLoggedInUserSiteId());
		return miscCfgRepo.save(miscCfg);
	}	

	public List<MiscCfg> getMiscCfg(String cfgKey){
		return miscCfgRepo.findByCfgKeyAndSiteid(cfgKey, getLoggedInUserSiteId());
	}

	@PreAuthorize("hasRole('SETUP_VIEW') or hasRole('SETUP_EDIT')")
	public MiscCfg findMiscCfg(long cfgId){
		return miscCfgRepo.findByCfgId(cfgId).get(0);
	
	}	

	@PreAuthorize("hasRole('SETUP_DELETE')")
	public String deleteMiscCfg(long cfgId){
		String result = "SUCCESS";
		try{
			miscCfgRepo.deleteById(cfgId);
		}catch(DataIntegrityViolationException ex){
			result = "DATA_INTEGRITY";
		}catch(Exception ex){
			result = "DATA_INTEGRITY";
		}
		return result;
	}
	/********************** Start: setup ************************/	
	
	/********************** Start: Company setup ************************/
	@PreAuthorize("hasRole('COMPANY_SETUP_EDIT')")
	@Transactional
	public int updateCompanyInfo(CompanySetup cmpSetup) {
		int count = cmpRepo.updateCompanyInfo(cmpSetup.getCmpName(), cmpSetup.getDisplayName(), cmpSetup.getDealsIn(), cmpSetup.getAddress1(), cmpSetup.getAddress2(), 
				cmpSetup.getAddress3(), cmpSetup.getCityid(), cmpSetup.getCityName(), cmpSetup.getStateid(), cmpSetup.getStateName(), cmpSetup.getFullAddress(), 
				cmpSetup.getZipCode(), cmpSetup.getPrimaryPhone(), cmpSetup.getSecondoryPhone(), cmpSetup.getMobile(), cmpSetup.getEmail(), cmpSetup.getWebsiteUrl(), 
				cmpSetup.getGstin(), cmpSetup.getSiteid(),cmpSetup.getAccountName(),cmpSetup.getBankName(),cmpSetup.getBranch(),cmpSetup.getAccountType(),cmpSetup.getAccountNo(),cmpSetup.getIfscCode());
		return count;
	}

	@PreAuthorize("hasRole('COMPANY_SETUP_VIEW') or hasRole('COMPANY_SETUP_EDIT')")
	@Transactional
	public CompanySetup getCompanyInfo() {
		return cmpRepo.findBySiteid(getLoggedInUserSiteId());
	}
	/********************** Start: Company setup ************************/	
	@Transactional
	public User getUserName(){
		return userRepo.findByLoginid(getLoggedInUserLoinId());
	
	}
	
//	public <T> void saveUsers(T t) {
//		
//		CustMst custMst =new CustMst();
//		PartnerMst ptMst = new PartnerMst();
//		PartnerTeam ptTeam = new PartnerTeam();
//		ThreadLocalRandom rand = ThreadLocalRandom.current();
//
//		if(t instanceof CustMst) {
//		    User u = new User();
//			Userrole d = new Userrole();
//		    Set<Userrole> mMap = new HashSet<Userrole>();
//
//
//			custMst =(CustMst)t;
//			u.setDisplayname(custMst.getDisplayName());
//			u.setFirstname(custMst.getName());
//			String[] loginId=custMst.getName().trim().split("\\s");
//			if(loginId[0].length() > 5) {
//				u.setLoginid((loginId[0].substring(0, 4)).toLowerCase().concat(String.valueOf(custMst.getCsId())));
//			}
//			else {
//				u.setLoginid((loginId[0].toLowerCase().concat(String.valueOf(custMst.getCsId()))));
//			}
//			u.setPassword((loginId[0].substring(0, 4)).toLowerCase().concat(String.valueOf(rand.nextInt(100, 10000))));
//			u.setMobile(custMst.getMobile());
//			u.setPhone(custMst.getPhone());
//			u.setAddr1(custMst.getAddress());
//			u.setEmail(custMst.getEmail());
//			u.setStatus("1");
//			u.setPrUserId(getLoggedInUserUserId());
//			u.setUiid(custMst.getCsId());
//			u.setUserType("Customer");
//			
//			d.setRoleid(4);
//			mMap.add(d);
//			u.setUserroles(mMap);
//			this.saveUser(u);
//			
//		} else if(t instanceof PartnerMst) {
//		    User u = new User();
//			Userrole d = new Userrole();
//		    Set<Userrole> mMap = new HashSet<Userrole>();
//
//			ptMst =(PartnerMst)t;
//			
//			u.setDisplayname(ptMst.getDisplayName());
//			u.setFirstname(ptMst.getName());
//			String[] loginId=ptMst.getName().trim().split("\\s");
//			if(loginId[0].length() > 5) {
//				u.setLoginid((loginId[0].substring(0, 4)).toLowerCase().concat(String.valueOf(ptMst.getPmId())));
//			}
//			else {
//				u.setLoginid((loginId[0].toLowerCase().concat(String.valueOf(custMst.getCsId()))));
//			}
//			u.setPassword((loginId[0].substring(0, 4)).toLowerCase().concat(String.valueOf(rand.nextInt(100, 10000))));
//			u.setMobile(ptMst.getMobile());
//			u.setPhone(ptMst.getPhone());
//			u.setAddr1(ptMst.getAddress());
//			u.setEmail(ptMst.getEmail());
//			u.setStatus("1");
//			u.setPrUserId(getLoggedInUserUserId());
//			u.setUiid(ptMst.getPmId());
//			u.setUserType("Partner");
//			
//			d.setRoleid(2);
//			mMap.add(d);
//			u.setUserroles(mMap);
//			this.saveUser(u);
//
//		} else {
//		    User u = new User();
//		    Set<Userrole> mMap = new HashSet<Userrole>();
//			Userrole d = new Userrole();
//
//			ptTeam =(PartnerTeam)t;
//			u.setDisplayname(ptTeam.getDisplayName());
//			u.setFirstname(ptTeam.getName());
//			String[] loginId=ptTeam.getName().trim().split("\\s");
//			if(loginId[0].length() > 5) {
//				u.setLoginid((loginId[0].substring(0, 4)).toLowerCase().concat(String.valueOf(ptTeam.getPtId())));
//			}
//			else {
//				u.setLoginid((loginId[0].toLowerCase().concat(String.valueOf(custMst.getCsId()))));
//			}
//			u.setPassword((loginId[0].substring(0, 4)).toLowerCase().concat(String.valueOf(rand.nextInt(100, 10000))));
//			u.setMobile(ptTeam.getMobile());
//			u.setPhone(ptTeam.getPhone());
//			u.setAddr1(ptTeam.getAddress());
//			u.setEmail(ptTeam.getEmail());
//			u.setStatus("1");
//			u.setPrUserId(ptTeam.getPmId());
//			u.setUiid(ptTeam.getPtId());
//			u.setUserType("Partner Team");
//			
//			d.setRoleid(3);
//			mMap.add(d);
//			u.setUserroles(mMap);
//			
//			this.saveUser(u);
//		}
//           
//
//	}
	
	@Transactional
	public User getUserNameByLoginId(String loginId){
		return userRepo.findByLoginid(loginId);
	
	}
	
	public List<CityMasterView> getAllCityMasterView()
	{
		return cityMasterViewRepo.findAll();
	}

	public List<RoleListVw> getRoleDropDown() {
		return roleListVwRepo.findAll();
	}

	public String getNumberCheck(String mobile, String phone, Long userid) {
		String result = "";
		if(userid == 0) {
			if(!StringUtils.isEmpty(mobile)) {
				List<User> user = userRepo.findByMobile(mobile);
				if(user.size() == 0)
					result = "success";
				else
					result = "mobile";
					return result;
			}
			
			if(!StringUtils.isEmpty(phone)) {
				List<User> user1 = userRepo.findByPhone(phone);
				if(user1.size() == 0)
					result = "success";
				else
					result = "phone";
					return result;
			}
		}
		else {
			if(!StringUtils.isEmpty(mobile)) {
				List<User> user = userRepo.findByMobileAndUserid(mobile, userid); 
				if(user.size() == 0) {
					List<User> user2 = userRepo.findByMobile(mobile);
					if(user2.size() == 0)
						result = "success";
					else
						result = "mobile";
						return result;
				}
				else
					result = "success";
			}
			if(!StringUtils.isEmpty(phone)) {
				List<User> user = userRepo.findByPhoneAndUserid(phone, userid); 
				if(user.size() == 0) {
					List<User> user3 = userRepo.findByMobile(phone);
					if(user3.size() == 0)
						result = "success";
					else
						result = "phone";
						return result;
				}
				else
					result = "success";
			}
		}
		
		return result;
	}


	/*
	 * public List<CityMasterView1> getAllCityMasterView() { return
	 * cityMasterViewRepo.findAll(); }
	 */
	/* This method is used in pinboard */	
	public List<PinBoardListView> getByIdPinboardListView()
	{
		Long userId= userRepo.findByLoginid(getLoggedInUserLoinId()).getUserid();
		return pinBoardListViewRepo.findByAssignedtoAndSiteid(userId,getLoggedInUserSiteId());
	}

	public List<UserListVw> getUserslistvwManager() {
		List<UserListVw> userslistVw = userlistvwRepo.getUserslistvwManager(getLoggedInUserSiteId());
		return userslistVw;
	}
	
	
	//@PreAuthorize("hasRole('ISSUE_LOG_ADD') or hasRole('ISSUE_LOG_EDIT')")
	@Transactional
	public IssueLog saveIssueLog(IssueLog issueLog) {
		List<IssueLogDtl> issueLogdtls = null;
		
		
		if(issueLog.getIssueLogDtl() != null) {
			issueLogdtls = new ArrayList<IssueLogDtl>(issueLog.getIssueLogDtl());
			issueLog.getIssueLogDtl().clear();
		}
		

		if(issueLog.getIlId() <= 0) {
			issueLog.setSiteId(getLoggedInUserSiteId());
			synchronized(issueLog) {
				issueLog = issueLogRepo.save(issueLog);
			}
		}
		else {
			issueLog = issueLogRepo.save(issueLog);
		}
		
		// issueLogDtlRepo.deleteByIlId(issueLog.getIlId());
		if(issueLogdtls != null && issueLogdtls.size() > 0) 
		{
			Long ilId = issueLog.getIlId();
			for(IssueLogDtl issue: issueLogdtls)
			{
				if(issue.getIldId() != null && issue.getIldId() <= 0) {
					issue.setLogDate(new Date());
				}
				issue.setIlId(issueLog.getIlId());
				issue.setSiteId(issueLog.getSiteId());	
			}
			if(issueLog.getIlId() >= 0) {
				issueLogDtlRepo.updateByIlId(ilId);
			}	
			issueLogDtlRepo.saveAll(issueLogdtls);
		}
		List<IssueLogDtl> issueLogdtl = issueLogDtlRepo.findByIlId(issueLog.getIlId());
		issueLog.setIssueLogDtl(new ArrayList<>(issueLogdtl));
		return issueLog;
	}

	
	
	public List<IssueLogDtl> getIssueLogByCode(String code) {
		return issueLogDtlRepo.findByCode(code);
	}
	
	public List<IssueLogDtl> getIssueLogDtl() {
		return issueLogDtlRepo.findAll();
	}
	
	public List<IssueLogDtlCommVw> getIssueLogDtlComments(Long ilId) {
		return issueLogDtlCommVwRepo.findByIlId(ilId);
	}
	
	public List<IssueLogDtlComments> getIssueLogByIldcId(Long ildcId) {
		return issueLogDtlCommRepo.findByIldcId(ildcId);
	}
	
	@Transactional
	public boolean saveIssueLogDtlComments(List<IssueLogDtlComments> issueLogDtlComm) 
	{
		for(IssueLogDtlComments ild: issueLogDtlComm)
		{
			if(ild.getIldcId() <= 0) {
				ild.setSiteId(getLoggedInUserSiteId());
			} 
		}
		issueLogDtlComm = issueLogDtlCommRepo.saveAll(issueLogDtlComm);

		return (issueLogDtlComm.size()>0? true : false);
	}
	
	//First Way 
		public IssueLog getIssueLog(Long ilId) {
			IssueLog is= issueLogRepo.findByIlId(ilId);
			is.getIssueLogDtl().stream().filter(p-> p.getIsActive().equals("Y"));
			return is;
		}
		
		@Transactional
		public int updateRecordById(Long ilId)
		{
		    int success = issueLogRepo.updateRecord(ilId);
		    return success;
		}
}
