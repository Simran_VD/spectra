package org.mysys.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.mysys.model.Displist;
import org.mysys.model.Displistcol;
import org.mysys.model.User;
import org.mysys.repository.ListingHelperRepo;
import org.mysys.repository.ListingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ListingService extends AbstractService {

    @Autowired
    private ListingRepo dispList;
    
    @Autowired
    private ListingHelperRepo repo;
    
    public Optional<Displist> loadMetadata(long id){
    	return dispList.findById(id);
    }
    
    
    public Page<String[]> getData(String whereClause, Long id,Pageable page,String type){
    	String us ="";
    	if(type !=null && type.length()>0){
    	List<Integer> l1 = new ArrayList<Integer>();
         //l1.add(id.intValue());
    	l1.addAll(Arrays.asList(type.split(",")).stream().map(str-> Integer.valueOf(str)).collect(Collectors.toList()));
    	
        for(Integer d:l1) {
      	  us+=d.toString()+",";
        }
      	us=us.substring(0, us.length()-1);
    	} else {
    		us+=us.toString();
    	}
    	String clausee = "";
    	Displist list = loadMetadata(id).get();
    	StringBuilder query =  new StringBuilder(" select "), countQuery = new StringBuilder(" select ");
    	boolean atLeastOneCol = false;
    	String sortableCol = null;
    	if(list.getDisplistcols().isEmpty()){
    		query.append(" * ");
    	}else{
    		for(Displistcol col : list.getDisplistcols()){
    			//if(col.getHidden() == null || col.getHidden().equalsIgnoreCase("N")){
    			if (col.getDatatype() != null && col.getDatatype().equalsIgnoreCase("DATE") && col.getColformat()!= null && !col.getColformat().isEmpty()) {
    				query.append("to_char(\"").append(col.getColumnname()).append("\", '").append(col.getColformat()).append("') as \"").append(col.getColumnname()).append("\",");
    			} else {
    				query.append("\"").append(col.getColumnname()).append("\",");
    			}
    			atLeastOneCol = true;
    			//}
    			if(col.getSortable() != null && col.getSortable().equalsIgnoreCase("Y")) {
    				if(col.getSortorder() != null && col.getSortorder().equalsIgnoreCase("DESC")) {
    					sortableCol = "\"" + col.getColumnname() + "\" DESC";
    				} else {
    					sortableCol = "\"" + col.getColumnname() + "\" ASC";
    				}
    			}
    		}
    		if(!atLeastOneCol){
    			query.append(" 1 as dummy  ");
    		}
    	}

    	User u=userRepo.findByLoginid(getLoggedInUserLoinId());

    	
    	query.deleteCharAt(query.length()-1);
    	//query.append(" from ").append("fnc_org_hier("+u.getUserid()+")");
    	//query.append(" from ").append(list.getTablename());
    	
        if(us.length()>0) {
        	query.append(" from ").append(list.getTablename().concat("("+u.getUserid()+","+us+")"));
        	countQuery.append(" count(1) from ").append(list.getTablename().concat("("+u.getUserid()+","+us+")"));
        }
        else {
        	query.append(" from ").append(list.getTablename().concat("("+u.getUserid()+","+id+")"));
        	countQuery.append(" count(1) from ").append(list.getTablename().concat("("+u.getUserid()+","+id+")"));
        }
    	
    	
    	System.out.println(list.getWhereclause());
    	if(whereClause != null && list.getWhereclause() != null){
    		whereClause += " and "+list.getWhereclause();
//    		clausee += " and "+list.getWhereclause();
    	}else if(list.getWhereclause() != null){
    		whereClause = list.getWhereclause();
//    		clausee = list.getWhereclause();
    	}
    	clausee = whereClause;
    	System.out.println(whereClause);
    	System.out.println(clausee);
    	if(whereClause == null || whereClause.equalsIgnoreCase("null")){
    		clausee = "siteid = " + getLoggedInUserSiteId();      // this is commented because today onwards userid will be use by function not by where clause commented by shubham jha+" and userid= "+u.getUserid();
            query.append(" where ").append(clausee);
    		countQuery.append(" where ").append(clausee);
    		
    		
    	} else { 
    			clausee = " siteid = " + getLoggedInUserSiteId();       // this is commented because today onwards userid will be use by function not by where clause commented by shubham jha +" and userid= "+u.getUserid();
    		query.append(" where ").append(clausee);
    		countQuery.append(" where ").append(clausee);
    	}
    	
    	if(sortableCol != null) {
    		//query.append(" order by ").append(sortableCol);	//.append(" desc ");
    		countQuery.append(" order by ").append(sortableCol);
    	}
    	return repo.fetchData(query.toString(),countQuery.toString(), null, page);
    }
}
