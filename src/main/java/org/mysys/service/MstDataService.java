package org.mysys.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mysys.model.Citymaster;
import org.mysys.model.Department;
import org.mysys.model.Designation;
import org.mysys.model.Skill;
import org.mysys.model.Statemaster;
import org.mysys.model.job.StateMasterView;
import org.mysys.model.spectra.AuditTypeMst;
import org.mysys.model.spectra.FreqMst;
import org.mysys.model.spectra.OrgType;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class MstDataService extends AbstractService 
{
	private static final Logger LOGGER = LogManager.getLogger(SpectraService.class);

	/********** End: Tool Master **********/

	/********** Start: Department Master **********/
	@PreAuthorize("hasRole('DEPARTMENT_ADD') or hasRole('DEPARTMENT_EDIT')")
	@Transactional
	public Department saveDepartment(Department department) {
		department.setSiteid(getLoggedInUserSiteId());
		Department tmpDepartment = deptRepo.save(department);
		return tmpDepartment;
	}

	public Department getDepartment(String deptname){
		return deptRepo.findByDeptnameAndSiteid(deptname, getLoggedInUserSiteId());
	}

	@PreAuthorize("hasRole('DEPARTMENT_VIEW') or hasRole('DEPARTMENT_EDIT')")
	@Transactional
	public Department findDepartment(Long deptId) {
		Department tmpDepartment = deptRepo.findById(deptId).get();
		return getDepartmentOnly(tmpDepartment);
	}

	private Department getDepartmentOnly(Department dept) {
		dept.setSiteid(getLoggedInUserSiteId());
		return dept;
	}

	@PreAuthorize("hasRole('DEPARTMENT_DELETE')")
	public String deleteDepartment(Long deptId){
		String result = "SUCCESS";
		try{
			deptRepo.deleteById(deptId);
		}catch(DataIntegrityViolationException ex){
			result = "DATA_INTEGRITY";
		}catch(Exception ex){
			result = "DATA_INTEGRITY";
		}
		return result;
	}
	/********** End: Department Master **********/


	/********** End: HSN Code Master **********/

	/********** Start: State Master **********/
	@Transactional
//	@PreAuthorize("hasRole('STATE_ADD') or hasRole('STATE_EDIT')")
	public Statemaster saveState(Statemaster state){
		state.setSiteid(getLoggedInUserSiteId());
		if(state.getStateid() > 0){
			state.setModifiedById(getLoggedInUserUserId());
		} else {
			state.setCreatedById(getLoggedInUserUserId());
		}
		return stateRepo.save(state);
	}
	
	/*************** State master Duplicate Check on add and Edit*********/

	public Integer checkDuplicateStatemaster(String statename, long stateid) {
		Integer dupl = stateRepo.findduplicateByStatenameonEdit(statename,stateid);
		return dupl;
	}

	public Integer getStatemaster(String statename){
		return stateRepo.findduplicateByStatename(statename);
	}

	
//	@PreAuthorize("hasRole('STATE_VIEW') or hasRole('STATE_EDIT')")
	public Statemaster findState(long stateid){
		return stateRepo.findByStateid(stateid);
	}
	
//	public List<Statemaster> findStates(){
//		String status="Y";
//		return stateRepo.findByStateidGreaterThanAndIsActive(0,status);
//	}	

//	@PreAuthorize("hasRole('STATE_DELETE')")
//	public String deleteState(long stateid){
//		String result = "SUCCESS";
//		try{
//			stateRepo.deleteById(stateid);
//		}catch(DataIntegrityViolationException ex){
//			result = "DATA_INTEGRITY";
//		}catch(Exception ex){
//			result = "DATA_INTEGRITY";
//		}
//		return result;
//	}
	/********** End: State Master **********/


	/********** Start: City Master **********/
	@Transactional
//	@PreAuthorize("hasRole('CITY_ADD') or hasRole('CITY_EDIT')")
	public Citymaster saveCitymaster(Citymaster city){
		city.setSiteid(getLoggedInUserSiteId());
		if(city.getCityId() > 0){
			city.setModifiedById(getLoggedInUserUserId());
		} else {
			city.setCreatedById(getLoggedInUserUserId());
		}
		return citymasterRepo.save(city);
	}	
	
	/*************** City master Duplicate Check on add and Edit*********/

	public Integer checkDuplicateCitymaster(String cityName, long cityId) {
		Integer dupl = citymasterRepo.findduplicateByCityNameonEdit(cityName,cityId);
		return dupl;
	}
	
	public Integer getCitymaster(String cityName){
		return citymasterRepo.findduplicateByCityName(cityName);
	}

//	@PreAuthorize("hasRole('CITY_VIEW') or hasRole('CITY_EDIT')")
	public Citymaster findCitymaster(long cityId){
		return citymasterRepo.findByCityId(cityId);
	}
	

	public List<Citymaster> findCitymasterByStateid(long stateid) {

		return citymasterRepo.findByStatemaster_stateidOrderByCitynameAsc(stateid);
	}

//	@PreAuthorize("hasRole('CITY_DELETE')")
//	public String deleteCity(long cityid){
//		String result = "SUCCESS";
//		try{
//			locMstRepo.deleteById(cityid);
//		}catch(DataIntegrityViolationException ex){
//			result = "DATA_INTEGRITY";
//		}catch(Exception ex){
//			result = "DATA_INTEGRITY";
//		}
//		return result;
//	}
	/********** End: City Master **********/
	
	/********** Start: Designation Master **********/	
	@Transactional
	@PreAuthorize("hasRole('DESIGNATION_ADD') or hasRole('DESIGNATION_EDIT')")
	public Designation saveDesignation(Designation designation){
		designation.setSiteid(getLoggedInUserSiteId());
		Designation mstDesignation = designationRepo.save(designation);
        return mstDesignation;
	}	

	@PreAuthorize("hasRole('DESIGNATION_VIEW') or hasRole('DESIGNATION_EDIT')")
	@Transactional
	public Designation getDesignationid(long designationid){
		return designationRepo.findByDesignationidAndSiteid(designationid, getLoggedInUserSiteId());
	}
	
	@PreAuthorize("hasRole('DESIGNATION_DELETE')")
	public String deleteDesignation(Long designationid){
		String result = "SUCCESS";
		try{
			designationRepo.deleteById(designationid);
		}catch(DataIntegrityViolationException ex){
			result = "DATA_INTEGRITY";
		}catch(Exception ex){
			result = "DATA_INTEGRITY";
		}
		return result;
	}
	
	/********** End: Designation Master **********/	
	
	/********** Start: Skill Master **********/	
	@Transactional
	@PreAuthorize("hasRole('SKILL_ADD') or hasRole('SKILL_EDIT')")
	public Skill saveSkill(Skill skill){
		skill.setSiteid(getLoggedInUserSiteId());
		Skill mstSkill = skillRepo.save(skill);
		return mstSkill;
	}	

	@PreAuthorize("hasRole('SKILL_VIEW') or hasRole('SKILL_EDIT')")
	@Transactional
	public Skill getSkillId(long id){
		return skillRepo.findByidAndSiteid(id, getLoggedInUserSiteId());
	}

	@PreAuthorize("hasRole('SKILL_DELETE')")
	public String deleteSkill(Long id){
		String result = "SUCCESS";
		try{
			skillRepo.deleteById(id);
		}catch(DataIntegrityViolationException ex){
			result = "DATA_INTEGRITY";
		}catch(Exception ex){
			result = "DATA_INTEGRITY";
		}
		return result;
	}


	/********** End: Skill Master **********/	

	/********** End: Indentor Master **********/
	
	
	
	/**********  SPECTRA Master STARTS HERE MSTDATA Service **********/
	
	/*************** Org Type MST Save and Edit*********/
	
	public OrgType saveOrgType(OrgType orgType) {
		LOGGER.info("inside saveOrgType() method. Saving data for Org Type");
		orgType.setSiteid(getLoggedInUserSiteId());
		if(orgType.getOtId() > 0){
			orgType.setModifiedById(getLoggedInUserUserId());
		} else {
			orgType.setCreatedById(getLoggedInUserUserId());
		}
		OrgType saveOrgType = orgTypeRepo.save(orgType);
		return saveOrgType;
	}

	public OrgType getOrgType(long otId) {
		LOGGER.info("inside getOrgType() method. editing data for Org Type Mst");
		return orgTypeRepo.findByOtId(otId);
	}
	
	/*************** Org Type MST Duplicate Check on add and Edit*********/
	
	public Integer checkDuplicateOrgType(String orgType, long otId) {
		Integer dupl = orgTypeRepo.findduplicateByOrgTypeonEdit(orgType,otId);
		return dupl;
	}

	public Integer getOrgType(String orgType) {
		return orgTypeRepo.findduplicateByOrgType(orgType);
	}

	
	/*************** Audit Type Mst Save and Edit*********/
	
	public AuditTypeMst saveAuditTypeMst(AuditTypeMst auditTypeMst) {
		LOGGER.info("inside saveAuditTypeMst() method. Saving data for Audit Type Mst");
		auditTypeMst.setSiteid(getLoggedInUserSiteId());
		if(auditTypeMst.getAtmId() > 0){
			auditTypeMst.setModifiedById(getLoggedInUserUserId());
		} else {
			auditTypeMst.setCreatedById(getLoggedInUserUserId());
		}
		AuditTypeMst saveAuditTypeMst = auditTypeMstRepo.save(auditTypeMst);
		return saveAuditTypeMst;
	}

	public AuditTypeMst getAuditTypeMst(long atmId) {
		LOGGER.info("inside getAuditTypeMst() method. editing data for Audit Type Mst");
		return auditTypeMstRepo.findByAtmId(atmId);
	}
	
	/*************** Audit Type Mst Duplicate Check on add and Edit*********/

	public Integer checkDuplicateAuditTypeMst(String auditName, long atmId) {
		Integer dupl = auditTypeMstRepo.findduplicateByAuditNameonEdit(auditName,atmId);
		return dupl;
	}

	public Integer getAuditTypeMst(String auditName) {
		return auditTypeMstRepo.findduplicateByAuditName(auditName);
	}
	
	/**********************  State Master List Vw Here ***********************/
	
	@Transactional
	public List<StateMasterView> getAllState()
	{
		String isActive = "Y";
		return this.stateMasterViewRepo.findByIsActive(isActive);
	}

	/*************** Freq Mst Save and Edit*********/
	
	public FreqMst saveFreqMst(FreqMst freqMst) {
		LOGGER.info("inside saveFreqMst() method. Saving data for Freq Mst");
		freqMst.setSiteid(getLoggedInUserSiteId());
		if(freqMst.getFmId() > 0){
			freqMst.setModifiedById(getLoggedInUserUserId());
		} else {
			freqMst.setCreatedById(getLoggedInUserUserId());
		}
		FreqMst saveFreqMst = freqMstRepo.save(freqMst);
		return saveFreqMst;
	}

	public FreqMst getFreqMst(long fmId) {
		LOGGER.info("inside getFreqMst() method. editing data for Freq Mst");
		return freqMstRepo.findByFmId(fmId);
	}

	/*************** Freq Mst Duplicate Check on add and Edit*********/

	public Integer checkDuplicateFreqMst(String freqName, long fmId) {
		Integer dupl = freqMstRepo.findduplicateByFreqNameonEdit(freqName , fmId);
		return dupl;
	}

	public Integer getFreqMst(String freqName) {
		return freqMstRepo.findduplicateByFreqName(freqName);
	}
	

}
