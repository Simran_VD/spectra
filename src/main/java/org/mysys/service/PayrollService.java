package org.mysys.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mysys.model.DropdownVO;
import org.springframework.stereotype.Service;

@Service
public class PayrollService extends AbstractService{

	@Transactional
	public List<DropdownVO> getDesignation(){
		return designRepo.findAll().stream().map((a)->{return new 
				DropdownVO(String.valueOf(a.getDesignationid()), 
						a.getDesignationName());
		}).collect(Collectors.toList());
	}

	@Transactional
	public List<DropdownVO> getSkills(){
		return skillRepo.findAll().stream().map((a)->{return new 
				DropdownVO(String.valueOf(a.getId()), 
						a.getName());
		}).collect(Collectors.toList());
	}

}
