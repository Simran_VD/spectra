package org.mysys.service;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mysys.constant.APPConstant;

import org.mysys.model.CompanyContext;
import org.mysys.model.CompanyInfo;
import org.mysys.model.DashboardSetting;
import org.mysys.model.DbSetting;
import org.mysys.model.User;
import org.mysys.model.notification.OTP;
import org.mysys.repository.DbSettingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class CommonService extends AbstractService {

    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private DbSettingRepo dbSettingRepo;

 

    public Map<String, String> getStaticValues(List<String> keys) {
	Map<String, String> staticVals = new HashMap<>();
	for (String key : keys) {
	    switch (key) {
	    case "date":
		SimpleDateFormat dt = new SimpleDateFormat(APPConstant.DATE_FORMAT);
		staticVals.put(key, dt.format(new Date()));
		break;
	    case "timemillis":
		staticVals.put(key, String.format("%d", new Date().getTime()));
		break;
	    case "datetime":
		SimpleDateFormat dttime = new SimpleDateFormat(APPConstant.DATE_TIME_FORMAT);
		staticVals.put(key, dttime.format(new Date()));
		break;
	    default:
		break;
	    }
	}
	return staticVals;
    }

    public CompanyContext getCompanyInformation() {
	List<CompanyInfo> companyInfo = commonRepo.executeNativeQuery("select key,value from company_info",
		CompanyInfo.class);
	Map<String, String> companyInfoContext = companyInfo.stream()
		.collect(Collectors.toMap(CompanyInfo::getKey, CompanyInfo::getValue));
	CompanyContext context = new CompanyContext();
	context.setCompanyContext(companyInfoContext);
	return context;
    }

    @Transactional
    public DbSetting getSetting(String key) {
	Optional<DbSetting> dbs = dbSettingRepo.findById(key);//.get();
	/*dbs.getValue();
	return dbs;*/
	if (dbs.isPresent()) 
	    return dbs.get();
	return null;
    }

    @Transactional
    public List<DbSetting> getDbSetting() {
	return dbSettingRepo.findAll();
    }


    @Autowired
    private HttpServletRequest request;

    public String firstTimeChangePassword() {
	User user = (User) request.getSession().getAttribute("user");
	if (user != null && user.getPasswordResetDate() == null) {
	    return "firstTimeUser";
	} else {
	    return "oldUser";
	}
    }

    @Transactional
    public String changePassword(String pswd, String oldpswd) {
	User user = (User) request.getSession().getAttribute("user");
	String oldPswd = user.getPassword();
	if (oldPswd != null && !oldpswd.isEmpty() && !oldPswd.equalsIgnoreCase(oldpswd)) {
	    return "Old Password is not correct.";
	}
	user.setPassword(pswd);
	user.setPasswordResetDate(new Date());
	user.setSiteid(getLoggedInUserSiteId());
	if (user.getUserroles() != null) {
	    user.getUserroles().clear();
	}
	userRepo.save(user);
	request.getSession().setAttribute("user", user);
	return "Password changed successfully.";
    }
    
    @Transactional
    public String changePasswordWAC() {
    	userRepo.updateResetPassworDate(new Date(),getLoggedInUserLoinId());
    	return "Password changed successfully.";
    }
    
    
    @Transactional
    public List<DashboardSetting> getDashboardSetting() {
	return dashboardsettingRepo.findAll();
    }
    
    
    
    
    @Transactional
	public Long saveOTP(String mobile, String otp) {
		otpRepo.deleteByPhone(mobile);
		OTP o= new OTP();
		o.setOtp(otp);
		o.setPhone(mobile);
		o= otpRepo.save(o);
		
		
		return o.getOtpId();
	}

	public boolean validateOTP(String mobile,String machId) {
		OTP otp= otpRepo.findByPhone(mobile);
		if(otp.getOtp().equals(machId)) {
			otpRepo.delete(otp);
			return true;
		}; 
		return false;
	}
    
    public List<User> getDuplicateContact(String mobile) {
		return userRepo.findByMobile(mobile);
	}
    
public boolean updatePwd(Long userId,String newPwd) {
		
		if(userId != null && userId > 0) {
			User user = userRepo.getUserByPK(userId);
			
			if(user != null && !StringUtils.isEmpty(newPwd)) {
				int count = userRepo.updatePwd(userId, newPwd);
				if(count > 0)
					return true;
				else
					return false;
			} else { 
				return false;
			}
		} else {
			return false;
		}
	}
}
