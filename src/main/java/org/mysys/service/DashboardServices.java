package org.mysys.service;

import java.util.List;

import org.mysys.model.AreaWiseCommVw;
import org.mysys.model.AreaWiseLeadCntVw;
import org.mysys.model.DashboardKPIServiceVw;
import org.mysys.model.LeadStatusCntVw;
//import org.mysys.model.PartnerWiseCommVw;
//import org.mysys.model.PartnerWiseLeadCntVw;
import org.mysys.model.User;
import org.mysys.model.Service.ServiceLeadVw;
import org.mysys.model.job.MatchingApplListVw;
import org.mysys.repository.AreaWiseCommVwRepo;
import org.mysys.repository.AreaWiseLeadCntVwRepo;
import org.mysys.repository.LeadStatusCntVwRepo;
import org.mysys.repository.MatchingApplListVwRepo;
//import org.mysys.repository.PartnerWiseCommVwRepo;
//import org.mysys.repository.PartnerWiseLeadCntVwRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DashboardServices extends AbstractService {
	@Autowired
	AreaWiseCommVwRepo areaWiseCommVwRepo ;
	
	@Autowired
	AreaWiseLeadCntVwRepo areaWiseLeadCntVwRepo;
	
	@Autowired
	LeadStatusCntVwRepo leadStatusCntVwRepo;
	
//	@Autowired
//	PartnerWiseLeadCntVwRepo partnerWiseLeadCntVwRepo;
//	
//	@Autowired
//	PartnerWiseCommVwRepo partnerWiseCommVwRepo;
	
//	@Autowired
//	ProductBrandMstVwRepo productBrandMstVwRepo;
	
//	public List<AreaWiseCommVw> getAreaWiseCommVw() {
//		User u= userRepo.findByLoginid(getLoggedInUserLoinId());
//		return areaWiseCommVwRepo.findByUserId((int)u.getUserid());
//	}

	public List<AreaWiseLeadCntVw> getAreaWiseLeadCntVw() {
		User u= userRepo.findByLoginid(getLoggedInUserLoinId());
		return areaWiseLeadCntVwRepo.findByUserId((int)u.getUserid());
	}

	public List<LeadStatusCntVw> getLeadStatusCntVw() {
		User u= userRepo.findByLoginid(getLoggedInUserLoinId());
		return leadStatusCntVwRepo.findByUserId((int)u.getUserid());
	}

//	public List<PartnerWiseLeadCntVw> getPartnerWiseLeadCntVw() {
//		User u= userRepo.findByLoginid(getLoggedInUserLoinId());
//		return partnerWiseLeadCntVwRepo.findByUserId((int)u.getUserid());
//	}
//
//	public List<PartnerWiseCommVw> getPartnerWiseCommVw() {
//		User u= userRepo.findByLoginid(getLoggedInUserLoinId());
//		return partnerWiseCommVwRepo.findByUserId((int)u.getUserid());
//	}
//	
	public List<DashboardKPIServiceVw> getKpiServiceDetails() {
		User u= userRepo.findByLoginid(getLoggedInUserLoinId());
		 //partnerWiseCommVwRepo.getByUserId((int)u.getUserid());
		 return dashBoardServiceRepo.getByUserId((int)u.getUserid());
	}
	
	public List<DashboardKPIServiceVw> getKpiServiceDetailsForMobile(String loginId) {
		User u= userRepo.findByLoginid(loginId);
		 return dashBoardServiceRepo.getByUserId((int)u.getUserid());
	}
	
//	public List<ProductBrandMstVw> getProductBrandMstVw(long prdId) {	
//		return productBrandMstVwRepo.findByprdId(prdId);
//	}
	
	public List<ServiceLeadVw> geServLeadData(long userId,long loginId) {
		 return serviceLeadVwRepo.getServLeadByUserIdAndType((int)userId,(int)loginId);
	}
	
	
	public List<MatchingApplListVw> getAllMatchingAppl(Integer jmId) {
		
		 return matchingappllistvwRepo.getAllMatchingAppl(jmId);
	}
}
