package org.mysys.ewaybill.vo;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the saleinv_eway_lv_1_vw database table.
 * 
 */
@Entity
@Table(name="saleinv_eway_lv_1_vw")
@NamedQuery(name="SaleinvEwayLv1Vw.findAll", query="SELECT s FROM SaleinvEwayLv1Vw s")
public class SaleinvEwayLv1Vw implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer actualFromStateCode;

	private Integer actualToStateCode;

	private Double cessValue;

	private Double cgstValue;

	private Timestamp docDate;

	private String docNo;

	private String docType;

	private String fromAddr1;

	private String fromAddr2;

	private String fromGstin;

	private Integer fromPinCode;

	private String fromPlace;

	private Integer fromStateCode;

	private String fromTrdName;

	private Double igstValue;

	private Double mainHsnCode;

	private Double sgstValue;
	@Id
	@Column(name="si_id")
	private long siId;

	private BigDecimal siteId;

	private Integer subSupplyType;

	private String supplyType;

	private String toAddr1;

	private String toAddr2;

	private String toGstin;

	private String toPlace;

	private Integer toStateCode;

	private Double totalValue;

	private Double totInvValue;

	private String toTrdName;

	private Double transDistance;

	private String transDocDate;

	private String transDocNo;

	private Double transMode;

	private String transporterId;

	private String transporterName;

	private String userGstin;

	private String vehicleNo;

	private String vehicleType;
	
	@OneToMany(mappedBy="saleInv", fetch=FetchType.EAGER)
	private List<SaleinvEwayLv2Vw> itemList;

	public SaleinvEwayLv1Vw() {
	}

	public Integer getActualFromStateCode() {
		return actualFromStateCode;
	}

	public void setActualFromStateCode(Integer actualFromStateCode) {
		this.actualFromStateCode = actualFromStateCode;
	}

	public Integer getActualToStateCode() {
		return actualToStateCode;
	}

	public void setActualToStateCode(Integer actualToStateCode) {
		this.actualToStateCode = actualToStateCode;
	}

	public Double getCessValue() {
		return cessValue;
	}

	public void setCessValue(Double cessValue) {
		this.cessValue = cessValue;
	}

	public Double getCgstValue() {
		return cgstValue;
	}

	public void setCgstValue(Double cgstValue) {
		this.cgstValue = cgstValue;
	}

	public Timestamp getDocDate() {
		return docDate;
	}

	public void setDocDate(Timestamp docDate) {
		this.docDate = docDate;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getFromAddr1() {
		return fromAddr1;
	}

	public void setFromAddr1(String fromAddr1) {
		this.fromAddr1 = fromAddr1;
	}

	public String getFromAddr2() {
		return fromAddr2;
	}

	public void setFromAddr2(String fromAddr2) {
		this.fromAddr2 = fromAddr2;
	}

	public String getFromGstin() {
		return fromGstin;
	}

	public void setFromGstin(String fromGstin) {
		this.fromGstin = fromGstin;
	}

	public Integer getFromPinCode() {
		return fromPinCode;
	}

	public void setFromPinCode(Integer fromPinCode) {
		this.fromPinCode = fromPinCode;
	}

	public String getFromPlace() {
		return fromPlace;
	}

	public void setFromPlace(String fromPlace) {
		this.fromPlace = fromPlace;
	}

	public Integer getFromStateCode() {
		return fromStateCode;
	}

	public void setFromStateCode(Integer fromStateCode) {
		this.fromStateCode = fromStateCode;
	}

	public String getFromTrdName() {
		return fromTrdName;
	}

	public void setFromTrdName(String fromTrdName) {
		this.fromTrdName = fromTrdName;
	}

	public Double getIgstValue() {
		return igstValue;
	}

	public void setIgstValue(Double igstValue) {
		this.igstValue = igstValue;
	}

	public Double getMainHsnCode() {
		return mainHsnCode;
	}

	public void setMainHsnCode(Double mainHsnCode) {
		this.mainHsnCode = mainHsnCode;
	}

	public Double getSgstValue() {
		return sgstValue;
	}

	public void setSgstValue(Double sgstValue) {
		this.sgstValue = sgstValue;
	}

	public long getSiId() {
		return siId;
	}

	public void setSiId(long siId) {
		this.siId = siId;
	}

	public BigDecimal getSiteId() {
		return siteId;
	}

	public void setSiteId(BigDecimal siteId) {
		this.siteId = siteId;
	}

	public Integer getSubSupplyType() {
		return subSupplyType;
	}

	public void setSubSupplyType(Integer subSupplyType) {
		this.subSupplyType = subSupplyType;
	}

	public String getSupplyType() {
		return supplyType;
	}

	public void setSupplyType(String supplyType) {
		this.supplyType = supplyType;
	}

	public String getToAddr1() {
		return toAddr1;
	}

	public void setToAddr1(String toAddr1) {
		this.toAddr1 = toAddr1;
	}

	public String getToAddr2() {
		return toAddr2;
	}

	public void setToAddr2(String toAddr2) {
		this.toAddr2 = toAddr2;
	}

	public String getToGstin() {
		return toGstin;
	}

	public void setToGstin(String toGstin) {
		this.toGstin = toGstin;
	}

	public String getToPlace() {
		return toPlace;
	}

	public void setToPlace(String toPlace) {
		this.toPlace = toPlace;
	}

	public Integer getToStateCode() {
		return toStateCode;
	}

	public void setToStateCode(Integer toStateCode) {
		this.toStateCode = toStateCode;
	}

	public Double getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(Double totalValue) {
		this.totalValue = totalValue;
	}

	public Double getTotInvValue() {
		return totInvValue;
	}

	public void setTotInvValue(Double totInvValue) {
		this.totInvValue = totInvValue;
	}

	public String getToTrdName() {
		return toTrdName;
	}

	public void setToTrdName(String toTrdName) {
		this.toTrdName = toTrdName;
	}

	public Double getTransDistance() {
		return transDistance;
	}

	public void setTransDistance(Double transDistance) {
		this.transDistance = transDistance;
	}

	public String getTransDocDate() {
		return transDocDate;
	}

	public void setTransDocDate(String transDocDate) {
		this.transDocDate = transDocDate;
	}

	public String getTransDocNo() {
		return transDocNo;
	}

	public void setTransDocNo(String transDocNo) {
		this.transDocNo = transDocNo;
	}

	public Double getTransMode() {
		return transMode;
	}

	public void setTransMode(Double transMode) {
		this.transMode = transMode;
	}

	public String getTransporterId() {
		return transporterId;
	}

	public void setTransporterId(String transporterId) {
		this.transporterId = transporterId;
	}

	public String getTransporterName() {
		return transporterName;
	}

	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}

	public String getUserGstin() {
		return userGstin;
	}

	public void setUserGstin(String userGstin) {
		this.userGstin = userGstin;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public List<SaleinvEwayLv2Vw> getItemList() {
		return itemList;
	}

	public void setItemList(List<SaleinvEwayLv2Vw> itemList) {
		this.itemList = itemList;
	}
	
	

}