package org.mysys.ewaybill.vo;

import java.math.BigDecimal;

public class Item {

	private String productName;
	private String productDesc;
	private BigDecimal hsnCode;
	private String qtyUnit;
	private BigDecimal quantity;
	private BigDecimal taxableAmount;
	private BigDecimal sgstRate;
	private BigDecimal cgstRate;
	private BigDecimal igstRate;
	private BigDecimal cessRate;
	
	
	public Item() {
		super();
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDesc() {
		return productDesc;
	}
	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}
	public BigDecimal getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(BigDecimal hsnCode) {
		this.hsnCode = hsnCode;
	}
	public String getQtyUnit() {
		return qtyUnit;
	}
	public void setQtyUnit(String qtyUnit) {
		this.qtyUnit = qtyUnit;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}
	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	public BigDecimal getSgstRate() {
		return sgstRate;
	}
	public void setSgstRate(BigDecimal sgstRate) {
		this.sgstRate = sgstRate;
	}
	public BigDecimal getCgstRate() {
		return cgstRate;
	}
	public void setCgstRate(BigDecimal cgstRate) {
		this.cgstRate = cgstRate;
	}
	public BigDecimal getIgstRate() {
		return igstRate;
	}
	public void setIgstRate(BigDecimal igstRate) {
		this.igstRate = igstRate;
	}
	public BigDecimal getCessRate() {
		return cessRate;
	}
	public void setCessRate(BigDecimal cessRate) {
		this.cessRate = cessRate;
	}
	
	
	
}
