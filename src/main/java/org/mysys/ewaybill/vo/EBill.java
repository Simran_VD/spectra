
package org.mysys.ewaybill.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "version",
    "billLists"
})
public class EBill {

    /**
     * Version
     * 
     */
    @JsonProperty("version")
    @JsonPropertyDescription("Version")
    private String version;
    @JsonProperty("billLists")
    private List<BillList> billLists = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public EBill() {
    }

    /**
     * 
     * @param billLists
     * @param version
     */
    public EBill(String version, List<BillList> billLists) {
        super();
        this.version = version;
        this.billLists = billLists;
    }

    /**
     * Version
     * 
     */
    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    /**
     * Version
     * 
     */
    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("billLists")
    public List<BillList> getBillLists() {
        return billLists;
    }

    @JsonProperty("billLists")
    public void setBillLists(List<BillList> billLists) {
        this.billLists = billLists;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("version", version).append("billLists", billLists).append("additionalProperties", additionalProperties).toString();
    }

}
