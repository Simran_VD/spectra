package org.mysys.ewaybill.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the SaleinvEwayLv2Vw database view.
 */

@Embeddable
public class SaleinvEwayLv2VwPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="si_id", insertable = false, updatable = false)
	private long siId;

	@Column(insertable = false, updatable = false)
	private Long itemno;

	public long getSiId() {
		return siId;
	}

	public void setSiId(long siId) {
		this.siId = siId;
	}

	public Long getItemno() {
		return itemno;
	}

	public void setItemno(Long itemno) {
		this.itemno = itemno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemno == null) ? 0 : itemno.hashCode());
		result = prime * result + (int) (siId ^ (siId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SaleinvEwayLv2VwPK other = (SaleinvEwayLv2VwPK) obj;
		if (itemno == null) {
			if (other.itemno != null)
				return false;
		} else if (!itemno.equals(other.itemno))
			return false;
		if (siId != other.siId)
			return false;
		return true;
	}

	

		
}