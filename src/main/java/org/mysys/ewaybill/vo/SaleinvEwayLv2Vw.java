package org.mysys.ewaybill.vo;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the saleinv_eway_lv_2_vw database table.
 * 
 */
@Entity
@Table(name="saleinv_eway_lv_2_vw")
@NamedQuery(name="SaleinvEwayLv2Vw.findAll", query="SELECT s FROM SaleinvEwayLv2Vw s")
public class SaleinvEwayLv2Vw implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private SaleinvEwayLv2VwPK id;
	
	private BigDecimal cessRate;

	private BigDecimal cgstRate;

	private BigDecimal hsnCode;

	private BigDecimal igstRate;

	private String productDesc;

	private BigDecimal productId;

	private String productName;

	private String qtyUnit;

	private BigDecimal quantity;

	private BigDecimal sgstRate;

	private BigDecimal siteId;

	private BigDecimal taxableAmount;
	
	@ManyToOne
    @JoinColumn(name="si_id", nullable=false, insertable=false, updatable=false)
	private SaleinvEwayLv1Vw saleInv;

	public SaleinvEwayLv2Vw() {
	}

	public SaleinvEwayLv2VwPK getId() {
		return id;
	}

	public void setId(SaleinvEwayLv2VwPK id) {
		this.id = id;
	}

	public BigDecimal getCessRate() {
		return cessRate;
	}

	public void setCessRate(BigDecimal cessRate) {
		this.cessRate = cessRate;
	}

	public BigDecimal getCgstRate() {
		return cgstRate;
	}

	public void setCgstRate(BigDecimal cgstRate) {
		this.cgstRate = cgstRate;
	}

	public BigDecimal getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(BigDecimal hsnCode) {
		this.hsnCode = hsnCode;
	}

	public BigDecimal getIgstRate() {
		return igstRate;
	}

	public void setIgstRate(BigDecimal igstRate) {
		this.igstRate = igstRate;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public BigDecimal getProductId() {
		return productId;
	}

	public void setProductId(BigDecimal productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getQtyUnit() {
		return qtyUnit;
	}

	public void setQtyUnit(String qtyUnit) {
		this.qtyUnit = qtyUnit;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSgstRate() {
		return sgstRate;
	}

	public void setSgstRate(BigDecimal sgstRate) {
		this.sgstRate = sgstRate;
	}

	public BigDecimal getSiteId() {
		return siteId;
	}

	public void setSiteId(BigDecimal siteId) {
		this.siteId = siteId;
	}

	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	public SaleinvEwayLv1Vw getSaleInv() {
		return saleInv;
	}

	public void setSaleInv(SaleinvEwayLv1Vw saleInv) {
		this.saleInv = saleInv;
	}
	

}