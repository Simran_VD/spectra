package org.mysys.ewaybill.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the SaleinvEwayLv2Vw database view.
 */

@Embeddable
public class JobWorkEwayLv2VwPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="itj_id", insertable = false, updatable = false)
	private long itjId;

	@Column(insertable = false, updatable = false)
	private Long itemno;


	public Long getItemno() {
		return itemno;
	}

	public void setItemno(Long itemno) {
		this.itemno = itemno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemno == null) ? 0 : itemno.hashCode());
		result = prime * result + (int) (itjId ^ (itjId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobWorkEwayLv2VwPK other = (JobWorkEwayLv2VwPK) obj;
		if (itemno == null) {
			if (other.itemno != null)
				return false;
		} else if (!itemno.equals(other.itemno))
			return false;
		if (itjId != other.itjId)
			return false;
		return true;
	}

}