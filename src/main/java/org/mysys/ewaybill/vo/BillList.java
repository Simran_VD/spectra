
package org.mysys.ewaybill.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "userGstin",
    "supplyType",
    "subSupplyType",
    "docType",
    "docNo",
    "docDate",
    "fromGstin",
    "fromTrdName",
    "fromAddr1",
    "fromAddr2",
    "fromPlace",
    "actualFromStateCode",
    "fromPincode",
    "fromStateCode",
    "toGstin",
    "toTrdName",
    "toAddr1",
    "toAddr2",
    "toPlace",
    "toPincode",
    "actualToStateCode",
    "toStateCode",
    "totalValue",
    "cgstValue",
    "sgstValue",
    "igstValue",
    "cessValue",
    "totInvValue",
    "transMode",
    "transDistance",
    "transporterName",
    "transporterId",
    "transDocNo",
    "transDocDate",
    "vehicleNo",
    "vehicleType",
    "mainHsnCode",
    "itemList"
})
public class BillList {

    /**
     * User GSTIN
     * 
     */
    @JsonProperty("userGstin")
    @JsonPropertyDescription("User GSTIN")
    private String userGstin;
    /**
     * Supply Type
     * (Required)
     * 
     */
    @JsonProperty("supplyType")
    @JsonPropertyDescription("Supply Type")
    private BillList.SupplyType supplyType;
    /**
     * Sub Supply Type
     * (Required)
     * 
     */
    @JsonProperty("subSupplyType")
    @JsonPropertyDescription("Sub Supply Type")
    private Double subSupplyType;
    /**
     * Document Type
     * (Required)
     * 
     */
    @JsonProperty("docType")
    @JsonPropertyDescription("Document Type")
    private BillList.DocType docType;
    /**
     * Document Number (Alphanumeric with / and - are allowed)
     * (Required)
     * 
     */
    @JsonProperty("docNo")
    @JsonPropertyDescription("Document Number (Alphanumeric with / and - are allowed)")
    private String docNo;
    /**
     * Document Date
     * (Required)
     * 
     */
    @JsonProperty("docDate")
    @JsonPropertyDescription("Document Date")
    private String docDate;
    /**
     * From GSTIN (Supplier or Consignor)
     * (Required)
     * 
     */
    @JsonProperty("fromGstin")
    @JsonPropertyDescription("From GSTIN (Supplier or Consignor)")
    private String fromGstin;
    /**
     * From Trade Name (Consignor Trade name)
     * 
     */
    @JsonProperty("fromTrdName")
    @JsonPropertyDescription("From Trade Name (Consignor Trade name)")
    private String fromTrdName;
    /**
     * From Address Line 1 (Valid Special Chars #,-,/)
     * 
     */
    @JsonProperty("fromAddr1")
    @JsonPropertyDescription("From Address Line 1 (Valid Special Chars #,-,/)")
    private String fromAddr1;
    /**
     * From Address Line 2(Valid Special Chars # , - ,/)
     * 
     */
    @JsonProperty("fromAddr2")
    @JsonPropertyDescription("From Address Line 2(Valid Special Chars # , - ,/)")
    private String fromAddr2;
    /**
     * From Place
     * 
     */
    @JsonProperty("fromPlace")
    @JsonPropertyDescription("From Place")
    private String fromPlace;
    /**
     * Dispatch From State Code
     * (Required)
     * 
     */
    @JsonProperty("actualFromStateCode")
    @JsonPropertyDescription("Dispatch From State Code")
    private Integer actualFromStateCode;
    /**
     * From Pincode
     * (Required)
     * 
     */
    @JsonProperty("fromPincode")
    @JsonPropertyDescription("From Pincode")
    private Integer fromPincode;
    /**
     * Bill From State Code
     * (Required)
     * 
     */
    @JsonProperty("fromStateCode")
    @JsonPropertyDescription("Bill From State Code")
    private Integer fromStateCode;
    /**
     * To GSTIN (Consignee or Recipient)
     * (Required)
     * 
     */
    @JsonProperty("toGstin")
    @JsonPropertyDescription("To GSTIN (Consignee or Recipient)")
    private String toGstin;
    /**
     * To Trade Name (Consignee Trade name or Recipient Trade name)
     * 
     */
    @JsonProperty("toTrdName")
    @JsonPropertyDescription("To Trade Name (Consignee Trade name or Recipient Trade name)")
    private String toTrdName;
    /**
     * To Address Line 1 (Valid Special Chars #,-,/)
     * 
     */
    @JsonProperty("toAddr1")
    @JsonPropertyDescription("To Address Line 1 (Valid Special Chars #,-,/)")
    private String toAddr1;
    /**
     * To Address Line 2 (Valid Special Chars #,-,/)
     * 
     */
    @JsonProperty("toAddr2")
    @JsonPropertyDescription("To Address Line 2 (Valid Special Chars #,-,/)")
    private String toAddr2;
    /**
     * To Place
     * 
     */
    @JsonProperty("toPlace")
    @JsonPropertyDescription("To Place")
    private String toPlace;
    /**
     * To Pincode
     * (Required)
     * 
     */
    @JsonProperty("toPincode")
    @JsonPropertyDescription("To Pincode")
    private Integer toPincode;
    /**
     * Dispatch To State Code
     * (Required)
     * 
     */
    @JsonProperty("actualToStateCode")
    @JsonPropertyDescription("Dispatch To State Code")
    private Integer actualToStateCode;
    /**
     * Bill To State Code
     * (Required)
     * 
     */
    @JsonProperty("toStateCode")
    @JsonPropertyDescription("Bill To State Code")
    private Integer toStateCode;
    /**
     * Sum of Taxable value and Tax value
     * 
     */
    @JsonProperty("totalValue")
    @JsonPropertyDescription("Sum of Taxable value and Tax value")
    private Double totalValue;
    /**
     * CGST value
     * 
     */
    @JsonProperty("cgstValue")
    @JsonPropertyDescription("CGST value")
    private Double cgstValue;
    /**
     * SGST value
     * 
     */
    @JsonProperty("sgstValue")
    @JsonPropertyDescription("SGST value")
    private Double sgstValue;
    /**
     * IGST value
     * 
     */
    @JsonProperty("igstValue")
    @JsonPropertyDescription("IGST value")
    private Double igstValue;
    /**
     * Cess value
     * 
     */
    @JsonProperty("cessValue")
    @JsonPropertyDescription("Cess value")
    private Double cessValue;
    /**
     * Total Invoice Value
     * (Required)
     * 
     */
    @JsonProperty("totInvValue")
    @JsonPropertyDescription("Total Invoice Value")
    private Double totInvValue;
    /**
     * Mode of transport (Road-1, Rail-2, Air-3, Ship-4) 
     * 
     */
    @JsonProperty("transMode")
    @JsonPropertyDescription("Mode of transport (Road-1, Rail-2, Air-3, Ship-4) ")
    private Double transMode;
    /**
     * Distance (<=4000 km) 
     * (Required)
     * 
     */
    @JsonProperty("transDistance")
    @JsonPropertyDescription("Distance (<=4000 km) ")
    private Double transDistance;
    /**
     * Name of the transporter
     * 
     */
    @JsonProperty("transporterName")
    @JsonPropertyDescription("Name of the transporter")
    private String transporterName;
    /**
     *  15 Char Transporter GSTIN/TRANSIN
     * 
     */
    @JsonProperty("transporterId")
    @JsonPropertyDescription("15 Char Transporter GSTIN/TRANSIN")
    private String transporterId;
    /**
     * Transport Document Number (Alphanumeric with / and – are allowed)
     * 
     */
    @JsonProperty("transDocNo")
    @JsonPropertyDescription("Transport Document Number (Alphanumeric with / and \u2013 are allowed)")
    private String transDocNo;
    /**
     * Transport Document Date
     * 
     */
    @JsonProperty("transDocDate")
    @JsonPropertyDescription("Transport Document Date")
    private String transDocDate;
    /**
     * Vehicle Number
     * 
     */
    @JsonProperty("vehicleNo")
    @JsonPropertyDescription("Vehicle Number")
    private String vehicleNo;
    /**
     * Vehicle Type
     * 
     */
    @JsonProperty("vehicleType")
    @JsonPropertyDescription("Vehicle Type")
    private String vehicleType;
    /**
     * HSN Code
     * 
     */
    @JsonProperty("mainHsnCode")
    @JsonPropertyDescription("HSN Code")
    private Double mainHsnCode;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("itemList")
    private List<Item> itemList = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public BillList() {
    }

    /**
     * 
     * @param fromPincode
     * @param toPincode
     * @param subSupplyType
     * @param docType
     * @param docNo
     * @param fromStateCode
     * @param actualFromStateCode
     * @param toAddr1
     * @param fromTrdName
     * @param toAddr2
     * @param igstValue
     * @param fromAddr2
     * @param fromAddr1
     * @param toGstin
     * @param toPlace
     * @param transDistance
     * @param docDate
     * @param vehicleNo
     * @param mainHsnCode
     * @param transDocDate
     * @param actualToStateCode
     * @param toTrdName
     * @param fromPlace
     * @param sgstValue
     * @param toStateCode
     * @param totInvValue
     * @param transDocNo
     * @param transporterName
     * @param cessValue
     * @param totalValue
     * @param transMode
     * @param userGstin
     * @param transporterId
     * @param cgstValue
     * @param itemList
     * @param vehicleType
     * @param fromGstin
     * @param supplyType
     */
    public BillList(String userGstin, BillList.SupplyType supplyType, Double subSupplyType, BillList.DocType docType, String docNo, String docDate, String fromGstin, String fromTrdName, String fromAddr1, String fromAddr2, String fromPlace, Integer actualFromStateCode, Integer fromPincode, Integer fromStateCode, String toGstin, String toTrdName, String toAddr1, String toAddr2, String toPlace, Integer toPincode, Integer actualToStateCode, Integer toStateCode, Double totalValue, Double cgstValue, Double sgstValue, Double igstValue, Double cessValue, Double totInvValue, Double transMode, Double transDistance, String transporterName, String transporterId, String transDocNo, String transDocDate, String vehicleNo, String vehicleType, Double mainHsnCode, List<Item> itemList) {
        super();
        this.userGstin = userGstin;
        this.supplyType = supplyType;
        this.subSupplyType = subSupplyType;
        this.docType = docType;
        this.docNo = docNo;
        this.docDate = docDate;
        this.fromGstin = fromGstin;
        this.fromTrdName = fromTrdName;
        this.fromAddr1 = fromAddr1;
        this.fromAddr2 = fromAddr2;
        this.fromPlace = fromPlace;
        this.actualFromStateCode = actualFromStateCode;
        this.fromPincode = fromPincode;
        this.fromStateCode = fromStateCode;
        this.toGstin = toGstin;
        this.toTrdName = toTrdName;
        this.toAddr1 = toAddr1;
        this.toAddr2 = toAddr2;
        this.toPlace = toPlace;
        this.toPincode = toPincode;
        this.actualToStateCode = actualToStateCode;
        this.toStateCode = toStateCode;
        this.totalValue = totalValue;
        this.cgstValue = cgstValue;
        this.sgstValue = sgstValue;
        this.igstValue = igstValue;
        this.cessValue = cessValue;
        this.totInvValue = totInvValue;
        this.transMode = transMode;
        this.transDistance = transDistance;
        this.transporterName = transporterName;
        this.transporterId = transporterId;
        this.transDocNo = transDocNo;
        this.transDocDate = transDocDate;
        this.vehicleNo = vehicleNo;
        this.vehicleType = vehicleType;
        this.mainHsnCode = mainHsnCode;
        this.itemList = itemList;
    }

    /**
     * User GSTIN
     * 
     */
    @JsonProperty("userGstin")
    public String getUserGstin() {
        return userGstin;
    }

    /**
     * User GSTIN
     * 
     */
    @JsonProperty("userGstin")
    public void setUserGstin(String userGstin) {
        this.userGstin = userGstin;
    }

    /**
     * Supply Type
     * (Required)
     * 
     */
    @JsonProperty("supplyType")
    public BillList.SupplyType getSupplyType() {
        return supplyType;
    }

    /**
     * Supply Type
     * (Required)
     * 
     */
    @JsonProperty("supplyType")
    public void setSupplyType(BillList.SupplyType supplyType) {
        this.supplyType = supplyType;
    }

    /**
     * Sub Supply Type
     * (Required)
     * 
     */
    @JsonProperty("subSupplyType")
    public Double getSubSupplyType() {
        return subSupplyType;
    }

    /**
     * Sub Supply Type
     * (Required)
     * 
     */
    @JsonProperty("subSupplyType")
    public void setSubSupplyType(Double subSupplyType) {
        this.subSupplyType = subSupplyType;
    }

    /**
     * Document Type
     * (Required)
     * 
     */
    @JsonProperty("docType")
    public BillList.DocType getDocType() {
        return docType;
    }

    /**
     * Document Type
     * (Required)
     * 
     */
    @JsonProperty("docType")
    public void setDocType(BillList.DocType docType) {
        this.docType = docType;
    }

    /**
     * Document Number (Alphanumeric with / and - are allowed)
     * (Required)
     * 
     */
    @JsonProperty("docNo")
    public String getDocNo() {
        return docNo;
    }

    /**
     * Document Number (Alphanumeric with / and - are allowed)
     * (Required)
     * 
     */
    @JsonProperty("docNo")
    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    /**
     * Document Date
     * (Required)
     * 
     */
    @JsonProperty("docDate")
    public String getDocDate() {
        return docDate;
    }

    /**
     * Document Date
     * (Required)
     * 
     */
    @JsonProperty("docDate")
    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    /**
     * From GSTIN (Supplier or Consignor)
     * (Required)
     * 
     */
    @JsonProperty("fromGstin")
    public String getFromGstin() {
        return fromGstin;
    }

    /**
     * From GSTIN (Supplier or Consignor)
     * (Required)
     * 
     */
    @JsonProperty("fromGstin")
    public void setFromGstin(String fromGstin) {
        this.fromGstin = fromGstin;
    }

    /**
     * From Trade Name (Consignor Trade name)
     * 
     */
    @JsonProperty("fromTrdName")
    public String getFromTrdName() {
        return fromTrdName;
    }

    /**
     * From Trade Name (Consignor Trade name)
     * 
     */
    @JsonProperty("fromTrdName")
    public void setFromTrdName(String fromTrdName) {
        this.fromTrdName = fromTrdName;
    }

    /**
     * From Address Line 1 (Valid Special Chars #,-,/)
     * 
     */
    @JsonProperty("fromAddr1")
    public String getFromAddr1() {
        return fromAddr1;
    }

    /**
     * From Address Line 1 (Valid Special Chars #,-,/)
     * 
     */
    @JsonProperty("fromAddr1")
    public void setFromAddr1(String fromAddr1) {
        this.fromAddr1 = fromAddr1;
    }

    /**
     * From Address Line 2(Valid Special Chars # , - ,/)
     * 
     */
    @JsonProperty("fromAddr2")
    public String getFromAddr2() {
        return fromAddr2;
    }

    /**
     * From Address Line 2(Valid Special Chars # , - ,/)
     * 
     */
    @JsonProperty("fromAddr2")
    public void setFromAddr2(String fromAddr2) {
        this.fromAddr2 = fromAddr2;
    }

    /**
     * From Place
     * 
     */
    @JsonProperty("fromPlace")
    public String getFromPlace() {
        return fromPlace;
    }

    /**
     * From Place
     * 
     */
    @JsonProperty("fromPlace")
    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    /**
     * Dispatch From State Code
     * (Required)
     * 
     */
    @JsonProperty("actualFromStateCode")
    public Integer getActualFromStateCode() {
        return actualFromStateCode;
    }

    /**
     * Dispatch From State Code
     * (Required)
     * 
     */
    @JsonProperty("actualFromStateCode")
    public void setActualFromStateCode(Integer actualFromStateCode) {
        this.actualFromStateCode = actualFromStateCode;
    }

    /**
     * From Pincode
     * (Required)
     * 
     */
    @JsonProperty("fromPincode")
    public Integer getFromPincode() {
        return fromPincode;
    }

    /**
     * From Pincode
     * (Required)
     * 
     */
    @JsonProperty("fromPincode")
    public void setFromPincode(Integer fromPincode) {
        this.fromPincode = fromPincode;
    }

    /**
     * Bill From State Code
     * (Required)
     * 
     */
    @JsonProperty("fromStateCode")
    public Integer getFromStateCode() {
        return fromStateCode;
    }

    /**
     * Bill From State Code
     * (Required)
     * 
     */
    @JsonProperty("fromStateCode")
    public void setFromStateCode(Integer fromStateCode) {
        this.fromStateCode = fromStateCode;
    }

    /**
     * To GSTIN (Consignee or Recipient)
     * (Required)
     * 
     */
    @JsonProperty("toGstin")
    public String getToGstin() {
        return toGstin;
    }

    /**
     * To GSTIN (Consignee or Recipient)
     * (Required)
     * 
     */
    @JsonProperty("toGstin")
    public void setToGstin(String toGstin) {
        this.toGstin = toGstin;
    }

    /**
     * To Trade Name (Consignee Trade name or Recipient Trade name)
     * 
     */
    @JsonProperty("toTrdName")
    public String getToTrdName() {
        return toTrdName;
    }

    /**
     * To Trade Name (Consignee Trade name or Recipient Trade name)
     * 
     */
    @JsonProperty("toTrdName")
    public void setToTrdName(String toTrdName) {
        this.toTrdName = toTrdName;
    }

    /**
     * To Address Line 1 (Valid Special Chars #,-,/)
     * 
     */
    @JsonProperty("toAddr1")
    public String getToAddr1() {
        return toAddr1;
    }

    /**
     * To Address Line 1 (Valid Special Chars #,-,/)
     * 
     */
    @JsonProperty("toAddr1")
    public void setToAddr1(String toAddr1) {
        this.toAddr1 = toAddr1;
    }

    /**
     * To Address Line 2 (Valid Special Chars #,-,/)
     * 
     */
    @JsonProperty("toAddr2")
    public String getToAddr2() {
        return toAddr2;
    }

    /**
     * To Address Line 2 (Valid Special Chars #,-,/)
     * 
     */
    @JsonProperty("toAddr2")
    public void setToAddr2(String toAddr2) {
        this.toAddr2 = toAddr2;
    }

    /**
     * To Place
     * 
     */
    @JsonProperty("toPlace")
    public String getToPlace() {
        return toPlace;
    }

    /**
     * To Place
     * 
     */
    @JsonProperty("toPlace")
    public void setToPlace(String toPlace) {
        this.toPlace = toPlace;
    }

    /**
     * To Pincode
     * (Required)
     * 
     */
    @JsonProperty("toPincode")
    public Integer getToPincode() {
        return toPincode;
    }

    /**
     * To Pincode
     * (Required)
     * 
     */
    @JsonProperty("toPincode")
    public void setToPincode(Integer toPincode) {
        this.toPincode = toPincode;
    }

    /**
     * Dispatch To State Code
     * (Required)
     * 
     */
    @JsonProperty("actualToStateCode")
    public Integer getActualToStateCode() {
        return actualToStateCode;
    }

    /**
     * Dispatch To State Code
     * (Required)
     * 
     */
    @JsonProperty("actualToStateCode")
    public void setActualToStateCode(Integer actualToStateCode) {
        this.actualToStateCode = actualToStateCode;
    }

    /**
     * Bill To State Code
     * (Required)
     * 
     */
    @JsonProperty("toStateCode")
    public Integer getToStateCode() {
        return toStateCode;
    }

    /**
     * Bill To State Code
     * (Required)
     * 
     */
    @JsonProperty("toStateCode")
    public void setToStateCode(Integer toStateCode) {
        this.toStateCode = toStateCode;
    }

    /**
     * Sum of Taxable value and Tax value
     * 
     */
    @JsonProperty("totalValue")
    public Double getTotalValue() {
        return totalValue;
    }

    /**
     * Sum of Taxable value and Tax value
     * 
     */
    @JsonProperty("totalValue")
    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    /**
     * CGST value
     * 
     */
    @JsonProperty("cgstValue")
    public Double getCgstValue() {
        return cgstValue;
    }

    /**
     * CGST value
     * 
     */
    @JsonProperty("cgstValue")
    public void setCgstValue(Double cgstValue) {
        this.cgstValue = cgstValue;
    }

    /**
     * SGST value
     * 
     */
    @JsonProperty("sgstValue")
    public Double getSgstValue() {
        return sgstValue;
    }

    /**
     * SGST value
     * 
     */
    @JsonProperty("sgstValue")
    public void setSgstValue(Double sgstValue) {
        this.sgstValue = sgstValue;
    }

    /**
     * IGST value
     * 
     */
    @JsonProperty("igstValue")
    public Double getIgstValue() {
        return igstValue;
    }

    /**
     * IGST value
     * 
     */
    @JsonProperty("igstValue")
    public void setIgstValue(Double igstValue) {
        this.igstValue = igstValue;
    }

    /**
     * Cess value
     * 
     */
    @JsonProperty("cessValue")
    public Double getCessValue() {
        return cessValue;
    }

    /**
     * Cess value
     * 
     */
    @JsonProperty("cessValue")
    public void setCessValue(Double cessValue) {
        this.cessValue = cessValue;
    }

    /**
     * Total Invoice Value
     * (Required)
     * 
     */
    @JsonProperty("totInvValue")
    public Double getTotInvValue() {
        return totInvValue;
    }

    /**
     * Total Invoice Value
     * (Required)
     * 
     */
    @JsonProperty("totInvValue")
    public void setTotInvValue(Double totInvValue) {
        this.totInvValue = totInvValue;
    }

    /**
     * Mode of transport (Road-1, Rail-2, Air-3, Ship-4) 
     * 
     */
    @JsonProperty("transMode")
    public Double getTransMode() {
        return transMode;
    }

    /**
     * Mode of transport (Road-1, Rail-2, Air-3, Ship-4) 
     * 
     */
    @JsonProperty("transMode")
    public void setTransMode(Double transMode) {
        this.transMode = transMode;
    }

    /**
     * Distance (<=4000 km) 
     * (Required)
     * 
     */
    @JsonProperty("transDistance")
    public Double getTransDistance() {
        return transDistance;
    }

    /**
     * Distance (<=4000 km) 
     * (Required)
     * 
     */
    @JsonProperty("transDistance")
    public void setTransDistance(Double transDistance) {
        this.transDistance = transDistance;
    }

    /**
     * Name of the transporter
     * 
     */
    @JsonProperty("transporterName")
    public String getTransporterName() {
        return transporterName;
    }

    /**
     * Name of the transporter
     * 
     */
    @JsonProperty("transporterName")
    public void setTransporterName(String transporterName) {
        this.transporterName = transporterName;
    }

    /**
     *  15 Char Transporter GSTIN/TRANSIN
     * 
     */
    @JsonProperty("transporterId")
    public String getTransporterId() {
        return transporterId;
    }

    /**
     *  15 Char Transporter GSTIN/TRANSIN
     * 
     */
    @JsonProperty("transporterId")
    public void setTransporterId(String transporterId) {
        this.transporterId = transporterId;
    }

    /**
     * Transport Document Number (Alphanumeric with / and – are allowed)
     * 
     */
    @JsonProperty("transDocNo")
    public String getTransDocNo() {
        return transDocNo;
    }

    /**
     * Transport Document Number (Alphanumeric with / and – are allowed)
     * 
     */
    @JsonProperty("transDocNo")
    public void setTransDocNo(String transDocNo) {
        this.transDocNo = transDocNo;
    }

    /**
     * Transport Document Date
     * 
     */
    @JsonProperty("transDocDate")
    public String getTransDocDate() {
        return transDocDate;
    }

    /**
     * Transport Document Date
     * 
     */
    @JsonProperty("transDocDate")
    public void setTransDocDate(String transDocDate) {
        this.transDocDate = transDocDate;
    }

    /**
     * Vehicle Number
     * 
     */
    @JsonProperty("vehicleNo")
    public String getVehicleNo() {
        return vehicleNo;
    }

    /**
     * Vehicle Number
     * 
     */
    @JsonProperty("vehicleNo")
    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    /**
     * Vehicle Type
     * 
     */
    @JsonProperty("vehicleType")
    public String getVehicleType() {
        return vehicleType;
    }

    /**
     * Vehicle Type
     * 
     */
    @JsonProperty("vehicleType")
    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    /**
     * HSN Code
     * 
     */
    @JsonProperty("mainHsnCode")
    public Double getMainHsnCode() {
        return mainHsnCode;
    }

    /**
     * HSN Code
     * 
     */
    @JsonProperty("mainHsnCode")
    public void setMainHsnCode(Double mainHsnCode) {
        this.mainHsnCode = mainHsnCode;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("itemList")
    public List<Item> getItemList() {
        return itemList;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("itemList")
    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userGstin", userGstin).append("supplyType", supplyType).append("subSupplyType", subSupplyType).append("docType", docType).append("docNo", docNo).append("docDate", docDate).append("fromGstin", fromGstin).append("fromTrdName", fromTrdName).append("fromAddr1", fromAddr1).append("fromAddr2", fromAddr2).append("fromPlace", fromPlace).append("actualFromStateCode", actualFromStateCode).append("fromPincode", fromPincode).append("fromStateCode", fromStateCode).append("toGstin", toGstin).append("toTrdName", toTrdName).append("toAddr1", toAddr1).append("toAddr2", toAddr2).append("toPlace", toPlace).append("toPincode", toPincode).append("actualToStateCode", actualToStateCode).append("toStateCode", toStateCode).append("totalValue", totalValue).append("cgstValue", cgstValue).append("sgstValue", sgstValue).append("igstValue", igstValue).append("cessValue", cessValue).append("totInvValue", totInvValue).append("transMode", transMode).append("transDistance", transDistance).append("transporterName", transporterName).append("transporterId", transporterId).append("transDocNo", transDocNo).append("transDocDate", transDocDate).append("vehicleNo", vehicleNo).append("vehicleType", vehicleType).append("mainHsnCode", mainHsnCode).append("itemList", itemList).append("additionalProperties", additionalProperties).toString();
    }

    public enum DocType {

        INV("INV"),
        CHL("CHL"),
        BIL("BIL"),
        BOE("BOE"),
        CNT("CNT"),
        OTH("OTH");
        private final String value;
        private final static Map<String, BillList.DocType> CONSTANTS = new HashMap<String, BillList.DocType>();

        static {
            for (BillList.DocType c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private DocType(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static BillList.DocType fromValue(String value) {
            BillList.DocType constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

    public enum SupplyType {

        O("O"),
        I("I");
        private final String value;
        private final static Map<String, BillList.SupplyType> CONSTANTS = new HashMap<String, BillList.SupplyType>();

        static {
            for (BillList.SupplyType c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private SupplyType(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static BillList.SupplyType fromValue(String value) {
            BillList.SupplyType constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
