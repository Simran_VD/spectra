package org.mysys.ewaybill.vo;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the jobwork_eway_lv_1_vw database table.
 * 
 */
@Entity
@Table(name = "jobwork_eway_lv_1_vw")
@NamedQuery(name = "JobworkEwayLv1Vw.findAll", query = "SELECT j FROM JobworkEwayLv1Vw j")
public class JobworkEwayLv1Vw implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "actualfromstatecode")
	private Integer actualFromStateCode;
	
	@Column(name = "actualtostatecode")
	private Integer actualToStateCode;
	
	@Column(name = "cessvalue")
	private Double cessValue;
	
	@Column(name = "cgstvalue")
	private Double cgstValue;
	
	@Column(name = "docdate")
	private Timestamp docDate;
	
	@Column(name = "docno")
	private BigDecimal docNo;
	
	@Column(name = "doctype")
	private String docType;
	
	@Column(name = "fromaddr1")
	private String fromAddr1;
	
	@Column(name = "fromaddr2")
	private String fromAddr2;
	
	@Column(name = "fromgstin")
	private String fromGstin;
	
	@Column(name = "frompincode")
	private Integer fromPinCode;
	
	@Column(name = "fromplace")
	private String fromPlace;
	
	@Column(name = "fromstatecode")
	private Integer fromStateCode;
	
	@Column(name = "fromtrdname")
	private String fromTrdName;
	
	@Column(name = "igstvalue")
	private Double igstValue;

	@Id
	@Column(name = "itj_id")
	private long itjId;

	@Column(name = "mainhsncode")
	private Double mainHsnCode;
	
	@Column(name = "sgstvalue")
	private Double sgstValue;
	
	@Column(name = "siteid")
	private BigDecimal siteid;
	
	@Column(name = "subsupplytype")
	private Integer subSupplyType;
	
	@Column(name = "supplytype")
	private String supplyType;
	
	@Column(name = "toaddr1")
	private String toAddr1;
	
	@Column(name = "toaddr2")
	private String toAddr2;
	
	@Column(name = "togstin")
	private String toGstin;
	
	@Column(name = "toplace")
	private String toPlace;
	
	@Column(name = "tostatecode")
	private String toStateCode;
	
	@Column(name = "totalvalue")
	private Double totalValue;
	
	@Column(name = "totinvvalue")
	private Double totInvValue;
	
	@Column(name = "totrdname")
	private String toTrdName;
	
	@Column(name = "transdistance")
	private Double transDistance;
	
	@Column(name = "transdocdate")
	private String transDocDate;
	
	@Column(name = "transdocno")
	private String transDocNo;
	
	@Column(name = "transmode")
	private Double transMode;
	
	@Column(name = "transporterid")
	private String transporterId;
	
	@Column(name = "transportername")
	private String transporterName;
	
	@Column(name = "usergstin")
	private String userGstin;
	
	@Column(name = "vehicleno")
	private String vehicleNo;
	
	@Column(name = "vehicletype")
	private String vehicleType;

	@OneToMany(mappedBy = "jobWork", fetch = FetchType.EAGER)
	private List<JobworkEwayLv2Vw> itemList;

	public JobworkEwayLv1Vw() {
	}
	
	public Integer getActualFromStateCode() {
		return actualFromStateCode;
	}

	public void setActualFromStateCode(Integer actualFromStateCode) {
		this.actualFromStateCode = actualFromStateCode;
	}

	public Integer getActualToStateCode() {
		return actualToStateCode;
	}

	public void setActualToStateCode(Integer actualToStateCode) {
		this.actualToStateCode = actualToStateCode;
	}

	public Double getCessValue() {
		return cessValue;
	}

	public void setCessValue(Double cessValue) {
		this.cessValue = cessValue;
	}

	public Double getCgstValue() {
		return cgstValue;
	}

	public void setCgstValue(Double cgstValue) {
		this.cgstValue = cgstValue;
	}

	public Timestamp getDocDate() {
		return docDate;
	}

	public void setDocDate(Timestamp docDate) {
		this.docDate = docDate;
	}

	public BigDecimal getDocNo() {
		return docNo;
	}

	public void setDocNo(BigDecimal docNo) {
		this.docNo = docNo;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getFromAddr1() {
		return fromAddr1;
	}

	public void setFromAddr1(String fromAddr1) {
		this.fromAddr1 = fromAddr1;
	}

	public String getFromAddr2() {
		return fromAddr2;
	}

	public void setFromAddr2(String fromAddr2) {
		this.fromAddr2 = fromAddr2;
	}

	public String getFromGstin() {
		return fromGstin;
	}

	public void setFromGstin(String fromGstin) {
		this.fromGstin = fromGstin;
	}

	public Integer getFromPinCode() {
		return fromPinCode;
	}

	public void setFromPinCode(Integer fromPinCode) {
		this.fromPinCode = fromPinCode;
	}

	public String getFromPlace() {
		return fromPlace;
	}

	public void setFromPlace(String fromPlace) {
		this.fromPlace = fromPlace;
	}

	public Integer getFromStateCode() {
		return fromStateCode;
	}

	public void setFromStateCode(Integer fromStateCode) {
		this.fromStateCode = fromStateCode;
	}

	public String getFromTrdName() {
		return fromTrdName;
	}

	public void setFromTrdName(String fromTrdName) {
		this.fromTrdName = fromTrdName;
	}

	public Double getIgstValue() {
		return igstValue;
	}

	public void setIgstValue(Double igstValue) {
		this.igstValue = igstValue;
	}

	public long getItjId() {
		return itjId;
	}

	public void setItjId(long itjId) {
		this.itjId = itjId;
	}

	public Double getMainHsnCode() {
		return mainHsnCode;
	}

	public void setMainHsnCode(Double mainHsnCode) {
		this.mainHsnCode = mainHsnCode;
	}

	public Double getSgstValue() {
		return sgstValue;
	}

	public void setSgstValue(Double sgstValue) {
		this.sgstValue = sgstValue;
	}

	public BigDecimal getSiteid() {
		return siteid;
	}

	public void setSiteid(BigDecimal siteid) {
		this.siteid = siteid;
	}

	public Integer getSubSupplyType() {
		return subSupplyType;
	}

	public void setSubSupplyType(Integer subSupplyType) {
		this.subSupplyType = subSupplyType;
	}

	public String getSupplyType() {
		return supplyType;
	}

	public void setSupplyType(String supplyType) {
		this.supplyType = supplyType;
	}

	public String getToAddr1() {
		return toAddr1;
	}

	public void setToAddr1(String toAddr1) {
		this.toAddr1 = toAddr1;
	}

	public String getToAddr2() {
		return toAddr2;
	}

	public void setToAddr2(String toAddr2) {
		this.toAddr2 = toAddr2;
	}

	public String getToGstin() {
		return toGstin;
	}

	public void setToGstin(String toGstin) {
		this.toGstin = toGstin;
	}

	public String getToPlace() {
		return toPlace;
	}

	public void setToPlace(String toPlace) {
		this.toPlace = toPlace;
	}

	public String getToStateCode() {
		return toStateCode;
	}

	public void setToStateCode(String toStateCode) {
		this.toStateCode = toStateCode;
	}

	public Double getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(Double totalValue) {
		this.totalValue = totalValue;
	}

	public Double getTotInvValue() {
		return totInvValue;
	}

	public void setTotInvValue(Double totInvValue) {
		this.totInvValue = totInvValue;
	}

	public String getToTrdName() {
		return toTrdName;
	}

	public void setToTrdName(String toTrdName) {
		this.toTrdName = toTrdName;
	}

	public Double getTransDistance() {
		return transDistance;
	}

	public void setTransDistance(Double transDistance) {
		this.transDistance = transDistance;
	}

	public String getTransDocDate() {
		return transDocDate;
	}

	public void setTransDocDate(String transDocDate) {
		this.transDocDate = transDocDate;
	}

	public String getTransDocNo() {
		return transDocNo;
	}

	public void setTransDocNo(String transDocNo) {
		this.transDocNo = transDocNo;
	}

	public Double getTransMode() {
		return transMode;
	}

	public void setTransMode(Double transMode) {
		this.transMode = transMode;
	}

	public String getTransporterId() {
		return transporterId;
	}

	public void setTransporterId(String transporterId) {
		this.transporterId = transporterId;
	}

	public String getTransporterName() {
		return transporterName;
	}

	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}

	public String getUserGstin() {
		return userGstin;
	}

	public void setUserGstin(String userGstin) {
		this.userGstin = userGstin;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public List<JobworkEwayLv2Vw> getItemList() {
		return itemList;
	}

	public void setItemList(List<JobworkEwayLv2Vw> itemList) {
		this.itemList = itemList;
	}


}