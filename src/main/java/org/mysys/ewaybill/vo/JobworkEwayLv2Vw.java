package org.mysys.ewaybill.vo;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the jobwork_eway_lv_2_vw database table.
 * 
 */
@Entity
@Table(name="jobwork_eway_lv_2_vw")
@NamedQuery(name="JobworkEwayLv2Vw.findAll", query="SELECT j FROM JobworkEwayLv2Vw j")
public class JobworkEwayLv2Vw implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private JobWorkEwayLv2VwPK id;
	
	private BigDecimal cessRate;

	private BigDecimal cgstRate;

	private BigDecimal hsnCode;

	private BigDecimal igstRate;

	private String productDesc;

	private String productName;

	private String qtyUnit;

	private BigDecimal quantity;

	private BigDecimal sgstRate;

	private BigDecimal siteid;

	private BigDecimal taxableAmount;
	
	@ManyToOne
    @JoinColumn(name="itj_id", nullable=false, insertable=false, updatable=false)
	private JobworkEwayLv1Vw jobWork;
	
	public JobworkEwayLv2Vw() {
	}

	
	public JobWorkEwayLv2VwPK getId() {
		return id;
	}

	public void setId(JobWorkEwayLv2VwPK id) {
		this.id = id;
	}

	public JobworkEwayLv1Vw getJobWork() {
		return jobWork;
	}

	public void setJobWork(JobworkEwayLv1Vw jobWork) {
		this.jobWork = jobWork;
	}


	


	public BigDecimal getCessRate() {
		return cessRate;
	}


	public void setCessRate(BigDecimal cessRate) {
		this.cessRate = cessRate;
	}


	public BigDecimal getCgstRate() {
		return cgstRate;
	}


	public void setCgstRate(BigDecimal cgstRate) {
		this.cgstRate = cgstRate;
	}


	public BigDecimal getHsnCode() {
		return hsnCode;
	}


	public void setHsnCode(BigDecimal hsnCode) {
		this.hsnCode = hsnCode;
	}


	public BigDecimal getIgstRate() {
		return igstRate;
	}


	public void setIgstRate(BigDecimal igstRate) {
		this.igstRate = igstRate;
	}


	public String getProductDesc() {
		return productDesc;
	}


	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getQtyUnit() {
		return qtyUnit;
	}


	public void setQtyUnit(String qtyUnit) {
		this.qtyUnit = qtyUnit;
	}


	public BigDecimal getQuantity() {
		return quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getSgstRate() {
		return sgstRate;
	}


	public void setSgstRate(BigDecimal sgstRate) {
		this.sgstRate = sgstRate;
	}


	public BigDecimal getSiteid() {
		return siteid;
	}


	public void setSiteid(BigDecimal siteid) {
		this.siteid = siteid;
	}


	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}


	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

}