package org.mysys.ewaybill.controller;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.mysys.ewaybill.service.EWayBillService;
import org.mysys.ewaybill.vo.EBill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Controller
public class EwayBillController {
	@Autowired
	private EWayBillService service;
	
	@GetMapping(path="/ewaybill/{module}/download")
	public void downloadEwayBill(@RequestParam List<Long> ids, @PathVariable String module,
			HttpServletResponse response){

		EBill ebill = service.getEBill(module, ids);
		response.setContentType("application/json");
		response.addHeader("Content-Disposition", "attachment; filename="+Instant.now().toEpochMilli()+".json");
		try
		{
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.writeValue(response.getOutputStream(), ebill);
			response.getOutputStream().flush();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}
