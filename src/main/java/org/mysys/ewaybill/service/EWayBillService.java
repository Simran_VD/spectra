package org.mysys.ewaybill.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mysys.constant.APPConstant;
import org.mysys.ewaybill.repository.JobWorkEwayRepo;
import org.mysys.ewaybill.repository.SaleinvEwayRepo;
import org.mysys.ewaybill.vo.BillList;
import org.mysys.ewaybill.vo.EBill;
import org.mysys.ewaybill.vo.Item;
import org.mysys.ewaybill.vo.JobworkEwayLv1Vw;
import org.mysys.ewaybill.vo.JobworkEwayLv2Vw;
import org.mysys.ewaybill.vo.SaleinvEwayLv1Vw;
import org.mysys.ewaybill.vo.SaleinvEwayLv2Vw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Service
public class EWayBillService {
	@Autowired
	private SaleinvEwayRepo saleInvRepo;
	
	@Autowired
	private JobWorkEwayRepo jobWorkEwayRepo;
	
	@Autowired
	private BoundMapperFacade<SaleinvEwayLv1Vw, BillList> boundMapper;
	
	@Autowired
	private BoundMapperFacade<JobworkEwayLv1Vw, BillList> jobWorkBoundMapper;
	
	

	@Transactional
	public Optional<List<BillList>> getSaleInvBill(final List<Long> saleInvId){
		return Optional.of(saleInvRepo.findBySiIdIn(saleInvId).stream().filter((item) -> item != null)
				.map(boundMapper::map).collect(Collectors.toList()));
	}
	
	@Transactional
	public Optional<List<BillList>> getJobWorkEbill(final List<Long> jobWorkIds){
		return Optional.of(jobWorkEwayRepo.findByItjIdIn(jobWorkIds)
				.stream().filter((item) -> item != null)
				.map(jobWorkBoundMapper::map).collect(Collectors.toList()));
	}
	
	@Bean
	public BoundMapperFacade<SaleinvEwayLv1Vw, BillList> boundMapper(){
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(SaleinvEwayLv1Vw.class, BillList.class);
		mapperFactory.classMap(SaleinvEwayLv2Vw.class, Item.class);
		return mapperFactory.getMapperFacade(SaleinvEwayLv1Vw.class, BillList.class);
	}
	
	
	@Bean
	public BoundMapperFacade<JobworkEwayLv1Vw, BillList> jobWorkBoundMapper(){
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(JobworkEwayLv1Vw.class, BillList.class);
		mapperFactory.classMap(JobworkEwayLv2Vw.class, Item.class);
		return mapperFactory.getMapperFacade(JobworkEwayLv1Vw.class, BillList.class);
	}
	
	
	public EBill getEBill(final String ebillFor, final List<Long> id){
		final EBill ebill = new EBill();
		ebill.setVersion(APPConstant.EBILL.VERSION.toString());
		switch(ebillFor){
		case "Invoice":
			getSaleInvBill(id).ifPresent(ebill::setBillLists);
			break;
		case "Jobwork":
			getJobWorkEbill(id).ifPresent(ebill::setBillLists);
			break;
		}
		return ebill;
	}
	
}
