package org.mysys.ewaybill.repository;

import java.util.List;

import org.mysys.ewaybill.vo.JobworkEwayLv1Vw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobWorkEwayRepo extends JpaRepository<JobworkEwayLv1Vw, Long> {
	public List<JobworkEwayLv1Vw> findByItjIdIn(List<Long> ids);
}
