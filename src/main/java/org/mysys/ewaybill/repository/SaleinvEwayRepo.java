package org.mysys.ewaybill.repository;

import java.util.List;

import org.mysys.ewaybill.vo.SaleinvEwayLv1Vw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleinvEwayRepo extends JpaRepository<SaleinvEwayLv1Vw, Long> {
	public List<SaleinvEwayLv1Vw> findBySiIdIn(List<Long> ids);
}
