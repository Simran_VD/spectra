package org.mysys.widget.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@NamedQuery(name="Widget.findAll", query="SELECT b FROM Widget b")
public class Widget {
	@Id
	private long widgetId;
	
	private String widgetName;
	
	private String widgetTitle;
	
	private String widgetJson;
	
	@JsonIgnore
	private String widgetQuery;
	
	public String getWidgetQuery() {
		return widgetQuery;
	}
	public void setWidgetQuery(String widgetQuery) {
		this.widgetQuery = widgetQuery;
	}
	public long getWidgetId() {
		return widgetId;
	}
	public void setWidgetId(long widgetId) {
		this.widgetId = widgetId;
	}
	public String getWidgetName() {
		return widgetName;
	}
	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}
	public String getWidgetTitle() {
		return widgetTitle;
	}
	public void setWidgetTitle(String widgetTitle) {
		this.widgetTitle = widgetTitle;
	}
	public String getWidgetJson() {
		return widgetJson;
	}
	public void setWidgetJson(String widgetJson) {
		this.widgetJson = widgetJson;
	}
}
