package org.mysys.widget.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.IntStream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.mysys.model.User;
import org.mysys.repository.CommonRepo;
import org.mysys.repository.UserRepository;
import org.mysys.service.AbstractService;
import org.mysys.widget.model.Widget;
import org.mysys.widget.repository.WidgetRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;

@Service
public class WidgetService extends AbstractService {
	@Autowired
	private WidgetRepo widgetRepo;
	
	@Cacheable(value="widget")
	public Optional<Widget> getWidget(final long widgetId){
		
		return widgetRepo.findById(widgetId);
		
	}
	
	@Transactional
	public Set<Widget> getAllWidgetForUser(final long userId){
		 final Set<Widget> allWidget = new HashSet<>();
		 Optional<User> user = userRepo.findById(userId);
		 user.map(u -> u.getUserroles())
				 .ifPresent(userRole -> {
					 userRole.forEach(role -> {
						 Optional.of(role.getRole().getRolewidgetaccesses())
						 .ifPresent(widgets -> {
							 widgets.forEach(wd -> allWidget
									 .add(wd.getWidget()));
						 });
					 });
				 });
		 
		return allWidget;
	}
	
	@Autowired
	CommonRepo commonRepo;
	
	@PersistenceContext
	EntityManager session;
	@Transactional
	public JsonNode widgetData(final long widgetId){
		final ObjectNode root = JsonNodeFactory.instance.objectNode();
		getWidget(widgetId).ifPresent(widget ->{
			Session unwrapped = session.unwrap(Session.class);
			unwrapped.doWork((connection) -> {
				PreparedStatement pstmt = connection.prepareStatement(widget.getWidgetQuery());
				ResultSet rs = pstmt.executeQuery();
				ResultSetMetaData metaData = rs.getMetaData();
				int columnCount = metaData.getColumnCount();
				
				while(rs.next()){
					IntStream.rangeClosed(1, columnCount).forEach(idx -> {
						try {
							ArrayNode node = (ArrayNode) root.get(metaData.getColumnName(idx));
							ValueNode dataNode = JsonNodeFactory.instance.textNode(rs.getString(idx));
							if(node == null){
								node = JsonNodeFactory.instance.arrayNode();
							}
							node.add(dataNode);
							root.set(metaData.getColumnName(idx), node);
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					});
				}

			});
		});
		return root;
	}

	
	
}
