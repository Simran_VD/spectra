package org.mysys.widget.repository;

import org.mysys.widget.model.Widget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WidgetRepo extends JpaRepository<Widget, Long> {

}
