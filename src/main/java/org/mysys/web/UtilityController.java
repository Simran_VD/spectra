package org.mysys.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mysys.config.ApplicationDynamicPath;
import org.mysys.model.job.JobMaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/utility")
@CrossOrigin(origins = "*")
public class UtilityController {
	
	@Autowired
	HttpServletResponse response;
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	/************************GET IMAGE AND DISPLAY**********************************/
	@GetMapping(path = "/uploads/{type:.+}/{filename:.+}")
	@Transactional
	public @ResponseBody void image(@PathVariable String type,@PathVariable String filename) {
		/* using type variable for folder path */ 
		String data=null;
		InputStream in=null;
		/******DEFINING FILE PATH OF LIVE******/
		File filePath=null;
		String originalFileExtension=null;
	 
		try {		
		
		 if(filename!=null) {
			 filePath=new File(ApplicationDynamicPath.getPath()+ File.separator +  type + File.separator + filename );
		 
		 System.out.println(filename);
		 if(filePath!=null) {
			
			 if(filePath.exists()) {
				 /*********GET FILE EXTENSION FROM FILE NAME*********/
				 originalFileExtension=FilenameUtils.getExtension(filename);
				 response.setContentType("image/"+originalFileExtension);
				 in =new FileInputStream(filePath);
			     IOUtils.copy(in, response.getOutputStream());
			     in.close();
			    
			 }else {
				 data= "File not found!";
			 }
		 }
		}
		
	 else {
		 throw new RuntimeException("No parameter found for filename!");
	 }
		 
	 } catch (Exception e) {
		e.printStackTrace();		
	 }	

	 }
	
	
}
