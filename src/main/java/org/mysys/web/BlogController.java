package org.mysys.web;

import org.mysys.model.XmlUrl;
import org.mysys.service.XmlUrlSet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/blog")
public class BlogController {
	
	@RequestMapping("/the-5-most-common-ro-problems-and-solutions")
	public String commonProblem(Model m)
	{ 
		String title="Better Buddys- Home Services- RO & AC service | The 5 Most Common RO Problems and Solutions";
		m.addAttribute("title", title);
		
		String keywords="Kent RO water purifier troubleshooting, Aquaguard water purifier not working, best home water purifier,ro service,reverse osmosis,on tap water filter,filter water purifier, filter for ro, at home water filter";
		m.addAttribute("keywords",keywords);

		String description="Common RO problems you face and their solutions";
		m.addAttribute("description", description);
		return "blog/the-5-most-common-ro-problems-and-solutions";
	}
	
	@RequestMapping("/best-water-purifiers-in-india-and-their-features")
	public String bestWaterPurifiers(Model m)
	{
		String title="Better Buddys- Home Services- RO & AC service | Top 5 Best Water Purifiers in India";
		m.addAttribute("title", title);
		
		String keywords="Kent ro, Pureit ro, Havells ro, best purifier water in india, best water purifier ro, best ro water purifier in india, best ro in india, ro water purifier service";
		m.addAttribute("keywords",keywords);
		
		String description="Kent water purifier, AO Smith water purifier, Eureka Forbes water purifier, Havells water purifier, PureIt water purifiers are the top best water purifiers in India.";
		m.addAttribute("description", description);
		return "blog/best-water-purifiers-in-india-and-their-features";
	}
	
	@RequestMapping("/RO-vs-UV")	
	public String roVsUV(Model m)
	{
		String title="Better Buddys- Home Services- RO & AC service | RO vs UV";
		m.addAttribute("title", title);
		
		String keywords="RO vs UV water purifier, difference between RO and UV water purifier, RO water purifier, UV water purifier, ro uv water purifier, ro uv, ro purified water";
		m.addAttribute("keywords",keywords);
		
		String description="Difference between RO and UV can help us to make a decision about which water purifier is best for our health.";
		m.addAttribute("description", description);
		return "blog/RO-vs-UV";
	}
	
	@RequestMapping("/how-to-choose-the-best-water-purifier-system-for-a-home-office-in-India")
	public String howToChooseBestWaterPurifier(Model m)
	{
		String title="Better Buddys- Home Services- RO & AC service | How to choose the best water purifier";
		m.addAttribute("title", title);
		
		String keywords="home water purifier, home water filter, best home water purifier in india, best home water purifier";
		m.addAttribute("keywords",keywords);
		
		String description="Choosing best water purifier for home or best water purifier for office use in India can be a tough decision.";
		m.addAttribute("description", description);
		return "blog/how-to-choose-the-best-water-purifier-system-for-a-home-office-in-India";
	}
	
	@RequestMapping("/types-of-water-purifiers-and-how-they-work")
	public String typesOfWaterPurifiersWork(Model m)
	{
		String title="Better Buddys- Home Services- RO & AC service | Types of Water Purifiers";
		m.addAttribute("title", title);
		
		String keywords="Types of Water Purifiers, uv water purifier, ro water purifier";
		m.addAttribute("keywords",keywords);

		String description="Types of water purifiers are RO water purifier, UV water purifier, UF water purifier.";
		m.addAttribute("description", description);
		return "blog/types-of-water-purifiers-and-how-they-work";
	}
	
	@RequestMapping(value={"/robots.txt", "/robot.txt"})
	@ResponseBody
	public String getRobotsTxt() {
	    return "User-Agent: *\n" +
	            "Allow: / \n" +
	    		"sitemap: http://betterbuddys.com/blog/sitemap.xml";
	}
	
	
	@RequestMapping(value = "/sitemap.xml", method = RequestMethod.GET)
    @ResponseBody
    public XmlUrlSet main() {
        XmlUrlSet xmlUrlSet = new XmlUrlSet();
        create(xmlUrlSet, "", XmlUrl.Priority.HIGH);
        create(xmlUrlSet, "blog/the-5-most-common-ro-problems-and-solutions", XmlUrl.Priority.HIGH);
        create(xmlUrlSet, "blog/best-water-purifiers-in-india-and-their-features", XmlUrl.Priority.MEDIUM);
        create(xmlUrlSet, "blog/RO-vs-UV", XmlUrl.Priority.MEDIUM);
        create(xmlUrlSet, "blog/how-to-choose-the-best-water-purifier-system-for-a-home-office-in-India", XmlUrl.Priority.MEDIUM);
        create(xmlUrlSet, "blog/types-of-water-purifiers-and-how-they-work", XmlUrl.Priority.MEDIUM);


        return xmlUrlSet;
    }

    private void create(XmlUrlSet xmlUrlSet, String link, XmlUrl.Priority priority) {
        xmlUrlSet.addUrl(new XmlUrl("http://betterbuddys.com/" + link, priority));
    }

	
	
	
}
