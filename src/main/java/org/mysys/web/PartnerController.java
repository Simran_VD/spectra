package org.mysys.web;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.mysys.model.PartnerDetBalVw;
//import org.mysys.model.PartnerTeamVw;
//import org.mysys.model.PartnerToNotifyVw;
import org.mysys.model.Service.ServliceLeadMapVw;
//import org.mysys.model.partner.AreaVw;
//import org.mysys.model.partner.PartnerMst;
//import org.mysys.model.partner.PartnerMstVw;
//import org.mysys.model.partner.PartnerTeam;
//import org.mysys.model.partner.PincodeMstVw;
//import org.mysys.model.partner.ServiceMstVw;
//import org.mysys.model.product.PartnerLedger;
//import org.mysys.model.product.PaymentModeMst;
//import org.mysys.model.product.TranTypeMst;
import org.mysys.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class PartnerController {

	private static final Logger LOGGER = LogManager.getLogger();

	@Autowired
	PartnerService partnerService;
	
//	@Autowired
//	ProductService productService;

	@Autowired
	protected Environment env;

//	@GetMapping("/partner/areas")
//	@ResponseBody
//	public List<AreaVw> getAreas(Long cityid){
//		return partnerService.getArea(cityid);
//	}
//	
//	@GetMapping("/partner/getPincode")
//	@ResponseBody
//	public List<PincodeMstVw> getPincode(long cityId) {
//		return partnerService.getPincode(cityId);
//	}
	
//	@GetMapping("/partner/getPartnerList")
//	@ResponseBody
//	public List<PartnerMstVw> getPartnerList(){
//		return partnerService.getPartnerList();
//	}
	

//	@GetMapping("/partner/productServices")
//	@ResponseBody
//	public List<ServiceMstVw> productServices(){
//		return partnerService.getpProdServices();
//	}

//	@PostMapping("/partner/savePartnerMst")
//	@ResponseBody
//	public PartnerMst productServices(@RequestBody PartnerMst partnerMst){
//		return partnerService.savePartner(partnerMst);
//	}

//	@GetMapping("/partner/getPartnerMst")
//	@ResponseBody
//	public PartnerMst getPartnermst(Long pmId){
//		return partnerService.gePartnerMst(pmId);
//	}
//
//	@PostMapping("/partner/deletePartnerMst")
//	@ResponseBody
//	public String deletePartnerMst(long pmId){
//		return partnerService.deletePartnerMst(pmId);
//	}

//	@PostMapping("/partner/uploadPartnerDoc")
//	@ResponseBody
//	public String uploadPartnerDoc(
//			@RequestParam(value = "partnerImage" , required=false) MultipartFile  imgFile,
//			@RequestParam(value = "adhar" , required=false) MultipartFile addhar,
//			@RequestParam(value = "gstin" , required=false) MultipartFile gstin,
//			@RequestParam(value = "pan" , required=false) MultipartFile pan,
//			@RequestParam Long pmId) throws IOException{
//
//		if (imgFile != null) {
//			String pathOfImgWrite = env.getProperty("master.service.Image.folder") + "/empImage";
//			BufferedImage src = ImageIO.read(new ByteArrayInputStream(imgFile.getBytes()));
//			File destination = new File(pathOfImgWrite +pmId + ".png"); // something like C:/Users/tom/Documents/nameBasedOnSomeId.png
//			boolean result = Files.deleteIfExists(destination.toPath());
//			ImageIO.write(src, "png", destination);
//			String path=destination.getAbsolutePath();
//		} 		
//		partnerService.savePartnerDoc(pmId, addhar , gstin , pan );
//		return "Succcess";
//	}

//	@GetMapping("/partner/getPartnerUploadedDoc")
//	@ResponseBody
//	public ResponseEntity<Resource> getPartnerUploadedDoc(
//			@RequestParam(value = "pmId", required = false) Long pmId,
//			@RequestParam(value = "mfaId", required = false) String docType) throws IOException{
//		
//		PartnerMst resDoc =  partnerService.gePartnerMst(pmId);
//
//		if(docType.equals("adhar")) {
//			return ResponseEntity.ok()
//				.contentLength(resDoc.getAdharImage().length)
//				.contentType(MediaType.parseMediaType("application/octet-stream"))
//				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+resDoc.getAdharNo()+".png\"")
//				.body(new ByteArrayResource(resDoc.getAdharImage()));
//		} else if(docType.equals("gstin")) {
//			return ResponseEntity.ok()
//					.contentLength(resDoc.getGstinImage().length)
//					.contentType(MediaType.parseMediaType("application/octet-stream"))
//					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+resDoc.getGstin()+".png\"")
//					.body(new ByteArrayResource(resDoc.getGstinImage()));
//		} else {
//			return ResponseEntity.ok()
//					.contentLength(resDoc.getPanImage().length)
//					.contentType(MediaType.parseMediaType("application/octet-stream"))
//					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+resDoc.getPan()+".png\"")
//					.body(new ByteArrayResource(resDoc.getPanImage()));
//		}
//	}
//	
//	
	/************************** Partner Team ******************************************/
	
//	@PostMapping("/partner/savePartnerTeam")
//	@ResponseBody
//	public PartnerTeam productServices(@RequestBody PartnerTeam partnerTeam){
//		return partnerService.savePartnerTeam(partnerTeam);
//	}
//
//	@GetMapping("/partner/getPartnerTeam")
//	@ResponseBody
//	public PartnerTeam getPartnerTeam(Long ptId){
//		return partnerService.getPartnerTeam(ptId);
//	}
//
//	@PostMapping("/partner/deletePartnerTeam")
//	@ResponseBody
//	public String deletePartnerTeam(long ptId){
//		return partnerService.deletePartnerTeam(ptId);
//	}

//	@PostMapping("/partner/uploadPartnerTeamDoc")
//	@ResponseBody
//	public String uploadPartnerTeamDoc(
//			@RequestParam(value = "partnerTeamImage" , required=false) MultipartFile  imgFile,
//			@RequestParam(value = "adhar" , required=false) MultipartFile addhar,
//			@RequestParam Long ptId) throws IOException{
//
//		if (imgFile != null) {
//			String pathOfImgWrite = env.getProperty("master.service.Image.folder") + "/team";
//			BufferedImage src = ImageIO.read(new ByteArrayInputStream(imgFile.getBytes()));
//			File destination = new File(pathOfImgWrite +ptId + ".png"); // something like C:/Users/tom/Documents/nameBasedOnSomeId.png
//			boolean result = Files.deleteIfExists(destination.toPath());
//			ImageIO.write(src, "png", destination);
//			String path=destination.getAbsolutePath();
//		} 		
//		partnerService.savePartnerTeamDoc(ptId, addhar);
//		return "Succcess";
//	}

//	@GetMapping("/partner/getPartnerTeamUploadedDoc")
//	@ResponseBody
//	public ResponseEntity<Resource> getPartnerTeamUploadedDoc( @RequestParam(value = "ptId", required = false) Long ptId) throws IOException{
//		   PartnerTeam resDoc =  partnerService.getPartnerTeam(ptId);
//			return ResponseEntity.ok()
//				.contentLength(resDoc.getAdharImage().length)
//				.contentType(MediaType.parseMediaType("application/octet-stream"))
//				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+resDoc.getAdharNo()+".png\"")
//				.body(new ByteArrayResource(resDoc.getAdharImage()));
//	}

	
	
//	@PostMapping("/partner/deletePartnerDoc")
//	@ResponseBody
//	public boolean deletePartnerDoc(Long id,String type) throws IOException {	
//		String fileName = env.getProperty("application.temp.EnqFolder")+"/empImage"+id+".png";
//		File root = new File(fileName);
//		boolean result = Files.deleteIfExists(root.toPath());
//		partnerService.deletePartnerDoc(id,type);
//		return result;	
//	}
	
//	@PostMapping("/partner/deletePartnerTeamDoc")
//	@ResponseBody
//	public boolean deletePartnerTeamDoc(Long id) throws IOException {	
//		String fileName = env.getProperty("application.temp.EnqFolder")+"/team"+id+".png";
//		File root = new File(fileName);
//		boolean result = Files.deleteIfExists(root.toPath());
//		partnerService.deletePartnerteamDoc(id);
//		return result;	
//	}

//	@GetMapping("/product/getTranType")
//	@ResponseBody
//	public List<TranTypeMst> getTranType(){
//		return productService.getTranType();
//	}
//
//	@GetMapping("/product/getPaymentMode")
//	@ResponseBody
//	public List<PaymentModeMst> getPaymentMode(){
//		return productService.getPaymentMode();
//	}
//	
//	@GetMapping("/product/getPartnerLedger")
//	@ResponseBody
//	public PartnerLedger getPartnerLedger(long plId){
//		return productService.getPartnerLedger(plId);
//	}
//
//	@PostMapping("/product/savePartnerLedger")
//	@ResponseBody
//	public PartnerLedger savePartnerLedger(@RequestBody PartnerLedger partnerLedger){
//		return productService.savePartnerLedger(partnerLedger);
//	}
//	
//	@PostMapping("/product/deletePartnerLedger")
//	@ResponseBody
//	public String deletePartnerLedger(long plId){
//		return productService.deletePartnerLedger(plId);
//	}
//	
//
//	@GetMapping("/product/getPartnerDetails")
//	@ResponseBody
//	public List<ServliceLeadMapVw> getPartnerDetails(long pmId){
//		return productService.getPartnerDetails(pmId);
//	}
//	
//	@GetMapping("/partner/getPartnerTeamVw")
//	@ResponseBody
//	public List<PartnerTeamVw> getPartnerTeamVw(Long pmId){
//		return partnerService.getListPartnerTeamVw(pmId);
//	}
//	
//	@GetMapping("/partner/getPartnerToNotifyVw")
//	@ResponseBody
//	public List<PartnerToNotifyVw> getPartnerToNotifyVw(Long slId){
//		return partnerService.getListPartnerToNotifyVw(slId);
//	}
	
	
//	@GetMapping("/partner/getPartnerBal")
//	@ResponseBody
//	public PartnerDetBalVw getPartnerBal(long pmId){
//		return productService.getPartnerBal(pmId);
//	}
}
