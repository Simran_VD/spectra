package org.mysys.web;

import java.util.List;

import org.mysys.exception.IdenticalException;
import org.mysys.model.Service.ServiceCharges;
//import org.mysys.model.partner.AreaMst;
//import org.mysys.model.partner.PincodeMstVw;
//import org.mysys.model.partner.ServiceMst;
//import org.mysys.model.product.LeadSourceVw;
//import org.mysys.model.product.ProductMst;
//import org.mysys.model.product.ProductMstVw;
//import org.mysys.model.product.ProductPartMst;
//import org.mysys.model.product.ProductServiceMap;
//import org.mysys.model.product.ProductServiceVw;
import org.mysys.service.PartnerService;
//import org.mysys.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ProductController {
	
//	@Autowired
//	ProductService productService;
//	
	@Autowired
	private PartnerService psm;
	
//	@GetMapping("/product/getProductList")
//	@ResponseBody
//	public List<ProductMstVw> getProductList(){
//		return productService.getProductList();
//	}
	
//	@GetMapping("/product/getProductServiceList")
//	@ResponseBody
//	public List<ProductServiceVw> getProductServiceList(Long prdId){
//		return productService.getProductServiceList(prdId);
//	}

//	@GetMapping("/product/getLeadSource")
//	@ResponseBody
//	public List<LeadSourceVw> getLeadSource(){
//		return productService.getLeadSource();
//	}
	
	
//	@PostMapping("/product/saveProductServiceMap")
//	@ResponseBody
//	public ProductServiceMap saveProdServiceMap(@RequestBody ProductServiceMap prodServiceMap){
//		ProductServiceMap prodSerMap= null;
//		if(prodServiceMap.getPsmId() > 0) {
//			prodSerMap= productService.saveProdServiceMap(prodServiceMap);
//		} else {
//			List<ProductServiceMap> psm = productService.getProductServiceMapByProductAndService(prodServiceMap.getPrdId(),prodServiceMap.getSmId());
//		  if( psm.size() > 0) {
//			  throw new IdenticalException();
//		  }
//		  prodSerMap= productService.saveProdServiceMap(prodServiceMap);
//		}
//		return prodSerMap;
//	}

//	@GetMapping("/product/getProdServiceMap")
//	@ResponseBody
//	public ProductServiceMap getProdServiceMap(Long psmId){
//		return productService.getProdServiceMap(psmId);
//	}
//	
//	@PostMapping("/product/deleteProdServiceMap")
//	@ResponseBody
//	public String deleteProdServiceMap(long psmId){
//		return productService.deleteProdServiceMap(psmId);
//	}
//	
	
	
//	@PostMapping("/product/saveProductMst")
//	@ResponseBody
//	public ProductMst saveProdMst(@RequestBody ProductMst productMst){
//		ProductMst prodMst = null;
//		if(productMst.getPrdId() > 0) {
//			prodMst= productService.saveProdMst(productMst);
//		} else {
//			List<ProductMst> pm = productService.getProductMstByProdName(productMst.getProdName());
//		  if( pm.size() > 0) {
//			  throw new IdenticalException();
//		  }
//		  prodMst =	productService.saveProdMst(productMst);
//
//		}
//		return productService.saveProdMst(productMst);	
//	}
//	
//	@GetMapping("/product/getProductMst")
//	@ResponseBody
//	public ProductMst getProductMst(long prdId){
//		return productService.getProductMst(prdId);
//	}
//	
//	@PostMapping("/product/deleteProductMst")
//	@ResponseBody
//	public String deleteProductMst(long prdId){
//		return productService.deleteProductMst(prdId);
//	}
//	

//	@PostMapping("/product/saveServiceMst")
//	@ResponseBody
//	public ServiceMst saveServiceMst(@RequestBody ServiceMst servMst){
//		ServiceMst serviceMst= null;
//		if(servMst.getSmId() > 0) {
//			 serviceMst= psm.saveServiceMst(servMst);
//		} else {
//			List<ServiceMst> psms = psm.getListServiceMst(servMst.getServiceName());
//		  if(psms.size() > 0) {
//			  throw new IdenticalException();
//		  }
//			 serviceMst= psm.saveServiceMst(servMst);
//		}
//		return serviceMst;
//	}
//
//	@GetMapping("/product/getServiceMst")
//	@ResponseBody
//	public ServiceMst getServiceMst(Long smId){
//		return psm.getServiceMst(smId);
//	}
//	
//	@PostMapping("/product/deleteServiceMst")
//	@ResponseBody
//	public String deleteServiceMst(long smId){
//		return psm.deleteServiceMst(smId);
//	}
//	
//
//	@PostMapping("/product/saveAreaMst")
//	@ResponseBody
//	public AreaMst saveAreaMst(@RequestBody AreaMst areaMst){
//		AreaMst areaMsta= null;
//		if(areaMst.getAmId() > 0) {
//			areaMsta= psm.saveAreaMst(areaMst);
//		} else {
//			List<AreaMst> psms = psm.getListAreaMst(areaMst.getAreaName(),areaMst.getPincode());
//		  if(psms.size() > 0) {
//			  throw new IdenticalException();
//		  }
//		  areaMsta= psm.saveAreaMst(areaMst);
//		}
//		return areaMsta;
//	}
//
//	@GetMapping("/product/getAreaMst")
//	@ResponseBody
//	public AreaMst getAreaMst(Long amId){
//		return psm.getAreaMst(amId);
//	}
//	
//	@PostMapping("/product/deleteAreaMst")
//	@ResponseBody
//	public String deleteAreaMst(long amId){
//		return psm.deleteAreaMst(amId);
//	}
//
//	@GetMapping("/product/getPincode")
//	@ResponseBody
//	public List<PincodeMstVw> getPincode() {
//		return psm.getPincode();
//	}
//	
	
//	@PostMapping("/product/saveProdPartMst")
//	@ResponseBody
//	public ProductPartMst saveProdPartMst(@RequestBody ProductPartMst prodPartMst){
//		ProductPartMst productPartMst= null;
//		if(prodPartMst.getPpId() > 0) {
//			productPartMst= psm.saveProdPartMst(prodPartMst);
//		} else {
//			List<ProductPartMst> ppm = psm.getListProdPartMst(prodPartMst.getPpId());
//		  if(ppm.size() > 0) {
//			  throw new IdenticalException();
//		  }
//		  productPartMst= psm.saveProdPartMst(prodPartMst);
//		}
//		return productPartMst;
//	}
//	
//	@GetMapping("/product/getProdPartMst")
//	@ResponseBody
//	public ProductPartMst getProductPartMst(Long ppId){
//		
//		return psm.getProductPartMst(ppId);
//	}
//	
//	@PostMapping("/product/deleteProdPartMst")
//	@ResponseBody
//	public String deleteProdPartMst(long ppId){	
//		return psm.deleteProdPartMst(ppId);
//	}
//	
	@PostMapping("/product/saveServiceCharges")
	@ResponseBody
	public ServiceCharges saveServiceCharges(@RequestBody ServiceCharges servCharges){
		ServiceCharges serviceCharges= null;
		if(servCharges.getScId() > 0) {
			serviceCharges= psm.saveServiceCharges(servCharges);
		} else {
			List<ServiceCharges> psms = psm.getListServiceCharges(servCharges.getScId());
		  if(psms.size() > 0) {
			  throw new IdenticalException();
		  }
		  serviceCharges= psm.saveServiceCharges(servCharges);
		}
		return serviceCharges;
	}
	
	@GetMapping("/product/getServiceCharges")
	@ResponseBody
	public ServiceCharges getServiceCharges(Long scId){
		return psm.getServiceCharges(scId);
	}
	
	@PostMapping("/product/deleteServiceCharges")
	@ResponseBody
	public String deleteServiceCharges(long scId){
		return psm.deleteServiceCharges(scId);
	}
	

	
}
