package org.mysys.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mysys.model.Displist;
import org.mysys.service.ListingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@EnableSpringDataWebSupport
public class ListingController {

	private static final Logger LOGGER = LogManager.getLogger();

	@Autowired
	private ListingService listingService;

	@GetMapping("/List")
	@ResponseBody
	public Displist fetchListMetadata(long id) {
		LOGGER.info("Fetching List for {}", id);
		Displist dispList = listingService.loadMetadata(id).get();
		return dispList;
	}

	@GetMapping("/FetchData")
	@ResponseBody
	public Page<String[]> fetchData(Long id, Pageable page, String whereClause,String type) {
		LOGGER.info("Fetching data for {}", id);
		return listingService.getData(whereClause, id, page,type);
	}

}
