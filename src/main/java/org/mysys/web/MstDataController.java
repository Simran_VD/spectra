package org.mysys.web;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mysys.exception.IdenticalException;
import org.mysys.model.Citymaster;
import org.mysys.model.Department;
import org.mysys.model.Designation;
import org.mysys.model.Skill;
import org.mysys.model.Statemaster;
import org.mysys.model.job.PositionMst;
import org.mysys.model.job.StateMasterView;
import org.mysys.model.spectra.AuditTypeMst;
import org.mysys.model.spectra.FreqMst;
import org.mysys.model.spectra.OrgType;
import org.mysys.service.MstDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MstDataController {
	private static final Logger LOGGER = LogManager.getLogger();

	@Autowired
	private MstDataService mstDataService;

	
	
	/********** Start: Department Master **********/
	@PostMapping("/mstData/saveDept")
	@ResponseBody
	public Department saveDepartmentDtl(@RequestBody Department department){
		//return masterService.saveDetailDept(department);
		LOGGER.debug("Department (" + department.getDeptname() + ") is being saved.");
		Department res = null;
		if(department.getDeptid() > 0) {
			res = mstDataService.saveDepartment(department);
		} else {
			Department tempDept = mstDataService.getDepartment(department.getDeptname());
			if(tempDept != null) {
				throw new IdenticalException();
			}
			res = mstDataService.saveDepartment(department);
		}
		LOGGER.debug("Department (" + department.getDeptname() + ") is saved.");
		return res;
	}

	@PostMapping("/mstData/getDept")
	@ResponseBody
	public Department getDept(Long deptId) {
		Department res = mstDataService.findDepartment(deptId);
		return res;
	}

	@PostMapping("/mstData/deleteDept")
	@ResponseBody
	public String deleteDept(Long deptId) {
		return mstDataService.deleteDepartment(deptId);
	}	
	/********** End: Department Master **********/
	
	
	

	/********** Start: State Master **********/
	@PostMapping("/mstData/saveState")
	@ResponseBody
	public Statemaster saveState(@RequestBody Statemaster state) {
		LOGGER.debug("State (" + state.getStatename() + ") is being saved.");
		Statemaster res = null;
		if(state.getStateid() > 0) {	
			Integer sm = mstDataService.checkDuplicateStatemaster(state.getStatename(), state.getStateid());
			if(sm !=null ) {
					throw new IdenticalException();
			}
			res = mstDataService.saveState(state);
		} else {
			Integer sm = mstDataService.getStatemaster(state.getStatename());
			if(sm != null) {
				throw new IdenticalException();
			}
			res = mstDataService.saveState(state);
		}
		LOGGER.debug("State (" + state.getStatename() + ") is saved.");
		return res;
	}

	@PostMapping("/mstData/getState")
	@ResponseBody
	public Statemaster getState(long stateid) {
		Statemaster res = mstDataService.findState(stateid);
		return res;
	}

//	@GetMapping("/mstData/getStates")
//	@ResponseBody
//	public List<Statemaster> getStates() {
//		List<Statemaster> res = mstDataService.findStates();
//		return res;
//	}	
//
//	@PostMapping("/mstData/deleteState")
//	@ResponseBody
//	public String deleteState(long stateid) {
//		return mstDataService.deleteState(stateid);
//	}	
	/********** End: State Master **********/
	
	
	/********** Start: City Master **********/
	@PostMapping("/mstData/saveCitymaster")
	@ResponseBody
	public Citymaster saveCitymaster(@RequestBody Citymaster city) {
		LOGGER.debug("Citymaster (" + city.getCityName() + ") is being saved.");
		Citymaster res = null;
		if(city.getCityId() > 0) {
			Integer cm = mstDataService.checkDuplicateCitymaster(city.getCityName(), city.getCityId());
			if(cm !=null ) {
					throw new IdenticalException();
			}
			res = mstDataService.saveCitymaster(city);
		} else {
			Integer cm = mstDataService.getCitymaster(city.getCityName());
			if(cm != null) {
				throw new IdenticalException();
			}
			res = mstDataService.saveCitymaster(city);
		}
		LOGGER.debug("Citymaster (" + city.getCityName() + ") is saved.");
		return res;
	}

	@PostMapping("/mstData/getCitymaster")
	@ResponseBody
	public Citymaster getCitymaster(long cityId) {
		Citymaster res = mstDataService.findCitymaster(cityId);
		return res;
	}
	
	@PostMapping("/mstData/getLocMstByStateid")
	@ResponseBody
	public List<Citymaster> getCitymasterByStateid(long stateid) {
		List<Citymaster> res = mstDataService.findCitymasterByStateid(stateid);
		return res;
	}

//	@PostMapping("/mstData/deleteCity")
//	@ResponseBody
//	public String deleteCity(long cityid) {
//		return mstDataService.deleteCity(cityid);
//	}	
	/********** End: City Master **********/
	
	/********** Start: Designation Master **********/
	@PostMapping("/mstData/saveDesignation")
	@ResponseBody
	public Designation saveDesignation(@RequestBody Designation designation) {
		LOGGER.debug("Designation (" + designation.getDesignationid() + ") is being saved.");
		Designation res = null;
			if(designation.getDesignationid() > 0) {
				res = mstDataService.saveDesignation(designation);
			} else {
				Designation tempDesignation = mstDataService.getDesignationid(designation.getDesignationid());
				if(tempDesignation != null) {
					throw new IdenticalException();
				}
				res = mstDataService.saveDesignation(designation);
			}
			LOGGER.debug("Designation (" + designation.getDesignationid() + ") is saved.");
			return res;
		
	}

	@PostMapping("/mstData/getDesignationId")
	@ResponseBody
	public Designation getDesignationId(long designationid) {
		Designation res = mstDataService.getDesignationid(designationid);
		return res;
	}
	
	@PostMapping("/mstData/deleteDesignation")
	@ResponseBody
	public String deleteDesignation(Long designationid){
		return mstDataService.deleteDesignation(designationid);
		
	}
	/********** End: Designation Master **********/

	/********** Start: Skill Master **********/
	@PostMapping("/mstData/saveSkill")
	@ResponseBody
	public Skill saveSkill(@RequestBody Skill skill) {
		LOGGER.debug("Skill (" + skill.getId()+ ") is being saved.");
		Skill res = null;
		if(skill.getId() > 0) {
			res = mstDataService.saveSkill(skill);
		} else {
			Skill tempskill = mstDataService.getSkillId(skill.getId());
			if(tempskill != null) {
				throw new IdenticalException();
			}
			res = mstDataService.saveSkill(skill);
		}
		LOGGER.debug("Skill (" + skill.getId() + ") is saved.");
		return res;

	}

	@PostMapping("/mstData/getSkillId")
	@ResponseBody
	public Skill getSkillId(long id) {
		Skill res = mstDataService.getSkillId(id);
		return res;
	}

	@PostMapping("/mstData/deleteSkill")
	@ResponseBody
	public String deleteSkill(Long id){
		return mstDataService.deleteSkill(id);

	}
	/********** End: Skill Master **********/
	
	
	
	/**********  SPECTRA Master STARTS HERE MSTDATA CONTROLLER **********/
	
			/*************** Org Type MST Save and Edit*********/
	
	@PostMapping("/mstData/saveOrgType")
	@ResponseBody
	public OrgType saveOrgType(@RequestBody OrgType orgType) {
		LOGGER.info("URL hit : /mstData/saveOrgType, save data for Org Type");
		OrgType tempOrgType= null;
		if(orgType.getOtId() > 0) {		
			Integer ot = mstDataService.checkDuplicateOrgType(orgType.getOrgType(), orgType.getOtId());
			if(ot !=null ) {
				throw new IdenticalException();
			}
			
			tempOrgType = mstDataService.saveOrgType(orgType);
		}	else {
			Integer ot = mstDataService.getOrgType(orgType.getOrgType());
			if(ot !=null) {
				throw new IdenticalException();
			}
			tempOrgType = mstDataService.saveOrgType(orgType);
		}
		return tempOrgType;
	}
	
	
	@PostMapping("/mstData/getOrgType")
	@ResponseBody
	public OrgType getOrgType(long otId) {
		LOGGER.info("URL hit : /mstData/getOrgType, edit data for Org Type By otId");
		OrgType res = mstDataService.getOrgType(otId);
		return res;
	}
	
	/*************** Audit Type Mst Save and Edit*********/
	
	@PostMapping("/mstData/saveAuditTypeMst")
	@ResponseBody
	public AuditTypeMst saveAuditTypeMst(@RequestBody AuditTypeMst auditTypeMst) {
		LOGGER.info("URL hit : /mstData/saveAuditTypeMst, save data for Audit Type");
		AuditTypeMst tempAuditTypeMst = null;
		if(auditTypeMst.getAtmId() > 0) {		
			Integer atm = mstDataService.checkDuplicateAuditTypeMst(auditTypeMst.getAuditName(), auditTypeMst.getAtmId());
			if(atm !=null ) {
				throw new IdenticalException();
			}
			
			tempAuditTypeMst = mstDataService.saveAuditTypeMst(auditTypeMst);
		}	else {
			Integer atm = mstDataService.getAuditTypeMst(auditTypeMst.getAuditName());
			if(atm !=null) {
				throw new IdenticalException();
			}
			tempAuditTypeMst = mstDataService.saveAuditTypeMst(auditTypeMst);
		}
		
		return tempAuditTypeMst;
	}
	
	@PostMapping("/mstData/getAuditTypeMst")
	@ResponseBody
	public AuditTypeMst getAuditTypeMst(long atmId) {
		LOGGER.info("URL hit : /mstData/getAuditTypeMst, edit data for Audit Type By atmId");
		AuditTypeMst res = mstDataService.getAuditTypeMst(atmId);
		return res;
	}
	
	/**********************  State Master List Vw Here ***********************/
	
	@GetMapping("/mstData/getAllState")
	@ResponseBody
	public List<StateMasterView> getAllState()
	{
		return this.mstDataService.getAllState();
	}
	
	/*************** Freq Mst Save and Edit*********/
	
	@PostMapping("/mstData/saveFreqMst")
	@ResponseBody
	public FreqMst saveFreqMst(@RequestBody FreqMst freqMst) {
		LOGGER.info("URL hit : /mstData/saveFreqMst, save data for Freq Mst");
		FreqMst tempFreqMst = null;
		if(freqMst.getFmId() > 0){
		Integer fm	= mstDataService.checkDuplicateFreqMst(freqMst.getFreqName(), freqMst.getFmId());
		if(fm != null){
			throw new IdenticalException();
		}
		
		tempFreqMst = mstDataService.saveFreqMst(freqMst);
	} else {
		Integer fm = mstDataService.getFreqMst(freqMst.getFreqName());
		if(fm != null){
			throw new IdenticalException();
		}
		tempFreqMst = mstDataService.saveFreqMst(freqMst);
	}
		return tempFreqMst;
	}
	
	@PostMapping("/mstData/getFreqMst")
	@ResponseBody
	public FreqMst getFreqMst(long fmId) {
		LOGGER.info("URL hit : /mstData/getFreqMst, edit data for Freq Mst By fmId");
		FreqMst res = mstDataService.getFreqMst(fmId);
		return res;
	}
	
}
