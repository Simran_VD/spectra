package org.mysys.web;

import java.util.List;

import org.mysys.model.AreaWiseCommVw;
import org.mysys.model.AreaWiseLeadCntVw;
import org.mysys.model.DashboardKPIServiceVw;
import org.mysys.model.LeadStatusCntVw;
import org.mysys.model.job.MatchingApplListVw;
import org.mysys.repository.MatchingApplListVwRepo;
//import org.mysys.model.PartnerWiseCommVw;
//import org.mysys.model.PartnerWiseLeadCntVw;
import org.mysys.service.DashboardServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DashboardController {
	
	@Autowired
	DashboardServices dasboardServices;
	
	/*
	 * @GetMapping("/AreaWiseCommVw/getAreaWiseCommVw")
	 * 
	 * @ResponseBody public List<AreaWiseCommVw> getAreaWiseCommVw(){ return
	 * dasboardServices.getAreaWiseCommVw(); }
	 */
	
	@GetMapping("/AreaWiseLeadCntVw/getAreaWiseLeadCntVw")
	@ResponseBody
	public List<AreaWiseLeadCntVw> getAreaWiseLeadCntVw(){
		return dasboardServices.getAreaWiseLeadCntVw();
	}
	
//	@GetMapping("/LeadStatusCntVw/getLeadStatusCntVw")
//	@ResponseBody
//	public List<LeadStatusCntVw> getLeadStatusCntVw(){
//		return dasboardServices.getLeadStatusCntVw();
//	}
//	
//	@GetMapping("/PartnerWiseLeadCntVw/getPartnerWiseLeadCntVw")
//	@ResponseBody
//	public List<PartnerWiseLeadCntVw> getPartnerWiseLeadCntVw(){
//		return dasboardServices.getPartnerWiseLeadCntVw();
//	}
//	
//	@GetMapping("/PartnerWiseCommVw/getPartnerWiseCommVw")
//	@ResponseBody
//	public List<PartnerWiseCommVw> getPartnerWiseCommVw(){
//		return dasboardServices.getPartnerWiseCommVw();
//	}
//
	@GetMapping("/master/getDashBoardKPI")
	@ResponseBody
	public List<DashboardKPIServiceVw> getKpiServiceDetails(){
		return dasboardServices.getKpiServiceDetails();
	}

//	
//	@GetMapping("/ProductBrandMstVw/getProductBrandMstVw")
//	@ResponseBody
//	public List<ProductBrandMstVw>  getProductBrandMstVw(long prdId){
//		return dasboardServices.getProductBrandMstVw(prdId);
//	}

	
	
	@PostMapping("/master/getMatchingList")
	@ResponseBody
	public List<MatchingApplListVw> getAllMatchingAppl(Integer jmId){
		return dasboardServices.getAllMatchingAppl(jmId);
	}
	

}
