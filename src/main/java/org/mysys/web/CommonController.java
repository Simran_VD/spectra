package org.mysys.web;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.mysys.model.DashboardSetting;
import org.mysys.model.DbSetting;
import org.mysys.model.Mail;
import org.mysys.model.Module;
import org.mysys.model.SMS;
import org.mysys.model.TemplateList;
import org.mysys.model.UserSetting;
import org.mysys.service.AbstractService;
import org.mysys.service.CommonService;
import org.mysys.service.EmailService;
import org.mysys.service.MenuService;
import org.mysys.service.SMSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


@Controller
public class CommonController extends AbstractService {
	@Autowired
	MenuService repo;

	@Autowired
	CommonService commonService;

	@Autowired
	EmailService emailService;
	
	@Autowired
	SMSService smsSerivce;

	@GetMapping("/GetMenu")
	@ResponseBody
	public List<Module> getMenu(){
		Object secPrincipal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String loginId;
		if(secPrincipal instanceof UserDetails){
			loginId = ((UserDetails)secPrincipal).getUsername();
			return repo.getMenu(loginId);
		}
		return null;
	}
	
	@GetMapping("/pages")
	public String renderAddUser(String name,Integer tabIndex) {
		tabIndex = 0;
		return name;
	}

	@GetMapping("/getStaticValues")
	@ResponseBody
	public Map<String,String> getStaticValues(String keys){
		List<String> keyList = Arrays.asList(keys.split(","));
		return commonService.getStaticValues(keyList);
	}

	@GetMapping("/getProperty")
	@ResponseBody
	public String getProperty(String key) {
		DbSetting dbSetting = commonService.getSetting(key);
		return (dbSetting != null && dbSetting.getValue() != null) ? dbSetting.getValue() : "";
	}

	public String index() {
		return "upload";
	}

	@PostMapping("/sendEmail")
	@ResponseBody
	public String upload(@RequestParam(value="file", required=false) List < MultipartFile > files,
			@RequestParam(value="mailTo", required=false) String mailTo, 
			@RequestParam(value="mailBcc", required=false) String mailBcc, 
			@RequestParam(value="mailCc", required=false) String mailCc,
			@RequestParam(value="subject", required=false) String subject, 
			@RequestParam(value="message", required=false) String message) {

		Mail mail = new Mail(mailTo, mailBcc, mailCc, subject, message, files);
		return emailService.sendEmail(mail);
	}
	
	@GetMapping("/sendSMS")
	@ResponseBody
	public int sendSMS(@RequestBody SMS sms) {
		return smsSerivce.sendSms(sms);
	}
	
	
	@PostMapping("/getTemplates")
	@ResponseBody
	public List<TemplateList> getTemplates(long templateCategory, String templateGroup) {
		return tempateListRepo.getTemplates(templateCategory, templateGroup, getLoggedInUserSiteId());
	}
	
	@PostMapping("/getTemplate")
	@ResponseBody
	public TemplateList getTemplate(Long tlId ) {
		return tempateListRepo.getTemplate(tlId);
	}
	
	@PostMapping("/FirstTimeChangePassword")
	@ResponseBody
	public String firstTimeChangePassword() {
		return commonService.firstTimeChangePassword();
	}
	
	@PostMapping("/changePassword")
	@ResponseBody
	public String changePassword(@RequestParam String pswd,@RequestParam String oldpswd) {
		return commonService.changePassword(pswd, oldpswd);
	}
	
	@PostMapping("/changePasswordWAC")
	@ResponseBody
	public String changePasswordWAC() {
		return commonService.changePasswordWAC();
	}
	
	
	@PostMapping("/getUserName")
	@ResponseBody
	public String getUserName() {
		return getLoggedInUserLoinId();
	}
	
	@PostMapping("/getUserSetting")
	@ResponseBody
	public List<UserSetting> getValu(String key) {
		return userService.getUserSetting();
	}
	
	@PostMapping("/saveUserSetting")
	@ResponseBody
	public UserSetting saveUserSetting(String key, String value) {
		return userService.saveUserSetting(key, value);
	}	
	
	@GetMapping("/getDashboardSetting")
	@ResponseBody
	public List<DashboardSetting> getDashboardSetting() {
		return this.commonService.getDashboardSetting();
	}
	
}
