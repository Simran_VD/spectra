package org.mysys.web;

import java.util.List;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.mysys.exception.IdenticalException;
import org.mysys.model.SMS;
import org.mysys.model.User;
import org.mysys.model.Service.ServiceLead1;
import org.mysys.model.Service.ServiceLeadVw;
//import org.mysys.model.customer.Suggestion;
import org.mysys.model.notification.Note;
import org.mysys.service.CommonService;
import org.mysys.service.ContactService;
import org.mysys.service.CustomerService;
import org.mysys.service.FirebaseMessagingService;
import org.mysys.service.MasterService;
import org.mysys.service.MstDataService;
import org.mysys.service.PartnerService;
//import org.mysys.model.partner.ServiceMstVw;
import org.mysys.service.SMSService;
import org.mysys.utility.OTPUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.messaging.FirebaseMessagingException;

@RestController
public class HomeController {


	@Autowired
	private Environment env;

	@Autowired
	protected FirebaseMessagingService db;

	@Autowired
	protected ContactService contactService;

	@Autowired
	protected MasterService masterService;

	@Autowired
	protected PartnerService partnerService;

	@Autowired
	protected CustomerService customerService;

//	@Autowired
//	protected ProductService prodService;	

	@Autowired
	protected MstDataService mstDataService;

//	@Autowired
//	ProductService productService;

	@Autowired
	protected SMSService smsService;

	@Autowired
	protected CommonService commonService;

	/*************************** Website ***/

	@GetMapping("/website/allCity")
	public boolean getAllCity() {
		boolean result =false;
		ObjectMapper obj= new ObjectMapper();
		try {
			String folderName = env.getProperty("application.temp.folder");
			Path directory=Files.createDirectories(Paths.get(folderName));

			File f = new File(directory+"/citymaster.json");
			obj.writeValue(f, masterService.getAllCity());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true; 
	}

//	@GetMapping("/website/getServices")
//	public boolean getServices() {
//		boolean result =false;
//		ObjectMapper obj= new ObjectMapper();
//		try {
//			String folderName = env.getProperty("application.temp.folder");
//			Path directory=Files.createDirectories(Paths.get(folderName));
//
//			File f = new File(directory+"/service.json");
//			obj.writeValue(f,  partnerService.getpProdServices());
//			result=true;
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return result; 
//	}

//	@GetMapping("/website/getAllServices")
//	public List<ServiceMstVw> getallServices() {
//		return partnerService.getpProdServices(); 
//	}
//	
//	@PostMapping("/website/suggestion")
//	public Suggestion saveSuggetion(@RequestBody Suggestion suugestion) {
//		return masterService.saveSuggestion(suugestion); 
//	}


//	@PostMapping("/website/saveCustomer")
//	public ServiceLead1 saveCustomer(@RequestBody ServiceLead1 service) throws FirebaseMessagingException{
//		ServiceLead1 servLead= null;
//		long id=service.getSlId();
//		 //if(customerService.validateOTP(service.getMobile(),service.getOtp())) {
//			 servLead =customerService.saveCustomer(service);
//			 if(id <=0) {
//					Map<String,String> data= new HashMap<>();
//					ServiceLeadVw slVw=customerService.getServleadVw(servLead.getSlId());
//					if(slVw !=null) {
//						data.put("address", slVw.getAddress());
//						data.put("custName", slVw.getDisplayName());
//						data.put("serviceReqTime", String.valueOf(slVw.getServReqdAtTime()));
//						data.put("mobile", slVw.getMobile());
//						data.put("city", slVw.getCity());
//						data.put("slId", String.valueOf(slVw.getSlId()));
//						data.put("area", slVw.getAreaName());
//						if(customerService.getCorporateToken() !=null)
//						db.sendMultiNotification(new Note("New Lead","Blank",data,"", ""),Arrays.asList("eXTIXe4TRqinX89pD1zhJw:APA91bGug1UlvYY4WSTHE5qLEqZJ7UqmbXu5TlmCrpPkotJpaXhX5nMkxEqXpnV9cyAVTHO5x_QmdvgHpcPFQ-xacrZ9H5qZ67ty74CjtvNMObZdMc4DIIIEN5DBc-Z0j7EdxYcdJDH9"));
//					 }
//					}
//		// }
//		return servLead;
//	}

//	@GetMapping("/website/getOurServices")
//	public boolean getOurServices(){
//		boolean result =false;
//		ObjectMapper obj= new ObjectMapper();
//		try {
//			String folderName = env.getProperty("application.temp.folder");
//			Path directory=Files.createDirectories(Paths.get(folderName));
//
//			File f = new File(directory+"/ourservices.json");
//			obj.writeValue(f,  prodService.getAllServices());
//			result=true;
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return result; 		
//	}


	/*
	 * @PostMapping("/website/saveEmp") public Emp saveEmp(@RequestBody Emp emp){
	 * CollectionReference si = db.getFirestore().collection("emp");
	 * si.document(String.valueOf(emp.getId())).set(emp); return emp;
	 * 
	 * }
	 */


	@PostMapping("/send-notification")
	public String sendNotification(@RequestBody Note note) throws FirebaseMessagingException {
		return db.sendNotification(note);
	}
//
//	@GetMapping("/website/getStates")
//	public boolean getStates() {
//		boolean result =false;
//		ObjectMapper obj= new ObjectMapper();
//		try {
//			String folderName = env.getProperty("application.temp.folder");
//			Path directory=Files.createDirectories(Paths.get(folderName));
//
//			File f = new File(directory+"/state.json");
//			obj.writeValue(f,  mstDataService.findStates());
//			result=true;
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return result; 		
//	}	

//	@GetMapping("/website/getProductList")
//	public boolean getProductList(){
//		boolean result =false;
//		ObjectMapper obj= new ObjectMapper();
//		try {
//			String folderName = env.getProperty("application.temp.folder");
//			Path directory=Files.createDirectories(Paths.get(folderName));
//
//			File f = new File(directory+"/product.json");
//			obj.writeValue(f,  productService.getProductList());
//			result=true;
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return result; 		
//	}

//	@GetMapping("/website/getProductServiceList")
//	public boolean getProductServiceList(){
//		boolean result =false;
//		ObjectMapper obj= new ObjectMapper();
//		try {
//			String folderName = env.getProperty("application.temp.folder");
//			Path directory=Files.createDirectories(Paths.get(folderName));
//
//			File f = new File(directory+"/productservice.json");
//			obj.writeValue(f,  productService.getProductServiceList());
//			result=true;
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return result; 		
//	}
	/********************* ---------------------***********************/
	/*
	 * @GetMapping("/website/sendOTP") public boolean sendOTP(String mobile){ Double
	 * f= Math.random(); boolean result=false; String
	 * ss=f.toString().substring(f.toString().length()-6, f.toString().length());
	 * int i= smsService.sendOTP(mobile, ss); if(i> 0) { long sd=
	 * customerService.saveOTP(mobile,ss); if(sd > 0) { result=true; } } return
	 * result; }
	 */
	

	/*
	 * @GetMapping("/website/varifyOtp") public boolean sendOTP(String mobile,String
	 * opt){ return customerService.validateOTP(mobile,opt); }
	 */
	
	
	@PostMapping("/pages/sendForgotOTP")
	@ResponseBody
	public OTPUtility sendForgotOTP(String mobile) {
		
		OTPUtility res = new OTPUtility();
		
		List<User> contact = commonService.getDuplicateContact(mobile);
		if(!(contact != null && contact.size() > 0)) {
			throw new IdenticalException();
		}
		else {
			Double f = Math.random();
			String ss = f.toString().substring(f.toString().length() - 6, f.toString().length());
			
			String msg = "";
			
			// custom OTP msg
				msg = "Dear user, "+ss+" is your OTP for recruiter.com . Team RARR";
				int i = smsService.sendCustomOTP(mobile, msg);
			  if(i> 0) { 
				  long sd= commonService.saveOTP(mobile,ss); 
				  if(sd > 0) { 
					  res.setResult(true);
					  res.setUserid(contact.get(0).getUserid());
				  } 
				  }
		}
		return res;
	}
	@GetMapping("/pages/varifyOtp")
	@ResponseBody
	public boolean sendOTP(String mobile, String opt) {
		return commonService.validateOTP(mobile,opt);
	}
	
	 @GetMapping("/pages/updatePwd")
	 @ResponseBody
	  public boolean updatePwd(Long userId, String newPwd){
		  return commonService.updatePwd(userId, newPwd);
		  }
}
