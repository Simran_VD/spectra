
package org.mysys.web;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mysys.model.Service.ServiceLead;
import org.mysys.model.Service.ServiceLeadVw;
//import org.mysys.model.customer.CustMst;
//import org.mysys.model.customer.CustMstVw;
import org.mysys.model.notification.Note;
//import org.mysys.model.partner.PartnerAcceptanceLadger;
import org.mysys.service.CustomerService;
import org.mysys.service.FirebaseMessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.firebase.messaging.FirebaseMessagingException;

@Controller
public class CustomerController {
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	protected FirebaseMessagingService fcm;

//	@PostMapping("/customer/saveCustomer")
//	@ResponseBody
//	public CustMst saveCustomer(@RequestBody CustMst custMst){
//		return customerService.saveCustMst(custMst);
//	}
//
//	@GetMapping("/customer/getCustomer")
//	@ResponseBody
//	public CustMst getCustomer(Long csId){
//		return customerService.getCustomer(csId);
//	}
//	
//	@PostMapping("/customer/deleteCustomer")
//	@ResponseBody
//	public String deleteCustomer(long pmId){
//		return customerService.deleteCustomer(pmId);
//	}
//	
//	@PostMapping("/customer/getCustomers")
//	@ResponseBody
//	public List<CustMstVw> getCustomers(){
//		return customerService.getCustmors();
//	}
	/********************************* Service lead Block 
	 * @throws FirebaseMessagingException ********************************/
	
//	@PostMapping("/customer/saveServiceLead")
//	@ResponseBody
//	public ServiceLead saveServiceLead(@RequestBody ServiceLead serviceLead) throws FirebaseMessagingException{
//		long id =serviceLead.getSlId();
//		ServiceLead servLead= customerService.saveServiceLead(serviceLead);
//		if(id <=0) {
//		Map<String,String> data= new HashMap<>();
//		ServiceLeadVw slVw=customerService.getServleadVw(servLead.getSlId());
//		if(slVw !=null) {
//			data.put("address", slVw.getAddress());
//			data.put("custName", slVw.getDisplayName());
//			data.put("serviceReqTime", slVw.getServReqdAtTime().toString());
//			data.put("mobile", slVw.getMobile());
//			data.put("city", slVw.getCity());
//			data.put("slId", String.valueOf(slVw.getSlId()));
//			data.put("area", slVw.getAreaName());
//			if(customerService.getCorporateToken() !=null)
//			fcm.sendMultiNotification(new Note("New Lead","Blank",data,"", ""),Arrays.asList("eXTIXe4TRqinX89pD1zhJw:APA91bGug1UlvYY4WSTHE5qLEqZJ7UqmbXu5TlmCrpPkotJpaXhX5nMkxEqXpnV9cyAVTHO5x_QmdvgHpcPFQ-xacrZ9H5qZ67ty74CjtvNMObZdMc4DIIIEN5DBc-Z0j7EdxYcdJDH9"));
//		 }
//		}
//		return servLead;
//	}

	@GetMapping("/customer/getServiceLead")
	@ResponseBody
	public ServiceLead getServiceLead(Long slId){
		return customerService.getServLead(slId);
	}
	
	@PostMapping("/customer/deleteServLead")
	@ResponseBody
	public String deleteServLead(long slid){
		return customerService.deleteServLead(slid);
	}
	
//	@PostMapping("/customer/updatePartnerInServlead")
//	@ResponseBody
//	public PartnerAcceptanceLadger updatePartnerInServlead(long slId,String loginId){
//		return customerService.updatePmId(slId,loginId);
//	}
}
