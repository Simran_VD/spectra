package org.mysys.web;


import org.mysys.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PurchaseOrderController {
	@Autowired
	private PurchaseOrderService poService;
	
	@Autowired
	private Environment env;
	
	@GetMapping("/contact/getAddress")
	@ResponseBody
	public String getContactAddress(Long id){
		return poService.getContactAddress(id);
	}
	
	@GetMapping("/contact/getStateID")
	@ResponseBody
	public String getContactStateID(Long id){
		return poService.getContactStateID(id);
	}
}
