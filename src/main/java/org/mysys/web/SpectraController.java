package org.mysys.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mysys.model.spectra.OrgMaster;
import org.mysys.service.SpectraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

public class SpectraController {

	private static final Logger LOGGER = LogManager.getLogger(SpectraController.class);
	
	@Autowired
	private SpectraService spectraService;
	
	@PostMapping("/saveOrgMaster")
	@ResponseBody
	public OrgMaster saveOrgMaster(@RequestBody OrgMaster master) {
		LOGGER.info("URL hit : /saveOrgMaster, save data for org master");
		return spectraService.saveOrgMaster(master);
	}
	
}
