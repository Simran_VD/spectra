package org.mysys.web;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mysys.exception.IdenticalException;
import org.mysys.model.Citymaster;
import org.mysys.model.Contact;
import org.mysys.model.ContactVo;
import org.mysys.model.Contactcateg;
import org.mysys.model.Contacttype;
import org.mysys.model.DbSetting;
import org.mysys.model.DropdownVO;
import org.mysys.model.Mail;
import org.mysys.model.Service.ServiceLeadVw;
import org.mysys.model.job.ClientMst;
import org.mysys.model.job.CompanyMst;
import org.mysys.model.job.CompanyMstVw;
import org.mysys.model.job.CourseMst;
import org.mysys.model.job.DomainDropdownVw;
import org.mysys.model.job.DomainMst;
import org.mysys.model.job.InstituteMst;
import org.mysys.model.job.MatchWeightCfg;
import org.mysys.model.job.PositionMst;
import org.mysys.model.job.PositionMstVw;
import org.mysys.model.job.RecuGrpMst;
import org.mysys.model.job.SpocMst;
import org.mysys.model.job.SpocMstListVw;
import org.mysys.model.job.UserTypeMst;
import org.mysys.model.job.UserTypeMstListVw;
import org.mysys.model.notification.Name;
import org.mysys.model.notification.NotifyListVw;
//import org.mysys.model.notification.NotifyListVw;
import org.mysys.model.notification.ServiceLeadNotifyVw;
import org.mysys.model.notification.UserResponse;
//import org.mysys.model.partner.LeadSourceMstVw;
import org.mysys.service.AbstractService;
import org.mysys.service.CommonService;
import org.mysys.service.ContactService;
import org.mysys.service.EmailService;
import org.mysys.service.MasterService;
import org.mysys.service.PartnerService;
import org.mysys.utility.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class MasterController extends AbstractService {
	private static final Logger LOGGER = LogManager.getLogger();

	@Autowired
	private MasterService masterService;
	
	@Autowired
	private ContactService contactService;

	@Autowired
	private CommonService commonService;
	
	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	protected EmailService emailService;

	@GetMapping("/getDbSetting")
	@ResponseBody
	public List<DbSetting> getDbSetting(){
		return commonService.getDbSetting();
	}

	@GetMapping("/ListCityMaster")
	@ResponseBody
	public Page<Citymaster> fetchListMetadata(int pageNo, int pageSize) {
		return masterService.findPages(pageNo,pageSize); 
	}
	

//	@GetMapping("/GetCitiesOfState")
//	@ResponseBody
//	public List<DropdownVO> getCitiesOfState(long stateId) {
//		return masterService.findCityByState(stateId).stream().map((a)->{return new DropdownVO(String.valueOf(a.getCityid()),a.getCityname());}).collect(Collectors.toList()); 
//	}
//	
//	@GetMapping("/GetCities")
//	@ResponseBody
//	public List<DropdownVO> getCitiesOfState() {
//		return masterService.findCity().stream().map((a)->{return new DropdownVO(String.valueOf(a.getCityid()),a.getCityname());}).collect(Collectors.toList()); 
//	}

	@GetMapping("/GetDropdownData")
	@ResponseBody
	public Map<String,List<DropdownVO>> getDropdownData(){
		return masterService.getDropDownData();
	}

	@GetMapping("/GetContactDropdownData")
	@ResponseBody
	public Map<String,List<DropdownVO>> getContactDropdownData(){
		return contactService.getDropDownData();
	}

	@PostMapping("/master/saveContactCateg")
	@ResponseBody
	public Contactcateg saveContactCateg(String categoryname){
		return contactService.saveContactCateg(categoryname);
	}

	@PostMapping("/master/isCotegoryExist")
	@ResponseBody
	public boolean isCategoryExist(String categoryName ){
		boolean flag= false;
		List<Contactcateg> cnCatage=contactService.isContactCategExist(categoryName);
		if(cnCatage !=null && cnCatage.size() > 0) {flag=true;}
		return flag;
	}

	/*
	 * @PostMapping("/master/isTypeExist")
	 * 
	 * @ResponseBody public boolean isTypeExist(String contacttypename ){ boolean
	 * flag= false; List<Contacttype>
	 * cnCatage=contactService.isContactTypeExist(contacttypename); if(cnCatage
	 * !=null && cnCatage.size() > 0) {flag=true;} return flag; }
	 */

	@PostMapping("/master/saveContactType")
	@ResponseBody
	public Contacttype saveContactType(String contactTypeName){
		return contactService.saveContactType(contactTypeName);
	}

	@PostMapping("/master/saveContact")
	@ResponseBody
	public Contact saveContact(@RequestBody Contact contact){
		return contactService.saveContact(contact);
	}

	@PostMapping("/master/getContact")
	@ResponseBody
	public Contact getContact(Long contactId){
		return contactService.getContact(contactId);
	}

	@GetMapping("/master/deleteContact")
	@ResponseBody
	public String deleteContact(Long contactId){
		return contactService.deleteContact(contactId);
	}

	@GetMapping("/master/getBOMNumber")
	@ResponseBody
	public String getBOMPage(){
		return String.format("%d", new Date().getTime());
	}

	@GetMapping("/master/getParties")
	@ResponseBody
	public Map<String,List<DropdownVO>> getParties(){
		return masterService.getParties();
	}

	@GetMapping("/master/getContacts")
	@ResponseBody
	public List<ContactVo> getContacts() {
		long contactroleid = 3;	
		return masterService.getContacts(contactroleid);
	}

	@GetMapping("/master/getSuppliers")
	@ResponseBody
	public List<ContactVo> getSuppliers() {
		long contactroleid = 1;				//TODO Need to remove hard code implementation (1 - Supplier, 2 - Vendor, 3 - Customer)
		return masterService.getContacts(contactroleid);
	}

	@GetMapping("/master/getVendors")
	@ResponseBody
	public List<ContactVo> getVendors() {
		long contactroleid = 2;				//TODO Need to remove hard code implementation (1 - Supplier, 2 - Vendor, 3 - Customer)
		return masterService.getContacts(contactroleid);
	}

	@GetMapping("/master/getCustomer")
	@ResponseBody
	public List<ContactVo> getCustomer() {
		long contactroleid = 3;			//TODO Need to remove hard code implementation (1 - Supplier, 2 - Vendor, 3 - Customer)
		return masterService.getContacts(contactroleid);
	}

	@GetMapping("/master/getItemAndParty")
	@ResponseBody
	public Map<String,List<DropdownVO>> getItemAndParty(){
		return masterService.getItemAndParty();
	}


	/********* Start: Opening Stock ************/
	@GetMapping("/master/getDepartments")
	@ResponseBody
	public List<DropdownVO> getDepartments(){
		return masterService.getDepartments();
	}


	@GetMapping("/master/getAllDistinctVendorList")
	@ResponseBody
	public List<DropdownVO> getAllDistinctVendorList() {
		LOGGER.debug("In getAllDistinctVendorList.....");
		return masterService.getAllDistinctVendorList();
	}



	@PostMapping("/master/saveDepartment")
	@ResponseBody
	public long saveDepartment(String deptName){
		return masterService.saveDepartment(deptName);
	}

	@PostMapping("/master/getLoggedInSiteid")
	@ResponseBody
	public Long getLoggedIsur() {
		return getLoggedInUserSiteId();
	}
	
	
//	@GetMapping("/master/getLeadStatus")
//	@ResponseBody
//	public List<LeadSourceMstVw> getLeadStatus() {
//		return masterService.getLeadSource();
//	}
//	
	/*
	 * @GetMapping("/master/getNotification")
	 * 
	 * @ResponseBody public List<NotifyListVw> getNotification() { return
	 * masterService.getNotification(); }
	 */
	
	
	@GetMapping("/master/getServiceLeadNotify")
	@ResponseBody
	public List<ServiceLeadNotifyVw> getServiceLeadNotify() {
		return masterService.getServiceLeadNotify();
	}
	
	@PostMapping("/master/notifyAt")
	@ResponseBody
	public String notifyAt(String ids) {
		return masterService.notifyAt(ids);
	}
	
	@PostMapping("/master/bellAt")
	@ResponseBody
	public String bellAt(String ids) {
		return masterService.bellAt(ids);
	}
	
	@PostMapping("/master/seenAt")
	@ResponseBody
	public int seenAt(Long ids) {
		return masterService.seenAt(ids);
	}

	 @Autowired
	    SimpMessagingTemplate template;
	
	@MessageMapping("/user")
    //@SendTo("/topic/user/")
    public UserResponse getUser(Name user) throws Exception {
      template.convertAndSendToUser("deepak", "/user/topic/user",new UserResponse("Hi " + user.getName()) );
       return new UserResponse("Hi " + user.getName());
    }
	
	@GetMapping("/master/getAllNotification")
	@ResponseBody
	public List<NotifyListVw> getAllNotification() {
		return masterService.getAllNotification();
	}
	
	
	/*
	 * @GetMapping("/master/getDashBoardKPI")
	 * 
	 * @ResponseBody public List<DashboardKPIServiceVw> getDashBoardKPI() { return
	 * masterService.getDashBoardKPI(); }
	 */
	
	@GetMapping("/master/getDrilDown")
	@ResponseBody
	public List<ServiceLeadVw> getDrilDown(Long amId, Long tmId) {
		return masterService.getDrilDown(amId,tmId);
	}
	
	@GetMapping("/master/getDrilDownPartnerWise")
	@ResponseBody
	public List<ServiceLeadVw> getDrilDownPartnerWise(Long pmId, Long tmId) {
		return masterService.getDrilDown1(pmId,tmId);
	}
	
	@GetMapping("/master/getDrilDownLeadWise")
	@ResponseBody
	public List<ServiceLeadVw> getDrilDownLeadWise(Long statusId, Long tmId) {
		return masterService.getDrilDown2(statusId,tmId);
	}
	
	@PostMapping("/master/sendEmailInvoice")
	@ResponseBody
	public String sendSaleInvEmail(@RequestParam("file") List <MultipartFile> files,
			@RequestParam String mailTo, @RequestParam String mailBcc, @RequestParam String mailCc,
			@RequestParam String subject, @RequestParam String message, @RequestParam String data) {

		String rptUrl = env.getProperty("application.reporting_url") + "/masters/SaleInv_New.rptdesign&__format=pdf&rp_pi_id="+data+"&rp_userid="+getLoggedInUserUserId();
		String fileName = env.getProperty("application.temp.folder") + "/SalesInvoice_" + System.currentTimeMillis() + ".pdf";

		files = CommonUtils.addReport(files, rptUrl, fileName);

		Mail mail = new Mail(mailTo, mailBcc, mailCc, subject, message, files);
		return emailService.sendEmail(mail);
	}
	
	@PostMapping("/master/sendUserEmail")
	@ResponseBody
	public String sendUserEmail(@RequestParam String mailTo, @RequestParam long userId) {

		  String message ="Welcome to BetterBuddys.\nYou have been Registered as our Partner\r\n" +
				  "Your Login Credentials are- \r\n" + "Username:-"+userRepo.findByUseridAndSiteid(userId,0l).getLoginid()+
		  "\nPassword:-" + userRepo.findByUseridAndSiteid(userId,0l).getPassword()+
		  "\nFor any support or query please contact 91 8586977307";
		 
		Mail mail = new Mail(mailTo, "", "", "Welcome to BetterBuddys", message, Collections.emptyList());
		return emailService.sendEmail(mail);
	}
	
	/***********************  Recruiter Form Starts Here   *******************************/
					/************User Type Master****************/
					 /*******User Type Master Saveing**********/
	@PostMapping("/master/saveUserTypeMst")
	@ResponseBody
	public UserTypeMst saveUserTypeMst(@RequestBody UserTypeMst userTypeMst)
	{
		UserTypeMst tempUserTypeMst = null;
		if(userTypeMst.getTypeId() > 0) {
			Integer cm = masterService.checkDuplicateUserTypeMst(userTypeMst.getUserType(), userTypeMst.getTypeId());
			if(cm !=null ) {
				throw new IdenticalException();
			}
			
			tempUserTypeMst = this.masterService.saveUserTypeMst(userTypeMst);
		} else {
			Integer utm = masterService.getUserTypeMst(userTypeMst.getUserType());
			if(utm !=null) {
				throw new IdenticalException();
			}
			tempUserTypeMst = this.masterService.saveUserTypeMst(userTypeMst);
		}
		return tempUserTypeMst;
	}
	
	/*******User Type Master Get IN Edit View**********/
	
	@PostMapping("/master/getUserTypeMst")
	@ResponseBody
	public UserTypeMst getUserTypeMst(Long typeId) {
		UserTypeMst getUserTypeMst = this.masterService.getUserTypeMst(typeId);
		return getUserTypeMst;
	}
	
	/*******User Type Master List View**********/
	@GetMapping("/master/getUserTypeMstListVw")
	@ResponseBody
	public List<UserTypeMstListVw> getUserTypeMstListVw() {
		return masterService.getUserTypeMstListVw();
	}
	
			/************Recruiter Group Master****************/
			 /*******Recruiter Group Master Saveing**********/
	@PostMapping("/master/saveRecuGrpMst")
	@ResponseBody
	public RecuGrpMst saveRecuGrpMst(@RequestBody RecuGrpMst recuGrpMst)
	{
		RecuGrpMst tempRecuGrpMst = null;
		if(recuGrpMst.getRgmId() > 0) {
			Integer cm = masterService.checkDuplicateRecuGrpMst(recuGrpMst.getRecuGrpName(), recuGrpMst.getRgmId());
			if(cm !=null ) {
				throw new IdenticalException();
			}
			
			tempRecuGrpMst = this.masterService.saveRecuGrpMst(recuGrpMst);
		} else {
			Integer rgm = masterService.getRecuGrpMst(recuGrpMst.getRecuGrpName());
			if(rgm !=null) {
				throw new IdenticalException();
			}
			tempRecuGrpMst = this.masterService.saveRecuGrpMst(recuGrpMst);
		}
		return tempRecuGrpMst;
	}

			/*******Recruiter Group Master Get IN Edit View**********/

	@PostMapping("/master/getRecuGrpMst")
	@ResponseBody
	public RecuGrpMst getRecuGrpMst(Long rgmId) {
		RecuGrpMst getRecuGrpMst = this.masterService.getRecuGrpMst(rgmId);
		return getRecuGrpMst;
	}
	
	/************Company Master****************/
	 /*******Company Master Saveing**********/
	@PostMapping("/master/saveCompanyMst")
	@ResponseBody
	public CompanyMst saveCompanyMst(@RequestBody CompanyMst companyMst)
	{
		CompanyMst tempCompanyMst = null;
	if(companyMst.getCmId() > 0) {	
		Integer cm = masterService.checkDuplicateCompanyMst(companyMst.getCompanyName(), companyMst.getCmId());
		if(cm !=null ) {
			throw new IdenticalException();
		}
		tempCompanyMst = this.masterService.saveCompanyMst(companyMst);
	} else {
		Integer cm = masterService.getCompanyMst(companyMst.getCompanyName());
		if(cm !=null) {
			throw new IdenticalException();
		}
		tempCompanyMst = this.masterService.saveCompanyMst(companyMst);
	}
	return tempCompanyMst;
	}
	
		/*******Company Master Get IN Edit View**********/
	
	@PostMapping("/master/getCompanyMst")
	@ResponseBody
	public CompanyMst getCompanyMst(Long cmId) {
		CompanyMst getCompanyMst = this.masterService.getCompanyMst(cmId);
	return getCompanyMst;
	}
	
	/************Client Master****************/
	 /*******Client Master Saveing**********/
	@PostMapping("/master/saveClientMst")
	@ResponseBody
	public ClientMst saveClientMst(@RequestBody ClientMst clientMst)
	{
		ClientMst tempClientMst = null;
	if(clientMst.getClId() > 0) {
		Integer cm = masterService.checkDuplicateClientMst(clientMst.getCompanyName(), clientMst.getClId());
		if(cm !=null ) {
			throw new IdenticalException();
		}
		
		tempClientMst = this.masterService.saveClientMst(clientMst);
	} else {
		Integer cm = masterService.getClientMst(clientMst.getCompanyName());
		if(cm !=null) {
			throw new IdenticalException();
		}
		tempClientMst = this.masterService.saveClientMst(clientMst);
	}
	return tempClientMst;
	}
	
		/*******Client Master Get IN Edit View**********/
	
	@PostMapping("/master/getClientMst")
	@ResponseBody
	public ClientMst getClientMst(Long clId) {
		ClientMst getClientMst = this.masterService.getClientMst(clId);
		return getClientMst;
	}
	
	/*******Domain Dropdown Vw Get IN List View**********/	
	@GetMapping("/master/getDomainDropdownVw")
	@ResponseBody
	public List<DomainDropdownVw> getDomainDropdownVw() {
		return masterService.getDomainDropdownVw();
	}
	
	/************Match Weight Cfg ****************/
	 /*******Match Weight Cfg Saveing**********/
	@PostMapping("/master/saveMatchWeightCfg")
	@ResponseBody
	public MatchWeightCfg saveMatchWeightCfg(@RequestBody MatchWeightCfg matchWeightCfg)
	{
		MatchWeightCfg tempMatchWeightCfg = null;
	if(matchWeightCfg.getParamId() > 0) {	
		Integer mw = masterService.checkDuplicateMatchWeightCfg(matchWeightCfg.getParamName(), matchWeightCfg.getParamId());
		if(mw !=null ) {
			throw new IdenticalException();
		}
		
		tempMatchWeightCfg = this.masterService.saveMatchWeightCfg(matchWeightCfg);
	} else {
		Integer mw = masterService.getMatchWeightCfg(matchWeightCfg.getParamName());
		if(mw !=null ) {
			throw new IdenticalException();
		}
		tempMatchWeightCfg = this.masterService.saveMatchWeightCfg(matchWeightCfg);
	}
	return tempMatchWeightCfg;
	}
	
		/*******Match Weight Cfg  Get IN Edit View**********/
	
	@PostMapping("/master/getMatchWeightCfg")
	@ResponseBody
	public MatchWeightCfg getMatchWeightCfg(Long paramId) {
		MatchWeightCfg getMatchWeightCfg = this.masterService.getMatchWeightCfg(paramId);
		return getMatchWeightCfg;
	}
	
	/************Domain Master ****************/
	 /*******Domain Master Saveing**********/
	@PostMapping("/master/saveDomainMst")
	@ResponseBody
	public DomainMst saveDomainMst(@RequestBody DomainMst domainMst)
	{
		DomainMst tempDomainMst= null;
	if(domainMst.getDmId() > 0) {
		Integer dm = masterService.checkDuplicateDomainMst(domainMst.getDomainName(), domainMst.getDmId());
		if(dm !=null ) {
			throw new IdenticalException();
		}
		
		tempDomainMst = this.masterService.saveDomainMst(domainMst);
	} else {
		Integer dm = masterService.getDomainMst(domainMst.getDomainName());
		if(dm !=null) {
			throw new IdenticalException();
		}
		tempDomainMst = this.masterService.saveDomainMst(domainMst);
	}
	return tempDomainMst;
	}
	
		/*******Domain Master Get IN Edit View**********/
	
	@PostMapping("/master/getDomainMst")
	@ResponseBody
	public DomainMst getDomainMst(Long dmId) {
		DomainMst getDomainMst = this.masterService.getDomainMst(dmId);
		return getDomainMst;
	}
	
	/************Course Master ****************/
	 /*******Course Master Saveing**********/
	@PostMapping("/master/saveCourseMst")
	@ResponseBody
	public CourseMst saveCourseMst(@RequestBody CourseMst courseMst)
	{
		CourseMst tempCourseMst= null;
	if(courseMst.getCoId() > 0) {		
		Integer dm = masterService.checkDuplicateCourseMst(courseMst.getCourseName(), courseMst.getCoId());
		if(dm !=null ) {
			throw new IdenticalException();
		}
		
		tempCourseMst = this.masterService.saveCourseMst(courseMst);
	} else {
		Integer cm = masterService.getCourseMst(courseMst.getCourseName());
		if(cm !=null) {
			throw new IdenticalException();
		}
		tempCourseMst = this.masterService.saveCourseMst(courseMst);
	}
	return tempCourseMst;
	}
	
		/*******Course Master Get IN Edit View**********/
	
	@PostMapping("/master/getCourseMst")
	@ResponseBody
	public CourseMst getCourseMst(Long coId) {
		CourseMst getCourseMst = this.masterService.getCourseMst(coId);
		return getCourseMst;
	}
	
	/************Spoc Master ****************/
	 /*******Spoc Master Saveing**********/
	@PostMapping("/master/saveSpocMst")
	@ResponseBody
	public SpocMst saveSpocMst(@RequestBody SpocMst spocMst)
	{
		SpocMst tempSpocMst= null;
	if(spocMst.getSpocId() > 0) {
		Integer sm = masterService.checkDuplicateSpocMst(spocMst.getSpocName(), spocMst.getSpocId());
		if(sm !=null ) {
			throw new IdenticalException();
		}
		
		tempSpocMst = this.masterService.saveSpocMst(spocMst);
	} else {
		Integer sm = masterService.getSpocMst(spocMst.getSpocName());
		if(sm !=null) {
			throw new IdenticalException();
		}
		tempSpocMst = this.masterService.saveSpocMst(spocMst);
	}
	return tempSpocMst;
	}
	
		/*******Spoc Master Get IN Edit View**********/
	
	@PostMapping("/master/getSpocMst")
	@ResponseBody
	public SpocMst getSpocMst(Long spocId) {
		SpocMst getSpocMst = this.masterService.getSpocMst(spocId);
		return getSpocMst;
	}
	
	
	/*******SpocMstList Vw Get IN List View**********/	
	@GetMapping("/master/getSpocMstListVw")
	@ResponseBody
	public List<SpocMstListVw> getSpocMstListVw(Long clId) {
		return masterService.getSpocMstListVw(clId);
	}
	
	/*******SpocMastertList Vw Get IN List View**********/	
	@GetMapping("/master/getSpocMasterListVw")
	@ResponseBody
	public List<SpocMstListVw> getSpocMasterListVw() {
		return masterService.getSpocMasterListVw();
	}
	
	
	/************Institute Master ****************/
	 /*******Institute Master Saveing**********/
	@PostMapping("/master/saveInstituteMst")
	@ResponseBody
	public InstituteMst saveInstituteMst(@RequestBody InstituteMst instituteMst)
	{
		InstituteMst tempInstituteMst= null;
	if(instituteMst.getImId() > 0) {
		Integer im = masterService.checkDuplicateInstituteMst(instituteMst.getInsName(), instituteMst.getImId());
		if(im !=null ) {
			throw new IdenticalException();
		}
		
		tempInstituteMst = this.masterService.saveInstituteMst(instituteMst);
	} else {
		Integer sm = masterService.getInstituteMst(instituteMst.getInsName());
		if(sm !=null) {
			throw new IdenticalException();
		}
		tempInstituteMst = this.masterService.saveInstituteMst(instituteMst);
	}
	return tempInstituteMst;
	}
	
		/*******Institute Master Get IN Edit View**********/
	
	@PostMapping("/master/getInstituteMst")
	@ResponseBody
	public InstituteMst getInstituteMst(Long imId) {
		InstituteMst getInstituteMst = this.masterService.getInstituteMst(imId);
		return getInstituteMst;
	}
	
	/************Position Master ****************/
	 /*******Position Master Saveing**********/
	@PostMapping("/master/savePositionMst")
	@ResponseBody
	public PositionMst savePositionMst(@RequestBody PositionMst positionMst)
	{
		PositionMst tempPositionMst= null;
	if(positionMst.getPmId() > 0) {		
		Integer dm = masterService.checkDuplicatePositionMst(positionMst.getPositionName(), positionMst.getPmId());
		if(dm !=null ) {
			throw new IdenticalException();
		}
		
		tempPositionMst = this.masterService.savePositionMst(positionMst);
	} else {
		Integer cm = masterService.getPositionMst(positionMst.getPositionName());
		if(cm !=null) {
			throw new IdenticalException();
		}
		tempPositionMst = this.masterService.savePositionMst(positionMst);
	}
	return tempPositionMst;
	}
	
		/*******Position Master Get IN Edit View**********/
	
	@PostMapping("/master/getPositionMst")
	@ResponseBody
	public PositionMst getPositionMst(Long pmId) {
		PositionMst getPositionMst = this.masterService.getPositionMst(pmId);
		return getPositionMst;
	}
	
	/*******CompanyMst Vw Get IN List View**********/	
	@GetMapping("/master/getCompanyMstVw")
	@ResponseBody
	public List<CompanyMstVw> getCompanyMstVw() {
		return masterService.getCompanyMstVw();
	}
	
	/*******PositionMst Vw Get IN List View**********/	
	@GetMapping("/master/getPositionMstVw")
	@ResponseBody
	public List<PositionMstVw> getPositionMstVw() {
		return masterService.getPositionMstVw();
	}
		
}