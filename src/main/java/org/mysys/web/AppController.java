package org.mysys.web;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.mysys.model.DashboardKPIServiceVw;
//import org.mysys.model.PartnerTeamVw;
import org.mysys.model.User;
import org.mysys.model.Service.ServiceLeadVw;
//import org.mysys.model.notification.NotifyListVw;
//import org.mysys.model.partner.PartnerAcceptanceLadger;
//import org.mysys.model.partner.PartnerLedgerVw;
import org.mysys.service.AbstractService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AppController extends AbstractService {	
	
	@GetMapping("/app/IsUserExists")
	public boolean isUserExists(@RequestParam(value="userid",required=true) String userid,
			@RequestParam(value="password",required=true) String pass,
			@RequestParam(value="token",required=true) String token) {
		return  appService.isLoginExists(userid,pass,token);
	}

	@GetMapping("/app/logout")
	public boolean loggOut(@RequestParam(value="loginId",required=true) String loginId,
			@RequestParam(value="token",required=true) String token) {
		return  appService.loggOut(loginId,token);
	}
	
	
//	@GetMapping("/app/getServiceLeadVw")
//	public ResponseEntity<List<ServiceLeadVw>> getServiceLeadVw(String token){
//		if(validateLogin(token)) {
//	        return new ResponseEntity<List<ServiceLeadVw>>(prodService.getServiceLeadVw(), new HttpHeaders(), HttpStatus.OK);
//		} else {
//	        return new ResponseEntity<List<ServiceLeadVw>>(null, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
//		}
//	}


//	@GetMapping("/app/getTransaction")
//	public ResponseEntity<List<PartnerLedgerVw>> getTransaction(Long pmId,String token){
//		if(validateLogin(token)) {
//	        return new ResponseEntity<List<PartnerLedgerVw>>(prodService.getPartnerLedgerPm(pmId), new HttpHeaders(), HttpStatus.OK);
//		} else {
//	        return new ResponseEntity<List<PartnerLedgerVw>>(null, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
//		}
//	}

	@GetMapping("/app/getProfileDetails")
	public ResponseEntity<User> getUserName(String loginId,String token) {
		if(validateLogin(token)) {
	        return new ResponseEntity<User>(service.getUserNameByLoginId(loginId), new HttpHeaders(), HttpStatus.OK);
		} else {
	        return new ResponseEntity<User>(null, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
		}
	}

	@GetMapping("/app/getDashBoardKPI")
	public ResponseEntity<List<DashboardKPIServiceVw>> getKpiServiceDetails(String loginId,String token){
		if(validateLogin(token)) {
	        return new ResponseEntity<List<DashboardKPIServiceVw>>(dasboardServices.getKpiServiceDetailsForMobile(loginId), new HttpHeaders(), HttpStatus.OK);
		} else {
	        return new ResponseEntity<List<DashboardKPIServiceVw>>(null, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
		}
	}

	@GetMapping("/app/geServLeadData")
	public ResponseEntity<List<ServiceLeadVw>> geServLeadData(long userId,long typeId,String token){
		if(validateLogin(token)) {
	        return new ResponseEntity<List<ServiceLeadVw>>(dasboardServices.geServLeadData(userId,typeId), new HttpHeaders(), HttpStatus.OK);
		} else {
	        return new ResponseEntity<List<ServiceLeadVw>>(null, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
		}
	}

	@GetMapping("/app/markWIP")
	public ResponseEntity<String> makeWIP(long slId,String token){
		if(validateLogin(token)) {
	        return new ResponseEntity<String>(customerService.updateWIP(slId), new HttpHeaders(), HttpStatus.OK);
		} else {
	        return new ResponseEntity<String>("fail", new HttpHeaders(), HttpStatus.UNAUTHORIZED);
		}
		
	}

	@GetMapping("/app/markComplete")
	public ResponseEntity<String> markComplete(long slId,Date start,Date end,String rating,BigDecimal totalCharge,String token){
		if(validateLogin(token)) {
	        return new ResponseEntity<String>(customerService.markComplete(slId,start,end,rating,totalCharge), new HttpHeaders(), HttpStatus.OK);
		} else {
	        return new ResponseEntity<String>("fail", new HttpHeaders(), HttpStatus.UNAUTHORIZED);
		}
	}
	
//	@GetMapping("/app/getPartnerTeamVw")
//	public ResponseEntity<List<PartnerTeamVw>> getPartnerTeamVw(Long pmId,String token){	
//		if(validateLogin(token)) {
//	        return new ResponseEntity<List<PartnerTeamVw>>(partnerService.getListPartnerTeamVw(pmId), new HttpHeaders(), HttpStatus.OK);
//		} else {
//	        return new ResponseEntity<List<PartnerTeamVw>>(null, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
//		}
//	}
	
//	@GetMapping("/app/updatePartnerInServlead")
//	public ResponseEntity<PartnerAcceptanceLadger> updatePartnerInServlead(long slId,String loginId,String token){
//		if(validateLogin(token)) {
//	        return new ResponseEntity<PartnerAcceptanceLadger>(customerService.updatePmId(slId,loginId), new HttpHeaders(), HttpStatus.OK);
//		} else {
//	        return new ResponseEntity<PartnerAcceptanceLadger>(null, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
//		}
//	}
	
	@GetMapping("/app/notifyAtMobile")
	public ResponseEntity<String> notifyAtMobile(String ids,String token) {
		if(validateLogin(token)) {
	        return new ResponseEntity<String>(masterService.notifyAtMobile(ids), new HttpHeaders(), HttpStatus.OK);
		} else {
	        return new ResponseEntity<String>("fail", new HttpHeaders(), HttpStatus.UNAUTHORIZED);
		}
	}
	
	@GetMapping("/app/updatePartnerTeam")
	public ResponseEntity<String> updatePartnerTeam(long slId,long pmId, long ptId,String token){ 
		if(validateLogin(token)) {
	        return new ResponseEntity<String>(customerService.markUpdatePtIdByPmId(slId,pmId,ptId), new HttpHeaders(), HttpStatus.OK);
		} else {
	        return new ResponseEntity<String>("fail", new HttpHeaders(), HttpStatus.UNAUTHORIZED);
		}
	}
	
	
	
//	@GetMapping("/app/allNotification")
//	public ResponseEntity<List<NotifyListVw>> markComplete(String token){ 
//		if(validateLogin(token)) {
//	        return new ResponseEntity<List<NotifyListVw>>(masterService.getAllNotification(token), new HttpHeaders(), HttpStatus.OK);
//		} else {
//	        return new ResponseEntity<List<NotifyListVw>>(null, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
//		}
//	}
}