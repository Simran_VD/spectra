package org.mysys.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.mysys.exception.IdenticalException;
import org.mysys.model.CityMasterView;
import org.mysys.model.CompanySetup;
import org.mysys.model.Designation;
import org.mysys.model.DropdownVO;
import org.mysys.model.job.IssueLogDtlCommVw;
import org.mysys.model.MiscCfg;
import org.mysys.model.PinBoardListView;
import org.mysys.model.Role;
import org.mysys.model.RoleListVw;
import org.mysys.model.Rolescreenaccess;
import org.mysys.model.Screen;
import org.mysys.model.User;
import org.mysys.model.job.CompanyMst;
import org.mysys.model.job.UserListVw;
import org.mysys.model.job.IssueLogDtlComments;
import org.mysys.model.job.IssueLog;
import org.mysys.model.job.IssueLogDtl;
import org.mysys.service.AdminService;
import org.mysys.service.EmailService;
import org.mysys.service.MstDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AdminController {
	

	
	@Autowired
	private AdminService service;
	
	@GetMapping("/admin/addRole")
    public String renderAddRole() {
        return "/admin/addRole";
    }
	
	@GetMapping("/admin/addUser")
    public String renderAddUser() {
        return "/admin/addUser";
    }
	
	@PostMapping("/admin/saveUser")
	@ResponseBody
	public User saveUser(@RequestBody User user){
		User tempUser = null;
		if(user.getUserid() > 0) {
		Integer us = service.checkDuplicateUser(user.getLoginid(), user.getUserid());
		if(us !=null ) {
			throw new IdenticalException();
		}
		tempUser = service.saveUser(user);
		} else {
		Integer us = service.getUser(user.getLoginid());
			if(us !=null) {
				throw new IdenticalException();
			}
			tempUser = service.saveUser(user);	
		}
		
		return tempUser;
	}
	
	@PostMapping("/admin/getNumberCheck")
	@ResponseBody
	public String getNumberCheck(String mobile, String phone, Long userid){
		return service.getNumberCheck(mobile, phone,userid);
	}
	
	@PostMapping("/admin/changePwd")
	@ResponseBody
	public String changePwd(String curPwd, String newPwd){
		return service.changePwd(curPwd, newPwd);
	}
	
	@PostMapping("/admin/getUser")
	@ResponseBody
	public User getUser(long id){
		return service.getUser(id);
	}
	
	/*
	 * @PostMapping("/admin/getUserName")
	 * 
	 * @ResponseBody public User getUserName(String name){ return
	 * service.getUserNameByLoginId(name) ;
	 * 
	 * }
	 */
	 
	
	@GetMapping("/admin/getUser")
	@ResponseBody
	public Long getUser(){
		return service.getLoggedInUserUserId();
	}
	
	@PostMapping("/admin/getUsersManager")
	@ResponseBody
	public List<User> getUsersManager(String loginid) {
		List<User> users = service.getUsersManager(loginid);
		return users;
	}
	@GetMapping("/admin/getUsers")
	@ResponseBody
	public List<DropdownVO> getUsers(){
		return service.getUsers();
	}
	@PostMapping("/admin/getUsersEmail")
	@ResponseBody
	public String getUsersEmail(long userid, long siteid){
		return service.getUsersEmail(userid, siteid);
	}
	
	@PostMapping("/admin/getCurrentUserId")
	@ResponseBody
	public Long getCurrentUserId(String loginid){
		return service.getCurrentUserId(loginid);
	}
	
	
	@PostMapping("/admin/saveRole")
	@ResponseBody
	public Role saveRole(@RequestBody Role role){
		return service.saveRole(role);
	}	
	
	@GetMapping("/admin/deleteUser")
	@ResponseBody
	public String deleteUser(long id){
		return service.deleteUser(id);
	}
	
	@GetMapping("/admin/GetRoleDropdownData")
	@ResponseBody
	public Map<String,List<DropdownVO>> getDropdownData(){
		return service.getDropDownData();
	}
	
	@GetMapping("/admin/GetDesignDropdownData")
	@ResponseBody
	public Map<String,List<DropdownVO>> GetDesignDropdownData(){
		return service.GetDesignDropdownData();
	}
	
	@GetMapping("/admin/deleteRole")
	@ResponseBody
	public String deleteRole(Long id){
		return  service.deleteRole(id);
	}
	
	@PostMapping("/admin/getRole")
	@ResponseBody
	public Role getRole(Long id){
		Role role =  service.getRole(id);
		
		//Remove duplicate entries.			//TODO Need to be updated mapping in Role class.
		List<Rolescreenaccess> rolescreenaccesses = new ArrayList<Rolescreenaccess>();
		if(role.getRolescreenaccesses() != null && role.getRolescreenaccesses().size() > 0) {
			for(Rolescreenaccess rsa: role.getRolescreenaccesses()) {
				if(!rolescreenaccesses.contains(rsa)) {
					rolescreenaccesses.add(rsa);
				}
			}
		}
		role.getRolescreenaccesses().clear();
		role.setRolescreenaccesses(rolescreenaccesses);
		return role;
	}
	
	@GetMapping("/admin/LoginIdExists")
	@ResponseBody
	public String LoginIdExists(String loginId) {
		String resp = service.LoginIdExists(loginId);
		return resp;
	}
	
	@PostMapping("/admin/saveDesignation")
	@ResponseBody
	public Designation saveDesignation(String designationName,String desigCode){
		return service.saveDesignation(designationName,desigCode);
	}
	
	@GetMapping("/admin/screens")
	@ResponseBody
	public List<Screen> getScreens(){
		return service.getScreens();
	}
	
	
	/****************************** Start :setup*************************/
	
	@PostMapping("/admin/saveMiscCfg")
	@ResponseBody
	public MiscCfg saveMiscCfg(@RequestBody MiscCfg misccfg) {
		//LOGGER.debug("Material (" + misccfg.getCfgKey() + ") is being saved.");
		MiscCfg res = null;
		if(misccfg.getCfgId() > 0) {
			res = service.saveMiscCfg(misccfg);
		} else {
			List<MiscCfg> setupList =service.getMiscCfg(misccfg.getCfgValue());
			
			if(setupList != null && setupList.size() > 0) {
				throw new IdenticalException();
			}
			res =service.saveMiscCfg(misccfg);
		}
		//LOGGER.debug("Material (" + misccfg.getCfgKey()) + ") is saved.");
		return res;
	}

	@PostMapping("/admin/getMiscCfg")
	@ResponseBody
	public MiscCfg getSetup(long cfgId) {
		MiscCfg res =service.findMiscCfg(cfgId);
		return res;
	}

	@PostMapping("/admin/deleteMiscCfg")
	@ResponseBody
	public String deleteSetup(long cfgId) {
		return service.deleteMiscCfg(cfgId);
	}	
	/****************************** End :setup*************************/
	
	/****************************** Start: Company setup*************************/
	@Autowired
	private MstDataService mstDataService;

	@PostMapping("/admin/saveCompanyInfo")
	@ResponseBody
	public boolean saveCompanyInfo(@RequestBody CompanySetup cmpSetup) {
		boolean cmpInfoSaved = false;
		
		CompanySetup res = service.getCompanyInfo();

		if(cmpSetup != null && res != null) {
			cmpSetup.setSiteid(res.getSiteid());
			cmpSetup.setDisplayName(cmpSetup.getCmpName());
//			if(cmpSetup.getCityid() != null && cmpSetup.getCityid() > 0) {
//				cmpSetup.setCityName(mstDataService.findCity(cmpSetup.getCityid()).getCityname());
//			}
			if(cmpSetup.getStateid() != null && cmpSetup.getStateid() > 0) {
				cmpSetup.setStateName(mstDataService.findState(cmpSetup.getStateid()).getStatename());
			}
			String fullAddress = "";
			if(cmpSetup.getAddress1() != null && cmpSetup.getAddress1().trim().length() > 0) {
				fullAddress = cmpSetup.getAddress1(); 
			}
			if(cmpSetup.getAddress2() != null && cmpSetup.getAddress2().trim().length() > 0) {
				fullAddress = (fullAddress.length() > 0) ? (fullAddress + ", " + cmpSetup.getAddress2()) : cmpSetup.getAddress2();
			}
			if(cmpSetup.getAddress3() != null && cmpSetup.getAddress3().trim().length() > 0) {
				fullAddress = (fullAddress.length() > 0) ? (fullAddress + ", " + cmpSetup.getAddress3()) : cmpSetup.getAddress3();
			}
			if(cmpSetup.getStateName() != null && cmpSetup.getStateName().trim().length() > 0) {
				fullAddress = (fullAddress.length() > 0) ? (fullAddress + ", " + cmpSetup.getStateName()) : cmpSetup.getStateName();
			}
			if(cmpSetup.getCityName() != null && cmpSetup.getCityName().trim().length() > 0) {
				fullAddress = (fullAddress.length() > 0) ? (fullAddress + ", " + cmpSetup.getCityName()) : cmpSetup.getCityName();
			}
			if(cmpSetup.getZipCode() != null && cmpSetup.getZipCode().trim().length() > 0) {
				fullAddress = (fullAddress.length() > 0) ? (fullAddress + ", " + cmpSetup.getZipCode()) : ("Zipcode: " + cmpSetup.getZipCode());
			}
			cmpSetup.setFullAddress(fullAddress);
			service.updateCompanyInfo(cmpSetup);
			cmpInfoSaved = true;
		}
		return cmpInfoSaved;
	}

	@PostMapping("/admin/getCompanyInfo")
	@ResponseBody
	public CompanySetup getCompanyInfo() {
		CompanySetup res = service.getCompanyInfo();
		return res;
	}
	/****************************** End: Company setup*************************/
	@PostMapping("/admin/getUserName")
	@ResponseBody
	public User getUserName() {
		User user = service.getUserName();
		return user;
		
	}
	
	/****************************** START: City Master view *************************/
	@GetMapping("/admin/getAllCityMasterView")
	@ResponseBody
	public List<CityMasterView> getCityMasterView()
	{
		return service.getAllCityMasterView();
	}
	
	/****************************** Role Drop down View *********************************/
	@GetMapping("/admin/getRoleDropdown")
	@ResponseBody
	public List<RoleListVw> getRoleDropDown(){
		return service.getRoleDropDown();
	}
	
	/* This controller is used in pinboard list view */
	@GetMapping("/admin/getPinBoardListView")
	@ResponseBody
	public List<PinBoardListView> getByIdPinboardList()
	{
		return service.getByIdPinboardListView();
	}
	
	
	@GetMapping("/admin/getUserslistvwManager")
	@ResponseBody
	public List<UserListVw> getUserslistvwManager() {
		List<UserListVw> users = service.getUserslistvwManager();
		return users;
	}
	 
@PostMapping("/admin/saveIssueLogDtl")
	
	@ResponseBody
	public IssueLog saveIssueLog(@RequestBody(required=true) IssueLog issueLog){
		IssueLog issLog = null;
		if(issueLog.getIlId() > 0) {
			issLog= service.saveIssueLog(issueLog);
		} else {
			if(!(issueLog.getType().equalsIgnoreCase("pinboard"))) {
				List<IssueLogDtl> issue = service.getIssueLogByCode(issueLog.getIssueLogDtl().get(0).getCode());
				if(issue.size() > 0) {
					throw new IdenticalException();
				}
			}
			
			
			issLog = service.saveIssueLog(issueLog);
		}
		return issLog;		
	}


		@GetMapping("/admin/getIssueLogDtlComments")
		@ResponseBody
		public List<IssueLogDtlCommVw> getIssueLogDtlComments(Long ilId){
			return service.getIssueLogDtlComments(ilId);
		}

		@PostMapping("/admin/saveIssueLogDtlComments")
		@ResponseBody
		public boolean saveIssueLogDtlComments(@RequestBody List<IssueLogDtlComments> issueLogDtlComm)
		{
			return service.saveIssueLogDtlComments(issueLogDtlComm);
		}
		
		@GetMapping("/admin/getIssueLog")
		@ResponseBody
		public IssueLog getIssueLog(Long ilId){
			return service.getIssueLog(ilId);
		}
		
		@GetMapping("/admin/updateRecordById")
		@ResponseBody
		public int updateRecordById(Long ilId)
		{
			int Success = service.updateRecordById(ilId);
			return Success;
		}
		
		@GetMapping("/test")
		@ResponseBody
		public String test()
		{
			return "success";
		}
}
