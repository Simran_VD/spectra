package org.mysys.web;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.jboss.logging.MDC;
import org.mysys.exception.IdenticalException;
//import org.mysys.model.Department;
import org.mysys.model.DashboardKPIServiceVw;
import org.mysys.model.InterviewLevelMst;
import org.mysys.model.InterviewStatusMst;
import org.mysys.model.job.ApplBackedOutMst;
import org.mysys.model.job.ApplRejectionMst;
import org.mysys.model.job.ApplicantMaster;
import org.mysys.model.job.ApplicantMasterWeb;
import org.mysys.model.job.ApplicantResume;
import org.mysys.model.job.CasesStatusMstVw;
import org.mysys.model.job.ClientMst;
import org.mysys.model.job.ClientSpoc;
import org.mysys.model.job.CourseMstListVw;
import org.mysys.model.job.DashJobOpeningList;
import org.mysys.model.job.DashboardApplicantCases;
import org.mysys.model.job.DashboardApplicantList;
import org.mysys.model.job.DashboardJobApplicantMatch;
import org.mysys.model.job.DashboardJobCount;
import org.mysys.model.job.DashboardKPIServiceDetailVw;
import org.mysys.model.job.DomainMstDropDownVw;
import org.mysys.model.job.EduMst;
import org.mysys.model.job.EduMstListVw;
import org.mysys.model.job.ExperienceDropDownVw;
import org.mysys.model.job.FormatSpocVw;
import org.mysys.model.job.InstituteMstVw;
import org.mysys.model.job.JobApplNotesCommentsVw;
import org.mysys.model.job.JobApplicant;
import org.mysys.model.job.JobApplicantInterview;
import org.mysys.model.job.JobMaster;
import org.mysys.model.job.JobOpeningListVw;
import org.mysys.model.job.JobStatusMaster;
import org.mysys.model.job.LocationMasterView;
import org.mysys.model.job.NoticePeriodDropdownListVw;
import org.mysys.model.job.RecruiterMaster;
import org.mysys.model.job.RecuGrpMapVw;
import org.mysys.model.job.RecuGrpMstListView;
import org.mysys.model.job.SkillCatMst;
import org.mysys.model.job.SkillCatMstListVw;
import org.mysys.model.job.SkillDomainMst;
import org.mysys.model.job.SkillDomainMstDropDownVw;
import org.mysys.model.job.SkillDomainMstListVw;
import org.mysys.model.job.SkillGroupListVw;
import org.mysys.model.job.SkillGroupMst;
import org.mysys.model.job.SkillMst;
import org.mysys.model.job.SkillMstDropDownVw;
import org.mysys.model.job.SourceMst;
import org.mysys.model.job.SourceMstDropDown;
import org.mysys.model.job.SpocMst;
import org.mysys.model.job.SpocShareFormat;
import org.mysys.model.job.StateMasterView;
import org.mysys.model.job.TaskAndReminder;
import org.mysys.model.job.UserListVw;
import org.mysys.service.JobService;
import org.mysys.utility.DashboardCountStatusVO;
import org.mysys.utility.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class JobController {
	
	@Autowired
	private JobService service;
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	@PostMapping("/job/saveJobMaster")
	@ResponseBody
	public JobMaster saveJobMaster(@RequestBody JobMaster jobMaster)
	{
		JobMaster jms = null;
		
		if(jobMaster.getJmId() > 0) {
			if(!StringUtils.isEmpty(jobMaster.getJobCode())) {
			Integer jm = service.checkDuplicateJobMaster(jobMaster.getJobCode(),jobMaster.getJmId());
			if(jm !=null ) {
				throw new IdenticalException();
				}
			}
			jms = service.saveJobMaster(jobMaster);
		}
		
		else {
			if(!StringUtils.isEmpty(jobMaster.getJobCode())) {
				Integer jobCode = service.getJobCode(jobMaster.getJobCode());
				
				if(jobCode != null)
				throw new IdenticalException();	
			}
			jms = this.service.saveJobMaster(jobMaster);
		}
		
		return jms;
	}
	
	@PostMapping("/job/saveJobApplicantNotes")
	@ResponseBody
	public JobApplicant saveJobApplicantNotes(@RequestBody JobApplicant jobApplicant)
	{
		JobApplicant ja = null;
		if( jobApplicant.getJaId() != null && jobApplicant.getJaId() > 0) {
			ja = service.saveJobApplicantNotes(jobApplicant);
		}
		return ja;
	}
	
	@PostMapping("/job/saveAppliInterview")
	@ResponseBody
	public JobApplicant saveAppliInterview(@RequestBody JobApplicant ja) {
		JobApplicant appli = null;
		if(ja.getJobInterview().get(0).getJaiId() > 0) {
			appli = service.saveAppliInterview(ja);
		}
		appli = service.saveAppliInterview(ja);
		return appli;
	}

	
	@GetMapping("/job/getJobMaster")
	@ResponseBody
	public JobMaster getJobMaster(Long jmId)
	{
		return this.service.getJobMaster(jmId);
	}
	
	@GetMapping("/job/getAllCityView")
	@ResponseBody
	public List<LocationMasterView> getAllCityView()
	{
		List<LocationMasterView> allCityData = this.service.getAllCityData();
		return allCityData;
	}
	
	@GetMapping("/job/getAllskillView")
	@ResponseBody
	public List<SkillMstDropDownVw> getAllskillView()
	{
		 List<SkillMstDropDownVw> allSkillData = this.service.getAllSkillData();
		return allSkillData;
	}
	
	@GetMapping("/job/getAllJobStatusView")
	@ResponseBody
	public List<JobStatusMaster> getAllJobStatusView()
	{
		List<JobStatusMaster> allJobStatusData = this.service.getAllJobStatusData();
		return allJobStatusData;
	}
	
	@GetMapping("/job/getAllCompanyNameView")
	@ResponseBody
	public List<DomainMstDropDownVw> getAllCompanyNameView()
	{
		List<DomainMstDropDownVw> allCompanyNameData = this.service.getAllCompanyNameData();
		return allCompanyNameData;		
	}
	
	@GetMapping("/job/getAllRecruiterView")
	@ResponseBody
	public List<RecruiterMaster> getAllRecruiterView()
	{
		List<RecruiterMaster> allRecruiterData = this.service.getAllRecruiterData();
		return allRecruiterData;		
	}
	
	@GetMapping("/job/getAllClientSpocView")
	@ResponseBody
	public List<ClientSpoc> getAllClientSpocView(Long clId)
	{
		return this.service.getAllClientSpocData(clId);
	}
	
	@PostMapping("/job/saveSkillDomainMst")
	@ResponseBody
	public SkillDomainMst saveSkillDomainMst(@RequestBody SkillDomainMst skillDomainMst)
	{
		SkillDomainMst skillDMst = null;
		if(skillDomainMst.getSdmId() > 0) {
			Integer sd = service.checkDuplicateSkillDomainMst(skillDomainMst.getSkillDomainName(),skillDomainMst.getSdmId());
			if(sd !=null ) {
				throw new IdenticalException();
			}
			skillDMst = this.service.saveSkillDomainMst(skillDomainMst);
		}else {
			Integer sd = service.getSkillDomainMst(skillDomainMst.getSkillDomainName());
			if(sd != null) {
				throw new IdenticalException();
			}
		}
		skillDMst = this.service.saveSkillDomainMst(skillDomainMst);
		return skillDMst;
	}
	
	@PostMapping("/job/getSkillDomainMst")
	@ResponseBody
	public SkillDomainMst getSkillDomainMst(Long sdmId) {
		SkillDomainMst getSkillDomainMst = this.service.getSkillDomainMst(sdmId);
		return getSkillDomainMst;
	}
	
	@PostMapping("/job/saveSkillGroupMst")
	@ResponseBody
	public SkillGroupMst saveSkillGroupMst(@RequestBody SkillGroupMst skillGroupMst)
	{
		SkillGroupMst skillGMst = null;
		if(skillGroupMst.getSgmId() > 0) {
			Integer sgm = service.checkDuplicateSkillGroupMst(skillGroupMst.getSkillGroupName(), skillGroupMst.getSgmId());
			if(sgm !=null ) {
				throw new IdenticalException();
			}
			skillGMst = this.service.saveSkillGroupMst(skillGroupMst);
		} else {
			Integer sgm = service.getSkillGroupMst(skillGroupMst.getSkillGroupName());
			if(sgm != null) {
				throw new IdenticalException();
			}
			skillGMst = this.service.saveSkillGroupMst(skillGroupMst);
		}
		return skillGMst;
	}
	
	@PostMapping("/job/getSkillGroupMst")
	@ResponseBody
	public SkillGroupMst getSkillGroupMst(Long sgmId) {
		SkillGroupMst getSkillGroupMst = this.service.getSkillGroupMst(sgmId);
		return getSkillGroupMst;
	}
	
	@GetMapping("/job/getAllSkillDoaminName")
	@ResponseBody
	public List<SkillDomainMstDropDownVw> getAllSkillDoaminName()
	{
		return this.service.getAllSkillDomainData();
	}
	
	@PostMapping("/job/saveSkillCatMst")
	@ResponseBody
	public SkillCatMst saveSkillCatMst(@RequestBody SkillCatMst skillCatMst)
	{
		SkillCatMst skillCMst = null;
		if(skillCatMst.getScmId() > 0) {
			Integer scm = service.checkDuplicateSkillCatMst(skillCatMst.getSkillCatName(), skillCatMst.getScmId());
			if(scm !=null ) {
				throw new IdenticalException();
			}
			skillCMst = this.service.saveSkillCatMst(skillCatMst);
		} else {
			Integer scm = service.getSkillCatMst(skillCatMst.getSkillCatName());
			if(scm != null ) {
				throw new IdenticalException();
			}
			skillCMst = this.service.saveSkillCatMst(skillCatMst);
		}
		return skillCMst;
	}
	
	@PostMapping("/job/getSkillCatMst")
	@ResponseBody
	public SkillCatMst getSkillCatMst(Long scmId) {
		SkillCatMst getSkillCatMst = this.service.getSkillCatMst(scmId);
		return getSkillCatMst;
	}
	
	// get all jobs for login dashboard
	@GetMapping("/job/getAllDashboardJobs")
	@ResponseBody
	public List<DashboardKPIServiceVw> getAllDashboardJobs()
	{
		return this.service.getAllDashboardJobs();
	}
	
	
	
	@PostMapping("/job/saveApplicantMaster")
	@ResponseBody
	public ApplicantMaster saveApplicantMaster(@RequestBody ApplicantMaster applicantMaster)
	{
		ApplicantMaster tempApplicantMaster= null;
		if(applicantMaster.getAmId() > 0) {
			Integer am = service.checkDuplicateApplicantMaster(applicantMaster.getEmail(), applicantMaster.getAmId());
			if(am !=null ) {
				throw new IdenticalException();
			}
			tempApplicantMaster = service.saveApplicantMaster(applicantMaster);
		}
		else {				
				Integer am = service.getApplicantMaster(applicantMaster.getEmail());
				if(am !=null) {
					throw new IdenticalException();
				}
				
				tempApplicantMaster = service.saveApplicantMaster(applicantMaster);
			}
		return tempApplicantMaster;
	}
	
	@PostMapping("/job/saveApplicantMasterWeb")
	@ResponseBody
	public ApplicantMasterWeb saveApplicantMasterWeb(@RequestBody ApplicantMasterWeb applicantMasterWeb)
	{
		return this.service.saveApplicantMasterWeb(applicantMasterWeb);
	}
	
	@GetMapping("/job/getApplicantMaster")
	@ResponseBody
	public ApplicantMaster getApplicantMaster(Long amId)
	{
		return this.service.getApplicantMaster(amId);
	}
	
	@GetMapping("/job/getAllSourceType")
	@ResponseBody
	public List<SourceMstDropDown> getAllSourceType()
	{
		return this.service.getAllSourceType();
	}
	
	@GetMapping("/job/getAllState")
	@ResponseBody
	public List<StateMasterView> getAllState()
	{
		return this.service.getAllState();
	}
	
	
	@PostMapping("/job/saveSkillMst")
	@ResponseBody
	public SkillMst saveSkillMst(@RequestBody SkillMst skillMst)
	{
		SkillMst skillM = null;
		if(skillMst.getSmId() > 0) {
			Integer sm = service.checkDuplicateSkillMst(skillMst.getSkillName(),skillMst.getSmId());
			if(sm !=null ) {
				throw new IdenticalException();
			}
			
			skillM = this.service.saveSkillMst(skillMst);
		} else {
			Integer sm = service.getSkillMst(skillMst.getSkillName());
			if(sm !=null) {
				throw new IdenticalException();
			}
			skillM = this.service.saveSkillMst(skillMst);
		}
		return skillM;
	}

	
	
	@PostMapping("/job/getSkillMst")
	@ResponseBody
	public SkillMst getSkillMst(Long smId) {
		SkillMst getSkillMst = this.service.getSkillMst(smId);
		return getSkillMst;
	}
	
	@PostMapping("/job/saveEduMst")
	@ResponseBody
	public EduMst saveEduMst(@RequestBody EduMst eduMst)
	{
		EduMst eduM = null;
		if(eduMst.getEmId() > 0) {
			Integer ed = service.checkDuplicateEduMst(eduMst.getEduLevel(),eduMst.getEmId());
			if(ed !=null ) {
				throw new IdenticalException();
			}
			eduM = this.service.saveEduMst(eduMst);
		} else {
			Integer ed = service.getEduMst(eduMst.getEduLevel());
			if(ed !=null) {
				throw new IdenticalException();
			}
			eduM = this.service.saveEduMst(eduMst);
		}
		return eduM;
	}
	
	@PostMapping("/job/getEduMst")
	@ResponseBody
	public EduMst getEduMst(Long emId) {
		EduMst getEduM = this.service.getEduMst(emId);
		return getEduM;
	}
	
	@PostMapping("/job/saveSourceMst")
	@ResponseBody
	public SourceMst saveSourceMst(@RequestBody SourceMst sourceMst)
	{
		SourceMst tempSourceMst = null;
		if(sourceMst.getSomId() > 0) {
			Integer sm = service.checkDuplicateSourceMst(sourceMst.getSourceName(),sourceMst.getSomId());
			if(sm !=null ) {
				throw new IdenticalException();
			}
			tempSourceMst = this.service.saveSourceMst(sourceMst);
		} else {
			Integer sm = service.getSourceMst(sourceMst.getSourceName());
			if(sm !=null) {
				throw new IdenticalException();
			}
			tempSourceMst = this.service.saveSourceMst(sourceMst);
		}
		return tempSourceMst;
	}
	
	@PostMapping("/job/getSourceMst")
	@ResponseBody
	public SourceMst getSourceMst(Long somId) {
		SourceMst getSourceMst = this.service.getSourceMst(somId);
		return getSourceMst;
	}
	
	@GetMapping("/job/getSkillCatMstListVw")
	@ResponseBody
	public List<SkillCatMstListVw> getSkillCatMstListVw() {
		return service.getSkillCatMstListVw();
	}
	
	@GetMapping("/job/getSkillDomainMstListVw")
	@ResponseBody
	public List<SkillDomainMstListVw> getSkillDomainMstListVw() {
		return service.getSkillDomainMstListVw();
	}
	
	@GetMapping("/job/getSkillGroupListVw")
	@ResponseBody
	public List<SkillGroupListVw> getSkillGroupListVw() {
		return service.getSkillGroupListVw();
	}
	
	@GetMapping("/job/NoticePeriodDropdownListVw")
	@ResponseBody
	public List<NoticePeriodDropdownListVw> getNoticePeriodDropdownListVw() {
		return service.getNoticePeriodDropdownListVw();
	}
	
	@GetMapping("/job/ExperienceDropDownVw")
	@ResponseBody
	public List<ExperienceDropDownVw> getExperienceDropDownVw() {
		return service.getExperienceDropDownVw();
	}
	
	// get detail of single job from dashboard
	@PostMapping("/job/getDashboardJobDetail")
	@ResponseBody
	public List<DashboardKPIServiceDetailVw> getDashboardJobDetail(@RequestBody JobMaster jobMaster)
	{
		return this.service.getDashboardJobDetail(jobMaster.getJmId());
	}
	
	@GetMapping("/job/getGrpTypeview")
	@ResponseBody
	public List<RecuGrpMstListView> getGrpTypeview(String grpType)
	{
		return this.service.getGrpType(grpType);
	}

	@GetMapping("/job/getAllRecuGrpList")
	@ResponseBody
	public List<RecuGrpMstListView> getAllRecuGrpList()
	{
		return this.service.getAllRecuGrp();
	}

	
	@GetMapping("/job/getAllDashboardJobCount")
	@ResponseBody
	public List<DashboardJobCount> getAllDashboardJobCount(Integer jmid)
	{
		return this.service.getAllDashboardJobCount(jmid);
	}
	
	@GetMapping("/job/getDashboardApplicantList")
	@ResponseBody
	public List<DashboardApplicantList> getDashboardApplicant(Long jmId)
	{
		return this.service.getDashboardApplicant(jmId);
	}
		
	/* Dashboard Count by Status & Month/Year API
	 * Use this to display Jobs & Applicant Count on Dashboard screen
	 *  */
	@GetMapping("/job/getAllDashboardJobStatusCount")
	@ResponseBody
	public DashboardCountStatusVO getAllDashboardJobStatusCount(String status)
	{
		return this.service.getAllDashboardJobStatusCount(status);
	}

		
    @GetMapping("/job/getDashboardApplicantMatchList")
    @ResponseBody
	public List<DashboardJobApplicantMatch> getDashboardApplicantMatch(Long jmId)
	{
		return this.service.getDashboardApplicantMatch(jmId);
	}
		
		
	@PostMapping("/job/saveJobApplicant")
	@ResponseBody
	public List<JobApplicant> saveJobApplicant(@RequestBody List<JobApplicant> jobApplicant)
	{
		return this.service.saveJobApplicant(jobApplicant);
	}
	
	
	@GetMapping("/job/getAllJobOpeningList")
	@ResponseBody
	public List<JobOpeningListVw> getAllJobOpeningList()
	{
		return this.service.getAllJobOpeningList();
	}
	
	@GetMapping("/job/getAllrecugrpmap")
	@ResponseBody
	public List<RecuGrpMapVw> getAllrecugrp(Long rgm_id)
	{
		if(rgm_id != null) {
		return this.service.getAllrecugrp(rgm_id); 
		}
		else {
			return null;
		}
	}
	
	@GetMapping("/job/getAllEduMst")
	@ResponseBody
	public List<EduMstListVw> getEduMstListVw()
	{
		return this.service.getEduMstListVw();
	}
	
	@GetMapping("/job/getAllCourseMstVw")
	@ResponseBody
	public List<CourseMstListVw> getAllCourseMstVw()
	{
		return this.service.getAllCourseMstVw();
	}
		
	@GetMapping("/job/getAllinstMstVw")
	@ResponseBody
	public List<InstituteMstVw> getAllinstMstVw()
	{
		return this.service.getAllinstMstVw();
	}
		
	@GetMapping("/job/getJobOpeningList")
	@ResponseBody
	public List<DashJobOpeningList> getJobOpeningList(int position)
	{
		return this.service.getJobOpeningList(position);
	}
	
	//Status drop down in Job Applicant
	@GetMapping("/job/getStatus")
	@ResponseBody
	public List<CasesStatusMstVw> getStatus(){
		return this.service.getCasesStatus();
	}
	
	//Display Comment Section of Job Applicant Form
	@GetMapping("/job/getJobComments")
	@ResponseBody
	public List<JobApplNotesCommentsVw> getJobComments(long jaId){
		return this.service.getJobComm(jaId);
	}
	
	//Get Job Applicant Data
	@GetMapping("/job/getJobApplicant")
	@ResponseBody
	public JobApplicant getJobApplicant(long jaId){
		return this.service.getJobAppli(jaId);
	}
	
	//Reason of Rejection dropdown
	@GetMapping("/job/getRejection")
	@ResponseBody
	public List<ApplRejectionMst> getApplRejection(){
		return this.service.getApplicantRejection();
	}
	
	//Reason of Backing Out dropdown
	@GetMapping("/job/getBackedOutApplicant")
	@ResponseBody
	public List<ApplBackedOutMst> getApplBackedOut(){
		return this.service.getApplicantBackOut();
	}
	
	//Get Interview Detail of Job Applicant
	@PostMapping("/job/getInterview")
	@ResponseBody
	public JobApplicantInterview getInterview(Long jaiId) {
		return this.service.getInterviewDetail(jaiId);
	}
	
	//DropDown for spoc name
	@GetMapping("/job/getSpocDropdown")
	@ResponseBody
	public List<SpocMst> getSpocDropdown() {
		return this.service.getSpocName();
	}
	
	//Status DropDown of jobInterview 
	@GetMapping("/job/getInterviewStatus")
	@ResponseBody
	public List<InterviewStatusMst> getInterviewStatus(){
		return this.service.getStatusInterview();
	}
	
	@GetMapping("/job/getdashboardapplicantCases")
	@ResponseBody
	public List<DashboardApplicantCases> getDashboardApplicantCases(){
		return this.service.getDashboardApplicantCases();
	}
	
	@GetMapping("/job/getTaskandReminder")
	@ResponseBody
	public List<TaskAndReminder> getTaskAndReminder(){
		return this.service.getTaskAndReminder();
	}
	
	@GetMapping("/job/getUserManager")
	@ResponseBody
	public List<UserListVw> getUserListVwRepo(){
		return this.service.getUserListVwRepo();
	}
	
	@GetMapping("/job/getApplicantResume")
	@ResponseBody
	public ApplicantResume getApplicantResume(long amid) {
		return this.service.getApplicantResume(amid);
	}
	
	// use this to send dynamic custom email
	@PostMapping("/job/sendEmail")
	@ResponseBody
	public int sendEMailAPI(Email email) {
		
		
		return this.service.sendEmail(email);
	}
	
	@GetMapping("/job/getSpocShareFormat")
	@ResponseBody
	public List<SpocShareFormat> getSpocShareFormat(Long spocid,Long fmid){
		return this.service.getSpocShareFormat(spocid,fmid);
	}
	
	@GetMapping("/job/CheckDuplicate")
	@ResponseBody
	public Integer getAllApplicantEmail(String email, String phoneno){
		return this.service.getAllApplicantEmail(email, phoneno);
	}
	
	//get email format for spoc list 
	@GetMapping("/job/getFormatView")
	@ResponseBody
	public List<FormatSpocVw> getSpocFormatView(Long clId){
		return this.service.getSpocFormatView(clId);
	}
		
	@GetMapping("/job/getInterviewLevel")
	@ResponseBody
	public List<InterviewLevelMst> getInterviewLevel(){
		return this.service.getInterviewLevel();
	}
	
	

	@GetMapping("/testmdc")
	@ResponseBody
	public String test(Long id)
	{
		LOGGER.info("Requested API : /testmdc");
		ThreadContext.put("req-method", "test()");
		ThreadContext.put("req-data", id.toString());
		LOGGER.info("Now At Service Level");
		return "ok";
	}

	
}
