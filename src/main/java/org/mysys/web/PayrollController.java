
package org.mysys.web;
import java.util.List;

import org.mysys.model.DropdownVO;
import org.mysys.service.AbstractService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class PayrollController extends AbstractService {

	@GetMapping("/payroll/getDesignation")
	@ResponseBody
	public List<DropdownVO> getDesignation(){
		return payrollService.getDesignation();
	}

	@GetMapping("/payroll/getSkills")
	@ResponseBody
	public List<DropdownVO> getSkills(){
		return payrollService.getSkills();
	}

}
