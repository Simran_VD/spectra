package org.mysys.utility;

import java.net.URI;
import java.util.Set;

import org.apache.xmlbeans.impl.jam.mutable.MMethod;
import org.json.simple.JSONObject;
import org.mysys.service.AbstractService;
import org.mysys.service.SMSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonObject;

@Component
public class SMSThread extends AbstractService implements Runnable{
	
	@Autowired 
	RestTemplate restTemplate;
	
	
	private Set<String> mobile;
	
	private String message;
	
	
	
	
	public Set<String> getMobile() {
		return mobile;
	}

	public void setMobile(Set<String> mobile) {
		this.mobile = mobile;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

	public SMSThread(Set<String> mobile, String message) {
		super();
		this.mobile = mobile;
		this.message = message;
	}

	public SMSThread() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		sendSMS(mobile, message);
	}
	
	public void sendSMS(Set<String> mobile,String message){
		
		try {
			mobile.add("9560773863");
			if(mobile != null && mobile.size() > 0) {
//				URI uri = new URI("http://localhost/happee/utility/sendSms");
				
//				try {
//				JSONObject json = new JSONObject();
//				json.put("mobile", mobile);
//				json.put("message", message);
//				
//
////				URI uri = new URI("http://localhost:8080/sendMail");
//				HttpHeaders headers = new HttpHeaders();
//				headers.setContentType(MediaType.APPLICATION_JSON);
//				
//				HttpEntity entity = new HttpEntity<>(json,headers);
//				
//			
//				ResponseEntity<String> result =	restTemplate.postForEntity(uri,entity, String.class);
//				
//				System.out.println(result.getStatusCodeValue());
//				System.out.println(result.getBody());
//				
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
				
				
				for(String mob : mobile){
					   SMSService.sendCustomMsg(mob, message);
				}
			
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	

}
