package org.mysys.utility;

import java.util.List;

import org.mysys.model.job.DashboardJobStatusCount;

public class DashboardCountStatusVO {
	
	
	private List<DashboardJobStatusCount> jobsCount;
	
	private List<DashboardJobStatusCount> applicantCount;

	public List<DashboardJobStatusCount> getJobsCount() {
		return jobsCount;
	}

	public void setJobsCount(List<DashboardJobStatusCount> jobsCount) {
		this.jobsCount = jobsCount;
	}

	public List<DashboardJobStatusCount> getApplicantCount() {
		return applicantCount;
	}

	public void setApplicantCount(List<DashboardJobStatusCount> applicantCount) {
		this.applicantCount = applicantCount;
	}
	
	

}
