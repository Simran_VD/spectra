package org.mysys.utility;


import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class MyLogInterceptor implements HandlerInterceptor{
	
	private static final Logger logger = LoggerFactory.getLogger(MyLogInterceptor.class);
	
	
	 @Value("${server.contextPath}")
	  private String contextPath;
	 
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		 long executionStartTime = System.currentTimeMillis();
		    request.setAttribute("start-time", executionStartTime);
		    String fishTag = UUID.randomUUID().toString();
		    String operationPath = this.contextPath + request.getServletPath();
		    String user = "myuser"; /* GET USER INFORMATION FROM SESSION */ 
		    ThreadContext.put("id", fishTag);
		    ThreadContext.put("path", operationPath);
		    ThreadContext.put("user", user);
		    
		    return true;
	}
	
	@Override
	  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mv) {
	    long executionStartTime = (Long)request.getAttribute("start-time");
	    long renderingStartTime = System.currentTimeMillis();
	    long executionTime = System.currentTimeMillis();
	    request.setAttribute("rendering-start-time", renderingStartTime);
	    long executionDuration = renderingStartTime - executionStartTime;
	    ThreadContext.put("execution-duration",String.valueOf(executionTime));
	  }
	
	
	@Override
	  public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
	   Exception exception) throws Exception {
	    long renderingStartTime = (Long)request.getAttribute("rendering-start-time");
	    long renderingEndTime = System.currentTimeMillis();
	    long renderingDuration = renderingEndTime - renderingStartTime;
	    ThreadContext.put("rendering-duration", String.valueOf(renderingDuration));
	    logger.info("My interceptor handler message");
	    ThreadContext.clearMap();
	  }
	 
}
