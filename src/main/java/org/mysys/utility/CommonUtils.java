package org.mysys.utility;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;

@Component
public class CommonUtils {

	public static byte[] getBarCodeImage(String text, int width, int height) {
		try {
			Writer writer = new Code128Writer();
			BitMatrix bitMatrix = writer.encode(text, BarcodeFormat.CODE_128, width, height);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);
			return byteArrayOutputStream.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			//TODO Error need to be logged.
		}
	}

	public static byte[] getBarCodeImage(String text) {
		try {
			Code128Bean code128 = new Code128Bean();
			code128.setHeight(10f);
			code128.setModuleWidth(0.1);
			code128.setQuietZone(1);
			//code128.setVerticalQuietZone(1);
			code128.doQuietZone(true);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			BitmapCanvasProvider canvas = new BitmapCanvasProvider(baos, "image/x-png", 300, BufferedImage.TYPE_BYTE_BINARY, false, 0);
			code128.generateBarcode(canvas, text);
			canvas.finish();

			return baos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			//TODO Error need to be logged.
		} 
	}

	/**
	 * To add report output to MultipartFile object (files).
	 * 
	 * @param files
	 * @param rptUrl
	 * @param fileName
	 * @return
	 */
	public static List <MultipartFile> addReport(List <MultipartFile> files, String rptUrl, String fileName) {
		File file = null;
		FileInputStream inputStream;
		try {
			FileUtils.copyURLToFile(new URL(rptUrl), new File(fileName));
			file = new File(fileName);
			inputStream = new FileInputStream(file);
			MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(inputStream));
			if(files == null || files.size() < 1) {
				files = new ArrayList <MultipartFile>();
			}
			files.add(multipartFile);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			if(file != null) {
				try {
					file.deleteOnExit();
					//file.delete();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			file = null;
		}
		return files;
	}
}
