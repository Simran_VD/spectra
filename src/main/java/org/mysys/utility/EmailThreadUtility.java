package org.mysys.utility;

import java.net.URI;

import org.mysys.service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Component
@Service
public class EmailThreadUtility implements Runnable{
	
	
	private String mailTo;
	
	private String mailSubject;
	
	private String mailContent;
	
	@Autowired
	private RestTemplate restTemplate;
	
	public EmailThreadUtility() {
		
	}

	public EmailThreadUtility(String mailTo, String mailSubject, String mailContent) {
		super();
		this.mailTo = mailTo;
		this.mailSubject = mailSubject;
		this.mailContent = mailContent;
	}




	public String getMailTo() {
		return mailTo;
	}




	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}




	public String getMailSubject() {
		return mailSubject;
	}




	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}




	public String getMailContent() {
		return mailContent;
	}




	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}




	@Override
	public void run() {
		System.out.println("Thread is running ");
		sendMail(mailTo);
	}
	
	
	public int sendMail(String mailTo){
		
		try {
			
			String email = mailTo;
			if(email != null) {
				String message = mailSubject;
				try {
				Email map = new Email();
				map.setMailSubject(message);
				map.setMailTo(email);
				map.setMailContent(mailContent);
				URI uri = new URI("http://164.52.211.113:8080/sendMail");

//				URI uri = new URI("http://localhost:8080/sendMail");
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				
				HttpEntity<Email> entity = new HttpEntity<Email>(map,headers);
				
			
				ResponseEntity<String> result =	restTemplate.postForEntity(uri,entity, String.class);
				if(result != null){
					System.out.println(result.getStatusCodeValue());
					System.out.println(result.getBody());
					return result.getStatusCodeValue();
				}
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
}
