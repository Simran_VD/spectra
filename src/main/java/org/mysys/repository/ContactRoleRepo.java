package org.mysys.repository;

import java.util.List;

import org.mysys.model.Contactrole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRoleRepo extends JpaRepository<Contactrole, Long> {
	
	public  List<Contactrole> findBySiteid(long siteid);
	

}
