package org.mysys.repository;

import java.util.List;

import org.mysys.model.AreaWiseLeadCntVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AreaWiseLeadCntVwRepo extends JpaRepository<AreaWiseLeadCntVw, Long> {

	@Query(value="select * from fnc_dashboard_kpi_area_wise_lead_cnt(:userId)",nativeQuery = true)
	List<AreaWiseLeadCntVw> findByUserId(@Param("userId") int id);

}
