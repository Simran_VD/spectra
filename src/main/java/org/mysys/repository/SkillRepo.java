package org.mysys.repository;

import org.mysys.model.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillRepo extends JpaRepository<Skill, Long> {

	public Skill findByidAndSiteid(long id, long siteid);

}
