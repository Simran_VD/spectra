//package org.mysys.repository.customer;
//
//import org.mysys.model.customer.CustMst;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository 
//public interface CustMstRepo extends JpaRepository<CustMst, Long>  {
//
//	CustMst findByCsId(Long csId);
//
//	CustMst findByMobile(String mobile);
//
//}
