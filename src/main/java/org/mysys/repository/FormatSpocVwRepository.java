package org.mysys.repository;

import java.util.List;

import org.mysys.model.job.FormatSpocVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FormatSpocVwRepository extends JpaRepository<FormatSpocVw, Long>{

	
	@Query(value="select * from format_spoc_drop_down_vw where siteid=:siteid and cl_id=:clid", nativeQuery=true)
	List<FormatSpocVw> getAllFormats(@Param("siteid") long siteid,@Param("clid") long clid);
	
}
