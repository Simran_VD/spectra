package org.mysys.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.mysys.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByLoginid(String loginid);
    
    @Query("select u from User as u where u.loginid!=:loginid and status='1'")
    User findByActiveLoginid(@Param("loginid") String loginid);
    
	@Modifying
	@Query("update User u set u.password = :password where u.loginid = :loginid")
    int changePwd(@Param("loginid") String loginid, @Param("password") String password);
	
	@Query("select email from User as u where u.userid=:userid  and  u.siteid = :siteid")
	public String getUserEmail(@Param("userid") long userid,@Param("siteid") long siteid);	
	
	@Query("select userid from User as u where u.loginid=:loginid")
	public Long getCurrentUserId(@Param("loginid") String loginid);	
	
	@Query("select u from User as u where u.loginid!=:loginid")
	public List<User> getUsersManager(@Param("loginid") String loginid);	
	
	@Modifying
	@Query("update User u set u.passwordResetDate = :passwordResetDate where u.loginid = :loginid")
	public int updateResetPassworDate(@Param("passwordResetDate") Date passwordResetDate,@Param("loginid") String loginid);

	@Query("select u from User as u where u.loginid=:loginid and u.password=:password and u.status=1")
	public User findByActiveLoginidAndPassword(@Param("loginid") String loginid, @Param("password") String password);
	
	User findByLoginidAndPassword(String userid, String pass);
	
	@Query("select userid from User where token=:token")
	Long findUserId(@Param("token") String token);


	@Modifying
	@Query("update User set token=:token where userid=:userid")
	void updateFCMToken(@Param("userid") long userid,@Param("token") String token);	
	
	@Modifying
	@Query("update User set token=:token where loginid=:loginid")
	int loggOut(@Param("loginid") String loginid,@Param("token") String token);	
	
	@Query("select token from User where loginid=:loginid")
	String findToken(@Param("loginid") String loginid);

	User findByUseridAndSiteid(long userId,long siteid);

	@Query(value="select SIMILARITY(us.loginid,:loginid) from users us  where userid!=:userid and SIMILARITY(us.loginid,:loginid)=1;",nativeQuery = true)
	Integer findduplicateByLoginidonEdit(@Param("loginid") String loginid,@Param("userid") long userid);

	@Query(value="select SIMILARITY(us.loginid,:loginid) from users us  where SIMILARITY(us.loginid,:loginid)=1;",nativeQuery = true)
	Integer findduplicateByLoginid(@Param("loginid") String loginid);
	
	@Query(value = "select * from users where userid=:userid", nativeQuery=true)
	public User getUserByPK(@Param("userid") Long userid);
	
	@Transactional
	@Modifying
	@Query("update User u set u.password = :password where u.userid = :userid")
    int updatePwd(@Param("userid") Long userid, @Param("password") String password);

	@Query("select mobile from User where userid=:userid")
	String getMobile(@Param("userid") Long userid);
	
	@Query(value="select trim(concat(u.firstname, ' ', u.lastname)) as names from users  u where u.userid=:userid", nativeQuery=true)
	public String getName(@Param("userid") long userid);

	List<User> findByMobile(String mobile);
	
	List<User> findByPhone(String phone);

	List<User> findByMobileAndUserid(String mobile, Long userid);

	List<User> findByPhoneAndUserid(String phone, Long userid);
	
	
}