package org.mysys.repository;


import java.util.List;

import org.mysys.model.job.DomainMstDropDownVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DomainMstDropDownVwRepo extends JpaRepository<DomainMstDropDownVw, Long>{

	public List<DomainMstDropDownVw> findByIsActive(String isActive);
}
