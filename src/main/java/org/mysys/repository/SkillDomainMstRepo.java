package org.mysys.repository;


import org.mysys.model.job.SkillDomainMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillDomainMstRepo extends JpaRepository<SkillDomainMst, Long>{

	public SkillDomainMst findBySdmId(Long sdmId);

	@Query(value="select SIMILARITY(sd.skill_domain_name,:skillDomainName) from skill_domain_mst sd  where SIMILARITY(sd.skill_domain_name,:skillDomainName)=1;",nativeQuery = true)
	public Integer findduplicateBySkillDomainName(@Param("skillDomainName") String skillDomainName);

	@Query(value="select SIMILARITY(sd.skill_domain_name,:skillDomainName) from skill_domain_mst sd  where sdm_id!=:sdmId and SIMILARITY(sd.skill_domain_name,:skillDomainName)=1;",nativeQuery = true)
	public Integer findduplicateBySkillDomainNameonEdit(@Param("skillDomainName") String skillDomainName,@Param("sdmId") long sdmId);

}
