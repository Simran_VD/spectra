//package org.mysys.repository;
//
//import java.util.List;
//
//import org.mysys.model.ProductBrandMstVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface ProductBrandMstVwRepo extends JpaRepository<ProductBrandMstVw, Long>{
//	
//
//	public List<ProductBrandMstVw> findByprdId(long pbmId);
//
//}
