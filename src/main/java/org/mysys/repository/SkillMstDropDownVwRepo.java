package org.mysys.repository;

import java.util.List;

import org.mysys.model.job.SkillMstDropDownVw;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillMstDropDownVwRepo extends JpaRepository<SkillMstDropDownVw, Long>{
	

	public List<SkillMstDropDownVw> findByIsActive(String isActive);
	
}
