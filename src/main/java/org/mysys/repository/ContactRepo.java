package org.mysys.repository;

import org.mysys.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepo extends JpaRepository<Contact, Long> {

	@Query(value = "SELECT concat_ws(' ', c.addr1 || ' ' || c.addr2 || ' ' || c.addr3, (select cm.cityname from Citymaster cm where cm.cityid = c.cityid ), "+
			" (select sm.statename from Statemaster sm where sm.stateid = c.stateid), c.pincode) AS address FROM Contact c where c.contactid = :contactid", nativeQuery = true)
	public String getContactAddressByID(@Param("contactid") long contactid);

	@Query(value = "SELECT c.stateid FROM Contact c where c.contactid = :contactid", nativeQuery = true)
	public String getContactStateID(@Param("contactid") long contactid);
}
