package org.mysys.repository;

import java.util.List;

import org.mysys.model.DashboardKPIServiceVw;
import org.mysys.model.Service.ServiceLeadVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DashboardKPIServiceVwRepo extends JpaRepository<DashboardKPIServiceVw, Long> {
	
	
	  @Query(value="select * from fnc_kpi_job_opening_dtl(:displid,:rm_id)",nativeQuery=
	  true) List<DashboardKPIServiceVw> getAllKPI(@Param("displid") int displid,@Param("rm_id") int rm_id);
	 
	@Query(value="select * from fnc_dashboard_kpi_service_detail(:userId)",nativeQuery = true)
	List<DashboardKPIServiceVw> getByUserId(@Param("userId") int id);
}
