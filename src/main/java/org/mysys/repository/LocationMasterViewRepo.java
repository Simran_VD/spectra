package org.mysys.repository;

import java.util.List;

import org.mysys.model.job.LocationMasterView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationMasterViewRepo extends JpaRepository<LocationMasterView, Long>{
	
//	@Query(value = "select c from LocationMasterView c")
//	public List<LocationMasterView> getAllCityName();

	public List<LocationMasterView> findByIsActive(String isActive);

}
