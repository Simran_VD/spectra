package org.mysys.repository;

import java.util.List;

import org.mysys.model.job.RecruiterMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RecruiterMasterRepo extends JpaRepository<RecruiterMaster, Long>{
	
	@Query(value = "select c from RecruiterMaster c")
	public List<RecruiterMaster> getallrecruiter();

}
