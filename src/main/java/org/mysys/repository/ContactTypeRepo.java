package org.mysys.repository;

import java.util.List;


import org.mysys.model.Contacttype;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactTypeRepo extends JpaRepository<Contacttype, Long> {
	
	//public List<Contacttype> findByContacttypenameAndSiteid(@Param("contacttypename ") String contacttypename,@Param("siteid") Long siteid);

	public  List<Contacttype> findBySiteid(long siteid);

}
