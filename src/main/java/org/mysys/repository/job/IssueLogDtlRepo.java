package org.mysys.repository.job;
import java.util.List;
import org.mysys.model.job.IssueLogDtl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IssueLogDtlRepo extends JpaRepository<IssueLogDtl, Long>{

	// void deleteByIlId(Long ilId);

	List<IssueLogDtl> findByIlId(Long ilId);

	List<IssueLogDtl> findByCode(String code);

	@Modifying
	@Query("update IssueLogDtl i set i.isActive = 'N' where i.ilId = :ilId")
	void updateByIlId(@Param("ilId")Long ilId);


}
