package org.mysys.repository.job;

import org.mysys.model.job.UserTypeMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeMstRepo extends JpaRepository<UserTypeMst, Long>{

	/*******User Type Master Check Duplicate**********/
//	@Query(value ="select * from user_type_mst utm where lower(utm.user_type) like (lower(concat('%',:userType,'%')))",nativeQuery = true)
	@Query(value="select SIMILARITY(utm.user_type,:userType) from user_type_mst utm  where SIMILARITY(utm.user_type,:userType)=1;",nativeQuery = true)
	public Integer findduplicateByUserType(@Param("userType") String userType);

	
	UserTypeMst findByTypeId(Long typeId);

	@Query(value="select SIMILARITY(utm.user_type,:userType) from user_type_mst utm  where type_id!=:typeId and SIMILARITY(utm.user_type,:userType)=1;",nativeQuery = true)
	public Integer findduplicateByUserTypeonEdit(@Param("userType")String userType,@Param("typeId") Long typeId);

}
