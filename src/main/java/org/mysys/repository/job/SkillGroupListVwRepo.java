package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.SkillGroupListVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillGroupListVwRepo extends JpaRepository<SkillGroupListVw, Long>{

	public List<SkillGroupListVw> findByIsActive(String isActive);
	
}
