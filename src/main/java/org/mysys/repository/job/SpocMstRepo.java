package org.mysys.repository.job;

import org.mysys.model.job.SpocMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SpocMstRepo extends JpaRepository<SpocMst, Long>{

//	@Query(value ="select * from spoc_mst spm where lower(spm.spoc_name) like (lower(concat('%',:spocName,'%')))",nativeQuery = true)
	@Query(value="select SIMILARITY(spm.spoc_name,:spocName) from spoc_mst spm  where SIMILARITY(spm.spoc_name,:spocName)=1;",nativeQuery = true)
	public Integer findduplicateBySpocName(@Param("spocName") String spocName);

	public SpocMst findBySpocId(Long spocId);
	
	@Query(value="select SIMILARITY(spm.spoc_name,:spocName) from spoc_mst spm  where spoc_id!=:spocId and SIMILARITY(spm.spoc_name,:spocName)=1;",nativeQuery = true)
	public Integer findduplicateBySpocNameonEdit(@Param("spocName") String spocName,@Param("spocId") Long spocId);

}
