package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.PositionMstVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionMstVwRepo extends JpaRepository<PositionMstVw, Long> {

	public List<PositionMstVw> findByIsActive(String isActive);

}
