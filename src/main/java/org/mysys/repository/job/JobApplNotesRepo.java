package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.JobApplNotes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobApplNotesRepo extends JpaRepository<JobApplNotes, Long>{

	

}
