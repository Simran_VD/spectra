package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.ApplicantMaster;
import org.mysys.model.job.DashboardApplicantList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface DashboardApplicantListRepo extends JpaRepository<DashboardApplicantList ,Long> {
	 @Query(value="select * from fnc_dashboard_applicant_list(:userid,:siteid,:jmId)",nativeQuery=true) 
	 List<DashboardApplicantList> getDashboardApplicantList(@Param("userid") int userid, @Param("siteid") int siteid,@Param("jmId") Long jmId);
}
