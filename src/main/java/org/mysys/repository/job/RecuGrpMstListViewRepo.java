package org.mysys.repository.job;


import java.util.List;

import org.mysys.model.job.RecuGrpMstListView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RecuGrpMstListViewRepo extends JpaRepository<RecuGrpMstListView, Long>{

	@Query(value = "select g from RecuGrpMstListView g where g.grpType = :grpType and g.isActive = :isActive")
    public List<RecuGrpMstListView> findGrptypeAndIsActive(@Param("grpType") String grpType ,@Param("isActive") String isActive);
	
	@Query(value = "select g from RecuGrpMstListView g")
    public List<RecuGrpMstListView> getAllRecuGrpMstLst();

	
	
}
