package org.mysys.repository.job;

import org.mysys.model.job.IssueLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IssueLogRepo extends JpaRepository<IssueLog, Long>{

	IssueLog findByIlId(Long ilId);

	@Modifying
	@Query("update IssueLog i set i.taskStatus='Y' where i.ilId=:ilId")
	public int updateRecord(@Param("ilId") long ilId);
		  

}
