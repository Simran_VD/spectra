package org.mysys.repository.job;

import org.mysys.model.job.PositionMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionMstRepo  extends JpaRepository<PositionMst, Long>{

	@Query(value="select SIMILARITY(pm.position_name,:positionName) from position_mst pm  where pm_id!=:pmId and SIMILARITY(pm.position_name,:positionName)=1;",nativeQuery = true)
	public Integer findduplicateByPositionNameonEdit(@Param("positionName") String positionName,@Param("pmId") Long pmId);

	@Query(value="select SIMILARITY(pm.position_name,:positionName) from position_mst pm  where SIMILARITY(pm.position_name,:positionName)=1;",nativeQuery = true)
	public Integer findduplicateByPositionName(@Param("positionName") String positionName);

	PositionMst findByPmId(Long pmId);

}
