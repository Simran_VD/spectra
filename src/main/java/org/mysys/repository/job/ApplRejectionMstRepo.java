package org.mysys.repository.job;

import org.mysys.model.job.ApplRejectionMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

@Repository
public interface ApplRejectionMstRepo extends JpaRepository<ApplRejectionMst, Long>{

}
