package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.DashboardJobStatusCount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DashboardJobStatusCountRepo extends JpaRepository<DashboardJobStatusCount, Long> {
	
	@Query(value = "select * from fnc_dashboard_job_status_count(:userid) where period =:status ", nativeQuery = true)
	public List<DashboardJobStatusCount> getAllDashboardJobStatusCount(@Param("userid") int userid,@Param("status") String status);

}
