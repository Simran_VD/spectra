package org.mysys.repository.job;

import org.mysys.model.job.JobEdu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JobEduRepo extends JpaRepository<JobEdu, Long>{

	@Modifying
	@Query("update JobEdu e set e.isActive = 'N' where e.jmId = :jmId")
	void updateByJmId(@Param("jmId") Long jmId);

	public void deleteByJmId(long jmId);

	@Modifying(clearAutomatically = true)
	@Query(value="delete from job_edu where jm_id=:id", nativeQuery = true)
	void deleteByJobMasterId(@Param("id") Long jmId);

}
