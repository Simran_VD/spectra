package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.SourceMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SourceMstRepo extends JpaRepository<SourceMst, Long>{

	@Query(value="select SIMILARITY(sm.source_name,:sourceName) from source_mst sm  where SIMILARITY(sm.source_name,:sourceName)=1;",nativeQuery = true)
	public Integer findduplicateBySourceName(@Param("sourceName") String sourceName);

	public SourceMst findBySomId(Long somId);
	
	@Query(value="select SIMILARITY(sm.source_name,:sourceName) from source_mst sm  where som_id!=:somId and SIMILARITY(sm.source_name,:sourceName)=1;",nativeQuery = true)
	public Integer findduplicateBySourceNameonEdit(@Param("sourceName") String sourceName,@Param("somId") long somId);

}
