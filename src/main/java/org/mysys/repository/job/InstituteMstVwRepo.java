package org.mysys.repository.job;

import org.mysys.model.job.InstituteMstVw;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstituteMstVwRepo extends JpaRepository<InstituteMstVw, Long>{

}
