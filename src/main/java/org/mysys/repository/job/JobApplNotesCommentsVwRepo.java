package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.JobApplNotesCommentsVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobApplNotesCommentsVwRepo extends JpaRepository<JobApplNotesCommentsVw, Long>{

	List<JobApplNotesCommentsVw> findByJaId(long jaId);

}
