package org.mysys.repository.job;

import org.mysys.model.job.ApplicantMasterWeb;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicantMasterWebRepo extends JpaRepository<ApplicantMasterWeb, Long>{

}
