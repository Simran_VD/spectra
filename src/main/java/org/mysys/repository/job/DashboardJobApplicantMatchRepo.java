package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.DashboardJobApplicantMatch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DashboardJobApplicantMatchRepo extends JpaRepository<DashboardJobApplicantMatch ,Long>  {
	 @Query(value="select * from fnc_dashboard_job_applicant_match(:userid,:siteid,:jmId)",nativeQuery=true) 
	 List<DashboardJobApplicantMatch> getDashboardApplicantMatch(@Param("userid") int userid, @Param("siteid") int siteid);
}
