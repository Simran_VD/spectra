package org.mysys.repository.job;

import org.mysys.model.job.JobCert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface JobCertRepo  extends JpaRepository<JobCert, Long>{

	@Modifying
	@Query("update JobCert jc set jc.isActive = 'N' where jc.jmId = :jmId")
	void updateByJmId(@Param("jmId") Long jmId);

	void deleteByJmId(long jmId);

	@Modifying(clearAutomatically = true)
	@Query(value = "delete from job_cert where jm_id=:id", nativeQuery = true)
	void deleteByJobMasterId(@Param("id") Long jmId);

}
