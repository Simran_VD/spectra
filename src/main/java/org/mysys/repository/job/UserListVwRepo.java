package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.UserListVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface UserListVwRepo extends JpaRepository<UserListVw, Long>{
	@Query(value="select * from userlist_vw",nativeQuery=true) 
	 List<UserListVw> getUserListVwRepo();
	
	@Query("select u from UserListVw as u where u.siteid=:siteid")
	List<UserListVw> getUserslistvwManager(@Param("siteid") long loggedInUserSiteId);
}
