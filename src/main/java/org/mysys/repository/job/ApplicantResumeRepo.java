package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.ApplicantResume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


@Repository
public interface ApplicantResumeRepo extends JpaRepository<ApplicantResume, Long>{
	
	@Modifying
	@Query("update ApplicantResume a set a.isActive = 'N' where a.amId = :amId")
	public void updateByAmId(@Param("amId") Long amId);

	public ApplicantResume findByAmId(long amid);

	@Query(value = "select resume_name from applicant_resume where am_id=:amId", nativeQuery = true)
	public String getByAmId(@Param("amId")long enId);

	@Modifying
	@Query(value = "delete from applicant_resume where am_id=:amId", nativeQuery = true)
	public void deleteByamId(@Param("amId") Long amid);
	
	

}
