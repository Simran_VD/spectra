package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.StateMasterView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StateMasterViewRepo extends JpaRepository<StateMasterView, Long>{

//	@Query(value = "select s from StateMasterView s")
//	public List<StateMasterView> getAllState();
//	@Query(value = "select s from StateMasterView s where s.isActive= :isActive")
	public List<StateMasterView> findByIsActive(String isActive);
}
