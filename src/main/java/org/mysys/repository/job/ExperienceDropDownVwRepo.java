package org.mysys.repository.job;

import org.mysys.model.job.ExperienceDropDownVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExperienceDropDownVwRepo extends JpaRepository<ExperienceDropDownVw, Long>{

}
