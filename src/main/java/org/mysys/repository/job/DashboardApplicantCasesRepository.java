package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.DashboardApplicantCases;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DashboardApplicantCasesRepository extends JpaRepository<DashboardApplicantCases, Long>{

	 @Query(value="select * from fnc_dashboard1_job_appl_list(:userid)",nativeQuery=true) 
	 List<DashboardApplicantCases> getDashboardApplicantCases(@Param("userid") int userid);
	
}
