package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.SpocShareFormat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SpocShareFormatRepo extends JpaRepository<SpocShareFormat, Long>{

	@Query(value="select * from fnc_spoc_share_format(:spocid,:userid,:siteid,:fmid)",nativeQuery=true) 
	
	List<SpocShareFormat> getSpocFormat(@Param("spocid") Integer spocid, @Param("userid") int loggedInUserUserId,@Param("siteid") int loggedInUserSiteId,@Param("fmid") Integer fmid);

}
