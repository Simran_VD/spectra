package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.IssueLogDtlCommVw;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IssueLogDtlCommVwRepo extends JpaRepository<IssueLogDtlCommVw, Long>{

	List<IssueLogDtlCommVw> findByIlId(Long ilId);


}
