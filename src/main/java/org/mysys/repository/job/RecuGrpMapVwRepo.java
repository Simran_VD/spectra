package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.RecuGrpMapVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RecuGrpMapVwRepo extends JpaRepository<RecuGrpMapVw, Long>{
	
	 @Query(value="select * from recu_grp_map_vw where rgm_id=:rgm_id",nativeQuery=true) 
	 List<RecuGrpMapVw> getAllrecugrp(@Param("rgm_id") Long rgm_id);

}
