package org.mysys.repository.job;

import java.util.Date;
import java.util.List;

import org.mysys.model.job.JobApplicantInterview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface JobApplicantInterviewRepo extends JpaRepository<JobApplicantInterview, Long>{

	@Query(value = "select * from job_applicant_interview jai join cases_status_mst csm on jai.csm_id = csm.csm_id where csm.status =:status", nativeQuery = true)
	List<JobApplicantInterview> findByCsmId(@Param("status")String status);

	JobApplicantInterview findByJaiId(Long jaiId);

	@Modifying
	@Query(value = "update job_applicant_interview set jai_id =:jaiId where ja_id =:jaId", nativeQuery = true)
	void updateByJaiIdAndJaId(@Param("jaiId")long jaiId, @Param("jaId")Long jaId);

	@Modifying
	@Query(value = "update job_applicant_interview set sch_date=:schdate, panel_name=:panelName, panel_feedback=:feedback, ilm_id=:ilmid, ism_id=:ismId where jai_id =:jaiId", nativeQuery = true)
	void updateByJaiIdAndJaId(@Param("jaiId")long jaiId, @Param("schdate")Date schDate, @Param("panelName")String panelName, @Param("feedback")String panelFeedback, @Param("ilmid")Long ilmid, @Param("ismId") long ismId);
}
