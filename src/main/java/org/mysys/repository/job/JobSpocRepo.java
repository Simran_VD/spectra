package org.mysys.repository.job;

import org.mysys.model.job.JobSpoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface JobSpocRepo extends JpaRepository<JobSpoc, Long> {

	
	@Modifying
	@Query("update JobSpoc js set js.isActive = 'N' where js.jmId = :jmId")
	void updateByJmId(@Param("jmId") Long jmId);

	void deleteByJmId(long jmId);
	
	@Query(value = "delete from job_spoc where jm_id=:id", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	int deleteByJobMasterId(@Param("id") long jmId);

}
