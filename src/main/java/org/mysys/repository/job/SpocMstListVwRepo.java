package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.SpocMstListVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpocMstListVwRepo extends JpaRepository<SpocMstListVw, Long> {

	List<SpocMstListVw> findByClId(Long clId);

}
