package org.mysys.repository.job;

import org.mysys.model.job.DomainDropdownVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DomainDropdownVwRepo extends JpaRepository<DomainDropdownVw, Long> {

}
