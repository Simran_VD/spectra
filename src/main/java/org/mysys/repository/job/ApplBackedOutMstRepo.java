package org.mysys.repository.job;

import org.mysys.model.job.ApplBackedOutMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplBackedOutMstRepo extends JpaRepository<ApplBackedOutMst, Long>{

}
