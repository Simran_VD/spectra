package org.mysys.repository.job;

import org.mysys.model.job.ApplicantSkill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicantSkillRepo extends JpaRepository<ApplicantSkill, Long>{

public void deleteByAmId(long amId);
	
	@Modifying
	@Query("update ApplicantSkill a set a.isActive = 'N' where a.amId = :amId")
	void updateByAmId(@Param("amId")Long amId);
}
