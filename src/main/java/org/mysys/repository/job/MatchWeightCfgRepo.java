package org.mysys.repository.job;

import org.mysys.model.job.MatchWeightCfg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MatchWeightCfgRepo extends JpaRepository<MatchWeightCfg, Long>{
	
	//@Query(value ="select * from match_weight_cfg mwc where lower(mwc.param_name) like (lower(concat('%',:paramName,'%')))",nativeQuery = true)
	@Query(value="select SIMILARITY(mwc.param_name,:paramName) from match_weight_cfg mwc  where SIMILARITY(mwc.param_name,:paramName)=1;",nativeQuery = true)
	public Integer findduplicateByParamName(@Param("paramName") String paramName);

	@Query(value="select SIMILARITY(mwc.param_name,:paramName) from match_weight_cfg mwc  where param_id!=:paramId and SIMILARITY(mwc.param_name,:paramName)=1;",nativeQuery = true)
	public Integer findduplicateByParamNameonEdit(@Param("paramName") String paramName,@Param("paramId") Long paramId);

	
	public MatchWeightCfg findByParamId(Long paramId);

}
