package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.JobOpeningListVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface JobOpeningListVwRepo extends JpaRepository<JobOpeningListVw, Long> {
	
	@Query(value = "select j from JobOpeningListVw j")
	public List<JobOpeningListVw> getAllJobOpeningList();

}
