package org.mysys.repository.job;

import org.mysys.model.job.ApplicantEducation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ApplicantEducationRepo extends JpaRepository<ApplicantEducation, Long>{

	@Modifying
	@Query(value="update applicant_edu set isactive = 'N' where am_id = :amId" ,nativeQuery = true)
	void updateByAmId(@Param("amId") Long amId);

}
