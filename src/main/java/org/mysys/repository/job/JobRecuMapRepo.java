package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.JobRecuMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JobRecuMapRepo extends JpaRepository<JobRecuMap, Long>{
	
	public void deleteByJmId(long jmId);
	
	public JobRecuMap findByJmId(long jmId);
	
	@Modifying
	@Query("update JobRecuMap j set j.isActive = 'N' where j.jmId = :jmId")
	void updateByJmId(@Param("jmId")Long jmId);

	@Query(value = "select rec_id from job_recu_map where jm_id =:jmId", nativeQuery = true)
	public long getByJmId(@Param("jmId") Long jmId);

	@Modifying(clearAutomatically = true)
	@Query(value="delete from job_recu_map where jm_id=:id",nativeQuery = true)
	public void deleteByJobMasterId(@Param("id")Long jmId);
	
	@Query(value = "select * from job_recu_map where jm_id =:jmId", nativeQuery = true)
	public List<JobRecuMap> getRecMapByJmId(@Param("jmId") Long jmId);

}
