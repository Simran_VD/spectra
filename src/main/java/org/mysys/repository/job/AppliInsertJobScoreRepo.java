package org.mysys.repository.job;

import org.mysys.model.job.AppliInsertJobScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AppliInsertJobScoreRepo extends JpaRepository<AppliInsertJobScore, Long>{

	@Query(value="select * from fnc_calc_appl_insert_job_score_P(:userid,:jm_id,:am_id,:flag);", nativeQuery=true)
	void updateApplicantScore(@Param("userid") int loggedInUserUserId,@Param("jm_id") int i,@Param("am_id") int intValue,@Param("flag") String string);

}
