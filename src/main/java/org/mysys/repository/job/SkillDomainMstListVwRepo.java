package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.SkillDomainMstListVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillDomainMstListVwRepo extends JpaRepository<SkillDomainMstListVw, Long>{

	public List<SkillDomainMstListVw> findByIsActive(String isActive);

}
