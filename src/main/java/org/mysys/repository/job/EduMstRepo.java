package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.EduMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EduMstRepo extends JpaRepository<EduMst, Long>{
	

	@Query(value="select SIMILARITY(edu.edu_level,:eduLevel) from edu_mst edu  where SIMILARITY(edu.edu_level,:eduLevel)=1;",nativeQuery = true)
	public Integer findduplicateByEduLevel(@Param("eduLevel") String eduLevel);

	public EduMst findByEmId(Long emId);

	@Query(value="select SIMILARITY(edu.edu_level,:eduLevel) from edu_mst edu  where em_id!=:emId and SIMILARITY(edu.edu_level,:eduLevel)=1;",nativeQuery = true)
	public Integer findduplicateByEduLevelonEdit(@Param("eduLevel") String eduLevel,@Param("emId") long emId);

}
