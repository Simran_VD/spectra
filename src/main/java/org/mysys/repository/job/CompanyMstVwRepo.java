package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.CompanyMstVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyMstVwRepo extends JpaRepository<CompanyMstVw, Long>  {

	public List<CompanyMstVw> findByIsActive(String isActive);

}
