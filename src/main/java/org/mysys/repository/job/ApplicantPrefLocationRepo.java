package org.mysys.repository.job;

import org.mysys.model.job.ApplicantPrefLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicantPrefLocationRepo extends JpaRepository<ApplicantPrefLocation, Long>{

public void deleteByAmId(long amId);
	
	@Modifying
	@Query("update ApplicantPrefLocation a set a.isActive = 'N' where a.amId = :amId")
	void updateByAmId(@Param("amId")Long amId);

}
