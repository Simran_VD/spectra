package org.mysys.repository.job;

import org.mysys.model.job.ApplicantCertificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ApplicantCertificateRepository extends JpaRepository<ApplicantCertificate, Integer>{

	@Modifying
	@Query(value="update ApplicantCertificate a set a.isactive = 'N' where a.am_id = :amId")
	void updateByAmId(@Param("amId") Long amId);
	
}
