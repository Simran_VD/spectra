package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.SourceMstDropDown;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SourceMstDropDownRepo extends JpaRepository<SourceMstDropDown, Long>{

//	@Query(value = "select c from SourceMstDropDown c")
//	public List<SourceMstDropDown> getAllSourceType();

	public List<SourceMstDropDown> findByIsActive(String isActive);
}
