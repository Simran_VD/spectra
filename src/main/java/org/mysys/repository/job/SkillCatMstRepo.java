package org.mysys.repository.job;

import org.mysys.model.job.SkillCatMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillCatMstRepo extends JpaRepository<SkillCatMst, Long>{

	public SkillCatMst findByScmId(Long scmId);
	
	@Query(value="select SIMILARITY(scm.skill_cat_name,:skillCatName) from skill_cat_mst scm  where SIMILARITY(scm.skill_cat_name,:skillCatName)=1;",nativeQuery = true)
	public Integer findduplicateBySkillCatName(@Param("skillCatName") String skillCatName);

	@Query(value="select SIMILARITY(scm.skill_cat_name,:skillCatName) from skill_cat_mst scm  where scm_id!=:scmId and SIMILARITY(scm.skill_cat_name,:skillCatName)=1;",nativeQuery = true)
	public Integer findduplicateBySkillCatNameonEdit(@Param("skillCatName") String skillCatName,@Param("scmId") long scmId);

}
