package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.DashboardJobCount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DashboardJobCountRepo extends JpaRepository<DashboardJobCount, Long> {
	
	@Query(value = "select * from fnc_dashboard_job_count(:userid,:dispid,:jmid)",nativeQuery=true)
	public List<DashboardJobCount> getAllDashboardJobCount(@Param("userid") int userid, @Param("dispid") int dispid, @Param("jmid") int jmid);

}
