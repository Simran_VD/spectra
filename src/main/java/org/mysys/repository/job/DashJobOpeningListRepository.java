package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.DashJobOpeningList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DashJobOpeningListRepository extends JpaRepository<DashJobOpeningList, Integer>{

	
	@Query(value="select * from fnc_dash_job_opening_list(:userid,:position)", nativeQuery=true)
	List<DashJobOpeningList> getJobOpeningList(@Param("userid") int userid,@Param("position") int position);
	
}
