package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.DomainMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DomainMstRepo extends JpaRepository<DomainMst, Long>{

//	@Query(value ="select * from domain_mst dm where lower(dm.domain_name) like (lower(concat('%',:domainName,'%')))",nativeQuery = true)
	@Query(value="select SIMILARITY(dm.domain_name,:domainName) from domain_mst dm  where SIMILARITY(dm.domain_name,:domainName)=1;",nativeQuery = true)
	public Integer findduplicateByDomainName(@Param("domainName") String domainName);

	public DomainMst findByDmId(Long dmId);

	@Query(value="select SIMILARITY(dm.domain_name,:domainName) from domain_mst dm  where dm_id!=:dmId and SIMILARITY(dm.domain_name,:domainName)=1;",nativeQuery = true)
	public Integer findduplicateByDomainNameonEdit(@Param("domainName")String domainName,@Param("dmId") Long dmId);

}
