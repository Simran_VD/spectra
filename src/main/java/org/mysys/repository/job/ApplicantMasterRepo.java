package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.ApplicantMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicantMasterRepo extends JpaRepository<ApplicantMaster, Long>{
   
	ApplicantMaster findByamId(Long amId);

	@Query(value="select SIMILARITY(am.email,:email) from applicant_mst am  where am_id!=:amId and SIMILARITY(am.email,:email)=1;",nativeQuery = true)
	Integer findduplicateByInsEmailonEdit(@Param("email") String email,@Param("amId") Long amId);

	@Query(value="select SIMILARITY(am.email,:email) from applicant_mst am  where SIMILARITY(am.email,:email)=1;",nativeQuery = true)
	Integer findduplicateByEmail(@Param("email") String email);

	/*
	 * @Query(
	 * value="select SIMILARITY(am.email,:email) from applicant_mst  am where SIMILARITY(am.email,:email)=1;"
	 * ,nativeQuery = true) String findduplicateEmailAll(@Param("email") String
	 * email);
	 */
	@Query(value = "select count(*) from applicant_mst where mobile =:mobile", nativeQuery = true)
	Integer findduplicateByMobile(@Param("mobile") String phoneno);

	ApplicantMaster findByAmId(Long amId);
	
	@Query(value="select mobile from applicant_mst where am_id=:amId", nativeQuery=true)
	String getMobile(@Param("amId") Long amId);

	//Integer findduplicateEmailAll(String email);
	
	
	@Query(value="select trim(concat(u.first_name, ' ', u.last_name)) as names from applicant_mst  u where u.am_id=:am_id", nativeQuery=true)
	public String getName(@Param("am_id") long am_id);
}
