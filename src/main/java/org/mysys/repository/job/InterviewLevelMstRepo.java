package org.mysys.repository.job;

import org.mysys.model.InterviewLevelMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InterviewLevelMstRepo extends JpaRepository<InterviewLevelMst, Long>{

}
