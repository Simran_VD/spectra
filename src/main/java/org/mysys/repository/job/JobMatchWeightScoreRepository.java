package org.mysys.repository.job;

import org.mysys.model.job.JobMatchWeightScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JobMatchWeightScoreRepository extends JpaRepository< JobMatchWeightScore, Long>{


	@Query(value="select * from fnc_job_match_weight_score(:createdby,:jm_id,:flag,:am_id);", nativeQuery=true)
	Long updateScore(@Param("createdby") int createdby,@Param("jm_id") int jm_id,@Param("flag") String flag,@Param("am_id") Integer am_id);	
}
