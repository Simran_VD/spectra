package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.IssueLogDtlComments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssueLogDtlCommRepo extends JpaRepository<IssueLogDtlComments, Long>{

	List<IssueLogDtlComments> findByIldcId(Long ildcId);

}
