package org.mysys.repository.job;

import org.mysys.model.job.ApplicantExperience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ApplicantExperienceRepository extends JpaRepository<ApplicantExperience, Integer>{

	@Modifying
	@Query(value="update applicant_exp  set isactive = 'N' where am_id = :amid",nativeQuery = true)
	void updateByAmId(@Param("amid") Long amid);
	
}
