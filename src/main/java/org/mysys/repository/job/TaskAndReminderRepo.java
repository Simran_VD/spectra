package org.mysys.repository.job;

import java.util.ArrayList;
import java.util.List;

import org.mysys.model.job.TaskAndReminder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskAndReminderRepo extends JpaRepository<TaskAndReminder, Long>{
	@Query(value="select * from fnc_dashboard_tasks_n_reminders(:userid)",nativeQuery=true) 
	ArrayList<TaskAndReminder> getTaskAndReminder(@Param("userid") int userid);
}
