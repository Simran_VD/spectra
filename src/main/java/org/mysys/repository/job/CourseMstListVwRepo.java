package org.mysys.repository.job;

import org.mysys.model.job.CourseMstListVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseMstListVwRepo extends JpaRepository<CourseMstListVw, Long>{

}
