package org.mysys.repository.job;

import org.mysys.model.job.CompanyMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyMstRepo extends JpaRepository<CompanyMst, Long>{

	// @Query(value ="select * from company_mst cm where lower(cm.company_name) like (lower(concat('%',:companyName,'%')))",nativeQuery = true)
	@Query(value="select SIMILARITY(com.company_name,:companyName) from company_mst com  where SIMILARITY(com.company_name,:companyName)=1;",nativeQuery = true)
	public Integer findduplicateByCompanyName(@Param("companyName") String companyName);

	public CompanyMst findByCmId(Long cmId);
	
	@Query(value="select SIMILARITY(com.company_name,:companyName) from company_mst com  where cm_id!=:cmId and SIMILARITY(com.company_name,:companyName)=1;",nativeQuery = true)
	public Integer findduplicateByCompanyNameonEdit(@Param("companyName") String companyName,@Param("cmId") long cmId);


	

}
