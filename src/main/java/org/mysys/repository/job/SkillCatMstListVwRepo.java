package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.SkillCatMstListVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillCatMstListVwRepo extends JpaRepository<SkillCatMstListVw, Long>{

 public List<SkillCatMstListVw> findByIsActive(String isActive);
	
}
