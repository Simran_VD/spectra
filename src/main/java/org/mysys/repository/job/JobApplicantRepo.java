package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.JobApplicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Repository;


@Repository
public interface JobApplicantRepo extends JpaRepository<JobApplicant, Long>{

	@Modifying
	@Query(value="update job_applicant set csm_id =:csmid,modifiedby=:userid where ja_id = :jaId",nativeQuery = true)
	void updateByCsmIdAndJaId(@Param("csmid")Long csmId,@Param("jaId")Long jaId,@Param("userid")String userid);

	@Modifying
	@Query(value="update job_applicant set negotiable =:negotiable where ja_id = :jaId",nativeQuery = true)
	void updateByNegotiableAndJaId(@Param("negotiable")String negotiable,@Param("jaId") Long jaId);

	JobApplicant findByJaId(long jaId);

	@Modifying
	@Query(value = "update job_applicant set arm_id =:armId where ja_id =:jaId", nativeQuery = true)
	void updateByArmIdAndJaId(@Param("armId")long armId, @Param("jaId")Long jaId);

	@Modifying
	@Query(value = "update job_applicant set abom_id =:abomId where ja_id =:jaId", nativeQuery = true)
	void updateByAbomIdAndJaId(@Param("abomId")long abomId, @Param("jaId")Long jaId);
	
}
