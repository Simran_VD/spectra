package org.mysys.repository.job;

import org.mysys.model.job.RecuGrpMap;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecuGroupMapRepository extends JpaRepository<RecuGrpMap, Integer>{

}
