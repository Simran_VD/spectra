package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.RecuGrpMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RecuGrpMapRepo extends JpaRepository<RecuGrpMap, Integer>{

	@Query(value = "select * from recu_grp_map where rec_id =:userid", nativeQuery = true)
	List<RecuGrpMap> findByRecId(@Param("userid") long id);

	@Query(value = "select count(*) from recu_grp_map where rec_id =:rec_id  and rgm_id =:rgmid", nativeQuery = true)
	int getByRecIdAndRgmId(@Param("rec_id") long rec_id, @Param("rgmid")long rgmId);
	
	@Modifying
	@Query(value = "delete from recu_grp_map where rec_id =:userid", nativeQuery = true)
	void deleteByRecId(@Param("userid") long userid);
	
	

}
