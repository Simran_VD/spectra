package org.mysys.repository.job;

import java.util.List;

import org.mysys.model.job.EduMstListVw;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EduMstListVwRepo extends JpaRepository<EduMstListVw, Long> {

	public List<EduMstListVw> findByIsActive(String isActive);

}
