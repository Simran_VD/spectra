package org.mysys.repository.job;

import org.mysys.model.job.RecuGrpMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RecuGrpMstRepo extends JpaRepository<RecuGrpMst, Long> {

	/*******Recu Grp Master Check Duplicate**********/
//	@Query(value ="select * from recu_grp_mst rgm where lower(rgm.recu_grp_name) like (lower(concat('%',:recuGrpName,'%')))",nativeQuery = true)
	@Query(value="select SIMILARITY(rgm.recu_grp_name,:recuGrpName) from recu_grp_mst rgm  where SIMILARITY(rgm.recu_grp_name,:recuGrpName)=1;",nativeQuery = true)
	public Integer findduplicateByRecuGrpName(@Param("recuGrpName") String recuGrpName);

	public RecuGrpMst findByRgmId(Long rgmId);

	@Query(value="select SIMILARITY(rgm.recu_grp_name,:recuGrpName) from recu_grp_mst rgm  where rgm_id!=:rgmId and SIMILARITY(rgm.recu_grp_name,:recuGrpName)=1;",nativeQuery = true)
	public Integer findduplicateByRecuGrpNameonEdit(@Param("recuGrpName")String recuGrpName,@Param("rgmId") Long rgmId);	
	

}
