package org.mysys.repository.job;

import org.mysys.model.job.ClientMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientMstRepo extends JpaRepository<ClientMst, Long>{

//	@Query(value ="select * from client_mst cm where lower(cm.company_name) like (lower(concat('%',:companyName,'%')))",nativeQuery = true)
	@Query(value="select SIMILARITY(cm.company_name,:companyName) from client_mst cm  where SIMILARITY(cm.company_name,:companyName)=1;",nativeQuery = true)
	public Integer findduplicateByCompanyName(@Param("companyName") String companyName);

	public ClientMst findByClId(Long clId);

	@Query(value="select SIMILARITY(cm.company_name,:companyName) from client_mst cm  where cl_id!=:clId and SIMILARITY(cm.company_name,:companyName)=1;",nativeQuery = true)
	public Integer findduplicateByCompanyNameonEdit(@Param("companyName") String companyName,@Param("clId") Long clId);

}
