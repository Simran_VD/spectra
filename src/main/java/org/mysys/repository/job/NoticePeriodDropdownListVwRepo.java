package org.mysys.repository.job;

import org.mysys.model.job.NoticePeriodDropdownListVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoticePeriodDropdownListVwRepo extends JpaRepository<NoticePeriodDropdownListVw, Long>{

}
