package org.mysys.repository.job;

import org.mysys.model.job.CourseMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseMstRepo extends JpaRepository<CourseMst, Long>{

//	@Query(value ="select * from course_mst cm where lower(cm.course_name) like (lower(concat('%',:courseName,'%')))",nativeQuery = true)
	@Query(value="select SIMILARITY(cm.course_name,:courseName) from course_mst cm  where SIMILARITY(cm.course_name,:courseName)=1;",nativeQuery = true)
	public Integer findduplicateByCourseName(@Param("courseName") String courseName);

	public CourseMst findByCoId(Long coId);

	@Query(value="select SIMILARITY(cm.course_name,:courseName) from course_mst cm  where co_id!=:coId and SIMILARITY(cm.course_name,:courseName)=1;",nativeQuery = true)
	public Integer findduplicateByCourseNameonEdit(@Param("courseName") String courseName,@Param("coId") Long coId);

}
