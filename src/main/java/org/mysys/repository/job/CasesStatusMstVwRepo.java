package org.mysys.repository.job;

import org.mysys.model.job.CasesStatusMstVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CasesStatusMstVwRepo extends JpaRepository<CasesStatusMstVw, Long>{

}
