package org.mysys.repository;

import org.mysys.model.job.InstituteMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InstituteMstRepo extends JpaRepository<InstituteMst, Long>{

	@Query(value="select SIMILARITY(im.ins_name,:insName) from institute_mst im  where im_id!=:imId and SIMILARITY(im.ins_name,:insName)=1;",nativeQuery = true)
	public Integer findduplicateByInsNameonEdit(@Param("insName") String insName,@Param("imId") Long imId);
	
	@Query(value="select SIMILARITY(im.ins_name,:insName) from institute_mst im  where SIMILARITY(im.ins_name,:insName)=1;",nativeQuery = true)
	public Integer findduplicateByInsName(@Param("insName") String insName);

	public InstituteMst findByImId(Long imId);


}
