package org.mysys.repository;

import org.mysys.model.job.JobLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JobLocationRepo extends JpaRepository<JobLocation, Long>{
	
	public void deleteByJmId(long jmId);
	
	@Modifying
	@Query("update JobLocation j set j.isActive = 'N' where j.jmId = :jmId")
	void updateByJmId(@Param("jmId")Long jmId);

	@Query(value = "delete from job_loc where jm_id=:id", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	public void deleteByJobMasterId(@Param("id") Long jmId);

}
