//package org.mysys.repository.partner;
//
//import java.util.List;
//
//import org.mysys.model.partner.LeadSourceMstVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface LeadSourceMstVwRepo extends JpaRepository<LeadSourceMstVw, Long>{
//
//	List<LeadSourceMstVw> findByUserid(long loggedInUserUserId);
//
//}
