//package org.mysys.repository.partner;
//
//import org.mysys.model.partner.PartnerMst;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface PartnerMstRepo extends JpaRepository<PartnerMst, Long>{
//
//	public PartnerMst findByPmId(long pmId);
//
//	@Modifying
//	@Query("update PartnerMst set panImage =:panByte where pmId=:pmId")
//	public int updatePanPartnerDocs(@Param("panByte") byte[] panByte,@Param("pmId") Long pmId);
//	
//	
//	@Modifying
//	@Query("update PartnerMst set adharImage =:adharByte where pmId=:pmId")
//	public int updateAdharPartnerDocs(@Param("adharByte") byte[] adharByte,@Param("pmId") Long pmId);
//	
//	@Modifying
//	@Query("update PartnerMst set  gstinImage =:gstinByte where pmId=:pmId")
//	public int updateGstinPartnerDocs(@Param("gstinByte") byte[] gstinByte,@Param("pmId") Long pmId);
//	
//	@Modifying
//	@Query("Update PartnerMst set panImage = NULL where pmId=:pmId")
//	public int deletePanPartnerDocs(@Param("pmId") Long pmId);
//	
//	@Modifying
//	@Query("Update PartnerMst set adharImage = NULL where pmId=:pmId")
//	public int deleteAdharPartnerDocs(@Param("pmId") Long pmId);
//	
//	@Modifying
//	@Query("Update PartnerMst set gstinImage = NULL where pmId=:pmId")
//	public int deleteGstinPartnerDocs(@Param("pmId") Long pmId);
//
//}
