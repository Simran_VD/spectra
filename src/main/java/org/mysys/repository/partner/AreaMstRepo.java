//package org.mysys.repository.partner;
//
//import java.util.List;
//
//import org.mysys.model.partner.AreaMst;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface AreaMstRepo extends JpaRepository<AreaMst, Long>{
//
//	AreaMst findByAmId(long amId);
//
//	void deleteByAmId(long amId);
//
//	List<AreaMst> findByAreaNameAndPincodeAndSiteid(String areaName, String pincode, long loggedInUserSiteId);
//
//}
