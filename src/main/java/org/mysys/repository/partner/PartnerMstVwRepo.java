//package org.mysys.repository.partner;
//
//import java.util.List;
//
//import org.mysys.model.partner.PartnerMstVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface PartnerMstVwRepo extends JpaRepository<PartnerMstVw, Long>{
//
//	List<PartnerMstVw> findByUserid(long loggedInUserUserId);
//
//}
