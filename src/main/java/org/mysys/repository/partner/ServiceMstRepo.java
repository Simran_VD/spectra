//package org.mysys.repository.partner;
//
//import java.util.List;
//
//import org.mysys.model.partner.ServiceMst;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface ServiceMstRepo extends JpaRepository<ServiceMst, Long> {
//
//	public ServiceMst findBySmId(long smId);
//
//	public void deleteBySmId(long smId);
//	
//	public List<ServiceMst> findBySmIdAndSiteid(long smId,long siteid);
//	
//	public List<ServiceMst> findByServiceNameAndSiteid(String serviceName,long siteid);
//
//}
