//package org.mysys.repository.partner;
//
//import java.util.List;
//
//import org.mysys.model.partner.AreaVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface AreaVwRepo extends JpaRepository<AreaVw, Long> {
//	
//	public List<AreaVw> findByIsActive(String isActive);
//
//	public List<AreaVw> findByIsActiveAndCityid(String isActive, Long cityid);
//
//}
