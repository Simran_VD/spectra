//package org.mysys.repository.partner;
//
//import org.mysys.model.partner.PartnerTeam;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//
//@Repository
//public interface PartnerTeamRepo extends JpaRepository<PartnerTeam, Long> {
//	
//	int deleteByPtId(long ptId);
//	
//	PartnerTeam findByPtId(long ptId);
//	
//	@Modifying
//	@Query("update PartnerTeam set adharImage =:adharByte where ptId=:ptId")
//	public int updateAdharPartnerTeamDocs(@Param("adharByte") byte[] adharByte,@Param("ptId") Long ptId);
//	
//	@Modifying
//	@Query("Update PartnerTeam set adharImage = NULL where ptId=:ptId")
//	public int deleteAdharPartnerDocs(@Param("ptId") Long ptId);
//
//}
