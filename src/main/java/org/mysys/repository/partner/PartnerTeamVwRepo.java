//package org.mysys.repository.partner;
//
//import java.util.List;
//
//import org.mysys.model.PartnerTeamVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface PartnerTeamVwRepo extends JpaRepository<PartnerTeamVw, Long>{
//
//	public List<PartnerTeamVw> findByPmId(Long pmId);
//
//}
