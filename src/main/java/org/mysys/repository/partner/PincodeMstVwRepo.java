//package org.mysys.repository.partner;
//
//import java.util.List;
//
//import org.mysys.model.partner.PincodeMstVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//
//@Repository
//public interface PincodeMstVwRepo extends JpaRepository<PincodeMstVw, Long> {
//	
//	List<PincodeMstVw> findByCityId(long cityId);
//
//}
