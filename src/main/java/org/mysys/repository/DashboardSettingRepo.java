package org.mysys.repository;

import org.mysys.model.DashboardSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardSettingRepo extends JpaRepository<DashboardSetting, String>{

}
