package org.mysys.repository;

import java.util.List;

import org.mysys.model.ContactVo;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactVoRepo extends JpaRepository<ContactVo, Long> {
	public List<ContactVo> findAll(Sort sort);
	
	public List<ContactVo> findByContactroleidAndSiteid(@Param("contactroleid")long contactroleid, Sort sort,@Param("siteid") long siteid);
}
