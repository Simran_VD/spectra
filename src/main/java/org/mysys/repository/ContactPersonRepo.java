package org.mysys.repository;

import org.mysys.model.Contact;
import org.mysys.model.Contactperson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactPersonRepo extends JpaRepository<Contactperson, Long> {
	
	public void deleteByContact(Contact contact);

}
