package org.mysys.repository;

import java.util.List;

import org.mysys.model.Screen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ScreenRepo extends JpaRepository<Screen, Long> {

	//public List<Screen> findByActiveAndSiteid(String active, long siteid);
	public List<Screen> findByActiveAndSiteidOrderByScreenname(String active, long siteid);
	
	@Query("select screenid from Screen")
	public List<Screen> getScId();
	
}
