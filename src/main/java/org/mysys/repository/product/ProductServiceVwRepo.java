//package org.mysys.repository.product;
//
//import java.util.List;
//
//import org.mysys.model.product.ProductServiceVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//public interface ProductServiceVwRepo extends JpaRepository<ProductServiceVw, Long> {
//	
//	public List<ProductServiceVw> findByPrdId(long prdId);
//
//	public List<ProductServiceVw> findByPrdIdAndIsActive(long prdId,String status);
//
//	public ProductServiceVw findByIsActiveAndSmId(String status, Long smId);
//
//	public List<ProductServiceVw> findByIsActive(String status);
//
//	public ProductServiceVw findByIsActiveAndSmIdAndPrdId(String status, Long smId, long prdId);
//
//	public ProductServiceVw findByPsmId(Long psmId);
//}
