//package org.mysys.repository.product;
//
//import java.util.List;
//
//import org.mysys.model.product.OurServices;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//
//@Repository
//public interface OurServicesRepo extends JpaRepository<OurServices, Long> {
//
//	List<OurServices> findByIsActive(String isActive);
//
//}
