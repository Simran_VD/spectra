//package org.mysys.repository.product;
//
//import java.util.List;
//
//import org.mysys.model.product.ProductMstVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface ProductMstVwRepo extends JpaRepository<ProductMstVw, Long> {
//
//	List<ProductMstVw> findByIsActive(String  c);
//
//	ProductMstVw findByPrdId(Long prdId);
//
//}
