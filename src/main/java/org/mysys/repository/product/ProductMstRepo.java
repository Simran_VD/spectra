//package org.mysys.repository.product;
//
//import java.util.List;
//
//import org.mysys.model.product.ProductMst;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface ProductMstRepo extends JpaRepository<ProductMst, Long> {
//
//	List<ProductMst> findByProdName(String prodName);
//
//	ProductMst findByPrdIdAndSiteId(long prdId, long loggedInUserSiteId);
//
//	void deleteByPrdId(long prdId);
//}
