//package org.mysys.repository.product;
//
//import java.util.List;
//
//import org.mysys.model.product.ProductPartMst;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface ProductPartMstRepo extends JpaRepository<ProductPartMst, Long> {
//
//	public ProductPartMst findByPpId(long ppId);
//	
//	public List<ProductPartMst> findByPpIdAndSiteId(long ppId, long loggedInUserSiteId);
//
//    public void deleteByPpId(long ppId);
//
//}
