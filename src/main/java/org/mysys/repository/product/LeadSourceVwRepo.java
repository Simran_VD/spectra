//package org.mysys.repository.product;
//
//import java.util.List;
//
//import org.mysys.model.product.LeadSourceVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface LeadSourceVwRepo extends JpaRepository<LeadSourceVw, Long>{
//
//	List<LeadSourceVw> findByUserid(long loggedInUserUserId);
//
//}
