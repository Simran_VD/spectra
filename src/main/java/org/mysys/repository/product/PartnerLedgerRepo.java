//package org.mysys.repository.product;
//
//
//import org.mysys.model.product.PartnerLedger;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface PartnerLedgerRepo extends JpaRepository<PartnerLedger, Long>{
//	
//	PartnerLedger findByPlId(long plId);
//}
