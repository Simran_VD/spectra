package org.mysys.repository.product;

import java.util.List;

import org.mysys.model.Service.ServiceLeadVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceLeadVwRepo extends JpaRepository<ServiceLeadVw, Long>{
	
	ServiceLeadVw findBySlId(long slId);
	

	  @Query(value = "select sllv.*\r\n"
	  		+ "from serv_lead_list_vw sllv ,fnc_area_wise_lead_drill_down (:userId,:amId,:tmId) f\r\n"
	  		+ "where sllv.sl_id = f.sl_id;", nativeQuery = true)
	  List<ServiceLeadVw> findByUserIdAndAmIdAndTmId(@Param("userId") int userId,@Param("amId") int amId,@Param("tmId") int tmId);
	  
	  

	  @Query(value = "select sllv.*\r\n"
	  		+ "from serv_lead_list_vw sllv ,fnc_partner_wise_lead_drill_down (:userId,:pmId,:tmId) f\r\n"
	  		+ "where sllv.sl_id = f.sl_id;", nativeQuery = true)
	  List<ServiceLeadVw> findByUserIdAndPmIdAndTmId(@Param("userId") int userId,@Param("pmId") int pmId,@Param("tmId") int tmId);
	  

	  @Query(value = "select sllv.*\r\n"
		  		+ "from serv_lead_list_vw sllv ,fnc_lead_wise_drill_down (:userId,:statusId,:tmId) f\r\n"
		  		+ "where sllv.sl_id = f.sl_id;", nativeQuery = true)
		  List<ServiceLeadVw> findLeadWiseAndPmIdAndTmId(@Param("userId") int userId,@Param("statusId") int statusId,@Param("tmId") int tmId);
	
	  
	  @Query(value="select * from fnc_serv_list(:userId,:typeId)",nativeQuery = true)
		List<ServiceLeadVw> getServLeadByUserIdAndType(@Param("userId") int id,@Param("typeId") int typeId);

}
