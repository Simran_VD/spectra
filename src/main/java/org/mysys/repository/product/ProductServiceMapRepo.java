//package org.mysys.repository.product;
//
//import java.util.List;
//
//import org.mysys.model.product.ProductServiceMap;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface ProductServiceMapRepo extends JpaRepository<ProductServiceMap, Long> {
//	
//	ProductServiceMap findByPsmId(long psmId);
//
//	List<ProductServiceMap> findByPrdIdAndSmId(Long prdId, Long smId);
//
//	void deleteByPsmId(long psmId);
//
//}
