//package org.mysys.repository;
//
//import java.util.List;
//
//import org.mysys.model.PartnerToNotifyVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface PartnerToNotifyVwRepo extends JpaRepository<PartnerToNotifyVw, Long> {
//
//	List<PartnerToNotifyVw> findBySlId(Long slId);
//}
