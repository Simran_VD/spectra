package org.mysys.repository;

import org.mysys.model.job.UserTypeMstListVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeMstListVwRepo extends JpaRepository<UserTypeMstListVw, Long>{

}
