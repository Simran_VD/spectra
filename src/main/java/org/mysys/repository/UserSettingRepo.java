package org.mysys.repository;

import java.util.List;

import org.mysys.model.UserSetting;
import org.mysys.model.UserSettingPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSettingRepo extends JpaRepository<UserSetting, UserSettingPK> {
	
	public List<UserSetting> findById_LoginidAndId_Siteid(String loginid, long siteid);
}