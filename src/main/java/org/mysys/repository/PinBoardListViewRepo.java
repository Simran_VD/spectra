package org.mysys.repository;

import java.util.List;

import org.mysys.model.PinBoardListView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PinBoardListViewRepo extends JpaRepository<PinBoardListView, Integer>{

	List<PinBoardListView> findByAssignedtoAndSiteid(Long userId, long loggedInUserSiteId);
	
	/*
	 * @Query(value =
	 * "select * from pin_board_list_vw p WHERE p.assigned_to = ?",nativeQuery =
	 * true) List<PinBoardListView> fin(int assigned_to, long siteid);
	 */
}
