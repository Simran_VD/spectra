package org.mysys.repository;

import java.util.List;

import org.mysys.model.job.ClientSpoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClientSpocRepo extends JpaRepository<ClientSpoc, Long>{
	
	@Query(value = "select c from ClientSpoc c where c.clId = :clId")
	public List<ClientSpoc> getallSpocData(@Param("clId")Long clId);

	
}
