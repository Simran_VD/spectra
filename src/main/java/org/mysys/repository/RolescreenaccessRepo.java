package org.mysys.repository;

import org.mysys.model.Rolescreenaccess;
import org.mysys.model.RolescreenaccessPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolescreenaccessRepo extends JpaRepository<Rolescreenaccess, RolescreenaccessPK> {
	
	void deleteById_Roleid(long id);

}
