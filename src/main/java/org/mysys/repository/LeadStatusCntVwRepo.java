package org.mysys.repository;

import java.util.List;

import org.mysys.model.AreaWiseCommVw;
import org.mysys.model.LeadStatusCntVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LeadStatusCntVwRepo extends JpaRepository<LeadStatusCntVw, Long>{
 
	@Query(value="select * from fnc_dashboard_kpi_lead_status_cnt(:userId)",nativeQuery = true)
	List<LeadStatusCntVw> findByUserId(@Param("userId") int id);

}
