package org.mysys.repository;

import java.util.List;

import org.mysys.model.AreaWiseCommVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AreaWiseCommVwRepo extends JpaRepository<AreaWiseCommVw, Long> {

	/*
	 * @Query(value="select * from fnc_dashboard_kpi_area_wise_comm(:userId)"
	 * ,nativeQuery = true) List<AreaWiseCommVw> findByUserId(@Param("userId") int
	 * id);
	 */

}
