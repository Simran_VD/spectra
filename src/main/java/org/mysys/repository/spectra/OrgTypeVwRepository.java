package org.mysys.repository.spectra;

import java.util.List;

import org.mysys.model.spectra.OrgTypeVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrgTypeVwRepository extends JpaRepository<OrgTypeVw,Long>{
	
	@Query("select n from OrgTypeVw n")
	public List<OrgTypeVw> getOrgTypeDropDownVw();

}
