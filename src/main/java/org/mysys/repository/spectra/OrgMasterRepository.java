package org.mysys.repository.spectra;

import org.mysys.model.spectra.OrgMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrgMasterRepository extends JpaRepository<OrgMaster, Long>{

}
