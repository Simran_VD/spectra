package org.mysys.repository.spectra;

import java.util.List;

import org.mysys.model.spectra.OrgParentDropDownVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrgParentDropDownVwRepository extends JpaRepository<OrgParentDropDownVw, Long>{
	@Query(value="select * from org_parent_drop_down_vw o where org_id =:org_id order by org_id asc",nativeQuery = true)
	public List<OrgParentDropDownVw> getOrgCorpParentDropDownVw(@Param("org_id") long org_id);
	
}
 