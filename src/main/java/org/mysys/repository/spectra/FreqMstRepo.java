package org.mysys.repository.spectra;

import org.mysys.model.spectra.FreqMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FreqMstRepo extends JpaRepository<FreqMst, Long> {

	public FreqMst findByFmId(long fmId);

	@Query(value="select SIMILARITY(fm.freq_name,:freqName) from freq_mst fm  where fm_id!=:fmId and SIMILARITY(fm.freq_name,:freqName)=1;",nativeQuery = true)
	public Integer findduplicateByFreqNameonEdit(@Param("freqName") String freqName,@Param("fmId") long fmId);

	@Query(value="select SIMILARITY(fm.freq_name,:freqName) from freq_mst fm  where SIMILARITY(fm.freq_name,:freqName)=1;",nativeQuery = true)
	public Integer findduplicateByFreqName(@Param("freqName") String freqName);

}
