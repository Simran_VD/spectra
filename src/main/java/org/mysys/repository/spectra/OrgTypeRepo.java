package org.mysys.repository.spectra;

import org.mysys.model.spectra.OrgType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrgTypeRepo extends JpaRepository<OrgType, Long>{

	public OrgType findByOtId(long otId);

	@Query(value="select SIMILARITY(ot.org_type,:orgType) from org_type ot  where ot_id!=:otId and SIMILARITY(ot.org_type,:orgType)=1;",nativeQuery = true)
	public Integer findduplicateByOrgTypeonEdit(@Param("orgType") String orgType,@Param("otId") long otId);

	@Query(value="select SIMILARITY(ot.org_type,:orgType) from org_type ot  where SIMILARITY(ot.org_type,:orgType)=1;",nativeQuery = true)
	public Integer findduplicateByOrgType(@Param("orgType") String orgType);

}
