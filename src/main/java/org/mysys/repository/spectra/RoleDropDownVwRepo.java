package org.mysys.repository.spectra;

import org.mysys.model.spectra.RoleDropDownVw;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDropDownVwRepo extends JpaRepository<RoleDropDownVw, Long>{

}
