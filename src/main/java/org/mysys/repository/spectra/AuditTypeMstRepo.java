package org.mysys.repository.spectra;

import org.mysys.model.spectra.AuditTypeMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AuditTypeMstRepo extends JpaRepository<AuditTypeMst, Long>{

	public AuditTypeMst findByAtmId(long atmId);
	
	@Query(value="select SIMILARITY(atm.audit_name,:auditName) from audit_type_mst atm  where atm_id!=:atmId and SIMILARITY(atm.audit_name,:auditName)=1;",nativeQuery = true)
	public Integer findduplicateByAuditNameonEdit(@Param("auditName") String auditName, @Param("atmId") long atmId);

	@Query(value="select SIMILARITY(atm.audit_name,:auditName) from audit_type_mst atm  where SIMILARITY(atm.audit_name,:auditName)=1;",nativeQuery = true)
	public Integer findduplicateByAuditName(@Param("auditName") String auditName);

}
