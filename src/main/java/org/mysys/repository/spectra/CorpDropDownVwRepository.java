package org.mysys.repository.spectra;

import java.util.List;

import org.mysys.model.spectra.CorpDropDownVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CorpDropDownVwRepository extends JpaRepository<CorpDropDownVw,Long>{
	@Query(value ="select * from corp_drop_down_vw order by org_id asc",nativeQuery = true)
	public List<CorpDropDownVw> getOrgCorpDropDownVw();
}
