package org.mysys.repository;

import org.mysys.model.DbSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DbSettingRepo extends JpaRepository<DbSetting, String> {

}
