package org.mysys.repository;

import org.mysys.model.UserSessionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSessionLogRepo extends JpaRepository<UserSessionLog, Long> {
	
	 @Procedure(name = "get_active_session")
	 public Integer func(@Param("userid") long userid,@Param("sessionId") String sessionId);
}
