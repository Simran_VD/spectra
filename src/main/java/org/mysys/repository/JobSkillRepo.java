package org.mysys.repository;

import org.mysys.model.job.JobSkill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JobSkillRepo extends JpaRepository<JobSkill, Long>{
	
	public void deleteByJmId(long jmId);
	
	@Modifying
	@Query("update JobSkill j set j.isActive = 'N' where j.jmId = :jmId")
	void updateByJmId(@Param("jmId")Long jmId);

	@Modifying(clearAutomatically = true)
	@Query(value = "delete from job_skill where jm_id=:id", nativeQuery = true)
	public void deleteByJobMasterId(@Param("id")Long jmId);
}
