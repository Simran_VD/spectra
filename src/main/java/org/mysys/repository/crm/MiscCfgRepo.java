package org.mysys.repository.crm;

import java.util.List;

import org.mysys.model.MiscCfg;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MiscCfgRepo  extends JpaRepository<MiscCfg, Long>  {
	
	public List<MiscCfg> findByCfgId(long cfgId);
	
	public void deleteByCfgId(long cfgId);
	
	public List<MiscCfg> findByCfgKeyAndSiteid(String cfgKey, long siteid);

}
