package org.mysys.repository.crm;

import org.mysys.model.CompanySetup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CompanySetupRepo  extends JpaRepository<CompanySetup, Long>  {

	public CompanySetup findBySiteid(long siteid);

	@Modifying
	@Query("update CompanySetup c set c.cmpName = :cmpName,  c.displayName = :displayName, c.dealsIn = :dealsIn, c.address1 = :address1, c.address2 = :address2, c.address3 = :address3, " + 
			"c.cityid = :cityid, c.cityName = :cityName,  c.stateid = :stateid, c.stateName = :stateName, c.fullAddress = :fullAddress, c.zipCode = :zipCode, " + 
			"c.primaryPhone = :primaryPhone, c.secondoryPhone = :secondoryPhone, c.mobile = :mobile, c.email = :email, c.websiteUrl = :websiteUrl, " +
			"c.gstin = :gstin,c.accountName = :accountName,c.bankName = :bankName,c.branch = :branch ,"
			+ "c.accountType = :accountType,c.accountNo = :accountNo,c.ifscCode = :ifscCode where c.siteid = :siteid")
	public int updateCompanyInfo(@Param("cmpName") String cmpName, @Param("displayName") String displayName, @Param("dealsIn") String dealsIn,  @Param("address1") String address1, 
			@Param("address2") String address2, @Param("address3") String address3, @Param("cityid") long cityid, @Param("cityName") String cityName, 
			@Param("stateid") long stateid, @Param("stateName") String stateName, @Param("fullAddress") String fullAddress, @Param("zipCode") String zipCode, 
			@Param("primaryPhone") String primaryPhone, @Param("secondoryPhone") String secondoryPhone, @Param("mobile") String mobile, @Param("email") String email, 
			@Param("websiteUrl") String websiteUrl,	@Param("gstin") String gstin, @Param("siteid") long siteid,	
			@Param("accountName") String accountName,	@Param("bankName") String bankName,	@Param("branch") String branch,	
			@Param("accountType") String accountType,	@Param("accountNo") String accountNo,	@Param("ifscCode") String ifscCode);
}
