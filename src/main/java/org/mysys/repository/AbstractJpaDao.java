package org.mysys.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractJpaDao< T extends Serializable,L> {

	private Class< T > clazz;

	@PersistenceContext
	EntityManager entityManager;

	public void setClazz( Class< T > clazzToSet ) {
		this.clazz = clazzToSet;
	}

	public T findOne( L id ){
		return entityManager.find( clazz, id );
	}
	@SuppressWarnings("unchecked")
	public List< T > findAll(){
		return entityManager.createQuery( "from " + clazz.getName() )
				.getResultList();
	}

	public T save( T entity ){
		entityManager.persist( entity );
		return entity;
	}

	public void update( T entity ){
		entityManager.merge( entity );
	}

	public void delete( T entity ){
		entityManager.remove( entity );
	}
	public void deleteById( L entityId ){
		T entity = findOne( entityId );
		delete( entity );
	}
	@Transactional
	public <F> void deleteByField( F value,String fieldName ){
		List<T> entity = findAllByFieldValue(value,fieldName);
		for(T sentity : entity){
			delete(sentity);
		}
	}
	
	public <F> List<T> findAllByFieldValue(F value, String fieldName){
		TypedQuery<T> query = entityManager.createQuery( "from " + clazz.getName() +" c where c."+fieldName +"=:value",clazz);
		query.setParameter("value", value);
		return query.getResultList();
	}
	
	public List<T> saveAll(Iterable<T> entities){
		List<T> result = new ArrayList<T>();

		for (T entity : entities) {
			result.add(save(entity));
		}
		return result;
	}
	
}
