package org.mysys.repository;

import java.io.Serializable;
import java.util.List;

public interface IGenericDao<T extends Serializable,L> {
	
	T findOne(final L id);

	List<T> findAll();
	
	T save( T entity );
	
	List<T> saveAll(Iterable<T> list);

	void update( T entity );

	void delete(final T entity);

	void deleteById(final L entityId);
	
	public <F> void deleteByField( F value,String fieldName );
	
	public <F> List<T> findAllByFieldValue(F value, String fieldName);
	
	void setClazz( Class< T > clazzToSet );
}
