package org.mysys.repository;

import java.util.List;

import org.mysys.model.Contactcateg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactCategRepo extends JpaRepository<Contactcateg, Long> {

	public List<Contactcateg> findByCategorynameAndSiteid(@Param("categoryname") String categoryName,@Param("siteid") Long siteid);

	public  List<Contactcateg> findBySiteid(long siteid);
}
