package org.mysys.repository;

import java.util.List;

import org.mysys.model.Statemaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StateRepo extends JpaRepository<Statemaster, Long> {
	
//	@Query(value ="select * from statemaster sm where lower(sm.statename) like (lower(concat('%',:statename,'%')))",nativeQuery = true)
	@Query(value="select SIMILARITY(sm.statename,:statename) from statemaster sm  where SIMILARITY(sm.statename,:statename)=1;",nativeQuery = true)
	public Integer findduplicateByStatename(@Param("statename") String statename);
	
	public List<Statemaster> findByStateidGreaterThanAndIsactive(long stateid,String status);

	public Statemaster findByStateid(long stateid);

	@Query(value="select SIMILARITY(sm.statename,:statename) from statemaster sm  where stateid!=:stateid and SIMILARITY(sm.statename,:statename)=1;",nativeQuery = true)
	public Integer findduplicateByStatenameonEdit(@Param("statename") String statename,@Param("stateid") long stateid);
}
