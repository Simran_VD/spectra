package org.mysys.repository;

import java.util.List;

import org.mysys.model.job.SkillDomainMstDropDownVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SkillDomainMstDropDownVwRepo extends JpaRepository<SkillDomainMstDropDownVw, Long> {
	
	//@Query(value = "select c from SkillDomainMstDropDownVw c")
//	public List<SkillDomainMstDropDownVw> getAllSkillDomainName();

	public List<SkillDomainMstDropDownVw> findByIsActive(String isActive);

}
