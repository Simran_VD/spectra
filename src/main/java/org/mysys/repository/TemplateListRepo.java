package org.mysys.repository;

import java.util.List;

import org.mysys.model.TemplateList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateListRepo extends JpaRepository<TemplateList, Long> {

	@Query("select i from TemplateList as i where i.templateCategory = :templateCategory and i.templateGroup = :templateGroup and  i.siteid = :siteid")
	public List<TemplateList> getTemplates(@Param("templateCategory") long templateCategory, @Param("templateGroup") String templateGroup, @Param("siteid") long siteid);

	@Query("select i from TemplateList as i where i.tlId = :tlId")
	public TemplateList getTemplate(@Param("tlId") long tlId);	
}