package org.mysys.repository;

import java.util.List;

import org.mysys.model.CityMasterView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CityMasterViewRepo extends JpaRepository<CityMasterView, Long> {
	
	@Query(value = "select s from CityMasterView s")
	public List<CityMasterView> getAllCity();

}
