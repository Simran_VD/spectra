package org.mysys.repository;

import org.mysys.model.job.SkillGroupMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillGroupMstRepo extends JpaRepository<SkillGroupMst, Long>{

	public SkillGroupMst findBySgmId(Long sgmId);
	
	@Query(value="select SIMILARITY(sgm.skill_group_name,:skillGroupName) from skill_group_mst sgm  where SIMILARITY(sgm.skill_group_name,:skillGroupName)=1;",nativeQuery = true)
	public Integer findduplicateBySkillGroupName(@Param("skillGroupName") String skillGroupName);

	@Query(value="select SIMILARITY(sgm.skill_group_name,:skillGroupName) from skill_group_mst sgm  where sgm_id!=:sgmId and SIMILARITY(sgm.skill_group_name,:skillGroupName)=1;",nativeQuery = true)
	public Integer findduplicateBySkillGroupNameonEdit(@Param("skillGroupName") String skillGroupName,@Param("sgmId") long sgmId);

}
