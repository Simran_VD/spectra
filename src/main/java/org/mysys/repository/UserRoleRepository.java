package org.mysys.repository;

import org.mysys.model.Userrole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends JpaRepository<Userrole, Long> {

	public void deleteByUserid(long userid);
}
