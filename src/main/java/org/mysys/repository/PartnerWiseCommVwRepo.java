//package org.mysys.repository;
//
//import java.util.List;
//
//import org.mysys.model.PartnerWiseCommVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface PartnerWiseCommVwRepo extends JpaRepository<PartnerWiseCommVw, Long> {
//
//	@Query(value="select * from fnc_dashboard_kpi_partner_wise_lead(:userId)",nativeQuery = true)
//	List<PartnerWiseCommVw> findByUserId(@Param("userId") int id);
//	
//}
