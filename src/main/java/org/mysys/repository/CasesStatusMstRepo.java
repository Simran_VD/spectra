package org.mysys.repository;

import org.mysys.model.job.CasesStatusMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CasesStatusMstRepo extends JpaRepository<CasesStatusMst, Long>{

	@Query(value="select status from cases_status_mst csm  where csm_id=:csm_id", nativeQuery=true)
	String getCaseStatusById(@Param("csm_id") Long csmId);
	
}
