package org.mysys.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CommonRepo {
	private static final Logger LOGGER = LogManager.getLogger();

	@Autowired
	protected Environment env;
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List<Object[]> executeQuery(String query) {
		LOGGER.debug("Query={}", query);
		return em.createQuery(query).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> executeSQLQuery(String query) {
		LOGGER.debug("NativeQuery={}", query);
		return em.createNativeQuery(query).getResultList();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> executeNativeQuery(String query, Class<T> clazz) {
		Query q = em.createNativeQuery(query, clazz);
		return (List<T>) q.getResultList();
	}

	/**
	 * @return list of vendors
	 */
	public List<Object[]> findAllDistinctVendorList(long siteid) {
		List<Object[]> list = executeQuery("SELECT distinct contact.contactid, contact.displayname "
				+ "FROM Contact contact, Vendoritemmaster vim "
				+ "WHERE vim.contactid = contact.contactid and contact.siteid = " + siteid);
		return list;
	}

	/**
	 * @return list of item codes w.r.t. a vendor
	 */
	public List<Object[]> findAllItemCodeList(Integer contactId, long siteid) {
		List<Object[]> list = executeQuery(
				"SELECT distinct i.itemcode, i.itemdesc, i.itemid " + "FROM Itemmaster i, Vendoritemmaster v "
						+ "WHERE i.itemid = v.itemid AND v.contactid = " + contactId + "and i.siteid = " + siteid);
		return list;
	}

	// TBR
	/*
	 * private void display(List<Object[]> list) { for (Object[] result : list)
	 * { if (result == null) continue; LOGGER.info("Query result={}",
	 * Arrays.asList(result)); } }
	 */

	/**
	 * MEthod will be used to get max,count values
	 * 
	 * @param query
	 * @return
	 */
	public Object getSingleResult(String query) {
		query = query.replaceAll(":acctyear", env.getProperty("application.acctYear"));
		return em.createNativeQuery(query).getSingleResult();
	}

	public List<Object[]> getContactsFromVendoritemmaster(long siteid) {
		List<Object[]> list = executeQuery(
				"SELECT distinct v.contactid, c.displayname , c.addr1||', '||c.addr2||', '||c.addr3||', '||cm.cityname||', '|| sm.statename AS Address, "
						+ "c.mobile, c.phone, c.email "
						+ "FROM Vendoritemmaster v, Contact c, Citymaster cm, Statemaster sm "
						+ "WHERE v.contactid = c.contactid AND c.cityid = cm.cityid AND c.stateid = sm.stateid AND c.siteid = "
						+ siteid);
		return list;
	}

	public List<Object[]> getVendorProducts(Long vendorId) {
		StringBuffer sb = new StringBuffer(
				"select distinct v.contactid,b.productid,i.itemcode,i.itemdesc, cu.unitname, v.jobrate, i.itemid ");
		sb.append(" FROM Bom b,Vendoritemmaster v, Itemmaster i, Itemunitmaster cu ");
		sb.append(" WHERE b.productid = i.itemid ");
		sb.append(" AND   b.productid = v.itemid ");
		sb.append(" AND   i.conversionunitid = cu.unitid ");
		sb.append(" AND v.contactid = ");
		sb.append(vendorId);
		sb.append(" AND now() >= v.fromdate ");
		sb.append(" AND COALESCE(v.todate, now()) >= now() ");
		sb.append(" order by i.itemcode ");

		LOGGER.info("Query=[{}]", sb);
		List<Object[]> list = executeQuery(sb.toString());
		LOGGER.info("list={}", list);
		return list;
	}

	public List<Object[]> getVendorProductBoms(Long vendorId, Long productId) {
		StringBuffer sb = new StringBuffer(
				"select v.contactid,b.bomId,b.bomno,b.productid,i.itemcode,i.itemdesc, cu.unitname, v.jobrate, i.itemid ");
		sb.append(" FROM Bom b,Vendoritemmaster v, Itemmaster i, Itemunitmaster cu ");
		sb.append(" WHERE b.productid = i.itemid ");
		sb.append(" AND   b.productid = v.itemid ");
		sb.append(" AND   i.conversionunitid = cu.unitid ");
		sb.append(" AND   v.contactid = ");
		sb.append(vendorId);
		sb.append(" AND   b.productid = ");
		sb.append(productId);
		sb.append(" AND now() >= v.fromdate ");
		sb.append(" AND COALESCE(v.todate, now()) >= now() ");
		sb.append(" order by i.itemcode ");

		LOGGER.info("Query=[{}]", sb);
		List<Object[]> list = executeQuery(sb.toString());
		LOGGER.info("list={}", list);
		return list;
	}

	public List<Object[]> getVendorItems(Long vendorId) {
		StringBuffer sb = new StringBuffer(
				"select v.contactid,v.itemid,i.itemcode,i.itemdesc, cu.unitname, v.jobrate ");
		sb.append(" FROM  Vendoritemmaster v, Itemmaster i, Itemunitmaster cu ");
		sb.append(" WHERE i.itemid = v.itemid ");
		sb.append(" AND   i.conversionunitid = cu.unitid ");
		sb.append(" AND   v.contactid = ");
		sb.append(vendorId);
		sb.append(" AND now() >= v.fromdate ");
		sb.append(" AND COALESCE(v.todate, now()) >= now() ");
		sb.append(" order by 1,4 ");

		LOGGER.info("Query=[{}]", sb);
		List<Object[]> list = executeQuery(sb.toString());
		LOGGER.info("list={}", list);
		return list;
	}

	public List<Object[]> getBomItemsWithItemDesc(long bomId) {
		StringBuffer sb = new StringBuffer(
				"SELECT im.itemcode,im.itemdesc, bi.id.bomId, bi.id.itemid, bi.itemqty, bi.itemvalue, bi.rate, iu.unitname ");
		sb.append(" FROM Bomitem bi, Itemmaster im, Itemunitmaster iu ");
		sb.append(" WHERE im.itemid = bi.id.itemid ");
		sb.append(" AND im.conversionunitid = iu.unitid ");
		sb.append(" AND bi.id.bomId = ");
		sb.append(bomId);
		sb.append(" ORDER BY itemdesc ASC ");

		LOGGER.info("Query=[{}]", sb);
		List<Object[]> list = executeQuery(sb.toString());
		LOGGER.info("list={}", list);
		return list;
	}
	
	public List<Object[]> getItemSpecifications(long itemId) {
		StringBuffer sb = new StringBuffer("select s.id.itemspecid, sm.itemspecname from Itemspecifaction s, Itemspecmaster sm "
				+ "where s.id.itemspecid = sm.id and s.id.itemid = " + itemId + " order by sm.itemspecname");
		LOGGER.info("Query=[{}]", sb);
		List<Object[]> list = executeQuery(sb.toString());
		LOGGER.info("list={}", list);
		return list;
	}	

	/**
	 * To execute native query that return single row single column.
	 * 
	 * @param query
	 * @return
	 */
	public String executeSingleRowColQuery(String query) {
		String barcodeStr = null;
		List<Object[]> list = executeSQLQuery(query);
		LOGGER.info("list={}", list);
		
		if(list != null && list.size() > 0) {
			barcodeStr = String.valueOf(list.get(0));
		}
		return barcodeStr;
	}
}
