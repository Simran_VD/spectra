package org.mysys.repository;


import org.mysys.model.job.SkillMst;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillMstRepo extends JpaRepository<SkillMst, Long>{
	

	public SkillMst findBySmId(Long smId);

	@Query(value="select SIMILARITY(sm.skill_name,:skillName) from skill_mst sm  where SIMILARITY(sm.skill_name,:skillName)=1;",nativeQuery = true)
	public Integer findduplicateBySkillName(@Param("skillName") String skillName);

	@Query(value="select SIMILARITY(sm.skill_name,:skillName) from skill_mst sm  where sm_id!=:smId and SIMILARITY(sm.skill_name,:skillName)=1;",nativeQuery = true)
	public Integer findduplicateBySkillNameonEdit(@Param("skillName") String skillName,@Param("smId") long smId);

}
