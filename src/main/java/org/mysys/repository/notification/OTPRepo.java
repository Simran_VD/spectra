package org.mysys.repository.notification;

import org.mysys.model.notification.OTP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OTPRepo extends JpaRepository<OTP, Long>{

	OTP findByPhone(String mobile);
	
	void deleteByPhone(String mobile);

}
