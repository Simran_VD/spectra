package org.mysys.repository.notification;

import java.util.Date;
import java.util.List;

import org.mysys.model.notification.NotifyListVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotifyListVwRepo extends JpaRepository<NotifyListVw, Long> {

	List<NotifyListVw> findByRecUserId(long loggedInUserUserId);

	//List<NotifyListVw> findByMobileUpdDt(Date d);

}	

