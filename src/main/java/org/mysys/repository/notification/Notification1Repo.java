package org.mysys.repository.notification;

import org.mysys.model.notification.Notification1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Notification1Repo extends JpaRepository<Notification1, Long> {
	/*
	 * @Modifying
	 * 
	 * @Query("Update Notification n set n.bellDt =:bellDt where ntId in :ids and siteid =:siteid"
	 * ) public Integer updateBellAt(@Param("bellDt") Date date,@Param("ids")
	 * List<Long> ids,@Param("siteid") Long siteid);
	 * 
	 * @Modifying
	 * 
	 * @Query("Update Notification n set n.seenDt =:seenDt where ntId= :id and siteid =:siteid"
	 * ) public int updateSeenAt(@Param("seenDt")Date seenDt,@Param("id") Long
	 * id,@Param("siteid") Long siteid);
	 * 
	 * @Modifying
	 * 
	 * @Query("Update Notification n set n.notifiedDt =:notifiedDt where ntId in :ids and siteid =:siteid"
	 * ) public Integer updateNotifyAt(@Param("notifiedDt") Date date,@Param("ids")
	 * List<Long> ids,@Param("siteid") Long siteid);
	 * 
	 * public void deleteBySlId(long slId);
	 */
	
}
