package org.mysys.repository.notification;

import java.util.List;

import org.mysys.model.notification.ServiceLeadNotifyVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceLeadNotifyVwRepo extends JpaRepository<ServiceLeadNotifyVw, Long> {

	List<ServiceLeadNotifyVw> findByUserId(long loggedInUserLoinId);

}
