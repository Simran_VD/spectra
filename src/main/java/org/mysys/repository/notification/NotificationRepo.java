package org.mysys.repository.notification;

import java.util.Date;
import java.util.List;

import org.mysys.model.notification.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepo extends JpaRepository<Notification, Long> {
	
	@Modifying
	@Query("Update Notification n set n.bellDt =:bellDt where ntId in :ids and siteid =:siteid")
	public Integer updateBellAt(@Param("bellDt") Date date,@Param("ids") List<Long> ids,@Param("siteid") Long siteid);
	
	@Modifying
	@Query("Update Notification n set n.seenDt =:seenDt where ntId= :id and siteid =:siteid")
	public int updateSeenAt(@Param("seenDt")Date seenDt,@Param("id") Long id,@Param("siteid") Long siteid);

	@Modifying
	@Query("Update Notification n set n.notifiedDt =:notifiedDt where ntId in :ids and siteid =:siteid")
	public Integer updateNotifyAt(@Param("notifiedDt") Date date,@Param("ids") List<Long> ids,@Param("siteid") Long siteid);
	
	@Modifying
	@Query("Update Notification n set n.mobileUpdDt =:mobileUpdDt where ntId in :ids and siteid =:siteid")
	public Integer updateNotifyAtMobile(@Param("mobileUpdDt") Date date,@Param("ids") List<Long> ids,@Param("siteid") Long siteid);

	@Modifying
	@Query("Update Notification n set n.mobieSeenDt =:mobieSeenDt where ntId in :ids and siteid =:siteid")
	public Integer updateSeenAtMobile(@Param("mobieSeenDt") Date date,@Param("ids") List<Long> ids,@Param("siteid") Long siteid);
	
	public void deleteBySlId(long slId);
	
}
