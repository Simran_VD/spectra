//package org.mysys.repository;
//
//import java.util.List;
//
//import org.mysys.model.LeadStatusCntVw;
//import org.mysys.model.PartnerWiseLeadCntVw;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface PartnerWiseLeadCntVwRepo extends JpaRepository<PartnerWiseLeadCntVw, Long> {
//  
//	@Query(value="select * from fnc_dashboard_kpi_partner_wise_comm(:userId)",nativeQuery = true)
//	List<PartnerWiseLeadCntVw> findByUserId(@Param("userId") int id);
//
//}
