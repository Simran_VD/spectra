package org.mysys.repository.Service;

import org.mysys.model.Service.ServiceLead1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceLead1Repo extends JpaRepository<ServiceLead1, Long>{

}
