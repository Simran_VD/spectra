package org.mysys.repository.Service;

import java.util.List;

import org.mysys.model.Service.ServliceLeadMapVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServliceLeadMapVwRepo extends JpaRepository<ServliceLeadMapVw, Long>{

	public List<ServliceLeadMapVw> findByStatus(String status);

	public List<ServliceLeadMapVw> findByPmId(long pmId);
}
