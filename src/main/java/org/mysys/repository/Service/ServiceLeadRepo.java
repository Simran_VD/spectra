package org.mysys.repository.Service;

import java.math.BigDecimal;
import java.util.Date;

import org.mysys.model.Service.ServiceLead;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceLeadRepo extends JpaRepository<ServiceLead, Long>{
	
   void deleteBySlId(long slId);

   ServiceLead findBySlId(Long slId);

   @Modifying
   @Query("Update ServiceLead set pmId =:uiid, lsId=:allocated where slId =:slId")
   int updatePmId(@Param("slId")long slId,@Param("uiid") Long uiid,@Param("allocated") Long allocated);
   
   @Modifying
   @Query(value="Update serv_lead set pm_id =:uiid, ls_id=:allocated where sl_id =:slId",nativeQuery=true)
   int updatePmIdNot(@Param("slId")long slId,@Param("uiid") Long uiid,@Param("allocated") Long allocated);
   
   @Modifying
   @Query(value="Update serv_lead set pm_id =:uiid where sl_id =:slId and pm_id is NULL",nativeQuery=true)
   int updatePmIdBySlId(@Param("slId")long slId,@Param("uiid") Long uiid);


   @Modifying
   @Query("Update ServiceLead set lsId =:lsId where slId =:slId")
   void updateLsId(@Param("lsId") Long lsId,@Param("slId")long slId);

   @Modifying
   @Query("Update ServiceLead set leadNotifyTime =:date where slId =:slId")
   void updateDate(@Param("date") Date date,@Param("slId") long slId);
   
   @Query(value=" select pm_id from serv_lead where sl_id =:slId for update",nativeQuery=true)
   void updatedForLock(@Param("slId")long slId);

   @Modifying
   @Query("Update ServiceLead set servStartTime =:start , servCompleteTime=:end, partnerRating=:rating, totalChargs=:totalCharge ,lsId=:lsId where slId =:slId")
   int markAsComplete(@Param("slId") long slId,@Param("start") Date start,@Param("end") Date end,@Param("rating") String rating,@Param("totalCharge") BigDecimal totalCharge,@Param("lsId") long lsId);

   @Modifying
   @Query("Update ServiceLead set ptId =:ptId where slId =:slId and pmId =:pmId")
   int updatePtIdBySlId(@Param("slId") long slId,@Param("pmId") long pmId,@Param("ptId") long ptId);
   
   
}
