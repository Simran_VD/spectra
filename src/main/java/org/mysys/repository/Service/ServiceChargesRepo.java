package org.mysys.repository.Service;

import java.util.List;

import org.mysys.model.Service.ServiceCharges;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceChargesRepo extends JpaRepository<ServiceCharges, Long> {

	 public ServiceCharges findByScId(long scId);

	 public void deleteByScId(long scId);

	 public List<ServiceCharges> findByScIdAndSiteId(Long scId, long loggedInUserSiteId);

}
