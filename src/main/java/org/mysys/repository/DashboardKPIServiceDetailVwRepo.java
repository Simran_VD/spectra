package org.mysys.repository;

import java.util.List;

import org.mysys.model.DashboardKPIServiceVw;
import org.mysys.model.Service.ServiceLeadVw;
import org.mysys.model.job.DashboardKPIServiceDetailVw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DashboardKPIServiceDetailVwRepo extends JpaRepository<DashboardKPIServiceDetailVw, Long> {
	
	
	  @Query(value="select * from fnc_kpi_job_opening_detail(:jm_id)",nativeQuery=
	  true) List<DashboardKPIServiceDetailVw> getDetailKPI(@Param("jm_id") Long jm_id);
}
