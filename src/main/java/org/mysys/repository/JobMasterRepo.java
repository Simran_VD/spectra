package org.mysys.repository;

import org.mysys.model.job.JobMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface JobMasterRepo extends JpaRepository<JobMaster, Long>{
	
	JobMaster findByjmId(Long jmId);
	
	// JobMaster findByJobCode(String jobCode);
	
	@Query(value="select SIMILARITY(jm.job_code,:jobCode) from job_mst jm  where jm_id!=:jmId and SIMILARITY(jm.job_code,:jobCode)=1;",nativeQuery = true)
	public Integer findduplicateByJobCodeonEdit(@Param("jobCode") String jobCode,@Param("jmId") Long jmId);

	@Query(value="select SIMILARITY(jm.job_code,:jobCode) from job_mst jm  where SIMILARITY(jm.job_code,:jobCode)=1;",nativeQuery = true)
	public Integer findduplicateByJobCode(@Param("jobCode")String jobCode);
	
	

}
