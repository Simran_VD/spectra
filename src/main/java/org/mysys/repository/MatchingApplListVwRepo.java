package org.mysys.repository;


import java.util.List;

import org.mysys.model.job.MatchingApplListVw;
import org.mysys.model.job.JobMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MatchingApplListVwRepo extends JpaRepository<MatchingApplListVw, Long> {
	
	@Query(value="select * from fnc_matching_appl_list(:jmId)",nativeQuery= true)
	List<MatchingApplListVw> getAllMatchingAppl(@Param("jmId") Integer jmId);

			
}
