package org.mysys.repository;

import org.mysys.model.Designation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DesignationRepo extends JpaRepository<Designation, Long> {
	
	public Designation findByDesignationidAndSiteid(long designationid, long siteid);

}
