package org.mysys.repository;

import java.util.List;

import org.mysys.model.job.JobStatusMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface JobStatusMasterRepo extends JpaRepository<JobStatusMaster, Long> {
	
	@Query(value = "select c from JobStatusMaster c")
	public List<JobStatusMaster> getAllStatus();

}
