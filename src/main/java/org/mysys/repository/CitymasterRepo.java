package org.mysys.repository;

import java.util.List;

import org.mysys.model.Citymaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CitymasterRepo extends JpaRepository<Citymaster, Long> {
	
	//	@Query(value ="select * from citymaster lm where lower(lm.cityName) like (lower(concat('%',:cityName,'%')))",nativeQuery = true)
	@Query(value="select SIMILARITY(cm.cityname,:cityName) from citymaster cm  where SIMILARITY(cm.cityname,:cityName)=1;",nativeQuery = true)           
	public Integer findduplicateByCityName(@Param("cityName") String cityName);
                
	@Query(value ="select * from citymaster lm where stateid=:stateid order by cityName asc",nativeQuery = true)
    public List<Citymaster> findByStatemaster_stateidOrderByCitynameAsc(@Param("stateid") long stateid);

	public List<Citymaster> findByIsactive(String status);

	@Query(value="select SIMILARITY(cm.cityname,:cityName) from citymaster cm  where cityid!=:cityId and SIMILARITY(cm.cityname,:cityName)=1;",nativeQuery = true)
	public Integer findduplicateByCityNameonEdit(@Param("cityName") String cityName,@Param("cityId") long cityId);

	public Citymaster findByCityId(long cityId);

}

