package org.mysys.repository;

import org.mysys.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepo extends JpaRepository<Department, Long> {
	
	public Department findByDeptnameAndSiteid(String deptname, long siteid);
}
