var pindispId  = 196;


$.fn.pinboardModal = function(){
	var resetTag = [];
	
	var assignednew = [];
	

	$("#mypinModalLabel").empty()
	$("#mypinModalLabel").html("Add New Task")

	$('#pinModal').find('input:hidden').val('');
	$('#pinModal').find('input:text').val('');
	$('#cmtInput').val('');
	
	$('#pinModal').find('input:hidden').attr("disabled",false)
	$('#pinModal').find('input:text').attr("disabled",false)
	$("#description").attr("disabled",false)
	$("#cmtInput").attr("disabled",false)
	$("#assignedTo").attr("disabled",false)
	$("#pinboardstatus").attr("disabled",false)
	$("#logDate").attr("disabled",true)
	$(this).populateAdvanceSelect($("#pinboardstatus"),resetTag,{"key":"userid","value":"displayname"});
	$('#pinModal').find('select').select2();
	$('#pinModal').find('#description').val('');
	
	$("#title").css("display","block")
	$("#lblTitle").css("display","block")
	$(".cmtBox").not('.cmtBox:last').remove();

	var FI = $("#pinModal")

	$("#pinModal").modal('show');
	
	
	//Current Date and Time
	$(this).cAjax('GET','./getStaticValues?keys=datetime', '', false, false, '', 'Error', true);
	logcurrDate =  $.parseJSON(strResponseText);
	FI.find("#logDate").val(logcurrDate["datetime"]);		
	
	$(this).cAjax('Post','./admin/getUserName', '', false, false, '', 'Error', true);
	var user = $.parseJSON(strResponseText);
	var loginUser = $("#crtby").val(user.loginid);
	
	$(this).cAjax('GET','./admin/getUserslistvwManager', '', false, false, '', 'Error', true);
    assign =  $.parseJSON(strResponseText)
    $(this).populateAdvanceSelect(FI.find("#assignedTo"),assign,{"key":"userid","value":"displayname"});
    var defaultUser = $("#userId").val();
	$("#assignedTo").val(defaultUser)
    $("#assignedTo").select2()
    $("#assignedTo option:first").remove()
    $("#pinboardstatus").select2()

    
    
    $("#pinboardstatus option:first").remove()
      var selUsrLen = $("#assignedTo").next().find('.select2-selection__choice__remove').parent().length;
	var selUsrs = $("#assignedTo").next().find('.select2-selection__choice__remove').parent();
	for(var i = 0; i < selUsrLen; i++){
		if(selUsrs.eq(i).attr('title') == $('#userName').text()){
			selUsrs.eq(i).find('.select2-selection__choice__remove').remove();
		}
		
		
		
	}
    
	
}

$.fn.savepinModal = function(){
	event.preventDefault();
	var editchkpinStatus = false;
	var sendPinObj = [];
	
    var taggedValue=[] 
	
	var tag = []
    
	var savePin = [];
	
	var editsendPinObj = [];
	
	var editsavejson = [];
	
	var editcmtArray = [];
	
	var FI= $(this).parents('.modal-content').find('form');
	var pJson = (serializeJSONIncludingDisabledFields(FI));
	
	var titleMandatory = $("#title").val().trim();
	if(titleMandatory == ""||titleMandatory == null){
		swal("Please Fill Title")
		return false;
	}
	//To save data on edit 
	if(FI.find("#ilId").val() != 0){
		var editsavejson = new Object();
		editsavejson.ilId = FI.find("#ilId").val()
		editsavejson.moduleId = -1;
		editsavejson.screenId = -1;
		editsavejson.type = "Pinboard";
		
		var editpinAssigned = FI.find("#assignedTo").val();
		var editlogDate =FI.find("#logDate").val()
		
		var editTagMembers = FI.find("#pinboardstatus").val()
		for(var p=0;p<editTagMembers.length;p++){
			taggedValue.push(editTagMembers[p])
		}
		
		var editDisable = $('#pinboardstatus option[disabled]:selected').val();
		taggedValue.push(editDisable)
		
		

			for(var j = 0; j<editpinAssigned.length;j++ ){
				editpinObj = new Object();
				if(FI.find("#assignedTo").find('option:selected').eq(j).data()['ildId']){
					editpinObj.ildId = FI.find("#assignedTo").find('option:selected').eq(j).data()['ildId']
				}
				else{
					editpinObj.ildId = 0;
				}
                 
				taggedValue.some(function(item){
               	  if(item === editpinAssigned[j]){
               	  	editpinObj.pinboardstatus = 1
               	    editchkpinStatus=true
               	  }
               })
				editpinObj.logDate = editlogDate
				editpinObj.title = FI.find("#title").val()
				editpinObj.assignedTo = editpinAssigned[j]
				editpinObj.description = FI.find("#description").val()
				editpinObj.statusId = -1;

				editpinObj.pinboardstatus = editchkpinStatus?1:0;
				editpinObj.isActive = 'Y';
				editsendPinObj.push(editpinObj)
				editchkpinStatus =false;
			}
			
		
		
		
	   editsavejson['issueLogDtl'] = editsendPinObj;
	   var edit = JSON.stringify(editsavejson);
	   
	$(this).cAjaxPostJson('Post','./admin/saveIssueLogDtl',(edit), false, false, '', 'Error', true);
		
 		if(strResponseText) { 
		var pinRes = $.parseJSON(strResponseText);
			if(pinRes.ilId){
				
			swal("Saved!", "Pinboard saved successfully.", "success");
 				$(this).displayPinboard()
				$('#pinModal').modal('hide');
			}
		}
	   
		return false;
			
	}
	
	
	//To save data on Add new 
	var chkpinStatus = false;
	var newTag = [];
	var savejson = new Object();
	savejson.ilId = 0;
	savejson.moduleId = -1;
	savejson.screenId = -1;
	savejson.type = "Pinboard";
	
	var pinAssigned = FI.find("#assignedTo").val();
	var logDate = FI.find("#logDate").val()
	
	var tagMembers = FI.find("#pinboardstatus").val()

				for(var i = 0; i<pinAssigned.length;i++ ){
					pinObj = new Object();
					
					tagMembers.some(function(item){
		               	  if(item === pinAssigned[i]){
		               		  chkpinStatus=true
		               	  }
		               })
					
					pinObj.ildId = 0;
					pinObj.logDate = logDate;
					pinObj.title = FI.find("#title").val()
					pinObj.assignedTo = pinAssigned[i]
					pinObj.description = FI.find("#description").val()
					pinObj.statusId = -1;
					pinObj.pinboardstatus = chkpinStatus?1:0 
					sendPinObj.push(pinObj)
					chkpinStatus = false;
			}
	
	

	savejson['issueLogDtl'] = sendPinObj;
   var pinJson = JSON.stringify(savejson);
   if(navigator.onLine == false){
		swal("Warning", "You're offline. Check your connection.", "warning");
		return false;
   }
	$(this).cAjaxPostJson('Post','./admin/saveIssueLogDtl',(pinJson), false, false, '', 'Error', true);
	
	if(strResponseText) { 
	var pinRes = $.parseJSON(strResponseText);
		if(pinRes.ilId){
			
			
		}
	}
	
	if(FI.find("#ilId").val() == 0)
	{	
		cmtDivsLen = FI.find(".cmt .comment").length;
		cmtArray = [];
		for(var i =0; i <cmtDivsLen; i++)
		{
			var currCmtDiv = FI.find(".cmt .comment").eq(i);
			
			cmtJson = new Object();
			cmtJson['ilId'] = pinRes.ilId
			
			cmtJson['commText'] = currCmtDiv.text();
			cmtJson['commUserId'] = $("#userId").val();
			cmtJson['ildcId'] = 0;
			cmtJson['stId'] = -1;
			cmtArray.push(cmtJson);

		}
		
		$(this).cAjaxPostJson('Post','./admin/saveIssueLogDtlComments',JSON.stringify(cmtArray), false, false, '', 'Error', true);
		swal("Saved!", "Pinboard saved successfully.", "success");
		$(this).autoScrollPinDiv()
		$(this).displayPinboard()
		
		$('#pinModal').modal('hide');
	}
	return false;
}


$.fn.sendDashboardCmt = function() 
{
	
	var cmtarr = [];
	

    
	var FI = $(this).parents('.modal-content').find('form');
	var currentCmt = FI.find("#cmtInput").val().trim();
	FI.find("#cmtInput").val("");
	var cmtBox = FI.find(".cmtBox:first").clone();
	FI.cAjax('GET','./getStaticValues?keys=datetime', '', false, false, '', 'Error', true);
	currDate =  $.parseJSON(strResponseText);
	
	
	
	
	if(currentCmt != "")
	{
		
		FI.find("#cmtInput").css("border-color", "black");
		cmtBox.find(".cmt").html('<span style="font-weight:bold;">[' + $("#userName").text()+ ' '+  currDate.datetime + '] :</span> <span class = "comment">'+ currentCmt+'</span>');
		//cmtBox.find(".cmt").text(currentCmt);
		//cmtBox.find("#senderNameDate").html($("#userName").text() + ' <span>'+  currDate.date + '</span>');
		
		cmtBox.css('display','');
		FI.find("#cmtContainer").find(".cmtBox:last").before(cmtBox);
		
		

		
		if(FI.find("#ilId").val() != 0)
		{
			cmtJson = new Object();
			cmtJson['ilId'] = FI.find("#ilId").val()
			cmtJson['stId'] =  -1;
			cmtJson['commText'] = currentCmt
			cmtJson['commUserId'] = $("#userId").val();
			cmtJson['ildcId'] = 0;
			cmtarr.push(cmtJson);
			$(this).cAjaxPostJson('Post','./admin/saveIssueLogDtlComments',JSON.stringify(cmtarr), false, false, '', 'Error', true);
			if(strResponseText == "")
			{				
				FI.find(".cmtBox:last").remove();
			}
		}
	}
	
	else
	{
		FI.find("#cmtInput").css("border-color", "coral");
	}
 
	
	
	
}


$.fn.displayPinboard = function(){
	var FI = $("#pinModal")
	//$("#pinModal").modal('show');
	var isStatus = false
	var ticketColor = "background-color: #ffeb3b"
	
	$(this).cAjax('GET','./admin/getPinBoardListView', '', false, false, '', 'Error', true);;
	var data= $.parseJSON(strResponseText);
	
	$(".pinboards").empty()
	$.each(data,function(){
		
		if(this.pinboardstatus  === "1" &&this.taskstatus == null ){
			var pinTask = `<div class="col-sm-3 py-2" id="column">
				<div id="closeBtn">
					<button type="button" class="close" data-toggle="tooltip" title="Close Task!"  id="closepinboard" onclick="$(this).closeTask()">X</button>
				</div>
					<div class="card card-body text-center h-100-p" id = "cardId" style="cursor:pointer;background-color: #ffeb3b" onclick="$(this).editPinboard()">
					<span id="hidespan" style="display:none">`+this.il_id+`</span>
			        <img src="./images/pin-icon.png" width="35px">
					<small>`+this.log_date+`</small>
					<h4>`+this.title+`</h4>
					<p>`+this.description+`</p>

				</div>
			</div>`
				$(".pinboards").append(pinTask)
				
				isStatus = true
				
		}
		
		 if(this.taskstatus  == 'Y'){
				var pinTask = `<div class="col-sm-3 py-2" id="column">
					<div id="closeBtn">
						<button type="button" class="close" data-toggle="tooltip" title="Close Task!"  id="closepinboard" disabled>X</button>
					</div>
					<div class="card card-body text-center h-100-p" id = "cardId" style="cursor:pointer;background-color: #4caf50d1;" onclick="$(this).editPinboard()">
					
						<span id="hidespan" style="display:none">`+this.il_id+`</span>
				        <img src="./images/pin-icon.png" width="35px">
						<small>`+this.log_date+`</small>
						<h4>`+this.title+`</h4>
						<p>`+this.description+`</p>

					</div>
				</div>`
					$(".pinboards").append(pinTask)
				
					$('#pinModal').find('input:hidden').attr("disabled",true)
					$('#pinModal').find('input:text').attr("disabled",true)
					}
		
		 else if(isStatus == false && this.taskstatus == null){
			   var pinTask = `<div class="col-sm-3 py-2" id="column">
				<div id="closeBtn">
					<button type="button" class="close" data-toggle="tooltip" title="Close Task!"  id="closepinboard" onclick="$(this).closeTask()">X</button>
				</div>
				<div class="card card-body text-center h-100-p" id = "cardId"style="cursor:pointer" onclick="$(this).editPinboard()">
					<span id="hidespan" style="display:none">`+this.il_id+`</span>
			        <img src="./images/pin-icon.png" width="35px">
					<small>`+this.log_date+`</small>
					<h4>`+this.title+`</h4>
					<p>`+this.description+`</p>

				</div>
			</div>`
				$(".pinboards").append(pinTask)
				

		}
		
	})
		
	
	
}


$.fn.editPinboard = function(){
	var FI = $(this).closest("#column");
	
	var ilId = FI.find("#hidespan").text()
	
	$(this).cAjax('GET','./admin/getIssueLog',"ilId="+ilId, false, false, '', 'Error', true);
	var issueLog = $.parseJSON(strResponseText);	

	
	$(this).cAjax('GET','./admin/getIssueLogDtlComments',"ilId="+ilId, false, false, '', 'Error', true);
	var comms = $.parseJSON(strResponseText);
	
	$("#pinModal").fillpinBoard(issueLog,comms)
	
	var divName = FI.find("#cardId").css('backgroundColor')
	if(divName  == "rgba(76, 175, 80, 0.82)"){
		$('#pinModal').find('input:hidden').attr("disabled",true)
		$('#pinModal').find('input:text').attr("disabled",true)
		$("#description").attr("disabled",true)
		$("#cmtInput").attr("disabled",true)
		$("#assignedTo").attr("disabled",true)
		$("#pinboardstatus").attr("disabled",true)
	}
	else{
		$('#pinModal').find('input:hidden').attr("disabled",false)
		$('#pinModal').find('input:text').attr("disabled",false)
		$("#description").attr("disabled",false)
		$("#cmtInput").attr("disabled",false)
		$("#assignedTo").attr("disabled",false)
		$("#pinboardstatus").attr("disabled",false)
		$("#logDate").attr("disabled",true)
	}
}

$.fn.fillpinBoard = function(issueLog,comms){
	var FI = $("#pinModal")
	

	$("#pinModal").modal('show');
	FI.find("#mypinModalLabel").empty()
	FI.find("#titleSpn").css("display","none")
	FI.find("#titleDesc").css("display","none")
	
	issueLogs = issueLog;
	issueLogDtl = issueLogs.issueLogDtl
	FI.find("#ilId").val(issueLogs.ilId)
	 FI.find("#crtby").val(issueLogs.createdby)

	
	var assigned = [];
	$(this).cAjax('GET','./admin/getUserslistvwManager', '', false, false, '', 'Error', true);
    assign =  $.parseJSON(strResponseText)
    $(this).populateAdvanceSelect(FI.find("#assignedTo"),assign,{"key":"userid","value":"displayname"});
    $(this).populateAdvanceSelect(FI.find("#pinboardstatus"),assign,{"key":"userid","value":"displayname"});
	for(var i =0;i<issueLogs.issueLogDtl.length;i++){
		//issueLogDtl = issueLogs.issueLogDtl[i]
		FI.find("#ildId").val(issueLogDtl[i].ildId)
		FI.find("#logDate").val(issueLogDtl[i].logDate)
		FI.find("#mypinModalLabel").html(issueLogDtl[i].title)
		FI.find("#title").val(issueLogDtl[i].title)
		FI.find("#description").val(issueLogDtl[i].description)
	    assigned.push(issueLogs.issueLogDtl[i].assignedTo)
	    
    }
	
	FI.find("#assignedTo").val(assigned)
	
	var selectedMembers = FI.find("#assignedTo").find('option:selected');
	FI.find("#assignedTo option:first").remove()
	for(var j = 0; j < selectedMembers.length; j++){
		FI.find("#assignedTo").find('option:selected').eq(j).data()['ildId'] = issueLogs.issueLogDtl[j].ildId
	}
	FI.find("#assignedTo").select2();
	FI.find("#assignedTo").trigger('change')
	
	var tag = [] 

	for(var i =0;i<issueLogs.issueLogDtl.length;i++){
		//issueLogDtl = issueLogs.issueLogDtl[i]
		if(issueLogs.issueLogDtl[i].pinboardstatus == '1'){
			tag.push(issueLogs.issueLogDtl[i].assignedTo)
		}
	    
    }
	FI.find("#pinboardstatus").val(tag)
	FI.find("#pinboardstatus").select2()
	//for comment data
	FI.find(".cmtBox").not('.cmtBox:last').remove();
	
	$.each(comms,function(){
		var cmtBox = FI.find(".cmtBox:first").clone();
		cmtBox.find(".cmt").html(this['commText']);
		cmtBox.css('display','');
		FI.find("#cmtContainer").find(".cmtBox:first").before(cmtBox);
	})
	
	var selUsrLen = FI.find("#assignedTo").next().find('.select2-selection__choice__remove').parent().length;
	var selUsrs = FI.find("#assignedTo").next().find('.select2-selection__choice__remove').parent();
	
	
	for(var i = 0; i < selUsrLen; i++){
		if(selUsrs.eq(i).attr('title') == $('#userName').text()){
			selUsrs.eq(i).find('.select2-selection__choice__remove').remove();
		}
		
		if(issueLog.createdby == $('#loginId').val()){
			
		}
		else{
			selUsrs.eq(i).find('.select2-selection__choice__remove').remove();
		}
	}
	$(this).teamIcon();
	
}

//for Auto scrolling in comment div
$.fn.autoScrollPinCmt = function(){
	 $("html, #cmtContainer").animate({ 
      scrollTop: $( 
        'html, #cmtContainer').get(0).scrollHeight 
  }, 2000); 
}



$.fn.checkTitleLength = function(){
	FI = $("#pinModal")
	var titleLength = FI.find("#title").val().length.toString()
	
	var checkLength = titleLength.match(/\S+/g)
		if(checkLength == 32){
			$("#title").css("border","2px solid red");
			$("#titleSpn").html("Max 32 characters are allowed")
				
		}
	
		else{
			$("#titleSpn").empty()
		}
	
}



$.fn.checkDescLength = function(){
	FI = $("#pinModal")
	var titleLength = FI.find("#description").val().split(' ').length.toString()
	
	var checkLength = titleLength.match(/\S+/g)
		if(checkLength > 100){
			$("#description").css("border","2px solid red");
			$("#titleDesc").html("Max limit 100 words")
	
			FI.find("#description").val(FI.find("#description").val().split( ' ' ).slice(0,100).join(" "))
			
		}
	
		else{
			$("#titleDesc").empty()
		}
	
}


//function for populating Team members in Tag column on onchange of Team dropdown

$.fn.teamMember = function(){
	FI = $("#pinModal")
	
	var loggedArr = [];
	
	var tagMembers = FI.find("#pinboardstatus").val();	
		for(t=0;t<tagMembers.length;t++){
			loggedArr.push(tagMembers[t])
		}
		
	var disableTag = $('#pinboardstatus option[disabled]:selected').val();	
	loggedArr.push(disableTag)
	var members = FI.find("#assignedTo").val();	
	var loggedinUser = $("#userId").val();
			
	var teamMembers = [];
		for(var i in members){
		 var memberid = members[i]
		    for(var j in assign){
		    	var memberUserId = assign[j].userid
		    	
		    	if(memberid == memberUserId){
		    		var chkMembers = assign[j]
		    		teamMembers.push(chkMembers)
		    	}
		    }
		}
		
		$(this).cAjax('GET','./admin/getUserslistvwManager', '', false, false, '', 'Error', true);
		assign =  $.parseJSON(strResponseText)
		$(this).populateAdvanceSelect23(FI.find("#pinboardstatus"),teamMembers,{"key":"userid","value":"displayname"},loggedinUser);
		$("#pinboardstatus").val(loggedArr)
		 
		
		
		var selUsrLen = $(this).next().find('.select2-selection__choice__remove').parent().length;
		var selUsrs = $(this).next().find('.select2-selection__choice__remove').parent();
		var existUsr = false;
		for(var i = 0; i < selUsrLen; i++){
			if(selUsrs.eq(i).attr('title') == $('#userName').text()){
				selUsrs.eq(i).find('.select2-selection__choice__remove').remove();
				existUsr = true;
			}
		}
		if(!existUsr){
			members.push($('#userId').val())
			
			FI.find("#assignedTo").val(members).select2().trigger('change');
		}
		
		$(this).find("div").next().removeClass("select2-search-choice-close");
		
}

$.fn.closeTask = function(){
	
	 var FI = $(this).closest("#column");
	 
		 var ilId = FI.find("#hidespan").text();
			$(this).cAjax('GET','./admin/updateRecordById', "ilId="+ilId, false, false, '', 'Error', true);
			Closed =  $.parseJSON(strResponseText)
			if(Closed == "1"){
				swal("Closed!", "Task Closed Successfully.", "success");
				$(this).displayPinboard()
				
				return false;
			}
	 
}


$.fn.teamIcon = function(){

	var ilId = $("#ilId").val()
	if(ilId == ""||ilId == null||ilId == undefined){
		return false;
	}
	
	
	
	$(this).cAjax('GET','./admin/getIssueLog',"ilId="+ilId, false, false, '', 'Error', true);
	var issueLog = $.parseJSON(strResponseText);	
	var loginUser = $("#crtby").val(issueLog.createdby)
	
	var selUsrLen = FI.find("#assignedTo").next().find('.select2-selection__choice__remove').parent().length;
	var selUsrs = FI.find("#assignedTo").next().find('.select2-selection__choice__remove').parent();
	
	
	for(var i = 0; i < selUsrLen; i++){
		
		
		if(loginUser.val() == $('#loginId').val()){
			
		}
		else{
			selUsrs.eq(i).find('.select2-selection__choice__remove').remove();
		}
	}
	 
}


//for Auto scrolling in pinboard div after save
$.fn.autoScrollPinDiv = function(){	
		  $("html, .pinboards").animate({ scrollTop: 0 }, "slow");
		  return false;
}