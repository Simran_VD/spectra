var userListId = 2;
var userGridOptions;
$.fn.getuserlist = function(){
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("UserListing",'./pages?name=/admin/userList',"Users");
};

$.fn.userListOnReady = function(){
	userGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',

		};

	$(this).reloadGrid("UserLst", userListId, userGridOptions);
	$("#userlistContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new"){
			
			
			try{
				var data = onSelectionChanged(userGridOptions)[0]['userid']
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
			var userName = onSelectionChanged(userGridOptions)[0]["Name"];
		}
		switch(operation) {
		case "add new":
			$(this).addTab("AddUser",'./pages?name=/admin/addEditUser',"Add User", {"action":"new"});
			break;
		case "edit":
			$(this).addTab("EditUser"+data,'./pages?name=/admin/addEditUser',"Edit User-"+data,{"data":data,"action":"edit"});
			break;
		case "newas":
			$(this).addTab("NewAsUser"+data,'./pages?name=/admin/addEditUser',"NewAs User-"+userName,{"data":data,"action":"newas"});
			break;
		case "view":
			$(this).addTab("ViewUser"+data,'./pages?name=/admin/addEditUser',"View User-"+data,{"data":data,"action":"view"});
			break;
		case "delete":
			swal({
				title: "Are you sure? want delete User?",
				text: "You will not be able to recover User now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false	,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if (isConfirm) {
                
			$(this).cAjax('Get','./admin/deleteUser',"id="+data, false, false, '', 'Error', true);
			if(strResponseText == "SUCCESS"){
				swal("Deleted!", "Selected user deleted successfully.", "success")
				$(this).reloadGrid("UserLst", userListId, userGridOptions);
			} else if(strResponseText == "DATA_INTEGRITY"||strResponseText == ""){
				swal("Error in deletion!", "Selected user is being used. So it cannot be deleted.", "error");
			}
				} else {
					swal("Cancelled", "Thank you! your user is safe :)", "error");
				}
			});
			break;
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});
	$("#UserLst").closest(".row").addClass("listContainer");
	$("#UserLst").closest('form').attr("oninput","$(this).onUseryFilterTextBoxChanged()");
};

$.fn.onUseryFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	userGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.prepareUserForm = function($FI) {
	var option = $FI.closest("div.active").data("data"),keyValue=option ? option.data : undefined;
	var action =option ? option.action : undefined;
	
	if(option.data){
		
		$FI.find("#radioDiv .statusActive").attr("id", "statusActive"+option.data)
		$FI.find("#radioDiv #statusActiveLabel").attr("for","statusActive"+option.data);
		$FI.find("#radioDiv .statusInactive").attr("id", "statusInactive"+option.data)
		$FI.find("#radioDiv #statusInactiveLable").attr("for","statusInactive"+option.data);
	}
	else{
		
		$FI.find("#radioDiv .statusActive").attr("id", "statusActive")
		$FI.find("#radioDiv #statusActiveLabel").attr("for","statusActive");
		$FI.find("#radioDiv .statusInactive").attr("id", "statusInactive")
		$FI.find("#radioDiv #statusInactiveLable").attr("for","statusInactive");
	}

	

	var $formContainer = $FI.find("#addUserFormContainer");
	var $container = $FI;
	
	
	$FI.cAjax('GET','./job/getAllState', '', false, false, '', 'Error', true);
	var stateJson= $.parseJSON(strResponseText); 
	$FI.populateAdvanceSelect($FI.find("#state"), stateJson, {"key":"stateId","value":"stateName"});
	$FI.find("#state").children().first().remove();
	$FI.find("#state").val("");
	
	
	$FI.cAjax('GET','./master/getUserTypeMstListVw', '', false, false, '', 'Error', true);
	var typeJson= $.parseJSON(strResponseText); 
	$FI.populateAdvanceSelect($FI.find("#typeId"), typeJson, {"key":"typeId","value":"userType"});
	$FI.find("#typeId").children().first().remove();
	$FI.find("#typeId").val("");
	if($FI.find("#typeId").val() == ""){
		$FI.find("#Group").children().first().attr("disabled",true);
	}
	
	$FI.cAjax('GET','./job/getUserManager', '', false, false, '', 'Error', true);
	var managerJson= $.parseJSON(strResponseText); 
	$FI.populateAdvanceSelect($FI.find("#mgrId"), managerJson, {"key":"userid","value":"displayname"});
	$FI.find("#mgrId").children().first().remove();
	$FI.find("#mgrId").val("");
	
	
	$FI.cAjax('GET','./admin/getRoleDropdown', '', false, false, '', 'Error', true);
	var roleJson= $.parseJSON(strResponseText); 
	$FI.populateAdvanceSelect($FI.find("#role"), roleJson, {"key":"id","value":"rolename"});
	$FI.find("#role").children().first().remove();
	$FI.find("#role").val("");
	
	$formContainer.find("#joindate").val(currentServerDate);
	if (action == 'view') {
		$formContainer.find("input,select ,button").attr("disabled","disabled")
		$formContainer.find(".user-eye").css("pointer-events", "none")
		$formContainer.find(".user-eye").css("opacity", "0.4")
	}
	else if (action == 'new') {
		$formContainer.find("#lastname,#firstname").keyup(function(){
			var $loginID = $formContainer.find("#loginid");
			var $pwd = $formContainer.find("#password");
			var firstName = $formContainer.find("#firstname").val();
			var lastName = $formContainer.find("#lastname").val();
			var loginID = "", pwd = "";
			if (firstName != "" && firstName != undefined) {
				loginID += firstName.charAt(0);
				pwd += firstName.charAt(0);
			}
			if (lastName != "" && lastName != undefined) {
				loginID += lastName;
				pwd += lastName
			}
			$($loginID).val(loginID.toLowerCase());
			$($pwd).val(loginID.toLowerCase());
		});
	}

	
		$formContainer.find("#saveUser").on('click touchstart',function(event){
			event.preventDefault();
			if($formContainer.ValidateForm()) {
				var role = $formContainer.find("#role").val();
				if(role == null  || role == undefined || role==""){
					swal("Please Select  Role");
				     return false;
				}
				
				var state = $formContainer.find("#state").val();
				if(state == null  || state == undefined || state==""){
					swal("Please Select  State");
				     return false;
				}
				
				/*var Utype = $formContainer.find("#mgrId").val();
				if(Utype == null  || Utype == undefined || Utype==""){
					swal("Please Enter User Manager");
				     return false;
				}*/
				var Utype = $formContainer.find("#mgrId").val();
				if(!$formContainer.find('#mgrId').prop('disabled')){
					if(Utype == null  || Utype == undefined || Utype==""){
						swal("Please Enter User Manager");
					     return false;
					}
				}
			
				
				var passwordValue = $formContainer.find("#password").val().trim(" ");
				var password= $formContainer.find("#password").val();
				if(password == null || password == ""){
					 swal("Warning!", "Please Enter The Password.", "warning");
				     return false;
				}
				if(password.split(" ").length > 1){
					 swal("Warning!", "Please Enter The Valid Password.", "warning");
				     return false;
				}
					
				var email = $formContainer.find("#email").val()
				
				if(email == null  || email == undefined || email=="")
				{
				}
				else
				{
				var pattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	  
			  if(!pattern.test(email)) {
			       swal("Warning!", "Please Enter the Valid Email.", "warning");
			     return false;
			    
				  }
		     }
		     
				var num = /^\d{10}$/;
				var phone = $formContainer.find("#phone").val();
				var mobile = $formContainer.find("#mobile").val();
				if(!mobile.match(num) && mobile != "" && mobile != null){
					 swal("Warning!", "Please Enter the Valid Mobile Number.", "warning");
				     return false;
				}
				
				if(!phone.match(num) && phone != "" && phone != null){
					 swal("Warning!", "Please Enter the Valid Phone Number.", "warning");
				     return false;
				}
				
				var formObj  = $.parseJSON(serializeJSONIncludingDisabledFields($formContainer));
				
				Object.assign(formObj, {password:passwordValue});
				
				var grpValue=[] 
				var grp = $formContainer.find("#Group").val()
				for(var i =0;i<grp.length;i++){
					var grpObj = new Object();
					grpObj.isActive = 'Y'
				    grpObj.rgmId = grp[i]
					grpValue.push(grpObj)
				}
				
				formObj.recugrpMap = grpValue;
				
				var roleArr = [];
				var role = $formContainer.find("#role").val();
				if(role != null){
					var roleObj = new Object();
					roleObj.roleid = role;
					roleArr.push(roleObj);
				}
                formObj.userroles = roleArr;
	                
				var formObj = JSON.stringify(formObj);
				
				if($formContainer.find(".hiddenWholeForm").val() == formObj){
					swal("Saved!", "User saved successfully.", "success");
					$formContainer.closeUserTab();
					$($FI).getuserlist()
					
					$($FI).reloadGrid("UserLst", userListId, userGridOptions);
					return true;
				}
				var userid = $formContainer.find("#userid").val();
				$(this).cAjax('Post','./admin/getNumberCheck','mobile='+mobile+'&phone='+phone+'&userid='+userid, false, false, '', 'Error', true);
				if(strResponseText == "success") 
				{
						$(this).cAjaxPostJson('Post','./admin/saveUser',formObj, false, false, '', 'Error', true);
						xhrObj = $(this).data("xhr");
						if(xhrObj.status == 409) 
						{
							swal("Duplicate Login Id!", "", "warning");  
						}
						else if (strResponseText) 
						{
							var productReturn = $.parseJSON(strResponseText);
		                     if(productReturn.userid)
		                     {
								swal("Saved!", "User saved successfully.", "success");
								$formContainer.closeUserTab();
		
								$($FI).reloadGrid("UserLst", userListId, userGridOptions);
								$(this).getuserlist()
								return true;
		                     } 
		                     else 
		                     {
		                    	 swal({title: "Error!",  text: "An internal error occurred while saving User. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
		                     }
						} 
						else 
						{
							swal({title: "Error!",  text: "An internal error occurred while saving User. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
						}
				}
				else{
					if(strResponseText == "phone"){
						swal("Duplicate Phone Number!", "", "warning");
					}
					else if(strResponseText == "mobile"){
						swal("Duplicate Mobile Number!", "", "warning");
					}
					  
				}
			}
			return false;
		});

		$container.find("#cancel").on('click touchstart',function(event){
			event.preventDefault();
			$formContainer.closeUserTab();
		});
		

	if (action != "new") {
		$($FI).getUser(option.data);
	}
};

$.fn.getUser = function(id) {
	var option = $("div.active").data("data"),keyValue=option ? option.data : undefined;
	var action =option ? option.action : undefined;
	$(this).cAjax('Post','./admin/getUser',"id="+id, false, false, '', 'Error', true);
	var productReturn = $.parseJSON(strResponseText);
	var $formContainer = $($FI);
	$formContainer.find("#passwordResetDate").val(productReturn.passwordResetDate);
	$formContainer.find("#title").val(productReturn.title);
	$formContainer.find("#displayname").val(productReturn.displayname);

	$formContainer.find("#firstname").val(productReturn.firstname);
	$formContainer.find("#middlename").val(productReturn.middlename);
	$formContainer.find("#lastname").val(productReturn.lastname);
	$formContainer.find("#addr1").val(productReturn.addr1);
	$formContainer.find("#addr2").val(productReturn.addr2);
	$formContainer.find("#state").val(productReturn.state);
	$formContainer.find("#state").trigger("change");
	$formContainer.find("#city").val(productReturn.city);
	$formContainer.find("#mobile").val(productReturn.mobile);
	$formContainer.find("#email").val(productReturn.email);
	$formContainer.find("#phone").val(productReturn.phone);
	$formContainer.find("#pincode").val(productReturn.pincode);
	
	$formContainer.find("#password").val(productReturn.password);
	$formContainer.find("#siteid").val(productReturn.siteid);	
	$formContainer.find("#typeId").val(productReturn.typeId).trigger('change');
	
	if(action == 'view'){
	    $formContainer.find("#mgrId").val(productReturn.mgrId).attr("disabled",true);	
	}
	else{
		$formContainer.find("#mgrId").val(productReturn.mgrId);
	}
	
	if(productReturn.userroles.length > 0){
	$formContainer.find("#role").val(productReturn.userroles[0].roleid).select2();
	}
	
	if(action=='newas') {
		$FI.find("#userid").val("-1");
		
		$FI.find("#loginid").val('');
	} else {
		$formContainer.find("#userid").val(productReturn.userid);
		$formContainer.find("#loginid").val(productReturn.loginid);
	}
	
	

	if(productReturn.status == 1) {
		$formContainer.find(".statusActive").attr('checked', 'checked');
	} else {
		$formContainer.find(".statusInactive").attr('checked', 'checked');
	}
	
	var grp =  [];
	$.each(productReturn.recugrpMap,function(i,val){
		grp.push(val.rgmId);
		 
		});
		$FI.find("#Group").val(grp)
		$FI.find("#Group").select2();
};





/**
 * To show and hide password
 */
$.fn.shoPassword = function() {
	var FI = $(this).closest("form");

	if(FI.find("#password").attr('type') == "password") {
		FI.find("#password").attr('type', 'text');
		$(this).removeAttr('class')
		$(this).attr('class','fa fa-eye-slash col-sm-2')
	} else {
		FI.find("#password").attr('type', 'password');
		$(this).removeAttr('class')
		$(this).attr('class','fa fa-eye col-sm-2')
	}
}



$.fn.ResetAllDataInFormUser = function(){
	var Form = $(this).closest('form');
	$(this).ResetAllDataInForm(Form);


};


$.fn.checkuserfieldLength = function(){
	var FI = $(this).closest('form');
	
	var mobileLength = FI.find("#mobile").val().length.toString()
	
		var checkmobileLength = mobileLength.match(/\S+/g)
	    if(checkmobileLength == 11){
			FI.find("#mobSpan").html("(Max 10 Digit No. are allowed)")
			FI.find("#mobile").val(FI.find("#mobile").val().split( ' ' ).slice(0,10).join(" "))
		}
		else{
			FI.find("#mobSpan").empty()
		}
		
}

$.fn.chkManager = function(){
	var FI = $(this).closest('form');
	var usertypeId = FI.find("#typeId").val()
	if(usertypeId == "1"){
		FI.find("#mgrId").attr("disabled",true);
		FI.find("#mgrId").val("").select2();
	}
	else{
		FI.find("#mgrId").attr("disabled",false);
	}
}

$.fn.Grouplisting = function() {
	$FI.cAjax('GET','./job/getAllRecuGrpList', '', false, false, '', 'Error', true);
	var grpJson= $.parseJSON(strResponseText); 
	$FI.populateAdvanceSelect($FI.find("#Group"), grpJson, {"key":"rgmId","value":"recuGrpName"});
	$FI.find("#Group").children().first().remove();
	$FI.find("#Group").val("");
	$FI.find("#Group").select2();
}

