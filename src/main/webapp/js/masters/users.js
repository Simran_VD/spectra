var userdispid=29;
var userGridOptions;
$.fn.getusers = function() {
$(this).addTab("User",'./pages?name=masters/UsersList',"User");
}
$.fn.UserList = function() {
	userGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("UserLst", userdispid, userGridOptions);
	$("#UserLstForm .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
			try{
				var email_data = onSelectionChanged(userGridOptions)[0];
				var data = onSelectionChanged(userGridOptions)[0]['userid'];
			}
			catch(e){
				var data = undefined;
				var email_data = undefined;
			}
			if(!data){
				swal("Please select a row first");
				return false;
			}
			if(!email_data){
				swal("Please select a row first");
				return false;
			}

		switch(operation) {
		case "email":
			$(this).userEmailSender(email_data);
			break;
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});

	$("#UserLst").closest('form').attr("oninput","$(this).onUserFilterTextBoxChanged()");
}

$.fn.onUserFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	userGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.userEmailSender = function(invdata){
	var invoice_email = invdata.email;
	if(invoice_email == "undefined"||invoice_email == null||invoice_email == ""){
		swal("Email cannot be empty")
		return false;
	}
	
	//var invoice_Date = invdata.inv_dt
	//var invoice_No = invdata.inv_no
	//var invoice_Name =  invdata.name
	var mailBcc ="";
	var mailCc ="";
	var userId=invdata.userid;
	var loginId = invdata.loginid;
	var password = invdata.password;
	
        //var subject="Welcome to BetterBuddys";
        //var message="You have been Registered as our Partner,/\n  Your Login Credentials are /\n  Login Id-"+loginId+"/\n"+password+"\nFor any support or query please contact 91 8586977307";
        var message = "You have been Registered as our Partner\r\n" +
			  "Your Login Credentials are- \r\n" + 
			  "Username:-"+loginId+ 
			  "\nPassword:-" +password+
			  "\nFor any support or query please contact 91 8586977307";
        //var dt = new FormData();
        //dt.append("files",null)
      
        jQuery.ajax({url: './master/sendUserEmail?mailTo='+invoice_email+'&userId='+userId,
    		cache: false,
    		contentType: false,
    		processData: false,
    		type: 'POST',
    		success: function(data){
    			swal("Email Sent!", "The e-mail has been sent successfully.", "success");
    		},
    		error: function(data) {
    			
    			swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
    		}
    	});
       
}	