var mstsaleinvListGridOptions;
var mstsaleinvlistdispid = 28;
$.fn.getreportsalesinvoice = function() {
	$(this).addTab("mstsaleinvList",'./pages?name=masters/mstSaleInvoice',"Sales Invoice");
}
$.fn.mstsaleinvList = function() {
	mstsaleinvListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			

		};
	$(this).reloadGrid("mstsaleinvLstLst", mstsaleinvlistdispid, mstsaleinvListGridOptions);
	
	$("#mstsaleinvLstLstForm .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();

		
			
			try{
				var email_data = onSelectionChanged(mstsaleinvListGridOptions)[0];
				var data = onSelectionChanged(mstsaleinvListGridOptions)[0]['pi_id'];
			}
			catch(e){
				var data = undefined;
				var email_data = undefined;
			}
			if(!data){
				swal("Please select a row first");
				return false;
			}
			if(!email_data){
				swal("Please select a row first");
				return false;
			}

		
		switch(operation) {
		case "print":
			$(this).printSalesInvoice(data);
			break;
		case "email":
			$(this).saleinvoiceEmail(email_data);
			break;
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});

	
	$("#mstsaleinvLstLst").closest('form').attr("oninput","$(this).onmstsaleinvFilterTextBoxChanged()");
}

$.fn.onmstsaleinvFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	mstsaleinvListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.printSalesInvoice = function(pi_id) {
	$(this).cAjax('POST','./admin/getUserName', '', false, false, '', 'Error', true);
	User =  $.parseJSON(strResponseText);
	userId = User.userid
	var rptUrl = reportingUrl + "/masters/SaleInv_New.rptdesign&__format=pdf&p_inv="+pi_id+"&rp_userid="+userId;
	window.open(rptUrl);
};

$.fn.saleinvoiceEmail = function(invdata){
	var invoice_email = invdata.email;
	if(invoice_email == "undefined"||invoice_email == null||invoice_email == ""){
		swal("Email cannot be empty")
		return false;
	}
	
	var invoice_Date = invdata.inv_dt
	var invoice_No = invdata.inv_no
	var invoice_Name =  invdata.name
	var mailBcc ="";
	var mailCc ="";
	var piId=invdata.pi_id;
	
	
        var subject="SaleInvoice No: "+invoice_No+" of Date : "+invoice_Date;
        var message="Dear "+invoice_Name+",\n  Please find attachment file.\n  Thanks and Regards.\n  www.BetterBuddys.com";
        var dt = new FormData();
        dt.append("files",null)
        
        
        
        jQuery.ajax({url: './master/sendSaleInvEmail?mailTo='+invoice_email+
    		'&mailBcc='+mailBcc+'&mailCc='+mailCc+'&subject='+subject+'&message='+message+'&data='+piId,
    		data:dt,
    		cache: false,
    		contentType: false,
    		processData: false,
    		type: 'POST',
    		success: function(data){
    			swal("Email Sent!", "The e-mail has been sent successfully.", "success");
    		},
    		error: function(data) {
    			
    			swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
    		}
    	});
       
		
}
