var crptLdgrListGridOptions;
var crptLdgrlistdispid = 27;
$.fn.getcoperateledger = function() {
	$(this).addTab("mstledgerList",'./pages?name=masters/mstledgerList',"Corporate Ledger");
}
$.fn.crptLdgrmstList = function() {
	crptLdgrListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			

		};
	$(this).reloadGrid("crptLdgrMstLst", crptLdgrlistdispid, crptLdgrListGridOptions);
	
	$("#crptLdgrMstLst").closest('form').attr("oninput","$(this).oncrptLdgrFilterTextBoxChanged()");
}

$.fn.oncrptLdgrFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	crptLdgrListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}
