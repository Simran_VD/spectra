var ptnrteamdispid=7;

var ptnrteamGridOptions;
$.fn.getpartnerteam = function() {
$(this).addTab("Partner_Team",'./pages?name=masters/partnerteamlist',"Partner Team");

}
$.fn.ptnrteamList = function() {
	ptnrteamGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("PtnrTeamLst", ptnrteamdispid, ptnrteamGridOptions);		
	
	$("#PtnrTeamMap .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" ){
			try{
				var data = onSelectionChanged(ptnrteamGridOptions)[0].pt_id;
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			$(this).addTab("AddTeam",'./pages?name=masters/addeditptnrteam',"Add Team",{"action":"new"});
			break;
		case "edit":
			$(this).addTab("EditTeam"+data,'./pages?name=masters/addeditptnrteam',"Edit Team-"+data,{"data":data,"action":"edit"});
			break;
		case "view":
			$(this).addTab("ViewTeam"+data,'./pages?name=masters/addeditptnrteam',"View Team-"+data,{"data":data,"action":"view"});
			break;
		
			
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});
	
	
	$("#PtnrTeamLst").closest('form').attr("oninput","$(this).onPtnrteamFilterTextBoxChanged()");
}

$.fn.onPtnrteamFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	ptnrteamGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.preparePtnrTeamForm = function($FI){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined,openingStockNumber = '';
	var formContainer = $FI;
	
	if(option.data){
		$FI.find("#prtteam .prtteamactivestatus").attr("id", "prtteamactivestatus"+option.data)
		$FI.find("#prtteam #prtteamactivestatuslabel").attr("for","prtteamactivestatus"+option.data);
		$FI.find("#prtteam .prtteaminactivestatus").attr("id", "prtteaminactivestatus"+option.data)
		$FI.find("#prtteam #prtteaminactivestatuslabel").attr("for","prtteaminactivestatus"+option.data);
	}
	else{
		$FI.find("#prtteam .prtteamactivestatus").attr("id", "prtteamactivestatus")
		$FI.find("#prtteam #prtteamactivestatuslabel").attr("for","prtteamactivestatus");
		$FI.find("#prtteam .prtteaminactivestatus").attr("id", "prtteaminactivestatus")
		$FI.find("#prtteam #prtteaminactivestatuslabel").attr("for","prtteaminactivestatus");
	}
	
	
		$FI.cAjax('GET','./partner/getPartnerList', '', false, false, '', 'Error', true);
	   partners =  $.parseJSON(strResponseText);
	   $FI.populateAdvanceSelect($FI.find("#pmId"),partners,{"key":"pmId","value":"displayName"});

	if(action == "new"){
		$FI.cAjax('POST','./admin/getUserName', '', false, false, '', 'Error', true);
		var prtType = $.parseJSON(strResponseText)
		var userType = prtType.userType
		var uiid = prtType.uiid
		if(userType == "Partner"){
			$FI.find("#pmId").val(prtType.uiid);
			$FI.find("#pmId").select2();
			$FI.find("#pmId").attr("disabled",true)
		}

	}
	
	
	$FI.find("#savePtnrTeam").on('click touchstart',function(event){
		if(formContainer.ValidateForm()){
			var ptnrteamFormJson = $.parseJSON(serializeJSONIncludingDisabledFields($FI));
			$(this).cAjaxPostJson('Post','./partner/savePartnerTeam',JSON.stringify(ptnrteamFormJson), false, false, '', 'Error', true);
			if(strResponseText){
				
				var ptnrteamReturn = $.parseJSON(strResponseText);
				if(ptnrteamReturn.pmId){
					if(strResponseText){
				var ptnrTeamReturn = $.parseJSON(strResponseText);
				var docform = new FormData();
				var teamImg = $FI.find('#imageFile').get(0).files[0];
				var aadhaarImage = $FI.find('#teamaadhaarImage1').get(0).files[0];
				if(teamImg){
					docform.append("partnerTeamImage", teamImg);
				}
				if(aadhaarImage){
					docform.append("adhar", aadhaarImage);
				}
				
				docform.append("ptId", ptnrTeamReturn.ptId);
				
				jQuery.ajax({url: './partner/uploadPartnerTeamDoc',
								data: docform,
								cache: false,
								contentType: false,
								processData: false,
								type: 'POST',
								success: function(docform){
									swal("Saved!", "Partner Team saved successfully.", "success");
					                $FI.closeUserTab();
					                $(this).getpartnerteam()
					                $(this).reloadGrid("PtnrTeamLst", ptnrteamdispid, ptnrteamGridOptions);
								},
								error: function(docform) {
									//alert(data);
									swal({title: "Error !",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
								}
							});
				}
					return true;
				}
				else{
					swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					return false;
				}
			}
			else{
					swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					return false;
				}
		}
	})
	
	if(keyValue){
		$(this).cAjax('GET','./partner/getPartnerTeam',"ptId="+keyValue, false, false, '', 'Error', true);
		var ptnrTeam = $.parseJSON(strResponseText);
		$(this).fillPtnrTeamForm($FI,ptnrTeam);
		var action = option ? option.action : undefined;
		if(action && action=="view" ){
			$FI.find("input,select,button").attr("disabled","disabled")
			$FI.find("#image_name").css("pointer-events", "none");
		}else if(action && action=="edit"){
			$FI.find("#pmId").attr("disabled",true)
			//$FI.find("#teamcloseBtn").removeAttr("disabled");
		}

	}
	
	
}

$.fn.fillPtnrTeamForm = function($FI,ptnrTeam){
	$FI.find("#name").val(ptnrTeam.name);
	$FI.find("#displayName").val(ptnrTeam.displayName);
	$FI.find("#address").val(ptnrTeam.address);
	$FI.find("#landmark").val(ptnrTeam.landmark);
	$FI.find("#city").val(ptnrTeam.city);
	$FI.find("#adharNo").val(ptnrTeam.adharNo);
	//$FI.find("#image_name").html(ptnrTeam.adharNo);
	$FI.find("#state").val(ptnrTeam.state);
	$FI.find("#company").val(ptnrTeam.company);
	$FI.find("#phone").val(ptnrTeam.phone);
	$FI.find("#mobile").val(ptnrTeam.mobile);
	$FI.find("#email").val(ptnrTeam.email);
	$FI.find("#remarks").val(ptnrTeam.remarks);
	$FI.find("#pmId").val(ptnrTeam.pmId);
	$FI.find("#pmId").select2();
	$FI.find("#ptId").val(ptnrTeam.ptId);
	$FI.find("[value='"+ptnrTeam.isActive+"'][name='isActive']").attr('checked','checked');
	
	if(imageExists('../uploaded/team'+ptnrTeam.ptId+'.png')){
		$FI.find("img").removeAttr('src');
		$FI.find("img").attr('src','../uploaded/team'+ptnrTeam.ptId+'.png').show();
	}
	
	if(ptnrTeam.adharImage){
		$FI.find('#docDownloadAdharBtn').removeAttr('disabled');
		$FI.find('#teamcloseBtn').removeAttr('disabled');
	}
	if(ptnrTeam.adharImage){
		$FI.find("#adharNo_image_name").text(ptnrTeam.adharNo);
	}
	
}


$.fn.downloadTeamDocImage = function(){
	var ptId = $(this).closest('form').find("#ptId").val();
	var param = 'ptId='+ptId;
	if(pmId){
		window.location='./partner/getPartnerTeamUploadedDoc?'+param;
	}
}

$.fn.ResetAllDataInFormprtteam=function(){
	var Form=$(this).closest('form')
	Form.find("#image_name").empty()
	Form.find("#teamaadhaarImage1").empty()
	$(this).ResetAllDataInForm(Form)
};

$.fn.removeteamAdhrImg=function(docname){
	var FI = $(this).closest('form');
	var pid = $(this).closest('form').find("#ptId").val();
	if(pid == "")
	{
		FI.find("#image_name").empty()
		FI.find("#teamaadhaarImage1").empty()
		return false;
	}
	var	param = 'id='+pid;
	FI.find("#image_name").empty()
	FI.find("#teamaadhaarImage1").empty()
	
	jQuery.ajax({url: './partner/deletePartnerTeamDoc?'+param,
	      
		data: "",
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		success: function(){
			swal("Deleted!", "Partner Team Document Deleted successfully.", "success");
            //$FI.closeUserTab();
            //$(this).reloadGrid("PtnrTeamLst", ptnrteamdispid, ptnrteamGridOptions);
		},
		error: function(docform) {
			swal({title: "Error !",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
		}
	});
	$(this).attr('disabled','disabled');
	FI.find("#docDownloadAdharBtn").attr('disabled','disabled');
	
}

//$.fn.removeteamImg=function(){
//	var FI = $(this).closest('form');
//	FI.find("#imglabel").empty()
//	FI.find("#imglabel").append('<img alt="user image" src="./images/avtara.png" id="imageDisp" >')
//	FI.find("#imglabel").append('<input type="file" id="imageFile" src="" style="display: none;" accept="image/x-png" onchange="$(this).showImage()">')
//}
