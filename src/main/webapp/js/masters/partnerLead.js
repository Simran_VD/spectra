var prtLeaddispid=5;
var prtLeadGridOptions;
$.fn.getpartleadmap = function() {
	
$(this).addTab("Partner_Lead",'./pages?name=masters/partnerLeadList',"Partner Lead");
}

$.fn.prtLeadList = function($FI) {
	
	var FI = $("#" +$FI)
	prtLeadGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			

		};
	var listDiv = FI.find("#prtleadLst");
	$(this).reloadGrid("prtleadLst", prtLeaddispid, prtLeadGridOptions,$FI,whereClause);	
	FI.find("#prtleadLst").closest('form').attr("oninput","$(this).onprtleadFilterTextBoxChanged()");
}

$.fn.onprtleadFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	prtLeadGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}