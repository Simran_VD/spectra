var prodserdispid=3;
var prodsermstGridOptions;
$.fn.getprodservicemap = function() {
$(this).addTab("Product_Service",'./pages?name=masters/prodsermaplist',"Product Service");
}
$.fn.ProdSermstList = function() {
	prodsermstGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			

		};
	$(this).reloadGrid("ProdSerMstLst", prodserdispid, prodsermstGridOptions);
	$("#ProdSerMstLst").closest('form').attr("oninput","$(this).onProdserFilterTextBoxChanged()");
}

$.fn.onProdserFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	prodsermstGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}