var ptnrId;

var ptnrMstlistdispid=9;
var ptnrListGridOptions;
$.fn.getpartnermaster = function() {
	$(this).addTab("Partner_Mst",'./pages?name=masters/partnerMstList',"Partner Master");
}
$.fn.PtnrMstList = function() {
	ptnrMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			onRowDoubleClicked: partnerlead,
			

		};
	$(this).reloadGrid("PtnrMstLst", ptnrMstlistdispid, ptnrMstListGridOptions);
	
	
	
	$("#PtnrMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh" ){
			try{
				var data = onSelectionChanged(ptnrMstListGridOptions)[0].pm_id;
				ptnrId = data;
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			$(this).addTab("AddPartner",'./pages?name=masters/addEditPtnrMst',"Add Partner",{"action":"new"});
			break;
		case "edit":
			$(this).addTab("EditPartner"+data,'./pages?name=masters/addEditPtnrMst',"Edit Partner-"+data,{"data":data,"action":"edit"});
			break;
		case "view":
			$(this).addTab("ViewPartner"+data,'./pages?name=masters/addEditPtnrMst',"View Partner-"+data,{"data":data,"action":"view"});
			break;
		case "wallet":
				whereClause = '"pm_id"='+data;
				$(this).addTab("manualTrans"+data,'./pages?name=wallet/manualTranList',"Wallet-"+data,{"data":data,"action":"view"});
				break;
        case "refresh":
        	$(this).reloadGrid("PtnrMstLst", ptnrMstlistdispid, ptnrMstListGridOptions);
        	swal("Refresh!","Partner Refresh Successfully.", "success");
        	break;
        			
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});
	
	
	
	
	
	$("#PtnrMstLst").closest('form').attr("oninput","$(this).onPtnrMstFilterTextBoxChanged()");
}

$.fn.onPtnrMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	ptnrMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.Refreshprtmst = function() {
	var FI = $(this).closest('form');
	ptnrMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.preparePtnrMstForm = function($FI){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined,openingStockNumber = '';
	var formContainer = $FI;
	
	if(option.data){
		$FI.find("#ptnrmst .activestatus").attr("id", "activestatus"+option.data)
		$FI.find("#ptnrmst #activestatuslabel").attr("for","activestatus"+option.data);
		$FI.find("#ptnrmst .inactivestatus").attr("id", "inactivestatus"+option.data)
		$FI.find("#ptnrmst #inactivestatuslabel").attr("for","inactivestatus"+option.data);
	}
	else{
		$FI.find("#ptnrmst .activestatus").attr("id", "activestatus")
		$FI.find("#ptnrmst #activestatuslabel").attr("for","activestatus");
		$FI.find("#ptnrmst .inactivestatus").attr("id", "inactivestatus")
		$FI.find("#ptnrmst #inactivestatuslabel").attr("for","inactivestatus");
	}
	
	$FI.cAjax('POST','./mstData/getStates', '', false, false, '', 'Error', true);
	states =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#state"),states,{"key":"stateid","value":"statename"});
	
	
	$FI.cAjax('GET','./product/getPincode', '', false, false, '', 'Error', true);
	areas =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#areas"),areas,{"key":"pcId","value":"pincode"});
	
	$FI.cAjax('GET','./partner/productServices', '', false, false, '', 'Error', true);
	
	services =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#services"),services,{"key":"smId","value":"serviceName"});
	
	$FI.cAjax('GET','./admin/getAllCityMasterView', '', false, false, '', 'Error', true);
	allSrvCity =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#srvCity"),allSrvCity,{"key":"cityId","value":"cityName"});
	
	$FI.find("#savePtnrMst").on('click touchstart',function(event){
		if(formContainer.ValidateForm()){
			var areaList = [];
		
			var selectedAreas = $FI.find("#areas").val();
			for(var i = 0; i < selectedAreas.length; i++){
				var areas = new Object();
				areas['pcId'] = parseInt(selectedAreas[i]);
				areaList.push(areas)
			}
			
			var servicesList = [];
			
			var selectedServices = $FI.find("#services").val();
			for(var i = 0; i < selectedServices.length; i++){
				var services = new Object()
				services['smId'] = parseInt(selectedServices[i]);
				servicesList.push(services)
			}
			
			
			var ptnrMstFormJson = $.parseJSON(serializeJSONIncludingDisabledFields($FI));
			ptnrMstFormJson['partnerAreaMap'] = areaList;
			ptnrMstFormJson['partnerServiceMap'] = servicesList;
			
			$(this).cAjaxPostJson('Post','./partner/savePartnerMst',JSON.stringify(ptnrMstFormJson), false, false, '', 'Error', true);
			
			
			if(strResponseText){
				var ptnrMstReturn = $.parseJSON(strResponseText);
				var docform = new FormData();
				var ptnrImg = $FI.find('#imageFile').get(0).files[0];
				var gstinImage = $FI.find('#gstinImage').get(0).files[0];
				var panImage = $FI.find('#panImage').get(0).files[0];
				var aadhaarImage = $FI.find('#aadhaarImage').get(0).files[0];
				if(ptnrImg){
					docform.append("partnerImage", ptnrImg);
				}
				if(gstinImage){
					docform.append("gstin", gstinImage);
				}
				if(panImage){
					docform.append("pan", panImage);
				}
				if(aadhaarImage){
					docform.append("adhar", aadhaarImage);
				}
				
				docform.append("pmId", ptnrMstReturn.pmId);
				
				jQuery.ajax({url: './partner/uploadPartnerDoc',
								data: docform,
								cache: false,
								contentType: false,
								processData: false,
								type: 'POST',
								success: function(docform){
									swal("Saved!","Partner saved Successfully.", "success");
									$FI.closeUserTab();
									$(this).getpartnermaster()
									$(this).reloadGrid("PtnrMstLst", ptnrMstlistdispid, ptnrMstListGridOptions);
									$(this).reloadGrid("PtnrAreaMapLst", ptnrlistdispid, ptnrListGridOptions);
								},
								error: function(docform) {
									//alert(data);
									swal({title: "Error !",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
								}
							});
				}
		
		}
		
		
	});
	
	if(keyValue){
		$(this).cAjax('GET','./partner/getPartnerMst',"pmId="+keyValue, false, false, '', 'Error', true);
		var ptnrMst = $.parseJSON(strResponseText);
		$(this).fillPtnrMstForm($FI,ptnrMst);
		var action = option ? option.action : undefined;
		if(action && action=="view" ){
			$FI.find("input,select,button").attr("disabled","disabled")
		}else if(action && action=="edit"){
			//$FI.find("#deptName").attr("disabled","disabled");
			
			
		}

	}
}

$.fn.fillPtnrMstForm = function($FI,ptnrMst){
	$FI.find("#pmId").val(ptnrMst.pmId);
	$FI.find("#name").val(ptnrMst.name);
	$FI.find("#displayName").val(ptnrMst.displayName);
	$FI.find("#company").val(ptnrMst.company);
	$FI.find("#mobile").val(ptnrMst.mobile);
	$FI.find("#phone").val(ptnrMst.phone);
	$FI.find("#email").val(ptnrMst.email);
	$FI.find("#address").val(ptnrMst.address);
	$FI.find("#landmark").val(ptnrMst.landmark);
	$FI.find("#state").val(ptnrMst.state);
	$FI.find("#state").trigger('change');
	$FI.find("#city").val(ptnrMst.city);
	$FI.find("#city").select2();
	$FI.find("#city").trigger('change');
	$FI.find("#city").val(ptnrMst.city);
	$FI.find("#pinCode").val(ptnrMst.pinCode);
	
	$FI.find("#contactPerson").val(ptnrMst.contactPerson);
	$FI.find("#contactPersonEmail").val(ptnrMst.contactPersonEmail);
	$FI.find("#contactPersonMob").val(ptnrMst.contactPersonMob);
	$FI.find("#remarks").val(ptnrMst.remarks);
	$FI.find("#bankName").val(ptnrMst.bankName);
	$FI.find("#branch").val(ptnrMst.branch);
	$FI.find("[value='"+ptnrMst.isActive+"'][name='isActive']").attr('checked','checked')
	
	$FI.find("#bankAccountNo").val(ptnrMst.bankAccountNo);
	$FI.find("#ifscCode").val(ptnrMst.ifscCode);
	$FI.find("#adharNo").val(ptnrMst.adharNo);
	$FI.find("#gstin").val(ptnrMst.gstin);
	$FI.find("#pan").val(ptnrMst.pan);
	var areaLst = []
	for(var i = 0; i<ptnrMst.partnerAreaMap.length; i++){
		areaLst.push(ptnrMst.partnerAreaMap[i]['pcId']);
	}
	
	var servicesLst = []
	for(var i = 0; i<ptnrMst.partnerServiceMap.length; i++){
		servicesLst.push(ptnrMst.partnerServiceMap[i]['smId']);
	}
	
	$FI.find("#areas").val(areaLst);
	
	
	
	if(ptnrMst.adharImage){
		$FI.find("#adharNo_image_name").text(ptnrMst.adharNo);
		$FI.find("#docDownloadBtn").removeAttr('disabled');
		$FI.find('#ptnrmstadharBtn').removeAttr('disabled');
	}
	
	if(ptnrMst.gstinImage){
		$FI.find("#gstin_image_name").text(ptnrMst.gstin);
		$FI.find("#docDownloadBtn1").removeAttr('disabled');
		$FI.find('#ptnrmstgstBtn').removeAttr('disabled');
	}
	
	if(ptnrMst.panImage){
		$FI.find("#pan_image_name").text(ptnrMst.pan);
		$FI.find("#docDownloadBtn2").removeAttr('disabled');
		$FI.find('#ptnrmstpanBtn').removeAttr('disabled');
	}
	
	$FI.find("#areas").select2();
	$FI.find("#services").val(servicesLst);
	$FI.find("#services").select2();
	
	if(imageExists('../uploaded/empImage'+ptnrMst.pmId+'.png')){
		$FI.find("#imageDisp").removeAttr('src');
		$FI.find("#imageDisp").attr('src','../uploaded/empImage'+ptnrMst.pmId+'.png').show();
	}
	
	
	
}




$.fn.hoverImage = function() {
	$(this).css('opacity','0.6');
	$(this).find('p').css('display','block');
}
$.fn.removeHoverImage = function() {
	$(this).css('opacity','1');
	$(this).find('p').css('display','none');
}
$.fn.showImage = function() {
	$(this).closest('form').find(".hiddenWholeForm").val("")
    if ($(this)[0].files && $(this)[0].files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageDisp').attr('src', e.target.result);
        }
       var imgname =  $("#imageFile");
    	var filename = imgname[0].files[0].name;  
    	var imgext =  filename.split('.')[1];
    	if(imgext  !="png"&"jpg"&"jpeg"){
    		swal("You cannot upload this file")
    		return false;
    	}
        reader.readAsDataURL($(this)[0].files[0]);
    }
}
$.fn.downloadDocImage = function(docType){
//	$("#docDownloadBtn").attr("disbaled","false")
	var pmId = $(this).closest('form').find("#pmId").val();
	var param = 'pmId='+pmId+'&mfaId='+docType;
	if(pmId){
		window.location='./partner/getPartnerUploadedDoc?'+param;
	}
	
}

function partnerlead(row) {
	var rowData = row.data;
	var pm_id = rowData.pm_id;
	whereClause = '"pm_id"='+pm_id;
	$(this).getpartleadmap();
	$(this).reloadGrid("prtleadLst", prtLeaddispid, prtLeadGridOptions,$FI,whereClause);	

}

$.fn.oncityChange = function(){
	var $FI = $(this).closest('form');
	var cityid = $(this).val();
	$(this).cAjax('GET','./partner/getPincode', 'cityId='+cityid, false, false, '', 'Error', true);
	pinarea =  $.parseJSON(strResponseText);
	
	$FI.populateAdvanceSelect($FI.find("#pinCode"),pinarea,{"key":"pcId","value":"pin"});
	
};

$.fn.ResetAllDataInFormprtmst=function(){
	
	var Form=$(this).closest('form')
	$(this).ResetAllDataInForm(Form)
};

$.fn.removeadhrimg=function(mstdocname){
	var FI = $(this).closest('form');
	var pmstid = $(this).closest('form').find("#pmId").val();
	if(pmstid == "")
	{
		FI.find("#adharNo_image_name").empty()
		FI.find("#aadhaarImage").empty()
		return false;
	}
	var	mstparam = 'id='+pmstid+'&type='+mstdocname;
	FI.find("#adharNo_image_name").empty()
	FI.find("#aadhaarImage").empty()
	
	jQuery.ajax({url: './partner/deletePartnerDoc?'+mstparam,
	      
		data: "",
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		success: function(){
			swal("Deleted!", "Partner Master Aadhar Document Deleted successfully.", "success");
            //$FI.closeUserTab();
            //$(this).reloadGrid("PtnrTeamLst", ptnrteamdispid, ptnrteamGridOptions);
		},
		error: function(docform) {
			swal({title: "Error !",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
		}
	});
	FI.find("#docDownloadBtn").attr('disabled','disabled');
}




$.fn.removegstImg=function(msggstDocname){
	var FI = $(this).closest('form');
	var pmstid = $(this).closest('form').find("#pmId").val();
	if(pmstid == "")
	{
		FI.find("#gstin_image_name").empty()
		FI.find("#gstinImage").empty()
		return false;
	}
	var	mstparam ='id='+pmstid+'&type='+msggstDocname;
	FI.find("#gstin_image_name").empty()
	FI.find("#gstinImage").empty()
	
	jQuery.ajax({url: './partner/deletePartnerDoc?'+mstparam,
	      
		data: "",
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		success: function(){
			swal("Deleted!", "Partner Master Gst Document Deleted successfully.", "success");
            //$FI.closeUserTab();
            //$(this).reloadGrid("PtnrTeamLst", ptnrteamdispid, ptnrteamGridOptions);
		},
		error: function(docform) {
			swal({title: "Error !",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
		}
	});
	FI.find("#docDownloadBtn1").attr('disabled','disabled');
}




$.fn.removepanImg=function(mstpanDocName){
	var FI = $(this).closest('form');
	var pmstid = $(this).closest('form').find("#pmId").val();
	if(pmstid == "")
	{
		FI.find("#pan_image_name").empty()
		FI.find("#panImage").empty()
		return false;
	}
	var	mstparam ='id='+pmstid+'&type='+mstpanDocName;
	FI.find("#pan_image_name").empty()
	FI.find("#panImage").empty()
	
	jQuery.ajax({url: './partner/deletePartnerDoc?'+mstparam,
	      
		data: "",
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		success: function(){
			swal("Deleted!", "Partner Master Pan Document Deleted successfully.", "success");
            //$FI.closeUserTab();
            //$(this).reloadGrid("PtnrTeamLst", ptnrteamdispid, ptnrteamGridOptions);
		},
		error: function(docform) {
			swal({title: "Error !",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
		}
	});
	FI.find("#docDownloadBtn2").attr('disabled','disabled');
}

$.fn.setRelatedPincodes = function(){
	var $FI = $(this).closest('form');
	var cityid = $(this).val();
	var allRelPin = [];
	if(cityid){
		$(this).cAjax('GET','./partner/getPincode', 'cityId='+cityid, false, false, '', 'Error', true);
		pinarea =  $.parseJSON(strResponseText);
		$.each(pinarea, function(){
			allRelPin.push(this.pcId);
		})
	}
	$FI.find("#areas").val(allRelPin);
	$FI.find("#areas").select2();
}

//$.fn.removemstImg=function(){
//	var FI = $(this).closest('form');
//	FI.find("#mstimglabel").empty()
//	FI.find("#mstimglabel").append('<img alt="user image" src="./images/avtara.png" id="imageDisp" >')
//	FI.find("#mstimglabel").append('<input type="file" id="imageFile" src="" style="display: none;" accept=".png,.jpg,.jpeg" onchange="$(this).showImage()">')
//}
