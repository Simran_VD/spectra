var domainArr = [];
var orgMasterlistdispid=1;
var orgMasterListGridOptions;
var additionalParam;
var skillMnt = [];
$.fn.getorgmst = function(position) {

	if(position){
		additionalParam =position;
	}

	else{
		additionalParam ="";
	}
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("OrgMaster",'./pages?name=masters/orgMst',"Org Master",'child',currDash);

}


$.fn.orgMasterList = function(){
	var id2=""
		orgMasterListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
	};
	$(this).reloadGrid("OrgMst", orgMasterlistdispid, orgMasterListGridOptions);



	$("#OrgMasterContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(JobMasterListGridOptions)[0].jm_id;

			}
			catch(e){
				var data = undefined;
			}

			if(!data){
				swal("Please select a row first");
				return false;
			}

			if(operation == "edit"){
				var data_status = onSelectionChanged(JobMasterListGridOptions)[0].status;
				if(data_status == "Closed"){
					swal("You cannot edit this Status is closed")
					return false;
				}
			}


		}
		switch(operation) {
		case "add new":
			$(this).addTab("AddOrgMaster",'./pages?name=masters/addEditMstOrg',"Add Org Master",{"action":"new"});
			break;
		case "edit":
			$(this).addTab("EditOrgMaster"+data,'./pages?name=masters/addEditMstOrg',"Edit Org Master-"+data,{"data":data,"action":"edit"});
			break;
		case "view":
			$(this).addTab("ViewOrgMaster"+data,'./pages?name=masters/addEditMstOrg',"View Org Master-"+data,{"data":data,"action":"view"});
			break;
		case "delete":
			swal({
				title: "Are you sure want to delete Job Master?",
				text: "You will not be able to recover Job Master now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false	,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if(isConfirm){
					$(this).cAjax('Post','./job/deleteJobMaster',"slid="+data, false, false, '', 'Error', true);
					if(strResponseText == "SUCCESS"){
						swal("Deleted!", "Selected record deleted successfully.", "success");
						$(this).reloadGrid("JobMst", jobMasterlistdispid, JobMasterListGridOptions);

					} else if(strResponseText == "DATA_INTEGRITY"||strResponseText == ""){
						swal("Error in deletion!", "Selected Item is being used. So it cannot be deleted.", "error");
					}
				} 
				else {
					swal("Cancelled", "Thank you! your Job Master is safe :)", "error");
				}
			});
			break;
		case "refresh":
			//var jobPage = $(this).closest('form').attr('id');
			//jobPageId = $("#"+jobPage);
			//var additionalParam = jobPageId.find("#typeParam").val();
			$(this).reloadGrid("JobMst", jobMasterlistdispid, JobMasterListGridOptions);

			swal("Refresh!","Job Master  Refresh Successfully.", "success");
			break;	
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});
	$("#JobMst").closest('form').attr("oninput","$(this).onJobMasterFilterTextBoxChanged()");		
}	

$.fn.onJobMasterFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	JobMasterListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.prepareOrgMasterForm = function($FI){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined,openingStockNumber = '';
	var formContainer = $FI;

	
	$(this).cAjax('GET','./getOrgTypeVw', '', false, false, '', 'Error', true);
	orgType =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect($FI.find("#orgType"),orgType,{"key":"otid","value":"orgType"});

	
	$(this).cAjax('GET','./job/getAllState', '', false, false, '', 'Error', true);
	var stateJson= $.parseJSON(strResponseText); 
	$(this).populateAdvanceSelect($FI.find("#currState"), stateJson, {"key":"stateId","value":"stateName"});
	 
	$FI.find("#saveJobMaster").on('click touchstart',function(event){
		if(formContainer.ValidateForm()){
			var email = formContainer.find("#companySpocEmail").val()

			if(email == null  || email == undefined || email=="")
			{
			}
			else
			{
				var pattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;


				if(!pattern.test(email)) {
					swal("Warning!", "Please Enter the Valid Email.", "warning");
					return false;

				}
			}


			if(navigator.onLine == false){
				swal("Warning", "You're offline. Check your connection.", "warning");
				return false;
			} 

			
			
			
			
			$(this).cAjaxPostJson('Post','./saveOrgMaster',JSON.stringify(jobMasterFormJson), false, false, '', 'Error', true);

			xhrObj = $(this).data("xhr");
			if(xhrObj.status == 409) {
				swal("Duplicate Job Code!", "", "warning");  
			}

			else if(strResponseText){

				var jobMasterReturn = $.parseJSON(strResponseText);
				if(jobMasterReturn.jmId){
					swal("Saved!", "Job Master Record saved successfully.", "success");
					
					formContainer.closeUserTab();
					$(this).getjobopening()
					
					$(this).reloadGrid("OrgMst", orgMasterlistdispid, orgMasterListGridOptions);
				
					return true;
				}
				else{
					swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					return false;
				}
			}
			/*else{
						swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
						return false;
					}*/
		}
	})

	if(keyValue){
		$(this).cAjax('GET','./job/getJobMaster',"jmId="+keyValue, false, false, '', 'Error', true);
		var jobMst = $.parseJSON(strResponseText);
		$(this).fillJobMstForm($FI,jobMst);
		var action = option ? option.action : undefined;

		if(action && action=="view" ){
			$FI.find("input,select,textarea ,button").attr("disabled","disabled")
			$FI.find("#jobEndtDt").attr("disabled",true)
		}else if(action && action=="edit"){
			$FI.find("#deptName").attr("disabled","disabled");
		}

	}


}



$.fn.setSdmId = function(){
	var selOpt = $(this).find('option:selected').eq(0);
	var data = selOpt.data();
	if(data){
		$(this).parent().find('input').val(data.sdmId);
	}
}

$.fn.fillJobMstForm = function($FI,jobMst){
	$FI.find("#jmId").val(jobMst.jmId);
	$FI.find("#clId").val(jobMst.clId).trigger('change');
	$FI.find("#recuName").val(jobMst.rmId);
	// $FI.find("#domain").val(jobMst.domainId);
//	$FI.find("#companySpoc").val(jobMst.companySpoc);
	//$FI.find("#spocId").val(jobMst.spocId);
//	$FI.find("#spocId").trigger('change');
	$FI.find("#companySpocEmail").val(jobMst.companySpocEmail);
	$FI.find("#companySpocPhone").val(jobMst.companySpocPhone);
	$FI.find("#jobTitle").val(jobMst.jobTitle);
	$FI.find("#jobRecdDt").val(jobMst.jobRecdDt).trigger('change');
	$FI.find("#minTotalExp").val(jobMst.minTotalExp).trigger('change');
	$FI.find("#maxTotalExp").val(jobMst.maxTotalExp);
	$FI.find("#maxTotalExp").trigger('change');
	$FI.find("#minRelExp").val(jobMst.minRelExp);
	$FI.find("#ctc").val(jobMst.ctc);
	$FI.find("#joiningPeriod").val(jobMst.joiningPeriod);
	$FI.find("#jobType").val(jobMst.jobType);

	var loc=[];
	$.each(jobMst.jobLocation,function(i,val){
		loc.push(val.lmId);
	});
	$FI.find("#location").val(loc);



	var skill=[];
	var sdmIds = [];
	$.each(jobMst.jobSkills,function(i,val){
		skill.push(val.smId);
		if(sdmIds.indexOf(val.sdmid) == -1){
			sdmIds.push(val.sdmId);
		}
	});
	$FI.find("#skillDomainName").val(sdmIds).trigger('change');
	var i = 0;
	$.each(jobMst.jobSkills,function(i,val){
		if(i != 0){
			$FI.find(".skillDtlRow:last").find("#addSkillDetailsButton").click();
		}
		var currentRow = $FI.find(".skillDtlRow:last");
		currentRow.find("#skillName").val(this.smId);
		currentRow.find("#skillName").select2();
		currentRow.find("#skillName").trigger('change');
		currentRow.find("#minExp").val(this.minExp);
		currentRow.find("#mandatoryskill").val(this.isMandatory);
		currentRow.find("#mandatoryskill").select2();

		i++;
	});

	var removefirstrow = false;
	i = 0;
	$.each(jobMst.jobEdu, function(i,val){
		if(i != 0){
			$FI.find("#eduDtlBody tr:last").find('#addEduDetailsButton').click()
		}
		var clonedRow = $FI.find("#eduDtlBody tr:last")
		clonedRow.find("#emId").val(this['emId']).select2();
		clonedRow.find("#coId").val(this['coId']).select2();
		clonedRow.find("#eduMandatory").val(this['isMandatory']).select2();
		clonedRow.find("#remark").val(this['remarks']);
		if(this['isMandatory'] == 'Y'){
			clonedRow.find('input:checkbox').attr('checked', 'checked');
			clonedRow.find('input:checkbox').val('Y')
		}
		else{
			clonedRow.find('input:checkbox').removeAttr('checked');
			//	clonedRow.find('input:checkbox').val('N')
		}
		//FI.find("#acctDtlBody").append(clonedRow);
		removefirstrow = true;
		i++
	})

	var removefirstrow = false;
	i = 0;
	$.each(jobMst.jobCert, function(i,val){
		if(i != 0){
			$FI.find("#certDtlBody tr:last").find('#addCertDetailsButton').click()
		}
		var clonedRow = $FI.find("#certDtlBody tr:last")
		clonedRow.find("#certifiName").val(this['certifiName']);
		clonedRow.find("#certifedBy").val(this['certifedBy']);
		clonedRow.find("#yearOfCompl").val(this['yearOfCompl']);
		clonedRow.find("#certMandatory").val(this['isMandatory']).select2();
		clonedRow.find("#remar").val(this['remarks']);

		i++
	})



	$FI.find("#jobLevel").val(jobMst.jobLevel);
	$FI.find("#jobDesc").val(jobMst.jobDesc);
	$FI.find("#jobCode").val(jobMst.jobCode);
	$FI.find("#noOfPosition").val(jobMst.noOfPosition);
	$FI.find("#employmentType").val(jobMst.employmentType);
	$FI.find("#budget").val(jobMst.budget);
	$FI.find("#jobStartDt").val(jobMst.jobStartDt);
	$FI.find("#jobEndtDt").val(jobMst.jobEndtDt);
//	$FI.find("#jobRecdDt").val(jobMst.jobRecdDt);
	$FI.find("#isActive").val(jobMst.isActive);
	$FI.find("#remarks").val(jobMst.remarks);
	$FI.find("#siteId").val(jobMst.siteId);
	$FI.find("#status").val(jobMst.jsmId);
	$FI.find("#skillNote").val(jobMst.skillNote);

//	for(var i=0; i<jobMst.jobSpoc.length;; i++){
//	if(jobMst.jobSpoc[i].category == "P"){
//	$FI.find("#spocId").val(jobMst.jobSpoc[i].category).trigger('change');
//	$FI.find("#secondaryspocId").select2();
//	}
//	}
//	$FI.find("#secondaryspocId").val(jobMst.jobSpoc)
//	$FI.find("#secondaryspocId").select2();



	var secondSpoc = [];
	var primarySpoc = [];
	$.each(jobMst.jobSpoc,function(i,val){
		if(jobMst.jobSpoc[i].category == "P"){
			primarySpoc.push(val.spocId);
		}
		else{
			secondSpoc.push(val.spocId);
		}
	});
	$FI.find("#spocId").val(primarySpoc).trigger('change')
	$FI.find("#spocId").select2();

	$FI.find("#secondaryspocId").val(secondSpoc)
	$FI.find("#secondaryspocId").select2();



	/*This code is internal and external recuiter*/
	var jobrec=[];
	var jobrecname = [];
	var extjobrec = [];
	var extjobrecname = [];
	$.each(jobMst.jobRecuMap,function(i,val){
		if(jobMst.jobRecuMap[i].sparev1 == "I"){
			if(val.rgmId){
				jobrec.push(val.rgmId);
			}
			if(val.recId){
				jobrecname.push(val.recId)
			}
		}
		else if(jobMst.jobRecuMap[i].sparev1 == "E"){

			if(val.rgmId){
				extjobrec.push(val.rgmId);
			}
			if(val.recId){
				extjobrecname.push(val.recId)
			}
		}
	});
	$FI.find("#internalRec").val(jobrec).trigger('change');
	$FI.find("#internalRecname").val(jobrecname);
	$FI.find("#externalRec").val(extjobrec).trigger('change');
	$FI.find("#externalRecname").val(extjobrecname);
}



$.fn.onSpocChange = function(){
	var selOpt = $(this).find('option:selected').eq(0);
	var data = selOpt.data();
	var optionVal = selOpt.text();
	var FI = $(this).closest('form');
	FI.find("#spocIdPer").find('option[manVal="'+optionVal+'"]').data('spocIdPer');
	if(optionVal){
		FI.find("#companySpocEmail").val(data.spocEmail);
		FI.find("#companySpocPhone").val(data.spocPhone);
		FI.find("#spocIdPer").val(optionVal)

	}
	else{
		FI.find("#companySpocEmail").val("");
		FI.find("#companySpocPhone").val("");
		FI.find("#spocIdPer").val("");
	}
}

$.fn.onChangeDatevalidate=function(){
	var jobRecdDt = $("#jobRecdDt").val();
	jobRecdDt = jobRecdDt.substr(3, 2)+"/"+jobRecdDt.substr(0, 2)+ "/"+jobRecdDt.substr(6, 4);
//	$('#jobEndtDt').removeAttr('disabled');
	var jobEndtDt = `<input class="form-control form-control-sm" type="text" data-provide="datepicker" id="jobEndtDt" autocomplete="off" readonly="readonly" placeholder="Select Job Close Date" name="jobEndtDt"/>`	 
		$('#jobEndtDt').remove();
	$('.jobclosesdata').html(jobEndtDt);
	$('#jobEndtDt').datepicker({
		dateFormat: 'dd/mm/yyyy',
		autoclose:true,
		startDate: new Date(jobRecdDt)
	});

}


/*/Start Code ||  Skill Name Dependent on Skill Domain Name/*/

$.fn.onDomainChange = function(){
	var FI = $(this).closest('form');
	domainArr = [];
	var sdmId = FI.find("#skillDomainName").val();
	if(sdmId != ""||sdmId.length != 0){
		$(this).cAjax('GET','./job/getAllskillView', "", false, false, '', 'Error', true);
		skillview =  $.parseJSON(strResponseText)
		for(var i in  sdmId){
			var domainId = sdmId[i]
			for(var j in skillview){
				var skillName = skillview[j].sdmId	
				if(domainId == skillName){
					var Name = skillview[j]
					domainArr.push(Name)
				}
			}	
		}
		//$(this).populateAdvanceSelect($FI.find("#skillName"),domainArr,{"key":"smId","value":"skillName"});
		//FI.find("#skillName").select2()
		var allSkillRows = FI.find('.skillDtlRow');
		var allSkillRowsLen = allSkillRows.length;
		var allSkillRowsToDec = allSkillRowsLen;
		for(var i = 0; i < allSkillRowsLen; i++){
			var currSkillVal = allSkillRows.eq(i).find('#skillName option:selected').val();
			var currExp = allSkillRows.eq(i).find('#minExp option:selected').val();
			
			if(currSkillVal == "" || currSkillVal == null || currSkillVal == undefined){
				$(this).populateAdvanceSelect(allSkillRows.eq(i).find('#skillName'),domainArr,{"key":"smId","value":"skillName"});
				$(this).populateAdvanceSelect(allSkillRows.eq(i).find("#minExp"),minExpJson,{"key":"values","value":"displayName"});
			}
			else{
				allSkillRows.eq(i).find('#skillName').parent().html(`<select class="form-control form-control-sm select2" onchange="$(this).setSdmId();" style="width: 100%;"  name="jobSkills[0][smId]" id="skillName" mandatory="true" message="Please Select Skill Name"><option value="">-- Select --</option></select>
				<input type="hidden" id="sdmId" name="jobSkills[0][sdmId]">`);
				
				allSkillRows.eq(i).find('#minExp').parent().html(`<select class="form-control form-control-sm select2" id="minExp" name="jobSkills[0][minExp]">
																	<option value="">---Select---</option>
														  		  </select>`);
				
				
				$(this).populateAdvanceSelect(allSkillRows.eq(i).find('#skillName'),domainArr,{"key":"smId","value":"skillName"});
				
				$(this).populateAdvanceSelect(allSkillRows.eq(i).find("#minExp"),minExpJson,{"key":"values","value":"displayName"});
				
				allSkillRows.eq(i).find('#skillName').val(currSkillVal);
                allSkillRows.eq(i).find('#minExp').val(currExp); 
                
				allSkillRows.eq(i).find('#skillName').select2();
				allSkillRows.eq(i).find('#minExp').select2();
				
				currSkillVal = allSkillRows.eq(i).find('#skillName option:selected').val();
				if(currSkillVal == "" || currSkillVal == null || currSkillVal == undefined){
					if(allSkillRowsToDec == 1){
						allSkillRows.eq(i).find('#skillName').val("").trigger('change');
						allSkillRows.eq(i).find('#minExp').val("");
					}
					else{
						allSkillRowsToDec--;
						allSkillRows.eq(i).remove();
					}
				}
				else{
					allSkillRows.eq(i).find('#skillName').trigger('change');
				}
			}
			

		}
	}
	else{
		var allSkillRows = FI.find('.skillDtlRow');
		
			if(allSkillRows.length > 1){
				FI.find("#skillDtlTable").find("tr:gt(1)").remove()
				
			}
		
		var blankArr =[]
		
		var minYrBlank =[]
		
		$(this).populateAdvanceSelect(allSkillRows.find('#skillName'),blankArr,{"key":"smId","value":"skillName"});
		
		$(this).populateAdvanceSelect(allSkillRows.find("#minExp"),minYrBlank,{"key":"values","value":"displayName"});
	}
}

/*	  /End Code/  */



$.fn.checkJobfieldLength = function(){
	var FI = $(this).closest('form');
	var titleLength = $("#jobTitle").val().length.toString().trim()
	var jobDescLength =$("#jobDesc").val().length.toString()
	var jobDesciptionLength =$("#jobDesciption").val().length.toString()
	var skillNoteLength =$("#skillNote").val().length.toString()
	var skillNoLength =$("#skillNo").val().length.toString()
	var clientLength =$("#client").val().length.toString()
	var remarksClLength =$("#remarksCl").val().length.toString()
	var jobCodeLength =$("#jobCode").val().length.toString()
	var spocNamePerLength =$("#spocNamePer").val().length.toString()
	var spocEmailPerLength =$("#spocEmailPer").val().length.toString()
	var spocPhonePerLength =$("#spocPhonePer").val().length.toString()
	var remarksPrsLength =$("#remarksPrs").val().length.toString()
	var ctcLength =$("#ctc").val().length.toString()	
	var skillDomainNameAddLength =$("#skillDomainNameAdd").val().length.toString()
	var remarksDomainAddLength =$("#remarksDomainAdd").val().length.toString()
	var skillNameAddLength =$("#skillNameAdd").val().length.toString()
	var remarksSkillAddLength =$("#remarksSkillAdd").val().length.toString()

	var noOfPositionLength =$("#noOfPosition").val().length.toString()

	var checkLength = titleLength.match(/\S+/g)
	if(checkLength > 256){
		$("#titleSpan").html("(Max 256 characters are allowed)")
		FI.find("#jobTitle").val(FI.find("#jobTitle").val().substr(0,256))	
	}
	else{
		$("#titleSpan").empty()
	}

	var checkDescLength = jobDescLength.match(/\S+/g)
	if(checkDescLength > 4000){
		$("#descSpan").html("(Max 4000 characters are allowed)")
		FI.find("#jobDesc").val(FI.find("#jobDesc").val().substr(0,4000))		
	}
	else{
		$("#descSpan").empty()
	}

	var checkDesciptionLength = jobDesciptionLength.match(/\S+/g)
	if(checkDesciptionLength > 4000){
		$("#descriptionSpan").html("(Max 4000 characters are allowed)")
		FI.find("#jobDesciption").val(FI.find("#jobDesciption").val().substr(0,4000))	
	}
	else{
		$("#descriptionSpan").empty()
	}

	var checkSkillNoteLength = skillNoteLength.match(/\S+/g)
	if(checkSkillNoteLength > 4000){
		$("#skillNoteSpan").html("(Max 4000 characters are allowed)")
		FI.find("#skillNote").val(FI.find("#skillNote").val().substr(0,4000))	

	}
	else{
		$("#skillNoteSpan").empty()
	}

	var checkSkillNoLength = skillNoLength.match(/\S+/g)
	if(checkSkillNoLength > 4000){
		$("#skillNoSpan").html("(Max 4000 characters are allowed)")
		FI.find("#skillNo").val(FI.find("#skillNo").val().substr(0,4000))	

	}
	else{
		$("#skillNoSpan").empty()
	}

	var checkclientLength = clientLength.match(/\S+/g)
	if(checkclientLength > 50){
		$("#clientMstSpan").html("(Max 50 characters are are allowed)")
		FI.find("#client").val(FI.find("#client").val().substr(0,50))
	}
	else{
		$("#clientMstSpan").empty()
	}

	var remarksClLengthcheckLength = remarksClLength.match(/\S+/g)
	if(remarksClLengthcheckLength > 256){
		$("#remarkclientMSpan").html("(Max 256 characters are allowed)")
		FI.find("#remarksCl").val(FI.find("#remarksCl").val().substr(0,256))

	}
	else{
		$("#remarkclientMSpan").empty()
	}

	var jobCodecheckLength = jobCodeLength.match(/\S+/g)
	if(jobCodecheckLength > 15){
		$("#jobCodeSpan").html("(Max 15 characters are allowed)")
		FI.find("#jobCode").val(FI.find("#jobCode").val().substr(0,15))

	}
	else{
		$("#jobCodeSpan").empty()
	}

	var spocNamePerLengthcheckLength = spocNamePerLength.match(/\S+/g)
	if(spocNamePerLengthcheckLength > 50){
		$("#spmSpan").html("(Max 50 characters are allowed)")
		FI.find("#spocNamePer").val(FI.find("#spocNamePer").val().substr(0,50))
	}
	else{
		$("#spmSpan").empty()
	}

	var spocEmailPercheckLength = spocEmailPerLength.match(/\S+/g)
	if(spocEmailPercheckLength > 64){
		$("#emailSpmSpan").html("(Max 64 characters are allowed)")
		FI.find("#spocEmailPer").val(FI.find("#spocEmailPer").val().substr(0,64))	
	}
	else{
		$("#emailSpmSpan").empty()
	}

	var ctcLengthcheckLength = ctcLength.match(/\S+/g)
	if(ctcLengthcheckLength > 2){
		$("#budgetSpan").html("(Max 2 digits are allowed)")
		FI.find("#ctc").val(FI.find("#ctc").val().substr(0,2))	
	}
	else{
		$("#budgetSpan").empty()
	}

	var spocPhonePerLengthcheckLength = spocPhonePerLength.match(/\S+/g)
	if(spocPhonePerLengthcheckLength  < 10){
		$("#phoneSpmSpan").html("(Max 10 Digits are allowed)")
		//	  FI.find("#spocPhonePer").val(FI.find("#spocPhonePer").val().substr(0,10))	
		if(FI.find("#spocPhonePer").val()==0){
			$("#phoneSpmSpan").empty()
		}

	}
	else{
		$("#phoneSpmSpan").empty()
	}

	var remarksPrsLengthcheckLength = remarksPrsLength.match(/\S+/g)
	if(remarksPrsLengthcheckLength > 256){
		$("#remarkSpmSpan").html("(Max 256 characters are allowed)")
		FI.find("#remarksPrs").val(FI.find("#remarksPrs").val().substr(0,256))	
	}
	else{
		$("#remarkSpmSpan").empty()
	}

	var skDomainaddcheckLength = skillDomainNameAddLength.match(/\S+/g)
	if(skDomainaddcheckLength > 256){
		$("#domainMstAddSpan").html("(Max 256 characters are allowed)")
		FI.find("#skillDomainNameAdd").val(FI.find("#skillDomainNameAdd").val().substr(0,256))

	}
	else{
		$("#domainMstAddSpan").empty()
	}

	var remarkDomcheckLength = remarksDomainAddLength.match(/\S+/g)
	if(remarkDomcheckLength > 256){
		$("#remarkDomAddSpan").html("(Max 256 characters are allowed)")
		FI.find("#remarksDomainAdd").val(FI.find("#remarksDomainAdd").val().substr(0,256))

	}
	else{
		$("#remarkDomAddSpan").empty()
	}

	var skillNameAddLengthcheckLength = skillNameAddLength.match(/\S+/g)
	if(skillNameAddLengthcheckLength > 256){
		$("#skillMstAddSpan").html("(Max 256 characters are allowed)")
		FI.find("#skillNameAdd").val(FI.find("#skillNameAdd").val().substr(0,256))
	}
	else{
		$("#skillMstAddSpan").empty()
	}

	var remarksSkillAddLengthcheckLength = remarksSkillAddLength.match(/\S+/g)
	if(remarksSkillAddLengthcheckLength > 256){
		$("#remarkSkillAddSpan").html("(Max 256 characters are allowed)")
		FI.find("#remarksSkillAdd").val(FI.find("#remarksSkillAdd").val().substr(0,256))	
	}
	else{
		$("#remarkSkillAddSpan").empty()
	}


	var noOfOpencheckLength = noOfPositionLength.match(/\S+/g)
	if(noOfOpencheckLength > 3){
		$("#noOfOpenSpan").html("(Max 3 Digits are allowed)")
		FI.find("#noOfPosition").val(FI.find("#noOfPosition").val().substr(0,3))	
	}
	else{
		$("#noOfOpenSpan").empty()
	}
}  

$.fn.saveclient = function() {
	event.preventDefault();

	var clientName = $FI.find("#clId").val()
	var FI= $(this).parents('.modal-content').find('form'); 

	if(!FI.ValidateForm()) {
		return false;
	}
	var operationObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	operationObj.isActive = isActive;
	var clientJson = JSON.stringify(operationObj);
	$(this).cAjaxPostJson('Post','./master/saveClientMst',clientJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Company Name!", "", "warning");  
	} else if(strResponseText) { 
		var clientMstReturn = $.parseJSON(strResponseText);
		if(clientMstReturn.clId != null && clientMstReturn.clId > "0"){
			swal("Saved!", "Company saved successfully.", "success");
			var setclientName = FI.find("#clId").val(clientName)
			$('#addclientModal').modal('hide');
			$(".ag-center-cols-container").css('width','100%')
			$(this).cAjax('GET','./job/getAllCompanyNameView', '', false, false, '', 'Error', true);
			cmpname =  $.parseJSON(strResponseText)
			$(this).populateAdvanceSelect($FI.find("#clId"),cmpname,{"key":"clId","value":"companyName"});
			$FI.find("#clId").val(clientName)


		}
	}
	return false;
};

$.fn.saveAddSpocPer = function(){

	event.preventDefault();
	var primarySpoc = $FI.find("#spocId").val()
	var FI= $(this).parents('.modal-content').find('form'); 

	if(!FI.ValidateForm()) {
		return false;
	}

	if(FI.ValidateForm()){
		var email = FI.find("#spocEmailPer").val()

		if(email == null  || email == undefined || email=="")
		{
		}
		else
		{
			var pattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;


			if(!pattern.test(email)) {
				swal("Warning!", "Please Enter the Valid Email.", "warning");
				return false;

			}
		}
	}

	if(FI.ValidateForm()){
		var phone = FI.find("#spocPhonePer").val();
		if(phone.length !=10){
			FI.find("#saveSPMstBtn").attr("disabled","disabled");
			return false;
		}

	}

	var operationObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	operationObj.isActive = isActive;
	var spocPerJson = JSON.stringify(operationObj);
	var clId = FI.find("#clId").val();
	$(this).cAjaxPostJson('Post','./master/saveSpocMst',spocPerJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Spoc Person Name!", "", "warning");  
	} else if(strResponseText) { 
		var spocMstReturn = $.parseJSON(strResponseText);
		if(spocMstReturn.spocId != null && spocMstReturn.spocId > "0"){
			swal("Saved!", "Spoc Person Name saved successfully.", "success");
			$('#addspocMstModel').modal('hide');
			//	$(this).reloadGrid("clientMst", clientMstlistdispid, clientMstListGridOptions);
			$(".ag-center-cols-container").css('width','100%')



			$(this).cAjax('GET','./master/getSpocMstListVw', 'clId='+clId, false, false, '', 'Error', true);
			companySpocJson =  $.parseJSON(strResponseText)
			$(this).populateAdvanceSelect($FI.find("#spocId"),companySpocJson,{"key":"spocId","value":"spocName"});
			$FI.find("#spocId").val(primarySpoc)
			//	FI.find("#spocNamePer").val('#spocIdPer');

		}
	}

	return false;
};


$.fn.addDomainN = function(){
	var FI = $("#addDomainmodalDiv")

	$('#domainMstAddSpan').html('');
	$('#remarkDomAddSpan').html('');
	FI.find('#addDomainMstModel').resetMstDataForm();
	FI.find("#addDomainMstModel").modal('show');
}


$.fn.saveAddDomainMst = function(){	
	event.preventDefault();
	var skillDomainName = $FI.find("#skillDomainName").val()
	var FI= $(this).parents('.modal-content').find('form'); 
	if(!FI.ValidateForm()) {
		return false;
	}
	var operationDomObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	operationDomObj.isActive = isActive;
	var domainAddJson = JSON.stringify(operationDomObj);
	$(this).cAjaxPostJson('Post','./job/saveSkillDomainMst',domainAddJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Skill Domain Name!", "", "warning");  
	} else if(strResponseText) { 
		var domainReturn = $.parseJSON(strResponseText);
		if(domainReturn.sdmId != null && domainReturn.sdmId > "0"){
			swal("Saved!", "Skill Domain Name saved successfully.", "success");
			$('#addDomainMstModel').modal('hide');
			$(".ag-center-cols-container").css('width','100%')

			$(this).cAjax('GET','./job/getAllSkillDoaminName', '', false, false, '', 'Error', true);
			skilldomname =  $.parseJSON(strResponseText)
			$(this).populateMultipleSelect($FI.find("#skillDomainName"),skilldomname,{"key":"sdmId","value":"skillDomainName"});
			$FI.find("#skillDomainName").val(skillDomainName)
		}
	}
	return false;
};


$.fn.addSkillN = function(){
	var FI = $("#addSkillmodalDiv")

	$('#skillMstAddSpan').html('');
	$('#remarkSkillAddSpan').html('');
	FI.find('#addSkillMstModel').resetMstDataForm();


	$(this).cAjax('GET','./job/getAllSkillDoaminName', '', false, false, '', 'Error', true);
	skilldomname =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect(FI.find("#sdmIdSk"),skilldomname,{"key":"sdmId","value":"skillDomainName"});

	FI.find("#addSkillMstModel").modal('show');
}


$.fn.saveAddSkillMst = function(){	
	event.preventDefault();
	var skillName = $FI.find("#skillName").val()
	var FI= $(this).parents('.modal-content').find('form'); 
	if(!FI.ValidateForm()) {
		return false;
	}
	var operationSkillObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	var sdmId= $("#sdmIdSk").val();
	operationSkillObj.isActive = isActive;
	operationSkillObj.sdmId = sdmId;
	var skillAddJson = JSON.stringify(operationSkillObj);
	$(this).cAjaxPostJson('Post','./job/saveSkillMst',skillAddJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Skill Name!", "", "warning");  
	} else if(strResponseText) { 
		var skillReturn = $.parseJSON(strResponseText);
		if(skillReturn.smId != null && skillReturn.smId > "0"){
			swal("Saved!", "Skill Name saved successfully.", "success");
			$('#addSkillMstModel').modal('hide');
			$(".ag-center-cols-container").css('width','100%')

			$(this).cAjax('GET','./job/getAllskillView', '', false, false, '', 'Error', true);
			domainArr =  $.parseJSON(strResponseText)
			$(this).populateAdvanceSelect($FI.find("#skillName"),domainArr,{"key":"smId","value":"skillName"});
			$FI.find("#skillName").val(skillName)
		}
	}
	return false;
};



$.fn.addClient = function(){
	var FI = $("#clientmodalDiv")
	FI.find('#companyTyp').val("");
	FI.find('#companyTyp').select2();
	$('#clientMstSpan').html('');
	$('#remarkclientMSpan').html('');
	FI.find('#addclientModal').resetMstDataForm();
	$(this).cAjax('GET','./job/getAllCityView', '', false, false, '', 'Error', true);
	locNameJson =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect(FI.find("#locId"),locNameJson,{"key":"lmId","value":"cityName"});

	$(this).cAjax('GET','./master/getDomainDropdownVw', '', false, false, '', 'Error', true);
	domainJson =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect(FI.find("#dmId"),domainJson,{"key":"dmId","value":"domainName"});

	FI.find("#addclientModal").modal('show');

};

$.fn.addSpocName = function(){
	var $FI = $(this).closest('form');
	 var FI= $("#spocmodalDiv");
	 var clientId = $("#clientMstForm input[name=clId]").val();
     
	
	$('#spmSpan').html('');
	$('#emailSpmSpan').html('');
	$('#phoneSpmSpan').html('');
	$('#remarkSpmSpan').html('');
	FI.find('#addspocMstModel').resetMstDataForm();

	$(this).cAjax('GET','./job/getAllCompanyNameView', '', false, false, '', 'Error', true);
	clientname =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect(FI.find("#clId"),clientname,{"key":"clId","value":"companyName"});
	var clid = $FI.find("#clId").val();
	FI.find("#clId").val(clid).attr("disabled","true").trigger('change');
	
	FI.find("#addspocMstModel").modal('show');
}

$.fn.addDescription = function() {
	var FI = $("#descriptionmodalDiv")
	$('#descriptionSpan').html('');
	var text = document.getElementById("jobDesc").value;
	document.getElementById("jobDesciption").value = text;	
	FI.find("#adddescripModal").modal('show');	
}

$.fn.savedescript = function() {
	var text = document.getElementById("jobDesciption").value;
	document.getElementById("jobDesc").value = text;
	$('#adddescripModal').modal('hide');
}

$.fn.addSkillNote = function() {
	var FI = $("#skillNotemodalDiv")
	$('#skillNoSpan').html('');
	var text = document.getElementById("skillNote").value;
	document.getElementById("skillNo").value = text;	
	FI.find("#addSkillNoteModal").modal('show');	
}

$.fn.saveSkillNote = function() {
	var text = document.getElementById("skillNo").value;
	document.getElementById("skillNote").value = text;
	$('#addSkillNoteModal').modal('hide');
}

$.fn.addSkillDtlRow = function(){
	var FI = $(this).closest('form');

	var skillrow = $("#skillDtlBody tr");
	for(var i = 0;i < skillrow.length;i++){

//		var skillName = $.trim(skillrow.eq(i).find('#skillName').val())
//		if(skillName == "" ||skillName == undefined||skillName == null){
//		swal("Please fill all fields")
//		return false;
//		}

		var minExp = $.trim(skillrow.eq(i).find('#minExp').val())
		if(minExp == "" ||minExp == undefined||minExp == null){
			swal("Please fill all fields")
			return false;
		}

		let mandatoryskill = $.trim(skillrow.eq(i).find('#mandatoryskill').val())
		if(mandatoryskill == "" ||mandatoryskill == undefined||mandatoryskill == null){
			swal("Please fill all fields")
			return false;
		}

	}

	var selhtml = `<select class="form-control form-control-sm select2" onchange="$(this).setSdmId();" style="width: 100%;"  name="jobSkills[0][smId]" id="skillName" mandatory="true" message="Please Select Skill Name"><option value="">-- Select --</option></select>
		<input type="hidden" id="sdmId" name="jobSkills[0][sdmId]">`

		var selhtml1 = `<select class="form-control form-control-sm select2" id="minExp" name="jobSkills[0][minExp]">
			<option value="">---Select---</option>
			</select>`	

			var selhtml2 = `<input type="hidden" id="isActive" name="jobSkills[0][isActive]" value="Y">
				<select class="form-control form-control-sm select2" id="mandatoryskill" name="jobSkills[0][isMandatory]">

				<option value="Y">Yes</option>
				<option value="N">No</option>
				</select>`	

				var clonedRow = $(this).closest(".skillDtlRow").clone(true);
	clonedRow.find("td:first").html(selhtml);
	clonedRow.find("td").eq(1).html(selhtml1);
	clonedRow.find("td").eq(2).html(selhtml2);
	FI.populateAdvanceSelect(clonedRow.find("#skillName"),domainArr,{"key":"smId","value":"skillName"});
	FI.populateAdvanceSelect(clonedRow.find("#minExp"),minExpJson,{"key":"values","value":"displayName"});
	clonedRow.find("#skillName").select2();
	clonedRow.find("#minExp").select2();
	clonedRow.find("#mandatoryskill").select2();
//	clonedRow.find("#isMandatory").select2();
	resetRow(clonedRow);
	clonedRow.find("#isActive").val("Y")
	FI.find("#skillDtlBody").append(clonedRow);
	FI.find("#skillDtlBody tr:last").find("#mandatoryskill").val('N').select2();

}

$.fn.addEduDtlRow = function(){
	var FI = $(this).closest('form');

	var eduDtlBody = $("#eduDtlBody tr");
	for(var i = 0; i < eduDtlBody.length; i++){

		var education =  eduDtlBody.eq(i).find("#emId").val();
		if(education == ""||education == null){
			swal("Please fill all fields")
			return false;
		}

//		var courseNameQ =  eduDtlBody.eq(i).find("#coId").val();
//		if(courseNameQ == ""||courseNameQ == null){
//		swal("Please fill all fields")
//		return false;
//		}

		var eduMandatory =  eduDtlBody.eq(i).find("#eduMandatory").val();
		if(eduMandatory == ""||eduMandatory == null){
			swal("Please fill all fields")
			return false;
		}
	};

	var selhtml = `<select class="form-control form-control-sm select2" style="width: 100%;"  name="jobEdu[0][emId]" id="emId"><option value="">-- Select --</option></select>`

		var selhtml1 = `<select class="form-control form-control-sm select2" id="coId" name="jobEdu[0][coId]">
			<option value="">---Select---</option>
			</select>`	


			var selhtml3 = `<input type="hidden" id="isActive" name="jobEdu[0][isActive]" value="Y">
				<select class="form-control form-control-sm select2" id="eduMandatory" name="jobEdu[0][isMandatory]">

				<option value="Y">Yes</option>
				<option value="N">No</option>
				</select>`	

				var clonedRow = $(this).closest(".eduDtlRow").clone(true);
	clonedRow.find("td:first").html(selhtml);
	clonedRow.find("td").eq(1).html(selhtml1);
//	clonedRow.find("td").eq(2).html(selhtml2);
	clonedRow.find("td").eq(3).html(selhtml3);
	FI.populateAdvanceSelect(clonedRow.find("#emId"),emIdJson,{"key":"emId","value":"eduLevel"});
	FI.populateAdvanceSelect(clonedRow.find("#coId"),coIdNameJson,{"key":"coId","value":"coursename"});
	clonedRow.find("#emId").select2();
	clonedRow.find("#coId").select2();
	clonedRow.find("#eduMandatory").select2();
	resetRow(clonedRow);
	clonedRow.find("#isActive").val("Y")
	FI.find("#eduDtlBody").append(clonedRow);
	FI.find("#eduDtlBody tr:last").find("#eduMandatory").val('N').select2();

}


$.fn.addCertDtlRow = function(){
	var FI = $(this).closest('form');

	var certrow = $("#certDtlBody tr");
	for(var i = 0;i < certrow.length;i++){

		var crtnameBox = $.trim(certrow.eq(i).find('#certifiName').val())
		if(crtnameBox == "" ||crtnameBox == undefined||crtnameBox == null){
			swal("Please fill all fields")
			return false;
		}

		var crtBy = $.trim(certrow.eq(i).find('#certifedBy').val())
		if(crtBy == "" ||crtBy == undefined||crtBy == null){
			swal("Please fill all fields")
			return false;
		}

		var certMandatory = $.trim(certrow.eq(i).find('#certMandatory').val())
		if(certMandatory == "" ||certMandatory == undefined||certMandatory == null){
			swal("Please fill all fields")
			return false;
		}

	}
	var selhtml = `<input class="form-control form-control-sm" name="jobCert[0][certifiName]" id="certifiName" type="text" placeholder="Certificate Name" >`

		var selhtml1 = `<input type="hidden" id="isActive" name="jobCert[0][isActive]" value="Y">
			<select class="form-control form-control-sm select2" id="certMandatory" name="jobCert[0][isMandatory]">

			<option value="Y">Yes</option>
			<option value="N">No</option>
			</select>`	

			var clonedRow = $(this).closest(".certDtlRow").clone(true);
	clonedRow.find("td:first").html(selhtml);
	clonedRow.find("td").eq(3).html(selhtml1);
	clonedRow.find("#certMandatory").select2();
	resetRow(clonedRow);

	clonedRow.find("#isActive").val("Y")
	FI.find("#certDtlBody").append(clonedRow);
	FI.find("#certDtlBody tr:last").find("#certMandatory").val('N').select2();

}

$.fn.addSpocSecDtlRow = function(){
	var FI = $(this).closest('form');

	var selhtml = `<select class="form-control form-control-sm select2" name="jobSpoc[0][spocId]" id="spocIdSec"><option value="">-- Select --</option></select>`	

		var selhtml1 = `<select class="form-control form-control-sm select2" id="category" name="jobSpoc[0][category]">
			<option value="">---Select---</option>
			<option value="P">Primary </option>
			<option value="S">Secondary</option>
			</select>`;

	var selhtml2 = `<input type="hidden" id="isActive" name="jobSpoc[0][isActive]" value="Y">
		<input class="form-control form-control-sm" type="checkbox" id="isMandatory" name="jobSpoc[0][isMandatory]" value="Y" style="margin-left: -25px;">`	

		var clonedRow = $(this).closest(".spocSecDtlRow").clone(true);
	clonedRow.find("td:first").html(selhtml);
	clonedRow.find("td").eq(1).html(selhtml1);
	clonedRow.find("td").eq(3).html(selhtml2);
	FI.populateAdvanceSelect(clonedRow.find("#spocIdSec"),secSpocJson,{"key":"spocId","value":"spocName"});
	clonedRow.find("#spocIdSec").select2();
	resetRow(clonedRow);
	clonedRow.find("#isMandatory").val("Y")
	clonedRow.find("#isActive").val("Y")
	FI.find("#spocSecDtlBody").append(clonedRow);
}

$.fn.removeSkillDtlRow = function(){
	var FI = $(this).closest('form');	
	if(FI.find("#skillDtlBody").find('tr').length > 1){
		$(this).closest('tr').remove();
	}
	else{
		swal("You can not remove first row");
	}
}

$.fn.removeEduDtlRow = function(){
	var FI = $(this).closest('form');	
	if(FI.find("#eduDtlBody").find('tr').length > 1){
		$(this).closest('tr').remove();
	}
	else{
		swal("You can not remove first row");
	}
}

$.fn.removeCertDtlRow = function(){
	var FI = $(this).closest('form');	
	if(FI.find("#certDtlBody").find('tr').length > 1){
		$(this).closest('tr').remove();
	}
	else{
		swal("You can not remove first row");
	}
}

$.fn.removeSpocSecDtlRow = function(){
	var FI = $(this).closest('form');	
	if(FI.find("#spocSecDtlBody").find('tr').length > 1){
		$(this).closest('tr').remove();
	}
	else{
		swal("You can not remove first row");
	}
}

$.fn.ResetAllDataInFormjobmst=function(){
	var Form=$(this).closest('form')
	$(this).ResetAllDataInForm(Form)
};

$.fn.companySpocName = function(){
	var FI = $(this).closest('form')
	var comName = FI.find('#clId').val()

	$(this).cAjax('GET','./job/getAllClientSpocView', 'clId='+comName, false, false, '', 'Error', true);
	companysp =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect($FI.find("#companySpocSel"),companysp,{"key":"clId","value":"companySpoc"});
}


$.fn.internalName = function(){
	var FI = $(this).closest('form')
	var intName = FI.find('#internalRec').val()
	var blanckAttr = [];


	if(intName.length > 0 ){
		$(this).cAjax('GET','./job/getAllrecugrpmap', 'rgm_id='+intName, false, false, '', 'Error', true);
		internalName =  $.parseJSON(strResponseText)
		$(this).populateMultipleSelect(FI.find("#internalRecname"),internalName,{"key":"userid","value":"displayname"});
	} else if(intName.length == 0) {

		$(this).populateMultipleSelect(FI.find("#internalRecname"),blanckAttr,{"key":"userid","value":"displayname"});

	}

}


$.fn.externalName = function(){
	var FI = $(this).closest('form')
	var extName = FI.find('#externalRec').val()
	var blanckAttr = [];

	if(extName.length > 0){
		$(this).cAjax('GET','./job/getAllrecugrpmap', 'rgm_id='+extName, false, false, '', 'Error', true);
		externalName =  $.parseJSON(strResponseText)
		$(this).populateMultipleSelect(FI.find("#externalRecname"),externalName,{"key":"userid","value":"displayname"});
	}else if(extName.length == 0) {

		$(this).populateMultipleSelect(FI.find("#externalRecname"),blanckAttr,{"key":"userid","value":"displayname"});
		//FI.find("#externalRec").text("-- Select --");
	}
}

$.fn.CheckSkill = function(){
	var FI = $(this).closest('form')
	var skill = $("#skillDtlBody tr")
	var disableArr =[];
	for(var id=0; id<skill.length;id++){
		//$('#aioConceptName').find(":selected").text();
		var skillId = skill.eq(id).find("#skillName :selected").val();
		skill.find("option[value='" + skillId + "']").attr("disabled",true);
		//skill.find('select').select2();
	}	
}

$.fn.disableMaxExp = function(){
	var FI = $(this).closest('form')

	var minExpVal = FI.find("#minTotalExp").val();

	$(this).cAjax('GET','./job/ExperienceDropDownVw', '', false, false, '', 'Error', true);
	maxTotalExpJson =  $.parseJSON(strResponseText)
	var finalMaxExp = maxTotalExpJson.filter(maxexp => maxexp.values >= minExpVal)

	$(this).populateAdvanceSelect($FI.find("#maxTotalExp"),finalMaxExp,{"key":"values","value":"displayName"});

	//$(this).cAjax('GET','./master/getSpocMstListVw', '', false, false, '', 'Error', true);
	//	companySpocJson =  $.parseJSON(strResponseText)
}



$.fn.CheckEducation = function(){
	var FI = $(this).closest('form')
	var edu = FI.find("#eduDtlBody tr")

	for(var id=0; id<edu.length;id++){	
		var courseNameQId = edu.eq(id).find("#coId :selected").val();
		edu.find("#coId").find("option[value='" + courseNameQId + "']").attr("disabled",true);

	}	
}

$.fn.CheckSecondarySpoc = function(){
	var FI = $(this).closest('form')
	var prySpoc = FI.find("#spocId").val()
	var clId = FI.find("#clId").val();

	$(this).cAjax('GET','./master/getSpocMstListVw', 'clId='+clId, false, false, '', 'Error', true);
	secSpocJson =  $.parseJSON(strResponseText)
	var spocSecondary = secSpocJson.filter(secSpoc => secSpoc.spocId !=  prySpoc)
	$(this).populateAdvanceSelect($FI.find("#secondaryspocId"),spocSecondary,{"key":"spocId","value":"spocName"});

	FI.find("#secondaryspocId").select2();

}

$.fn.onChangeClientName = function(){
	var FI = $(this).closest('form')
	var clId = FI.find("#clId").val();

	$(this).cAjax('GET','./master/getSpocMstListVw', 'clId='+clId, false, false, '', 'Error', true);
	secSpocJson =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect($FI.find("#spocId"),secSpocJson,{"key":"spocId","value":"spocName"});

	FI.find("#spocId").select2();
}


$.fn.onClientChange = function(){
	var FI = $(this).closest('form')
	var clId = FI.find("#clId").val();
	
	$(this).cAjax('GET','./job/getFormatView', 'clId='+clId, false, false, '', 'Error', true);
	formatname =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect(FI.find("#formatId"),formatname,{"key":"fmId","value":"name"});
}