var cstleaddispid=14;
var cstleadGridOptions;
$.fn.getcustomerhist = function() {
	
$(this).addTab("Customer_lead",'./pages?name=masters/cstList',"Customer Lead");
}
$.fn.cstleadmstList = function($FI) {
	var FI = $("#" +$FI)
	cstleadGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			

		};
	var listDiv = FI.find("#cstleadLst");
	$(this).reloadGrid("cstleadLst", cstleaddispid, cstleadGridOptions,$FI , whereClause);
	FI.find("#cstleadLst").closest('form').attr("oninput","$(this).oncstleadFilterTextBoxChanged()");
}

$.fn.oncstleadFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	cstleadGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}