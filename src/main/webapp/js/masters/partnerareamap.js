var ptnrlistdispid=1;
var ptnrListGridOptions;
$.fn.getpartnerareamap = function() {
$(this).addTab("PARTNER_AREA_MAP",'./pages?name=masters/partnerareamaplist',"Partner Area");
}
$.fn.PtnrList = function() {
	ptnrListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			

		};
	$(this).reloadGrid("PtnrAreaMapLst", ptnrlistdispid, ptnrListGridOptions);
	$("#PtnrAreaMapLst").closest('form').attr("oninput","$(this).onPtnrFilterTextBoxChanged()");
}

$.fn.onPtnrFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	ptnrListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}