var prodbranddispid=2;
var prodmstGridOptions;
$.fn.getprodbrandmst = function() {
$(this).addTab("Product_Brand",'./pages?name=masters/prodbrandmstlist',"Product Brand");
}
$.fn.ProdBrmstList = function() {
	prodmstGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			

		};
	$(this).reloadGrid("ProdBrandMstLst", prodbranddispid, prodmstGridOptions);
	$("#ProdBrandMstLst").closest('form').attr("oninput","$(this).onProdbrandFilterTextBoxChanged()");
}

$.fn.onProdbrandFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	prodmstGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}