var cstmstdispid=10;

var cstmstGridOptions;

$.fn.getcustomermaster = function() {
	
$(this).addTab("Customer_Master",'./pages?name=masters/cstmstlist',"Customer Master");
}

$.fn.cstmstList = function() {
	cstmstGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			onRowDoubleClicked: customerlead,
	};
	$(this).reloadGrid("CstMstLst", cstmstdispid, cstmstGridOptions);		
	
	$("#CstMstMap .btn-container .btn").on('click touchstart',function(){
		
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(cstmstGridOptions)[0].cs_id;
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			$(this).addTab("AddCustomer",'./pages?name=masters/addeditcstmst',"Add Customer",{"action":"new"});
			break;
		case "edit":
			$(this).addTab("EditCustomer"+data,'./pages?name=masters/addeditcstmst',"Edit Customer-"+data,{"data":data,"action":"edit"});
			break;
		case "view":
			$(this).addTab("ViewCustomer"+data,'./pages?name=masters/addeditcstmst',"View Customer-"+data,{"data":data,"action":"view"});
			break;
			
		case "delete":
			swal({
				title: "Are you sure want to delete Customer ?",
				text: "You will not be able to recover Customer  now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false	,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if (isConfirm) {
					$(this).cAjax('POST','./customer/deleteCustomer',"pmId="+data, false, false, '', 'Error', true);
					if(strResponseText == "Success"){
						swal("Deleted!", "Selected record deleted successfully.", "success");
						$(this).reloadGrid("CstMstLst", cstmstdispid, cstmstGridOptions);
					} } 
				else {
					swal("Cancelled", "Thank you! your Customer is safe :)", "error");
				}
			});
			break;
		 case "refresh":
			 $(this).reloadGrid("CstMstLst", cstmstdispid, cstmstGridOptions);
	        	swal("Refresh!","Customer Refresh Successfully.", "success");
	        	break;
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});
	
	//$("#CstMstLst").closest(".row").addClass("listContainer");
	$("#CstMstLst").closest('form').attr("oninput","$(this).onCstmstFilterTextBoxChanged()");
}

$.fn.onCstmstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	cstmstGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}



$.fn.prepareCstMstForm = function($FI){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined,openingStockNumber = '';
	var formContainer = $FI;
	
	
	if(option.data){
		$FI.find("#cstmststatus .cstmstactivestatus").attr("id", "cstmstactivestatus"+option.data)
		$FI.find("#cstmststatus #cstmstactivelabel").attr("for","cstmstactivestatus"+option.data);
		$FI.find("#cstmststatus .cstmstinactivestatus").attr("id", "cstmstinactivestatus"+option.data)
		$FI.find("#cstmststatus #cstmstinactivelabel").attr("for","cstmstinactivestatus"+option.data);
	}
	else{
		$FI.find("#cstmststatus .cstmstactivestatus").attr("id", "cstmstactivestatus")
		$FI.find("#cstmststatus #cstmstactivelabel").attr("for","cstmstactivestatus");
		$FI.find("#cstmststatus .cstmstinactivestatus").attr("id", "cstmstinactivestatus")
		$FI.find("#cstmststatus #cstmstinactivelabel").attr("for","cstmstinactivestatus");
	}
	
	
	
	$FI.cAjax('POST','./mstData/getStates', '', false, false, '', 'Error', true);
	cststates =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#state"),cststates,{"key":"stateid","value":"statename"});
	
	
	$FI.cAjax('GET','./partner/areas', '', false, false, '', 'Error', true);
	areas =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#am_id"),areas,{"key":"amId","value":"areaName"});
	
	
	$FI.find("#saveCstMst").on('click touchstart',function(event){
		if(formContainer.ValidateForm()){
			var cstMstFormJson = $.parseJSON(serializeJSONIncludingDisabledFields($FI));
			$(this).cAjaxPostJson('Post','./customer/saveCustomer',JSON.stringify(cstMstFormJson), false, false, '', 'Error', true);
			if(strResponseText){
				
				var cstMstReturn = $.parseJSON(strResponseText);
				if(cstMstReturn.csId){
					swal("Saved!", "Customer saved successfully.", "success");
					$FI.closeUserTab();
					$(this).getcustomermaster()
					$(this).reloadGrid("CstMstLst", cstmstdispid, cstmstGridOptions);
					return true;
				}
				else{
					swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					return false;
				}
			}
			else{
					swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					return false;
				}
		}
	})
	
	if(keyValue){
		$(this).cAjax('GET','./customer/getCustomer',"csId="+keyValue, false, false, '', 'Error', true);
		var cstMst = $.parseJSON(strResponseText);
		$(this).fillCstMstForm($FI,cstMst);
		var action = option ? option.action : undefined;
		if(action && action=="view" ){
			$FI.find("input,select,button").attr("disabled","disabled")
		}else if(action && action=="edit"){
			$FI.find("#deptName").attr("disabled","disabled");
		}

	}
	
	
}

$.fn.fillCstMstForm = function($FI,cstMst){
	$FI.find("#csId").val(cstMst.csId);
	$FI.find("#name").val(cstMst.name);
	$FI.find("#displayName").val(cstMst.displayName);
	$FI.find("#address").val(cstMst.address);
	$FI.find("#landmark").val(cstMst.landmark);
	
	$FI.find("#state").val(cstMst.state);
	$FI.find("#state").select2();
	$FI.find("#state").trigger('change');
	$FI.find("#city").val(cstMst.city);
	$FI.find("#city").select2();
	$FI.find("#city").trigger('change');
	$FI.find("#city").val(cstMst.city);
	$FI.find("#pinCode").val(cstMst.am_id);
	$FI.find("#phone").val(cstMst.phone);
	$FI.find("#mobile").val(cstMst.mobile);
	$FI.find("#email").val(cstMst.email);
	$FI.find("#remarks").val(cstMst.remarks);
	$FI.find("#am_id").val(cstMst.am_id);
	$FI.find("#am_id").select2();
	$FI.find("[value='"+cstMst.isActive+"'][name='isActive']").attr('checked','checked')
}



$.fn.oncstcityChange = function(){
	var $FI = $(this).closest('form');
	var cityid = $(this).val();
	$(this).cAjax('GET','./partner/areas', 'cityid='+cityid, false, false, '', 'Error', true);
	pinarea =  $.parseJSON(strResponseText);
	
	$FI.populateAdvanceSelect($FI.find("#pinCode"),pinarea,{"key":"amId","value":"pincode"});
	
};

function customerlead(row) {
	var rowData = row.data;
	var cs_id = rowData.cs_id;
	whereClause = '"cs_id"='+cs_id;
	$(this).getcustomerhist(cs_id);
}

$.fn.oncstpinchange = function(){
	var $FI = $(this).closest('form');
	
	var selectpin = $('#pinCode :selected').text();
	var replacepin = selectpin.split(" ",1)
	var strpin = (replacepin.toString())
	var finalpin = strpin.replace("("," ")
	$FI.find("#cstpin").val(finalpin)
}


$.fn.ResetAllDataInFormcstmst=function(){
	var Form=$(this).closest('form')
	$(this).ResetAllDataInForm(Form)
};