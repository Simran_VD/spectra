areamasterListId = 12;
var areamasterGridOptions;
$.fn.getarea = function(){
	$(this).addTab("getarea",'./pages?name=/masters/areaMaster',"Area Master");
};

$.fn.areamasterListing = function(){
	areamasterGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',

		};
$(this).reloadGrid("areamasterList", areamasterListId, areamasterGridOptions);
	
	$("#areamasterListContainer .btn-container .btn").on('click touchstart',function(){
		var opration=$(this).attr("title"),data;
		opration=opration.toLocaleLowerCase();
		if(opration != "add new"){
			
			
			try{
				var data = onSelectionChanged(areamasterGridOptions)[0].am_id;
			}
			catch(e){
				var data = undefined;
			}
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(opration) {
			case "add new":
				$('#areamasterModal').ResetAllElemOfareaForm();
				$('#areamasterModal').modal('show');
				
				$('#areamasterModal').find("#areamstActive").val("Y");
				$('#areamasterModal').find("#areamstInactive").val("N");
				$('#areamasterModal').fillcityDropDown();
				break;
			case "edit":
			case "view":
				$(this).cAjax('GET', './product/getAreaMst',"amId="+data, false, false, '','Error', true);
				var areamstJson = $.parseJSON(strResponseText);
				$('#areamasterModal').modal('show')
				$('#areamasterModal').fillcityDropDown();
				$('#areamasterModal').fillareamstForm(areamstJson, opration);
				
					break;
			case "delete":
				swal({
					title: "Are you sure Do you want  to delete Area?",
					text: "You will not be able to recover Area now!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No cancel please!",
					confirmButtonColor:'#3095d6',
					cancelmButtonColor:'#d33',
					closeOnConfirm: false,
					closeOnCancel: false,
					showConfirmButton: true,
				},
				function(isConfirm) {
					if (isConfirm) {
				$(this).cAjax('Post','./product/deleteAreaMst',"amId="+data, false, false, '', 'Error', true);
				if(strResponseText == "SUCCESS"){
					swal("Deleted!", "Selected Area deleted successfully.", "success");
					$(this).reloadGrid("areamasterList", areamasterListId, areamasterGridOptions);
				} else if(strResponseText == "DATA_INTEGRITY"){
					swal("Error in deletion!", "Selected Area is being used. So it cannnot be deleted.", "success");
				}
					}
					else {
						swal("Cancelled", "Thank you! your Area is safe :)", "error");
					}});
				break;
				
//			case "print":
//				var rptUrl = reportingUrl + "/MasterData/designation.rptdesign&__format=pdf";
//				window.open(rptUrl);
//				break;
				
			default:
				swal("Not implemented.");
		}
		return false;
	});
	$("#areamasterListContainer").closest(".row").addClass("listContainer");
	$("#areamasterList").closest('form').attr("oninput","$(this).onareamstFilterTextBoxChanged()");

};


$.fn.onareamstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	areamasterGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}


$.fn.fillcityDropDown = function() {
	$(this).cAjax('Get','./GetCities', '', false, false, '', 'Error', true);
	city = $.parseJSON(strResponseText);
	$(this).populateAdvanceSelect($(this).find("#city"), city, {"key":"key","value":"displayValue"});
	
}

$.fn.ResetAllElemOfareaForm=function(){
	var FI=$("#areamasterForm")
	$(this).ResetAllElemOfForm(FI)
};



$.fn.fillareamstForm = function(areamstJson, opration) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#amid").val(areamstJson.amId);
	$(this).find("#siteid").val(areamstJson.siteId);
	$(this).find("#areaname").val(areamstJson.areaName);
	$(this).find("#city").val(areamstJson.cityid).select2();
	$(this).find("#pincode").val(areamstJson.pincode);
	$(this).find("#arearmrk").val(areamstJson.remarks);
	
	
    
    if(areamstJson.isActive == 'Y') {
        $("#areamstActive").prop("checked", true)
	} else {
        $("#areamstInactive").prop("checked", true)     
	}

	if (opration && opration == "view") {
		$(this).find("input, select, button").attr("disabled", "disabled");
		$(this).find("#closeDesignationBtn1").attr("disabled", false);
		$(this).find("#closeDesignationBtn2").attr("disabled", false);
		$(this).find("#saveareamst").attr("disabled", "disabled");
	} else if (opration && opration == "edit") {
		//$(this).find("#slabNo").attr("disabled", true);
		//$(this).find("#saveareamst").attr("disabled", true);
	}
	
};

$.fn.saveareaMaster = function() {
	var FI= $(this).closest('form');
	if(!FI.ValidateForm() ){//|| !$(this).validate(FI)) {
		return false;
	}
	var areamstJson = (serializeJSONIncludingDisabledFields(FI));
	
	
	$(this).cAjaxPostJson('Post','./product/saveAreaMst',(areamstJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Area!", "", "warning");  
	} else  if(strResponseText) { 
		var areamst = $.parseJSON(strResponseText);
		if(areamst.amId){
			swal("Saved!", "Area  saved successfully.", "success");
			$('#areamasterModal').modal('hide');
			$(this).getarea()
			$(this).reloadGrid("areamasterList", areamasterListId, areamasterGridOptions);
			$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};	
