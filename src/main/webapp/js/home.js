/*
 * Self exiecuting function call on after login.
 * 
 */
(function ($) {
    //Here jQuery 
	//$(this).createMenuBar();
	$(this).loadUserModule()
	$(this).addTab("Dashboard",'./pages?name=dashboard',"Dashboard");
	$(this).addButton("ActiveJobs",'./pages?name=activeJobs',"Active Jobs");

    $(this).getUserName()
    
})(jQuery);


$.fn.saveChangedPass = function() {
	var FI = $(this).closest('form');
	var oldPass = FI.find("#oldPass").val();
	var pass = FI.find("#pass").val();
	var rePass = FI.find("#rePass").val();
	if(FI.ValidateForm()){

	if(pass == oldPass){
		FI.find("#oldPassErr").css('display','');
		return false;
	}
	
	if(pass != rePass){
		FI.find("#passInvalid").css('display','');
		return false;
	}
	//var changePass =serializeJSONIncludingDisabledFields(FI);
	FI.cAjax('POST','./changePassword?oldpswd='+oldPass+'&pswd='+pass,'', false, false, '', 'Error', true);
	var changePassReturn = (strResponseText);
	if(changePassReturn == "INVALID_PWD"){
		FI.find("#invldCrrPwd").css('display','');
		return false;
	}
	if(changePassReturn == 'Old Password is not correct.'){
		FI.find("#invldCrrPwd").css('display','');
		return false;
	}
	if(changePassReturn == 'SUCCESS'){
		window.location = './logout'
	}
	if(changePassReturn == 'Password changed successfully.'){
		window.location = './logout'
	}
	if(changePassReturn == ""){
		swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
	}
}	
}

/*$.fn.saveUnChangedPass = function() {
	var FI = $(this).closest('form');
	var oldPass = '';
	var pass = '';
	var rePass = '';
	FI.cAjax('POST','./changePasswordWAC?oldpswd='+oldPass+'&pswd='+pass,'', false, false, '', 'Error', true);
	var unChangePassReturn = (strResponseText);
	if(unChangePassReturn == 'Password changed successfully.'){
		window.location = './logout';
	}
}*/

$.fn.onRePassChange = function() {
	var FI = $(this).closest('form');
	FI.find("#passInvalid").css('display','none');
	FI.find("#oldPassErr").css('display','none');
	FI.find("#invldCrrPwd").css('display','none');
}

$.fn.changePass = function() {
	$(this).cAjax('POST','./FirstTimeChangePassword', '', false, false, '', 'Error', true);
	var newUser = (strResponseText);
	if(newUser == 'firstTimeUser'){
		$("#changePass").modal({
            backdrop: 'static',
            keyboard: false
        });
		/*$("#changePass").modal('show');*/
	}
}

$.fn.getBaseAmt = function(){
	$(this).cAjax('POST','./admin/getUserName', '', false, false, '', 'Error', true);
	User =  $.parseJSON(strResponseText);
	var userId = User.uiid;
	if(userId){
		$(".totalBal").css('display','');
		$(this).cAjax('GET','./partner/getPartnerBal', 'pmId='+userId, false, false, '', 'Error', true);
		if(strResponseText){
			var baseAmt =  $.parseJSON(strResponseText);
	    	$("#totBal").text(baseAmt.closBal);
		}
	    
	}
	else{
		$(".totalBal").css('display','none');
	}
	
}
