let departments = [];
var userListId = 2;
var userGridOptions;
$.fn.getuserlist = function(){
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("UserListing",'./pages?name=spectra/userList',"Users");
};


$.fn.addUsersList = function(){
	userGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',

		};

	$(this).reloadGrid("UserLst", userListId, userGridOptions);
	$("#userlistContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new"){
			
			
			try{
				var data = onSelectionChanged(userGridOptions)[0]['user_id']
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
			var userName = onSelectionChanged(userGridOptions)[0]["Name"];
		}
		switch(operation) {
		case "add new":
			$(this).addTab("AddUser",'./pages?name=spectra/addEditUsers',"Add User", {"action":"new"});
			break;
		case "edit":
			$(this).addTab("EditUser"+data,'./pages?name=spectra/addEditUsers',"Edit User-"+data,{"data":data,"action":"edit"});
			break;
		case "newas":
			$(this).addTab("NewAsUser"+data,'./pages?name=spectra/addEditUsers',"NewAs User-"+userName,{"data":data,"action":"newas"});
			break;
		case "view":
			$(this).addTab("ViewUser"+data,'./pages?name=spectra/addEditUsers',"View User-"+data,{"data":data,"action":"view"});
			break;
		case "delete":
			swal({
				title: "Are you sure? want delete User?",
				text: "You will not be able to recover User now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false	,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if (isConfirm) {
                
			$(this).cAjax('Get','./admin/deleteUser',"id="+data, false, false, '', 'Error', true);
			if(strResponseText == "SUCCESS"){
				swal("Deleted!", "Selected user deleted successfully.", "success")
				$(this).reloadGrid("UserLst", userListId, userGridOptions);
			} else if(strResponseText == "DATA_INTEGRITY"||strResponseText == ""){
				swal("Error in deletion!", "Selected user is being used. So it cannot be deleted.", "error");
			}
				} else {
					swal("Cancelled", "Thank you! your user is safe :)", "error");
				}
			});
			break;
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});
	$("#UserLst").closest(".row").addClass("listContainer");
	$("#UserLst").closest('form').attr("oninput","$(this).onUseryFilterTextBoxChanged()");
};

$.fn.prepareAddUserForm = function($FI)
{
   var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined,openingStockNumber = '';
	var formContainer = $FI;
}


$.fn.ResetAllDataInFormUser = function(){
	var Form = $(this).closest('form');
	$(this).ResetAllDataInForm(Form);


};

