$.fn.setService = function(id){
	var service = $(this).text();
	$('#serviceBtn').val("");
	$('#serviceBtn').trigger('change');
	$('#serviceBtn').val(service);
	var srvId = $(this).attr('servVal');
	$("#serviceId").val(srvId);
	var psmId = $(this).attr('psmId');
	$("#psmId").val(psmId)
}

$.fn.setCity = function(id){
	var city = $(this).text();
	$('#cityBtn').val("");
	$('#cityBtn').trigger('change');
	$('#cityBtn').val(city);
	var cityVal = $(this).attr('cityVal');
	var stateVal = $(this).attr('stateVal');
	$("#cityId").val(cityVal);
	$("#stateId").val(stateVal);
}

$.fn.setState = function(id){
	var state = $(this).text();
	var stateId = $(this).attr('stateVal');
	$('#stateBtn').val("");
	//$('#stateBtn').trigger('change');
	$('#stateBtn').val(state);
		var json = null;
	  $.ajax({
	    'async': false,
	    'global': false,
	    'url': "/js/website/citymaster.json",
	    'dataType': "json",
	    'success': function(data) {
	      json = data;
	    }
	  });
	var allcity = json;
	var allcity = allcity.filter(city => city.stateid == stateId)
	$("#cityItems").empty();
	$.each(allcity, function(i,val){
		$("#cityItems").append(`<a class="dropdown-item" cityVal="`+allcity[i].cityid+`" stateVal="`+allcity[i].stateid+`" onclick="$(this).setCity('serviceBtn')" href="#">`+allcity[i].cityname+`</a>`)
	})
}

$.fn.setProd = function(id){
	var prod = $(this).text();
	$('#prodBtn').val("");
	$('#prodBtn').trigger('change');
	$('#prodBtn').val(prod);
	var prodId = $(this).attr('prodVal');
	$("#productId").val(prodId);


	

	var services = null;
	  $.ajax({
	    'async': false,
	    'global': false,
	    'url': "/js/website/productservice.json",
	    'dataType': "json",
	    'success': function(data) {
	    	services = data;
	    }
	  });
	  
	  var fillServices = services
	  var filterServices = fillServices.filter(srv => srv.prdId == prodId)
	  $("#serviceItems").empty();
	  var changeServices = filterServices;
		$.each(changeServices, function(i,val){
			$("#serviceItems").append(`<a class="dropdown-item" psmId = "`+changeServices[i].psmId+`" servVal="`+changeServices[i].smId+`" onclick="$(this).setService('serviceBtn')" href="#">`+changeServices[i].serviceName+`</a>`)
	})

}


$.fn.onStateChange = function(){

}

$.fn.prepareWebsite = function(){
	
	

	var state = null;
	  $.ajax({
	    'async': false,
	    'global': false,
	    'url': "/js/website/state.json",
	    'dataType': "json",
	    'success': function(data) {
	    	state = data;
	    }
	  });
	  
	 var allstate = state;
	 $.each(allstate, function(i,val){
		$("#stateItems").append(`<a class="dropdown-item" onclick="$(this).setState('stateBtn')" stateVal="`+allstate[i].stateid+`" href="#">`+allstate[i].statename+`</a>`)	
			
	})

  	var product = null;
	$.ajax({
	    'async': false,
	    'global': false,
	    'url': "/js/website/product.json",
	    'dataType': "json",
	    'success': function(data) {
	    	product = data;
	    }
	});
	  
	var allproduct = product;
	$.each(allproduct, function(i,val){
		 $("#prodItems").append(`<a class="dropdown-item" prodVal="`+allproduct[i].prdId+`" onclick="$(this).setProd('prodBtn')" href="#">`+allproduct[i].prodName+`</a>`)
	})
		
		
	
	
	
	
	var ourServices = null;
	$.ajax({
	    'async': false,
	    'global': false,
	    'url': "/js/website/ourservices.json",
	    'dataType': "json",
	    'success': function(data) {
	    	ourServices = data;
	    }
	});
	
	
	
	
	$.each(ourServices,function(){
		var srvCard = $("#ourServices div:first").clone(true);
		srvCard.css('display','');
		srvCard.find('img').attr('src',this.image).show();
		srvCard.find('h6').text(this.header);
		srvCard.find('.subHeader').text(this.subHeader);
		srvCard.find('.textDiv p').text(this.text);
		$("#ourServices").append(srvCard);
	})
	$("#ourServices div:first").remove();
	
	var $FI = $("#bookAppForm");
	$FI.find("#bookApp").on('click touchstart',function(event){
		// $FI.find("#otpModal").modal('show');
		formContainer = $FI;
		var mblNum = formContainer.find('#usrMbl').val();
		if(formContainer.ValidateForm()){
			$(this).cAjax('GET','./website/sendOTP', 'mobile='+mblNum, false, false, '', 'Error', true);
			if(strResponseText){
				$FI.find("#otpModal").modal('show');
				$FI.find("#otpModal").find("#mblNum").text($FI.find("#usrMbl").val());
			}
		}
	});
	
	$FI.find("#verifyOtp").on('click touchstart',function(event){
		formContainer = $FI;
		var mblNum = formContainer.find('#usrMbl').val();
		var otp = formContainer.find('#otp').val();
		$(this).cAjax('GET','./website/varifyOtp', 'mobile='+mblNum+'&opt='+otp, false, false, '', 'Error', true);
		if(strResponseText){
			if(formContainer.ValidateForm()){
				var appFormJson = $.parseJSON(serializeJSONIncludingDisabledFields($FI));
				$(this).cAjaxPostJson('Post','./website/saveCustomer',JSON.stringify(appFormJson), false, false, '', 'Error', true);
				if(strResponseText){
					var appForm = $.parseJSON(strResponseText);
					if(appForm.slId){
						$FI.find("#otpModal").modal('hide');
						swal("Thanks For Connecting With Us!","We Will Get Back To You Shortly.", "success")
						$(this).ResetAllElemOfForm($FI);
						$FI.find("#siteid").val(0);
						//$FI.find("#amId").val(1);
						$FI.find("#lsId").val(1);
						$FI.find("#lsmId").val(1);
						$FI.find("#slId").val(0);
					}
				}
			}
		}
	
		else{
			formContainer.find("#validOtp").css('display','');
		}
		
	});
	
	var $FI2 = $("#suggForm");
	$FI2.find("#savesugg").on('click touchstart',function(event){
		formContainer2 = $FI2;
		if(formContainer2.ValidateForm()){
			var suggFormJson = $.parseJSON(serializeJSONIncludingDisabledFields($FI2));
			$(this).cAjaxPostJson('Post','./website/suggestion',JSON.stringify(suggFormJson), false, false, '', 'Error', true);
			if(strResponseText){
				var suggForm = $.parseJSON(strResponseText);
				if(suggForm.sId){
					swal("Thank You!","Message Sent Successfully.", "success");
					$(this).ResetAllElemOfForm($FI2);
				}
			}
		}
	});
}

$.fn.showProdDesc = function(prodName,prodImg,prodRate){
	$('#prodDtlModal').modal('show');
	$("#prodDesc").html($(this).closest('.hiddenDescBox').find('.hiddenDesc').html());
	$("#prodDtlModal").find('#prodName').text(prodName);
	$("#prodDtlModal").find('#prodRate').text(prodRate)
	$("#prodDtlModal").find('#prodModalImg').attr('src',prodImg).show();
}