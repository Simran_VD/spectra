var openTabArr=[""];
var currentServerDate='';
$.fn.datepicker.defaults.format = "dd/mm/yyyy";
$.fn.datepicker.defaults.autoclose = true;
$.fn.datepicker.defaults.clearBtn = true;
var tabId = '';
var maxOpenTab = 7;
var maxLenTabName=25;
var secondTab = "ActiveJobs"



$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
    $(".graphClassToggle").toggleClass("graphBoxWithMenu");
});



$.fn.addTab = function(name,content,displayName,option){
	if(openTabArr.length > (parseInt(maxOpenTab)+1)) {
		swal({   
			title: "Exceed Limit!",   
			text: "Maximum allowed tab limit reached. So please close opened tab to open new tab.",   
			timer: 10000,   
			showConfirmButton: true 
		});
		return false;
	}	
	if(openTabArr.indexOf(name) == -1){
		var nextTab = $('#tabList li').length+1;
		// create the tab
		tabId=name+"_"+nextTab;
		var displayNameTitle = displayName;
		if(displayName.length > parseInt(maxLenTabName)) {
			displayName = displayName.substring(0,parseInt(maxLenTabName)) + '...';
		}
		
		$('.menuTabActive').addClass('menuTabInActive');
		$('.menuTabActive').removeClass('menuTabActive');
		
		
		if(name == "Dashboard"){
			$('<li class="nav-item mr-1" style="margin-left: 6px;" onclick="$(this).kpiData();"><a class="nav-link nav-link pr-2 pl-2 pt-1 pb-1" tabId="'+tabId+'"  href="#'+tabId+'" data-toggle="tab" title="' + displayNameTitle + '" >'+displayName+'</a></li>').appendTo('#tabList');

		}
		
		
		else{
			$('<li class="nav-item mr-1" onclick="$(this).toggleTabClass()"><a class="nav-link nav-link pr-2 pl-2 pt-1 pb-1" tabId="'+tabId+'" href="#'+tabId+'" data-toggle="tab" title="' + displayNameTitle + '"><button class="close closeTab pl-2" type="button" >x</button> '+displayName+'</a></li>').appendTo('#tabList');
		}
		// create the tab content
		//$(".tab-pane.active").removeClass('active')
		$('<div class="tab-pane" id="'+tabId+'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loading content.....</div>').appendTo('.tab-content');
		$("#"+tabId).load(content+"&tabIndex="+nextTab);
		if(option){
			$("#"+tabId).data("data",option);
		}
		// make the new tab active
		$('#tabList a:last').tab('show');
		$('#tabList a:last').addClass('menuTabActive')
		openTabArr.push(name)
		registerCloseEvent();
	}else {
		//$("#tabs a[tabId^=addRole]")
		$('.menuTabActive').addClass('menuTabInActive');
		$('.menuTabActive').removeClass('menuTabActive');
		
		$('#tabList a[tabId^='+name+']').tab('show');
		$('#tabList a[tabId^='+name+']').addClass('menuTabActive');
		$('#tabList a[tabId^='+name+']').removeClass('menuTabInActive');
		
		

	}
	

};

//This method is used to create button from dashboard_setting

$.fn.addButton = function(aname,acontent,adisplayName,aoption){
	
	if(openTabArr.indexOf(aname) == -1){
		var nextTab = $('#tabList li').length+1;
		// create the tab
		tabId=aname+"_"+nextTab;
		var displayNameTitle = adisplayName;
		if(adisplayName.length > parseInt(maxLenTabName)) {
			adisplayName = adisplayName.substring(0,parseInt(maxLenTabName)) + '...';
		}
		
	/*	$('.menuTabActive').addClass('menuTabInActive');
		$('.menuTabActive').removeClass('menuTabActive');*/
		
		
		if(aname == "ActiveJobs"){
			$('<li class="nav-item mr-1" style="margin-left: 6px;" onclick="$(this).dashboardJobs();"><a class="nav-link nav-link pr-2 pl-2 pt-1 pb-1" tabId="'+tabId+'"  href="#'+tabId+'" data-toggle="tab" title="' + displayNameTitle + '" >'+adisplayName+'</a></li>').appendTo('#tabList');
		}
		
		// create the tab content
		//$(".tab-pane.active").removeClass('active')
		$('<div class="tab-pane" id="'+tabId+'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Loading content.....</div>').appendTo('.tab-content');
		$("#"+tabId).load(acontent+"&tabIndex="+nextTab);
		if(aoption){
			$("#"+tabId).data("data",aoption);
		}
		// make the new tab active
		//$('#tabList a:last').tab('show');
		$('#tabList a:last').addClass('menuTabActive')
		openTabArr.push(aname)
		registerCloseEvent();
	}else {
		//$("#tabs a[tabId^=addRole]")
		$('.menuTabActive').addClass('menuTabInActive');
		$('.menuTabActive').removeClass('menuTabActive');
		
		$('#tabList a[tabId^='+aname+']').tab('show');
		$('#tabList a[tabId^='+aname+']').addClass('menuTabActive');
		$('#tabList a[tabId^='+aname+']').removeClass('menuTabInActive');
	
	}
	

};


//this method will called on tab click
$.fn.toggleTabClass = function() {
	$('a.active.show').addClass('menuTabActive');
	$('a.active.show').removeClass('menuTabInActive');
	$(this).find('a').removeClass('menuTabInActive');
	$(this).find('a').addClass('menuTabActive');
	if($(this).find('a').text() ==  " Dashboard" ){
		$(this).loadData();
		$(this).getBaseAmt();
		
		
	}
	
}


//this method will register event on close icon on the tab..
function registerCloseEvent() {	
	$(".closeTab").click(function () {

		//there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
		var tabContentId = $(this).parent().attr("href");
		$(this).parent().parent().remove(); //remove li of tab
		if(tabContentId.indexOf("_") != -1){
			var tabName=tabContentId.slice(tabContentId.indexOf("_"));
			openTabArr.pop(tabName);
		}
		
		
		 
		$(tabContentId).remove(); //remove respective tab content
		if($('#tabList a').length < 3){
			$('#tabList a:contains("Dashboard")').tab('show');
			$('#tabList a:contains("Dashboard")').parent().click();
		}
		else{
			$('#tabList a:last').tab('show'); // Select first tab
		}
		if($('#tabList a:last').text() == " Dashboard"){
			$(this).loadData();
			$(this).getBaseAmt();
			
		}
		
	});
}

/*
 * Ajax call
 * 
 *  $(this).cAjax('POST','UserConfigDetails.action', '', false, false, '', 'Error', true);
 */
var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");
$(document).ajaxSend(function(e, xhr, options) {
	xhr.setRequestHeader(header, token);
});

//function to call ajax
$.fn.cAjax =  function(cType, cUrl, cData, cCache, cAsync, cFn, cAlert, cPrData, functionArguments)
{
	$(this).cAjaxBase(false,cType, cUrl, cData, cCache, cAsync, cFn, cAlert, cPrData, functionArguments);
}

$.fn.cAjaxPostJson =  function(cType, cUrl, cData, cCache, cAsync, cFn, cAlert, cPrData, functionArguments)
{
	$(this).cAjaxBase(true,cType, cUrl, cData, cCache, cAsync, cFn, cAlert, cPrData, functionArguments);
}

//function to call ajax
$.fn.cAjaxBase =  function(includeHeaders,cType, cUrl, cData, cCache, cAsync, cFn, cAlert, cPrData, functionArguments)

{
	//$("#loader").css('display','block');
	var self = $(this).eq(0),
	xhrObj = {},headers;
	if(includeHeaders){
		headers= { 
				'Accept': 'application/json',
				'Content-Type': 'application/json' 
				
		}
	}
	try{
		/*if(typeof cData == string)
		if(screen != ''){
			cData += "&screen="+screen;
		}
		if(operation != ''){
			cData += "&operation="+operation;
		}*/
		strResponseText = "";
		xhrObj = $.ajax({
			
			type: cType,
			url: cUrl,
			headers:headers,
			data: cData,
			cache: cCache,
			async: cAsync,
			processData:cPrData? true : false,
					// ; Event will be called on Ajax SUCCESS
					success: function(data, textStatus, XMLHttpRequest) {
						if	((typeof data).toUpperCase() == "OBJECT"){	
							data = JSON.stringify(data);
						}
						if((typeof data).toUpperCase() == "STRING" && data.indexOf('Session Expired') != -1){
							
							//$("#loader").css('display','none');
							swal("Session Expired");
						}
						else if(data =="System error")
						{
							//$("#loader").css('display','none');
							swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
						}
						else{
							if(cFn && cFn != '')
							{
								if(typeof functionArguments == 'boolean' || functionArguments)
									self[cFn](data, functionArguments);		//calling function by jquery Object notation	
								else
									self[cFn](data);
							}
							strResponseText = data;
							return data;
						}
						return false;
					},

					// ; Event will be called on Ajax ERROR
					error:function (xhr,textStatus,ajaxOptions, thrownError){
						 if (navigator.onLine == false){
							 swal("Warning!", "No Internet Connection!", "warning");
							 return false;
						 }
						var btnObject = {
								"OK": function(){$(this).dialog('close');}},errLabelCon = "";

						var  errorCode = xhr.status;
				
						//$("#loader").css('display','none');
						if (errorCode == "403"){
							location.reload();
							$("#loader").css('display','none');
							swal("Un-authorized access.");	
						}
					},

					// ; Event will be called on Request Complete
					complete:function(XMLHttpRequest,textStatus){
						//; swal("Complete:  "+strResponseText+"  "+textStatus+" "+XMLHttpRequest);
						if((typeof strResponseText).toUpperCase() == "STRING" && strResponseText.indexOf('Session Expired') != -1){
							/*document.location.href = "/";*/
							location.reload();
						}
						if(strResponseText=="System error"){
							if (navigator.onLine == false){
								swal("Warning!", "No Internet Connection!", "warning");
								 return false;
							 }
						}
						
						
						if(strResponseText=="System error")
						{
							//$("#loader").css('display','none');
							var btnObject = {
									"OK": function(){$(this).dialog('close');}};
							$(this).removeLoadingMsg().cDialog('customErrorDialog',sysErrorObj[0],sysErrorObj[1],false,true,null,null,btnObject,false,null,false,"slide","center");
						}
					},
					
					xhr: function () {
				        var xhr = $.ajaxSettings.xhr();
				        xhr.onprogress = function (e) {
				            // For downloads
				            if (e.lengthComputable) {
				                console.warn(e);
				            }
				        };
				        xhr.upload.onprogress = function (e) {
				            // For uploads
				            if (e.lengthComputable) {
				                console.warn(e);
				            }
				        };
				        return xhr;
				    }
		}).done(function(e){
			//$("#loader").css('display','none');
		});
		//store the XHR object so that it can be used while ajax in progress
		$(this).data("xhr", xhrObj);
	}
	catch(e){
		console.log(e)
		$(this).systemErrorFunc(e);
	}
};




function serializeJSONIncludingDisabledFields($form) {
  var $disabledFields = $form.find('[disabled]');
  $disabledFields.prop('disabled', false); // enable fields so they are included
  var json = $form.serializeJSON();
  $disabledFields.prop('disabled', true); // back to disabled
  return json;
};

function updateRowNumbers(FI,productRowSelector){
	var productRows = FI.find(productRowSelector);
	
	var productRowsLength = productRows.length;
	
	var i;
	
	
	for(i = 0 ; i < productRowsLength;i++){
		productRows.eq(i).find("select,input").each(function(){
			var name = $(this).attr("name");
			if(name){
				$(this).attr("name",name.replace(/\[0]/g,"\["+(i)+"]"))
			}
		});
	}

}


$.fn.reloadGrid = function(id, dispid, gridOption, id2, whereClause, additonalParam) {
	if(id2){
		$("#"+id2).find('#'+id).empty();
	}
	else{
		$("#"+id).empty();
	}
	
	if(whereClause){
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type=&page=0&size=20&whereClause='+whereClause, '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	else if(additonalParam != undefined){
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type='+additonalParam+'&page=0&size=20', '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	else{
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type=&page=0&size=20', '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	
//	colModel.displistcols = colModel.displistcols.sort((a, b) => {
//	    return a.columnid - b.columnid;
//	});
	
	var hiddenCol = "";
	var columnDefs = [];
	var columsnName = [];
	$.each(colModel.displistcols, function(){
	    var fieldName = new Object();
	    fieldName.headerName = this.displayname
	    fieldName.field = this.columnname
	    if(this.datatype == 'numeric'){
	    	fieldName.type = 'rightAligned'
	    }
	    
	    if(this.columnname == 'doc_name'){
	    	fieldName.type = 'leftAligned'
	    }
	    columsnName.push(this.columnname)
	    fieldName.hide = false
	   
	    if(this.uniquekey == 'Y' || this.uniquekey == 'y'){
	    	fieldName.hide = true
	    }
	    if(this.sortorder){
			if((this.sortorder).toLowerCase() == 'desc'){
	    		fieldName.sort = 'desc'
	    	}
	    	else{
	    		fieldName.sort = 'asc'
	    	}
		}
	    if(this.hidden == 'Y' || this.hidden == 'y'){
	    	fieldName.hide = true
	    }
	    columnDefs.push(fieldName)
	})
	var rowData = [];
	$.each(data.content, function(i, val) {
		var obj = new Object();
		for(var j = 0; j<columsnName.length; j++){
			obj[columsnName[j]] = this[j]
		}
		rowData.push(obj)
	});
	gridOption['columnDefs'] = columnDefs
	gridOption['rowData'] = rowData
	gridOption['rowHeight'] = 30;
	gridOption['headerHeight'] = 35;


	if(id2){
		var gridDiv = document.getElementById(id2).querySelector("#"+id);
	}
	else{
		var gridDiv = document.querySelector('#'+id);
	}
	
	
    new agGrid.Grid(gridDiv, gridOption);
    var allColumnIds = [];
    gridOption.columnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });

    gridOption.columnApi.autoSizeColumns(allColumnIds, false);
}

//Return with commas in between
var numberWithCommas = function(x) {
   return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};


//Return with dots in string
function add3Dots(string, limit)
{
  var dots = "...";
  if(string.length > limit)
  {
    // you can also use substr instead of substring
    string = string.substring(0,limit) + dots;
  }

    return string;
}

// find value in array of objects javaScript
function searchInObjArr(nameKey, myArray, pos){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].macId === nameKey) {
        	if(pos == 'prev'){
        		return [myArray[i-1], myArray[i-2]];
        	}
        	else{
        		return [myArray[i+1], myArray[i+2]];
        	}
            
        }
    }
}

function resetRow(row){
	var $row = $(row);
	var elements = $row.find("input");
	$.each(elements,function(){
		$(this).val("");
	});
	var elements = $row.find("select");
	$.each(elements,function(){
		$(this).val("");
		$(this).select2();
	});
}


$.fn.ValidateForm = function() {
	var inputElems = $(this).find("input");
	var selectElems = $(this).find("select");
	
	var textareaElems = $(this).find("textarea");
	if (inputElems.length > 0) {
		for (var int = 0; int < inputElems.length; int++) {
			var $inputEl = $(inputElems[int]);
			var isMandatory = $inputEl.attr("mandatory");
			if (isMandatory) {
				var value = $inputEl.val().trim();
				var message = $inputEl.attr("message");
				if (value == undefined || value == "") {
					swal(message);
					return false;
				}
			}
		}
	}
	
	if (selectElems.length > 0) {
		for (var int = 0; int < selectElems.length; int++) {
			var $selectEl = $(selectElems[int]);
			var isMandatory = $selectEl.attr("mandatory");
			if (isMandatory) {
				var value = $selectEl.val();
				var message = $selectEl.attr("message");
				if ($.isArray(value)) {
					if (value.length == 0) {
						swal(message);
						return false;
					}
				} else if(value == "") {
					swal(message);
					return false;
				}
			}
		}
	}
	
	if (textareaElems.length > 0) {
		for (var int = 0; int < textareaElems.length; int++) {
			var $textareaEl = $(textareaElems[int]);
			var isMandatory = $textareaEl.attr("mandatory");
			if (isMandatory) {
				var value = $textareaEl.val().trim();
				var message = $textareaEl.attr("message");
				if (value == undefined || value == "") {
					swal(message);
					return false;
				}
			}
		}
	}
	
	
	return true;
};


function onSelectionChanged(gridOption) {
	try{
		var selectedRows = gridOption.api.getSelectedRows();
		//if(selectedRows.length === 1 ){
			return (selectedRows);
		//} 
		//else{
		//	return null;
	//}
	}
	catch(e){
		return null;
	}
	
};

function onBtnExport(gridOptions) {
	  gridOptions.api.exportDataAsCsv();
}

function imageExists(image_url){

    var http = new XMLHttpRequest();

    http.open('HEAD', image_url, false);
    http.send();

    return http.status != 404;

}



$.fn.populateAdvanceSelect = function(id,json,config){
	var $select = $(id);
	$select.children("option").remove();
	$select.append($("<option />").val("").text("--Select--"));
	$.each(json, function() {
		var opt = $("<option />");
		opt.data(this);
		$select.append(opt.val(this[config.key]).text(this[config.value]));
	});
};

$.fn.populateMultipleSelect = function(id,json,config){
	var $select = $(id);
	$select.children("option").remove();
	$.each(json, function() {
		var opt = $("<option />");
		opt.data(this);
		$select.append(opt.val(this[config.key]).text(this[config.value]));
	});
};

/*$.fn.populateMultipleSelect2 = function(id,json,config){
	var $select = $(id);
	$select.children("option").remove();
	$.each(json, function() {
		var opt = $("<option />");
		opt.data(this);
		$select.append(opt.val(this[config.key]).text(this[config.value]).attr("sdmId",this.sdmId));
	});
};*/

$.fn.populateDatalist  = function(id, json){
	var $datalist = $(id);
	$datalist.empty();
	//$datalist.children("option").remove();
	$.each(json, function() {
		$datalist.append("<option value='" +  this.name + "'></option>");
	});
};



$.fn.closeTab = function(index) {
    var parentList = $('ul.customtab');
    var listItem = parentList.children('li').eq(index);
    var closeIcon = listItem.find('button, .tab-close').eq(0);
    closeIcon.trigger('click');
};
$.fn.closeUserTab = function(){
	try{
		var type = this[0].type || this[0].tagName.toLowerCase();
		var formInst = type != "form" ? $(this).parents('form') : $(this);	// jquery form Instance
		var navigateTab = $(this).parents('.tab-pane').attr('id'), // get active tab instance
		    tabs = formInst.parents("#page-content-wrapper").find("ul.customtab"),			// get all tabs
		    currentIndex = -1,
		    tabList = tabs.children("li");
	
		for ( var m = 0, len = tabList.length; m < len; m++) {
			if (tabList.eq(m).find('a').attr('href') == "#" + navigateTab)
				currentIndex = m;
		}
		
		tabs.closeTab(currentIndex);	// close tab
	}
	catch(e)
	{
	}
};


$.fn.getUserName = function(){
    $(this).cAjax('POST','./admin/getUserName', '', false, false, '', 'Error', true);
	var user = $.parseJSON(strResponseText);
	$(".userName").text(user.displayname);
	$("#userId").val(user.userid);
}

$.fn.openCloseMenu = function(){
	if($('.openMenu').css('left') == '208px'){
		$('.openMenu').css('left','0px');
	}
	else{
		$('.openMenu').css('left','208px');
	}
	
}

$.fn.showFileName = function(id,closebtnid,FI) {
	if(FI == undefined){
		var FI = $(this).closest("form");
	}
	FI.find('#'+closebtnid).removeAttr('disabled');
	$(this).prev('#'+id).text($(this)[0].files[0]['name']);
}

$.fn.populateCities = function(stateId,cityId, FI){
	if(FI == undefined){
		var FI = $(this).closest("form");
	}
	var selectedState = FI.find("#"+stateId+" option:selected").val();
	if(selectedState){
		$(this).cAjax('POST','./mstData/getLocMstByStateid?stateid='+selectedState, '', false, false, '', 'Error', true);
		var dropDownJson= $.parseJSON(strResponseText);
		$(this).populateAdvanceSelect(FI.find("#"+cityId),dropDownJson,{"key":"lmId","value":"cityname"});
	}

};



$.fn.populateSelect2 = function(firstName, $elem,data){
	$elem.children("option").remove();
	$elem.append($("<option />").val("").text("-- Select --"));
	$.each(data, function() {
		$elem.append($("<option />").val(this.key).text(this.displayValue));
	});
}



$.fn.ResetAllDataInForm = function(form) {
	var Fi=form;
	var fi=Fi;
	Fi.find('select').each(function(i){
		var $selectDiabled = this.disabled;
		if(!($selectDiabled)){
			fi.find('select#'+this.id).val("");
			fi.find('select#'+this.id).select2();
			fi.find('select#'+this.id).trigger('change')
		}	 
	});
	Fi.find('input[type="text"]').each(function()
			{
		var isDisabled = $(this).prop('disabled');


		if (!(isDisabled))
		{
			$(this).val("")
	}});  

	Fi.find('input[type="email"]').each(function()
			{
		var isDisabled = $(this).prop('disabled');


		if (!(isDisabled))
		{
			$(this).val("")
	}}); 
	
	
	Fi.find('input[type="password"]').each(function()
			{
		var isDisabled = $(this).prop('disabled');


		if (!(isDisabled))
		{
			$(this).val("")
	}});

	
	Fi.find('textarea').val('');
	
	Fi.find('input[type="number"]').each(function()
			{
		var isDisabled = $(this).prop('disabled');

		if (!(isDisabled))
		{
			$(this).val("");
		}});

	
};

$.fn.ResetAllElemOfForm = function(FI) {
	FI.find("input").val("");
	FI.find("textarea").val("");
	FI.find("select").val($(this).find("select option:first").val());
	FI.find("input, select, button").attr("disabled", false);
	
};


$.fn.enableFields =  function(){
	$(this).find("input, select, button").attr("disabled", false);
}


$.fn.manageCalander = function(FI, id, startDate, val){
	FI.find(id).datepicker({
	    dateFormat: "dd/mm/yyyy",
	    autoclose: true,
	    startDate: new Date(startDate),
	});
	if(val){
		FI.find(id).datepicker('setDate', new Date(val));
	}
}


$.fn.populateAdvanceSelect23 = function(id,json,config,loginUser){
	
	var $select = $(id);
	$select.children("option").remove();
	$select.append($("<option />").val("").text("--Select--"));
	$.each(json, function() {
		var opt = $("<option />");
		opt.data(this);
		if(this.userid == loginUser){
			$select.append(opt.val(this[config.key]).text(this[config.value]).attr("disabled","disabled"));
		} else {
			$select.append(opt.val(this[config.key]).text(this[config.value]));
		}
		
	});
};