var userId;
// here dispId is not used as this listing was reqired to show data on dashboard too.
var additionalParam;
var serviceLeadListId = 8;
$.fn.datetimepicker.defaults.format = "dd/mm/yyyy hh:ii:ss";

var wholeSrvcLeadListGridOptions;
var serviceLeadLst;
$.fn.getservicelead = function(param, tabName){
	if(!param){
		additionalParam = 0;
	}
	else{
		additionalParam = param;
	}
	if(tabName == "Total Leads" || tabName == undefined){
		tabName = "Service Lead"
	}
	$(this).addTab("serviceList"+additionalParam,'./pages?name=service/serviceLeadList',tabName);
}
$.fn.srvcLeadList = function(leadPage){
	
	leadPageId = $("#"+leadPage);
	leadPageId.find("#typeParam").val(additionalParam);
	var srvcLeadListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("srvcLeadLst", serviceLeadListId, srvcLeadListGridOptions,leadPage,null, additionalParam);
	
	leadPageId.find("#srvcLeadContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(wholeSrvcLeadListGridOptions)[0].sl_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
			
			if(operation == "edit"){
			var data_status = onSelectionChanged(wholeSrvcLeadListGridOptions)[0].status;
			if(data_status == "Closed"){
				swal("You cannot edit this Status is closed")
				return false;
				}
			}
			
			
		}
		switch(operation) {
		case "add new":
			$(this).addTab("AddServiceLead",'./pages?name=service/addEditserviceLead',"Add Service Lead",{"action":"new"});
			break;
		case "edit":
			$(this).addTab("EditServiceLead"+data,'./pages?name=service/addEditserviceLead',"Edit Service Lead-"+data,{"data":data,"action":"edit"});
			break;
		case "view":
			$(this).addTab("ViewServiceLead"+data,'./pages?name=service/addEditserviceLead',"View Service Lead-"+data,{"data":data,"action":"view"});
			break;
		case "delete":
			swal({
				title: "Are you sure want to delete Service Lead?",
				text: "You will not be able to recover Service Lead now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false	,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if(isConfirm){
					$(this).cAjax('Post','./customer/deleteServLead',"slid="+data, false, false, '', 'Error', true);
					if(strResponseText == "SUCCESS"){
						swal("Deleted!", "Selected record deleted successfully.", "success");
						$(this).reloadGrid("srvcLeadLst", serviceLeadListId, wholeSrvcLeadListGridOptions,leadPage,null, additionalParam);

					} else if(strResponseText == "DATA_INTEGRITY"||strResponseText == ""){
						swal("Error in deletion!", "Selected Item is being used. So it cannot be deleted.", "error");
					}
				} 
				else {
					swal("Cancelled", "Thank you! your Service Lead is safe :)", "error");
				}
			});
			break;
		case "refresh":
				var leadPage = $(this).closest('form').attr('id');
				leadPageId = $("#"+leadPage);
				var additionalParam = leadPageId.find("#typeParam").val();
				$(this).reloadGrid("srvcLeadLst", serviceLeadListId, wholeSrvcLeadListGridOptions,leadPage,null, additionalParam);
				swal("Refresh!","Service Lead  Refresh Successfully.", "success");
				break;	
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});
	
	if(additionalParam == 0){
		serviceLeadLst = leadPage;
		wholeSrvcLeadListGridOptions = srvcLeadListGridOptions;
		leadPageId.find("#srvcLeadLst").closest('form').attr("oninput","$(this).onSrvcLeadFilterTextBoxChanged()");
	}
	else{
		leadPageId.find("#srvcLeadLst").closest('form').find(".btn-container").remove();
	}
	
	
}

$.fn.onSrvcLeadFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	wholeSrvcLeadListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.prepareSrvcLeadForm = function($FI){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined,openingStockNumber = '';
	var formContainer = $FI;
	
	
	if(option.data){
		$FI.find("#servlead .srvleadactivestatus").attr("id", "srvleadactivestatus"+option.data)
		$FI.find("#servlead #srvleadactivestatuslabel").attr("for","srvleadactivestatus"+option.data);
		$FI.find("#servlead .srvleadinactivestatus").attr("id", "srvleadinactivestatus"+option.data)
		$FI.find("#servlead #srvleadinactivestatuslabel").attr("for","srvleadinactivestatus"+option.data);
	}
	else{
		$FI.find("#servlead .srvleadactivestatus").attr("id", "srvleadactivestatus")
		$FI.find("#servlead #srvleadactivestatuslabel").attr("for","srvleadactivestatus");
		$FI.find("#servlead .srvleadinactivestatus").attr("id", "srvleadinactivestatus")
		$FI.find("#servlead #srvleadinactivestatuslabel").attr("for","srvleadinactivestatus");
	}
	
	
	if(action == "new"){
		$FI.cAjax('GET','./getStaticValues?keys=datetime', '', false, false, '', 'Error', true);
		leadGenrTime =  $.parseJSON(strResponseText);
		$FI.find("#leadGenrTime").val(leadGenrTime["datetime"]);
		$FI.find("#status").attr("disabled","disabled");
		var validateTime = $FI.find("#leadGenrTime").val()
		validateTime = validateTime.substr(3, 2)+"/"+validateTime.substr(0, 2)+ "/"+validateTime.substr(6, 4)+ " "+validateTime.substr(11,2)+ ":" +validateTime.substr(14,2);
		$FI.find('#servReqdAtTime').datetimepicker({
	        dateFormat: 'dd/mm/yyyy  hh:ii:ss',
	        autoclose:true,
	        startDate: new Date(validateTime)
	    });
		
		
	}
	
	
	$FI.cAjax('POST','./mstData/getStates', '', false, false, '', 'Error', true);
	states =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#state"),states,{"key":"stateid","value":"statename"});
	
	$FI.cAjax('GET','./master/getLeadStatus', '', false, false, '', 'Error', true);
	statuss =  $.parseJSON(strResponseText);
	
	$(this).cAjax('POST','./admin/getUserName', '', false, false, '', 'Error', true);
	User =  $.parseJSON(strResponseText);
	userId = User.uiid;
	var stsFrPtnr = [4, 5, 6, 19, 20, 3, 7, 20, 22];
	var ptnrStatus = [];
	if(userId){
		$.each(statuss, function(){
			if(stsFrPtnr.includes(this.lsmId)){
				ptnrStatus.push(this);
			}
		});
		$FI.populateAdvanceSelect($FI.find("#status"),ptnrStatus,{"key":"lsmId","value":"status"});
	}
	else{
		$FI.populateAdvanceSelect($FI.find("#status"),statuss,{"key":"lsmId","value":"status"});
	}
	
	
	
	
	var default_Status = statuss[0].lsmId
	$FI.find("#status").val(1)
	
	$FI.cAjax('GET','./partner/areas', '', false, false, '', 'Error', true);
	areas =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#serviceArea"),areas,{"key":"amId","value":"areaName"});
	
	$FI.cAjax('GET','./product/getLeadSource', '', false, false, '', 'Error', true);
	lsmIds =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#lsmId"),lsmIds,{"key":"prdId","value":"source"});
	
	var default_lead  = lsmIds[1].prdId
	$FI.find("#lsmId").val(default_lead)
	
	$FI.cAjax('POST','./customer/getCustomers', '', false, false, '', 'Error', true);
	customers =  $.parseJSON(strResponseText);
	 $.map(customers,function(val,i){
		 val['mob']= val['mobile'] + '('+val['name'] + ')';	
	})
	$FI.populateAdvanceSelect($FI.find("#custmobile"),customers,{"key":"csId","value":"mob"});
	
	$FI.cAjax('GET','./product/getProductList', '', false, false, '', 'Error', true);
	products =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#product"),products,{"key":"prdId","value":"prodName"});
	
	
		
		
		
		
	$FI.find("#saveSrvcLead").on('click touchstart',function(event){
		if(formContainer.ValidateForm()){
			$FI.find("#saveSrvcLead").attr("disabled","disbaled")
			var srvcLeadFormJson = $.parseJSON(serializeJSONIncludingDisabledFields($FI));
			
			
			$(this).cAjaxPostJson('Post','./customer/saveServiceLead',JSON.stringify(srvcLeadFormJson), false, false, '', 'Error', true);
			$FI.find("#saveSrvcLead").attr("disabled",false)
			
			if(strResponseText){
				if($(this).chkValidation()){
				var srvcLeadReturn = $.parseJSON(strResponseText);
				if(srvcLeadReturn.slId){
					swal("Saved!", "Service Lead successfully.", "success");
					$FI.closeUserTab();
					$(this).getservicelead()
					
					$(this).reloadGrid("srvcLeadLst", serviceLeadListId, wholeSrvcLeadListGridOptions,serviceLeadLst,null, additionalParam);
					return true;
				}
				else{
					swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					$FI.find("#saveSrvcLead").attr("disabled",false)
					return false;
				}
				}
			}
			else{
					swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					$FI.find("#saveSrvcLead").attr("disabled",false)
					return false;
			}
		}
	})
	
	if(keyValue){
		$(this).cAjax('GET','./customer/getServiceLead',"slId="+keyValue, false, false, '', 'Error', true);
		var srvcLead = $.parseJSON(strResponseText);
		$(this).fillSrvcLeadForm($FI,srvcLead);
		var action = option ? option.action : undefined;
		if(action && action=="view" ){
			$FI.find("input,select,button").attr("disabled","disabled")
		}else if(action && action=="edit"){
			$FI.find("#custmobile").attr("disabled","disabled");
		}
	}
}


$.fn.fillSrvcLeadForm =  function($FI,srvcLead){
	$FI.find("#slId").val(srvcLead.slId);
	$FI.find("#pmId").val(srvcLead.pmId);
	//$FI.find("#psmId").val(srvcLead.psmId);
	$FI.find("#srvtime").val(srvcLead.service_time);
	$FI.find("#leadGenrTime").val(srvcLead.leadGenTime);
	
	$FI.find("#leadNotifyTime").val(srvcLead.leadNotifyTime);
	$FI.find("#lsmId").val(srvcLead.lsmId);
	$FI.find("#lsmId").select2();
	$FI.find("#servReqdAtTime").val(srvcLead.servReqdAtTime);
	$FI.find("#custmobile").val(srvcLead.csId);
	$FI.find("#custmobile").select2();
	$FI.find("#product").val(srvcLead.prdId);
	$FI.find("#product").select2();
	if(srvcLead.prdId){
		$FI.find("#product").trigger('change');
	}
	
	
	$FI.find("#name").val(srvcLead.name);
	$FI.find("#pin").val(srvcLead.pinCode);
	$FI.find("#displayName").val(srvcLead.displayName);
	//$FI.find("#serviceArea").val(srvcLead.amId);
	$FI.find("#serviceArea").select2();
	$FI.find("#address").val(srvcLead.address);
	$FI.find("#landmark").val(srvcLead.landmark);
	$FI.find("#state").val(srvcLead.stateId);
	if(srvcLead.stateId){
		$FI.find("#state").trigger('change');
	}
	$FI.find("#city").val(srvcLead.cityId);
	if(srvcLead.cityId){
		$FI.find("#city").trigger('change');
	}
	
	$FI.find("#pinCode").val(srvcLead.amId);
	$FI.find("#phone").val(srvcLead.phone);
	$FI.find("#mobile").val(srvcLead.mobile);
	$FI.find("#email").val(srvcLead.email);
	$FI.find("#pbmId").val(srvcLead.pbmId);
	$FI.find("#services").val(srvcLead.smId);
	$FI.find("#services").select2();
	if(srvcLead.smId){
		$FI.find("#services").trigger("change");
	}
	if(srvcLead.rate){
		$FI.find("#rate").val(srvcLead.rate);
	}
	//$FI.find("#rate").val(srvcLead.rate);
	$FI.find("#remarks").val(srvcLead.remarks);
	$FI.find("#custRating").val(srvcLead.custRating);
	$FI.find("#partnerRating").val(srvcLead.partnerRating);
	$FI.find("#totalChargs").val(srvcLead.totalChargs);
	$FI.find("#servStartTime").val(srvcLead.servStartTime);
	$FI.find("#servCompleteTime").val(srvcLead.servCompleteTime);
	$FI.find("#servCloserTime").val(srvcLead.servCloserTime);
	$FI.find("#status").val(srvcLead.lsId);
	$FI.find("#status").select2();
	if(srvcLead.lsId){
		$FI.find("#status").trigger("change")
	}
	$FI.find("[value='"+srvcLead.status+"'][name='status']").attr('checked','checked')
	
	
	var validateTime = $FI.find("#leadGenrTime").val()
	validateTime = validateTime.substr(3, 2)+"/"+validateTime.substr(0, 2)+ "/"+validateTime.substr(6, 4)+ " "+validateTime.substr(11,2)+ ":" +validateTime.substr(14,2);
	$FI.find('#servReqdAtTime').datetimepicker({
        dateFormat: 'dd/mm/yyyy  hh:ii:ss',
        autoclose:true,
        startDate: new Date(validateTime)
    });
	
	var pmId = $FI.find("#pmId").val()
	
	if(pmId != ""){
		// $FI.find("#hideprtbody").css("display","block")
		$(this).cAjax('GET','./partner/getPartnerList', '', false, false, '', 'Error', true);
		prtDetail =  $.parseJSON(strResponseText);
		$FI.populateAdvanceSelect($FI.find("#prtname"),prtDetail,{"key":"pmId","value":"name"});
		$FI.find("#prtname").val(pmId).select2()
		var selOpt = $FI.find('#prtname').find(':selected').eq(0);
		
		
		var data = selOpt.data();
		var optionVal = selOpt.val();
		
		if(optionVal)
		
		//FI.find("#prtname").val(data.name);
		$FI.find("#mobileno").val(data.mobile);
		$FI.find("#addr").val(data.address);
	}
	// else{
	// 	$FI.find("#hidebody").css("display","none")
	// }
	if(pmId){
		$FI.find("input,select").attr("disabled","disabled")
		$FI.find("#hideprtbody").css("display","block")
		$FI.cAjax('GET','./partner/getPartnerTeamVw?pmId='+pmId, '', false, false, '', 'Error', true);
		productTeam =  $.parseJSON(strResponseText);
		$FI.populateAdvanceSelect($FI.find("#prtTeam"),productTeam,{"key":"prdId","value":"name"});
		$FI.find("#prtTeam").val(srvcLead.ptId).select2()
		
		if(srvcLead.lsId == 5){
			$FI.find("#prtTeam").attr("disabled",true);
			$FI.find("#status").attr("disabled",true);
			$FI.find("#hidebody :input").attr("disabled", true);
		}
		else{
			$FI.find("#prtTeam").attr("disabled",false);
			$FI.find("#status").attr("disabled",false);
			$FI.find("#hidebody :input").attr("disabled", false);
		}
		
		
	}	
}


$.fn.onCustChange = function(){
	var selOpt = $(this).find('option:selected').eq(0);
	var data = selOpt.data();
	var optionVal = selOpt.val();
	var FI = $(this).closest('form');
	if(optionVal){
		FI.find("#name").val(data.name);
		FI.find("#displayName").val(data.displayName);
		
		FI.find("#address").val(data.address);
		FI.find("#landmark").val(data.landmark);
		FI.find("#state").val(data.stateid).select2();
		if(data.stateid){
			FI.find("#state").trigger('change')
		}
		
		FI.find("#city").val(data.cityid).select2();
		FI.find("#city").trigger('change')
		FI.find("#pinCode").val(data.am_id).select2()
		FI.find("#phone").val(data.phone);
		FI.find("#mobile").val(data.mobile);
		FI.find("#email").val(data.email);
	}
	else{
		FI.find("#name").val("");
		FI.find("#displayName").val("");
		FI.find("#serviceArea").val("");
		FI.find("#serviceArea").select2();
		FI.find("#address").val("");
		FI.find("#landmark").val("");
		FI.find("#city").val("");
		FI.find("#state").val("");
		FI.find("#pinCode").val("");
		FI.find("#phone").val("");
		FI.find("#mobile").val("");
		FI.find("#email").val("");
	}
}
$.fn.onProdChange = function(){
	
	var $FI = $(this).closest('form');
	var pbmId = $(this).val();
	var prdId = $(this).val();
	$(this).cAjax('GET','./ProductBrandMstVw/getProductBrandMstVw', 'prdId='+prdId, false, false, '', 'Error', true);
	
	if(strResponseText){
		brands =  $.parseJSON(strResponseText);
	}
	else{
		brands = [];
	}
	$FI.populateAdvanceSelect($FI.find("#pbmId"),brands,{"key":"pbmId","value":"brandName"});
	$(this).cAjax('GET','./product/getProductServiceList', 'prdId='+prdId, false, false, '', 'Error', true);
	if(strResponseText){
		services =  $.parseJSON(strResponseText);
	}
	else{
		services = [];
	}
	$FI.populateAdvanceSelect($FI.find("#services"),services,{"key":"smId","value":"serviceName"});
	$FI.populateAdvanceSelect($FI.find("#psmId"),services,{"key":"psmId","value":"serviceName"});
};


$.fn.onServiceChange = function(){
	
	var selOpt = $(this).find('option:selected').eq(0);
	var data = selOpt.data();
	var optionVal = selOpt.val();
	var FI = $(this).closest('form');
	var selTxt = selOpt.text();
	FI.find("#psmId option:contains('"+selTxt+"')").attr('selected', 'selected');
	if(optionVal){
		FI.find("#rate").val(data.comm);
		FI.find("#srvtime").val(data.serviceTime);
	}
	else{
		FI.find("#rate").val("");
		FI.find("#srvtime").val("");
		
	}

};

$.fn.showinboxbody = function(){
	var $FI = $(this).closest('form');
	var ratingBox = $FI.find("#hidebody");
	var status = $FI.find("#status").val();
	if(status == "5"){
		$FI.find("#hidebody").css("display","block");
		ratingBox.find('#servStartTime').attr('mandatory','true');
		ratingBox.find('#servCompleteTime').attr('mandatory','true');
	}
	else{
		$FI.find("#hidebody").css("display","none");
		ratingBox.find('#servStartTime').removeAttr('mandatory');
		ratingBox.find('#servCompleteTime').removeAttr('mandatory');
	}
}
$.fn.onsrvleadcityChange = function(){
	var $FI = $(this).closest('form');
	var cityid = $(this).val();
	//var selOpt = $(this).find('option:selected').eq(0);
	//var data = selOpt.data();
	$(this).cAjax('GET','./partner/areas', 'cityid='+cityid, false, false, '', 'Error', true);
	pinarea =  $.parseJSON(strResponseText);
	$.map(pinarea,function(val,indx){
		if(val["pincode"]) {
			val["pincode"] = val["pincode"] + " (" + val["areaName"] + ")";
		}
	});
	$FI.populateAdvanceSelect($FI.find("#pinCode"),pinarea,{"key":"amId","value":"pincode"});
	//$FI.find("#state").val(data.stateid);
	//$FI.find("#state").select2();
}

$.fn.onpincodechange = function(){
	var $FI = $(this).closest('form');
	var selectpin = $('#pinCode :selected').text();
	var replacepin = selectpin.split(" ",1)
	var strpin = (replacepin.toString())
	var finalpin = strpin.replace("("," ")
	$FI.find("#pin").val(finalpin)
}


$.fn.ResetAllDataInFormsrvlead=function(){
	var Form=$(this).closest('form')
	Form.find("#rate").val("")
	Form.find("#srvtime").val("")
	Form.find("#services").select2();
	Form.find("#services").trigger('change')
	
	$(this).ResetAllDataInForm(Form)
};

$.fn.showpartnerinbox = function(){
	var selOpt = $(this).find('option:selected').eq(0);
	var data = selOpt.data();
	var optionVal = selOpt.val();
	var FI = $(this).closest('form');
	if(optionVal)
	
	FI.find("#prtname").val(data.name);
	FI.find("#mobileno").val(data.mobile);
	FI.find("#addr").val(data.address);
}

$.fn.chkValidation = function(){
	var FI = $(this).closest('form');
	
	var statusVal = FI.find("#status").val()
	if(statusVal == "2"){
	
	
	var addr = FI.find("#address").val();
	
	if(addr == ""){
		swal("Please Fill Address")
		return false;
	}
	
	var pinCode = FI.find("#pinCode").val();
	
	if(pinCode == ""){
		swal("Please Fill pinCode")
		return false;
	}
	var serviceType = FI.find("#services").val();
	if(serviceType == ""){
		swal("Please Fill Service")
		return false;
	}
	
}
	
	return true;
}