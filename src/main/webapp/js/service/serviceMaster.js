srvmasterListId = 16;
var srvmasterGridOptions;
$.fn.getservicemaster = function(){
	$(this).addTab("getservicemaster",'./pages?name=/service/serviceMaster',"Service Master");
};

$.fn.srvmasterListing = function(){
	srvmasterGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',

		};
$(this).reloadGrid("srvmasterList", srvmasterListId, srvmasterGridOptions);
	
	$("#srvmasterListContainer .btn-container .btn").on('click touchstart',function(){
		var opration=$(this).attr("title"),data;
		opration=opration.toLocaleLowerCase();
		if(opration != "add new"){
			
			
			try{
				var data = onSelectionChanged(srvmasterGridOptions)[0].sm_id;
			}
			catch(e){
				var data = undefined;
			}
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(opration) {
			case "add new":
				$('#srvmasterModal').modal('show');
				$('#srvmasterModal').find("#prdmstActive").val("Y");
				$('#srvmasterModal').find("#prdmstInactive").val("N");
				$('#srvmasterModal').enableFields()
				break;
			case "edit":
			case "view":
				$(this).cAjax('GET', './product/getServiceMst',"smId="+data, false, false, '','Error', true);
				var srvmstJson = $.parseJSON(strResponseText);
				$('#srvmasterModal').modal('show')
				$('#srvmasterModal').fillsrvmstForm(srvmstJson, opration);
					break;
			case "delete":
				swal({
					title: "Are you sure Do you want  to delete Service?",
					text: "You will not be able to recover Service now!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No cancel please!",
					confirmButtonColor:'#3095d6',
					cancelmButtonColor:'#d33',
					closeOnConfirm: false,
					closeOnCancel: false,
					showConfirmButton: true,
				},
				function(isConfirm) {
					if (isConfirm) {
				$(this).cAjax('Post','./product/deleteServiceMst',"smId="+data, false, false, '', 'Error', true);
				if(strResponseText == "SUCCESS"){
					swal("Deleted!", "Selected Product deleted successfully.", "success");
					$(this).reloadGrid("srvmasterList", srvmasterListId, srvmasterGridOptions);
					$(".ag-center-cols-container").css('width','100%')
				} else if(strResponseText == "DATA_INTEGRITY"){
					swal("Error in deletion!", "Selected Service is being used. So it cannnot be deleted.", "warning");
				}
					}
					else {
						swal("Cancelled", "Thank you! your Service is safe :)", "error");
					}});
				break;
				
//			case "print":
//				var rptUrl = reportingUrl + "/MasterData/designation.rptdesign&__format=pdf";
//				window.open(rptUrl);
//				break;
				
			default:
				swal("Not implemented.");
		}
		return false;
	});
	$("#srvmasterListContainer").closest(".row").addClass("listContainer");
	$("#srvmasterList").closest('form').attr("oninput","$(this).onsrvmstFilterTextBoxChanged()");

};


$.fn.onsrvmstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	srvmasterGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}



$.fn.fillsrvmstForm = function(srvmstJson, opration) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#smid").val(srvmstJson.smId);
	
	$(this).find("#siteid").val(srvmstJson.siteid);
	$(this).find("#srvname").val(srvmstJson.serviceName);
	$(this).find("#srvremark").val(srvmstJson.remarks);
	
	
    
    if(srvmstJson.isActive == 'Y') {
        $("#srvmstActive").prop("checked", true)
	} else {
        $("#srvmstInactive").prop("checked", true)     
	}

	if (opration && opration == "view") {
		$(this).find("input, select, button").attr("disabled", "disabled");
		$(this).find("#closeDesignationBtn1").attr("disabled", false);
		$(this).find("#closeDesignationBtn2").attr("disabled", false);
		$(this).find("#savesrvmst").attr("disabled", true);
		$(this).find("#srvnxtBtn").attr("disabled", false);
	} else if (opration && opration == "edit") {
		//$(this).find("#slabNo").attr("disabled", true);
		//$(this).find("#savesrvmst").attr("disabled", true);
	}
	
};

$.fn.savesrvMaster = function() {
	var FI= $(this).closest('form');
	if(!FI.ValidateForm() ){//|| !$(this).validate(FI)) {
		return false;
	}
	var srvmstJson = (serializeJSONIncludingDisabledFields(FI));
	
	
	$(this).cAjaxPostJson('Post','./product/saveServiceMst',(srvmstJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Product!", "", "warning");  
	} else  if(strResponseText) { 
		var srvmst = $.parseJSON(strResponseText);
		if(srvmst.smId){
			$(this).cAjax('GET','./website/getServices', '', false, false, '', 'Error', true);
			swal("Saved!", "Service  saved successfully.", "success");
			$('#srvmasterModal').modal('hide');
			$(this).getservicemaster();
			$(this).reloadGrid("srvmasterList", srvmasterListId, srvmasterGridOptions);
			$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};	
