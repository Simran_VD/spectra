srvChargeListListId = 20;
var srvChargeListGridOptions;
$.fn.getservice = function(){
	$(this).addTab("getservice",'./pages?name=/service/serviceCharges',"Service Charge");
};

$.fn.srvChargeListing = function(){
	srvChargeListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',

		};
$(this).reloadGrid("srvChargeList", srvChargeListListId, srvChargeListGridOptions);
	
	$("#srvChargeListContainer .btn-container .btn").on('click touchstart',function(){
		var opration=$(this).attr("title"),data;
		opration=opration.toLocaleLowerCase();
		if(opration != "add new"){
			
			
			try{
				var data = onSelectionChanged(srvChargeListGridOptions)[0].sc_id;
			}
			catch(e){
				var data = undefined;
			}
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(opration) {
			case "add new":
				$('#srvChargeModal').modal('show');
				$('#srvChargeModal').enableFields();
				$('#srvChargeModal').ResetAllDataInFormsrvCharge();
				break;
			case "edit":
			case "view":
				$(this).cAjax('GET', './product/getServiceCharges',"scId="+data, false, false, '','Error', true);
				var chargeJson = $.parseJSON(strResponseText);
				$('#srvChargeModal').modal('show')
				$('#srvChargeModal').fillchargesForm(chargeJson, opration);
				
				break;
			case "delete":
				swal({
					title: "Are you sure Do you want  to delete Product Parts?",
					text: "You will not be able to recover Product Parts now!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No cancel please!",
					confirmButtonColor:'#3095d6',
					cancelmButtonColor:'#d33',
					closeOnConfirm: false,
					closeOnCancel: false,
					showConfirmButton: true,
				},
				function(isConfirm) {
					if (isConfirm) {
				$(this).cAjax('Post','./product/deleteServiceCharges',"scId="+data, false, false, '', 'Error', true);
				if(strResponseText == "SUCCESS"){
					swal("Deleted!", "Selected Service Charges  deleted successfully.", "success");
					$(this).reloadGrid("srvChargeList", srvChargeListListId, srvChargeListGridOptions);
				} else if(strResponseText == "DATA_INTEGRITY"){
					swal("Error in deletion!", "Selected Service Charges is being used. So it cannnot be deleted.", "warning");
				}
					}
					else {
						swal("Cancelled", "Thank you! your Service Charges is safe :)", "error");
					}});
				break;
				
//			case "print":
//				var rptUrl = reportingUrl + "/MasterData/designation.rptdesign&__format=pdf";
//				window.open(rptUrl);
//				break;
				
			default:
				swal("Not implemented.");
		}
		return false;
	});
	$("#srvChargeListContainer").closest(".row").addClass("listContainer");
	$("#srvChargeList").closest('form').attr("oninput","$(this).onsrvChargeFilterTextBoxChanged()");

};


$.fn.onsrvChargeFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	srvChargeListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.ResetAllDataInFormsrvCharge=function(){
	var Form=$(this).find("#srvChargeForm")
	$(this).ResetAllDataInForm(Form)
};



$.fn.fillchargesForm = function(chargeJson, opration) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#scId").val(chargeJson.scId);
	$(this).find("#siteId").val(chargeJson.siteId);
	$(this).find("#prodName").val(chargeJson.prodName);
	$(this).find("#serviceType").val(chargeJson.serviceType);
	$(this).find("#serviceCost").val(chargeJson.serviceCost);
	$(this).find("#leadCost").val(chargeJson.leadCost);
	$(this).find("#remarks").val(chargeJson.remarks);
	
	
    
    if(chargeJson.isActive == 'Y') {
        $("#srvChargeActive").prop("checked", true)
	} else {
        $("#srvChargeInactive").prop("checked", true)     
	}

	if (opration && opration == "view") {
		$(this).find("input, select, button").attr("disabled", "disabled");
		$(this).find("#nxtBtn").attr("disabled",false);
		
//		$(this).find("#savesrvCharge").attr("disabled", true);
	} else if (opration && opration == "edit") {
		//$(this).find("#slabNo").attr("disabled", true);
		//$(this).find("#saveareamst").attr("disabled", true);
	}
	
};

$.fn.saveCharge = function() {
	var FI= $(this).parents('.modal-content').find('form');
	if(!FI.ValidateForm() ){//|| !$(this).validate(FI)) {
		return false;
	}
	var chargeJson = (serializeJSONIncludingDisabledFields(FI));
	
	
	$(this).cAjaxPostJson('Post','./product/saveServiceCharges',(chargeJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Service Charge!", "", "warning");  
	} else  if(strResponseText) { 
		var srvchrg = $.parseJSON(strResponseText);
		if(srvchrg.scId){
			swal("Saved!", "Service Charge  saved successfully.", "success");
			$('#srvChargeModal').modal('hide');
			$(this).reloadGrid("srvChargeList", srvChargeListListId, srvChargeListGridOptions);
			$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};	
