$.fn.togglePass = function(){
    var eyeId = $(this).parent().find('input').attr('type');
    if(eyeId == "password"){
        $(this).find('i').attr('class', 'fa fa-eye-slash')
        $(this).parent().find('input').attr('type','text')
    }
    else{
        $(this).find('i').attr('class', 'fa fa-eye')
        $(this).parent().find('input').attr('type','password')
    }
}