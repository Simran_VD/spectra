var ptnrTransdispid = '19'
var ptnrIdViaJava = '';
$.fn.gettransaction = function(pmId){
	if(pmId){
		ptnrIdViaJava = pmId;
	}
    $(this).addTab("Manual_Tran", './pages?name=wallet/addEditManualTran', " Wallet");
}

$.fn.ptnrTransList = function($FI) {
	
	var FI = $("#" +$FI)
	if(ptnrIdViaJava == ''){
		FI.find("#ptnrId").val(ptnrId);
	}
	else{
		FI.find("#ptnrId").val(ptnrIdViaJava);
	}
	ptnrTransGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			

		};
	// var listDiv = FI.find("#ptnrTransLst");
	$(this).reloadGrid("ptnrTransLst", ptnrTransdispid, ptnrTransGridOptions,$FI,whereClause);	
    FI.find("#ptnrTransLst").closest('form').attr("oninput","$(this).onptnrTransFilterTextBoxChanged()");
    

    $("#"+$FI+" .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		var data = "";
		if(operation != "add new"){
			try{
				data = onSelectionChanged(ptnrTransGridOptions)[0].pl_id;
			}
			catch(e){
				data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
			case "edit":
			case "add new":
			case "view":
				ptnrId = FI.find("#ptnrId").val();
            	// $(this).addTab("AddPartner",'./pages?name=masters/addEditPtnrMst',"Add Partner",{"action":"new"});
            	$("#leadgerBox").modal('show');
            	$(this).prepareLedgerForm(operation,data,ptnrId);
				break;
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});


}

$.fn.onptnrTransFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	ptnrTransGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.prepareLedgerForm = function(operation,data,ptnrId){
	var $FI = $("#ledgerForm");
	
	if(operation == "add new"){
		$.fn.ResetAllElemOfForm($FI);
		$FI.find("#tranDate").attr("disabled","diabled");
	}
	
	$FI.find("#pmId").val(ptnrId);
	$FI.cAjax('GET','./getStaticValues?keys=datetime', '', false, false, '', 'Error', true);
	leadGerGenTime =  $.parseJSON(strResponseText);
	$FI.find("#tranDate").val(leadGerGenTime.datetime);
	// $FI.cAjax('GET','./partner/getPartnerList', '', false, false, '', 'Error', true);
	// partners = $.parseJSON(strResponseText);
	// $FI.populateAdvanceSelect($FI.find("#pmId"),partners,{"key":"pmId","value":"name"});
	// $FI.find("#pmId").select2();
	$FI.cAjax('GET','./product/getTranType', '', false, false, '', 'Error', true);
	tranFor = $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#ttmId"),tranFor,{"key":"ttmId","value":"tranDesc"});
	$FI.find("#ttmId").select2();
	$FI.cAjax('GET','./product/getPaymentMode', '', false, false, '', 'Error', true);
	tranMode = $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#pmmId"),tranMode,{"key":"pmmId","value":"paymentMode"});
	$FI.find("#pmmId").select2();
	
	if(operation == "view"){
		$FI.find("input, select, button").attr("disabled", "disabled");
		$FI.find("#cancelLedgerMdl").removeAttr("disabled");
		$FI.fillLedgerDtl($FI,data);
	}

}

$.fn.onPtnrChange = function(){
	var $FI = $("#ledgerForm");
	var ptId = $(this).val();
	$FI.cAjax('GET','./product/getPartnerDetails', 'pmId='+ptId, false, false, '', 'Error', true);
	customers= $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#cstName"),customers,{"key":"slId","value":"customerName"});
	$FI.find("#cstName").select2();
}

$.fn.onTranForChange = function(){
	var $FI = $("#ledgerForm");
	var ptId = $FI.find("#pmId").val();
	$FI.find("#ptnrNameBox").css('display','');
	if(($(this).val()) == 3 || ($(this).val()) ==2) {
		$FI.find("#cstBox").css('display','');
		$FI.cAjax('GET','./product/getPartnerDetails', 'pmId='+ptId, false, false, '', 'Error', true);
		customers= $.parseJSON(strResponseText);
		$FI.populateAdvanceSelect($FI.find("#cstName"),customers,{"key":"slId","value":"customerName"});
		$FI.find("#cstName").select2();
	}
	else{
		$FI.find("#cstBox").css('display','none');
	}
	
}

$.fn.fillLedgerDtl = function($FI,data){
	$FI.cAjax('GET','./product/getPartnerLedger', 'plId='+data, false, false, '', 'Error', true);
	ledgerData = $.parseJSON(strResponseText);
	$FI.find("#plId").val(ledgerData.plId);
	$FI.find("#tranNo").val(ledgerData.tranNo);
	$FI.find("#tranDate").val(ledgerData.tranDate);
	$FI.find("#ttmId").val(ledgerData.ttmId);
	$FI.find("#ttmId").select2();
	$FI.find("#ttmId").trigger('onchange');
	$FI.find("#pmId").val(ledgerData.pmId);

	$FI.find("#slId").val(ledgerData.slId);
	$FI.find("#pmmId").val(ledgerData.pmmId);
	$FI.find("#pmmId").select2();
	$FI.find("#referenceNo").val(ledgerData.referenceNo);
	$FI.find("#tranAmount").val(ledgerData.tranAmount);
	$FI.find("#remarks").val(ledgerData.remarks);
}

$.fn.saveLedger = function(){
	var FI = $(this).closest('form');
	var btn = $(this);
	
	if(FI.ValidateForm()){
		btn.attr('disabled','disabled');
		var manualTranFormJson = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
		$(this).cAjaxPostJson('Post','./product/savePartnerLedger',JSON.stringify(manualTranFormJson), false, false, '', 'Error', true);
		if(strResponseText){
			btn.removeAttr('disabled');
			var manualTranReturn = $.parseJSON(strResponseText);
			if(manualTranReturn.plId){
				$("#leadgerBox").modal('hide');
				swal("Saved!","Transaction Was Successful", "success");
				$(this).reloadGrid("ptnrTransLst", ptnrTransdispid, ptnrTransGridOptions,$FI,whereClause);
				return true;
			};
		}
		else{
			btn.removeAttr('disabled');
			swal({title: "Error !",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
		};
	};
};