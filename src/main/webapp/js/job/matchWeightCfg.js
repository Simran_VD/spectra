var matchweightcfglistdispid=18;
var matchweightcfgListGridOptions;	

$.fn.getmatchweightcfg = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("matchweightcfgList",'./pages?name=job/matchweightcfgList',"Match Weight Config",'child',currDash);

}
$.fn.matchweightcfgList = function(){
	
	matchweightcfgListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("matchweightcfg", matchweightcfglistdispid, matchweightcfgListGridOptions);

	$("#matchweightcfgContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(matchweightcfgListGridOptions)[0].param_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#paramNameSpan').html('');
			$('#weightageSpan').html('');
			$('#prioritySpan').html('');
			$('#remarksMWCFSpan').html('');
			$('#matchweightcfgModel').resetMstDataForm();
			$('#matchweightcfgModel').modal('show');
			$('#matchweightcfgModel').find("#statusActMWCF").val("Y").prop("checked", "true");
			$('#matchweightcfgModel').find("#statusInActMWCF").val("N");
		//	$('#matchweightcfgModel').fillLocMstDropDown();
			$('#matchweightcfgModel').fillmatchweightcfgForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./master/getMatchWeightCfg',"paramId="+data, false, false, '', 'Error', true);
			var matchweightcfgJson = $.parseJSON(strResponseText);
			$('#paramNameSpan').html('');
			$('#weightageSpan').html('');
			$('#prioritySpan').html('');
			$('#remarksMWCFSpan').html('');
			$('#matchweightcfgModel').modal('show');
			$('#matchweightcfgModel').fillmatchweightcfgForm(matchweightcfgJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#matchweightcfg").closest(".row").addClass("listContainer");
	$("#matchweightcfg").closest('form').attr("oninput","$(this).onmatchweightcfgFilterTextBoxChanged()");

};

$.fn.onmatchweightcfgFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	matchweightcfgListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

//$.fn.fillLocMstDropDown = function() {
//	 $(this).cAjax('GET','./job/getAllState', '', false, false, '', 'Error', true);
//	 stateNameJson =  $.parseJSON(strResponseText)
//	    $(this).populateAdvanceSelect($(this).find("#stateid"),stateNameJson,{"key":"stateId","value":"stateName"});
//}

$.fn.fillmatchweightcfgForm = function(matchweightcfgJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(matchweightcfgJson.siteid);
	$(this).find("#paramId").val(matchweightcfgJson.paramId);
	$(this).find("#paramName").val(matchweightcfgJson.paramName);
	$(this).find("#weightage").val(matchweightcfgJson.weightage);
	$(this).find("#priority").val(matchweightcfgJson.priority);
	$(this).find("#remarksMWCF").val(matchweightcfgJson.remarks);
	$(this).find("input[name=isActive][value='"+matchweightcfgJson.isActive+"']").prop("checked", "true");

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeMatchWeightcfgBtn2").attr("disabled", false);
		$(this).find("#statusActMWCF").attr("disabled",true);
		$(this).find("#statusInActMWCF").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
		$(this).find("#statusInActMWCF").attr("disabled",false);
	    $(this).find("#statusActMWCF").attr("disabled",false);
	}
	  var FI= $(this).find('form');
	  var mWCFJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".mWCFhiddenWholeForm").val(mWCFJson)
};

$.fn.saveMatchWeightcfg = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	      } 

	if(!FI.ValidateForm() || !$(this).validateMWCF(FI)) {
		return false;
	}
	var mWCFJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".mWCFhiddenWholeForm").val() == (mWCFJson)){
		swal("Saved!", "Parameter Name saved successfully.", "success");
		$('#matchweightcfgModel').modal('hide');
		$(this).reloadGrid("matchweightcfg", matchweightcfglistdispid, matchweightcfgListGridOptions);
		// $("#matchweightcfg").closest('form').trigger('oninput');
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./master/saveMatchWeightCfg',(mWCFJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Parameter Name!", "", "warning");  
	} else if(strResponseText) { 
		var mWCFReturn = $.parseJSON(strResponseText);
		if(mWCFReturn.paramId != null && mWCFReturn.paramId > "0"){
			swal("Saved!", "Parameter Name saved successfully.", "success");
			$('#matchweightcfgModel').modal('hide');
			$(this).reloadGrid("matchweightcfg", matchweightcfglistdispid, matchweightcfgListGridOptions);
		//	$("#matchweightcfg").closest('form').trigger('oninput');
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateMWCF = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var paramName = FI.find('#paramName').val();

	
	if(!(paramName.length > 0) ) {
		swal({title: "Parameter Name Can not be blank!",  text: "Enter Value in Parameter Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
	return true;
};

$.fn.checkMWCFLength = function(){
	var FI = $(this).closest('form');
	var paramNameLength = $("#paramName").val().length.toString().trim()
	var remarksMWCFLength =$("#remarksMWCF").val().length.toString()
	var weightageLength =$("#weightage").val().length.toString()
	var priorityLength =$("#priority").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = paramNameLength.match(/\S+/g)
		if(checkLength > 255){
			$("#paramNameSpan").html("(Max 255 characters are allowed)")
			FI.find("#paramName").val(FI.find("#paramName").val().substr(0,255))		
		}
		else{
			$("#paramNameSpan").empty()
		}
		
		var checkRemarkLength = remarksMWCFLength.match(/\S+/g)
	    if(checkRemarkLength > 256){
			$("#remarksMWCFSpan").html("(Max 256 characters are allowed)")
			FI.find("#remarksMWCF").val(FI.find("#remarksMWCF").val().substr(0,256))	
		}
		else{
			$("#remarksMWCFSpan").empty()
		}
		
		
		var checkWeightageLength = weightageLength.match(/\S+/g)
	    if(checkWeightageLength > 3){
			$("#weightageSpan").html("(Max 3 Digit Number are allowed)")
			 FI.find("#weightage").val(FI.find("#weightage").val().substr(0,3))
				
		}
		else{
			$("#weightageSpan").empty()
		}
		
		var checkPriorityLength = priorityLength.match(/\S+/g)
	    if(checkPriorityLength > 2){
			$("#prioritySpan").html("(Max 2 Digit Number are allowed)")
			 FI.find("#priority").val(FI.find("#priority").val().substr(0,2))		
		}
		else{
			$("#prioritySpan").empty()
		}
   } 

function validateNumber(evt) {
	var e = evt || window.event;
	var key = e.keyCode || e.which;

	if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
			// numbers   
			key >= 48 && key <= 57 ||
			// Numeric keypad
			key >= 96 && key <= 105 ||
			// Backspace and Tab and Enter
			key == 8 || key == 9 || key == 13 ||
			// Home and End and Space
			key == 35 || key == 36 ||key == 32||
			// left and right arrows
			key == 37 || key == 39 ||
			// Del and Ins
			key == 46 || key == 45||
			
			key == 110||key == 190) {	
		// input is VALID
	}
	else {
		// input is INVALID
		e.returnValue = false;
		if (e.preventDefault) e.preventDefault();
	}
}


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};