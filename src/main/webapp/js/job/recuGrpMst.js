var recuGrpMstlistdispid=11;
var recuGrpMstListGridOptions;	

$.fn.getrecugroup = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("recuGrpMstList",'./pages?name=job/recuGrpMstList',"Recruitment Group",'child',currDash);

}
$.fn.recuGrpMstList = function(){
	
	recuGrpMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("recuGroupMst", recuGrpMstlistdispid, recuGrpMstListGridOptions);

	$("#recuGroupMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(recuGrpMstListGridOptions)[0].rgm_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#rgroupSpan').html('');
			$('#gTypeSpan').html('');
			$('#remarksRGSpan').html('');
			$('#grpType').val("");
			$('#grpType').select2();
			$('#recuGroupMstModel').resetMstDataForm();
			$('#recuGroupMstModel').modal('show');
			$('#recuGroupMstModel').find("#statusActRG").val("Y").prop("checked", "true");
			$('#recuGroupMstModel').find("#statusInActRG").val("N");
		//	$('#skillCatMstModel').resetMstDataForm();
			$('#recuGroupMstModel').fillRecuGrpMstForm();
		//	$('#skillGroupMstModel').checkSkillMstLength();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./master/getRecuGrpMst',"rgmId="+data, false, false, '', 'Error', true);
			var recuGrpMstJson = $.parseJSON(strResponseText);
			$('#rgroupSpan').html('');
			$('#gTypeSpan').html('');
			$('#remarksRGSpan').html('');
			$('#recuGroupMstModel').modal('show');
			$('#recuGroupMstModel').fillRecuGrpMstForm(recuGrpMstJson,operation);
			break;

//		case "delete":
//			swal({
//				title: "Are you sure want  to delete Skill Group ?",
//				text: "You will not be able to recover Skill Group now!",
//				type: "warning",
//				showCancelButton: true,
//				confirmButtonClass: "btn-danger",
//				confirmButtonText: "Yes, delete it!",
//				cancelButtonText: "No cancel please!",
//				confirmButtonColor:'#3095d6',
//				cancelmButtonColor:'#d33',
//				closeOnConfirm: false	,
//				closeOnCancel: false,
//				showConfirmButton: true,
//			},
//			function(isConfirm) {
//				if (isConfirm) {
//			$(this).cAjax('POST','./job/deleteSkillGroupMst',"sgmId="+data, false, false, '', 'Error', true);
//			if(strResponseText == "SUCCESS"){
//				swal("Deleted!", "Selected record deleted successfully.", "success");
//				$(this).reloadGrid("skillGroupMst", skillGroupMstlistdispid, skillGroupMstListGridOptions);
//			}else if(strResponseText == "DATA_INTEGRITY"){
//				swal("Error in deletion!", "Selected Skill Group is being used. So it cannot be deleted.", "error");
//			}
//				}
//				else {
//					swal("Cancelled", "Thank you! your Skill Group  is safe :)", "error");
//				}});
//			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#recuGroupMst").closest(".row").addClass("listContainer");
	$("#recuGroupMst").closest('form').attr("oninput","$(this).onRecuGroupMstFilterTextBoxChanged()");

};

$.fn.onRecuGroupMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	recuGrpMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillRecuGrpMstForm = function(recuGrpMstJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteId").val(recuGrpMstJson.siteId);
	$(this).find("#rgmId").val(recuGrpMstJson.rgmId);
	$(this).find("#recuGrpName").val(recuGrpMstJson.recuGrpName);
	$(this).find("#grpType").val(recuGrpMstJson.grpType).select2();
	$(this).find("#remarksRG").val(recuGrpMstJson.remarks);
	$(this).find("input[name=isActive][value='"+recuGrpMstJson.isActive+"']").prop("checked", "true");
//	$(this).find("#isActive").val(skillCatJson.isActive);

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#recuGrpName").attr("disabled","disabled");
		$(this).find("#remarksRG").attr("disabled","disabled");
		$(this).find("#closeRecuGMstBtn2").attr("disabled", false);
	//	$(this).find("#closeSkillGroupMstBtn1").attr("disabled", false);
		$(this).find("#statusActRG").attr("disabled","disabled");
		$(this).find("#statusInActRG").attr("disabled","disabled");

	}else if(operation && operation == "edit" ) {
	//	$(this).find("#skillGroupName").attr("disabled", true);
		$(this).find("#statusInActRG").attr("disabled",false);
	    $(this).find("#statusActRG").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var recuGroupJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".recruGrouphiddenWholeForm").val(recuGroupJson)
};

$.fn.saveRecruGroupMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
    if(navigator.onLine == false){
	    swal("Warning", "You're offline. Check your connection.", "warning");
	    return false;
      } 

	if(!FI.ValidateForm() || !$(this).validateRecruGroupMst(FI)) {
		return false;
	}
	var recuGroupJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".recruGrouphiddenWholeForm").val() == (recuGroupJson)){
		swal("Saved!", "Recruitment Group saved successfully.", "success");
		$('#recuGroupMstModel').modal('hide');
		$(this).reloadGrid("recuGroupMst", recuGrpMstlistdispid, recuGrpMstListGridOptions);
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./master/saveRecuGrpMst',(recuGroupJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Recruitment Group Name!", "", "warning");  
	} else if(strResponseText) { 
		var recruGroupReturn = $.parseJSON(strResponseText);
		if(recruGroupReturn.rgmId != null && recruGroupReturn.rgmId > "0"){
			swal("Saved!", "Recruitment Group saved successfully.", "success");
			$('#recuGroupMstModel').modal('hide');
			$(this).reloadGrid("recuGroupMst", recuGrpMstlistdispid, recuGrpMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateRecruGroupMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var recuGrpName = FI.find('#recuGrpName').val();
	//var  taxtRate= FI.find('#taxrate').val();

	
	if(!(recuGrpName.length > 0) ) {
		swal({title: "Recruitment Group Name Can not be blank!",  text: "Enter Value in Recruitment Group Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
//
//	if(!$.isNumeric(taxtRate) || !(taxtRate >0 )) {
//		swal({title: "Tax Rate Error!",  text: "Tax Rate Should be Greater than 0", timer: 10000, showConfirmButton: true});
//		return false;	
//	}
//	
	return true;
};

$.fn.checkRecruGMstLength = function(){
	var recruGroupNameLength = $("#recuGrpName").val().length.toString().trim()
	var remarksLength =$("#remarksRG").val().length.toString()
	var grpTypeLength =$("#grpType").val().length.toString()
//	var remarkLength =$("#remarks").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = recruGroupNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#rgroupSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#rgroupSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarksRGSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarksRGSpan").empty()
		}
		
		
		var checkgrpTypeLength = grpTypeLength.match(/\S+/g)
	    if(checkgrpTypeLength == 50){
			$("#gTypeSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#gTypeSpan").empty()
		}
   } 

$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};