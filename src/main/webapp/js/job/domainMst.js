var domainMstlistdispid=20;
var domainMstListGridOptions;	

$.fn.getdomainmst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("domainMstList",'./pages?name=job/domainMstList',"Domain",'child',currDash);

}
$.fn.domainMstList = function(){
	
	domainMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("domainMst", domainMstlistdispid, domainMstListGridOptions);

	$("#domainMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(domainMstListGridOptions)[0].dm_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#domainMstSpan').html('');
			$('#remarkDMSpan').html('');
			$('#domainMstModel').resetMstDataForm();
			$('#domainMstModel').modal('show');
			$('#domainMstModel').find("#statusDMAct").val("Y").prop("checked", "true");
			$('#domainMstModel').find("#statusDMInAct").val("N");
			$('#domainMstModel').fillDomainMstForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./master/getDomainMst',"dmId="+data, false, false, '', 'Error', true);
			var domainMstJson = $.parseJSON(strResponseText);
			$('#domainMstSpan').html('');
			$('#remarkDMSpan').html('');
			$('#domainMstModel').modal('show');
			$('#domainMstModel').fillDomainMstForm(domainMstJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	$("#domainMst").closest(".row").addClass("listContainer");
	$("#domainMst").closest('form').attr("oninput","$(this).onDomainMstFilterTextBoxChanged()");

};

$.fn.onDomainMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	domainMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillDomainMstForm = function(domainMstJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(domainMstJson.siteid);
	$(this).find("#dmId").val(domainMstJson.dmId);
	$(this).find("#domainName").val(domainMstJson.domainName);
	$(this).find("#remarksDM").val(domainMstJson.remarks);
	$(this).find("input[name=isActive][value='"+domainMstJson.isActive+"']").prop("checked", "true");

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeDomainMstBtn2").attr("disabled", false);
		$(this).find("#statusDMAct").attr("disabled",true);
		$(this).find("#statusDMInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
		$(this).find("#statusDMInAct").attr("disabled",false);
	    $(this).find("#statusDMAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var domainJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".domainhiddenWholeForm").val(domainJson)
};

$.fn.saveDomainMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	    }

	if(!FI.ValidateForm() || !$(this).validateDomainMst(FI)) {
		return false;
	}
	var domainJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".domainhiddenWholeForm").val() == (domainJson)){
		swal("Saved!", "Domain Name saved successfully.", "success");
		$('#domainMstModel').modal('hide');
		$(this).reloadGrid("domainMst", domainMstlistdispid, domainMstListGridOptions);
		$("#domainMst").closest('form').trigger('oninput');
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./master/saveDomainMst',(domainJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Domain Name!", "", "warning");  
	} else if(strResponseText) { 
		var domainReturn = $.parseJSON(strResponseText);
		if(domainReturn.dmId != null && domainReturn.dmId > "0"){
			swal("Saved!", "Domain Name saved successfully.", "success");
			$('#domainMstModel').modal('hide');
			$(this).reloadGrid("domainMst", domainMstlistdispid, domainMstListGridOptions);
			$("#domainMst").closest('form').trigger('oninput');
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateDomainMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var domainName = FI.find('#domainName').val();

	
	if(!(domainName.length > 0) ) {
		swal({title: "Domain Name Can not be blank!",  text: "Enter Value in Domain Name", timer: 10000, showConfirmButton: true});
		return false;	
	}	
	return true;
};

$.fn.checkDomainMstLength = function(){
	var domainNameLength = $("#domainName").val().length.toString().trim()
	var remarksLength =$("#remarksDM").val().length.toString()
	
	    var checkLength = domainNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#domainMstSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#domainMstSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarkDMSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarkDMSpan").empty()
		}
		
   } 


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};