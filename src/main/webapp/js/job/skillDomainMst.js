var skillDomainMstlistdispid=4;
var skillDomainMstListGridOptions;	

$.fn.getskilldomainmst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("skillDomainMstList",'./pages?name=job/skillDomainMstList',"Skill Domain",'child',currDash);

}
$.fn.skillDomainMstList = function(){
	
	skillDomainMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("skillDomainMst", skillDomainMstlistdispid, skillDomainMstListGridOptions);

	$("#skillDomainMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(skillDomainMstListGridOptions)[0].sdm_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#sdomainSpan').html('');
			$('#sremarkSpan').html('');
			$('#skillDomainMstModel').resetMstDataForm();
			$('#skillDomainMstModel').modal('show');
			$('#skillDomainMstModel').find("#statusDAct").val("Y").prop("checked", "true");
			$('#skillDomainMstModel').find("#statusDInAct").val("N");
		//	$('#skillCatMstModel').resetMstDataForm();
			$('#skillDomainMstModel').fillSkillDomainForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./job/getSkillDomainMst',"sdmId="+data, false, false, '', 'Error', true);
			var skillDomainJson = $.parseJSON(strResponseText);
			$('#sdomainSpan').html('');
			$('#sremarkSpan').html('');
			$('#skillDomainMstModel').modal('show');
			$('#skillDomainMstModel').fillSkillDomainForm(skillDomainJson,operation);
			break;

		case "delete":
			swal({
				title: "Are you sure want  to delete Skill Domain ?",
				text: "You will not be able to recover Skill Domain now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false	,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if (isConfirm) {
			$(this).cAjax('POST','./job/deleteSkillDomainMst',"sdmId="+data, false, false, '', 'Error', true);
			if(strResponseText == "SUCCESS"){
				swal("Deleted!", "Selected record deleted successfully.", "success");
				$(this).reloadGrid("skillDomainMst", skillDomainMstlistdispid, skillDomainMstListGridOptions);
			}else if(strResponseText == "DATA_INTEGRITY"){
				swal("Error in deletion!", "Selected Skill Domain is being used. So it cannot be deleted.", "error");
			}
				}
				else {
					swal("Cancelled", "Thank you! your Skill Domain  is safe :)", "error");
				}});
			break;
			
//		case "print":
//			$(this).cAjax('POST','./master/getLoggedInSiteid', '', false, false, '', 'Error', true);
//			var siteid =  (strResponseText);
//			var rptUrl = reportingUrl + "/MasterData/HSN.rptdesign&__format=pdf&rp_siteid="+siteid;
//			window.open(rptUrl);
//			break;	

		default:
			swal("Not implemented.");
		}
		return false;
	});
	$("#skillDomainMst").closest(".row").addClass("listContainer");
	$("#skillDomainMst").closest('form').attr("oninput","$(this).onSkillDomainMstFilterTextBoxChanged()");

};

$.fn.onSkillDomainMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	skillDomainMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillSkillDomainForm = function(skillDomainJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(skillDomainJson.siteid);
	$(this).find("#sdmId").val(skillDomainJson.sdmId);
	$(this).find("#skillDomainName").val(skillDomainJson.skillDomainName);
	$(this).find("#remarksD").val(skillDomainJson.remarks);
	$(this).find("input[name=isActive][value='"+skillDomainJson.isActive+"']").prop("checked", "true");
//	$(this).find("#isActive").val(skillCatJson.isActive);

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeSkillDomainMstBtn2").attr("disabled", false);
		$(this).find("#closeSkillDomainMstBtn1").attr("disabled", false);
		$(this).find("#statusDAct").attr("disabled",true);
		$(this).find("#statusDInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
	//	$(this).find("#skillDomainName").attr("disabled", true);
		$(this).find("#statusDInAct").attr("disabled",false);
	    $(this).find("#statusDAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var skillDomainMstJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".skillDomainhiddenWholeForm").val(skillDomainMstJson)
};

$.fn.saveSkillDomainMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	    }

	if(!FI.ValidateForm() || !$(this).validateSkillDomainMst(FI)) {
		return false;
	}
	var skillDomainMstJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".skillDomainhiddenWholeForm").val() == (skillDomainMstJson)){
		swal("Saved!", "Skill Domain saved successfully.", "success");
		$('#skillDomainMstModel').modal('hide');
		$(this).reloadGrid("skillDomainMst", skillDomainMstlistdispid, skillDomainMstListGridOptions);
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./job/saveSkillDomainMst',(skillDomainMstJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Skill Domain Name!", "", "warning");  
	} else if(strResponseText) { 
		var skillDomainReturn = $.parseJSON(strResponseText);
		if(skillDomainReturn.sdmId != null && skillDomainReturn.sdmId > "0"){
			swal("Saved!", "Skill Domain saved successfully.", "success");
			$('#skillDomainMstModel').modal('hide');
			$(this).reloadGrid("skillDomainMst", skillDomainMstlistdispid, skillDomainMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateSkillDomainMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var skillDomainName = FI.find('#skillDomainName').val();
	//var  taxtRate= FI.find('#taxrate').val();

	
	if(!(skillDomainName.length > 0) ) {
		swal({title: "Skill Domain Name Can not be blank!",  text: "Enter Value in Skill Domain Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
//
//	if(!$.isNumeric(taxtRate) || !(taxtRate >0 )) {
//		swal({title: "Tax Rate Error!",  text: "Tax Rate Should be Greater than 0", timer: 10000, showConfirmButton: true});
//		return false;	
//	}
//	
	return true;
};

$.fn.checkSkillDMstLength = function(){
	var skillDomainNameLength = $("#skillDomainName").val().length.toString().trim()
	var remarksLength =$("#remarksD").val().length.toString()
//	var skillNoteLength =$("#skillNote").val().length.toString()
//	var remarkLength =$("#remarks").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = skillDomainNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#sdomainSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#sdomainSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#sremarkSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#sremarkSpan").empty()
		}
		
		
//		var checkPhoneLength = phoneLength.match(/\S+/g)
//	    if(checkPhoneLength == 10){
//			$("#phoneSpan").html("(Max 10 Digit Number are allowed)")
//				
//		}
//		else{
//			$("#phoneSpan").empty()
//		}
   } 


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};