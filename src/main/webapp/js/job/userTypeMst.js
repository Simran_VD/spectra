var userTypeMstlistdispid=9;
var userTypeMstListGridOptions;	

$.fn.getusertype = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("userTypeList",'./pages?name=job/userTypeList',"User Type",'child',currDash);

}
$.fn.userTypeList = function(){
	
	userTypeMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("userTypeMst", userTypeMstlistdispid, userTypeMstListGridOptions);

	$("#userTypeMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(userTypeMstListGridOptions)[0].type_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#userTypeSpan').html('');
			$('#remarksUMSpan').html('');
			$('#userTypeModel').resetMstDataForm();
			$('#userTypeModel').modal('show');
			$('#userTypeModel').find("#statusUserAct").val("Y").prop("checked", "true");
			$('#userTypeModel').find("#statusUserInAct").val("N");
			$('#userTypeModel').fillUserTypeForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./master/getUserTypeMst',"typeId="+data, false, false, '', 'Error', true);
			var userTypeJson = $.parseJSON(strResponseText);
			$('#userTypeSpan').html('');
			$('#remarksUMSpan').html('');
			$('#userTypeModel').modal('show');
			$('#userTypeModel').fillUserTypeForm(userTypeJson,operation);
			break;

//		case "delete":
//			swal({
//				title: "Are you sure want  to delete Skill Group ?",
//				text: "You will not be able to recover Skill Group now!",
//				type: "warning",
//				showCancelButton: true,
//				confirmButtonClass: "btn-danger",
//				confirmButtonText: "Yes, delete it!",
//				cancelButtonText: "No cancel please!",
//				confirmButtonColor:'#3095d6',
//				cancelmButtonColor:'#d33',
//				closeOnConfirm: false	,
//				closeOnCancel: false,
//				showConfirmButton: true,
//			},
//			function(isConfirm) {
//				if (isConfirm) {
//			$(this).cAjax('POST','./job/deleteSkillGroupMst',"sgmId="+data, false, false, '', 'Error', true);
//			if(strResponseText == "SUCCESS"){
//				swal("Deleted!", "Selected record deleted successfully.", "success");
//				$(this).reloadGrid("skillGroupMst", skillGroupMstlistdispid, skillGroupMstListGridOptions);
//			}else if(strResponseText == "DATA_INTEGRITY"){
//				swal("Error in deletion!", "Selected Skill Group is being used. So it cannot be deleted.", "error");
//			}
//				}
//				else {
//					swal("Cancelled", "Thank you! your Skill Group  is safe :)", "error");
//				}});
//			break;
				

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#userTypeMst").closest(".row").addClass("listContainer");
	$("#userTypeMst").closest('form').attr("oninput","$(this).onuserTypeFilterTextBoxChanged()");

};

$.fn.onuserTypeFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	userTypeMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillUserTypeForm = function(userTypeJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(userTypeJson.siteid);
	$(this).find("#typeId").val(userTypeJson.typeId);
	$(this).find("#userType").val(userTypeJson.userType);
	$(this).find("#remarksUser").val(userTypeJson.remarks);
	$(this).find("input[name=isActive][value='"+userTypeJson.isActive+"']").prop("checked", "true");
//	$(this).find("#isActive").val(skillCatJson.isActive);

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#userType").attr("disabled","disabled");
		$(this).find("#remarksUser").attr("disabled","disabled");
		$(this).find("#closeuserTypeBtn2").attr("disabled", false);
		$(this).find("#statusUserAct").attr("disabled","disabled");
		$(this).find("#statusUserInAct").attr("disabled","disabled");

	}else if(operation && operation == "edit" ) {
	//	$(this).find("#skillGroupName").attr("disabled", true);
		$(this).find("#statusUserInAct").attr("disabled",false);
	    $(this).find("#statusUserAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var userTypeMstJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".userTypeMsthiddenWholeForm").val(userTypeMstJson)
};

$.fn.saveuserTypeMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
    if(navigator.onLine == false){
	    swal("Warning", "You're offline. Check your connection.", "warning");
	    return false;
      } 

	if(!FI.ValidateForm() || !$(this).validateUserTypeMst(FI)) {
		return false;
	}
	var userTypeMstJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".userTypeMsthiddenWholeForm").val() == (userTypeMstJson)){
		swal("Saved!", "User Type Name saved successfully.", "success");
		$('#userTypeModel').modal('hide');
		$(this).reloadGrid("userTypeMst", userTypeMstlistdispid, userTypeMstListGridOptions);
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./master/saveUserTypeMst',(userTypeMstJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate User Type Name!", "", "warning");  
	} else if(strResponseText) { 
		var userTypeReturn = $.parseJSON(strResponseText);
		if(userTypeReturn.typeId != null && userTypeReturn.typeId > "0"){
			swal("Saved!", "User Type Name saved successfully.", "success");
			$('#userTypeModel').modal('hide');
			$(this).reloadGrid("userTypeMst", userTypeMstlistdispid, userTypeMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateUserTypeMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var userType = FI.find('#userType').val();
	//var  taxtRate= FI.find('#taxrate').val();

	
	if(!(userType.length > 0) ) {
		swal({title: "User Type Can not be blank!",  text: "Enter Value in User Type", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
//
//	if(!$.isNumeric(taxtRate) || !(taxtRate >0 )) {
//		swal({title: "Tax Rate Error!",  text: "Tax Rate Should be Greater than 0", timer: 10000, showConfirmButton: true});
//		return false;	
//	}
//	
	return true;
};

$.fn.checkuserTypeLength = function(){
	var userTypeLength = $("#userType").val().length.toString().trim()
	var remarksLength =$("#remarksUser").val().length.toString()
//	var skillNoteLength =$("#skillNote").val().length.toString()
//	var remarkLength =$("#remarks").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = userTypeLength.match(/\S+/g)
		if(checkLength == 50){
			$("#userTypeSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#userTypeSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarksUMSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarksUMSpan").empty()
		}
		
		
//		var checkPhoneLength = phoneLength.match(/\S+/g)
//	    if(checkPhoneLength == 10){
//			$("#phoneSpan").html("(Max 10 Digit Number are allowed)")
//				
//		}
//		else{
//			$("#phoneSpan").empty()
//		}
   } 

$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};