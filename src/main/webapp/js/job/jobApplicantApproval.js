var JobApprovallistdispid=30;
var JobApprovalGridOptions;
var managerData;
var whereClause;
$.fn.getsharedwithclient = function(jmid) {
	whereClause = "jm_id='"+jmid+"'"
			var currDash = $(this).closest('.childApp').attr('id');
			$(this).addTab("getsharedwithclient",'./pages?name=job/jobApplicantApprovalList',"Share With Client",'child',currDash);
			
		}
		
		
$.fn.jobApprovalList = function(){
	var id2 =""	
	JobApprovalGridOptions = {
					defaultColDef: {
						sortable: true,
						resizable: true,
						filter: true,
					},
					pagination: true,
					 rowSelection: 'multiple',
					 rowMultiSelectWithClick: true,
					 onRowDoubleClicked: ApplicantResume,
				};
			$(this).reloadShareGrid("JobApproval", JobApprovallistdispid, JobApprovalGridOptions,id2,whereClause);
			
			$("#JobApprovalForm .btn-container .btn").on('click touchstart',function(){
				var operation=$(this).attr("title"),data;
				operation=operation.toLocaleLowerCase();
				if(operation != "add new" && operation != "refresh"){
					try{
						var data = [];
						data.push(onSelectionChanged(JobApprovalGridOptions)[0].jm_id);
						data.push(onSelectionChanged(JobApprovalGridOptions)[0].am_id);
						
					}
					catch(e){
						var data = undefined;
					}
					
					if(!data){
						swal("Please select a row first");
						return false;
					}
					
					var valdata = onSelectionChanged(JobApprovalGridOptions)[0]["ja_id"];
					var amId = onSelectionChanged(JobApprovalGridOptions)[0]["am_id"];
					
				}
				switch(operation) {
				
				case "email":
					$(this).sendEmail(JobApprovalGridOptions);
					//$(this).addTab("ShareJobApproval"+data,'./pages?name=job/jobApplicantApprovalList',"Share With Client-"+data,{"data":data,"action":"edit"});
					break;
				case "edit":
					$(this).addTab("ediShareWithClient"+valdata,'./pages?name=job/addEditJobApplicant',"Edit Share With Client-"+valdata,{"data":data,"action":"edit",valdata,amId});
					break;
				
				
				default:
					swal("Un-implemented feature.");
				}
				return false;
			});

			
			$("#JobApproval").closest('form').attr("oninput","$(this).onJobApprovalFilterTextBoxChanged()");
	
}

/*This Function is used for listing*/
$.fn.reloadShareGrid = function(id, dispid, gridOption, id2, whereClause, additonalParam) {
	if(id2){
		$("#"+id2).find('#'+id).empty();
	}
	else{
		$("#"+id).empty();
	}
	
	if(whereClause){
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type=&page=0&size=20&whereClause='+whereClause, '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	else if(additonalParam != undefined){
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type='+additonalParam+'&page=0&size=20', '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	else{
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type=&page=0&size=20', '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	
//	colModel.displistcols = colModel.displistcols.sort((a, b) => {
//	    return a.columnid - b.columnid;
//	});
	
	var hiddenCol = "";
	var columnDefs = [];
	var columsnName = [];
	var Resume = function(params) {
	    return 	`<a href="#" style="color: #e22462;" onclick="$(this).ViewResume();">Resume</a>` ;
	}
	
	
	$.each(colModel.displistcols, function(){
	    var fieldName = new Object();
	    fieldName.headerName = this.displayname
	    fieldName.field = this.columnname
	    if(this.datatype == 'numeric'){
	    	fieldName.type = 'rightAligned'
	    }
	    
	    if(this.columnname == 'doc_name'){
	    	fieldName.type = 'leftAligned'
	    }
	    columsnName.push(this.columnname)
	    fieldName.hide = false
	   
	    if(this.uniquekey == 'Y' || this.uniquekey == 'y'){
	    	fieldName.hide = true
	    }
	    if(this.sortorder){
			if((this.sortorder).toLowerCase() == 'desc'){
	    		fieldName.sort = 'desc'
	    	}
	    	else{
	    		fieldName.sort = 'asc'
	    	}
		}
	    if(this.hidden == 'Y' || this.hidden == 'y'){
	    	fieldName.hide = true
	    }
	    
	    
	    if(this.columnname == 'resume'){
	    	fieldName.cellRendererSelector = function (params){
	    		var btn = {
	    				component: 'resume',
	    				//params: {'rpt_url':params.data.rpt_url, 'siteid':siteid}
	    		}
		    	return btn
		    }
	    	//fieldName.width = 320
	    }
	    
	    
	    columnDefs.push(fieldName)
	})
	var rowData = [];
	$.each(data.content, function(i, val) {
		var obj = new Object();
		for(var j = 0; j<columsnName.length; j++){
			obj[columsnName[j]] = this[j]
		}
		rowData.push(obj)
	});
	gridOption['columnDefs'] = columnDefs
	gridOption['rowData'] = rowData
	gridOption['components'] = {
			resume: Resume
    }
	gridOption['rowHeight'] = 41;
	gridOption['headerHeight'] = 35;


	if(id2){
		var gridDiv = document.getElementById(id2).querySelector("#"+id);
	}
	else{
		var gridDiv = document.querySelector('#'+id);
	}
	
	
    new agGrid.Grid(gridDiv, gridOption);
    var allColumnIds = [];
    gridOption.columnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });

    gridOption.columnApi.autoSizeColumns(allColumnIds, false);
}




$.fn.onJobApprovalFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	JobApprovalGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.sendEmail = function(row){
	var Data = row.rowData
	var columnName = row.columnDefs
	var FI = $("#ApprovalModal")
	$.each(Data,function(i,val){
		FI.find("#Spocid").empty()
		FI.find("#fmid").empty()
		FI.find("#fromContent").empty()
		FI.find("#toContent").empty()
		FI.find("#CcContent").empty()
		FI.find("#subjectContent").empty()
		FI.find("#recipientContent").empty()
		FI.find("#senderContent").empty()
		
		var SpocId =`<p>`+this.spoc_id+`</p><hr>`
		var formatId =`<p>`+this.fm_id+`</p><hr>`
		var from = `<p contenteditable="true" style="outline:none;">`+this.mgr_email+`</p><hr>`
		var to = `<p contenteditable="true" style="outline:none;">`+this.p_spoc_email+`</p><hr>`
		var CcContent = `<p contenteditable="true" style="outline:none;">`+this.rec_email+`</p><hr>`
		var Subject = `<p contenteditable="true" style="outline:none;">New Applicants for the position `+this.job_title+`</p><hr>`
		var dear = `<p>`+this.p_spoc_name+`,</p> 
		<p contenteditable="true" style="outline:none;">Following candidates have been found suitable for mentioned position. Please review and suggest in case further processing is to be initiated. 
       	</p><hr>`
			
		var Regards = `<p>`+this.recruiter_manager+`</p>`	
		FI.find("#Spocid").append(SpocId)
		FI.find("#fmid").append(formatId)
		FI.find("#fromContent").append(from)
		FI.find("#toContent").append(to)
		FI.find("#CcContent").append(CcContent)
		FI.find("#subjectContent").append(Subject)
		FI.find("#recipientContent").append(dear)
		FI.find("#senderContent").append(Regards)
		
	})
	
	/*This controller is used for candidates format*/
	var formatData =[];
	var SpocId = FI.find("#Spocid").text()
	var formatId = FI.find("#fmid").text()
	if(formatId == "null"){
		formatId = 0;
	}
	$(this).cAjax('GET','./job/getSpocShareFormat', 'spocid='+SpocId+'&fmid='+formatId, false, false, '', 'Error', true);

	var formatJson =  $.parseJSON(strResponseText)
	var fetchRowData = JobApprovalGridOptions.api.getSelectedRows();
	

	var formatJson =  $.parseJSON(strResponseText);
	var fetchRowData = JobApprovalGridOptions.api.getSelectedRows();
	$("#mailTableBody").empty()
	var header = "";
	var headerExist = false;
	$.each(fetchRowData, function(){
	    currentRow = this;
	    row = "<tr>"
	    $.each(formatJson, function(){
	        if(!headerExist){
	            header += '<th class="m-0" nowrap>'+this['colDispName']+'</th>'
	        }
	        if(!(currentRow[this['colName']] == undefined || currentRow[this['colName']] == null)){
	        	row+= ('<td>'+currentRow[this['colName']]+'</td>')
	        }
	        else{
	        	row+= ('<td></td>')
	        }
	    })
	    headerExist = true;
	    row+='</tr>'
	    $("#mailTableBody").append(row);
	})
	headerExist = false;
	$("#mailTableHead tr").html(header)
	$('#ApprovalModal').modal('show');
}


$.fn.Mail = function(){
	var FI = $("#ApprovalModal")
	var FromEmail = FI.find("#fromContent").text()
	var ToEmail = FI.find("#toContent").text()
	var mailCc =  FI.find("#CcContent").text()
	var EmailSubject = FI.find("#subjectContent").text()
	var RecipientContent = FI.find("#recipientContent").text()
	var SenderName = FI.find("#senderContent").text()
	var formatData = FI.find("#msgContentTable").html();
	
	 var abc = `<html><head>
		 	<link rel=
		 	"stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

		 	<!-- jQuery library -->	
		 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

		 	<!-- Popper JS -->
		 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

		 	<!-- Latest compiled JavaScript -->
		 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
		
		 	</head><body>Dear ${RecipientContent}`+formatData+``

		 	  var message= `${abc}  Regards, ${SenderName} </body></html>`
			    console.log(message);	
				var maildata = new FormData();
				maildata.append("mailFrom",FromEmail)
				maildata.append("mailTo",ToEmail)
				maildata.append("mailCc",mailCc)
				maildata.append("mailSubject",EmailSubject)
				maildata.append("mailContent",message)
				maildata.append("files",null)
	
	 jQuery.ajax({url: './job/sendEmail',
 		data:maildata,
 		cache: false,
 		contentType: false,
 		processData: false,
 		type: 'POST',
 		success: function(data){
 			swal("Email Sent!", "The e-mail has been sent successfully.", "success");
 		},
 		error: function(data) {
 			
 			swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
 		}
 	});
				
     
}



/*This is to open Applicant Resume on row double click*/
function ApplicantResume(row) {
	var amId = row.data.am_id
	var irName = "ApplicantResume";
	$FI.cAjax('POST','./FileUpload/getUploadedFiles',"enId=" + amId+'&drName='+irName, false, false, '', 'Error', true);
    var appliResumeFileUpload = $.parseJSON(strResponseText);
	
    if(appliResumeFileUpload.length > 0){
		var path = '../uploads/ApplicantResume/'+amId+"/"+appliResumeFileUpload[0];
		var value = path;
		window.open(value, '_blank'); 
	}
	else{
		swal("Warning","Please Upload Resume Of Applicant.","warning");
	}
}

/*This is to open Applicant Resume on when click on resume link*/
$.fn.ViewResume = function(){
	var applicantId = JobApprovalGridOptions.rowData
	for(var v=0;v<applicantId.length;v++){
	  var Id =   applicantId[v].am_id
	}
	var irName = "ApplicantResume";
	$FI.cAjax('POST','./FileUpload/getUploadedFiles',"enId=" + Id+'&drName='+irName, false, false, '', 'Error', true);
    var appliResumeFileUpload = $.parseJSON(strResponseText);
    if(appliResumeFileUpload.length > 0){
		var path = '../uploads/ApplicantResume/'+Id+"/"+appliResumeFileUpload[0];
		var value = path;
		window.open(value, '_blank'); 
	}
	else{
		swal("Warning","Please Upload Resume Of Applicant.","warning");
	}
}