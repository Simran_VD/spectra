var skillGroupMstlistdispid=5;
var skillGroupMstListGridOptions;	

$.fn.getskillgroupmst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("skillGroupMstList",'./pages?name=job/skillGroupMstList',"Skill Group",'child',currDash);

}
$.fn.skillGroupMstList = function(){
	
	skillGroupMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("skillGroupMst", skillGroupMstlistdispid, skillGroupMstListGridOptions);

	$("#skillGroupMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(skillGroupMstListGridOptions)[0].sgm_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#sgroupSpan').html('');
			$('#remarksGSpan').html('');
			$('#skillGroupMstModel').resetMstDataForm();
			$('#skillGroupMstModel').modal('show');
			$('#skillGroupMstModel').find("#statusGAct").val("Y").prop("checked", "true");
			$('#skillGroupMstModel').find("#statusGInAct").val("N");
		//	$('#skillCatMstModel').resetMstDataForm();
			$('#skillGroupMstModel').fillSkillGroupForm();
		//	$('#skillGroupMstModel').checkSkillMstLength();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./job/getSkillGroupMst',"sgmId="+data, false, false, '', 'Error', true);
			var skillGroupJson = $.parseJSON(strResponseText);
			$('#sgroupSpan').html('');
			$('#remarksGSpan').html('');
			$('#skillGroupMstModel').modal('show');
			$('#skillGroupMstModel').fillSkillGroupForm(skillGroupJson,operation);
			break;

		case "delete":
			swal({
				title: "Are you sure want  to delete Skill Group ?",
				text: "You will not be able to recover Skill Group now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false	,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if (isConfirm) {
			$(this).cAjax('POST','./job/deleteSkillGroupMst',"sgmId="+data, false, false, '', 'Error', true);
			if(strResponseText == "SUCCESS"){
				swal("Deleted!", "Selected record deleted successfully.", "success");
				$(this).reloadGrid("skillGroupMst", skillGroupMstlistdispid, skillGroupMstListGridOptions);
			}else if(strResponseText == "DATA_INTEGRITY"){
				swal("Error in deletion!", "Selected Skill Group is being used. So it cannot be deleted.", "error");
			}
				}
				else {
					swal("Cancelled", "Thank you! your Skill Group  is safe :)", "error");
				}});
			break;
			
//		case "print":
//			$(this).cAjax('POST','./master/getLoggedInSiteid', '', false, false, '', 'Error', true);
//			var siteid =  (strResponseText);
//			var rptUrl = reportingUrl + "/MasterData/HSN.rptdesign&__format=pdf&rp_siteid="+siteid;
//			window.open(rptUrl);
//			break;	

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#skillGroupMst").closest(".row").addClass("listContainer");
	$("#skillGroupMst").closest('form').attr("oninput","$(this).onSkillGroupMstFilterTextBoxChanged()");

};

$.fn.onSkillGroupMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	skillGroupMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillSkillGroupForm = function(skillGroupJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(skillGroupJson.siteid);
	$(this).find("#sgmId").val(skillGroupJson.sgmId);
	$(this).find("#skillGroupName").val(skillGroupJson.skillGroupName);
	$(this).find("#remarksG").val(skillGroupJson.remarks);
	$(this).find("input[name=isActive][value='"+skillGroupJson.isActive+"']").prop("checked", "true");
//	$(this).find("#isActive").val(skillCatJson.isActive);

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#skillGroupName").attr("disabled","disabled");
		$(this).find("#remarksG").attr("disabled","disabled");
		$(this).find("#closeSkillDomainMstBtn2").attr("disabled", false);
		$(this).find("#closeSkillGroupMstBtn1").attr("disabled", false);
		$(this).find("#statusGAct").attr("disabled","disabled");
		$(this).find("#statusGInAct").attr("disabled","disabled");

	}else if(operation && operation == "edit" ) {
	//	$(this).find("#skillGroupName").attr("disabled", true);
		$(this).find("#statusGInAct").attr("disabled",false);
	    $(this).find("#statusGAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var skillGroupMstJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".skillGrouphiddenWholeForm").val(skillGroupMstJson)
};

$.fn.saveSkillGroupMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
    if(navigator.onLine == false){
	    swal("Warning", "You're offline. Check your connection.", "warning");
	    return false;
      } 

	if(!FI.ValidateForm() || !$(this).validateSkillGroupMst(FI)) {
		return false;
	}
	var skillGroupMstJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".skillGrouphiddenWholeForm").val() == (skillGroupMstJson)){
		swal("Saved!", "Skill Group saved successfully.", "success");
		$('#skillGroupMstModel').modal('hide');
		$(this).reloadGrid("skillGroupMst", skillGroupMstlistdispid, skillGroupMstListGridOptions);
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./job/saveSkillGroupMst',(skillGroupMstJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Skill Group Name!", "", "warning");  
	} else if(strResponseText) { 
		var skillGroupReturn = $.parseJSON(strResponseText);
		if(skillGroupReturn.sgmId != null && skillGroupReturn.sgmId > "0"){
			swal("Saved!", "Skill Group saved successfully.", "success");
			$('#skillGroupMstModel').modal('hide');
			$(this).reloadGrid("skillGroupMst", skillGroupMstlistdispid, skillGroupMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateSkillGroupMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var skillGroupName = FI.find('#skillGroupName').val();
	//var  taxtRate= FI.find('#taxrate').val();

	
	if(!(skillGroupName.length > 0) ) {
		swal({title: "Skill Group Name Can not be blank!",  text: "Enter Value in Skill Group Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
//
//	if(!$.isNumeric(taxtRate) || !(taxtRate >0 )) {
//		swal({title: "Tax Rate Error!",  text: "Tax Rate Should be Greater than 0", timer: 10000, showConfirmButton: true});
//		return false;	
//	}
//	
	return true;
};

$.fn.checkSkillGMstLength = function(){
	var skillGroupNameLength = $("#skillGroupName").val().length.toString().trim()
	var remarksLength =$("#remarksG").val().length.toString()
//	var skillNoteLength =$("#skillNote").val().length.toString()
//	var remarkLength =$("#remarks").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = skillGroupNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#sgroupSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#sgroupSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarksGSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarksGSpan").empty()
		}
		
		
//		var checkPhoneLength = phoneLength.match(/\S+/g)
//	    if(checkPhoneLength == 10){
//			$("#phoneSpan").html("(Max 10 Digit Number are allowed)")
//				
//		}
//		else{
//			$("#phoneSpan").empty()
//		}
   } 

$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};