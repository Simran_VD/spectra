var positionMstlistdispid=28;
var PositionMstListGridOptions;	

$.fn.getpositionmst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("positionMstList",'./pages?name=job/positionMstList',"Position",'child',currDash);

}
$.fn.positionMstList = function(){
	
	PositionMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("positionMst", positionMstlistdispid, PositionMstListGridOptions);

	$("#positionMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(PositionMstListGridOptions)[0].pm_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#positionMstSpan').html('');
			$('#remarkPosMstSpan').html('');
			$('#positionMstModel').resetMstDataForm();
			$('#positionMstModel').modal('show');
			$('#positionMstModel').find("#statusPosMstAct").val("Y").prop("checked", "true");
			$('#positionMstModel').find("#statusPosMstInAct").val("N");
			$('#positionMstModel').fillPosMstForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./master/getPositionMst',"pmId="+data, false, false, '', 'Error', true);
			var posMstJson = $.parseJSON(strResponseText);
			$('#positionMstSpan').html('');
			$('#remarkPosMstSpan').html('');
			$('#positionMstModel').modal('show');
			$('#positionMstModel').fillPosMstForm(posMstJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	$("#positionMst").closest(".row").addClass("listContainer");
	$("#positionMst").closest('form').attr("oninput","$(this).onPositionMstFilterTextBoxChanged()");

};

$.fn.onPositionMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	PositionMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillPosMstForm = function(posMstJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(posMstJson.siteid);
	$(this).find("#pmId").val(posMstJson.pmId);
	$(this).find("#positionName").val(posMstJson.positionName);
	$(this).find("#remarksPosMst").val(posMstJson.remarks);
	$(this).find("input[name=isActive][value='"+posMstJson.isActive+"']").prop("checked", "true");

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closePosMstBtn2").attr("disabled", false);
		$(this).find("#statusPosMstAct").attr("disabled",true);
		$(this).find("#statusPosMstInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
		$(this).find("#statusPosMstInAct").attr("disabled",false);
	    $(this).find("#statusPosMstAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var positionJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".posMsthiddenWholeForm").val(positionJson)
};

$.fn.savePosMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	    }

	if(!FI.ValidateForm()) {
		return false;
	}
	var positionJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".posMsthiddenWholeForm").val() == (positionJson)){
		swal("Saved!", "Position Name saved successfully.", "success");
		$('#positionMstModel').modal('hide');
		$(this).reloadGrid("positionMst", positionMstlistdispid, PositionMstListGridOptions);
		$("#positionMst").closest('form').trigger('oninput');
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./master/savePositionMst',(positionJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Position Name!", "", "warning");  
	} else if(strResponseText) { 
		var positionReturn = $.parseJSON(strResponseText);
		if(positionReturn.pmId != null && positionReturn.pmId > "0"){
			swal("Saved!", "Position Name saved successfully.", "success");
			$('#positionMstModel').modal('hide');
			$(this).reloadGrid("positionMst", positionMstlistdispid, PositionMstListGridOptions);
			$("#positionMst").closest('form').trigger('oninput');
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};


$.fn.checkPositionMstLength = function(){
	var FI = $(this).closest('form');
	var positionNameLength = $("#positionName").val().length.toString().trim()
	var remarksLength =$("#remarksPosMst").val().length.toString()
	
	    var checkLength = positionNameLength.match(/\S+/g)
		if(checkLength > 256){
			$("#positionMstSpan").html("(Max 256 characters are allowed)")
			FI.find("#positionName").val(FI.find("#positionName").val().substr(0,256))	
		}
		else{
			$("#positionMstSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength > 256){
			$("#remarkPosMstSpan").html("(Max 256 characters are allowed)")
			FI.find("#remarksPosMst").val(FI.find("#remarksPosMst").val().substr(0,256))	
		}
		else{
			$("#remarkPosMstSpan").empty()
		}
		
   } 


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};