var JobApplicantListdispid=22;
var JobApplicantInterviewdispid=23;
var JobApplicantListGridOptions;
var JobApplicantInterviewGridOptions;
var valdata;
var caseOpenDate;
var score;
var interviewJaiId;
var jobMst;
var amId;
var whereClause;
var id2;
var companyName;

$.fn.getjobapplicant = function()
{
	$(this).addTab("Job_Applicant",'./pages?name=job/jobApplicantList',"Job Applicant");	
}

$.fn.jobApplicantList = function()
{
	JobApplicantListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
	};
	$(this).reloadGrid("jobAppl", JobApplicantListdispid, JobApplicantListGridOptions);

	$("#jobApplicantContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = [];
				data.push(onSelectionChanged(JobApplicantListGridOptions)[0].jm_id);
				data.push(onSelectionChanged(JobApplicantListGridOptions)[0].am_id);

			}
			catch(e){
				var data = undefined;
			}

			if(!data){
				swal("Please select a row first");
				return false;
			}

			
			
			if(operation == "edit"){
				var data_status = onSelectionChanged(JobApplicantListGridOptions)[0].status
				if(data_status == "L1 Reject" || data_status == "L2 Reject" || data_status == "L3 Reject" || data_status == "Closed" || data_status == "HR Reject" || data_status == "Screen Reject" || data_status == "Written Test Rejected" ){
					if(data_status == "Closed"){
						swal(`Job Has Been Closed. Kindly Click On View For More Details.`)
					}
					else{
						swal(`Applicant Has Been Rejected. Kindly Click On View For More Details.`)
					}
					return false;
				}
			}
			
			valdata = onSelectionChanged(JobApplicantListGridOptions)[0]["ja_id"];
			amId = onSelectionChanged(JobApplicantListGridOptions)[0]["am_id"];
			caseOpenDate =  onSelectionChanged(JobApplicantListGridOptions)[0]["createddt"];
			score = onSelectionChanged(JobApplicantListGridOptions)[0]["score"];
			companyName = onSelectionChanged(JobApplicantListGridOptions)[0]["company_name"];
		}
		switch(operation) {
		case "add new":
			$(this).addTab("AddJobApplicantr",'./pages?name=job/addEditJobApplicant',"Add Job Applicant",{"action":"new"});
			break;
		case "edit":
			$(this).addTab("EditJobApplicant"+valdata,'./pages?name=job/addEditJobApplicant',"Edit Job Applicant-"+valdata,{"data":data,"action":"edit"});
			break;
		case "view":
			$(this).addTab("ViewJobApplicant"+valdata,'./pages?name=job/addEditJobApplicant',"View Job Applicant-"+valdata,{"data":data,"action":"view"});
			break;
		case "delete":
			swal({
				title: "Are you sure want to delete Job Applicant?",
				text: "You will not be able to recover Job Applicant now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false	,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if(isConfirm){
					$(this).cAjax('Post','./job/deleteJobMaster',"slid="+data, false, false, '', 'Error', true);
					if(strResponseText == "SUCCESS"){
						swal("Deleted!", "Selected record deleted successfully.", "success");
						$(this).reloadGrid("jobAppl", JobApplicantListdispid, JobApplicantListGridOptions);

					} else if(strResponseText == "DATA_INTEGRITY"||strResponseText == ""){
						swal("Error in deletion!", "Selected Item is being used. So it cannot be deleted.", "error");
					}
				} 
				else {
					swal("Cancelled", "Thank you! your Job Master is safe :)", "error");
				}
			});
			break;
		case "refresh":
			$(this).reloadGrid("jobAppl", JobApplicantListdispid, JobApplicantListGridOptions);

			swal("Refresh!","Job Applicant  Refresh Successfully.", "success");
			break;	
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});
	$("#jobAppl").closest('form').attr("oninput","$(this).onJobApplicantFilterTextBoxChanged()");		
}	

$.fn.onJobApplicantFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	JobApplicantListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.jobInterviewList = function(FI) {
	whereClause = "ja_id='"+valdata+"'";
	id2 =FI.attr('id');
		JobApplicantInterviewGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,

			},
			pagination: true,

			rowSelection: 'single',


	};
	FI.reloadGrid("jobApplicantInterview", JobApplicantInterviewdispid, JobApplicantInterviewGridOptions,id2,whereClause);


	$FI.find("#jobApplicantInterviewCont .btn-container .btn").on('click touchstart',function(){
		event.preventDefault();
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(JobApplicantInterviewGridOptions)[0].jai_id;

			}
			catch(e){
				var data = undefined;
			}

			if(!data){
				swal("Please select a row first");
				return false;
			}
			interviewJaiId = data;

		}

		var modalForm = $("#interviewModal");
		$(this).cAjax('GET','./job/getInterviewLevel', '', false, false, '', 'Error', true);
		interviewLevel = JSON.parse(strResponseText);
		$(this).populateAdvanceSelect(modalForm.find("#ilmId"),interviewLevel,{"key":"ilmId","value":"level"});
		
		$(this).cAjax('GET','./job/getInterviewStatus', '', false, false, '', 'Error', true);
		interviewStatus = JSON.parse(strResponseText);
		$(this).populateAdvanceSelect(modalForm.find("#ismId"),interviewStatus,{"key":"ismid","value":"status"});

		switch(operation) {
		case "add new":
			$('#interviewModal').resetMstDataForm();
			$('#interviewModal').modal('show');
			$("#sdate").datetimepicker({format: 'dd/mm/yyyy hh:ii', startDate: new Date(),autoclose: 1, minuteStep: 30,clear :true});
			break;
		case "edit":
			$(this).cAjax('POST','./job/getInterview',"jaiId="+data, false, false, '', 'Error', true);
			var interJson = $.parseJSON(strResponseText);
			$('#interviewModal').modal('show');
			$('#interviewModal').fillInterviewForm(interJson);
			break;
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});

}

$.fn.fillInterviewForm = function(interJson){
	$(this).find("#sdate").val(interJson.schDate);
	$(this).find("#panel").val(interJson.panelName);
	$(this).find("#ismId").val(interJson.ismId);
	$(this).find("#ismId").select2();
	$(this).find("#feedback").val(interJson.panelFeedback);
	$(this).find("#ilmId").val(interJson.ilmId);
}

$.fn.prepareJobInterviewModal = function(FI)
{
	$(this).cAjax('GET','./job/getInterviewStatus', '', false, false, '', 'Error', true);
	interviewStatus = JSON.parse(strResponseText);
	$(this).populateAdvanceSelect(FI.find("#ismId"),interviewStatus,{"key":"ismId","value":"status"});
}

$.fn.prepareJobApplicantForm = function($FI)
{
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined,openingStockNumber = '';

	/*This is used to get ja_id in valdata from job Approval Tab*/
	if(option.valdata){
		valdata = option.valdata
	}
	
	/*This is used to get am_id   from job Approval Tab to open applicant master*/
	if(option.amId){
		amId = option.amId
	}
	var formContainer = $FI;

	$FI.find(".gridfilter").css("display","none");

	$FI.find("#caseNo").val(valdata);
	$FI.find("#caseOpenDate").val(caseOpenDate);

	$(this).cAjax('GET','./job/getAllCityView', '', false, false, '', 'Error', true);
	locati =  $.parseJSON(strResponseText)
	$(this).populateMultipleSelect($FI.find("#loca"),locati,{"key":"lmId","value":"cityName"});
	$(this).populateMultipleSelect($FI.find("#currloc"),locati,{"key":"lmId","value":"cityName"});
	$FI.find("#loca").select2();
	$FI.find("#currloc").select2();

	$(this).cAjax('GET','./job/getAllCityView', '', false, false, '', 'Error', true);
	Prefloca =  $.parseJSON(strResponseText)
	$(this).populateMultipleSelect($FI.find("#prefLoc"),Prefloca,{"key":"lmId","value":"cityName"});
	$FI.find("#prefLoc").select2();

	$(this).cAjax('GET','./job/getAllCompanyNameView', '', false, false, '', 'Error', true);
	cmpname =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect($FI.find("#comp"),cmpname,{"key":"clId","value":"companyName"});
	$FI.find("#comp").select2();

	$(this).cAjax('GET','./job/getAllskillView', "", false, false, '', 'Error', true);
	skillview =  $.parseJSON(strResponseText)
	$(this).populateMultipleSelect($FI.find("#skil"),skillview,{"key":"smId","value":"skillName"});
	$FI.find("#skil").select2();

	$(this).cAjax('GET','./job/getStatus', "", false, false, '', 'Error', true);
	status1 = JSON.parse(strResponseText);
	$(this).populateAdvanceSelect($FI.find("#status"),status1,{"key":"csmId","value":"status"});
	$FI.find("#status").val(1).select2();

	$(this).cAjax('GET','./job/getSpocDropdown', "", false, false, '', 'Error', true);
	spoc = JSON.parse(strResponseText);
	$(this).populateAdvanceSelect($FI.find("#spocName"),spoc,{"key":"spocId","value":"spocName"});

	$(this).cAjax('GET','./job/getRejection', '', false, false, '', 'Error', true);
	reject = JSON.parse(strResponseText);
	$(this).populateAdvanceSelect($FI.find("#armId"),reject,{"key":"armId","value":"reasons"});

	$(this).cAjax('GET','./job/getBackedOutApplicant', '', false, false, '', 'Error', true);
	backout = JSON.parse(strResponseText);
	$(this).populateAdvanceSelect($FI.find("#abomId"),backout,{"key":"abomId","value":"reasons"});

	$(this).cAjax('GET','./getDbSetting', '', false, false, '', 'Error', true);
	budgetJson =  $.parseJSON(strResponseText);

	$(this).cAjax('GET','./job/ExperienceDropDownVw', '', false, false, '', 'Error', true);
	ExpRelJson =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect($FI.find("#workExp"),ExpRelJson,{"key":"values","value":"displayName"});
	$(this).populateAdvanceSelect($FI.find("#relExp"),ExpRelJson,{"key":"values","value":"displayName"});

	//budgetJson is used for fetching Rs. in Lacs from db_setting table
	for(var b=0;b<budgetJson.length;b++){
		if(budgetJson[b].value == "Rs. in Lacs"){
			$FI.find("#budgetLabel").html("Budget"+" ("+budgetJson[b].value+")")
			$FI.find("#CurrentCTC").html("Current CTC"+" ("+budgetJson[b].value+")")
			$FI.find("#ExpectedCTC").html("Expected CTC"+" ("+budgetJson[b].value+")")
		}
	}

	$FI.find("#saveJobApplicant").on('click touchstart',function(event){
		if(formContainer.ValidateForm()){

			var spocName = $FI.find("#spocName").val();
			if(spocName == null  || spocName == undefined || spocName==""){
				swal("Warning","Kindly update spoc for this job !","warning");
			     return false;
			} 

			//To pass remarks and ntmid of notes
			var arr = [];
			var notesval = $FI.find("#note").val();
			var first = new Object();
			first.remarks = notesval;
			first.ntmId = "2";
			first.isActive = 'Y';
			arr.push(first);

			//To pass remarks and ntmid of followup
			var followUpval = $FI.find("#followUp").val();
			var date = $FI.find("#followUpDate").val();
			var second = new Object();
			second.remarks = followUpval;
			second.ntmId = "3";
			second.isActive = 'Y';
			second.followUpDt = date;
			arr.push(second);

			var jobApplicantFormJson = $.parseJSON(serializeJSONIncludingDisabledFields($FI));
			jobApplicantFormJson.jobApplNotes = arr;

			jobApplicantFormJson.armId = $FI.find("#armId").val();
			jobApplicantFormJson.abomId = $FI.find("#abomId").val();
			$(this).cAjaxPostJson('Post','./job/saveJobApplicantNotes',JSON.stringify(jobApplicantFormJson), false, false, '', 'Error', true);

			xhrObj = $(this).data("xhr");
			if(xhrObj.status == 409) {
				swal("Duplicate Job Code!", "", "warning");  
			}

			else if(strResponseText){

				var jobApplicantReturn = $.parseJSON(strResponseText);
				if(jobApplicantReturn.jaId){
					swal("Saved!", "Job Applicant saved successfully.", "success");
					$FI.closeUserTab();
					$(this).getjobapplicant();
					$(this).reloadGrid("jobAppl", JobApplicantListdispid, JobApplicantListGridOptions);
					return true;
				}
				else{
					swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					return false;
				}
			}
		}
	});

	if(keyValue){
		if(keyValue[0]){
			$(this).cAjax('GET','./job/getJobMaster',"jmId="+keyValue[0], false, false, '', 'Error', true);
			jobMst = $.parseJSON(strResponseText);
		}

		if(keyValue[1]){
			$(this).cAjax('GET','./job/getApplicantMaster',"amId="+keyValue[1], false, false, '', 'Error', true);
			applicant = $.parseJSON(strResponseText);
		}

		$(this).cAjax('GET','./job/getJobApplicant',"jaId="+valdata, false, false, '', 'Error', true);
		jobApplicant = JSON.parse(strResponseText);

		var action = option ? option.action : undefined;

		if(action && action=="view" ){
			$FI.find("input,select,textarea,button").attr("disabled","disabled")
			$FI.find("#show").removeAttr("onclick");
			$FI.find("#refresh").removeAttr("onclick");
			$FI.find("#closeDesignationBtn2").removeAttr("disabled",false);
			$FI.find(".cross").removeAttr("disabled",false);
		}
		else if(action && action=="edit"){
			$FI.find("input,select,textarea").attr("disabled","disabled");
			$FI.find("#followUpDate").removeAttr("disabled");
			$FI.find("#note").removeAttr("disabled");
			$FI.find("#followUp").removeAttr("disabled");
			$FI.find("#status").removeAttr("disabled");
			$FI.find("#isNegoActive").removeAttr("disabled");
			$FI.find("#isNegoInactive").removeAttr("disabled");	
			$FI.find("#armId").removeAttr("disabled");
			$FI.find("#abomId").removeAttr("disabled");
			$FI.find("#sdate").removeAttr("disabled");
			$FI.find("#panel").removeAttr("disabled");
			$FI.find("#feedback").removeAttr("disabled");
			$FI.find("#remarks").removeAttr("disabled");
			$FI.find("btnClosePopup").removeAttr("disabled");
		}
		
		$(this).fillJobApplicantForm($FI,jobMst,applicant,jobApplicant);
	}
}

$.fn.saveInterview = function() {
	var FI= $(this).closest('form');
	if(!FI.ValidateForm() ){//|| !$(this).validate(FI)) {
		return false;
	}
	var interviewJson = $.parseJSON(serializeJSONIncludingDisabledFields(FI));

	Object.assign(interviewJson['jobInterview'][0], {jaId:valdata});
	Object.assign(interviewJson['jobInterview'][0], {jaiId:interviewJaiId});

	var interview = JSON.stringify(interviewJson)
	$(this).cAjaxPostJson('Post','./job/saveAppliInterview',(interview), false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");

	if(xhrObj.status == 409) {
		swal("Duplicate Interview Detail!", "", "warning");  
	} else  if(strResponseText) { 
		var interviewMst = $.parseJSON(strResponseText);
		if(interviewMst.jobInterview[0].jaiId){
			swal("Saved!", "Interview saved successfully.", "success");
			$('#interviewModal').modal('hide');
			FI.reloadGrid("jobApplicantInterview", JobApplicantInterviewdispid, JobApplicantInterviewGridOptions,id2,whereClause);
			$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.fillJobApplicantForm = function($FI,jobMst,applicant,jobApplicant){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined;

	$FI.find("#jaId").val(jobApplicant.jaId);
	$FI.find("#jobCode").val(jobMst.jobCode);
	$FI.find("#jobTitle").val(jobMst.jobTitle);
	$FI.find("#jobOpenDate").val(jobMst.jobRecdDt);
	$FI.find("#budget").val(jobMst.ctc);
	$FI.find("#minExp").val(jobMst.minRelExp);
	for(var i=0; i< jobMst.jobSpoc.length; i++){
		if(jobMst.jobSpoc[i].category == "P"){
			$FI.find("#spocName").val(jobMst.jobSpoc[i].spocId);
		}	
	    $FI.find("#spocName").select2();
	}

	$FI.find("#comp").val(jobMst.clId);
	$FI.find("#comp").select2();

	var loc=[];
	$.each(jobMst.jobLocation,function(i,val){
		loc.push(val.lmId);
	});
	$FI.find("#loca").val(loc);
	$FI.find("#loca").select2();

	$FI.find("#AppName").val(applicant.firstName+" "+applicant.lastName);
	$FI.find("#currloc").val(applicant.currentCityId);
	$FI.find("#currloc").select2();

	var prefLoc = [];
	$.each(applicant.applicantPrefLocation,function(i,val){
		prefLoc.push(val.lmId);
	}); 
	$FI.find("#prefLoc").val(prefLoc);
	$FI.find("#prefLoc").select2();

	var skill=[];
	$.each(applicant.applicantSkill,function(i,val){
		skill.push(val.smId);
	});
	$FI.find("#skil").val(skill);
	$FI.find("#skil").select2();
	$FI.find("#matScore").val(score);
	$FI.find("#mobile").val(applicant.mobile);
	$FI.find("#currCtc").val(applicant.currentCtc);
	$FI.find("#ExpCtc").val(applicant.expCtc);
	$FI.find("#ExpCtc").val(applicant.expCtc);
	$FI.find("#email").val(applicant.email);
	$FI.find("#currEmployer").val(companyName);
	$FI.find("#noticePeriod").val(applicant.noticePeriod);
	$FI.find("#workExp").val(applicant.totalexp).select2();
	$FI.find("#relExp").val(applicant.relExp).select2();
	var stat = jobApplicant.csmId;
	
	//If Applicant status is backed out and rejected then disable all the fields and buttons
	/*if(stat == 20 || stat == 21 || stat == 25 || stat == 9 || stat == 11 || stat == 13 || stat == 15 || stat == 3 || stat == 7){
		$FI.find("input,select,textarea,button").attr("disabled","disabled")
		$FI.find("#closeDesignationBtn2").removeAttr("disabled",false);
		$FI.find(".cross").removeAttr("disabled",false);
	}*/
	$FI.find("#status").val(jobApplicant.csmId).select2();

	//To set radio button
	var value = jobApplicant.negotiable;
	if(value == 1){
		$FI.find("#isNegoActive").prop("checked", "true");
	}
	else{
		$FI.find("#isNegoInactive").prop("checked", "true");	
	}

	var arm = jobApplicant.armId;
	var abom = jobApplicant.abomId;
	if(arm != 0 && arm != null){
		$FI.find("#armId").val(arm);
		$FI.find("#armId").select2();
		$FI.find(".cause").css("display","block");
	}
	if(abom != 0 && abom != null){
		$FI.find("#abomId").val(abom);
		$FI.find("#abomId").select2();
		$FI.find(".issue").css("display","block");
	}
}

$.fn.displayComments = function($FI){
	$(this).cAjax('GET','./job/getJobComments',"jaId="+valdata, false, false, '', 'Error', true);
	dispdata = JSON.parse(strResponseText);
	$.each(dispdata,function(){
		var Box = `<div class="col-md-12 displaydata">
			<p class = "data">`+this.commText+`</p>
			</div>`
			$FI.find(".comm").append(Box);
	})	
}

$.fn.onChangestate = function(){
	var FI= $(this).closest('form');
	var reason = FI.find("#status").val();
	
	if(reason == 9|| reason == 30||reason == 15|| reason == 3 || reason == 6 || reason == 12){
		$FI.find(".issue").css("display", "none");
		$FI.find(".cause").css("display", "block");
		$FI.find("#armId").val(0).select2();
		$FI.find("#abomId").val(0).select2();
	}
	else if(reason == 20 ||reason == 22 || reason == 19){
		$FI.find(".cause").css("display", "none");
		$FI.find(".issue").css("display", "block");
		$FI.find("#armId").val(0).select2();
		$FI.find("#abomId").val(0).select2();
	}
	else{
		$FI.find(".cause").css("display", "none");
		$FI.find(".issue").css("display", "none");
		$FI.find("#armId").val(0).select2();
		$FI.find("#abomId").val(0).select2();
	}
}

$.fn.JobDetail = function() {
	var FI = $(this).closest('form');
	FI.find(".description").html("");
	jobMst.jobDesc = null;
	if(jobMst.jobDesc != null){
		$('#MyPopup').modal('show');
		FI.find(".description").append(jobMst.jobDesc);
	}
	else{
		swal("Job Description not provided!");
	}	
}

$.fn.openResume = function(){
	var irName = "ApplicantResume";
	$FI.cAjax('POST','./FileUpload/getUploadedFiles',"enId=" + amId+'&drName='+irName, false, false, '', 'Error', true);
	var appliResumeFileUpload = $.parseJSON(strResponseText);

	//var path = resume.dirLocation.split("/webapps");

	if(appliResumeFileUpload.length > 0){
		var path = '../uploads/ApplicantResume/'+amId+"/"+appliResumeFileUpload[0];
		var value = path;
		window.open(value, '_blank'); 
	}
	else{
		swal("Warning","Please Upload Resume Of Applicant.","warning");
	}
}

$.fn.onEditApplicant = function(){
	var FI = $(this).closest('form');
	valdata = FI.find("#jaId").val();
	$(this).addTab("EditApplicant"+amId,'./pages?name=job/addEditApplicantMaster',"Edit Applicant-"+amId,{"data":amId,"action":"edit","applicantform":"EditJobApplicant"+valdata});

}	

$.fn.onRefresh = function(){
	var $FI = $(this).closest('form');
	$(this).cAjax('GET','./job/getApplicantMaster',"amId="+amId, false, false, '', 'Error', true);
	var applicant = $.parseJSON(strResponseText);
	$FI.find("#AppName").val(applicant.firstName+" "+applicant.lastName);
	$FI.find("#currloc").val(applicant.currentCityId);
	$FI.find("#currloc").select2();

	var prefLoc = [];
	$.each(applicant.applicantPrefLocation,function(i,val){
		prefLoc.push(val.lmId);
	}); 
	$FI.find("#prefLoc").val(prefLoc);
	$FI.find("#prefLoc").select2();

	var skill=[];
	$.each(applicant.applicantSkill,function(i,val){
		skill.push(val.smId);
	});
	$FI.find("#skil").val(skill);
	$FI.find("#skil").select2();
	$FI.find("#mobile").val(applicant.mobile);
	$FI.find("#currCtc").val(applicant.currentCtc);
	$FI.find("#ExpCtc").val(applicant.expCtc);
	$FI.find("#email").val(applicant.email);
	$FI.find("#noticePeriod").val(applicant.noticePeriod);
	$FI.find("#workExp").val(applicant.totalexp).select2();
	$FI.find("#relExp").val(applicant.relExp).select2();
}


