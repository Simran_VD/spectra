var skillCatMstlistdispid=3;
var skillCatMstListGridOptions;	

$.fn.getskillcatmst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("skillCatMstList",'./pages?name=job/skillCatMstList',"Skill Category",'child',currDash);

}
$.fn.skillCatMstList = function(){
	
	skillCatMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("skillCatMst", skillCatMstlistdispid, skillCatMstListGridOptions);

	$("#skillCatMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(skillCatMstListGridOptions)[0].scm_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#scategorySpan').html('');
			$('#remarksCSpan').html('');
			$('#skillCatMstModel').resetMstDataForm();
			$('#skillCatMstModel').modal('show');
			$('#skillCatMstModel').find("#statusCatAct").val("Y").prop("checked", "true");
			$('#skillCatMstModel').find("#statusCatInAct").val("N");
		//	$('#skillCatMstModel').resetMstDataForm();
			$('#skillCatMstModel').fillSkillCatForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./job/getSkillCatMst',"scmId="+data, false, false, '', 'Error', true);
			var skillCatJson = $.parseJSON(strResponseText);
			$('#scategorySpan').html('');
			$('#remarksCSpan').html('');
			$('#skillCatMstModel').modal('show');
			$('#skillCatMstModel').fillSkillCatForm(skillCatJson,operation);
			break;

		case "delete":
			swal({
				title: "Are you sure want  to delete Skill Cat?",
				text: "You will not be able to recover Skill Cat now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false	,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if (isConfirm) {
			$(this).cAjax('POST','./job/deleteSkillCatMst',"scmId="+data, false, false, '', 'Error', true);
			if(strResponseText == "SUCCESS"){
				swal("Deleted!", "Selected record deleted successfully.", "success");
				$(this).reloadGrid("skillCatMst", skillCatMstlistdispid, skillCatMstListGridOptions);
			}else if(strResponseText == "DATA_INTEGRITY"){
				swal("Error in deletion!", "Selected Skill Cat is being used. So it cannot be deleted.", "error");
			}
				}
				else {
					swal("Cancelled", "Thank you! your Skill Cat  is safe :)", "error");
				}});
			break;
			
//		case "print":
//			$(this).cAjax('POST','./master/getLoggedInSiteid', '', false, false, '', 'Error', true);
//			var siteid =  (strResponseText);
//			var rptUrl = reportingUrl + "/MasterData/HSN.rptdesign&__format=pdf&rp_siteid="+siteid;
//			window.open(rptUrl);
//			break;	

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#skillCatMst").closest(".row").addClass("listContainer");
	$("#skillCatMst").closest('form').attr("oninput","$(this).onSkillCatMstFilterTextBoxChanged()");

};

$.fn.onSkillCatMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	skillCatMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillSkillCatForm = function(skillCatJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(skillCatJson.siteid);
	$(this).find("#scmId").val(skillCatJson.scmId);
	$(this).find("#skillCatName").val(skillCatJson.skillCatName);
	$(this).find("#remarksC").val(skillCatJson.remarks);
	$(this).find("input[name=isActive][value='"+skillCatJson.isActive+"']").prop("checked", "true");
//	$(this).find("#isActive").val(skillCatJson.isActive);

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeSkillCatMstBtn2").attr("disabled", false);
//		$(this).find("#closeSkillCatMstBtn1").attr("disabled", false);
		$(this).find("#statusCatAct").attr("disabled",true);
		$(this).find("#statusCatInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
	//	$(this).find("#skillCatName").attr("disabled", true);
		$(this).find("#statusCatInAct").attr("disabled",false);
	    $(this).find("#statusCatAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var skillCatMstJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".skillcathiddenWholeForm").val(skillCatMstJson)
};

$.fn.saveSkillCatMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	      } 

	if(!FI.ValidateForm() || !$(this).validateSkillCatMst(FI)) {
		return false;
	}
	var skillCatMstJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".skillcathiddenWholeForm").val() == (skillCatMstJson)){
		swal("Saved!", "Skill Cat saved successfully.", "success");
		$('#skillCatMstModel').modal('hide');
		$(this).reloadGrid("skillCatMst", skillCatMstlistdispid, skillCatMstListGridOptions);
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./job/saveSkillCatMst',(skillCatMstJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Skill Cat Name!", "", "warning");  
	} else if(strResponseText) { 
		var skillCatReturn = $.parseJSON(strResponseText);
		if(skillCatReturn.scmId != null && skillCatReturn.scmId > "0"){
			swal("Saved!", "Skill Cat saved successfully.", "success");
			$('#skillCatMstModel').modal('hide');
			$(this).reloadGrid("skillCatMst", skillCatMstlistdispid, skillCatMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateSkillCatMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var skillCatName = FI.find('#skillCatName').val();
	//var  taxtRate= FI.find('#taxrate').val();

	
	if(!(skillCatName.length > 0) ) {
		swal({title: "Skill Cat Name Can not be blank!",  text: "Enter Value in Skill Cat Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
//
//	if(!$.isNumeric(taxtRate) || !(taxtRate >0 )) {
//		swal({title: "Tax Rate Error!",  text: "Tax Rate Should be Greater than 0", timer: 10000, showConfirmButton: true});
//		return false;	
//	}
//	
	return true;
};

$.fn.checkSkillCMstLength = function(){
	var skillCatNameLength = $("#skillCatName").val().length.toString().trim()
	var remarksLength =$("#remarksC").val().length.toString()
//	var skillNoteLength =$("#skillNote").val().length.toString()
//	var remarkLength =$("#remarks").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = skillCatNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#scategorySpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#scategorySpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarksCSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarksCSpan").empty()
		}
		
		
//		var checkPhoneLength = phoneLength.match(/\S+/g)
//	    if(checkPhoneLength == 10){
//			$("#phoneSpan").html("(Max 10 Digit Number are allowed)")
//				
//		}
//		else{
//			$("#phoneSpan").empty()
//		}
   } 


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};