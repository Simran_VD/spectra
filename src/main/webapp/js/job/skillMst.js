var skillMstListId = 6;
var skillMstGridoptions;

$.fn.getskillmst= function(){
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("skillMst", './pages?name=/job/skillMstList', 'Skill Master','child',currDash);
};


$.fn.skillMstList = function() {
	skillMstGridoptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("skillMst", skillMstListId, skillMstGridoptions);

	$("#skillMstContainer .btn-container .btn").on('click touchstart',function(){
		var opration=$(this).attr("title"),data;
		opration=opration.toLocaleLowerCase();
		if(opration != "add new"){
			try{
				var data = onSelectionChanged(skillMstGridoptions)[0].sm_id;
			}
			catch(e){
				var data = undefined;
			}
			//var table = $('#operationList').DataTable();
			//var data = table.row('.selected').id();
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(opration) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#smSpan').html('');
			$('#descSpan').html('');
			$('#remarksSpan').html('');
			$('#skillMstModel').resetMstDataForm();
			$('#skillMstModel').modal('show');
			$('#skillMstModel').find("#statusSkillMstAct").val("Y").prop("checked", "true");
			$('#skillMstModel').find("#statusSkillMstInAct").val("N");
			$('#skillMstModel').fillSkillMstDropDown();
			$('#skillMstModel').fillSkillMstForm();
			break;
			
		case "edit":
		case "view":
			$(this).cAjax('POST','./job/getSkillMst',"smId="+data, false, false, '', 'Error', true);
			var skillJson = $.parseJSON(strResponseText);
			$('#smSpan').html('');
			$('#descSpan').html('');
			$('#remarksSpan').html('');	
			$('#skillMstModel').modal('show');
			$('#skillMstModel').fillSkillMstDropDown();
			$('#skillMstModel').fillSkillMstForm(skillJson, opration);
			break;
			
		case "delete":
			swal({
				title: "Are you sure want  to delete Skill Mst ?",
				text: "You will not be able to recover Skill Mst now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false	,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if (isConfirm) {
			$(this).cAjax('POST','./job/deleteSkillMst',"smId="+data, false, false, '', 'Error', true);
			if(strResponseText == "SUCCESS"){
				swal("Deleted!", "Selected record deleted successfully.", "success");
				$(this).reloadGrid("skillMst", skillMstListId, skillMstGridoptions);
			}else if(strResponseText == "DATA_INTEGRITY"){
				swal("Error in deletion!", "Selected Operation is being used. So it cannot be deleted.", "error");
			}
		}
		else {
			swal("Cancelled", "Thank you! your  Operation  is safe :)", "error");
		}});
			break;
			
//		case "print":
//			var rptUrl = reportingUrl + "/MasterData/Operation.rptdesign&__format=pdf";
//			window.open(rptUrl);
//			break;		
			
		default:
			swal("Not implemented.");
		}
		return false;
	});
	$("#skillMst").closest(".row").addClass("listContainer");
	$("#skillMst").closest('form').attr("oninput","$(this).onSkillMstFilterTextBoxChanged()");
};

$.fn.onSkillMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	skillMstGridoptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillSkillMstDropDown = function() {
	 $(this).cAjax('GET','./job/getSkillDomainMstListVw', '', false, false, '', 'Error', true);
	    skilldomname =  $.parseJSON(strResponseText)
	    $(this).populateAdvanceSelect($(this).find("#sdmId"),skilldomname,{"key":"sdmId","value":"skillDomainName"});

	    $(this).cAjax('GET','./job/getSkillCatMstListVw', '', false, false, '', 'Error', true);
	    skilldomname =  $.parseJSON(strResponseText)
	    $(this).populateAdvanceSelect($(this).find("#scmId"),skilldomname,{"key":"scmId","value":"skillCatName"});

	    $(this).cAjax('GET','./job/getSkillGroupListVw', '', false, false, '', 'Error', true);
	    skilldomname =  $.parseJSON(strResponseText)
	    $(this).populateAdvanceSelect($(this).find("#sgmId"),skilldomname,{"key":"sgmId","value":"skillGroupName"});

}

$.fn.fillSkillMstForm = function(skillJson, opration) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(skillJson.siteid);
	$(this).find("#smId").val(skillJson.smId);
	$(this).find("#skillName").val(skillJson.skillName);
	$(this).find("#skillDesc").val(skillJson.skillDesc);
	$(this).find("#sgmId").val(skillJson.sgmId);
	$(this).find("#scmId").val(skillJson.scmId);
	$(this).find("#sdmId").val(skillJson.sdmId);
	$(this).find("#remarksSM").val(skillJson.remarks);
	$(this).find("input[name=isActive][value='"+skillJson.isActive+"']").prop("checked", "true");

	if(opration && opration == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
	//	$(this).find("#closeSkillCatMstBtn2").attr("disabled", false);
		$(this).find("#closeSkillMstBtn2").attr("disabled", false);
		$(this).find("#statusSkillMstAct").attr("disabled",true);
		$(this).find("#statusSkillMstInAct").attr("disabled",true);


	}
	
	if(opration && opration == "edit" ) {
		$(this).find("#statusSkillMstInAct").attr("disabled",false);
	    $(this).find("#statusSkillMstAct").attr("disabled",false);
	}
};

$.fn.saveSkillMst = function() {
	event.preventDefault();
	var FI=$(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	      } 

	if(!FI.ValidateForm()) {
		return false;
	}
	var operationObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var groupName = FI.find("#sgmId").val();
	var categoryName = FI.find("#scmId").val();
	var domainName = FI.find("#sdmId").val();
//	var deptArr = [];
//	for (var int = 0; int < depts.length; int++) {
//		var myObject = {};
//		var depObj = {};
//		depObj.deptid = depts[int];
//		myObject.id = depObj;
//		deptArr.push(myObject);
//	}
	operationObj.sgmId = groupName;
	operationObj.scmId = categoryName;
	operationObj.sdmId = domainName;
	var skillMstJson = JSON.stringify(operationObj);
	$(this).cAjaxPostJson('Post','./job/saveSkillMst',skillMstJson, false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Skill Name!", "", "warning");  
	} else if(strResponseText) { 
		var skillM = $.parseJSON(strResponseText);
		if(skillM.smId){
			swal("Saved!", "Skill Master saved successfully.", "success");
			$('#skillMstModel').modal('hide');
			$(this).reloadGrid("skillMst", skillMstListId, skillMstGridoptions);
		}
	}
	return false;
};

//$.fn.validateSkillMst = function() {
//	var option = $("div.active").data("data"),keyValue=option ? option.data : undefined;
//	var action = option ? option.action : undefined;
//	var FI= $(this).parents('.modal-content').find('form');
//	var description = FI.find('#operationDesc').val();
//	var  department= FI.find('#mstOperDept').val();
//
//	
//	if(!(description.length > 0) ) {
//		swal({title: "Description Can not be blank!",  text: "Enter Value in Description Field", timer: 10000, showConfirmButton: true});
//		return false;	
//	}
//	if(actionSm == 'add new') {	
//		var name= FI.find("#operationName").val();
//		var param = 'name='+name;
//		$(this).cAjax('POST','./mstData/isOpreationNameExist', param, false, false, '', 'Error', true);
//		if(strResponseText) {
//			swal("Opeartion Name is already Exist!", "Choose Another Operatio Name.", "success");
//			return false;
//		}
//	}
//
//	return true;
//};


$.fn.checkSkillMstLength = function(){
	var skillNameLength = $("#skillName").val().length.toString().trim()	
	var remarksLength =$("#remarksSM").val().length.toString()
	var skillDescLength =$("#skillDesc").val().length.toString()
//	var remarkLength =$("#remarks").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = skillNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#smSpan").html("(Max 50 characters are allowed)")		
		}
		else{
			$("#smSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarksSpan").html("(Max 50 characters are allowed)")			
		}
		else{
			$("#remarksSpan").empty()
		}
		
		var checkskillDescLength = skillDescLength.match(/\S+/g)
	    if(checkskillDescLength == 50){
			$("#descSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#descSpan").empty()
		}
   } 

