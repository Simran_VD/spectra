var spocMstListId = 21;
var spocMstGridoptions;

$.fn.getspocmst= function(){
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("spocMstList", './pages?name=/job/spocMstList', 'Spoc Person','child',currDash);
};


$.fn.spocMstList = function() {
	spocMstGridoptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("spocMst", spocMstListId, spocMstGridoptions);

	$("#spocMstContainer .btn-container .btn").on('click touchstart',function(){
		var opration=$(this).attr("title"),data;
		opration=opration.toLocaleLowerCase();
		if(opration != "add new"){
			try{
				var data = onSelectionChanged(spocMstGridoptions)[0].spoc_id;
			}
			catch(e){
				var data = undefined;
			}
			//var table = $('#operationList').DataTable();
			//var data = table.row('.selected').id();
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(opration) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#spmSpan').html('');
			$('#emailSpmSpan').html('');
			$('#phoneSpmSpan').html('');
			$('#remarkSpmSpan').html('');
			$('#spocMstModel').resetMstDataForm();
			$('#spocMstModel').modal('show');
			$('#spocMstModel').find("#statusSPMAct").val("Y").prop("checked", "true");
			$('#spocMstModel').find("#statusSPMInAct").val("N");
			$('#spocMstModel').fillSpocMstDropDown();
			$('#spocMstModel').fillSpocMstForm();
			break;
			
		case "edit":
		case "view":
			$(this).cAjax('POST','./master/getSpocMst',"spocId="+data, false, false, '', 'Error', true);
			var spocMstJson = $.parseJSON(strResponseText);
			$('#spmSpan').html('');
			$('#emailSpmSpan').html('');
			$('#phoneSpmSpan').html('');
			$('#remarkSpmSpan').html('');	
			$('#spocMstModel').modal('show');
			$('#spocMstModel').fillSpocMstDropDown();
			$('#spocMstModel').fillSpocMstForm(spocMstJson, opration);
			break;
			
		default:
			swal("Not implemented.");
		}
		return false;
	});
	$("#spocMst").closest(".row").addClass("listContainer");
	$("#spocMst").closest('form').attr("oninput","$(this).onSpocMstFilterTextBoxChanged()");
};

$.fn.onSpocMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	spocMstGridoptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillSpocMstDropDown = function() {
	
    $(this).cAjax('GET','./job/getAllCompanyNameView', '', false, false, '', 'Error', true);
    cmpnameJson =  $.parseJSON(strResponseText)
    $(this).populateAdvanceSelect($(this).find("#clId"),cmpnameJson,{"key":"clId","value":"companyName"});
   
}

$.fn.fillSpocMstForm = function(spocMstJson, opration) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(spocMstJson.siteid);
	$(this).find("#spocId").val(spocMstJson.spocId);
	$(this).find("#spocName").val(spocMstJson.spocName);
	$(this).find("#spocEmail").val(spocMstJson.spocEmail);
	$(this).find("#spocPhone").val(spocMstJson.spocPhone);
	$(this).find("#clId").val(spocMstJson.clId);
//	$(this).find("#sdmId").val(skillJson.sdmId);
	$(this).find("#remarksSPMst").val(spocMstJson.remarks);
	$(this).find("input[name=isActive][value='"+spocMstJson.isActive+"']").prop("checked", "true");

	if(opration && opration == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
	//	$(this).find("#closeSkillCatMstBtn2").attr("disabled", false);
		$(this).find("#closeSPMstBtn2").attr("disabled", false);
		$(this).find("#statusSPMAct").attr("disabled",true);
		$(this).find("#statusSPMInAct").attr("disabled",true);


	}
	
	if(opration && opration == "edit" ) {
		$(this).find("#statusSPMInAct").attr("disabled",false);
	    $(this).find("#statusSPMAct").attr("disabled",false);
	}
};

$.fn.saveSpocMst = function() {
	event.preventDefault();
	var FI=$(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	      } 

	if(!FI.ValidateForm()) {
		return false;
	}
	
	if(FI.ValidateForm()){
		var email = FI.find("#spocEmail").val()
		
		if(email == null  || email == undefined || email=="")
		{
		}
		else
		{
		var pattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;


	  if(!pattern.test(email)) {
	       swal("Warning!", "Please Enter the Valid Email.", "warning");
	     return false;
	    
		  }
     }
	}
	
	if(FI.ValidateForm()){
		var phone = FI.find("#spocPhone").val();
		if(phone.length !=10){
		//	FI.find("#saveSPMstBtn").attr("disabled","disabled");
			 return false;
		}
		
	}
	
	
	var spocMstObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var spocJson = JSON.stringify(spocMstObj);
	$(this).cAjaxPostJson('Post','./master/saveSpocMst',spocJson, false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Spoc Person Name!", "", "warning");  
	} else if(strResponseText) { 
		var spocMstId = $.parseJSON(strResponseText);
		if(spocMstId.spocId){
			swal("Saved!", "Spoc Person Name saved successfully.", "success");
			$('#spocMstModel').modal('hide');
			$(this).reloadGrid("spocMst", spocMstListId, spocMstGridoptions);
		}
	}
	return false;
};

$.fn.checkSpocMstLength = function(){
	var FI = $(this).closest('form');
	var spocNameLength = $("#spocName").val().length.toString().trim()	
	var remarksLength =$("#remarksSPMst").val().length.toString()
	var spocEmailLength =$("#spocEmail").val().length.toString()
	var spocPhoneLength =$("#spocPhone").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = spocNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#spmSpan").html("(Max 50 characters are allowed)")		
		}
		else{
			$("#spmSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarkSpmSpan").html("(Max 50 characters are allowed)")			
		}
		else{
			$("#remarkSpmSpan").empty()
		}
		
		var checkspocEmailLength = spocEmailLength.match(/\S+/g)
	    if(checkspocEmailLength == 64){
			$("#emailSpmSpan").html("(Max 64 characters are allowed)")
				
		}
		else{
			$("#emailSpmSpan").empty()
		}
		
		var checkspocPhoneLength = spocPhoneLength.match(/\S+/g)
	    if(checkspocPhoneLength < 10){
			$("#phoneSpmSpan").html("(Max 10 Digit No. are allowed)")
			//FI.find("#spocPhone").val(FI.find("#spocPhone").val().substr(0,10))	
			  if(FI.find("#spocPhone").val()==0){
					$("#phoneSpmSpan").empty()
				}
			
		}
		else{
			$("#phoneSpmSpan").empty()
		}
   } 

