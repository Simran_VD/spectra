$.fn.getMasterDataList = function(){
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("MasterData",'./pages?name=/job/masterTabData',"Master Data",'child',currDash,"");
};

$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};