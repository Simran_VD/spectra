var JobSharelistdispid=31;
var JobShareGridOptions;
$.fn.getjobwiseshare = function() {

			var currDash = $(this).closest('.childApp').attr('id');
			$(this).addTab("JobSHare",'./pages?name=job/jobShareList',"Job Share With Client",'child',currDash);
			
		}
		
		
$.fn.jobShareList = function(){
		
	JobShareGridOptions = {
					defaultColDef: {
						sortable: true,
						resizable: true,
						filter: true,
					},
					pagination: true,
					 rowSelection: 'single',
					 onRowDoubleClicked: jobShareRowClicked,
				};
			$(this).reloadGrid("JobShare", JobSharelistdispid, JobShareGridOptions);
			
			$("#JobShareContainer .btn-container .btn").on('click touchstart',function(){
				var operation=$(this).attr("title"),data;
				operation=operation.toLocaleLowerCase();
				if(operation != "add new" && operation != "refresh"){
					try{
						
						data.push(onSelectionChanged(JobApprovalGridOptions)[0].jm_id);
						
					}
					catch(e){
						var data = undefined;
					}
					
					if(!data){
						swal("Please select a row first");
						return false;
					}
					
					
					
				}
				switch(operation) {
				
				case "share":
					$(this).sendEmail(JobApprovalGridOptions);
					//$(this).addTab("ShareJobApproval"+data,'./pages?name=job/jobApplicantApprovalList',"Share With Client-"+data,{"data":data,"action":"edit"});
					break;
				case "edit":
					$(this).addTab("ediShareWithClient"+valdata,'./pages?name=job/addEditJobApplicant',"Edit Share With Client-"+valdata,{"data":data,"action":"view",valdata});
					break;
				
				
				default:
					swal("Un-implemented feature.");
				}
				return false;
			});

			
			$("#JobShare").closest('form').attr("oninput","$(this).onJobShareFilterTextBoxChanged()");
	
}


$.fn.onJobShareFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	JobShareGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

function jobShareRowClicked(row){
	var listjmId = row.data.jm_id
	$(this).getsharedwithclient(listjmId)
}