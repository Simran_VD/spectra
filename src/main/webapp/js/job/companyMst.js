var companyMstlistdispid=14;
var companyMstListGridOptions;	

$.fn.getcompanymst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("companyMstList",'./pages?name=job/companyMstList',"Company",'child',currDash);

}

$.fn.companyMstList = function(){
	
	companyMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("companyMst", companyMstlistdispid, companyMstListGridOptions);

	$("#companyMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(companyMstListGridOptions)[0].cm_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#comMstSpan').html('');
			$('#remarkComSpan').html('');
			$('#companyMstModel').resetMstDataForm();
			$('#companyMstModel').modal('show');
			$('#companyMstModel').find("#statusComAct").val("Y").prop("checked", "true");
			$('#companyMstModel').find("#statusComInAct").val("N");
//			$('#companyMstModel').fillComMstDropDown();
			$('#companyMstModel').fillComMstForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./master/getCompanyMst',"cmId="+data, false, false, '', 'Error', true);
			var companyMstJson = $.parseJSON(strResponseText);
			$('#comMstSpan').html('');
			$('#remarkComSpan').html('');
			$('#companyMstModel').modal('show');
	//		$('#companyMstModel').fillComMstDropDown();
			$('#companyMstModel').fillComMstForm(companyMstJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#companyMst").closest(".row").addClass("listContainer");
	$("#companyMst").closest('form').attr("oninput","$(this).onCompanyMstFilterTextBoxChanged()");

};

$.fn.onCompanyMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	companyMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

//$.fn.fillComMstDropDown = function() {
//	 $(this).cAjax('GET','./job/getAllState', '', false, false, '', 'Error', true);
//	 stateNameJson =  $.parseJSON(strResponseText)
//	    $(this).populateAdvanceSelect($(this).find("#stateid"),stateNameJson,{"key":"stateId","value":"stateName"});
//}

$.fn.fillComMstForm = function(companyMstJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(companyMstJson.siteid);
	$(this).find("#cmId").val(companyMstJson.cmId);
	$(this).find("#companyName").val(companyMstJson.companyName);
//	$(this).find("#location").val(companyMstJson.location);
//	$(this).find("#domainName").val(companyMstJson.domainName);
	$(this).find("#remarksCom").val(companyMstJson.remarks);
	$(this).find("input[name=isActive][value='"+companyMstJson.isActive+"']").prop("checked", "true");
//	$(this).find("#isActive").val(skillCatJson.isActive);

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeComMstBtn2").attr("disabled", false);
		$(this).find("#statusComAct").attr("disabled",true);
		$(this).find("#statusComInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
		$(this).find("#statusComInAct").attr("disabled",false);
	    $(this).find("#statusComAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var compMstJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".compMsthiddenWholeForm").val(loCJson)
};

$.fn.saveCompMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	      } 

	if(!FI.ValidateForm()) {
		return false;
	}
	var compMstJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".compMsthiddenWholeForm").val() == (compMstJson)){
		swal("Saved!", "Company saved successfully.", "success");
		$('#companyMstModel').modal('hide');
		$(this).reloadGrid("companyMst", companyMstlistdispid, companyMstListGridOptions);
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./master/saveCompanyMst',(compMstJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Company Name!", "", "warning");  
	} else if(strResponseText) { 
		var companyMstReturn = $.parseJSON(strResponseText);
		if(companyMstReturn.cmId != null && companyMstReturn.cmId > "0"){
			swal("Saved!", "Company saved successfully.", "success");
			$('#companyMstModel').modal('hide');
			$(this).reloadGrid("companyMst", companyMstlistdispid, companyMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.checkCompanyMstLength = function(){
	var FI = $(this).closest('form');
	var companyNameLength = $("#companyName").val().length.toString().trim()
	var remarksLength =$("#remarksCom").val().length.toString()
	
	
	    var checkLength = companyNameLength.match(/\S+/g)
		if(checkLength > 256){
			$("#comMstSpan").html("(Max 256 characters are allowed)")
			FI.find("#companyName").val(FI.find("#companyName").val().substr(0,256))		
		}
		else{
			$("#comMstSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength > 256){
			$("#remarkComSpan").html("(Max 256 characters are allowed)")
			FI.find("#remarksCom").val(FI.find("#remarksCom").val().substr(0,256))		
		}
		else{
			$("#remarkComSpan").empty()
		}
		
}


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};