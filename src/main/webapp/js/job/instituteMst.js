var instituteMstlistdispid=26;
var instituteMstListGridOptions;	

$.fn.getinstitutemaster = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("instituteMstList",'./pages?name=job/instituteMstList',"Institute",'child',currDash);

}

$.fn.instituteMstList = function(){
	
	instituteMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("instituteMst", instituteMstlistdispid, instituteMstListGridOptions);

	$("#instituteMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(instituteMstListGridOptions)[0].im_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#instituteMstSpan').html('');
			$('#remarkInsMstSpan').html('');
			$('#instituteMstModel').resetMstDataForm();
			$('#instituteMstModel').modal('show');
			$('#instituteMstModel').find("#statusInsMstAct").val("Y").prop("checked", "true");
			$('#instituteMstModel').find("#statusInsMstInAct").val("N");
			$('#instituteMstModel').fillInstituteMstForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./master/getInstituteMst',"imId="+data, false, false, '', 'Error', true);
			var instituteMstJson = $.parseJSON(strResponseText);
			$('#instituteMstSpan').html('');
			$('#remarkInsMstSpan').html('');
			$('#instituteMstModel').modal('show');
			$('#instituteMstModel').fillInstituteMstForm(instituteMstJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	$("#instituteMst").closest(".row").addClass("listContainer");
	$("#instituteMst").closest('form').attr("oninput","$(this).onInstituteMstFilterTextBoxChanged()");

};

$.fn.onInstituteMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	instituteMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillInstituteMstForm = function(instituteMstJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(instituteMstJson.siteid);
	$(this).find("#imId").val(instituteMstJson.imId);
	$(this).find("#insName").val(instituteMstJson.insName);
	$(this).find("#remarksInsMst").val(instituteMstJson.remarks);
	$(this).find("input[name=isActive][value='"+instituteMstJson.isActive+"']").prop("checked", "true");

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeInstituteMstBtn2").attr("disabled", false);
		$(this).find("#statusInsMstAct").attr("disabled",true);
		$(this).find("#statusInsMstInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
		$(this).find("#statusInsMstInAct").attr("disabled",false);
	    $(this).find("#statusInsMstAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var instituteJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".institutehiddenWholeForm").val(courseJson)
};

$.fn.saveInstituteMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	    }

	if(!FI.ValidateForm() || !$(this).validateInstituteMst(FI)) {
		return false;
	}
	var instituteJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".institutehiddenWholeForm").val() == (instituteJson)){
		swal("Saved!", "Institute Name saved successfully.", "success");
		$('#instituteMstModel').modal('hide');
		$(this).reloadGrid("instituteMst", instituteMstlistdispid, instituteMstListGridOptions);
		$("#instituteMst").closest('form').trigger('oninput');
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./master/saveInstituteMst',(instituteJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Institute Name!", "", "warning");  
	} else if(strResponseText) { 
		var instituteReturn = $.parseJSON(strResponseText);
		if(instituteReturn.imId != null && instituteReturn.imId > "0"){
			swal("Saved!", "Institute Name saved successfully.", "success");
			$('#instituteMstModel').modal('hide');
			$(this).reloadGrid("instituteMst", instituteMstlistdispid, instituteMstListGridOptions);
			$("#instituteMst").closest('form').trigger('oninput');
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateInstituteMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var insName = FI.find('#insName').val();

	
	if(!(insName.length > 0) ) {
		swal({title: "Institute Name Can not be blank!",  text: "Enter Value in Course Name", timer: 10000, showConfirmButton: true});
		return false;	
	}	
	return true;
};

$.fn.checkInstituteMstLength = function(){
	var FI = $(this).closest('form');
	var insNameLength = $("#insName").val().length.toString().trim()
	var remarksLength =$("#remarksInsMst").val().length.toString()
	
	    var checkLength = insNameLength.match(/\S+/g)
		if(checkLength > 50){
			$("#instituteMstSpan").html("(Max 50 characters are allowed)")
			FI.find("#insName").val(FI.find("#insName").val().substr(0,50))	
		}
		else{
			$("#instituteMstSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength > 256){
			$("#remarkInsMstSpan").html("(Max 256 characters are allowed)")
			FI.find("#remarksInsMst").val(FI.find("#remarksInsMst").val().substr(0,256))		
		}
		else{
			$("#remarkInsMstSpan").empty()
		}
		
   } 


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};