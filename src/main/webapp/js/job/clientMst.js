var clientMstlistdispid=15;
var clientMstListGridOptions;	

$.fn.getclientmst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("clientMstList",'./pages?name=job/clientMstList',"Client",'child',currDash);

}
$.fn.clientMstList = function(){
	
	clientMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("clientMst", clientMstlistdispid, clientMstListGridOptions);

	$("#clientMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(clientMstListGridOptions)[0].cl_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#clientMstSpan').html('');
			$('#remarkclientMSpan').html('');
			$('#companyTp').val("");
			$('#companyTp').select2();
			$('#clientMstModel').resetMstDataForm();
			$('#clientMstModel').modal('show');
			$('#clientMstModel').find("#statusClientMAct").val("Y").prop("checked", "true");
			$('#clientMstModel').find("#statusClientMInAct").val("N");
			$('#clientMstModel').fillClientMstDropDown();
			$('#clientMstModel').fillClientMstForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./master/getClientMst',"clId="+data, false, false, '', 'Error', true);
			var clientMstJson = $.parseJSON(strResponseText);
			$('#clientMstSpan').html('');
			$('#remarkclientMSpan').html('');
			$('#clientMstModel').modal('show');
			$('#clientMstModel').fillClientMstDropDown();
			$('#clientMstModel').fillClientMstForm(clientMstJson,operation);
			break;


		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#clientMst").closest(".row").addClass("listContainer");
	$("#clientMst").closest('form').attr("oninput","$(this).onClientMstFilterTextBoxChanged()");

};

$.fn.onClientMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	clientMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillClientMstDropDown = function() {
	 $(this).cAjax('GET','./job/getAllCityView', '', false, false, '', 'Error', true);
	 locNameJson =  $.parseJSON(strResponseText)
	    $(this).populateAdvanceSelect($(this).find("#locId"),locNameJson,{"key":"lmId","value":"cityName"});
	 
	 $(this).cAjax('GET','./master/getDomainDropdownVw', '', false, false, '', 'Error', true);
	 domainJson =  $.parseJSON(strResponseText)
	    $(this).populateAdvanceSelect($(this).find("#dmId"),domainJson,{"key":"dmId","value":"domainName"});
}

$.fn.fillClientMstForm = function(clientMstJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(clientMstJson.siteid);
	$(this).find("#clId").val(clientMstJson.clId);
	$(this).find("#companyName").val(clientMstJson.companyName);
	$(this).find("#companyTp").val(clientMstJson.companyType).select2();
	$(this).find("#locId").val(clientMstJson.locId);
	$(this).find("#dmId").val(clientMstJson.dmId);
	$(this).find("#remarksClient").val(clientMstJson.remarks);
	$(this).find("input[name=isActive][value='"+clientMstJson.isActive+"']").prop("checked", "true");
//	$(this).find("#isActive").val(skillCatJson.isActive);

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
//		$(this).find("#closeSkillCatMstBtn2").attr("disabled", false);
		$(this).find("#closeClientMstBtn2").attr("disabled", false);
		$(this).find("#statusClientMAct").attr("disabled",true);
		$(this).find("#statusClientMInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
	//	$(this).find("#skillCatName").attr("disabled", true);
		$(this).find("#statusClientMInAct").attr("disabled",false);
	    $(this).find("#statusClientMAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var clientJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".clientMsthiddenWholeForm").val(clientJson)
};

$.fn.saveclientMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	      } 

	if(!FI.ValidateForm() || !$(this).validateclientMst(FI)) {
		return false;
	}
	var clientJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".clientMsthiddenWholeForm").val() == (clientJson)){
		swal("Saved!", "Company saved successfully.", "success");
		$('#clientMstModel').modal('hide');
		$(this).reloadGrid("clientMst", clientMstlistdispid, clientMstListGridOptions);
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./master/saveClientMst',(clientJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Company Name!", "", "warning");  
	} else if(strResponseText) { 
		var clientMstReturn = $.parseJSON(strResponseText);
		if(clientMstReturn.clId != null && clientMstReturn.clId > "0"){
			swal("Saved!", "Company saved successfully.", "success");
			$('#clientMstModel').modal('hide');
			$(this).reloadGrid("clientMst", clientMstlistdispid, clientMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateclientMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var Company = FI.find('#companyName').val();
	//var  taxtRate= FI.find('#taxrate').val();

	
	if(!(Company.length > 0) ) {
		swal({title: "Company Name Can not be blank!",  text: "Enter Value in Company Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
//
//	if(!$.isNumeric(taxtRate) || !(taxtRate >0 )) {
//		swal({title: "Tax Rate Error!",  text: "Tax Rate Should be Greater than 0", timer: 10000, showConfirmButton: true});
//		return false;	
//	}
//	
	return true;
};

$.fn.checkClientMstLength = function(){
	var companyNameLength = $("#companyName").val().length.toString().trim()
	var remarksLength =$("#remarksClient").val().length.toString()
//	var skillNoteLength =$("#skillNote").val().length.toString()
//	var remarkLength =$("#remarks").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = companyNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#clientMstSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#clientMstSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarkclientMSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarkclientMSpan").empty()
		}
		
		
//		var checkPhoneLength = phoneLength.match(/\S+/g)
//	    if(checkPhoneLength == 10){
//			$("#phoneSpan").html("(Max 50 characters are allowed)")
//				
//		}
//		else{
//			$("#phoneSpan").empty()
//		}
   } 


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};