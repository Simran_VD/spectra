var eduMstlistdispid=8;
var eduMstListGridOptions;	

$.fn.geteducation = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("eduMstList",'./pages?name=job/eduMstList',"Education",'child',currDash);

}
$.fn.eduMstList = function(){
	
	eduMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("eduMst", eduMstlistdispid, eduMstListGridOptions);

	$("#eduMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(eduMstListGridOptions)[0].em_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#eduMstSpan').html('');
			$('#remarksEduSpan').html('');
			$('#eduMstModel').resetMstDataForm();
			$('#eduMstModel').modal('show');
			$('#eduMstModel').find("#statusEduAct").val("Y").prop("checked", "true");
			$('#eduMstModel').find("#statusEduInAct").val("N");
			$('#eduMstModel').fillEduMstForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./job/getEduMst',"emId="+data, false, false, '', 'Error', true);
			var eduMstJson = $.parseJSON(strResponseText);
			$('#eduMstSpan').html('');
			$('#remarksEduSpan').html('');
			$('#eduMstModel').modal('show');
			$('#eduMstModel').fillEduMstForm(eduMstJson,operation);
			break;

//		case "delete":
//			swal({
//				title: "Are you sure want  to delete Skill Group ?",
//				text: "You will not be able to recover Skill Group now!",
//				type: "warning",
//				showCancelButton: true,
//				confirmButtonClass: "btn-danger",
//				confirmButtonText: "Yes, delete it!",
//				cancelButtonText: "No cancel please!",
//				confirmButtonColor:'#3095d6',
//				cancelmButtonColor:'#d33',
//				closeOnConfirm: false	,
//				closeOnCancel: false,
//				showConfirmButton: true,
//			},
//			function(isConfirm) {
//				if (isConfirm) {
//			$(this).cAjax('POST','./job/deleteSkillGroupMst',"sgmId="+data, false, false, '', 'Error', true);
//			if(strResponseText == "SUCCESS"){
//				swal("Deleted!", "Selected record deleted successfully.", "success");
//				$(this).reloadGrid("skillGroupMst", skillGroupMstlistdispid, skillGroupMstListGridOptions);
//			}else if(strResponseText == "DATA_INTEGRITY"){
//				swal("Error in deletion!", "Selected Skill Group is being used. So it cannot be deleted.", "error");
//			}
//				}
//				else {
//					swal("Cancelled", "Thank you! your Skill Group  is safe :)", "error");
//				}});
//			break;
				

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#eduMst").closest(".row").addClass("listContainer");
	$("#eduMst").closest('form').attr("oninput","$(this).oneduMstFilterTextBoxChanged()");

};

$.fn.oneduMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	eduMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillEduMstForm = function(eduMstJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(eduMstJson.siteid);
	$(this).find("#emId").val(eduMstJson.emId);
	$(this).find("#eduLevel").val(eduMstJson.eduLevel);
	$(this).find("#remarksEduM").val(eduMstJson.remarks);
	$(this).find("input[name=isActive][value='"+eduMstJson.isActive+"']").prop("checked", "true");
//	$(this).find("#isActive").val(skillCatJson.isActive);

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#eduLevel").attr("disabled","disabled");
		$(this).find("#remarksEduM").attr("disabled","disabled");
		$(this).find("#closeEduMstBtn2").attr("disabled", false);
		$(this).find("#statusEduAct").attr("disabled","disabled");
		$(this).find("#statusEduInAct").attr("disabled","disabled");

	}else if(operation && operation == "edit" ) {
	//	$(this).find("#skillGroupName").attr("disabled", true);
		$(this).find("#statusEduInAct").attr("disabled",false);
	    $(this).find("#statusEduAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var eduJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".eduMsthiddenWholeForm").val(eduJson)
};

$.fn.saveEduMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
    if(navigator.onLine == false){
	    swal("Warning", "You're offline. Check your connection.", "warning");
	    return false;
      } 

	if(!FI.ValidateForm() || !$(this).validateEduMst(FI)) {
		return false;
	}
	var eduJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".eduMsthiddenWholeForm").val() == (eduJson)){
		swal("Saved!", "Education Level saved successfully.", "success");
		$('#eduMstModel').modal('hide');
		$(this).reloadGrid("eduMst", eduMstlistdispid, eduMstListGridOptions);
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./job/saveEduMst',(eduJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Education Level!", "", "warning");  
	} else if(strResponseText) { 
		var eduMstReturn = $.parseJSON(strResponseText);
		if(eduMstReturn.emId != null && eduMstReturn.emId > "0"){
			swal("Saved!", "Education Level saved successfully.", "success");
			$('#eduMstModel').modal('hide');
			$(this).reloadGrid("eduMst", eduMstlistdispid, eduMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateEduMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var eduLevel = FI.find('#eduLevel').val();
	//var  taxtRate= FI.find('#taxrate').val();

	
	if(!(eduLevel.length > 0) ) {
		swal({title: "Education Level Can not be blank!",  text: "Enter Value in Education Level", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
//
//	if(!$.isNumeric(taxtRate) || !(taxtRate >0 )) {
//		swal({title: "Tax Rate Error!",  text: "Tax Rate Should be Greater than 0", timer: 10000, showConfirmButton: true});
//		return false;	
//	}
//	
	return true;
};

$.fn.checkEduMstLength = function(){
	var eduLevelLength = $("#eduLevel").val().length.toString().trim()
	var remarksLength =$("#remarksEduM").val().length.toString()
//	var skillNoteLength =$("#skillNote").val().length.toString()
//	var remarkLength =$("#remarks").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = eduLevelLength.match(/\S+/g)
		if(checkLength == 50){
			$("#eduMstSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#eduMstSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarksEduSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarksEduSpan").empty()
		}
		
		
//		var checkPhoneLength = phoneLength.match(/\S+/g)
//	    if(checkPhoneLength == 10){
//			$("#phoneSpan").html("(Max 10 Digit Number are allowed)")
//				
//		}
//		else{
//			$("#phoneSpan").empty()
//		}
   } 

$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};