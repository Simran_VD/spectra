var AppliMasterlistdispid=2;
var AppliMasterListGridOptions;	
var irName="applicantmstResume";
var inames=[];
var EduJson ="";
var courseJson = "";
var instJson = "";
var ApplicantTab;
$.fn.getapplicantmst = function(rowJmid) {
    $(this).addTab("applicant",'./pages?name=job/applicantMasterList',"Applicant Master");
	
}
	
		
$.fn.applicantMasterList = function(){
		AppliMasterListGridOptions = {
				defaultColDef: {
					sortable: true,
					resizable: true,
					filter: true,
				},
				pagination: true,
				rowSelection: 'single',
			};
		$(this).reloadGrid("applicant", AppliMasterlistdispid, AppliMasterListGridOptions);


		
		$("#applicantContainer .btn-container .btn").on('click touchstart',function(){
			var operation=$(this).attr("title"),data;
			operation=operation.toLocaleLowerCase();
			if(operation != "add new" && operation != "refresh"){
				try{
					var data = onSelectionChanged(AppliMasterListGridOptions)[0].am_id;
					
				}
				catch(e){
					var data = undefined;
				}
				
				if(!data){
					swal("Please select a row first");
					return false;
				}
				
				if(operation == "edit"){
				var data_status = onSelectionChanged(AppliMasterListGridOptions)[0].status;
				if(data_status == "Closed"){
					swal("You cannot edit this Status is closed")
					return false;
					}
				}
				
				
			}
			switch(operation) {
			case "add new":
				$(this).addTab("AddApplicant",'./pages?name=job/addEditApplicantMaster',"Add Applicant",{"action":"new"});
				break;
			case "edit":
				$(this).addTab("EditApplicant"+data,'./pages?name=job/addEditApplicantMaster',"Edit Applicant-"+data,{"data":data,"action":"edit"});
				break;
			case "view":
				$(this).addTab("ViewApplicant"+data,'./pages?name=job/addEditApplicantMaster',"View Applicant-"+data,{"data":data,"action":"view"});
				break;
			case "refresh":
					
		             $(this).reloadGrid("applicant", AppliMasterlistdispid, AppliMasterListGridOptions);
					
					swal("Refresh!","Applicant Master  Refresh Successfully.", "success");
					break;	
			default:
				swal("Un-implemented feature.");
			}
			return false;
		});
		$("#applicant").closest('form').attr("oninput","$(this).onAppliFilterTextBoxChanged()");		
}
$.fn.onAppliFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	AppliMasterListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.prepareJobApplicant = function($FI){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined,openingStockNumber = '';
	
	/*This line is used for fetching tabName */
	var applicantTabName = $FI.find("#applicantTabName").val()
	ApplicantTab = option.tabName? option.tabName : applicantTabName
	
//	This line is used to fetch jmId from active jobs
	var jmId = option.JmId	
	var formContainer = $FI;
	
	
	  $(this).cAjax('GET','./job/getAllCityView', '', false, false, '', 'Error', true);
	  locat =  $.parseJSON(strResponseText)
	  $(this).populateMultipleSelect($FI.find("#location"),locat,{"key":"lmId","value":"cityName"});
	
	
	 $(this).cAjax('GET','./job/getAllSourceType', '', false, false, '', 'Error', true);
	 sourcetypeview =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect($FI.find("#sourceType"),sourcetypeview,{"key":"somId","value":"sourceName"});
	 
	 $(this).cAjax('GET','./job/getAllSkillDoaminName', '', false, false, '', 'Error', true);
	 skilldomname =  $.parseJSON(strResponseText)
	 $(this).populateMultipleSelect($FI.find("#skillDomainName"),skilldomname,{"key":"sdmId","value":"skillDomainName"});
	 
 
	 $(this).cAjax('GET','./job/ExperienceDropDownVw', '', false, false, '', 'Error', true);
	 ExpJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect($FI.find("#workExperience"),ExpJson,{"key":"values","value":"displayName"});
	 $(this).populateAdvanceSelect($FI.find("#relExperience"),ExpJson,{"key":"values","value":"displayName"});
	 
	 
	 $(this).cAjax('GET','./job/getAllEduMst', '', false, false, '', 'Error', true);
	 EduJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect($FI.find("#education"),EduJson,{"key":"emId","value":"eduLevel"});
	 
	 $(this).cAjax('GET','./job/getAllCourseMstVw', '', false, false, '', 'Error', true);
	 courseJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect($FI.find("#courseNameQ"),courseJson,{"key":"coId","value":"coursename"});

	 $(this).cAjax('GET','./job/getAllinstMstVw', '', false, false, '', 'Error', true);
	 instJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect($FI.find("#institute"),instJson,{"key":"imId","value":"insName"});

	 
	  $(this).cAjax('GET','./job/getAllSkillDoaminName', '', false, false, '', 'Error', true);
	  domainName =  $.parseJSON(strResponseText)
	  $(this).populateMultipleSelect($FI.find("#skillDomainName1"),domainName,{"key":"sdmId","value":"skillDomainName"});
	   
	  
	  for(var i=0;i<domainName.length;i++){
		  domainName[i].skillDomainName
          
      if(domainName[i].skillDomainName == "IT"){
      	 var defaultDomain = domainName[i].sdmId
      	 formContainer.find("#skillDomainName1").val(defaultDomain).trigger('change')
          
          }
      }
	  
	 
	 $(this).cAjax('GET','./job/NoticePeriodDropdownListVw', '', false, false, '', 'Error', true);
	 noticePeriodJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect($FI.find("#noticePeriod"),noticePeriodJson,{"key":"values","value":"timePeriod"});
	
	 
	 $FI.cAjax('GET','./job/getAllState', '', false, false, '', 'Error', true);
	 var stateJson= $.parseJSON(strResponseText); 
	 $FI.populateAdvanceSelect($FI.find("#currState"), stateJson, {"key":"stateId","value":"stateName"});
	 
	 $FI.cAjax('GET','./job/getAllState', '', false, false, '', 'Error', true);
	 var stateJson= $.parseJSON(strResponseText); 
	 $FI.populateAdvanceSelect($FI.find("#stateId"), stateJson, {"key":"stateId","value":"stateName"});
	 
	 $(this).cAjax('GET','./master/getCompanyMstVw', '', false, false, '', 'Error', true);
	 companyMstJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect($FI.find("#company"),companyMstJson,{"key":"cmId","value":"companyName"});

	 $(this).cAjax('GET','./master/getPositionMstVw', '', false, false, '', 'Error', true);
	 positionMstJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect($FI.find("#position"),positionMstJson,{"key":"pmId","value":"positionName"});

	 
	 
	 $(this).cAjax('GET','./getDbSetting', '', false, false, '', 'Error', true);
		budgetJson =  $.parseJSON(strResponseText)

		//budgetJson is used for fetching Rs. in Lacs from db_setting table
		for(var b=0;b<budgetJson.length;b++){
		    
		    if(budgetJson[b].value == "Rs. in Lacs"){
		    	
		    	
		        $FI.find("#applicrtctc").html("Current CTC"+" ("+budgetJson[b].value+")")
		        $FI.find("#appliextctc").html("Expected CTC"+" ("+budgetJson[b].value+")")
		    }
		}
	/* $FI.find("#email").focusout(function(){
		 var Email = $FI.find("#email").val()
		 if(Email){
			 $(this).cAjax('GET','./job/CheckDuplicate',"email="+Email, false, false, '', 'Error', true);
			 EmailResponse = (strResponseText)
			 if(EmailResponse == "1"){
				 swal(`Duplicate Email ${Email}`)
				 return false;
			}
		} 
	 })
	 
	  $FI.find("#mobile").focusout(function(){
		 var mobile = $FI.find("#mobile").val()
		 if(mobile){
			 $(this).cAjax('GET','./job/CheckDuplicate',"phoneno="+mobile, false, false, '', 'Error', true);
			 mobileResponse = (strResponseText)
			 if(mobileResponse == "1"){
				 swal(`Duplicate Mobile No. ${mobile}`)
				 return false;
			}
		} 
	 })
	 */
		 $FI.find("#saveAppliMaster").on('click touchstart',function(event){
	  if(formContainer.ValidateForm() && $(this).validateMobAadhPanForm($FI)){
		  updateRowNumbers(formContainer, ".qualDtlRow");	
		  updateRowNumbers(formContainer, ".certDtlRow");	
		  updateRowNumbers(formContainer, ".wrkexpDtlRow");	
	    var email = formContainer.find("#email").val()
			
			if(email == null  || email == undefined || email=="")
			{
			}
			else
			{
			var pattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  
		  if(!pattern.test(email)) {
		       swal("Warning!", "Please Enter the Valid Email.", "warning");
		     return false;
		    
			  }
	     }
	     
	     
	      if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
          } 

            	var jobApplicantFormJson = $.parseJSON(serializeJSONIncludingDisabledFields($FI));
            	Object.assign(jobApplicantFormJson, {tabName:ApplicantTab});
            	Object.assign(jobApplicantFormJson, {jmId:option.JmId});
            	/* Job Location */
			 	
 			   var temp=$FI.find("#location").val();
  			 
 			  var locamid = $FI.find("#amId").val(); 
 				var applicant=[];
 				
 				
 				$.each(temp,function(i,val){
 				var applicantlocation={
 				    lmId:val,
 					isActive:'Y',
 					amId:locamid
 				}
 				applicant.push(applicantlocation);
 				});
 				jobApplicantFormJson.applicantPrefLocation = applicant;
 				
 				
 				
 				/* Skill */
			 	
  			   var skillName=$FI.find("#skillName1").val();
  			 var domainName = $FI.find("#skillDomainName1").val(); 
  			   var amid = $FI.find("#amId").val(); 
  				var applicantSkill=[];
  				
  				
  				$.each(skillName,function(i,val){
  				var applicantSkillname={
  						smId:val,					
  						isActive:'Y',
  						amId:amid,
  						sdmId:domainName[i]
  				}
  				applicantSkill.push(applicantSkillname);
  				});
  				jobApplicantFormJson.applicantSkill = applicantSkill;
  				
  				
  			   var tempResum=$FI.find("#file-input").val();
    			 
  			  var resumeamid = $FI.find("#amId").val(); 
  				var resumeName=[];
  				
  				var applicantResumeName={
  				//	arId:val,
  					resume:tempResum,
  					isActive:'Y',
  					amId:resumeamid
  				}
  				resumeName.push(applicantResumeName);
  				
  				jobApplicantFormJson.resumeName = resumeName;
  						
  	 			var applicantImage=$FI.find("#imageDisp").attr("src");			
  	 			if(applicantImage != "./images/avtara.png"){
  	 				applicantImage = $FI.find("#imageFile").val();
  	 			}
  	 			jobApplicantFormJson.applicantImage = applicantImage;
  				
				$(this).cAjaxPostJson('Post','./job/saveApplicantMaster',JSON.stringify(jobApplicantFormJson), false, false, '', 'Error', true);
				
				
					xhrObj = $(this).data("xhr");
					if(xhrObj.status == 409) {
						swal("Duplicate Email Id!", "", "warning"); 
						return false;
					}
					
				
	  
				 else if(strResponseText){
					var jobApplicantReturn = $.parseJSON(strResponseText);
					if(jobApplicantReturn.amId){

						 var imgFile = $FI.find('#imageFile').get(0).files[0];
					        var formData = new FormData();
					        formData.append("imgFile", imgFile);
					        var name=formData.get('imgFile');
					        if(name !='undefined'){
					        	 jQuery.ajax({url: './FileUpload/uploadImgOfAppliction?amId='+jobApplicantReturn.amId,
							    		data: formData,
							    		cache: false,
							    		contentType: false,
							    		processData: false,
							    		type: 'POST',
							    		success: function(formData){
							    			console.log("Image uploaded Successful");
							    		},
							    		error: function(formData) {
							    			//alert(data);
							    			swal({title: "Error !",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
							    		}
							    	});
					        }
						
						
						  var data=new FormData();					        
					        if(inames.length > 0){
							    for (var i = 0; i < inames.length; ++i) {
							        data.append("resumeName",inames[i])
							    }
							    inames.length=0;
							   jQuery.ajax({url: './FileUpload/FileUpload?enId='+jobApplicantReturn.amId+'&drName='+irName,
							   data: data,
					    		cache: false,
					    		contentType: false,
					    		processData: false,
					    		type: 'POST',
					    		success: function(data){
					    			console.log("File  uploaded Successful");
					    		},
					    		error: function(data) {
					    			//alert(data);
					    			swal({title: "Error !",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					    		}
					    	});
					        }
					        
					        
					        if($FI.find("#applicantformtype").val().length > 0 ){
					        	if(openTabArr.indexOf($FI.find("#applicantformtype").val()) != -1){
					        		$(this).addTab($FI.find("#applicantformtype").val(),'./pages?name=job/addEditJobApplicant',"Edit Job Applicant-"+valdata,{"action":"edit"});
					        		$FI.closeUserTab();
					        	}
					        	else{
					        		swal("Saved!", "Applicant Master Record saved successfully.", "success");
					        		$FI.closeUserTab();
					        	}
					        }
					        else{
					        	swal("Saved!", "Applicant Master Record saved successfully.", "success");
								$FI.closeUserTab();
								$(this).getapplicantmst()
								 $(this).reloadGrid("applicant", AppliMasterlistdispid, AppliMasterListGridOptions);
					        }
						
						return true;
					}
					else{
						swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
						return false;
					}
				}
				  else{
						swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
						return false;
					}
		
		    }
		})
		
		if(keyValue){
			$(this).cAjax('GET','./job/getApplicantMaster',"amId="+keyValue, false, false, '', 'Error', true);
			var applicant = $.parseJSON(strResponseText);
			$(this).fillApplicantMstForm($FI,applicant);
			var action = option ? option.action : undefined;
			if(action && action=="view" ){
				$FI.find("input,select,textarea ,button").attr("disabled","disabled")
			}else if(action && action=="edit"){
				$FI.find("#applicantformtype").val(option.applicantform);
				$FI.find("#deptName").attr("disabled","disabled");
			}
	
		}

	
	
}

$.fn.validateMobAadhPanForm = function($FI){
	
		var mobile = $FI.find("#mobile").val();
		if(mobile.length !=10 && mobile!=""){
			 return false;
		}
		
		var panNo = $FI.find("#panNo").val();
		if(panNo.length !=10 && panNo!=""){
			 return false;
		}
		
		var aadhaar = $FI.find("#aadhaar").val();
		if(aadhaar.length !=12 && aadhaar!=""){
			 return false;
		}
		
		var mobile1 = $FI.find("#mobile1").val();
		if(mobile1.length !=10 && mobile1!=""){
			 return false;
		}
		
		 return true;
}


$.fn.fillApplicantMstForm = function($FI,applicant){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined,openingStockNumber = '';
	
    $FI.find("#amId").val(applicant.amId);
    $FI.find("#sourceType").val(applicant.somId);
    $FI.find("#sourceName").val(applicant.sourceName);
	$FI.find("#email").val(applicant.email);
	$FI.find("#mobile").val(applicant.mobile);
	$FI.find("#mobile1").val(applicant.mobile1);
	$FI.find("#firstName").val(applicant.firstName);
	$FI.find("#lastName").val(applicant.lastName);
	$FI.find("#fatherName").val(applicant.fatherName);
	$FI.find("#motherName").val(applicant.motherName);
	$FI.find("#dob").val(applicant.dob);
	$FI.find("#addr1").val(applicant.addr1);
	$FI.find("#addr2").val(applicant.addr2);
	$FI.find("#gender").val(applicant.gender);
    $FI.find("#stateId").val(applicant.stateId).trigger('change');
    $FI.find("#lmId").val(applicant.lmId);
    $FI.find("#panNo").val(applicant.panNo);
    $FI.find("#aadhaar").val(applicant.aadhaar);
    $FI.find("#expCtc").val(applicant.expCtc);
    $FI.find("#noticePeriod").val(applicant.noticePeriod);
    $FI.find("#workExperience").val(applicant.totalexp); 
    $FI.find("#currCtc").val(applicant.currentCtc);
    $FI.find("#remarks").val(applicant.remarks);
    $FI.find("#relExperience").val(applicant.relExp).select2();
   
    $FI.cAjax('POST','./FileUpload/getApplicantResume',"enId=" + applicant.amId, false, false, '', 'Error', true);
    var appliResumeFile = strResponseText;
    var folderName = "../uploads/ApplicantResume/"+ applicant.amId;
    var folder=applicant.amId;
    var path =  folderName + "/" + appliResumeFile;
    if(appliResumeFile){
    	if(action=='view'){
			$FI.find("#ihorizontal-list").append("<li id='fileRemove'><a class='download' onclick='$(this).download(this);' href='"+ path + "'>"+appliResumeFile+"</a><a href='javascript:function() { return false; }' class='remove' fileName='"+appliResumeFile+"' folderName='"+folder+"' drName='"+irName+"'><b> &times; </a></li>");
		}
		else{
			$FI.find("#ihorizontal-list").append("<li id='fileRemove'><a class='download' onclick='$(this).download(this);' href='"+ path + "'>"+appliResumeFile+"</a><a href='javascript:void(0);' onclick='$(this).removeIFile(this);' class='remove' fileName='"+appliResumeFile+"' folderName='"+folder+"' drName='"+irName+"'><b> &times; </a></li>");
		}
    }
    
    
	/*$FI.cAjax('POST','./FileUpload/getUploadedFiles',"enId=" + applicant.amId+'&drName='+irName, false, false, '', 'Error', true);
    var appliResumeFileUpload = $.parseJSON(strResponseText);
    if(appliResumeFileUpload.length > 0){
		
		var folderName = "../uploads/ApplicantResume/"+ applicant.amId;
	    var folder=applicant.amId;
		for(var i=0;i<appliResumeFileUpload.length;i++) {
			
			if(action=='view'){
				$FI.find("#ihorizontal-list").append("<li id='fileRemove'><a class='download' onclick='$(this).download(this);' href='"+ path + "'>"+appliResumeFileUpload[i]+"</a><a href='javascript:function() { return false; }' class='remove' fileName='"+appliResumeFileUpload[i]+"' folderName='"+folder+"' drName='"+irName+"'><b> &times; </a></li>");
			}
			else{
				$FI.find("#ihorizontal-list").append("<li id='fileRemove'><a class='download' onclick='$(this).download(this);' href='"+ path + "'>"+appliResumeFileUpload[i]+"</a><a href='javascript:void(0);' onclick='$(this).removeIFile(this);' class='remove' fileName='"+appliResumeFileUpload[i]+"' folderName='"+folder+"' drName='"+irName+"'><b> &times; </a></li>");
			}
		}
    }*/
	
    var name=applicant.amId+".png";
	if(imageExists('../uploads/ApplicantProf/'+name)){
		//$FI.find("#imageFile").attr('src','.\/images/applicantmst/Images/'+name).show();
		$FI.find("#imageDisp").attr('src','../uploads/ApplicantProf/'+name).show();
	}
    
    
	$FI.find("#currState").val(applicant.currentstateid).trigger('change');
	$FI.find("#currCity").val(applicant.currentCityId);

	//To set Notice Period radio button
	var value = applicant.inNoticePeriod;
	if(value == "Y"){
		$FI.find("#isNpActive").prop("checked", "true");
	}
	else{
		$FI.find("#isNpInactive").prop("checked", "true");	
	}

	//To set Offer in hand radio button
	var value = applicant.offerInHand;
	if(value == "Y"){
		$FI.find("#isOicActive").prop("checked", "true");
	}
	else{
		$FI.find("#isOicInactive").prop("checked", "true");	
	}

	$FI.find("#lastworkdate").val(applicant.lastWorkingDate);
	if(applicant.oihCtc){
		 $FI.find("#offerInHandCtc").attr("disabled",false)
		 $FI.find("#offerInHandCtc").val(applicant.oihCtc);
	}
   
    
	var loc=[];
	$.each(applicant.applicantPrefLocation,function(i,val){
	  loc.push(val.lmId);
	});
	$FI.find("#location").val(loc);
	
	var skill=[];
	var domainName =[];
	$.each(applicant.applicantSkill,function(i,val){
		domainName.push(val.sdmId);
		skill.push(val.smId);
	});
	$FI.find("#skillDomainName1").val(domainName)
	$FI.find("#skillDomainName1").select2();
	$FI.find("#skillDomainName1").trigger('change');
	$FI.find("#skillName1").val(skill);
	$FI.find("#skillName1").select2();
	
	//edit code for Education Table rows
	 var row = 0;
		$.each(applicant.applicanteducation,function(){
			if(row != 0){
				$FI.find("button[id=addQualButton]").eq(0).click();
			}
			var currentRow = $FI.find(".qualDtlRow").eq(row);

			currentRow.find('#education').val(this.emId);
			currentRow.find('#education').select2();
			currentRow.find('#courseNameQ').val(this.coId);
			currentRow.find('#courseNameQ').select2();
			currentRow.find('#institute').val(this.imId);
			currentRow.find('#institute').select2();
			currentRow.find('#passingYear').val(this.completeyear);
			currentRow.find('#marks').val(this.grade);
			

		row++;
		});
	
		
		//edit code for Certificate Table rows
		 var row = 0;
			$.each(applicant.applicantCertificate,function(){
				if(row != 0){
					$FI.find("button[id=addcertButton]").eq(0).click();
				}
				var certRow = $FI.find(".certDtlRow").eq(row);

				certRow.find('#crtname').val(this.certificate_name);
				certRow.find('#crtby').val(this.certified_by);
				certRow.find('#yearofcmpl').val(this.year_of_compl);
				certRow.find('#isactive').val(this.isactive);
			
			row++;
			});
		
			//edit code for Work Experience Table rows
			 var row = 0;
				$.each(applicant.applicantexperience,function(){
					if(row != 0){
						$FI.find("button[id=addwrkexpButton]").eq(0).click();
					}
					var wrkRow = $FI.find(".wrkexpDtlRow").eq(row);

					wrkRow.find('#company').val(this.cmId);
					wrkRow.find('#company').select2();
					wrkRow.find('#position').val(this.pmId);
					wrkRow.find('#position').select2();
					wrkRow.find('#startdt').val(this.start_date);
					wrkRow.find('#enddt').val(this.end_date);
				
				row++;
				});
		
		
		
		
}

	
   $.fn.ResetAllDataInFormAppliMstt=function(){
		var Form=$(this).closest('form')
		$(this).ResetAllDataInForm(Form)
		$("#fileRemove").remove();
	};
   
   
   $.fn.checkApplicantfieldLength = function(){
	var FI = $(this).closest('form');
	var sourceNameLength =$("#sourceName").val().length.toString()
	var firstNameLength = $("#firstName").val().length.toString().trim()
	var lastNameLength =$("#lastName").val().length.toString()
	var fatherNameLength =$("#fatherName").val().length.toString()
	var motherNameLength =$("#motherName").val().length.toString()
	var address1Length =$("#addr1").val().length.toString()
	var address2Length =$("#addr2").val().length.toString()
	var phoneLength =$("#mobile").val().length.toString()
	var phone1Length =$("#mobile1").val().length.toString()
	var panLength =$("#panNo").val().length.toString()
	var aadharLength =$("#aadhaar").val().length.toString()
	var companyNameAddLength =$("#companyNameAdd").val().length.toString() 
	var remarksComAddLength =$("#remarksComAdd").val().length.toString()
	var positionNameAddLength =$("#positionNameAdd").val().length.toString()  
	var remarksPosAddLength =$("#remarksPosAdd").val().length.toString()
	
	var eduLevelAddLength =$("#eduLevelAdd").val().length.toString()
	var remarksEduAddLength =$("#remarksEduAdd").val().length.toString()
	var courseNameAddLength =$("#courseNameAdd").val().length.toString() 
	var remarksCourseAddLength =$("#remarksCourseAdd").val().length.toString()
	var insNameAddLength =$("#insNameAdd").val().length.toString()  
	var remarksInstituteAddLength =$("#remarksInstituteAdd").val().length.toString()
	
	var skillDomainNameAdd1Length =$("#skillDomainNameAdd1").val().length.toString()
	var remarksDomainAdd1Length =$("#remarksDomainAdd1").val().length.toString()
	var skillNameAddMLength =$("#skillNameAddM").val().length.toString() 
	var remarksSkillAddMLength =$("#remarksSkillAddM").val().length.toString()
	
	
		var checkSourceNameLength = sourceNameLength.match(/\S+/g)
		if(checkSourceNameLength == 32){
			$("#sourceNameSpan").html("(Max 32 characters are allowed)")
				
		}
		else{
			$("#sourceNameSpan").empty()
		}
		
		var checkfirstNameLength = firstNameLength.match(/\S+/g)
	    if(checkfirstNameLength == 32){
			$("#firstNameSpan").html("(Max 32 characters are allowed)")
				
		}
		else{
			$("#firstNameSpan").empty()
		}
		
		var checkLastNameLength = lastNameLength.match(/\S+/g)
	    if(checkLastNameLength == 32){
			$("#lastNameSpan").html("(Max 32 characters are allowed)")
				
		}
		else{
			$("#lastNameSpan").empty()
		}
		
		var checkphoneLength = phoneLength.match(/\S+/g)
	    if(checkphoneLength != 0 && checkphoneLength < 10){
	    	
			$("#phoneSpan").html("(Max 10 Digit No. are allowed)")
		//	FI.find("#mobile").valspocPhonePer(FI.find("#mobile").val().substr(0,10))
			  if(FI.find("#mobile").val()==0){
					$("#phoneSpan").empty()
				}
		}
		else{
			$("#phoneSpan").empty()
		}
		
		var checkphone1Length = phone1Length.match(/\S+/g)
	    if(checkphone1Length != 0 && checkphone1Length < 10){
			$("#phone1Span").html("(Max 10 Digit No. are allowed)")
		//	FI.find("#mobile1").val(FI.find("#mobile1").val().substr(0,10))
			 if(FI.find("#mobile1").val()==0){
					$("#phone1Span").empty()
				}
		}
		else{
			$("#phone1Span").empty()
		}
		
		var checkfatherNameLength = fatherNameLength.match(/\S+/g)
	    if(checkfatherNameLength == 32){
			$("#fatherNameSpan").html("(Max 32 characters are allowed)")
				
		}
		else{
			$("#fatherNameSpan").empty()
		}
		
		var checkmotherNameLength = motherNameLength.match(/\S+/g)
	    if(checkmotherNameLength == 32){
			$("#motherNameSpan").html("(Max 32 characters are allowed)")
				
		}
		else{
			$("#motherNameSpan").empty()
		}
		
		
		var checkaddress1Length = address1Length.match(/\S+/g)
	    if(checkaddress1Length == 4000){
			$("#address1Span").html("(Max 4000 characters are allowed)")
				
		}
		else{
			$("#address1Span").empty()
		}
		
		var checkaddress2Length = address2Length.match(/\S+/g)
	    if(checkaddress2Length == 4000){
			$("#address2Span").html("(Max 4000 characters are allowed)")
				
		}
		else{
			$("#address2Span").empty()
		}
		
		
		var checkAadharLength = aadharLength.match(/\S+/g)
	    if(checkAadharLength != 0 && checkAadharLength < 12){
			$("#aadharSpan").html(" (Only 12 Digits Are Allowed!) ")
			//FI.find("#aadhaar").val(FI.find("#aadhaar").val().substr(0,12))
			 if(FI.find("#aadhaar").val()==0){
					$("#aadharSpan").empty()
				}
		}
		else{
			$("#aadharSpan").empty()
		}
		
		var checkPanLength = panLength.match(/\S+/g)
	    if(checkPanLength != 0 && checkPanLength < 10){
			$("#panSpan").html(" (Only 10 Digits Are Allowed!) ")
			//FI.find("#panNo").val(FI.find("#panNo").val().substr(0,10))
			 if(FI.find("#panNo").val()==0){
					$("#panSpan").empty()
				}
		}
		else{
			$("#panSpan").empty()
		}
		
		var checkComLength = companyNameAddLength.match(/\S+/g)
	    if(checkComLength > 256){
	    	
			$("#comMstAddSpan").html("(Max 256 characters are allowed)")
			FI.find("#companyNameAdd").val(FI.find("#companyNameAdd").val().substr(0,256))
		}
		else{
			$("#comMstAddSpan").empty()
		}
		
		var checkRemLength = remarksComAddLength.match(/\S+/g)
	    if(checkRemLength > 256){
	    	
			$("#remarkComAddSpan").html("(Max 256 characters are allowed)")
			FI.find("#remarksComAdd").val(FI.find("#remarksComAdd").val().substr(0,256))
		}
		else{
			$("#remarkComAddSpan").empty()
		}
		
		var checkPosLength = positionNameAddLength.match(/\S+/g)
	    if(checkPosLength > 256){
	    	
			$("#posMstAddSpan").html("(Max 256 characters are allowed)")
			FI.find("#positionNameAdd").val(FI.find("#positionNameAdd").val().substr(0,256))
		}
		else{
			$("#posMstAddSpan").empty()
		}
		
		var checkRemarkLength = remarksPosAddLength.match(/\S+/g)
	    if(checkRemarkLength > 256){
	    	
			$("#remarkPosAddSpan").html("(Max 256 characters are allowed)")
			FI.find("#remarksPosAdd").val(FI.find("#remarksPosAdd").val().substr(0,256))
		}
		else{
			$("#remarkPosAddSpan").empty()
		}
		
		var checkeduAddLength = eduLevelAddLength.match(/\S+/g)
	    if(checkeduAddLength > 256){
	    	
			$("#educationMstAddSpan").html("(Max 256 characters are allowed)")
			FI.find("#eduLevelAdd").val(FI.find("#eduLevelAdd").val().substr(0,256))
		}
		else{
			$("#educationMstAddSpan").empty()
		}
		
		var checkRemEduLength = remarksEduAddLength.match(/\S+/g)
	    if(checkRemEduLength > 256){
	    	
			$("#remarkEduAddSpan").html("(Max 256 characters are allowed)")
			FI.find("#remarksEduAdd").val(FI.find("#remarksEduAdd").val().substr(0,256))
		}
		else{
			$("#remarkEduAddSpan").empty()
		}
		
		var checkRemCourLength = courseNameAddLength.match(/\S+/g)
	    if(checkRemCourLength > 256){
	    	
			$("#courseMstAddSpan").html("(Max 256 characters are allowed)")
			FI.find("#courseNameAdd").val(FI.find("#courseNameAdd").val().substr(0,256))
		}
		else{
			$("#courseMstAddSpan").empty()
		}
		
		var checkRemCouLength = remarksCourseAddLength.match(/\S+/g)
	    if(checkRemCouLength > 256){
	    	
			$("#remarkCourseAddSpan").html("(Max 256 characters are allowed)")
			FI.find("#remarksCourseAdd").val(FI.find("#remarksCourseAdd").val().substr(0,256))
		}
		else{
			$("#remarkCourseAddSpan").empty()
		}
		
		var checkInsLength = insNameAddLength.match(/\S+/g)
	    if(checkInsLength > 256){
	    	
			$("#instituteMstAddSpan").html("(Max 256 characters are allowed)")
			FI.find("#insNameAdd").val(FI.find("#insNameAdd").val().substr(0,256))
		}
		else{
			$("#instituteMstAddSpan").empty()
		}
		
		var checkRemarkInsLength = remarksInstituteAddLength.match(/\S+/g)
	    if(checkRemarkInsLength > 256){
	    	
			$("#remarkInsAddSpan").html("(Max 256 characters are allowed)")
			FI.find("#remarksInstituteAdd").val(FI.find("#remarksInstituteAdd").val().substr(0,256))
		}
		else{
			$("#remarkInsAddSpan").empty()
		}
	
	
		
		var checkskDomainaddLength = skillDomainNameAdd1Length.match(/\S+/g)
	    if(checkskDomainaddLength > 256){
	    	
			$("#domainMstAddSpan1").html("(Max 256 characters are allowed)")
			FI.find("#skillDomainNameAdd1").val(FI.find("#skillDomainNameAdd1").val().substr(0,256))
		}
		else{
			$("#domainMstAddSpan1").empty()
		}
		
		var checkRemDomainAddLength = remarksDomainAdd1Length.match(/\S+/g)
	    if(checkRemDomainAddLength > 256){
	    	
			$("#remarkDomAddSpan1").html("(Max 256 characters are allowed)")
			FI.find("#remarksDomainAdd1").val(FI.find("#remarksDomainAdd1").val().substr(0,256))
		}
		else{
			$("#remarkDomAddSpan1").empty()
		}
		
		var checkSkMstLength = skillNameAddMLength.match(/\S+/g)
	    if(checkSkMstLength > 256){
	    	
			$("#skillMstAddSpanM").html("(Max 256 characters are allowed)")
			FI.find("#skillNameAddM").val(FI.find("#skillNameAddM").val().substr(0,256))
		}
		else{
			$("#skillMstAddSpanM").empty()
		}
		
		var checkRemarkSkAddLength = remarksSkillAddMLength.match(/\S+/g)
	    if(checkRemarkSkAddLength > 256){
	    	
			$("#remarkSkillAddSpanM").html("(Max 256 characters are allowed)")
			FI.find("#remarksSkillAddM").val(FI.find("#remarksSkillAddM").val().substr(0,256))
		}
		else{
			$("#remarkSkillAddSpanM").empty()
		}
		
		
   }  
   
   $.fn.showImage = function() {
   		var FI = $(this).closest('form')
		FI.find(".hiddenWholeForm").val("")
	    if ($(this)[0].files && $(this)[0].files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            FI.find('#imageDisp').attr('src', e.target.result);
	        }

	        reader.readAsDataURL($(this)[0].files[0]);
	    }
	}   
   
   /*/Start Code ||  Skill Name Dependent on Skill Domain Name/*/
   
$.fn.onJobDomainChange = function(){
		var FI = $(this).closest('form');
		domainArr = [];
		var domainSkillArr = []
		var sdmId = FI.find("#skillDomainName1").val();
		if(sdmId.length <= 0){
			//FI.find("#skillName1").val("").select2();
			var blankArr =[]
			$(this).populateAdvanceSelect(FI.find('#skillName1'),blankArr,{"key":"smId","value":"skillName"});
		}
		
		
		var skillDomain = FI.find("#skillName1").val();
		for(var d=0;d<skillDomain.length;d++){
			domainSkillArr.push(skillDomain[d])
		}
		
		
		if(sdmId != ""){
			$(this).cAjax('GET','./job/getAllskillView', "", false, false, '', 'Error', true);
		    skillview =  $.parseJSON(strResponseText)
		 	for(var i in  sdmId){
		 		var domainId = sdmId[i]
			 	for(var j in skillview){
					var skillName = skillview[j].sdmId	
			 		if(domainId == skillName){
					    var Name = skillview[j]
					    domainArr.push(Name)
					 }
			 	}	
		 		
			 }
			 $(this).populateMultipleSelect(FI.find("#skillName1"),domainArr,{"key":"smId","value":"skillName"});
			 FI.find("#skillName1").val(domainSkillArr)
		 	

  }
}



$.fn.addQualDtlRow = function(){
	var selhtml1 = `<select class="form-control form-control-sm select2" id="education" name="applicanteducation[0][emId]">
		<option value="" selected="selected">-- Select --</option>		
		</select>`
	var selhtml2 = `<select class="form-control form-control-sm select2" id="courseNameQ" name="applicanteducation[0][coId]">
		<option value="" selected="selected">-- Select --</option>
		</select>`
		
	var $FI = $(this).closest('form');
	
	var qualDtlBody = $("#qualDtlBody tr");
	for(var i = 0; i < qualDtlBody.length; i++){

	var education =  qualDtlBody.eq(i).find("#education").val();
	if(education == ""||education == null){
		swal("Please fill all fields")
		return false;
	}
	
//	var courseNameQ =  qualDtlBody.eq(i).find("#courseNameQ").val();
//	if(courseNameQ == ""||courseNameQ == null){
//		swal("Please fill all fields")
//		return false;
//	}
	
	var institute =  qualDtlBody.eq(i).find("#institute").val();
	if(institute == ""||institute == null){
		swal("Please fill all fields")
		return false;
	}
	
	var passingYear =  $.trim(qualDtlBody.eq(i).find("#passingYear").val());
	if(passingYear == ""||passingYear == null){
		swal("Please fill all fields")
		return false;
	}
	
	var marks =  $.trim(qualDtlBody.eq(i).find("#marks").val());
	if(marks == ""||marks == null){
		swal("Please fill all fields")
		return false;
	}
	};
	
	var currentRow = $(this).closest('.qualDtlRow')
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined;
	
	var bodyToValidate = $(this).closest('#qualDtlBody');
	var bodyRowLen = bodyToValidate.find('tr').length;
	
		
		var clonedRow = $(this).closest('.qualDtlRow').clone(true);
		
		var selhtml1 = `<select class="form-control form-control-sm select2" id="education" name="applicanteducation[0][emId]">
								<option value="" selected="selected">-- Select --</option>		
						</select>`
		var selhtml2 = `<select class="form-control form-control-sm select2" id="courseNameQ" name="applicanteducation[0][coId]">
								<option value="" selected="selected">-- Select --</option>
							
						</select>`
		var selhtml3 = `<select class="form-control form-control-sm select2" id="institute" name="applicanteducation[0][imId]">
								<option value="" selected="selected">-- Select --</option>
								
						</select>`
		
		var selhtml4 = `<input class="form-control form-control-sm" type="hidden"  value="Y" id="isactive" name="applicantexperience[0][isactive]">`	
			
		clonedRow.find("td").eq(0).html(selhtml1);
		clonedRow.find("#education").select2();
		clonedRow.populateAdvanceSelect(clonedRow.find("#education"),EduJson,{"key":"emId","value":"eduLevel"});
		
		clonedRow.find("td").eq(1).html(selhtml2);
		clonedRow.find("#courseNameQ").select2();
		clonedRow.populateAdvanceSelect(clonedRow.find("#courseNameQ"),courseJson,{"key":"coId","value":"coursename"});
		
		clonedRow.find("td").eq(2).html(selhtml3);
		clonedRow.find("#institute").select2();
		clonedRow.populateAdvanceSelect(clonedRow.find("#institute"),instJson,{"key":"imId","value":"insName"});
		
		resetRow(clonedRow);
		clonedRow.find("#isactive").val("Y")
		
		$FI.find("#qualDtlBody").append(clonedRow);
	
	
}

$.fn.CheckEdu = function(){
	var FI = $(this).closest('form')
	var edu = FI.find("#qualDtlBody tr")
	var disableArr =[];
	for(var id=0; id<edu.length;id++){	
		var courseNameQId = edu.eq(id).find("#courseNameQ :selected").val();
		edu.find("#courseNameQ").find("option[value='" + courseNameQId + "']").attr("disabled",true);

	}	
}


$.fn.removeQualDtlRow = function(){
	var FI = $(this).closest('form');	
	if(FI.find("#qualDtlBody").find('tr').length > 1){
		$(this).closest('tr').remove();
	}
	else{
		swal("You cannot remove first row");
	}
}


$.fn.addcertDtlRow = function(){

	var $FI = $(this).closest('form');
	var certrow = $("#certDtlBody tr");
	for(var i = 0;i < certrow.length;i++){
		
		var crtnameBox = $.trim(certrow.eq(i).find('#crtname').val())
		if(crtnameBox == "" ||crtnameBox == undefined||crtnameBox == null){
			swal("Please fill all fields")
			return false;
		}
		
		var crtBy = $.trim(certrow.eq(i).find('#crtby').val())
		if(crtBy == "" ||crtBy == undefined||crtBy == null){
			swal("Please fill all fields")
			return false;
		}
		
		let yearofcmplBox = $.trim(certrow.eq(i).find('#yearofcmpl').val())
		if(yearofcmplBox == "" ||yearofcmplBox == undefined||yearofcmplBox == null){
			swal("Please fill all fields")
			return false;
		}
		
	}
	
	
	var currentRow = $(this).closest('.certDtlRow')
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined;
	
	var bodyToValidate = $(this).closest('#certDtlBody');
	var bodyRowLen = bodyToValidate.find('tr').length;
	
	var selhtml4 = `<input class="form-control form-control-sm" type="hidden"  value="Y" id="isactive" name="applicantexperience[0][isactive]">`	
	
	var clonedRow = $(this).closest('.certDtlRow').clone(true);
	
	resetRow(clonedRow);	
	clonedRow.find("#isactive").val("Y")
	$FI.find("#certDtlBody").append(clonedRow);

}

$.fn.removecertDtlRow = function(){
	var FI = $(this).closest('form');	
	if(FI.find("#certDtlBody").find('tr').length > 1){
		$(this).closest('tr').remove();
	}
	else{
		swal("You cannot remove first row");
	}
}


$.fn.addwrkexpDtlRow = function(){
	var selhtml = `<select class="form-control form-control-sm select2" id="company" name="applicantexperience[0][cmId]">
		<option value="" selected="selected">-- Select --</option>				
		</select>`
		
	var $FI = $(this).closest('form');
	
	var workExpBody = $("#wrkexpDtlBody tr");
	for(var i = 0; i < workExpBody.length; i++){

	var company =  workExpBody.eq(i).find("#company").val();
	if(company == ""||company == null){
		swal("Please fill all fields")
		return false;
	}
	
	var position =  workExpBody.eq(i).find("#position").val();
	if(position == ""||position == null){
		swal("Please fill all fields")
		return false;
	}
	
	var startdt =  workExpBody.eq(i).find("#startdt").val();
	if(startdt == ""||startdt == null){
		swal("Please fill all fields")
		return false;
	}
	
//	var enddt =  workExpBody.eq(i).find("#enddt").val();
//	if(enddt == ""||enddt == null){
//		swal("Please fill all fields")
//		return false;
//	}
	};
	
	var currentRow = $(this).closest('.wrkexpDtlRow')
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined;
	
	var bodyToValidate = $(this).closest('#wrkexpDtlBody');
	var bodyRowLen = bodyToValidate.find('tr').length;
	
		
	var clonedRow = $(this).closest('.wrkexpDtlRow').clone(true);
		
	var selhtml = `<select class="form-control form-control-sm select2" id="company" name="applicantexperience[0][cmId]">
					<option value="" selected="selected">-- Select --</option>				
					</select>`
	
	var selhtml1 = `<select class="form-control form-control-sm select2" id="position" name="applicantexperience[0][pmId]">
					<option value="" selected="selected">-- Select --</option>			
					</select>`			
	
	var selhtml2 = `<input class="form-control form-control-sm" type="text" data-provide="datepicker" 
					name="applicantexperience[0][start_date]" id="startdt" autocomplete="off" readonly="readonly" onchange="$(this).disbaleEndDate();"/>`
		
	var selhtml3 = 	`<input class="form-control form-control-sm" type="text" data-provide="datepicker" 
					name="applicantexperience[0][end_date]" id="enddt" autocomplete="off" readonly="readonly"/>`
	
	var selhtml4 = `<input class="form-control form-control-sm" type="hidden"  value="Y" id="isactive" name="applicantexperience[0][isactive]">`	
		
	clonedRow.find("td").eq(0).html(selhtml);
	clonedRow.find("#company").select2();
	clonedRow.populateAdvanceSelect(clonedRow.find("#company"),companyMstJson,{"key":"cmId","value":"companyName"});
	
	clonedRow.find("td").eq(1).html(selhtml1);
	clonedRow.find("#position").select2();
	clonedRow.populateAdvanceSelect(clonedRow.find("#position"),positionMstJson,{"key":"pmId","value":"positionName"});	
		
	clonedRow.find("td").eq(2).html(selhtml2);
	clonedRow.find("td").eq(3).html(selhtml3);

	resetRow(clonedRow);
	clonedRow.find("#isactive").val("Y")
	clonedRow.find("#enddt").attr("disabled","disabled");
	clonedRow.find("#startdt").datepicker({Format: 'dd/mm/yyyy',autoclose:true,});
	clonedRow.find("#enddt").datepicker({Format: 'dd/mm/yyyy',autoclose:true,});

	$FI.find("#wrkexpDtlBody").append(clonedRow);

}

$.fn.removeIFile = function(id) {
	var FI = $(this).closest('form')
    $(this).parent().remove();
    FI.find("#file-input").val("")
    var fileName=$(id).attr("fileName");
    //var folderName=$(id).attr("folderName");
    //var irName=$(id).attr("drName");
    inames.splice($.inArray(fileName, inames),1);
    var am_id = FI.find("#amId").val();
    if(am_id != null){
    	 $(this).cAjax('Post','./fileUpload/deleteResume',"amid="+am_id, false, false, '', 'Error', true);
    		if(strResponseText){
    			swal("Deleted!", "Resume deleted successfully.", "success");
    			}
    }
   
};

$.fn.addCompanyN = function(){
	var FI = $("#addcompanymodalDiv")
	
			$('#comMstAddSpan').html('');
			$('#remarkComAddSpan').html('');
	FI.find('#addcompanyMstModel').resetMstDataForm();
	FI.find("#addcompanyMstModel").modal('show');
}


$.fn.saveAddCompMst = function(){	
	event.preventDefault();
	var company = $FI.find("#company").val()
	var FI= $(this).parents('.modal-content').find('form'); 
	if(!FI.ValidateForm()) {
		return false;
	}
	var operationObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	operationObj.isActive = isActive;
	var companyJson = JSON.stringify(operationObj);
	$(this).cAjaxPostJson('Post','./master/saveCompanyMst',companyJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Company Name!", "", "warning");  
	} else if(strResponseText) { 
		var companyReturn = $.parseJSON(strResponseText);
		if(companyReturn.cmId != null && companyReturn.cmId > "0"){
			swal("Saved!", "Company Name saved successfully.", "success");
			$('#addcompanyMstModel').modal('hide');
		//	$(this).reloadGrid("clientMst", clientMstlistdispid, clientMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')
		 	
		 	 $(this).cAjax('GET','./master/getCompanyMstVw', '', false, false, '', 'Error', true);
			 companyMstJson =  $.parseJSON(strResponseText)
			 $(this).populateAdvanceSelect($FI.find("#company"),companyMstJson,{"key":"cmId","value":"companyName"});
			 $FI.find("#company").val(company)
	
		 
		}
	}
	return false;
};


$.fn.addPositionN = function(){
	var FI = $("#addPositionmodalDiv")
	
			$('#posMstAddSpan').html('');
			$('#remarkPosAddSpan').html('');
	FI.find('#addPositionMstModel').resetMstDataForm();
	FI.find("#addPositionMstModel").modal('show');
}


$.fn.saveAddPosMst = function(){	
	event.preventDefault();
	var position = $FI.find("#position").val()
	var FI= $(this).parents('.modal-content').find('form'); 
	if(!FI.ValidateForm()) {
		return false;
	}
	var operationPosObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	operationPosObj.isActive = isActive;
	var positionJson = JSON.stringify(operationPosObj);
	$(this).cAjaxPostJson('Post','./master/savePositionMst',positionJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Position Name!", "", "warning");  
	} else if(strResponseText) { 
		var positionReturn = $.parseJSON(strResponseText);
		if(positionReturn.pmId != null && positionReturn.pmId > "0"){
			swal("Saved!", "Position Name saved successfully.", "success");
			$('#addPositionMstModel').modal('hide');
		//	$(this).reloadGrid("clientMst", clientMstlistdispid, clientMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')
		 	
			 $(this).cAjax('GET','./master/getPositionMstVw', '', false, false, '', 'Error', true);
			 positionMstJson =  $.parseJSON(strResponseText)
			 $(this).populateAdvanceSelect($FI.find("#position"),positionMstJson,{"key":"pmId","value":"positionName"});
			 $FI.find("#position").val(position)
		//	FI.find("#spocNamePer").val('#spocIdPer');
		 
		}
	}
	return false;
};

$.fn.addEducationalN = function(){
	var FI = $("#addEducationmodalDiv")
	
			$('#educationMstAddSpan').html('');
			$('#remarkEduAddSpan').html('');
	FI.find('#addEducationMstModel').resetMstDataForm();
	FI.find("#addEducationMstModel").modal('show');
}


$.fn.saveAddEducationMst = function(){	
	event.preventDefault();
	var education = $FI.find("#education").val()
	var FI= $(this).parents('.modal-content').find('form'); 
	if(!FI.ValidateForm()) {
		return false;
	}
	var operationEduObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	operationEduObj.isActive = isActive;
	var educationJson = JSON.stringify(operationEduObj);
	$(this).cAjaxPostJson('Post','./job/saveEduMst',educationJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Education Name!", "", "warning");  
	} else if(strResponseText) { 
		var educationReturn = $.parseJSON(strResponseText);
		if(educationReturn.emId != null && educationReturn.emId > "0"){
			swal("Saved!", "Education Name saved successfully.", "success");
			$('#addEducationMstModel').modal('hide');
		//	$(this).reloadGrid("clientMst", clientMstlistdispid, clientMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')
		 	
		 	 $(this).cAjax('GET','./job/getAllEduMst', '', false, false, '', 'Error', true);
			 EduJson =  $.parseJSON(strResponseText)
			 $(this).populateAdvanceSelect($FI.find("#education"),EduJson,{"key":"emId","value":"eduLevel"});
			 $FI.find("#education").val(education)
		
		 
		}
	}
	return false;
};


$.fn.addCourseN = function(){
	var FI = $("#addCoursemodalDiv")
	
			$('#courseMstAddSpan').html('');
			$('#remarkCourseAddSpan').html('');
	FI.find('#addCourseMstModel').resetMstDataForm();
	FI.find("#addCourseMstModel").modal('show');
}


$.fn.saveAddCourseMst = function(){	
	event.preventDefault();
	var courseNameQ = $FI.find("#courseNameQ").val()
	var FI= $(this).parents('.modal-content').find('form'); 
	if(!FI.ValidateForm()) {
		return false;
	}
	var operationCourseObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	operationCourseObj.isActive = isActive;
	var courseAddJson = JSON.stringify(operationCourseObj);
	$(this).cAjaxPostJson('Post','./master/saveCourseMst',courseAddJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Course Name!", "", "warning");  
	} else if(strResponseText) { 
		var courseReturn = $.parseJSON(strResponseText);
		if(courseReturn.coId != null && courseReturn.coId > "0"){
			swal("Saved!", "Course Name saved successfully.", "success");
			$('#addCourseMstModel').modal('hide');
		//	$(this).reloadGrid("clientMst", clientMstlistdispid, clientMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')
		 	
		 	 $(this).cAjax('GET','./job/getAllCourseMstVw', '', false, false, '', 'Error', true);
			 courseJson =  $.parseJSON(strResponseText)
			 $(this).populateAdvanceSelect($FI.find("#courseNameQ"),courseJson,{"key":"coId","value":"coursename"});
			 $FI.find("#courseNameQ").val(courseNameQ)
	
		 
		}
	}
	return false;
};


$.fn.addInstituteN = function(){
	var FI = $("#addInstitutemodalDiv")
	
			$('#instituteMstAddSpan').html('');
			$('#remarkInsAddSpan').html('');
	FI.find('#addInstituteMstModel').resetMstDataForm();
	FI.find("#addInstituteMstModel").modal('show');
}


$.fn.saveAddInstituteMst = function(){	
	event.preventDefault();
	var institute = $FI.find("#institute").val()
	var FI= $(this).parents('.modal-content').find('form'); 
	if(!FI.ValidateForm()) {
		return false;
	}
	var operationInsObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	operationInsObj.isActive = isActive;
	var instituteAddJson = JSON.stringify(operationInsObj);
	$(this).cAjaxPostJson('Post','./master/saveInstituteMst',instituteAddJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Institute Name!", "", "warning");  
	} else if(strResponseText) { 
		var instituteReturn = $.parseJSON(strResponseText);
		if(instituteReturn.imId != null && instituteReturn.imId > "0"){
			swal("Saved!", "Institute Name saved successfully.", "success");
			$('#addInstituteMstModel').modal('hide');
		 	$(".ag-center-cols-container").css('width','100%')
		 	
		 	 $(this).cAjax('GET','./job/getAllinstMstVw', '', false, false, '', 'Error', true);
			 instJson =  $.parseJSON(strResponseText)
			 $(this).populateAdvanceSelect($FI.find("#institute"),instJson,{"key":"imId","value":"insName"});
			 $FI.find("#institute").val(institute)
		}
	}
	return false;
};



$.fn.addDomainN1 = function(){
	var FI = $("#addDomainmodalDiv1")
	
			$('#domainMstAddSpan1').html('');
			$('#remarkDomAddSpan1').html('');
	FI.find('#addDomainMstModel1').resetMstDataForm();
	FI.find("#addDomainMstModel1").modal('show');
}


$.fn.saveAddDomainMst1 = function(){	
	event.preventDefault();
	var skillDomainName1 = $FI.find("#skillDomainName1").val()
	var FI= $(this).parents('.modal-content').find('form'); 
	if(!FI.ValidateForm()) {
		return false;
	}
	var operationDomObj1 = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	operationDomObj1.isActive = isActive;
	var domainAddJson1 = JSON.stringify(operationDomObj1);
	$(this).cAjaxPostJson('Post','./job/saveSkillDomainMst',domainAddJson1, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Skill Domain Name!", "", "warning");  
	} else if(strResponseText) { 
		var domainReturn1 = $.parseJSON(strResponseText);
		if(domainReturn1.sdmId != null && domainReturn1.sdmId > "0"){
			swal("Saved!", "Skill Domain Name saved successfully.", "success");
			$('#addDomainMstModel1').modal('hide');
		 	$(".ag-center-cols-container").css('width','100%')
	  
		    $(this).cAjax('GET','./job/getAllSkillDoaminName', '', false, false, '', 'Error', true);
		    skilldomname =  $.parseJSON(strResponseText)
		    $(this).populateMultipleSelect($FI.find("#skillDomainName1"),skilldomname,{"key":"sdmId","value":"skillDomainName"});
		    $FI.find("#skillDomainName1").val(skillDomainName1)
		}
	}
	return false;
};
	

$.fn.addSkillN1 = function(){
	var FI = $("#addSkillmodalDivM")
	
			$('#skillMstAddSpanM').html('');
			$('#remarkSkillAddSpanM').html('');
	FI.find('#addSkillMstModelM').resetMstDataForm();
	

    $(this).cAjax('GET','./job/getAllSkillDoaminName', '', false, false, '', 'Error', true);
    skilldomname =  $.parseJSON(strResponseText)
    $(this).populateAdvanceSelect(FI.find("#sdmIdSkM"),skilldomname,{"key":"sdmId","value":"skillDomainName"});
	
	FI.find("#addSkillMstModelM").modal('show');
}


$.fn.saveAddSkillMstM = function(){	
	event.preventDefault();
	var skillName1 = $FI.find("#skillName1").val()
	var FI= $(this).parents('.modal-content').find('form'); 
	if(!FI.ValidateForm()) {
		return false;
	}
	var operationSkillMObj = $.parseJSON(serializeJSONIncludingDisabledFields(FI));
	var isActive = $("#isActive").val();
	var sdmId= $("#sdmIdSkM").val();
	operationSkillMObj.isActive = isActive;
	operationSkillMObj.sdmId = sdmId;
	var skillAddMJson = JSON.stringify(operationSkillMObj);
	$(this).cAjaxPostJson('Post','./job/saveSkillMst',skillAddMJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Skill Name!", "", "warning");  
	} else if(strResponseText) { 
		var skillMReturn = $.parseJSON(strResponseText);
		if(skillMReturn.smId != null && skillMReturn.smId > "0"){
			swal("Saved!", "Skill Name saved successfully.", "success");
			$('#addSkillMstModelM').modal('hide');
		 	$(".ag-center-cols-container").css('width','100%')
	  
		    $(this).cAjax('GET','./job/getAllskillView', '', false, false, '', 'Error', true);
		 	domainArr =  $.parseJSON(strResponseText)
		    $(this).populateAdvanceSelect($FI.find("#skillName1"),domainArr,{"key":"smId","value":"skillName"});
		 	$FI.find("#skillName1").val(skillName1)
		}
	}
	return false;
};


$.fn.onChangeResumeFile= function() {
	var FI = $(this).closest('form');
	
	/*var fileName = FI.find("#file-input")
	var duplicateFiles = fileName.get(0).files[0].name
	var fileList = FI.find("#ihorizontal-list li")
	for(var j=0;j<fileList.length;j++){
		if(duplicateFiles == fileList.eq(j).find('a').eq(0).text()){
        	swal("You cannot upload same file again")
        	return false;
        }
	}*/
	
	$("input[name=file]");
	    for (var i = 0; i < $(this).get(0).files.length; ++i) {
	        inames.push($(this).get(0).files[i])
	        
			FI.find("#ihorizontal-list").append("<li  id='fileRemove'><a href='#'>"+$(this).get(0).files[i].name+"</a><a href='javascript:void(0);' onclick='$(this).removeIFile(this);' class='remove' fileName='"+$(this).get(0).files[i].name+"' ><b> &times; </a></li>");

	    }
};

$.fn.download=function(id){
	var FI = $(this).closest('form');
	FI.find("#ihorizontal-list li").on("click", "a.download" , function(e) {
	    e.preventDefault(); 
	    var href=$(this).attr("href");
		  if(href){
			window.open(href, '_blank'); 
		  }
		  //stop the browser from following
	});
}

/*function ValidateSize(file) {
	// alert('Hi');
    var FileSize = file.files[0].size / 1024 / 1024; // in MiB
    if (FileSize > 2) {
    	swal('File size exceeds 2 MB');
        $(file).val(''); //for clearing with Jquery
    } else {

    }
}*/


$.fn.removewrkexpDtlRow = function(){
	var FI = $(this).closest('form');	
	if(FI.find("#wrkexpDtlBody").find('tr').length > 1){
		$(this).closest('tr').remove();
	}
	else{
		swal("You cannot remove first row");
	}
}


/*This function is used to enable offr in hand box*/ 
$.fn.enable = function(){
	var FI = $(this).closest('form');	
	var offrVal = FI.find('input[name="offerInHand"]:checked').val();
	if(offrVal == "Y"){
		FI.find("#offerInHandCtc").attr("disabled",false)
	}
	else{
		FI.find("#offerInHandCtc").attr("disabled",true)
		FI.find("#offerInHandCtc").val("");
	}
}



/*This function is used to disbale previous date in end date column*/
$.fn.disbaleEndDate = function(){
	var start = $(this).closest(".wrkexpDtlRow").find("#startdt").val();
	start = start.substr(3, 2)+"/"+start.substr(0, 2)+ "/"+start.substr(6, 4);
	$(this).closest(".wrkexpDtlRow").find('#enddt').removeAttr('disabled');
	var endDate = `<input class="form-control form-control-sm" type="text" data-provide="datepicker" name="applicantexperience[0][end_date]" id="enddt" autocomplete="off"/>`
	$(this).closest(".wrkexpDtlRow").find('.endDateBox').html(endDate)
	
	$(this).closest(".wrkexpDtlRow").find('#enddt').datepicker({
        dateFormat: 'dd/mm/yyyy',
        autoclose:true,
        startDate: new Date(start)
});
	
}