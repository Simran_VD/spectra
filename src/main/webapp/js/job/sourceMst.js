var sourceMstlistdispid=10;
var sourceMstListGridOptions;	

$.fn.getsourcelist = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("sourceMstList",'./pages?name=job/sourceMstList',"Source",'child',currDash);

}
$.fn.sourceMstList = function(){
	
	sourceMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("sourceMst", sourceMstlistdispid, sourceMstListGridOptions);

	$("#sourceMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(sourceMstListGridOptions)[0].som_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#sourceMstSpan').html('');
			$('#remarksSouSpan').html('');
			$('#sourceMstModel').resetMstDataForm();
			$('#sourceMstModel').modal('show');
			$('#sourceMstModel').find("#statusSouAct").val("Y").prop("checked", "true");
			$('#sourceMstModel').find("#statusSouInAct").val("N");
			$('#sourceMstModel').fillSourceMstForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./job/getSourceMst',"somId="+data, false, false, '', 'Error', true);
			var sourceJson = $.parseJSON(strResponseText);
			$('#sourceMstSpan').html('');
			$('#remarksSouSpan').html('');
			$('#sourceMstModel').modal('show');
			$('#sourceMstModel').fillSourceMstForm(sourceJson,operation);
			break;

//		case "delete":
//			swal({
//				title: "Are you sure want  to delete Skill Group ?",
//				text: "You will not be able to recover Skill Group now!",
//				type: "warning",
//				showCancelButton: true,
//				confirmButtonClass: "btn-danger",
//				confirmButtonText: "Yes, delete it!",
//				cancelButtonText: "No cancel please!",
//				confirmButtonColor:'#3095d6',
//				cancelmButtonColor:'#d33',
//				closeOnConfirm: false	,
//				closeOnCancel: false,
//				showConfirmButton: true,
//			},
//			function(isConfirm) {
//				if (isConfirm) {
//			$(this).cAjax('POST','./job/deleteSkillGroupMst',"sgmId="+data, false, false, '', 'Error', true);
//			if(strResponseText == "SUCCESS"){
//				swal("Deleted!", "Selected record deleted successfully.", "success");
//				$(this).reloadGrid("skillGroupMst", skillGroupMstlistdispid, skillGroupMstListGridOptions);
//			}else if(strResponseText == "DATA_INTEGRITY"){
//				swal("Error in deletion!", "Selected Skill Group is being used. So it cannot be deleted.", "error");
//			}
//				}
//				else {
//					swal("Cancelled", "Thank you! your Skill Group  is safe :)", "error");
//				}});
//			break;
				

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#sourceMst").closest(".row").addClass("listContainer");
	$("#sourceMst").closest('form').attr("oninput","$(this).onsourceMstFilterTextBoxChanged()");

};

$.fn.onsourceMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	sourceMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillSourceMstForm = function(sourceJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteId").val(sourceJson.siteid);
	$(this).find("#somId").val(sourceJson.somId);
	$(this).find("#sourceName").val(sourceJson.sourceName);
	$(this).find("#remarksSou").val(sourceJson.remarks);
	$(this).find("input[name=isActive][value='"+sourceJson.isActive+"']").prop("checked", "true");
//	$(this).find("#isActive").val(skillCatJson.isActive);

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#sourceName").attr("disabled","disabled");
		$(this).find("#remarksSou").attr("disabled","disabled");
		$(this).find("#closeSourceMstBtn2").attr("disabled", false);
		$(this).find("#statusSouAct").attr("disabled","disabled");
		$(this).find("#statusSouInAct").attr("disabled","disabled");

	}else if(operation && operation == "edit" ) {
	//	$(this).find("#skillGroupName").attr("disabled", true);
		$(this).find("#statusSouInAct").attr("disabled",false);
	    $(this).find("#statusSouAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var sourceMstJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".sourceMsthiddenWholeForm").val(sourceMstJson)
};

$.fn.saveSourceMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
    if(navigator.onLine == false){
	    swal("Warning", "You're offline. Check your connection.", "warning");
	    return false;
      } 

	if(!FI.ValidateForm() || !$(this).validateSourceMst(FI)) {
		return false;
	}
	var sourceMstJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".sourceMsthiddenWholeForm").val() == (sourceMstJson)){
		swal("Saved!", "Source Name saved successfully.", "success");
		$('#sourceMstModel').modal('hide');
		$(this).reloadGrid("sourceMst", sourceMstlistdispid, sourceMstListGridOptions);
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./job/saveSourceMst',(sourceMstJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Source Name!", "", "warning");  
	} else if(strResponseText) { 
		var sourceMstReturn = $.parseJSON(strResponseText);
		if(sourceMstReturn.somId != null && sourceMstReturn.somId > "0"){
			swal("Saved!", "Source Name saved successfully.", "success");
			$('#sourceMstModel').modal('hide');
			$(this).reloadGrid("sourceMst", sourceMstlistdispid, sourceMstListGridOptions);
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateSourceMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var sourceName = FI.find('#sourceName').val();
	//var  taxtRate= FI.find('#taxrate').val();

	
	if(!(sourceName.length > 0) ) {
		swal({title: "Source Name Can not be blank!",  text: "Enter Value in Source Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
//
//	if(!$.isNumeric(taxtRate) || !(taxtRate >0 )) {
//		swal({title: "Tax Rate Error!",  text: "Tax Rate Should be Greater than 0", timer: 10000, showConfirmButton: true});
//		return false;	
//	}
//	
	return true;
};

$.fn.checkSourceMstLength = function(){
	var sourceNameLength = $("#sourceName").val().length.toString().trim()
	var remarksLength =$("#remarksSou").val().length.toString()
//	var skillNoteLength =$("#skillNote").val().length.toString()
//	var remarkLength =$("#remarks").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = sourceNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#sourceMstSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#sourceMstSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarksSouSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarksSouSpan").empty()
		}
		
		
//		var checkPhoneLength = phoneLength.match(/\S+/g)
//	    if(checkPhoneLength == 10){
//			$("#phoneSpan").html("(Max 10 Digit Number are allowed)")
//				
//		}
//		else{
//			$("#phoneSpan").empty()
//		}
   } 

$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};