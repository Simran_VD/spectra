var courseMstlistdispid=19;
var courseMstListGridOptions;	

$.fn.getcoursemst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("courseMstList",'./pages?name=job/courseMstList',"Course",'child',currDash);

}
$.fn.courseMstList = function(){
	
	courseMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("courseMst", courseMstlistdispid, courseMstListGridOptions);

	$("#courseMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(courseMstListGridOptions)[0].co_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#courseMstSpan').html('');
			$('#remarkCMstSpan').html('');
			$('#courseMstModel').resetMstDataForm();
			$('#courseMstModel').modal('show');
			$('#courseMstModel').find("#statusCMstAct").val("Y").prop("checked", "true");
			$('#courseMstModel').find("#statusCMstInAct").val("N");
			$('#courseMstModel').fillCourseMstForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./master/getCourseMst',"coId="+data, false, false, '', 'Error', true);
			var courseMstJson = $.parseJSON(strResponseText);
			$('#courseMstSpan').html('');
			$('#remarkCMstSpan').html('');
			$('#courseMstModel').modal('show');
			$('#courseMstModel').fillCourseMstForm(courseMstJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	$("#courseMst").closest(".row").addClass("listContainer");
	$("#courseMst").closest('form').attr("oninput","$(this).onCourseMstFilterTextBoxChanged()");

};

$.fn.onCourseMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	courseMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillCourseMstForm = function(courseMstJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(courseMstJson.siteid);
	$(this).find("#coId").val(courseMstJson.coId);
	$(this).find("#courseName").val(courseMstJson.courseName);
	$(this).find("#remarksCMst").val(courseMstJson.remarks);
	$(this).find("input[name=isActive][value='"+courseMstJson.isActive+"']").prop("checked", "true");

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeCourseMstBtn2").attr("disabled", false);
		$(this).find("#statusCMstAct").attr("disabled",true);
		$(this).find("#statusCMstInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
		$(this).find("#statusCMstInAct").attr("disabled",false);
	    $(this).find("#statusCMstAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var courseJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".coursehiddenWholeForm").val(courseJson)
};

$.fn.saveCourseMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	    }

	if(!FI.ValidateForm() || !$(this).validateCourseMst(FI)) {
		return false;
	}
	var courseJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".coursehiddenWholeForm").val() == (courseJson)){
		swal("Saved!", "Course Name saved successfully.", "success");
		$('#courseMstModel').modal('hide');
		$(this).reloadGrid("courseMst", courseMstlistdispid, courseMstListGridOptions);
		$("#courseMst").closest('form').trigger('oninput');
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./master/saveCourseMst',(courseJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Course Name!", "", "warning");  
	} else if(strResponseText) { 
		var courseReturn = $.parseJSON(strResponseText);
		if(courseReturn.coId != null && courseReturn.coId > "0"){
			swal("Saved!", "Course Name saved successfully.", "success");
			$('#courseMstModel').modal('hide');
			$(this).reloadGrid("courseMst", courseMstlistdispid, courseMstListGridOptions);
			$("#courseMst").closest('form').trigger('oninput');
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateCourseMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var courseName = FI.find('#courseName').val();

	
	if(!(courseName.length > 0) ) {
		swal({title: "Course Name Can not be blank!",  text: "Enter Value in Course Name", timer: 10000, showConfirmButton: true});
		return false;	
	}	
	return true;
};

$.fn.checkCourseMstLength = function(){
	var courseNameLength = $("#courseName").val().length.toString().trim()
	var remarksLength =$("#remarksCMst").val().length.toString()
	
	    var checkLength = courseNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#courseMstSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#courseMstSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarkCMstSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarkCMstSpan").empty()
		}
		
   } 


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};