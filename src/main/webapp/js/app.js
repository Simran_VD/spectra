var stompClient = null;
$.fn.connect =function() {
    var socket = new SockJS('./websocket-example');
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function (frame) {
		stompClient.subscribe('/queue/recruiter', function (greeting) {
			$(this).showGreeting(JSON.parse(greeting.body).content);
		});
	});
}


$.fn.showGreeting =function(message) {
    
	$(this).cAjax('GET','./admin/getUser', '', false, false, '', 'Error', true);
	lgusrId = strResponseText;
	var count = 0;
	const result =message.filter(nt=> nt.recUserId===lgusrId)
	var ntId = "";
    $.each(result, function(){
        if(!this.notifiedDt){
            ntId += this.ntId+",";
            if(this.ntType == "Service Lead"){
                if(this.leadType == "New Lead"){
                    if(this.recGrpId == 1){
                        $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`" bellDt = `+this.ntId+`>
                                                        <div class="row m-0">
                                                            <div class="col-10 p-0">
                                                                <pre style="margin:0px;">`+this.message+`</pre>
                                                            </div>                                                            
                                                        </div>
                                                    </li>`)
                    }
                    else{
                        $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`)" bellDt = `+this.ntId+`>
                                                        <div class="row m-0">
                                                            <div class="col-10 p-0">
                                                                <pre style="margin:0px;">`+this.message+`</pre>
                                                            </div>
                                                            <div class="col-2 p-0" style="position:inherit;">
                                                                <button onclick="$(this).acceptLead(`+this.slId+`)"  style="border: none;background: transparent;" type="button"><i class="fa fa-check-circle" style="font-size: 25px;color: #47de47;"></i></button>
                                                            </div>
                                                        </div>
                                                    </li>`)
                        var toaster = `<div class="toast" id="`+this.ntId+`" role="alert" aria-live="assertive" aria-atomic="true" data-delay="10000" style="width: 250px;margin: 0px 10px 10px 0px;opacity: 1;background-color: #45ff086b;">
                                            <div class="toast-body msg">
                                                <pre style="margin:0px;">`+this.message+`</pre>
                                            </div>
                                            <div style="margin: 0px 10px 10px 0px;display: flow-root;">
                                                <button onclick="$(this).acceptLead(`+this.slId+`)" class="toastBtn" type="button"><i class="fa fa-check-circle" style="font-size: 25px;color: #47de47;"></i></button>
                                            </div>
                                        </div>`;
                        $("#toastDiv").append(toaster);
                        if($("#toastDiv .toast").length > 4){
                            $("#toastDiv .toast:first").remove();
                        };
                        $('#'+this.ntId).toast('show');
                        $('#'+this.ntId).on('hidden.bs.toast', function () {
                            (this).remove();
                        });
                    }
                    
                }
                else{
                    $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`" bellDt = `+this.ntId+`>
                                                        <div class="row m-0">
                                                            <div class="col-10 p-0">
                                                                <pre style="margin:0px;">`+this.message+`</pre>
                                                            </div>                                                            
                                                        </div>
                                                    </li>`)
                }
                

            }
            else{
                $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`" bellDt = `+this.ntId+`>
                                                    <div class="row m-0">
                                                        <div class="row m-0">
                                                            <div class="col-10 p-0">
                                                                <pre style="margin:0px;">`+this.message+`</pre>
                                                            </div>                                                            
                                                        </div>
                                                    </div>
                                                </li>`)
            }
            count++;
    	}
    });
    if(count > 0){
        $(".bellNum").text(parseInt($(".bellNum").text())+count);
    }
    if(ntId != ""){
        $(this).cAjax('POST','./master/notifyAt', 'ids='+ntId, false, false, '', 'Error', true);
        notifyAtRes =  (strResponseText);
    }
    for(var i = 0; i <= ($("#notificationList li").length - 10); i++){
        $("#notificationList li").last().remove();
    }
}
$.fn.addNotifications = function(){
    
	$(this).cAjax('GET','./master/getAllNotification', '', false, false, '', 'Error', true);
    notifications =  $.parseJSON(strResponseText);
    
    $(this).cAjax('GET','./admin/getUser', '', false, false, '', 'Error', true);
	lgusrId = strResponseText;
	
	var result = notifications.filter(nt=> nt.recUserId===lgusrId)
    if(result.length == 0){
        return false;
    }
    else{
        $("#notNewNoti").css('display','none');
    }
    var count = 0;
    var ntId = "";
    $.each(result, function(){
    	if(!this.notifiedDt){
    		ntId += (this.ntId+",");
    	}
        if(this.seenDt){
            if(this.ntType == "Service Lead"){
                if(this.leadType == "New Lead"){
                    if(this.recGrpId == 1){
                        $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`');`+this.pageLink+`" style="background-color:white;">
                                                        <div class="row m-0">
                                                            <div class="col-10 p-0">
                                                                <pre style="margin:0px;">`+this.message+`</pre>
                                                            </div> 
                                                        </div>
                                                    </li>`);
                    }
                    else{
                        $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`');`+this.pageLink+`" style="background-color:white;">
                                                        <div class="row m-0">
                                                            <div class="col-10 p-0">
                                                                <pre style="margin:0px;">`+this.message+`</pre>
                                                            </div> 
                                                            <div class="col-2 p-0" style="position:inherit;">
                                                                <button onclick="$(this).acceptLead(`+this.slId+`);" style="border: none;background: transparent;" type="button"><i class="fa fa-check-circle" style="font-size: 21px;color: #47de47;"></i></button>
                                                            </div>
                                                        </div>
                                                    </li>`);
                    }
                }
                else{
                    $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`');`+this.pageLink+`" style="background-color:white;">
                                                        <div class="row m-0">
                                                            <div class="col-10 p-0">
                                                                <pre style="margin:0px;">`+this.message+`</pre>
                                                            </div> 
                                                        </div>
                                                    </li>`);
                }

            }
            else{
                $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`');`+this.pageLink+`" style="background-color:white;">
                                                    <div class="col-10 p-0">
                                                        <pre style="margin:0px;">`+this.message+`</pre>
                                                    </div> 
                                                </li>`); 
            }
        }
        else{
            if(!this.bellDt){
                count++;
                if(this.ntType == "Service Lead"){
                    if(this.leadType == "New Lead"){
                        if(this.recGrpId == 1){
                            $("#notificationList").append(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`" bellDt = `+this.ntId+`>
                                                            <div class="row m-0">
                                                                <div class="col-10 p-0">
                                                                    <pre style="margin:0px;">`+this.message+`</pre>
                                                                </div> 
                                                            </div>
                                                        </li>`)
                        }
                        else{
                            $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`" bellDt = `+this.ntId+`>
                                                            <div class="row m-0">
                                                                <div class="col-10 p-0">
                                                                    <pre style="margin:0px;">`+this.message+`</pre>
                                                                </div> 
                                                                <div class="col-2 p-0" style="position:inherit;">
                                                                    <button onclick="$(this).acceptLead(`+this.slId+`);" style="border: none;background: transparent;" type="button"><i class="fa fa-check-circle" style="font-size: 21px;color: #47de47;"></i></button>
                                                                </div>
                                                            </div>
                                                        </li>`)
                        }
                    }
                    else{
                        $("#notificationList").append(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`" bellDt = `+this.ntId+`>
                                                            <div class="row m-0">
                                                                <div class="col-10 p-0">
                                                                    <pre style="margin:0px;">`+this.message+`</pre>
                                                                </div> 
                                                            </div>
                                                        </li>`)
                    }
                }
                else{
                    $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`" bellDt = `+this.ntId+`>
                                                        <div class="col-10 p-0">
                                                            <pre style="margin:0px;">`+this.message+`</pre>
                                                        </div> 
                                                    </li>`)
                }
            }
            else{
                if(this.ntType == "Service Lead"){
                    if(this.leadType == "New Lead"){
                        if(this.recGrpId == 1){
                            $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`">
                                                            <div class="row m-0">
                                                                <div class="col-10 p-0">
                                                                    <pre style="margin:0px;">`+this.message+`</pre>
                                                                </div> 
                                                            </div>
                                                        </li>`)
                        }
                        else{
                            $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`">
                                                            <div class="row m-0">
                                                                <div class="col-10 p-0">
                                                                    <pre style="margin:0px;">`+this.message+`</pre>
                                                                </div> 
                                                                <div class="col-2 p-0" style="position:inherit;">
                                                                    <button onclick="$(this).acceptLead(`+this.slId+`)" style="border: none;background: transparent;" type="button"><i class="fa fa-check-circle" style="font-size: 21px;color: #47de47;"></i></button>
                                                                </div>
                                                            </div>
                                                        </li>`)
                        }
                    }
                    else{
                        $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`">
                                                            <div class="row m-0">
                                                                <div class="col-10 p-0">
                                                                    <pre style="margin:0px;">`+this.message+`</pre>
                                                                </div> 
                                                            </div>
                                                        </li>`)
                    }
                }
                else{
                    $("#notificationList").prepend(`<li onclick="$(this).openPage('`+this.pageLink+`',`+this.ntId+`);`+this.pageLink+`">
                                                        <div class="col-10 p-0">
                                                            <pre style="margin:0px;">`+this.message+`</pre>
                                                        </div> 
                                                    </li>`)
                }
            }
        }
    });
    
    if(ntId != ""){
        $(this).cAjax('POST','./master/notifyAt', 'ids='+ntId, false, false, '', 'Error', true);
        notifyAtRes =  (strResponseText);
    }
    $(".bellNum").text(count);
    for(var i = 0; i <= ($("#notificationList li").length - 10); i++){
        $("#notificationList li").last().remove();
    }
    $(this).showGreeting(notifications);
}

$.fn.openPage = function(pageLink, ntId){

    if(ntId){
        if($(this).css('background-color') != "white"){
            $(this).cAjax('POST','./master/seenAt', 'ids='+ntId, false, false, '', 'Error', true);
            if(strResponseText == 1){
                $(this).css('background-color','white');
            }
        }
    }
}
$.fn.acceptLead = function(slId){
    $(this).cAjax('POST','./customer/updatePartnerInServlead', 'slId='+slId+"&loginId=", false, false, '', 'Error', true);
    acceptLeadRes =  $.parseJSON(strResponseText);
    if(acceptLeadRes.success == 1){
		$(this).parent().remove();
    	swal("Congratulations!", "You have been assigned a new lead.", "success");
    }
    else if(acceptLeadRes.success == 0){
        swal("Sorry!", "You don't have enough balance to accept this lead.", "warning");
    }
    else if(acceptLeadRes.success == -1){
        swal("Sorry!", "Please complete your pending leads.", "warning");
    }
    // Not Enough Amount
    else{
    	swal("Sorry!", "Lead has been assingned already to another partner.", "error");
    }
    
    $(this).getBaseAmt();
}