whereClause = '';
rptDatatable = '';
var reportGridOptions;
var reportListId = 29;
/**
 * Before Landing page for Reports.
 */
$.fn.getreport = function(rptType){
	/** View as 'reportList.html' will call */
	whereClause = "rpt_type='" + rptType + "'";
	rptDatatable = 'reportDatatable' + rptType;
	$(this).addTab("RptListing"+rptType, './pages?name=reports/reportList', rptType+" Reports");
};

/**
 * After Landing page for Reports.
 */
$.fn.reportListOnReady = function($FI){
	var FI = $FI
	$FI = $('#'+$FI);
	$(this).cAjax('POST','./master/getLoggedInSiteid', '', false, false, '', 'Error', true);
	var siteid =  (strResponseText);

	$(this).cAjax('POST','./admin/getUserName', '', false, false, '', 'Error', true);
	var user = $.parseJSON(strResponseText);
	
	
	reportGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',

		};

	$(this).cAjax('GET','./List?id=29', '', false, false, '', 'Error', true);
	var colModel= $.parseJSON(strResponseText);

	$(this).cAjax('GET','./FetchData?id=29&whereClause=' + whereClause + '&page=0&size=20', '', false, false, '', 'Error', true);
	var data =  $.parseJSON(strResponseText);
	colModel.displistcols = colModel.displistcols.sort((a, b) => {
	    return a.columnid - b.columnid;
	});
	var hiddenCol = "";
	var columnDefs = [];
	var columsnName = [];
	var Reports = function(params) {
	    return 	'<button onClick="$(this).printReport(\'' + params.rpt_url + '&__format=html&rp_userid='+user.userid+'\')" type="button" class="btn btn-sm btn-primary">HTML</button> ' +
				'<button onClick="$(this).printReport(\'' + params.rpt_url + '&__format=pdf&rp_userid='+user.userid+'\')" type="button" class="btn btn-sm btn-primary">PDF</button> ' +
				'<button onClick="$(this).printReport(\'' + params.rpt_url + '&__format=xls&rp_userid='+user.userid+'\')" type="button" class="btn btn-sm btn-primary">XLS</button> ' +
				'<button onClick="$(this).printReport(\'' + params.rpt_url + '&__format=doc&rp_userid='+user.userid+'\')" type="button" class="btn btn-sm btn-primary">DOC</button> ' ;
	}
	
	$.each(colModel.displistcols, function(){
	    var fieldName = new Object();
	    fieldName.headerName = this.displayname
	    fieldName.field = this.columnname
	    columsnName.push(this.columnname)
	    fieldName.hide = false
	   
	    if(this.uniquekey == 'Y' || this.uniquekey == 'y'){
	    	fieldName.hide = true
	    }
	    if(this.sortorder){
			if((this.sortorder).toLowerCase() == 'desc'){
	    		fieldName.sort = 'desc'
	    	}
	    	else{
	    		fieldName.sort = 'asc'
	    	}
		}
	    if(this.hidden == 'Y' || this.hidden == 'y'){
	    	fieldName.hide = true
	    }
	    if(this.columnname == 'rpt_url'){
	    	fieldName.cellRendererSelector = function (params){
	    		var btn = {
	    				component: 'reports',
	    				params: {'rpt_url':params.data.rpt_url, 'siteid':siteid}
	    		}
		    	return btn
		    }
	    	fieldName.width = 320
	    }
	    
	    columnDefs.push(fieldName)
	})
	
	var rowData = [];
	var l = 1;
	$.each(data.content, function(i, val) {
		var obj = new Object();
		for(var j = 0; j<columsnName.length; j++){
			if(columsnName[j]  == 'rp_id'){
				obj[columsnName[j]] = l;
				l++;
			}
			else{
				obj[columsnName[j]] = this[j]
			}
			
		}
		rowData.push(obj)
	});
	reportGridOptions['columnDefs'] = columnDefs
	reportGridOptions['rowData'] = rowData
	reportGridOptions['components'] = {
			reports: Reports
    }

	var gridDiv = document.getElementById(FI);
	gridDiv = gridDiv.getElementsByClassName('reportDatatable')[0]
	
    new agGrid.Grid(gridDiv, reportGridOptions);
    var allColumnIds = [];
    reportGridOptions.columnApi.getAllColumns().forEach(function(column) {
  		allColumnIds.push(column.colId);
	});

    reportGridOptions.columnApi.autoSizeColumns(allColumnIds, false);
	
	
	
	

	$FI.find('#'+rptDatatable).closest(".row").addClass("listContainer");
	$FI.find('#reportListContainer').find(".ag-center-cols-container").css('width','100%')
	$FI.find(".reportDatatable").closest('form').attr("oninput","$(this).onReportFilterTextBoxChanged()");
};

$.fn.onReportFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	reportGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.printReport = function(rtpUrl) {
	var rptUrl = reportingUrl + rtpUrl; 
	window.open(rptUrl);
};
