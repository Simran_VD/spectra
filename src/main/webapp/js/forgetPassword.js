var userId;	
$("#sendforgotOtp").on('click touchstart',function(event){
		formContainer = $(this).closest('form');
		var mblNum = formContainer.find('#mobileno').val();
		if(!(mblNum == "" || mblNum == null || mblNum == undefined)){
			if(checkMobile(mblNum)){
				$(this).cAjax('Post','./pages/sendForgotOTP', 'mobile='+mblNum, false, false, '', 'Error', true);
				xhrObj = $(this).data("xhr");
				if(xhrObj.status == 409) {
					 $("#unsuccessfull").html("User Does Not Exist!");
				}
				var Return = $.parseJSON(strResponseText);
				userId = Return.userid;
				if(Return.result){
					$("#unsuccessfull").html("");
					$("#CheckMobile").html("");
					formContainer.find(".verify").css("display","block")
					//formContainer.find('#mobileno').attr("disabled",true);
				}
			}
			else{
				 $("#CheckMobile").html("Enter Valid Mobile Number (Like - XXX-XXX-XXXX)");
			}
		}
	});
	
	$("#verifyforgotOtp").on('click touchstart',function(event){
		formContainer = $(this).closest('form');
		var mblNum = formContainer.find('#mobileno').val();
		var otp = formContainer.find('#otp').val();
		$(this).cAjax('GET','./pages/varifyOtp', 'mobile='+mblNum+'&opt='+otp, false, false, '', 'Error', true);
		if(strResponseText){
			formContainer.find(".pass").css("display","block");
		}
		else{
			formContainer.find(".pass").css("display","none");
			formContainer.find("#CheckOTP").html("Incorrect OTP");
		}
	});
	
	$("#saveForgotPass").on('click touchstart',function(event){
		formContainer = $(this).closest('form');
		event.preventDefault();
		if(formContainer.ValidateForm()) {
			var pass = formContainer.find('#pwd').val();
			var repass = formContainer.find('#repwd').val();
			var formObj  = $.parseJSON(serializeJSONIncludingDisabledFields(formContainer));
			/*formObj.loginid = formContainer.find('#mobileno').val();
			var name = formContainer.find('#name').val();
			var arr = name.split(" ");
			var len = arr.length;
			if(len == 3){
				formObj.firstname = arr[0];
				formObj.middlename = arr[1];
				formObj.lastname = arr[2];
			}
			else if(len == 2){
				formObj.firstname = arr[0];
				formObj.lastname = arr[1];
			}
			else{
				formObj.firstname = arr[0];
			}*/
			//var formObj = JSON.stringify(formObj);
			if(pass == repass){
				$(this).cAjaxPostJson('GET','./pages/updatePwd',"userId="+userId+"&newPwd="+pass , false, false, '', 'Error', true);	
				xhrObj = $(this).data("xhr");
				if(xhrObj.status == 409) {
					
				}
				else if (strResponseText) {
					var Return = $.parseJSON(strResponseText);
	                 if(strResponseText){
	                	 swal("Registered Successfully!", "","success");
	                	window.open("./signup","_self");
	                	
	                 }
				}
			}
		}	
	});
	
	function checkPasswordMatch() {
        var password = $("#pwd").val();
        var confirmPassword = $("#repwd").val();
        if (password != confirmPassword)
            $("#CheckPasswordMatch").html("Password does not match!").css("color","red");
        else
            $("#CheckPasswordMatch").html("Password matched.").css("color","green");
    }
	
	function checkMobile(mblNum) {
		var phoneno = /^\d{10}$/;
		 if(mblNum.match(phoneno))
			 return true;
		 else 
			 return false;
    }
	
	$.fn.togglePass = function(){
	    var eyeId = $(this).parent().find('input').attr('type');
	    if(eyeId == "password"){
	        $(this).find('i').attr('class', 'fa fa-eye-slash')
	        $(this).parent().find('input').attr('type','text')
	    }
	    else{
	        $(this).find('i').attr('class', 'fa fa-eye')
	        $(this).parent().find('input').attr('type','password')
	    }
	}