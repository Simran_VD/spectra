var JmId;
var addApplicantListdispid=17;
var addApplicantListGridOptions;
var whereClause;

$.fn.getdashboardapplist = function(){
	
	

	var FI = $(this).closest('.flip-card')
	JmId = FI.find("#hidetxt").text()
	$(this).addTab("AddApplicant" +JmId,'./pages?name=addApplicantForm',"Add Job Applicant",{"action":"view"});
	//$(this).filldashboardForm(JmId)
	
}
$.fn.filldashboardForm = function($FI){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined
	var formContainer = $FI;
	formContainer.find(".filter-text-box").remove()
	
	if(action && action=="view" ){
		$FI.find("input,select,textarea").attr("disabled","disabled")
		$FI.find(".filter-add-text-box").attr("disabled",false)
	}
	
	
	 $(this).cAjax('GET','./job/getAllCompanyNameView', '', false, false, '', 'Error', true);
	 cmpname =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect(formContainer.find("#clId"),cmpname,{"key":"clId","value":"companyName"});
	
	 $(this).cAjax('GET','./job/getAllCityView', '', false, false, '', 'Error', true);
	 locat =  $.parseJSON(strResponseText)
	 $(this).populateMultipleSelect(formContainer.find("#location"),locat,{"key":"lmId","value":"cityName"});
	
	 $(this).cAjax('GET','./job/NoticePeriodDropdownListVw', '', false, false, '', 'Error', true);
	 noticeJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect(formContainer.find("#noticePeriod"),noticeJson,{"key":"values","value":"timePeriod"});
	 
	 
	 $(this).cAjax('GET','./job/ExperienceDropDownVw', '', false, false, '', 'Error', true);
	 miniTotalExpJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect(formContainer.find("#miniTotalExp"),miniTotalExpJson,{"key":"values","value":"displayName"});
	 
	
	 $(this).populateAdvanceSelect(formContainer.find("#maxmTotalExp"),miniTotalExpJson,{"key":"values","value":"displayName"});
	 
	
	 $(this).populateAdvanceSelect(formContainer.find("#minReleExp"),miniTotalExpJson,{"key":"values","value":"displayName"});
	 
	 
	$(this).cAjax('GET','./master/getSpocMstListVw', '', false, false, '', 'Error', true);
	primarySpocJson =  $.parseJSON(strResponseText)
	$(this).populateAdvanceSelect(formContainer.find("#spocappId"),primarySpocJson,{"key":"spocId","value":"spocName"});
	 
	 
	 
	$(this).cAjax('GET','./job/getJobMaster',"jmId="+JmId, false, false, '', 'Error', true);
	var jobMst = $.parseJSON(strResponseText);
	formContainer.find("#jmId").val(jobMst.jmId);
	formContainer.find("#clId").val(jobMst.clId);
	formContainer.find("#jobCode").val(jobMst.jobCode);
	formContainer.find("#recuName").val(jobMst.rmId);
	formContainer.find("#spocappId").val(jobMst.spocId);
	formContainer.find("#companySpoc").val(jobMst.companySpoc);

	formContainer.find("#companySpocEmail").val(jobMst.companySpocEmail);
	formContainer.find("#companySpocPhone").val(jobMst.companySpocPhone);
	formContainer.find("#jobTitle").val(jobMst.jobTitle);
	formContainer.find("#jobRecdDt").val(jobMst.jobRecdDt);
	formContainer.find("#miniTotalExp").val(jobMst.minTotalExp);
	formContainer.find("#maxmTotalExp").val(jobMst.maxTotalExp);
	formContainer.find("#minReleExp").val(jobMst.minRelExp);
	formContainer.find("#budget").val(jobMst.ctc);
	formContainer.find("#noticePeriod").val(jobMst.joiningPeriod);
	formContainer.find("#jobType").val(jobMst.jobType);
	formContainer.find("#noOfPosition").val(jobMst.noOfPosition);
	formContainer.find("#skillNote").val(jobMst.skillNote);

	
	var loc=[];
	$.each(jobMst.jobLocation,function(i,val){
	  loc.push(val.lmId);
	});
	formContainer.find("#location").val(loc);
	formContainer.find('#location').select2()
	formContainer.find("#ctc").val(jobMst.ctc);
	
}



$.fn.addApplicantList = function(FI) {
	whereClause = "jm_id='"+JmId+"'";
	var id2 =""
	addApplicantListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
				
			},
			pagination: true,
			
			rowSelection: 'single',
			onRowDoubleClicked: openApplicantDetails,

			
			
		};
	$(this).reloadAddApplicantList("addApplicantList", addApplicantListdispid, addApplicantListGridOptions,FI,whereClause);
    $FI.find("#addApplicantListCont").find('input').attr("oninput","$(this).onaddApplicantTextBoxChanged()");
}

$.fn.onaddApplicantTextBoxChanged = function() {
	var FI = $(this).closest('form');
	addApplicantListGridOptions.api.setQuickFilter(FI.find('.filter-add-text-box').val());
}

$.fn.openApplicantForm = function(){
	var FI = $(this).closest('form');
	var tabName = FI.find("#tabName").val()
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
       } 
	$(this).addTab("Applicant",'./pages?name=job/addEditApplicantMaster',"Add Job Applicant",{"action":"new",tabName,JmId});
	
}

 function openApplicantDetails(row){
	var applicantSelection = row.data;
	var data = applicantSelection.am_id;
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
       } 
	$(this).addTab("ViewApplicant"+data,'./pages?name=job/addEditApplicantMaster',"View Matching Applicant-"+data,{"data":data,"action":"view"});
}

 
 $.fn.reloadAddApplicantList = function(id, dispid, gridOption, id2, whereClause, additonalParam){
		if(id2){
			$("#"+id2).find('#'+id).empty();
		}
		else{
			$("#"+id).empty();
		}
		
		if(whereClause){
			$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
			var colModel= $.parseJSON(strResponseText);
			$(this).cAjax('GET','./FetchData?id='+dispid+'&type=&page=0&size=20&whereClause='+whereClause, '', false, false, '', 'Error', true);
			var data =  $.parseJSON(strResponseText);
		}
		else if(additonalParam != undefined){
			$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
			var colModel= $.parseJSON(strResponseText);
			$(this).cAjax('GET','./FetchData?id='+dispid+'&type='+additonalParam+'&page=0&size=20', '', false, false, '', 'Error', true);
			var data =  $.parseJSON(strResponseText);
		}
		else{
			$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
			var colModel= $.parseJSON(strResponseText);
			$(this).cAjax('GET','./FetchData?id='+dispid+'&type=&page=0&size=20', '', false, false, '', 'Error', true);
			var data =  $.parseJSON(strResponseText);
		}
		
//		colModel.displistcols = colModel.displistcols.sort((a, b) => {
//		    return a.columnid - b.columnid;
//		});
		
		var hiddenCol = "";
		var columnDefs = [];
		var columsnName = [];
		$.each(colModel.displistcols, function(){
		    var fieldName = new Object();
		    fieldName.headerName = this.displayname
		    fieldName.field = this.columnname
		    if(this.datatype == 'numeric'){
		    	fieldName.type = 'rightAligned'
		    }
		    
		    if(this.columnname == 'doc_name'){
		    	fieldName.type = 'leftAligned'
		    }
		    columsnName.push(this.columnname)
		    fieldName.hide = false
		   
		    if(this.uniquekey == 'Y' || this.uniquekey == 'y'){
		    	fieldName.hide = true
		    }
		    if(this.sortorder){
				if((this.sortorder).toLowerCase() == 'desc'){
		    		fieldName.sort = 'desc'
		    	}
		    	else{
		    		fieldName.sort = 'asc'
		    	}
			}
		    if(this.hidden == 'Y' || this.hidden == 'y'){
		    	fieldName.hide = true
		    }
		    columnDefs.push(fieldName)
		})
		var rowData = [];
		$.each(data.content, function(i, val) {
			var obj = new Object();
			for(var j = 0; j<columsnName.length; j++){
				obj[columsnName[j]] = this[j]
			}
			rowData.push(obj)
		});
		gridOption['columnDefs'] = columnDefs
		gridOption['rowData'] = rowData
		gridOption['rowHeight'] = 30;
		gridOption['headerHeight'] = 35;


		if(id2){
			var gridDiv = document.getElementById(id2).querySelector("#"+id);
		}
		else{
			var gridDiv = document.querySelector('#'+id);
		}
		
		
	    new agGrid.Grid(gridDiv, gridOption);
	    var allColumnIds = [];
	    gridOption.columnApi.getAllColumns().forEach(function(column) {
	      allColumnIds.push(column.colId);
	    });

	    gridOption.columnApi.autoSizeColumns(allColumnIds, false);
}

 $.fn.onaddApplicantTextBoxChanged = function() {
		var FI = $(this).closest('form');
		addApplicantListGridOptions.api.setQuickFilter(FI.find('.filter-add-text-box').val());
	}