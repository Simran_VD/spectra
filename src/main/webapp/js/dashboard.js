var totalCount = [];
var matchCount = [];
var dashboarddispid = 27;
var dashboardgridOptions;

$.fn.YearKpiData = function(){
	var yearKpi = $("#yearlink").attr("data-value");
	$(this).kpiData(yearKpi)
}

$.fn.MonthKpiData = function(){
 	var monthKpi = $("#monthlink").attr("data-value");
 	$(this).kpiData(monthKpi)
}
$.fn.kpiData = function(filterkpi){
	var filter;
	
	
	$(".jobDtl").empty()
	$(".cardContainer").css('display','')
	$(".filterBtn").css('display','')
	if(filterkpi){
		
		filter = filterkpi
	}
	else{
		filter ="M"
	}
	$(this).cAjax('GET','./job/getAllDashboardJobStatusCount', 'status='+filter, false, false, '', 'Error', true);
	var kpiData= $.parseJSON(strResponseText);
	$(".jobcontent").empty()
	$(".applContent").empty()
	var jobsCount = kpiData.jobsCount
	
	var applicantCount = kpiData.applicantCount
	
	$.each(jobsCount,function(){
		var jobsBox = `<div class="row p-0">
						    <p class="col-lg-8 col-8 mb-1 p-0">`+this.label+`</p>
						    <p class="col-lg-1 col-1 mb-1 p-0">:</p>
						    <p class="col-lg-3 col-3 mb-1 p-0">`+this.count+`</p>
						</div>`;
        $(".jobcontent").append(jobsBox)
	})	
	
	$.each(applicantCount,function(){
		var applBox = `<div class="row p-0">
						    <p class="col-lg-8 col-8 mb-1 p-0">`+this.label+`</p>
						    <p class="col-lg-1 col-1 mb-1 p-0">:</p>
						    <p class="col-lg-3 col-3 mb-1 p-0">`+this.count+`</p>
						</div>`;
        $(".applContent").append(applBox)
	})	
	$(this).displayChart(jobsCount);
    $(this).displayapplChart(applicantCount);  
}


$.fn.dashboardJobs = function(){
	$(".filterBtn").css('display','none')
	
	$(".cardContainer").css('display','none')
	$(".jobDtl").css('display','')
	$(this).cAjax('GET','./job/getAllDashboardJobs', '', false, false, '', 'Error', true);;
	var data= $.parseJSON(strResponseText);
	$(".jobDtl").empty()
	
	
	if(data.length > "0"){
	$.each(data,function(){
		var jobsBox = `<div class="activeDiv col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
						<div class="flip-card"  onclick="$(this).openDetails()" >
						  
						    <div class="flip-card-front">  
						      <small style="display:none;" id="hidetxt">`+this.jm_id+`</small>
							  <p class="fronttext">`+this.message+`</p>
						    </div>
						    <div class="flip-card-back" >
						     <small class="col-lg-7 col-7 backtxt p-0 mb-n3">Total Applicant&nbsp&nbsp:&nbsp`+this.total_matching+`</small><br>
						     
						     <small class="col-lg-7 col-7 backtxt p-0 mb-n3">Matching Applicant&nbsp&nbsp:&nbsp`+this.matching_applicants+`</small><br>
						     
						     
						     <small class="col-lg-8 col-8  p-0 mb-n3 clickLinks" onclick="$(this).getdashboardapplist()"><b>Add Job Applicant</b></small><br>
						     <small class="col-lg-8 col-8  p-0 mb-n3 clickLinks"  onclick="$(this).getjobopeningdash()"><b>View Matching Details</b></small>      
							 </div>
							 </div> 
						   
						 
 					</div>`
        $(".jobDtl").append(jobsBox)
	})	
  }
	else{
		var Text =  `<div class="noJobs col-12 col-sm-12 col-md-12 col-lg-12 text-center mt-4">
						
						<h1 class="displayMsg">At this moment, there are no jobs allocated to you.</h1>
						
					</div>`
	  $(".jobDtl").append(Text)
			
	}
	
}

$.fn.openDetails = function(){
	var FI = $(this).closest('.flip-card').toggleClass('flipped');
		
}

$.fn.closeDetails = function(){
	
	var FI = $(this).closest('.flip-card').toggleClass('flipped');
	

}

/*This function is used for charts on dashboard*/
$.fn.displayChart = function(jobsCount){
	var backgroundColor = [];
	var borderColor = [];
	var names=[];
	var count=[];
	$.each(jobsCount, function(i,v) {
		names.push(jobsCount[i].label);
		count.push(jobsCount[i].count);
		backgroundColor.push('rgba(255, 99, 132, 1)')
		borderColor.push('rgba(255, 99, 132, 1)')

	});
	
	
	var ctx1 = document.getElementById("myChart1").getContext("2d");
	
	  myChart1 = new Chart(ctx1, {
		  type: 'bar',
		    data: {
		        labels:names,
		        datasets: [{
		            label:'Job Details',
		            data: count,
		            pointBackgroundColor:'purple',
		            pointBorderColor:'purple',
		            pointRadius:1.5,
		            backgroundColor:backgroundColor, 
		            borderColor: borderColor,
		            borderWidth: 2
		        }]
		    },
		    options: {
		    	responsive: true,
		    	legend: {
		 		   display: false,
		 		},
		        tooltips: {
		         mode: 'label'
		     },
		   title: {
		 		display: true,
		 		text: 'Job Details'
		 	},
		   scales: {
		         xAxes: [{
		           maxBarThickness: 50,
		             stacked: true,
		           gridLines: {
		                   color: "rgba(255,255,255,0)",
		                   drawOnChartArea: false,
		               	}
		         	}],
		         yAxes: [{
		             stacked: true,
		              ticks: {
		                 min: 0,
		             	}
		         	}]
		     	}
	     	}
		});
	
	
	
};


/*This function is used for charts on dashboard*/
$.fn.displayapplChart = function(applicantCount){
	
	var backgroundColor = [];
	var borderColor = [];
	var applnames=[];
	var applcount=[];
	$.each(applicantCount, function(i,v) {
		applnames.push(applicantCount[i].label);
		applcount.push(applicantCount[i].count);
		backgroundColor.push('rgba(255, 99, 132, 1)')
		borderColor.push('rgba(255, 99, 132, 1)')

	});
	
	var ctx2 = document.getElementById("myChart2").getContext("2d");
	

	  myChart2 = new Chart(ctx2, {
			  type: 'bar',
			    data: {
			        labels:applnames,
			        datasets: [{
			            label: 'Applicant Details',
			            data: applcount,
			            pointBackgroundColor:'purple',
			            pointBorderColor:'purple',
			            pointRadius:1.5,
			            backgroundColor:backgroundColor, 
			            borderColor: borderColor,
			            borderWidth: 2
			        }]
			    },
			    options: {
			    	responsive: true,
			    	legend: {
			 		   display: false,
			 		},
			        tooltips: {
			         mode: 'label'
			     },
			   title: {
			 		display: true,
			 		text: 'Applicant Details'
			 	},
			   scales: {
			         xAxes: [{
			           maxBarThickness: 50,
			             stacked: true,
			           gridLines: {
			                   color: "rgba(255,255,255,0)",
			                   drawOnChartArea: false,
			               	}
			         	}],
			         yAxes: [{
			             stacked: true,
			              ticks: {
			                 min: 0,
			             	}
			         	}]
			     	}
		     	}
			});
}

/*This is used to show grid on dashboard*/
$.fn.dashboardGrid = function(){
	$(".filter-text-box").css("display","none")
	dashboardgridOptions = {
				defaultColDef: {
					sortable: true,
					resizable: true,
					filter: true,
				},
				pagination: true,
				rowSelection: 'single',
			};
		$(this).reloadCaseGrid("dashboardlistgrid", dashboarddispid, dashboardgridOptions);
}


/*This Function is used for listing*/
$.fn.reloadCaseGrid = function(id, dispid, gridOption, id2, whereClause, additonalParam) {
	if(id2){
		$("#"+id2).find('#'+id).empty();
	}
	else{
		$("#"+id).empty();
	}
	
	if(whereClause){
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type=&page=0&size=20&whereClause='+whereClause, '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	else if(additonalParam != undefined){
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type='+additonalParam+'&page=0&size=20', '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	else{
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type=&page=0&size=20', '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	
//	colModel.displistcols = colModel.displistcols.sort((a, b) => {
//	    return a.columnid - b.columnid;
//	});
	
	var hiddenCol = "";
	var columnDefs = [];
	var columsnName = [];
	var JobApplicant = function(params) {
	    return 	`<a href="#" style="color: #e22462;" onclick="$(this).openJobApplicant();">Job Applicant</a>` ;
	}
	
	
	$.each(colModel.displistcols, function(){
	    var fieldName = new Object();
	    fieldName.headerName = this.displayname
	    fieldName.field = this.columnname
	    if(this.datatype == 'numeric'){
	    	fieldName.type = 'rightAligned'
	    }
	    
	    if(this.columnname == 'doc_name'){
	    	fieldName.type = 'leftAligned'
	    }
	    columsnName.push(this.columnname)
	    fieldName.hide = false
	   
	    if(this.uniquekey == 'Y' || this.uniquekey == 'y'){
	    	fieldName.hide = true
	    }
	    if(this.sortorder){
			if((this.sortorder).toLowerCase() == 'desc'){
	    		fieldName.sort = 'desc'
	    	}
	    	else{
	    		fieldName.sort = 'asc'
	    	}
		}
	    if(this.hidden == 'Y' || this.hidden == 'y'){
	    	fieldName.hide = true
	    }
	    
	    
	    if(this.columnname == 'job_applicant'){
	    	fieldName.cellRendererSelector = function (params){
	    		var btn = {
	    				component: 'jobApplicant',
	    		}
		    	return btn
		    }
	    	
	    }
	    
	    
	    columnDefs.push(fieldName)
	})
	var rowData = [];
	$.each(data.content, function(i, val) {
		var obj = new Object();
		for(var j = 0; j<columsnName.length; j++){
			obj[columsnName[j]] = this[j]
		}
		rowData.push(obj)
	});
	gridOption['columnDefs'] = columnDefs
	gridOption['rowData'] = rowData
	gridOption['components'] = {
			jobApplicant: JobApplicant
    }
	gridOption['rowHeight'] = 41;
	gridOption['headerHeight'] = 35;


	if(id2){
		var gridDiv = document.getElementById(id2).querySelector("#"+id);
	}
	else{
		var gridDiv = document.querySelector('#'+id);
	}
	
	
    new agGrid.Grid(gridDiv, gridOption);
    var allColumnIds = [];
    gridOption.columnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });

    gridOption.columnApi.autoSizeColumns(allColumnIds, false);
}



/*This function is user to show Task And Reminder on dashboard*/

$.fn.TaskandReminder = function(){
	$(this).cAjax('GET','./job/getTaskandReminder', '', false, false, '', 'Error', true);;
	var reminderData= $.parseJSON(strResponseText);
	$(".reminderText").empty();
	$.each(reminderData,function(){
		var taskBox = `<p class="ml-1 taskPara mb-1">`+this.message+` </p> <hr>`
        $(".reminderText").append(taskBox)
	})	
}


/*This function is used to open JOb applicant tab from case listing*/ 
$.fn.openJobApplicant = function(){
	try{
		var data = [];
		data.push(dashboardgridOptions.api.getSelectedRows()[0].jm_id);
		data.push(dashboardgridOptions.api.getSelectedRows()[0].am_id);
		
	}
	catch(e){
		var data = undefined;
	}
	
	if(!data){
		swal("Please select a row first");
		return false;
	}
		
	
	var valdata = dashboardgridOptions.api.getSelectedRows()[0].ja_id;
	
	$(this).addTab("ediShareWithClient"+valdata,'./pages?name=job/addEditJobApplicant',"Edit Job Applicant-"+valdata,{"data":data,"action":"view",valdata});

}