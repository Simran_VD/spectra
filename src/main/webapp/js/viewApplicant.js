var JmId;
var viewApplicantListdispid=16;
var viewApplicantListGridOptions;
var whereClause;
$.fn.getjobopeningdash = function(){	
	var FI = $(this).closest('.flip-card')
	JmId = FI.find("#hidetxt").text()
	whereClause ="jm_id="+JmId;
	$(this).addTab("ViewJobApplicant" +JmId,'./pages?name=viewApplicantForm',"View Job Applicant",{"action":"view"});

}
$.fn.fillviewdashboardForm = function($FI){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined
	var formContainer = $FI;
	formContainer.find(".filter-text-box").remove()
	
	
	if(action && action=="view" ){
		formContainer.find("input,select,textarea").attr("disabled","disabled")
		formContainer.find(".filter-view-text-box").attr("disabled",false)
	}
	
	
	 $(this).cAjax('GET','./job/getAllCompanyNameView', '', false, false, '', 'Error', true);
	 cmpname =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect(formContainer.find("#clId"),cmpname,{"key":"clId","value":"companyName"});
	
	 $(this).cAjax('GET','./job/getAllCityView', '', false, false, '', 'Error', true);
	 locat =  $.parseJSON(strResponseText)
	 $(this).populateMultipleSelect(formContainer.find("#location"),locat,{"key":"lmId","value":"cityName"});
	
	
	 $(this).cAjax('GET','./job/NoticePeriodDropdownListVw', '', false, false, '', 'Error', true);
	 noticeJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect(formContainer.find("#noticePeriod"),noticeJson,{"key":"values","value":"timePeriod"});
	 
	 
	 $(this).cAjax('GET','./job/ExperienceDropDownVw', '', false, false, '', 'Error', true);
	 miniTotalExpJson =  $.parseJSON(strResponseText)
	 $(this).populateAdvanceSelect(formContainer.find("#miniTotalExp"),miniTotalExpJson,{"key":"values","value":"displayName"});
	 
	
	 $(this).populateAdvanceSelect(formContainer.find("#maxmTotalExp"),miniTotalExpJson,{"key":"values","value":"displayName"});
	 
	
	 $(this).populateAdvanceSelect(formContainer.find("#minReleExp"),miniTotalExpJson,{"key":"values","value":"displayName"});
	
	 $(this).cAjax('GET','./job/getSpocDropdown', "", false, false, '', 'Error', true);
		spoc = JSON.parse(strResponseText);
		/*$(this).populateAdvanceSelect($FI.find("#spocName"),spoc,{"key":"spocId","value":"spocName"});	 
	 $(this).cAjax('GET','./master/getSpocMstListVw', '', false, false, '', 'Error', true);
	 primarySpocJson =  $.parseJSON(strResponseText)
*/	 $(this).populateAdvanceSelect(formContainer.find("#spocviewId"),spoc,{"key":"spocId","value":"spocName"});
	 
	 
	$(this).cAjax('GET','./job/getJobMaster',"jmId="+JmId, false, false, '', 'Error', true);
	var jobMst = $.parseJSON(strResponseText);
	formContainer.find("#jmId").val(jobMst.jmId);
	formContainer.find("#clId").val(jobMst.clId);
	formContainer.find("#jobCode").val(jobMst.jobCode);
	formContainer.find("#recuName").val(jobMst.rmId);
	
	for(var i=0; i< jobMst.jobSpoc.length; i++){
		if(jobMst.jobSpoc[i].category == "P"){
			formContainer.find("#spocviewId").val(jobMst.jobSpoc[i].spocId);
		}	
		formContainer.find("#spocviewId").select2();
	}
	//formContainer.find("#spocviewId").val(jobMst.spocId);

	formContainer.find("#companySpocEmail").val(jobMst.companySpocEmail);
	formContainer.find("#companySpocPhone").val(jobMst.companySpocPhone);
	formContainer.find("#jobTitle").val(jobMst.jobTitle);
	formContainer.find("#jobRecdDt").val(jobMst.jobRecdDt);
	formContainer.find("#miniTotalExp").val(jobMst.minTotalExp);
	formContainer.find("#maxmTotalExp").val(jobMst.maxTotalExp);
	formContainer.find("#minReleExp").val(jobMst.minRelExp);
	formContainer.find("#ctc").val(jobMst.ctc);
	formContainer.find("#noticePeriod").val(jobMst.joiningPeriod);
	formContainer.find("#jobType").val(jobMst.jobType);
	formContainer.find("#noOfPosition").val(jobMst.noOfPosition);
	formContainer.find("#skillNote").val(jobMst.skillNote);
	
	var loc=[];
	$.each(jobMst.jobLocation,function(i,val){
	  loc.push(val.lmId);
	});
	formContainer.find("#location").val(loc);
	formContainer.find('#location').select2()
	formContainer.find("#ctc").val(jobMst.ctc);
	
	
	
}




$.fn.viewApplicantList = function($FI) {
	
	var id2 =""
		viewApplicantListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
				
			},
			pagination: true,
			
			 rowSelection: 'multiple',
			 rowMultiSelectWithClick: true,
			onRowDoubleClicked: viewApplicantDetails,

		};
	$(this).reloadViewApplicantList("viewApplicantList",viewApplicantListdispid,viewApplicantListGridOptions,$FI,whereClause);
	$("#viewApplicantList").closest('form').attr("oninput","$(this).onviewApplicantTextBoxChanged()");	
}


$.fn.reloadViewApplicantList = function(id, dispid, gridOption, id2, whereClause, additonalParam){
	if(id2){
		$("#"+id2).find('#'+id).empty();
	}
	else{
		$("#"+id).empty();
	}
	
	if(whereClause){
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type=&page=0&size=20&whereClause='+whereClause, '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	else if(additonalParam != undefined){
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type='+additonalParam+'&page=0&size=20', '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	else{
		$(this).cAjax('GET','./List?id='+dispid, '', false, false, '', 'Error', true);
		var colModel= $.parseJSON(strResponseText);
		$(this).cAjax('GET','./FetchData?id='+dispid+'&type=&page=0&size=20', '', false, false, '', 'Error', true);
		var data =  $.parseJSON(strResponseText);
	}
	
//	colModel.displistcols = colModel.displistcols.sort((a, b) => {
//	    return a.columnid - b.columnid;
//	});
	
	var hiddenCol = "";
	var columnDefs = [];
	var columsnName = [];
	$.each(colModel.displistcols, function(){
	    var fieldName = new Object();
	    fieldName.headerName = this.displayname
	    fieldName.field = this.columnname
	    if(this.datatype == 'numeric'){
	    	fieldName.type = 'rightAligned'
	    }
	    
	    if(this.columnname == 'doc_name'){
	    	fieldName.type = 'leftAligned'
	    }
	    columsnName.push(this.columnname)
	    fieldName.hide = false
	   
	    if(this.uniquekey == 'Y' || this.uniquekey == 'y'){
	    	fieldName.hide = true
	    }
	    if(this.sortorder){
			if((this.sortorder).toLowerCase() == 'desc'){
	    		fieldName.sort = 'desc'
	    	}
	    	else{
	    		fieldName.sort = 'asc'
	    	}
		}
	    if(this.hidden == 'Y' || this.hidden == 'y'){
	    	fieldName.hide = true
	    }
	    columnDefs.push(fieldName)
	})
	var rowData = [];
	$.each(data.content, function(i, val) {
		var obj = new Object();
		for(var j = 0; j<columsnName.length; j++){
			obj[columsnName[j]] = this[j]
		}
		rowData.push(obj)
	});
	gridOption['columnDefs'] = columnDefs
	gridOption['rowData'] = rowData
	gridOption['rowHeight'] = 30;
	gridOption['headerHeight'] = 35;


	if(id2){
		var gridDiv = document.getElementById(id2).querySelector("#"+id);
	}
	else{
		var gridDiv = document.querySelector('#'+id);
	}
	
	
    new agGrid.Grid(gridDiv, gridOption);
    var allColumnIds = [];
    gridOption.columnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });

    gridOption.columnApi.autoSizeColumns(allColumnIds, false);
}



$.fn.onviewApplicantTextBoxChanged = function() {
	var FI = $(this).closest('form');
	viewApplicantListGridOptions.api.setQuickFilter(FI.find('.filter-view-text-box').val());
}



$.fn.saveViewApplicant = function($FI){
	var FI = $FI
	var form = $(this).closest("#addApplicant");
	var jobObj = [];
	
	var d = new Date(); // for now
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    
    var currentDate = day + "/" + month + "/" + year;

	
	let rowsSelection = viewApplicantListGridOptions.api.getSelectedRows();
	if(rowsSelection.length > "0"){
    for(var i=0;i<rowsSelection.length;i++){
      var viewObj = new Object()
      viewObj.jaId = 0
      viewObj.amId = rowsSelection[i].am_id
      viewObj.jmId = rowsSelection[i].jm_id
      viewObj.dateapplied = currentDate
      console.log(viewObj)
      jobObj.push(viewObj)
    }
}
	else{
		swal("Please Select Row To Pick Profiles")
		return false;
	}
	
    var viewJson = JSON.stringify(jobObj)
    if(navigator.onLine == false){
	    swal("Warning", "You're offline. Check your connection.", "warning");
	    return false;
      } 
	$(this).cAjaxPostJson('Post','./job/saveJobApplicant',viewJson, false, false, '', 'Error', true);
	xhrObj = $(this).data("xhr");
	
	if(strResponseText) {
		var applicantReturn = $.parseJSON(strResponseText);
		for(var a = 0;a<applicantReturn.length;a++){
		   
         if(applicantReturn[a].jaId){
			swal("Saved!", "Profile picked successfully.", "success");
			//$(this).reloadGrid("viewApplicantList", viewApplicantListdispid, viewApplicantListGridOptions);
			$(this).reloadViewApplicantList("viewApplicantList",viewApplicantListdispid,viewApplicantListGridOptions,$FI,whereClause)
		//	FI.find("#viewApplicantList").empty()
		//	$(this).viewApplicantList()
			
			return true;
		} else {
			swal({title: "Error!",  text: "An internal error occurred while saving User. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
		}
	}
	} else {
		swal({title: "Error!",  text: "An internal error occurred while saving User. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
	}
	return false;

}


function viewApplicantDetails(row){
	var viewapplicantSelection = row.data;
	var data = viewapplicantSelection.am_id;
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
       } 
	$(this).addTab("ViewApplicant"+data,'./pages?name=job/addEditApplicantMaster',"View Applicant-"+data,{"data":data,"action":"view"});
}