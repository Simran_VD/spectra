prodpartListId = 17;
var prodpartGridOptions;
$.fn.getproductpart = function(){
	$(this).addTab("getproductpart",'./pages?name=/product/productPart',"Product Parts");
};

$.fn.prodpartListing = function(){
	prodpartGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',

		};
$(this).reloadGrid("prodpartList", prodpartListId, prodpartGridOptions);
	
	$("#prodpartListContainer .btn-container .btn").on('click touchstart',function(){
		var opration=$(this).attr("title"),data;
		opration=opration.toLocaleLowerCase();
		if(opration != "add new"){
			
			
			try{
				var data = onSelectionChanged(prodpartGridOptions)[0].pp_id;
			}
			catch(e){
				var data = undefined;
			}
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(opration) {
			case "add new":
				$('#prodpartModal').modal('show');
				$('#prodpartModal').enableFields()
				$('#prodpartModal').ResetAllDataInFormpdrPart();
				break;
			case "edit":
			case "view":
				$(this).cAjax('GET', './product/getProdPartMst',"ppId="+data, false, false, '','Error', true);
				var srvchrgJson = $.parseJSON(strResponseText);
				$('#prodpartModal').modal('show')
				$('#prodpartModal').fillsrvchrgForm(srvchrgJson, opration);
				
				break;
			case "delete":
				swal({
					title: "Are you sure Do you want  to delete Product Parts?",
					text: "You will not be able to recover Product Parts now!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No cancel please!",
					confirmButtonColor:'#3095d6',
					cancelmButtonColor:'#d33',
					closeOnConfirm: false,
					closeOnCancel: false,
					showConfirmButton: true,
				},
				function(isConfirm) {
					if (isConfirm) {
				$(this).cAjax('Post','./product/deleteProdPartMst',"ppId="+data, false, false, '', 'Error', true);
				if(strResponseText == "SUCCESS"){
					swal("Deleted!", "Selected Product Parts  deleted successfully.", "success");
					$(this).reloadGrid("prodpartList", prodpartListId, prodpartGridOptions);
				} else if(strResponseText == "DATA_INTEGRITY"){
					swal("Error in deletion!", "Selected Product Parts is being used. So it cannnot be deleted.", "warning");
				}
					}
					else {
						swal("Cancelled", "Thank you! your Product Parts is safe :)", "error");
					}});
				break;
				
//			case "print":
//				var rptUrl = reportingUrl + "/MasterData/designation.rptdesign&__format=pdf";
//				window.open(rptUrl);
//				break;
				
			default:
				swal("Not implemented.");
		}
		return false;
	});
	$("#prodpartListContainer").closest(".row").addClass("listContainer");
	$("#prodpartList").closest('form').attr("oninput","$(this).onprodpartFilterTextBoxChanged()");

};


$.fn.onprodpartFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	prodpartGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}




$.fn.ResetAllDataInFormpdrPart=function(){
	var Form=$(this).find("#prodpartForm")

	
	$(this).ResetAllDataInForm(Form)
};



$.fn.fillsrvchrgForm = function(srvchrgJson, opration) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#ppId").val(srvchrgJson.ppId);
	$(this).find("#siteId").val(srvchrgJson.siteId);
	$(this).find("#partsName").val(srvchrgJson.partsName);
	$(this).find("#kent").val(srvchrgJson.kent);
	$(this).find("#aquaguard").val(srvchrgJson.aquaguard);
	$(this).find("#zeroB").val(srvchrgJson.zeroB);
	$(this).find("#normal").val(srvchrgJson.normal);
	$(this).find("#remarks").val(srvchrgJson.remarks);
	
	
    
    if(srvchrgJson.isActive == 'Y') {
        $("#prdpartActive").prop("checked", true)
	} else {
        $("#prdpartInactive").prop("checked", true)     
	}

	if (opration && opration == "view") {
		$(this).find("input, select, button").attr("disabled", "disabled");
		$(this).find("#nxtBtn").attr("disabled",false);
		$(this).find("#closeDesignationBtn1").attr("disabled", false);
		$(this).find("#closeDesignationBtn2").attr("disabled", false);
		$(this).find("#savesrvCharge").attr("disabled", true);
	} else if (opration && opration == "edit") {
		//$(this).find("#slabNo").attr("disabled", true);
		//$(this).find("#saveareamst").attr("disabled", true);
	}
	
};

$.fn.savesrvCharge = function() {
	var FI= $(this).parents('.modal-content').find('form');
	if(!FI.ValidateForm() ){//|| !$(this).validate(FI)) {
		return false;
	}
	var srvChargeJson = (serializeJSONIncludingDisabledFields(FI));
	
	
	$(this).cAjaxPostJson('Post','./product/saveProdPartMst',(srvChargeJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Parts!", "", "warning");  
	} else  if(strResponseText) { 
		var srvchrg = $.parseJSON(strResponseText);
		if(srvchrg.ppId){
			swal("Saved!", " Product Part saved successfully.", "success");
			$('#prodpartModal').modal('hide');
			$(this).reloadGrid("prodpartList", prodpartListId, prodpartGridOptions);
			$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};	
