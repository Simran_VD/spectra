prdmasterListId = 15;
var prdmasterGridOptions;
$.fn.getproductmaster = function(){
	$(this).addTab("getproductmaster",'./pages?name=/product/productMaster',"Product Master");
};

$.fn.prdmasterListing = function(){
	prdmasterGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',

		};
$(this).reloadGrid("prdmasterList", prdmasterListId, prdmasterGridOptions);
	
	$("#prdmasterListContainer .btn-container .btn").on('click touchstart',function(){
		var opration=$(this).attr("title"),data;
		opration=opration.toLocaleLowerCase();
		if(opration != "add new"){
			
			
			try{
				var data = onSelectionChanged(prdmasterGridOptions)[0].prd_id;
			}
			catch(e){
				var data = undefined;
			}
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(opration) {
			case "add new":
				$('#prdmasterModal').ResetAllElemOfprdmstForm()
				$('#prdmasterModal').modal('show');
				$('#prdmasterModal').find("#prdmstActive").val("Y");
				$('#prdmasterModal').find("#prdmstInactive").val("N");
				break;
			case "edit":
			case "view":
				$(this).cAjax('GET', './product/getProductMst',"prdId="+data, false, false, '','Error', true);
				var prdmstJson = $.parseJSON(strResponseText);
				$('#prdmasterModal').modal('show')
				$('#prdmasterModal').fillprdmstForm(prdmstJson, opration);
					break;
			case "delete":
				swal({
					title: "Are you sure Do you want  to delete Product?",
					text: "You will not be able to recover Product now!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No cancel please!",
					confirmButtonColor:'#3095d6',
					cancelmButtonColor:'#d33',
					closeOnConfirm: false,
					closeOnCancel: false,
					showConfirmButton: true,
				},
				function(isConfirm) {
					if (isConfirm) {
				$(this).cAjax('Post','./product/deleteProductMst',"prdId="+data, false, false, '', 'Error', true);
				if(strResponseText == "Success"){
					swal("Deleted!", "Selected Product deleted successfully.", "success");
					$(this).reloadGrid("prdmasterList", prdmasterListId, prdmasterGridOptions);
				} else if(strResponseText == "DATA_INTEGRITY"){
					swal("Error in deletion!", "Selected Product is being used. So it cannnot be deleted.", "success");
				}
					}
					else {
						swal("Cancelled", "Thank you! your Product is safe :)", "error");
					}});
				break;
				
//			case "print":
//				var rptUrl = reportingUrl + "/MasterData/designation.rptdesign&__format=pdf";
//				window.open(rptUrl);
//				break;
				
			default:
				swal("Not implemented.");
		}
		return false;
	});
	$("#prdmasterListContainer").closest(".row").addClass("listContainer");
	$("#prdmasterList").closest('form').attr("oninput","$(this).onprdmstFilterTextBoxChanged()");

};


$.fn.onprdmstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	prdmasterGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}



$.fn.fillprdmstForm = function(prdmstJson, opration) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#prdid").val(prdmstJson.prdId);
	$(this).find("#siteid").val(prdmstJson.siteId);
	$(this).find("#prdname").val(prdmstJson.prodName);
	$(this).find("#remark").val(prdmstJson.remarks);
	
	
    
    if(prdmstJson.isActive == 'Y') {
        $("#prdmstActive").prop("checked", true)
	} else {
        $("#prdmstInactive").prop("checked", true)     
	}

	if (opration && opration == "view") {
		$(this).find("input, select, button").attr("disabled", "disabled");
		$(this).find("#closeDesignationBtn1").attr("disabled", false);
		$(this).find("#closeDesignationBtn2").attr("disabled", false);
		$(this).find("#saveprdmst").attr("disabled", true);
	} else if (opration && opration == "edit") {
		
	}
	
};

$.fn.saveprdMaster = function() {
	var FI= $(this).closest('form');
	if(!FI.ValidateForm() ){//|| !$(this).validate(FI)) {
		return false;
	}
	var prdmstJson = (serializeJSONIncludingDisabledFields(FI));
	
	
	$(this).cAjaxPostJson('Post','./product/saveProductMst',(prdmstJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Product!", "", "warning");  
	} else  if(strResponseText) { 
		var prdmst = $.parseJSON(strResponseText);
		if(prdmst.prdId){
			swal("Saved!", "Product  saved successfully.", "success");
			$(this).cAjax('GET','./website/getProductList', '', false, false, '', 'Error', true);
			$('#prdmasterModal').modal('hide');
			$(this).getproductmaster()
			$(this).reloadGrid("prdmasterList", prdmasterListId, prdmasterGridOptions);
			$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};	


$.fn.ResetAllElemOfprdmstForm=function(){
	var FI=$("#prdmasterForm")
	$(this).ResetAllElemOfForm(FI)
};
