var pdrsrvdispid=3;
var pdrsrvGridOptions;
$.fn.getprodservicemap = function() {
$(this).addTab("Product_Service",'./pages?name=product/productList',"Product Service");

}
$.fn.pdrsrvList = function() {
	pdrsrvGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true
			},
			pagination: true,
			rowSelection: 'single',
			

		};
	$(this).reloadGrid("PdrsrvLst", pdrsrvdispid, pdrsrvGridOptions);		
	
	$("#pdrsrv .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(pdrsrvGridOptions)[0].psm_id;
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			$(this).addTab("AddProductService",'./pages?name=product/addeditproductService',"Add Product Service",{"action":"new"});
			break;
		case "edit":
			$(this).addTab("EditProductService"+data,'./pages?name=product/addeditproductService',"Edit Product Service-"+data,{"data":data,"action":"edit"});
			break;
		case "view":
			$(this).addTab("ViewProductService"+data,'./pages?name=product/addeditproductService',"View Product Service-"+data,{"data":data,"action":"view"});
			break;
		case "delete":
			swal({
				title: "Are you sure Do you want  to delete Product?",
				text: "You will not be able to recover Product now!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No cancel please!",
				confirmButtonColor:'#3095d6',
				cancelmButtonColor:'#d33',
				closeOnConfirm: false,
				closeOnCancel: false,
				showConfirmButton: true,
			},
			function(isConfirm) {
				if (isConfirm) {
			$(this).cAjax('Post','./product/deleteProdServiceMap',"psmId="+data, false, false, '', 'Error', true);
			if(strResponseText == "Success"){
				swal("Deleted!", "Selected Product deleted successfully.", "success");
				$(this).reloadGrid("PdrsrvLst", pdrsrvdispid, pdrsrvGridOptions);
			} else if(strResponseText == "DATA_INTEGRITY"){
				swal("Error in deletion!", "Selected Product is being used. So it cannnot be deleted.", "success");
			}
				}
				else {
					swal("Cancelled", "Thank you! your Product is safe :)", "error");
				}});
			break;
		case "refresh":
			$(this).reloadGrid("PdrsrvLst", pdrsrvdispid, pdrsrvGridOptions);
			swal("Refresh!","Product Service  Refresh Successfully.", "success");
			break;	
		default:
			swal("Un-implemented feature.");
		}
		return false;
	});
	
	
	$("#PdrsrvLst").closest('form').attr("oninput","$(this).onPrdsvFilterTextBoxChanged()");
}

$.fn.onPrdsvFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	pdrsrvGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.preparePdrsrvForm = function($FI){
	var option = $FI.closest("div.active").data("data"),keyValue=option && option.data ? option.data : undefined;
	var action = option ? option.action : undefined,openingStockNumber = '';
	var formContainer = $FI;
	
	
	if(option.data){
		$FI.find("#pdrsrv .pdractivestatus").attr("id", "pdractivestatus"+option.data)
		$FI.find("#pdrsrv #pdractivestatuslabel").attr("for","pdractivestatus"+option.data);
		$FI.find("#pdrsrv .pdrinactivestatus").attr("id", "pdrinactivestatus"+option.data)
		$FI.find("#pdrsrv #pdrinactivestatuslabel").attr("for","pdrinactivestatus"+option.data);
	}
	else{
		$FI.find("#pdrsrv .pdractivestatus").attr("id", "pdractivestatus")
		$FI.find("#pdrsrv #pdractivestatuslabel").attr("for","pdractivestatus");
		$FI.find("#pdrsrv .pdrinactivestatus").attr("id", "pdrinactivestatus")
		$FI.find("#pdrsrv #pdrinactivestatuslabel").attr("for","pdrinactivestatus");
	}
	
	

	$FI.cAjax('GET','./product/getProductList', '', false, false, '', 'Error', true);
	products =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#product"),products,{"key":"prdId","value":"prodName"});
	
	$(this).cAjax('GET','./website/getAllServices', '', false, false, '', 'Error', true);
	pdrservices =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#services"),pdrservices,{"key":"smId","value":"serviceName"});
	
	
	$FI.find("#saveprdsrv").on('click touchstart',function(event){
		if(formContainer.ValidateForm()){
			var pdrsrvFormJson = $.parseJSON(serializeJSONIncludingDisabledFields($FI));
			$(this).cAjaxPostJson('Post','./product/saveProductServiceMap',JSON.stringify(pdrsrvFormJson), false, false, '', 'Error', true);
		
			xhrObj = $(this).data("xhr");
			if(xhrObj.status == 409) {
				swal("Duplicate Product!", "", "warning");  
			} 
			else if (strResponseText){
				
				var pdrsrvReturn = $.parseJSON(strResponseText);
				if(pdrsrvReturn.psmId){
					swal("Saved!", "Product Service successfully.", "success");
					$(this).cAjax('GET','./website/getProductServiceList', '', false, false, '', 'Error', true);
					$FI.closeUserTab();
					$(this).getprodservicemap()
					$(this).reloadGrid("PdrsrvLst", pdrsrvdispid, pdrsrvGridOptions);		
					return true;
				}
				else{
					swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					return false;
				}
			}
			else{
					swal({title: "Error!",  text: "An internal error occurred. Please contact your system administrator.", timer: 10000, showConfirmButton: true});
					return false;
				}
		}
	})
	
	if(keyValue){
		$(this).cAjax('GET','./product/getProdServiceMap',"psmId="+keyValue, false, false, '', 'Error', true);
		var pdrsrvTeam = $.parseJSON(strResponseText);
		$(this).fillpdrsrvForm($FI,pdrsrvTeam);
		var action = option ? option.action : undefined;
		if(action && action=="view" ){
			$FI.find("input,select,button").attr("disabled","disabled")
		}else if(action && action=="edit"){
		}

	}
	
	
}

$.fn.fillpdrsrvForm = function($FI,pdrsrvTeam){
	$FI.find("#psmId").val(pdrsrvTeam.psmId);
	$FI.find("#siteid").val(pdrsrvTeam.siteId);
	$FI.find("#rate").val(pdrsrvTeam.rate);
	$FI.find("#serviceTime").val(pdrsrvTeam.serviceTime);
	$FI.find("#commDed").val(pdrsrvTeam.commDed);
	$FI.find("#remarks").val(pdrsrvTeam.remarks);
	$FI.find("#product").val(pdrsrvTeam.prdId)
	$FI.find("#product").select2();
	$FI.find("#product").trigger('change');
	$FI.find("#services").val(pdrsrvTeam.smId)
	$FI.find("#services").select2();
	$FI.find("[value='"+pdrsrvTeam.isActive+"'][name='isActive']").attr('checked','checked');
}

$.fn.onProdsrvChange = function(){
	
	var $FI = $(this).closest('form');
	var prdId = $(this).val();	$(this).cAjax('GET','./product/getProductServiceList', 'prdId='+prdId, false, false, '', 'Error', true);
	pdrservices =  $.parseJSON(strResponseText);
	$FI.populateAdvanceSelect($FI.find("#services"),pdrservices,{"key":"smId","value":"serviceName"});
	
};



$.fn.resetprdsrv=function(){
	var Form=$(this).closest('form')
	$(this).ResetAllDataInForm(Form)
};

