var statemasterlistdispid=4;
var statemasterListGridOptions;	

$.fn.getstatemst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("statemasterList",'./pages?name=mstData/statemasterList',"States",'child',currDash);

}
$.fn.statemasterList = function(){
	
	statemasterListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("statemaster", statemasterlistdispid, statemasterListGridOptions);

	$("#statemasterContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(statemasterListGridOptions)[0].stateid;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#stateNameSpan').html('');
	//		$('#remarksSpan').html('');
			$('#statemasterModel').resetMstDataForm();
			$('#statemasterModel').modal('show');
			$('#statemasterModel').find("#statusStateAct").val("Y").prop("checked", "true");
			$('#statemasterModel').find("#statusStateInAct").val("N");
		//	$('#skillCatMstModel').resetMstDataForm();
			$('#statemasterModel').fillStatemasterModelForm();
		//	$('#skillGroupMstModel').checkSkillMstLength();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./mstData/getState',"stateid="+data, false, false, '', 'Error', true);
			var stateJson = $.parseJSON(strResponseText);
			$('#stateNameSpan').html('');
	//		$('#remarksSpan').html('');
			$('#statemasterModel').modal('show');
			$('#statemasterModel').fillStatemasterModelForm(stateJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#statemaster").closest(".row").addClass("listContainer");
	$("#statemaster").closest('form').attr("oninput","$(this).onStatemasterFilterTextBoxChanged()");

};

$.fn.onStatemasterFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	statemasterListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillStatemasterModelForm = function(stateJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(stateJson.siteid);
	$(this).find("#stateid").val(stateJson.stateid);
	$(this).find("#statename").val(stateJson.statename);
//	$(this).find("#remarks").val(stateJson.remarks);
	$(this).find("input[name=isactive][value='"+stateJson.isactive+"']").prop("checked", "true");

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#statename").attr("disabled","disabled");
		$(this).find("#closeStateMstBtn2").attr("disabled", false);
		$(this).find("#statusStateAct").attr("disabled","disabled");
		$(this).find("#statusStateInAct").attr("disabled","disabled");

	}else if(operation && operation == "edit" ) {
		$(this).find("#statusStateInAct").attr("disabled",false);
	    $(this).find("#statusStateAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var stateMstJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".stateMsthiddenWholeForm").val(stateMstJson)
};

$.fn.saveStateMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
    if(navigator.onLine == false){
	    swal("Warning", "You're offline. Check your connection.", "warning");
	    return false;
      } 

	if(!FI.ValidateForm() || !$(this).validateStateMst(FI)) {
		return false;
	}
	var stateMstJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".stateMsthiddenWholeForm").val() == (stateMstJson)){
		swal("Saved!", "State saved successfully.", "success");
		$('#statemasterModel').modal('hide');
		$(this).reloadGrid("statemaster", statemasterlistdispid, statemasterListGridOptions);
		$("#statemaster").closest('form').trigger('oninput');
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./mstData/saveState',(stateMstJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate State Name!", "", "warning");  
	} else if(strResponseText) { 
		var stateMstReturn = $.parseJSON(strResponseText);
		if(stateMstReturn.stateid != null && stateMstReturn.stateid > "0"){
			swal("Saved!", "State saved successfully.", "success");
			$('#statemasterModel').modal('hide');
			$(this).reloadGrid("statemaster", statemasterlistdispid, statemasterListGridOptions);
			$("#statemaster").closest('form').trigger('oninput');
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateStateMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var statename = FI.find('#statename').val();

	
	if(!(statename.length > 0) ) {
		swal({title: "State Name Can not be blank!",  text: "Enter Value in State Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
		
	return true;
};

$.fn.checkStateMstLength = function(){
	var stateNameLength = $("#statename").val().length.toString().trim()

	    var checkLength = stateNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#stateNameSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#stateNameSpan").empty()
		}
		
//		var checkRemarkLength = remarksLength.match(/\S+/g)
//	    if(checkRemarkLength == 50){
//			$("#remarksSpan").html("(Max 50 characters are allowed)")
//				
//		}
//		else{
//			$("#remarksSpan").empty()
//		}
		
		
//		var checkPhoneLength = phoneLength.match(/\S+/g)
//	    if(checkPhoneLength == 10){
//			$("#phoneSpan").html("(Max 10 Digit Number are allowed)")
//				
//		}
//		else{
//			$("#phoneSpan").empty()
//		}
   } 

$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};