var citymasterlistdispid=3;
var citymasterListGridOptions;	

$.fn.getcitymst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("citymasterList",'./pages?name=mstData/citymasterList',"City",'child',currDash);

}
$.fn.citymasterList = function(){
	
	citymasterListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("citymaster", citymasterlistdispid, citymasterListGridOptions);

	$("#citymasterContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(citymasterListGridOptions)[0].cityid;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#locMstSpan').html('');
			$('#remarkLocSpan').html('');
			$('#citymasterModel').resetMstDataForm();
			$('#citymasterModel').modal('show');
			$('#citymasterModel').find("#statuscityAct").val("Y").prop("checked", "true");
			$('#citymasterModel').find("#statuscityInAct").val("N");
			$('#citymasterModel').fillCitymasterDropDown();
			$('#citymasterModel').fillCitymasterForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./mstData/getCitymaster',"cityId="+data, false, false, '', 'Error', true);
			var cityMasterJson = $.parseJSON(strResponseText);
			$('#locMstSpan').html('');
			$('#remarkLocSpan').html('');
			$('#citymasterModel').modal('show');
			$('#citymasterModel').fillCitymasterDropDown();
			$('#citymasterModel').fillCitymasterForm(cityMasterJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#citymaster").closest(".row").addClass("listContainer");
	$("#citymaster").closest('form').attr("oninput","$(this).onCitymasterFilterTextBoxChanged()");

};

$.fn.onCitymasterFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	citymasterListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillCitymasterDropDown = function() {
	 $(this).cAjax('GET','./mstData/getAllState', '', false, false, '', 'Error', true);
	 stateNameJson =  $.parseJSON(strResponseText)
	    $(this).populateAdvanceSelect($(this).find("#stateid"),stateNameJson,{"key":"stateId","value":"stateName"});
}

$.fn.fillCitymasterForm = function(cityMasterJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(cityMasterJson.siteid);
	$(this).find("#cityId").val(cityMasterJson.cityId);
	$(this).find("#cityName").val(cityMasterJson.cityName);
	$(this).find("#stateid").val(cityMasterJson.stateid);
	$(this).find("#remarkscity").val(cityMasterJson.remarks);
	$(this).find("input[name=isactive][value='"+cityMasterJson.isactive+"']").prop("checked", "true");

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeCitymaster2").attr("disabled", false);
		$(this).find("#statuscityAct").attr("disabled",true);
		$(this).find("#statuscityInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
		$(this).find("#statuscityInAct").attr("disabled",false);
	    $(this).find("#statuscityAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var cityMasterJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".cityMasterhiddenWholeForm").val(cityMasterJson)
};

$.fn.saveCitymaster = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	      } 

	if(!FI.ValidateForm() || !$(this).validateCityMst(FI)) {
		return false;
	}
	var cityJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".cityMasterhiddenWholeForm").val() == (cityJson)){
		swal("Saved!", "City saved successfully.", "success");
		$('#citymasterModel').modal('hide');
		$(this).reloadGrid("citymaster", citymasterlistdispid, citymasterListGridOptions);
		$("#citymaster").closest('form').trigger('oninput');
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./mstData/saveCitymaster',(cityJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate City Name!", "", "warning");  
	} else if(strResponseText) { 
		var cityMasterReturn = $.parseJSON(strResponseText);
		if(cityMasterReturn.cityId != null && cityMasterReturn.cityId > "0"){
			swal("Saved!", "City saved successfully.", "success");
			$('#citymasterModel').modal('hide');
			$(this).reloadGrid("citymaster", citymasterlistdispid, citymasterListGridOptions);
			$("#citymaster").closest('form').trigger('oninput');
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateCityMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var cityname = FI.find('#cityName').val();
	//var  taxtRate= FI.find('#taxrate').val();

	
	if(!(cityname.length > 0) ) {
		swal({title: "City Name Can not be blank!",  text: "Enter Value in City Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
	return true;
};

$.fn.checkCitymasterLength = function(){
	var cityNameLength = $("#cityName").val().length.toString().trim()
	var remarksLength =$("#remarkscity").val().length.toString()
//	var skillNoteLength =$("#skillNote").val().length.toString()
//	var remarkLength =$("#remarks").val().length.toString()
//	var phoneLength =$("#companySpocPhone").val().length.toString()
	
	
	    var checkLength = cityNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#locMstSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#locMstSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarkLocSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarkLocSpan").empty()
		}
		
		
//		var checkPhoneLength = phoneLength.match(/\S+/g)
//	    if(checkPhoneLength == 10){
//			$("#phoneSpan").html("(Max 10 Digit Number are allowed)")
//				
//		}
//		else{
//			$("#phoneSpan").empty()
//		}
   } 


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};