var orgtypelistdispid=5;
var orgtypeListGridOptions;	

$.fn.getorgtype = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("orgtypeList",'./pages?name=mstData/orgTypeList',"Organization Type",'child',currDash);

}

$.fn.orgtypeList = function(){
	
	orgtypeListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("orgtype", orgtypelistdispid, orgtypeListGridOptions);

	$("#orgtypeContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(orgtypeListGridOptions)[0].ot_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#organizationSpan').html('');
			$('#remarksorgtypeSpan').html('');
			$('#orgtypeModel').resetMstDataForm();
			$('#orgtypeModel').modal('show');
			$('#orgtypeModel').find("#statusOrgTypAct").val("Y").prop("checked", "true");
			$('#orgtypeModel').find("#statusOrgTyInAct").val("N");
			$('#orgtypeModel').fillOrgTypeModelForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./mstData/getOrgType',"otId="+data, false, false, '', 'Error', true);
			var orgtypeJson = $.parseJSON(strResponseText);
			$('#organizationSpan').html('');
			$('#remarksorgtypeSpan').html('');
			$('#orgtypeModel').modal('show');
			$('#orgtypeModel').fillOrgTypeModelForm(orgtypeJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#orgtype").closest(".row").addClass("listContainer");
	$("#orgtype").closest('form').attr("oninput","$(this).onOrgTypeFilterTextBoxChanged()");

};

$.fn.onOrgTypeFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	orgtypeListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillOrgTypeModelForm = function(orgtypeJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(orgtypeJson.siteid);
	$(this).find("#otId").val(orgtypeJson.otId);
	$(this).find("#orgType").val(orgtypeJson.orgType);
	$(this).find("#remarksorg").val(orgtypeJson.remarks);
	$(this).find("input[name=isactive][value='"+orgtypeJson.isactive+"']").prop("checked", "true");

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeOrgTypeBtn2").attr("disabled", false);
		$(this).find("#statusOrgTypAct").attr("disabled","disabled");
		$(this).find("#statusOrgTyInAct").attr("disabled","disabled");

	}else if(operation && operation == "edit" ) {
		$(this).find("#statusOrgTyInAct").attr("disabled",false);
	    $(this).find("#statusOrgTypAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var orgTypeJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".orgTypehiddenWholeForm").val(orgTypeJson)
};

$.fn.saveOrgType = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
    if(navigator.onLine == false){
	    swal("Warning", "You're offline. Check your connection.", "warning");
	    return false;
      } 

	if(!FI.ValidateForm() || !$(this).validateOrgType(FI)) {
		return false;
	}
	var orgTypeJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".orgTypehiddenWholeForm").val() == (orgTypeJson)){
		swal("Saved!", "Organization Type Name saved successfully.", "success");
		$('#orgtypeModel').modal('hide');
		$(this).reloadGrid("orgtype", orgtypelistdispid, orgtypeListGridOptions);
		$("#orgtype").closest('form').trigger('oninput');
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./mstData/saveOrgType',(orgTypeJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Organization Type Name!", "", "warning");  
	} else if(strResponseText) { 
		var orgTypeReturn = $.parseJSON(strResponseText);
		if(orgTypeReturn.otId != null && orgTypeReturn.otId > "0"){
			swal("Saved!", "Organization Type Name saved successfully.", "success");
			$('#orgtypeModel').modal('hide');
			$(this).reloadGrid("orgtype", orgtypelistdispid, orgtypeListGridOptions);
			$("#orgtype").closest('form').trigger('oninput');
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateOrgType = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var orgType = FI.find('#orgType').val();
	
	if(!(orgType.length > 0) ) {
		swal({title: "Organization Type Name Can not be blank!",  text: "Enter Value in Organization Type Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
	return true;
};

$.fn.checkOrgTypeLength = function(){
	var orgTypeLength = $("#orgType").val().length.toString().trim()
	var remarksorgLength = $("#remarksorg").val().length.toString().trim()
	    var checkLength = orgTypeLength.match(/\S+/g)
		if(checkLength == 50){
			$("#organizationSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#organizationSpan").empty()
		}
	
	 var checkRemarkLength = remarksorgLength.match(/\S+/g)
		if(checkRemarkLength == 50){
			$("#remarksorgtypeSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarksorgtypeSpan").empty()
		}
}	


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};