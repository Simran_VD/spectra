var freqMstlistdispid=7;
var freqMstListGridOptions;	

$.fn.getfreqmst = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("freqMstList",'./pages?name=mstData/freqMstList',"Frequency",'child',currDash);

}
$.fn.freqMstList = function(){
	
	freqMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("freqMst", freqMstlistdispid, freqMstListGridOptions);

	$("#freqMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(freqMstListGridOptions)[0].fm_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#freqNameSpan').html('');
			$('#remarkFreqMstSpan').html('');
			$('#freqMstModel').resetMstDataForm();
			$('#freqMstModel').modal('show');
			$('#freqMstModel').find("#statusFreqMstAct").val("Y").prop("checked", "true");
			$('#freqMstModel').find("#statusFreqMstInAct").val("N");
			$('#freqMstModel').fillFreqMstForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./mstData/getFreqMst',"fmId="+data, false, false, '', 'Error', true);
			var freqMstJson = $.parseJSON(strResponseText);
			$('#freqNameSpan').html('');
			$('#remarkFreqMstSpan').html('');
			$('#freqMstModel').modal('show');
			$('#freqMstModel').fillFreqMstForm(freqMstJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#freqMst").closest(".row").addClass("listContainer");
	$("#freqMst").closest('form').attr("oninput","$(this).onFreqMstFilterTextBoxChanged()");

};

$.fn.onFreqMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	freqMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillFreqMstForm = function(freqMstJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(freqMstJson.siteid);
	$(this).find("#fmId").val(freqMstJson.fmId);
	$(this).find("#freqName").val(freqMstJson.freqName);
	$(this).find("#remarksFreq").val(freqMstJson.remarks);
	$(this).find("input[name=isactive][value='"+freqMstJson.isactive+"']").prop("checked", "true");

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeFreqMstBtn2").attr("disabled", false);
		$(this).find("#statusFreqMstAct").attr("disabled",true);
		$(this).find("#statusFreqMstInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
		$(this).find("#statusFreqMstInAct").attr("disabled",false);
	    $(this).find("#statusFreqMstAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var freqMstJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".freqMsthiddenWholeForm").val(freqMstJson)
};

$.fn.saveFreqMst = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	      } 

	if(!FI.ValidateForm() || !$(this).validateFreqMst(FI)) {
		return false;
	}
	var freqJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".freqMsthiddenWholeForm").val() == (freqJson)){
		swal("Saved!", "Frequency Name saved successfully.", "success");
		$('#freqMstModel').modal('hide');
		$(this).reloadGrid("freqMst", freqMstlistdispid, freqMstListGridOptions);
		$("#freqMst").closest('form').trigger('oninput');
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./mstData/saveFreqMst',(freqJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Frequency Name!", "", "warning");  
	} else if(strResponseText) { 
		var freqReturn = $.parseJSON(strResponseText);
		if(freqReturn.fmId != null && freqReturn.fmId > "0"){
			swal("Saved!", "Frequency Name saved successfully.", "success");
			$('#freqMstModel').modal('hide');
			$(this).reloadGrid("freqMst", freqMstlistdispid, freqMstListGridOptions);
			$("#freqMst").closest('form').trigger('oninput');
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateFreqMst = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var freqName = FI.find('#freqName').val();
	
	if(!(freqName.length > 0) ) {
		swal({title: "Frequency Name Can not be blank!",  text: "Enter Value in Frequency Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
	return true;
};

$.fn.checkFreqMstLength = function(){
	var freqNameLength = $("#freqName").val().length.toString().trim()
	var remarksLength =$("#remarksFreq").val().length.toString()
	
	
	    var checkLength = freqNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#freqNameSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#freqNameSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarkFreqMstSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarkFreqMstSpan").empty()
		}
		
   } 


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};