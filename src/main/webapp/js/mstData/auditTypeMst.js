var auditTypeMstlistdispid=6;
var auditTypeMstListGridOptions;	

$.fn.getaudittype = function() {
	var currDash = $(this).closest('.childApp').attr('id');
	$(this).addTab("auditTypeMstList",'./pages?name=mstData/auditTypeMstList',"Audit Type",'child',currDash);

}
$.fn.auditTypeMstList = function(){
	
	auditTypeMstListGridOptions = {
			defaultColDef: {
				sortable: true,
				resizable: true,
				filter: true,
			},
			pagination: true,
			rowSelection: 'single',
		};
	$(this).reloadGrid("auditTypeMst", auditTypeMstlistdispid, auditTypeMstListGridOptions);

	$("#auditTypeMstContainer .btn-container .btn").on('click touchstart',function(){
		var operation=$(this).attr("title"),data;
		operation=operation.toLocaleLowerCase();
		if(operation != "add new" && operation != "refresh"){
			try{
				var data = onSelectionChanged(auditTypeMstListGridOptions)[0].atm_id;
				
			}
			catch(e){
				var data = undefined;
			}
			
			if(!data){
				swal("Please select a row first");
				return false;
			}
		}
		switch(operation) {
		case "add new":
			 if(navigator.onLine == false){
				    swal("Warning", "No Internet Connection!", "warning");
				    return false;
			      }
			$('#auditNameSpan').html('');
			$('#remarkAuditTypeSpan').html('');
			$('#auditTypeMstModel').resetMstDataForm();
			$('#auditTypeMstModel').modal('show');
			$('#auditTypeMstModel').find("#statusAuditTAct").val("Y").prop("checked", "true");
			$('#auditTypeMstModel').find("#statusAuditTInAct").val("N");
		//	$('#auditTypeMstModel').fillAuditTypeMstDropDown();
			$('#auditTypeMstModel').fillAuditTypeMstForm();
			break;

		case "edit":
		case "view":
			$(this).cAjax('POST','./mstData/getAuditTypeMst',"atmId="+data, false, false, '', 'Error', true);
			var auditTypeMstJson = $.parseJSON(strResponseText);
			$('#auditNameSpan').html('');
			$('#remarkAuditTypeSpan').html('');
			$('#auditTypeMstModel').modal('show');
	//		$('#locMstModel').fillLocMstDropDown();
			$('#auditTypeMstModel').fillAuditTypeMstForm(auditTypeMstJson,operation);
			break;

		default:
			swal("Not implemented.");
		}
		return false;
	});
	
	$("#auditTypeMst").closest(".row").addClass("listContainer");
	$("#auditTypeMst").closest('form').attr("oninput","$(this).onAuditTypeMstFilterTextBoxChanged()");

};

$.fn.onAuditTypeMstFilterTextBoxChanged = function() {
	var FI = $(this).closest('form');
	auditTypeMstListGridOptions.api.setQuickFilter(FI.find('.filter-text-box').val());
}

$.fn.fillAuditTypeMstForm = function(auditTypeMstJson, operation) {
	$(this).find("input, select, button").attr("disabled", false);
	$(this).find("#siteid").val(auditTypeMstJson.siteid);
	$(this).find("#atmId").val(auditTypeMstJson.atmId);
	$(this).find("#auditName").val(auditTypeMstJson.auditName);
//	$(this).find("#auditLevel").val(auditTypeMstJson.auditLevel);
	$(this).find("#remarksAuditType").val(auditTypeMstJson.remarks);
	$(this).find("input[name=isactive][value='"+auditTypeMstJson.isactive+"']").prop("checked", "true");

	if(operation && operation == "view" ) {
		$(this).find("input, select, button").attr("disabled","disabled");
		$(this).find("#closeAuditTypeBtn2").attr("disabled", false);
		$(this).find("#statusAuditTAct").attr("disabled",true);
		$(this).find("#statusAuditTInAct").attr("disabled",true);

	} else if(operation && operation == "edit" ) {
		$(this).find("#statusAuditTInAct").attr("disabled",false);
	    $(this).find("#statusAuditTAct").attr("disabled",false);

	}
	  var FI= $(this).find('form');
	  var auditTypeJson =(serializeJSONIncludingDisabledFields(FI));
	  $(".auditTypehiddenWholeForm").val(auditTypeJson)
};

$.fn.saveAuditType = function() {
	event.preventDefault();
	var FI= $(this).parents('.modal-content').find('form');
	
	 if(navigator.onLine == false){
		    swal("Warning", "You're offline. Check your connection.", "warning");
		    return false;
	      } 

	if(!FI.ValidateForm() || !$(this).validateAuditType(FI)) {
		return false;
	}
	var auditJson = (serializeJSONIncludingDisabledFields(FI));
	if($(".auditTypehiddenWholeForm").val() == (auditJson)){
		swal("Saved!", "Audit Type saved successfully.", "success");
		$('#auditTypeMstModel').modal('hide');
		$(this).reloadGrid("auditTypeMst", auditTypeMstlistdispid, auditTypeMstListGridOptions);
		$("#auditTypeMst").closest('form').trigger('oninput');
	 	$(".ag-center-cols-container").css('width','100%')

		return true;
	}
	$(this).cAjaxPostJson('Post','./mstData/saveAuditTypeMst',(auditJson), false, false, '', 'Error', true);

	xhrObj = $(this).data("xhr");
	if(xhrObj.status == 409) {
		swal("Duplicate Audit Type!", "", "warning");  
	} else if(strResponseText) { 
		var auditTypeMstReturn = $.parseJSON(strResponseText);
		if(auditTypeMstReturn.atmId != null && auditTypeMstReturn.atmId > "0"){
			swal("Saved!", "Audit Type saved successfully.", "success");
			$('#auditTypeMstModel').modal('hide');
			$(this).reloadGrid("auditTypeMst", auditTypeMstlistdispid, auditTypeMstListGridOptions);
			$("#auditTypeMst").closest('form').trigger('oninput');
		 	$(".ag-center-cols-container").css('width','100%')

		}
	}
	return false;
};

$.fn.validateAuditType = function() {
	var FI= $(this).parents('.modal-content').find('form');
	var auditName = FI.find('#auditName').val();
	
	if(!(auditName.length > 0) ) {
		swal({title: "Audit Name Can not be blank!",  text: "Enter Value in Audit Name", timer: 10000, showConfirmButton: true});
		return false;	
	}
	
	return true;
};

$.fn.checkAuditTypeMstLength = function(){
	var auditNameLength = $("#auditName").val().length.toString().trim()
	var remarksLength =$("#remarksAuditType").val().length.toString()
	
	
	    var checkLength = auditNameLength.match(/\S+/g)
		if(checkLength == 50){
			$("#auditNameSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#auditNameSpan").empty()
		}
		
		var checkRemarkLength = remarksLength.match(/\S+/g)
	    if(checkRemarkLength == 50){
			$("#remarkAuditTypeSpan").html("(Max 50 characters are allowed)")
				
		}
		else{
			$("#remarkAuditTypeSpan").empty()
		}
		
   } 


$.fn.resetMstDataForm = function() {
	$(this).find("input").val("");
	$(this).find("select").val($(this).find("select option:first").val());
	$(this).find("input,select,button").attr("disabled", false);
};